/**
 * @private
 */
interface BrowerGeolocation extends Geolocation {
}
declare namespace egret.web {
    /**
     * @private
     */
    class WebGeolocation extends EventDispatcher implements Geolocation {
        /**
         * @private
         */
        private geolocation;
        /**
         * @private
         */
        private watchId;
        /**
         * @private
         */
        constructor(option?: PositionOptions);
        /**
         * @private
         *
         */
        start(): void;
        /**
         * @private
         *
         */
        stop(): void;
        /**
         * @private
         */
        private onUpdate;
        /**
         * @private
         */
        private onError;
    }
}
