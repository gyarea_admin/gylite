module GYLite
{
					
						
	export class GYScaleSprite extends GYUIComponent
	{
		// public _initData:egret.Texture;		//初始位图data
		protected _bitmap:GYBitmap;
		
		
		/**九切片 此类矢量绘制在Shape上面，不会产生新的BitmapData
		 * @param texture 源位图数据
		 * @param rect 九宫格
		 * @param batch 是否自动合批
		 * */
		public constructor(texture:egret.Texture=null,rect:Scale9GridRect=null,batch:boolean|null=true)
		{			
			super();
			let s= this;
			s._bitmap = new GYBitmap(texture,rect,batch);
			s.addChild(s._bitmap);
		}		
		public set texture(val:egret.Texture)
		{
			this._bitmap.texture = val;
		}
		public set bitmapData(bmp:egret.Texture)
		{
			this._bitmap.texture = bmp;		
		}		
		public get texture():egret.Texture
		{
			return this._bitmap.texture;
		}
		public get bitmapData():egret.Texture
		{
			return this._bitmap.texture;
		}
		public get smoothing():boolean
		{
			return this._bitmap.smoothing;
		}
		public set smoothing(val:boolean)
		{			
			this._bitmap.smoothing = val;
		}
		public set repeatWidth(val:number)
		{
			this._bitmap.repeatWidth = val;
		}
		/**重复模式的砖块宽度*/
		public get repeatWidth():number
		{
			return this._bitmap.repeatWidth;
		}
		public set repeatHeight(val:number)
		{
			this._bitmap.repeatHeight = val;
		}
		/**重复模式的砖块高度*/
		public get repeatHeight():number
		{
			return this._bitmap.repeatHeight;
		}
		public set repeatX(val:number)
		{
			this._bitmap.repeatX = val;
		}
		/**重复模式的起始X*/
		public get repeatX():number
		{
			return this._bitmap.repeatX;
		}
		public set repeatY(val:number)
		{
			this._bitmap.repeatY = val;
		}
		/**重复模式的起始Y*/
		public get repeatY():number
		{
			return this._bitmap.repeatY;
		}
		public set width(val:number)
		{
			let s = this;			
			if(s.set_width(val))
				s._bitmap.width = val;
		}
		public get width():number
		{
			let s = this;
			return s.$width==s.$width?s.$width:s._bitmap.width;
		}
		
		public set height(val:number)
		{
			let s = this;
			if(s.set_height(val))
				s._bitmap.height = val;
		}
		public get height():number
		{
			let s = this;
			return s.$height==s.$height?s.$height:s._bitmap.height;
		}
		public set scale9GridRect(value:Scale9GridRect) 
		{
			let s = this;			
			s._bitmap.scale9GridRect=value;
			
		}		
		/**绘制模式 @see ScaleMode
		 * @param val 0 拉伸 1 裁切 2 重复 */
		public set mode(val:number)
		{
			let s = this;			
			s._bitmap.mode=val;			
		}
		public get mode():number
		{
			return this._bitmap.mode;
		}
		public set clipX(val:number)
		{
			this._bitmap.clipX = val;			
		}
		public set clipY(val:number)
		{
			this._bitmap.clipY = val;			
		}
		/**裁切的s.x坐标*/
		public get clipX():number
		{
			return this._bitmap.clipX;	
		}
		/**裁切的s.y坐标*/
		public get clipY():number
		{
			let s = this;
			return s._bitmap.clipY;
		}
		public set drawX(val:number)
		{
			let s = this;
			if(s._bitmap.drawX == val)
				return;
			s._bitmap.drawX = val;
		}
		public set drawY(val:number)
		{
			this._bitmap.drawY = val;			
		}
		/**绘制点的s.x坐标*/
		public get drawX():number
		{
			let s = this;
			return s._bitmap.drawX;	
		}
		/**绘制点的s.y坐标*/
		public get drawY():number
		{
			let s = this;
			return s._bitmap.drawY;
		}
		public updateView():void
		{
			let s = this;
			super.updateView()			
		}
		/**刷新绘图*/
		public invalidDraw():void
		{
			this._bitmap.invalidDraw();
		}

		public get scale9GridRect():Scale9GridRect
		{
			let s = this;
			return s._bitmap.scale9GridRect;
		}

		protected resetBound():void
		{
			let s= this;
			s._boundRect.bottom = s.height;
			s._boundRect.right = s.width;
			s._boundRect.y=s._boundRect.x=0;
		}
		protected resetElementsBound():void
		{
			let s= this;
			s._elementsRect.bottom = s.height;
			s._elementsRect.right = s.width;
			s._elementsRect.y=s._elementsRect.x=0;
		}
	}
}