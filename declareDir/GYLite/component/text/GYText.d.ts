/**
@author 迷途小羔羊
2012.11.18
*/
declare module GYLite {
    /**羔羊组件的基础文本组件*/
    class GYText extends GYTextBase {
        /**-1 字号自适应, 由于白鹭底层设置宽度设置为0则不计算文本尺寸直接返回0，所以此处若设置为0（宽度等于padding + textWidth,textWidth为计算前为0）会导致文本底层返回一直为0，所以为保证底层对文本进行宽度计算，此处应该大于0*/
        static defualtPadding: number;
        protected _autoHeight: boolean;
        protected _autoWidth: boolean;
        /**上下边距和,当为-1时使用GYText.defualtPadding*/
        protected _padding: number;
        protected _invalidFormat: boolean;
        /**是否使用文本测量的失效机制*/
        useInvalidText: boolean;
        clipGroup: GYGroup;
        /**是否不参与布局计算*/ offLayout: boolean;
        constructor(mul?: boolean);
        protected initComponent(): void;
        protected textChange(e: egret.Event): void;
        /**刷新文字格式,调用此函数将立即刷新文本格式，否则将在渲染前刷新，建议需要测量时请先调用此函数刷新文本*/
        validFormat(): void;
        updateView(): void;
        set bold(b: any);
        get bold(): any;
        set size(val: any);
        get size(): any;
        set color(val: any);
        get color(): any;
        set letterSpacing(val: any);
        get letterSpacing(): any;
        set indent(val: any);
        get indent(): any;
        set italic(val: any);
        get italic(): any;
        set font(val: any);
        get font(): any;
        set leading(val: any);
        get leading(): any;
        set align(val: any);
        get align(): any;
        get contentWidth(): number;
        get contentHeight(): number;
        set autoHeight(val: boolean);
        get autoHeight(): boolean;
        set autoWidth(val: boolean);
        get autoWidth(): boolean;
        /**是否描边*/
        set glow(val: boolean);
        set text(str: string);
        setText(val: string): void;
        get text(): string;
        set htmlText(str: string);
        protected setHTML(val: string): void;
        get htmlText(): string;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        get padding(): number;
        set padding(val: number);
        set textFormat(val: TextFormat);
        get textFormat(): TextFormat;
        /**格式刷新*/
        invalidFormat(): void;
        stageTextShow(): void;
        stageTextHide(): void;
        /**替换指定索引上的文本(仅替换文本，不包括下划线，此方法在动态合批时能提高渲染效率)
         * @param index 替换开始的起始索引
         * @param text 替换进去的文本
         * @param measure 是否重新测量，如果替换的文字大小一致，此处应该使用false，默认false
         * @param format 替换文字的样式
        */
        replaceTextAt(index: number, text: string, measure: boolean, format?: any): void;
    }
}
