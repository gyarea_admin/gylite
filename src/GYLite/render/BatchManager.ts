module GYLite{
    /**合批管理器，管理源纹理的合批信息*/
    export class BatchManager implements IResource, IUpdate{
		public $disposed:boolean;
		protected _batchDict:any;		
		protected _batchDisplayDict:any;
		protected _ownerTexture:egret.Texture;
		protected _invalidReference:boolean;
		public $referenceCount:number;
		constructor(o:egret.Texture){
			let s= this;
			s._batchDict = egret.createMap();
			s._batchDisplayDict = egret.createMap();
			s._ownerTexture = o;
			s.$referenceCount = 0;
		}
		/**源纹理对图集的引用数*/
		public referenceCount():number
		{
			return this.$referenceCount;
		}
		/**根据显示对象batchAtlasName属性，获取batchAtlasName名称的图集上面的合批信息
		 * @param display 显示对象		 
		*/
		public getBatchByDisplay(display:IBatch):BatchInfo
		{
			let s= this;
			return s._batchDict[display.getBatchAtlasName()];			
		}
		public addDisplay(display:IBatch, batchInfo:BatchInfo):void
		{
			let s = this;
			batchInfo.addReference(display);
			//根据显示对象记录合批信息
			s._batchDisplayDict[display.$hashCode] = batchInfo;			
		}
		/**添加一次合批记录
		 * @param display 申请合批的显示对象
		 * @param atlasInfo 合批图集小图信息
		 * @param atlas 合批图集
		 * @param batchRes 合批资源对象
		 * @return 此次合批信息
		*/
		public addBatch(display:IBatch,atlasInfo:AtlasInfo,atlas:Atlas,batchRes:ResObject):BatchInfo
		{
			let batchInfo:BatchInfo;			
			let s= this;
			if(s._batchDict[atlasInfo.atlasName])
			{
				console.warn("重复创建关联-atlas:batchInfo");
				return;
			}
			
			batchInfo = new BatchInfo(atlasInfo,atlas,batchRes,s._ownerTexture, s);
			//根据图集名称记录合批信息
			s._batchDict[atlasInfo.atlasName] = batchInfo;
			++s.$referenceCount;
			s.addDisplay(display,batchInfo);
			return batchInfo;
		}
		/***移除合批记录
		 * @param atlasInfo 合批图集小图信息
		*/
		public removeBatch(batchInfo:BatchInfo):void
		{
			let s= this;
			if(batchInfo)
			{
				delete s._batchDict[batchInfo.atlasInfo.atlasName];
				--s.$referenceCount;
				s.invalidReference();
			}
		}
		/***移除在指定display的合批记录，如果无display引用此源纹理的合批会被移除
		 * @param display 显示对象		 
		*/
		public removeDisplayBatch(display:IBatch):void
		{
			let s= this;
			let batchInfo:BatchInfo;					
			let hashCode:number;
			hashCode = display.$hashCode;
			batchInfo = s._batchDisplayDict[hashCode];
			if(batchInfo == null)
				return;					
			batchInfo.removeReference(display);
			delete s._batchDisplayDict[hashCode];
		}        		
		public get disposed(): boolean
		{
			return this.$disposed;
		}
		public dispose():void
		{
			let s= this;
			if(s.$disposed)
				return;			
			s.$disposed = true;
			for(var key in s._batchDisplayDict)							
				s._batchDisplayDict[key].clear();
			s._batchDisplayDict = null;
			s._batchDict = null;
		}

		public invalidReference():void
		{
			let s = this;
			if(s._invalidReference)
				return;
			s._invalidReference = true;
			LayoutManager.addRenderFunc(s);
		}
		public validReference():void
		{
			let s= this;
			//不存在合批，清理回收纹理对象（如文字、矢量的纹理对象，纹理对象直接销毁）
			if(s.$referenceCount == 0)
			{				
				s._ownerTexture.clear();
			}	
		}
		public updateView():void
		{
			let s= this;
			if(s._invalidReference)
			{
				s.validReference();
				s._invalidReference = false;
			}
		}
		public updating:boolean;
	}
}