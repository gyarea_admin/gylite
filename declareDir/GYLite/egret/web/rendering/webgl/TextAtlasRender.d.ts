declare namespace egret.web {
    var textAtlasRenderEnable: boolean;
    let __textAtlasRender__: TextAtlasRender;
    const property_drawLabel: string;
    class DrawLabel extends HashObject {
        private static pool;
        anchorX: number;
        anchorY: number;
        textBlocks: TextBlock[];
        private clear;
        static create(): DrawLabel;
        static back(drawLabel: DrawLabel, checkRepeat: boolean): void;
    }
    class TextAtlasRender extends HashObject {
        private readonly book;
        private readonly charImageRender;
        private readonly textBlockMap;
        private _canvas;
        private readonly textAtlasTextureCache;
        private readonly webglRenderContext;
        constructor(webglRenderContext: WebGLRenderContext, maxSize: number, border: number);
        static analysisTextNodeAndFlushDrawLabel(textNode: sys.TextNode): void;
        private convertLabelStringToTextAtlas;
        private createTextTextureAtlas;
        private get canvas();
    }
}
