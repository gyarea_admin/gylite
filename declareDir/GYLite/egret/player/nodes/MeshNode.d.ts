declare namespace egret.sys {
    /**
     * @private
     * Mesh 渲染节点
     */
    class MeshNode extends RenderNode {
        constructor();
        /**
         * 要绘制的位图
         */
        image: BitmapData;
        /**
         * 控制在缩放时是否对位图进行平滑处理。
         */
        smoothing: boolean;
        /**
         * 图片宽度。WebGL渲染使用
         */
        imageWidth: number;
        /**
         * 图片高度。WebGL渲染使用
         */
        imageHeight: number;
        /**
         * 相对偏移矩阵。
         */
        matrix: egret.Matrix;
        /**
         * UV 坐标。
         */
        uvs: number[];
        /**
         * 顶点坐标。
         */
        vertices: number[];
        /**
         * 顶点索引。
         */
        indices: number[];
        /**
         * 顶点索引。
         */
        bounds: Rectangle;
        /**
         * 使用的混合模式
         */
        blendMode: number;
        /**
         * 相对透明度
         */
        alpha: number;
        /**
         * 颜色变换滤镜
         */
        filter: ColorMatrixFilter;
        /**
         * 翻转
         */
        rotated: boolean;
        /**
         * 绘制一次位图
         */
        drawMesh(sourceX: number, sourceY: number, sourceW: number, sourceH: number, drawX: number, drawY: number, drawW: number, drawH: number): void;
        /**
         * 在显示对象的$updateRenderNode()方法被调用前，自动清空自身的drawData数据。
         */
        cleanBeforeRender(): void;
    }
}
