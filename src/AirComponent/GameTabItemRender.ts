module GYLite
{
		
	export class GameTabItemRender extends GYTabItemRender
	{
		public constructor()
		{
			super();
			var s = this;
			s.label = "tab";
			s.labelDisplay.color = ColorConst.tabColor;
		}
	}
}