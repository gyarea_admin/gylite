module GYLite
{
						
			
	export class MenuSkin extends GYSkin implements IMenuSkin
	{
		protected _background:egret.Texture;
		protected _backScaleShape:GYScaleSprite;
		protected _rect:Scale9GridRect;
		protected _list:GYLite.GYListV;
		public constructor(back:egret.Texture|GYScaleSprite=null,rect:Scale9GridRect=null, list:GYLite.GYListV=null)
		{
			super();
			var s = this;
			if(CommonUtil.GYIs(back, egret.Texture))
			{
				s.background = <egret.Texture>back;
				s.scale9GridRect=rect;
			}				
			else
			{
				if(back)
				{
					s._backScaleShape = <GYScaleSprite>back;
					s._backScaleShape.percentHeight = s._backScaleShape.percentWidth = 1;
					if(rect)
						s._backScaleShape.scale9GridRect = rect;
				}
			}
			s._list = list;
			
		}
		public release():void
		{var s = this;
			s._backScaleShape.bitmapData = null;
			if(s._list)s._list.dispose();
		}
		public set hostComponent(val:GYSprite)
		{var s = this;
			if(val==s._hostComponent)
				return;
			if(s._backScaleShape == null)
			{
				s._hostComponent=val;
				return;
			}
			if(s._hostComponent)
			{
				if(s._backScaleShape.parent)
					s._hostComponent.removeElement(s._backScaleShape);
				if(s._list && s._list.parent)
					s._hostComponent.removeElement(s._list);
			}
			s._hostComponent=val;
			if(s._hostComponent)
			{
				s._hostComponent.addElementAt(s._backScaleShape, 0);
				if(s._list)
					s._hostComponent.addElement(s._list);
				if(!isNaN(s.hostComponent.width))
					s._backScaleShape.width = s._hostComponent.width;
				if(!isNaN(s.hostComponent.height))
					s._backScaleShape.height = s._hostComponent.height;
			}
		}
		public get hostComponent():GYSprite
		{var s = this;
			return s._hostComponent;
		}
		public set background(val:egret.Texture)
		{var s = this;
			if(s._background == val)
				return;
			s._background = val
			if(val==null)
			{
				if(s._backScaleShape)
					s._backScaleShape.bitmapData=null;
				return;
			}
			if(s._backScaleShape == null)
			{
				s._backScaleShape = new GYScaleSprite(val);
				s._backScaleShape.scale9GridRect = s._rect;
				s._backScaleShape.percentHeight = s._backScaleShape.percentWidth = 1;
			}
			s._backScaleShape.bitmapData = val;
			
		}
		public get background():egret.Texture
		{
			return this._background;
		}
		public set backgroundImage(val:GYScaleSprite)
		{
			this._backScaleShape = val;
		}
		public get backgroundImage():GYScaleSprite
		{
			return this._backScaleShape;
		}		
		public set scale9GridRect(val:Scale9GridRect)
		{
			let s = this;
			s._rect=val;
			if(s._backScaleShape)
				s._backScaleShape.scale9GridRect = s._rect;
		}
		public get scale9GridRect():Scale9GridRect
		{			
			return this._rect;
		}
		public get list():GYLite.GYListV
		{
			return this._list;
		}
		public set list(val:GYLite.GYListV)
		{
			let s = this;
			if(s._list == val)
				return
			if(s._list)
			{
				val.copy(this.list);
				s._list.dispose();
			}			
			s._list = val;
			
		}
		public copy(skin:IGYSkin)
		{
			if(skin == null)
				return;
			var s = this;
			let menuSkin:IMenuSkin;
			menuSkin = (skin as MenuSkin);
			s.background = (skin as MenuSkin).background;
			s.list.copy(menuSkin.list);
		}
		public clone():IGYSkin
		{
			let s = this;
			let list:GYLite.GYListV;
			if(s._list)
			{				
				list = <GYLite.GYListV>s._list.clone();
				list.x = s._list.x;
				list.y = s._list.y;
				list.layout = s._list.layout;
			}
			return new MenuSkin(s._background, s._rect, list);
		}		
	}
}