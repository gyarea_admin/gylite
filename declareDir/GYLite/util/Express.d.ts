declare module GYLite {
    enum ExpressType {
        CONST = 0,
        VAR = 1,
        FUNC = 2,
        OPERATOR = 3,
        EXPRESS = 4,
        OTHER = 5
    }
    interface ExpStack {
        index: number;
    }
    /**量子技术之表达式解析器*/
    class Express {
        static analyze(str: string): ExpTree;
    }
    class Def {
        private static _globalDef;
        static getGlobalDef(): Def;
        operatorList: {
            [key: string]: Operator;
        };
        constList: {
            [key: string]: any;
        };
        constructor();
        /**增加运算符或者函数定义*/
        addOpDef(code: string, op: Operator): void;
        /**增加常量定义*/
        addConst(code: string, val: number): void;
    }
    class ExpTree {
        protected _leafs: ExpTree[];
        _rootValue: string;
        type: ExpressType;
        parent: ExpTree;
        _operator: Operator;
        /**语法树的公共定义集*/ publicDef: Def;
        constructor(type: ExpressType, parent?: ExpTree, value?: string, leafs?: ExpTree[]);
        addRootValue(val: string): void;
        addLeaf(leaf: ExpTree): number;
        /**节点已经满*/
        isFull(): boolean;
        getValue(): any;
        protected cal(): any;
        /**打印公式*/
        print(): string;
    }
    class Operator {
        key: string;
        /**优先级*/ _priority: number;
        /**参数数量*/ _length: number;
        cal: Function;
        _thisObj: any;
        /**
         * @param priority 默认-1，优先级 值越大优先级越高
         * @param length 默认2，运算参数长度
         * @param thisObj 默认null，运算执行函数this指向
         * @param func 默认null，运算执行函数
        */
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        print(...args: any[]): string;
        /**默认的运算执行函数*/
        defaultCal(...args: any[]): number;
    }
    class OpAdd extends Operator {
        print(...args: any[]): string;
        defaultCal(...args: any[]): number;
    }
    class OpSubtract extends Operator {
        print(...args: any[]): string;
        defaultCal(...args: any[]): number;
    }
    class OpMultiply extends Operator {
        print(...args: any[]): string;
        defaultCal(...args: any[]): number;
    }
    class OpDivide extends Operator {
        print(...args: any[]): string;
        defaultCal(...args: any[]): number;
    }
    class OpReMainder extends Operator {
        print(...args: any[]): string;
        defaultCal(...args: any[]): number;
    }
    class OpPow extends Operator {
        print(...args: any[]): string;
        defaultCal(...args: any[]): number;
    }
    class OpInt extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        print(...args: any[]): string;
        defaultCal(...args: any[]): number;
    }
    class OpMax extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        defaultCal(...args: any[]): number;
    }
    class OpMin extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        defaultCal(...args: any[]): number;
    }
    class OpRound extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        defaultCal(...args: any[]): number;
    }
    class OpFloor extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        defaultCal(...args: any[]): number;
    }
    class OpCeil extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        defaultCal(...args: any[]): number;
    }
    class OpAbs extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        defaultCal(...args: any[]): number;
    }
    class OpSin extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        defaultCal(...args: any[]): number;
    }
    class OpCos extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        defaultCal(...args: any[]): number;
    }
    class OpRnd extends Operator {
        constructor(priority?: number, length?: number, thisObj?: any, func?: Function);
        defaultCal(...args: any[]): number;
    }
}
