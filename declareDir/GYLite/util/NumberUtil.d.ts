declare module GYLite {
    class NumberUtil {
        static LARGE_NUMBER: number;
        static TINY_NUMBER: number;
        static TINY_NUMBER_01: number;
        /**四分之π*/ static QUTER_PI: number;
        /**二分之π*/ static HALF_PI: number;
        /**π*/ static PI: number;
        /**二倍π*/ static DOUBLE_PI: number;
        /**获取位为1的数量*/
        static bitCount(val: number): number;
        /**保留多少位小数，小数值为0则不保留
         * @param val 数值
         * @param num n为小数则参数为10的n次方
         **/
        static fixed(val: number, num?: number): number;
        /**求余，小数求余排除精度错误
         * @param val 数值
         * @param modVal 除数
         * @return 返回结果
         **/
        static mod(val: number, modVal: number): number;
        /**判断是否相等(排除精度问题)*/
        static isNumberEqual(valA: number, valB: number): boolean;
        /**排除小数后7位的小数*/
        static accuracyInt(val: number): number;
    }
}
