declare module GYLite {
    class MathUtils3D {
        constructor();
        static isZero(v: number): boolean;
        static nearEqual(n1: number, n2: number): boolean;
        static fastInvSqrt(value: number): number;
        static zeroTolerance: number;
        static MaxValue: number;
        static MinValue: number;
    }
}
