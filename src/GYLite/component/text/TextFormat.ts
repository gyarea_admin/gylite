module GYLite {
	export class TextFormat {
		public font:any;
		public bold:any;
		public italic:any;
		public size:any;
		public textColor:any;		
		public letterSpacing:any;
		public indent:any;
		public underline:any;
		public leading:any;
		public align:any;		
		public constructor(font:string=null, size:any=null, color:any=null, bold:any=null, italic:any=null, underline:any=null, url:string=null, target:string=null, align:string=null, leftMargin:any=null, rightMargin:any=null, indent:any=null, leading:any=null)
		{			
			let s = this;
			s.font = font;
			s.bold =bold;
			s.italic = italic;
			s.size = size;
			s.textColor = color;
			//s.letterSpacing = letterSpacing;
			s.underline = underline;
			s.indent = indent;
			s.leading = leading;
			s.align = align;
			s.letterSpacing = 0;			
		}
		public clone():TextFormat
		{			
			let s = this;
			var t:TextFormat;
			//t.letterSpacing = s.letterSpacing;
			t = new TextFormat;
			t.font = s.font;
			t.bold = s.bold;
			t.italic = s.italic;
			t.size = s.size;
			t.textColor = s.textColor;			
			t.underline = s.underline;
			t.indent = s.indent;
			t.leading = s.leading;
			t.align = s.align;			
			return t;
		}
		public setFormat(t:any):void
		{			
			let s = this;
			//t.letterSpacing = s.letterSpacing;
			t.font = s.font!=null?s.font:t.font;
			t.bold = s.bold!=null?s.bold:t.bold;
			t.italic = s.italic!=null?s.italic:t.italic;
			t.size = s.size!=null?s.size:t.size;
			t.color = s.textColor!=null?s.textColor:t.color;			
			t.indent = s.indent!=null?s.indent:t.indent;
			t.underline = s.underline!=null?s.underline:t.underline;
			t.leading = s.leading!=null?s.leading:t.leading;
			t.align = s.align!=null?s.align:t.align;			
		}
	}
}