module GYLite{
    export class GYTextElement extends BatchElement{
        /**当前字体格式*/public $format:Format;
        /**字符*/public $char:string;
        /**字体css字符串*/public $font:string;        
        /**加边距之后的居中偏移量x*/public $canvasWidthOffset:number;        
        /**加边距之后的居中偏移量y*/public $canvasHeightOffset:number;
        /**描边值*/public $stroke:number;
        /**两倍描边值*/public $stroke2:number;
        /**文本测量后的宽度*/private $measureWidth:number;
        /**文本测量后的高度*/public $measureHeight:number;        
        public constructor(){
            super();        
            let s = this;
            s.$format = new Format;
        }
        public reset(char:string="A",font:string="microsoft YaHei",element:GYTextBase=null,format:any=null):void
        {
            let s= this;
            format = s.$format.reset(element,format);            
            s.$char = char;
            s.$font = font;
            s.$measureWidth=s.width = BatchDrawParam.measure(char,font,format);
            s.$measureHeight=s.height = format.size;
            s.$stroke = format.stroke;
            const strokeDouble:number = s.$stroke * 2;
            let shadowX:number,shadowY:number;
            if(element._shadowBlur > 0)
            {
                shadowX = element._shadowBlur * element._shadowOffsetX;
                shadowY = element._shadowBlur * element._shadowOffsetY;
                if(shadowX < 0)
                    shadowX = -shadowX;
                if(shadowY < 0)
                    shadowY = -shadowY;
                s.width += shadowX;
                s.height += shadowY;
            }            
            if (strokeDouble > 0) {
                s.width += strokeDouble;
                s.height += strokeDouble;
                s.$stroke2 = strokeDouble;
            }
            else
                s.$stroke2 = 0;            
            s.width = Math.ceil(s.width);
            s.height = Math.ceil(s.height) + 4;
            s.$canvasWidthOffset = (s.width - s.$measureWidth) / 2;
            s.$canvasHeightOffset = (s.height - s.$measureHeight) / 2;
            const precision:number = 1000;
            s.$canvasWidthOffset = Math.floor(s.$canvasWidthOffset * precision) / precision;
            s.$canvasHeightOffset = Math.floor(s.$canvasHeightOffset * precision) / precision;
        }
        public dispose():void
        {
            super.dispose();
            let s= this;
            s.$format = null;            
        }
    }
    export class Format{
        /**字体颜色*/public textColor:number;
        /**描边颜色*/public strokeColor:number;
        /**描边大小*/public stroke:number;
        /**阴影颜色*/public shadowColor:number;
        /**阴影偏移X*/public shadowOffsetX:number;
        /**阴影偏移Y*/public shadowOffsetY:number;
        /**阴影大小*/public shadowBlur:number;
        /**字号*/public size:number;
        /**加粗*/public bold:boolean;
        /**倾斜*/public italic:boolean;
        /**字体*/public fontFamily:string;        
        /**
         * @param element 文本组件
         * @param format 组件的附加文本格式信息
        */
        constructor()
        {            
        }
        public clear():void
        {
            let s = this;
            s.fontFamily = s.italic = s.bold = s.size = s.stroke = s.strokeColor = s.textColor = undefined;
        }
        public reset(element:GYTextBase,format:any):Format
        {
            let s = this;
            let color:number;
            s.clear();
            s.textColor = format.textColor!=null ? format.textColor : element.$TextField[egret.sys.TextKeys.textColor];
			s.strokeColor = format.strokeColor!=null ? format.strokeColor : element.$TextField[egret.sys.TextKeys.strokeColor];
			s.stroke = format.stroke!=null ? format.stroke : element.$TextField[egret.sys.TextKeys.stroke];
			s.size = format.size!=null ? format.size : element.$TextField[egret.sys.TextKeys.fontSize];
			s.bold = format.bold!=null ? format.bold : element.$TextField[egret.sys.TextKeys.bold];
			s.italic = format.italic!=null ? format.italic : element.$TextField[egret.sys.TextKeys.italic];
			s.fontFamily = format.fontFamily!=null ? format.fontfamily : element.$TextField[egret.sys.TextKeys.fontFamily];
            s.shadowBlur = format.shadowBlur!=null ? format.shadowBlur : element._shadowBlur;
            s.shadowColor = format.shadowColor!=null ? format.shadowColor : element._shadowColor;
            s.shadowOffsetX = format.shadowOffsetX!=null ? format.shadowOffsetX : element._shadowOffsetX;
            s.shadowOffsetY = format.shadowOffsetY!=null ? format.shadowOffsetY : element._shadowOffsetY;
            color = s.textColor;                        
            return s;
        }
    }
}