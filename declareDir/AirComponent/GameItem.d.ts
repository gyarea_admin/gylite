declare module GYLite {
    class GameItem extends ItemRender {
        private static bgBitData;
        private static frameBitData;
        private static gridBitData;
        lab: GYText;
        img: GYImage;
        frame: GYImage;
        gridImg: GYImage;
        constructor();
        setData(obj: any): void;
        set selected(b: boolean);
        get selected(): boolean;
    }
}
