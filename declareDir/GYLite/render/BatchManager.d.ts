declare module GYLite {
    /**合批管理器，管理源纹理的合批信息*/
    class BatchManager implements IResource, IUpdate {
        $disposed: boolean;
        protected _batchDict: any;
        protected _batchDisplayDict: any;
        protected _ownerTexture: egret.Texture;
        protected _invalidReference: boolean;
        $referenceCount: number;
        constructor(o: egret.Texture);
        /**源纹理对图集的引用数*/
        referenceCount(): number;
        /**根据显示对象batchAtlasName属性，获取batchAtlasName名称的图集上面的合批信息
         * @param display 显示对象
        */
        getBatchByDisplay(display: IBatch): BatchInfo;
        addDisplay(display: IBatch, batchInfo: BatchInfo): void;
        /**添加一次合批记录
         * @param display 申请合批的显示对象
         * @param atlasInfo 合批图集小图信息
         * @param atlas 合批图集
         * @param batchRes 合批资源对象
         * @return 此次合批信息
        */
        addBatch(display: IBatch, atlasInfo: AtlasInfo, atlas: Atlas, batchRes: ResObject): BatchInfo;
        /***移除合批记录
         * @param atlasInfo 合批图集小图信息
        */
        removeBatch(batchInfo: BatchInfo): void;
        /***移除在指定display的合批记录，如果无display引用此源纹理的合批会被移除
         * @param display 显示对象
        */
        removeDisplayBatch(display: IBatch): void;
        get disposed(): boolean;
        dispose(): void;
        invalidReference(): void;
        validReference(): void;
        updateView(): void;
        updating: boolean;
    }
}
