declare module GYLite {
    /**资源对象，包含资源以及资源的引用计数，引用计数为0时，资源会被回收*/
    class ResObject {
        static id: number;
        type: number;
        /**资源，如BitmapData，直接调取并不增加应用计数count，不需调用relRes释放，如需增加计数，请调用getRes，释放请调用relRes*/
        res: any;
        /**资源加载的路径*/
        pathKey: string;
        /**附属资源*/
        param: any;
        inPool: boolean;
        /**生命周期*/ lifeTime: number;
        private _bornTime;
        private _id;
        private _refList;
        private _refResObjects;
        private _inWait;
        constructor();
        /**id*/
        get id(): number;
        /**设置关联的引用资源（例如图集json关联引用的图片资源）*/
        setRefResObject(resObjs: ResObject[]): void;
        /**引用资源，refObj对象将持有资源此资源的引用，
         * 同一个对象只能持同一个资源的一个引用，重复引用将被忽略
         * 当resObj被销毁，引用也会自动解除，或者调用relRes解除引用
         * @param resObj 引用资源的对象
         * */
        refRes(refObj: IResource, lifeTime?: number): any;
        /**资源释放
         * @param resObj 引用资源的对象
         * @param lifeTime 为释放的资源设定一个存活周期(毫秒)，默认NaN，不设置周期
        */
        relRes(refObj: IResource, lifeTime?: number): void;
        dispose(): void;
        clear(gc?: boolean): void;
        /**引用数量*/
        get count(): number;
        /**生产时间*/
        get bornTime(): number;
        /**是否仍然有未销毁的对象使用资源中*/
        inUse(): boolean;
        /**检测是否引用为0且存活周期有限制，进入回收队列*/
        checkUse(): void;
        /**从等候销毁队列中移除*/
        private removeFromWait;
        private static _pool;
        private static _disposeWaitList;
        static create(): ResObject;
        /**销毁轮候队列**/
        private static disposeInterval;
    }
}
