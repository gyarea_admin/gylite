module GYLite
{	
	export class GYViewEvent extends GYEvent
	{
		/**当GYUIComponent视图改变时，如width/height/scaleX/scaleY/x/y/right/bottom*/
		public static VIEWCHANGE:string="GY_viewChange";
		/**动画播放完毕*/
		public static ANIMATION_END:string="GY_animation_end";
		/**视图更新完毕*/
		public static UPDATE_COMPLETE:string="GY_UpdateComplete";
		/**radio组件被选择时，回调data [上次选择的radio，当前选择的radio]*/
		public static RADIO_SELECTED:string="GY_radio_selected";
		/**组件被选择时*/
		public static SELECTED:string="GY_selected";
		/**组件取消选择时触发*/
		public static UNSELECTED:string="GY_unSelected";
		/**列表格子刷新*/
		public static GRID_UPDATE:string="GY_grid_update";
		/**列表数据刷新*/
		public static DATA_UPDATE:string="GY_data_upate";
		/**垂直拖动到尽头*/
		public static DRAGEND_VERTICAL:string="GY_dragEnd_vertical";
		/**水平拖动到尽头*/
		public static DRAGEND_HORIZONAL:string="GY_dragEnd_horizonal";
		/**垂直滚动(点击箭头按钮、滚轮)到尽头MIN*/
		public static SCROLL_VERTICAL_MIN:string="GY_scroll_vertical_min";
		/**水平滚动(点击箭头按钮、滚轮)到尽头MIN*/
		public static SCROLL_HORIZONAL_MIN:string="GY_scroll_horizonal_min";
		/**垂直滚动(点击箭头按钮、滚轮)到尽头MAX*/
		public static SCROLL_VERTICAL_MAX:string="GY_scroll_vertical_max";
		/**水平滚动(点击箭头按钮、滚轮)到尽头MAX*/
		public static SCROLL_HORIZONAL_MAX:string="GY_scroll_horizonal_max";
		/**垂直滚动到尽头*/
		public static SCROLLEND_VERTICAL:string="GY_scrollEnd_vertical";
		/**水平滚动到尽头*/
		public static SCROLLEND_HORIZONAL:string="GY_scrollEnd_horizonal";
		/**拖动结束*/
		public static DRAGSTOP:string="GY_dragStop";
		/**拖动开始*/
		public static DRAGSTART:string="GY_dragStart";
		/**裁切滚动*/
		public static CLIP_SCROLL:string="GY_clip_scroll";
		/**静态文本显示*/
		public static STATIC_TEXT_SHOW:string="GY_static_text_show";
		/**静态文本隐藏*/
		public static STATIC_TEXT_HIDE:string="GY_static_text_hide";		
		public constructor(type:string, bubbles:boolean=false, cancelable:boolean=false, data:any=null)
		{
			super(type, bubbles, cancelable,data);			
		}
	}
}