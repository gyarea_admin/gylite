declare namespace egret.sys {
    /**
     * @private
     * 组渲染节点,用于组合多个渲染节点
     */
    class GroupNode extends RenderNode {
        /**
         * 相对偏移矩阵。
         */
        matrix: egret.Matrix;
        constructor();
        addNode(node: RenderNode): void;
        /**
         * 覆盖父类方法，不自动清空缓存的绘图数据，改为手动调用clear()方法清空。
         * 这里只是想清空绘制命令，因此不调用super
         */
        cleanBeforeRender(): void;
        $getRenderCount(): number;
    }
}
