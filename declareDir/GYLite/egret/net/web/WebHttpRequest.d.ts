declare namespace egret.web {
    /**
     * @private
     */
    class WebHttpRequest extends EventDispatcher implements HttpRequest {
        /**
         * @private
         */
        constructor();
        /**
         * @private
         */
        private _xhr;
        /**
         *
         */
        timeout: number;
        /**
         * @private
         * 本次请求返回的数据，数据类型根据responseType设置的值确定。
         */
        get response(): any;
        /**
         * @private
         */
        private _responseType;
        /**
         * @private
         * 设置返回的数据格式，请使用 HttpResponseType 里定义的枚举值。设置非法的值或不设置，都将使用HttpResponseType.TEXT。
         */
        get responseType(): "" | "arraybuffer" | "blob" | "document" | "json" | "text";
        set responseType(value: "" | "arraybuffer" | "blob" | "document" | "json" | "text");
        /**
         * @private
         */
        private _withCredentials;
        /**
         * @private
         * 表明在进行跨站(cross-site)的访问控制(Access-Control)请求时，是否使用认证信息(例如cookie或授权的header)。 默认为 false。(这个标志不会影响同站的请求)
         */
        get withCredentials(): boolean;
        set withCredentials(value: boolean);
        /**
         * @private
         */
        private _url;
        private _method;
        /**
         * @private
         *
         * @returns
         */
        private getXHR;
        /**
         * @private
         * 初始化一个请求.注意，若在已经发出请求的对象上调用此方法，相当于立即调用abort().
         * @param url 该请求所要访问的URL该请求所要访问的URL
         * @param method 请求所使用的HTTP方法， 请使用 HttpMethod 定义的枚举值.
         */
        open(url: string, method?: string): void;
        /**
         * @private
         * 发送请求.
         * @param data 需要发送的数据
         */
        send(data?: any): void;
        /**
         * @private
         * 如果请求已经被发送,则立刻中止请求.
         */
        abort(): void;
        /**
         * @private
         * 返回所有响应头信息(响应头名和值), 如果响应头还没接受,则返回"".
         */
        getAllResponseHeaders(): string;
        private headerObj;
        /**
         * @private
         * 给指定的HTTP请求头赋值.在这之前,您必须确认已经调用 open() 方法打开了一个url.
         * @param header 将要被赋值的请求头名称.
         * @param value 给指定的请求头赋的值.
         */
        setRequestHeader(header: string, value: string): void;
        /**
         * @private
         * 返回指定的响应头的值, 如果响应头还没被接受,或该响应头不存在,则返回"".
         * @param header 要返回的响应头名称
         */
        getResponseHeader(header: string): string;
        /**
         * @private
         */
        private onTimeout;
        /**
         * @private
         */
        private onReadyStateChange;
        /**
         * @private
         */
        private updateProgress;
        /**
         * @private
         */
        private onload;
        /**
         * @private
         */
        private onerror;
    }
}
