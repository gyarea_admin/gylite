module GYLite
{
		
		
	export class GameExcelRowGrid extends ExcelRowGrid
	{
		public constructor()
		{
			super();
		}
		protected initComponent():void
		{var s = this;
			s.width = 35;
			super.initComponent();			
			s._text.width = s.width;
			s._text.color = 0xfffae4;
			s._text.size = 14;
			s._text.bold = true;
			// s._img.verticalCenter = 8;
			var g:egret.Graphics = s._drager.graphics;
			g.clear();
			g.beginFill(0, 0.2);
			g.drawRect(0,0,s.width,5);
			g.endFill();
			// s.cacheAsBitmap = true;
		}
		protected drawBg():void
		{var s = this;
			s.graphics.clear();
			s.graphics.beginFill(0xcccccc,0.5);
			s.graphics.drawRect(0,0,s.width,s.height);
			s.graphics.beginFill(0x4a2e0a,1);
			s.graphics.drawRect(1,1,s.width-2,s.height - 2);
			s.graphics.endFill();
		}
		/**@inheriDoc */
		public setData(obj:any):void
		{var s = this;
			s._data = obj;
			if(s._data==null)
				s._text.text ="无";
			else
				s._text.text =String(s._data);
			// s._img.draw(s._text);
			s.width = 35;
			s.height = 30;
		}
	}
}