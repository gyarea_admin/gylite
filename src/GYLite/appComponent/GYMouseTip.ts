module GYLite
{
			
	export class GYMouseTip
	{
		public static t:GYText;
		private static flyTxt:GYText;
		private static count:number;
		public static showFlyTip(val:string):void
		{
			if(GYMouseTip.flyTxt == null)
			{
				GYMouseTip.flyTxt = new GYText;
				GYMouseTip.flyTxt.color = 0xff0000;
				GYMouseTip.flyTxt.size = 14;
				GYMouseTip.flyTxt.touchEnabled = false;
			}
			GYMouseTip.count = 0;
			if(GYMouseTip.flyTxt.parent == null)
			{
				GYSprite.stage.addChild(GYMouseTip.flyTxt);
				CommonUtil.addStageLoop(GYMouseTip.flyLoop,null);
			}
			GYMouseTip.flyTxt.htmlText = val;
			GYMouseTip.flyTxt.validFormat();
			GYMouseTip.flyTxt.x = Math.min(GYSprite.stage.stageWidth - GYMouseTip.flyTxt.width,GYSprite.stageX);
			GYMouseTip.flyTxt.y = Math.max(GYSprite.stageY,50 + GYMouseTip.flyTxt.height);
		}
		private static flyLoop(t:number):void
		{
			++GYMouseTip.count;
			if(GYMouseTip.count < 5)
				GYMouseTip.flyTxt.y -= 15;
			else if(GYMouseTip.count > 100)
			{
				CommonUtil.delStageLoop(GYMouseTip.flyLoop,null);
				GYSprite.stage.removeChild(GYMouseTip.flyTxt);
			}
		}
		public static showTip(val:string):void
		{
			if(GYMouseTip.t == null)
			{
				GYMouseTip.t = new GYText;
				GYMouseTip.t.color = 0xff0000;
				GYMouseTip.t.size = 14;
				GYMouseTip.t.touchEnabled = false;
			}
			if(GYMouseTip.t.parent == null)
			{
				GYSprite.stage.addChild(GYMouseTip.t);
				CommonUtil.addStageLoop(GYMouseTip.tipLoop,null);
			}
			GYMouseTip.t.htmlText = val;
		}
		private static tipLoop(l:number):void
		{
			GYMouseTip.t.x = GYSprite.stageX;
			GYMouseTip.t.y = GYSprite.stageY;
		}
		public static hideTip():void
		{
			if(GYMouseTip.t.parent)
			{
				GYSprite.stage.removeChild(GYMouseTip.t);
				CommonUtil.delStageLoop(GYMouseTip.tipLoop,null);
			}
		}
	}
}