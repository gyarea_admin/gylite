declare module GYLite {
    interface ILabelData {
        [key: string]: GYLite.ImageLabelData;
    }
}
