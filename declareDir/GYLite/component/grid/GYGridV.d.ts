/**
 @author 迷途小羔羊
 2015.3.5
 */
declare module GYLite {
    class GYGridV extends GYGridBase {
        /**垂直滚动Grid组件
        @inheritDoc*/
        constructor(sizeW: number, sizeH: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        protected initComponent(): void;
        /**inheritDoc*/
        scrollToIndex(val: number): void;
        /**inheritDoc*/
        scrollToEndIndex(val: number): void;
        protected selectLine(data: any, selectedData: any, ctrlKey: boolean, checkNext?: boolean): void;
        /**@inheritDoc*/
        updateGrid(): void;
        /**@inheritDoc*/
        get dataProvider(): Array<any>;
        set dataProvider(val: Array<any>);
        protected vScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        set rows(val: number);
        get rows(): number;
        set cols(val: number);
        get cols(): number;
        protected boxNumChange(update?: boolean): void;
        /**刷新列表可见的所有项*/
        updateItems(): void;
        set height(val: number);
        get height(): number;
        set width(val: number);
        get width(): number;
        protected wheelRoll(e: any): void;
        /**滚动条 0自动 1显示 2不显示*/
        set vScrollerPolicy(val: number);
        protected downGroup(e: egret.TouchEvent): void;
        protected dragLoop(t: number): void;
        protected groupDrag(): void;
        protected groupDragStop(): void;
        scrollToNextItem(): number;
        scrollToPreItem(): number;
        protected scrollEnd(e?: GYScrollerEvent): void;
    }
}
