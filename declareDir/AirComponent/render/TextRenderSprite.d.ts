declare module GYLite {
    class TextRenderSprite extends GYText implements IRenderTestElement {
        private _tick;
        changeTime: number;
        private _owner;
        constructor(o: TextRenderGroup, color: number, size: number);
        get graphics(): GYGraphics;
        private _v;
        private _a;
        initX: number;
        initY: number;
        setPos(x: number, y: number): void;
        loop(t: number): void;
    }
}
