module GYLite
{
	export class GridData
	{
		public width:number;
		public height:number;
		public posX:number;
		public posY:number;
		public index:number;
		constructor()
		{
			let s= this;
			s.width = s.height = NaN;
			s.posX = s.posY = 0;
			s.index = 0;
		}
		// public isSet:boolean=false;
	}
}