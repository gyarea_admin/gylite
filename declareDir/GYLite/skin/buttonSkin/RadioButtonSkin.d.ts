/**
 @author 迷途小羔羊
 2015.6.5
 */
declare module GYLite {
    class RadioButtonSkin extends GYSkin implements IRadioButtonSkin {
        protected _stsVec: egret.Texture[];
        protected _curSkin: any;
        protected _text: GYText;
        protected _hasLabel: boolean;
        protected _gap: number;
        /**按钮皮肤，自定义需实现接口IRadioButtonSkin
         * 皮肤数组，包括8个状态的Bitmapdata
         * */
        constructor(skinVec: egret.Texture[]);
        release(): void;
        drawSkin(state: number): void;
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        get width(): number;
        get height(): number;
        set width(value: number);
        set height(value: number);
        set gap(val: number);
        get gap(): number;
        /**当s.label被赋值的时候自动产生Mytext文本(在此之前不存在s.labelDisplay)，在s.label为null时，文本对象不会被清除
         * */
        set label(val: string);
        get label(): string;
        get labelDisplay(): GYText;
        set labelDisplay(val: GYText);
        /**此函数只克隆s.labelDisplay的textFormat*/
        cloneLabel(): GYText;
        /**此函数复制皮肤属性 如s.label s.gap等皮肤的属性*/
        copy(skin: IGYSkin): void;
        /**此函数不克隆s.labelDisplay*/
        clone(): IGYSkin;
    }
}
