declare module GYLite {
    class GYExcel extends GYSprite implements IKeyBoardObject {
        protected _dataGrid: GYDataGrid;
        protected _rowsDataList: GYDataListV;
        protected _colsDataList: GYDataListH;
        protected _selFrame: ExcelFrame;
        protected _frameRect: egret.Rectangle;
        protected _movePadding: number;
        config: any;
        /**GYExcel是由GYDataGrid组合而成是简易excel组件
         * 由excel包下的类组合而成，可见GYDataGrid的功能强大，更多功能请自行扩展，或者自己构建一个
         * */
        constructor(cfg?: any);
        protected init(): void;
        protected getExcelDrag(): GYSprite;
        keyFocus(): boolean;
        kDown(keyCode: number): void;
        kUp(keyCode: number): void;
        protected selectedItem(e: GYViewEvent): void;
        protected colChange(e: GYGridEvent): void;
        protected rowChange(e: GYGridEvent): void;
        protected dragFrame(d: DraggerHandle): void;
        protected valueCommit(e: GYEvent): void;
        protected vScrollerChange(e: GYScrollerEvent): void;
        protected hScrollerChange(e: GYScrollerEvent): void;
        protected dataGridViewChange(e: GYViewEvent): void;
        protected gyGridDataSizeChange(drager: DraggerHandle): void;
        /**重写此方法，自定义表格的格子*/
        protected createExcelGrid(): IItemRender;
        /**重写此方法，自定义设置表格的格子
         * @param grid 格子 obj 数据
         * @param obj 数据ExcelData类型
         * */
        protected setExcelGrid(grid: IItemRender, obj: any): void;
        /**重写此方法，自定义创建行标的格子
         * @return ExcelRowGrid
         * */
        protected createRowGrid(): IItemRender;
        /**重写此方法，自定义设置行标的格子
         * @param grid 格子 obj 数据
         * */
        protected setRowGrid(grid: IItemRender, obj: any): void;
        /**重写此方法，自定义创建列标的格子
         * @return ExcelColGrid
         * */
        protected createColGrid(): IItemRender;
        /**重写此方法，自定义设置列标的格子
         * @param grid 格子 obj 数据
         * */
        protected setColGrid(grid: IItemRender, obj: any): void;
        /**重写此方法，自定义计算规则
         * @param nowData 数据ExcelData
         * @param nextData 数据ExcelData
         * @add 递增的值（如果格子数据是数字）
         * */
        protected calculateRule(nowData: ExcelData, nextData: ExcelData, add?: number): void;
        /**设置行标的宽度*/
        set rowsListWidth(val: number);
        /**设置列标的宽度*/
        set colsListHeight(val: number);
        /**设置数据*/
        set dataProvider(val: Array<any>);
        /**设置某列宽度
         * @param ind 列索引 val 宽度值
         * */
        setGridWidth(ind?: number, val?: number): void;
        /**设置某行高度
         * @param ind 行索引 val 高度值
         * */
        setGridHeight(ind?: number, val?: number): void;
        /**获取行数据
         * @param ind 行索引
         * @return 格子信息
         * */
        getRowGridData(ind?: number): GridData;
        /**获取列数据
         * @param ind 列索引
         * @return 格子信息
         * */
        getColGridData(ind?: number): GridData;
        /**获取行标的行数据
         * @param 索引
         * */
        getRowListData(len?: number): void;
        /**获取列标的列数据
         * @param 索引
         * */
        getColListData(len?: number): void;
        /**选择起始格子 */
        selStartGrid(grid: IItemRender): void;
        /**选择结束数据 */
        selStartData(d: any): void;
        /**选择结束格子*/
        selEndGrid(grid: IItemRender): void;
        /**选择结束数据*/
        selEndData(d: any): void;
        /**选择计算的结束格子*/
        selCalEndGrid(grid: IItemRender): void;
        /**选择计算的结束数据*/
        setEndData(d: any): void;
        /**清除选择*/
        clearSel(grid: IItemRender): void;
        /**行计算 从第一行开始，根据运算规则s.calculateRule方法往下一行运算
         * @param fromRow 起始行
         * @param toRow 结束行
         * */
        calculateRow(fromRow?: number, toRow?: number, fromCol?: number, toCol?: number): void;
        /**列计算 从第一列开始，根据运算规则s.calculateRule方法往下一列运算
         * @param fromCol 起始列
         * @param toCol 结束列
         * */
        calculateCol(fromRow?: number, toRow?: number, fromCol?: number, toCol?: number): void;
        /**行列移动 从startRow,endRow,startCol,endCol选中的数据，移动到位置起始格toRow toCol */
        moveData(startRow?: number, endRow?: number, startCol?: number, endCol?: number, toRow?: number, toCol?: number): void;
        /**获取当前滚动的s.x坐标位置*/
        get posX(): number;
        /**获取当前滚动的s.y坐标位置*/
        get posY(): number;
        /**设置excel区域宽度*/
        setDataGridWidth(val: number): void;
        /**设置excel区域高度*/
        setDataGridHeight(val: number): void;
        /**设置滚动条高度 */
        setScrollerHeight(val: number): void;
        /**设置滚动条宽度 */
        setScrollerWidth(val: number): void;
        /**边缘移动的感应距离*/
        get movePadding(): number;
        set movePadding(value: number);
    }
}
