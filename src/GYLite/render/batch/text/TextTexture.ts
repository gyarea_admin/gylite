/**
@author 迷途小羔羊
2022.09.27
*/
module GYLite
{
    export class TextTexture extends BatchTexture implements IResource,IPoolObject{             
        public constructor(){
            super();                        
        }        
        protected beforeToPool():void
        {
            let s= this;
            delete TextTexture._textTextureMap[s.hash];
            super.beforeToPool();            
        }

        public static batchParam:BatchDrawParam = new BatchDrawParam(BatchDrawType.TEXT);
        private static _textTextureMap:any = egret.createMap();
        public static getFontString(gytext:GYTextBase, format:any):string {
            let italic:boolean = format.italic?format.italic:gytext.$textFormat.italic;
            let bold:boolean = format.bold?format.bold:gytext.$textFormat.bold;
            let size:number = format.size?format.size:gytext.$textFormat.size;            
            let fontFamily:string = format.fontFamily?format.fontFamily:gytext.fontFamily;
            let font:string = italic ? "italic " : "normal ";
            font += bold ? "bold " : "normal ";
            font += size + "px " + fontFamily;
            return font;
        }
        public static getDescription(gytext:GYTextBase,format:any = null):{desc:string,font:string}{            
            let s = this;
            let desc:string;            
            let font:string = TextTexture.getFontString(gytext,format);            
            let strokeColor:number = (format.strokeColor != null ? format.strokeColor : gytext.strokeColor);
            let stroke:number = (format.stroke != null ? format.stroke : gytext.stroke);
            desc = font;            
            desc += "," + (format.textColor != null ? format.textColor : gytext.textColor);
            desc += "," + strokeColor;            
            if(gytext._shadowBlur > 0)
                desc += gytext._shadowBlur + "," + gytext._shadowColor + "," + gytext._shadowOffsetX + "," + gytext._shadowOffsetY;
            if (stroke) {
                desc += "," + stroke * 2;
            }   
            return {desc:desc,font:font};         
        } 
        
        public static getTextTexture(char:string, gytext:GYTextBase, format:any=null):TextTexture
        {
            let tex:TextTexture;
            var hsCode:number;
            let description:any;
            description = TextTexture.getDescription(gytext,format);
            hsCode = egret.NumberUtils.convertStringToHashCode(char + ':' + description.desc);
            tex = TextTexture._textTextureMap[hsCode];            
            if(tex == null)
            {
                let textElement:GYTextElement;
                textElement = <GYTextElement>PoolUtil.fromPool(GYTextElement);
                textElement.reset(char,description.font,gytext,format);                
                tex = <TextTexture>PoolUtil.fromPool(TextTexture);
                tex.reset(hsCode, textElement);           
                TextTexture._textTextureMap[hsCode] = tex;
            } 
            return tex;
        }

    }
}