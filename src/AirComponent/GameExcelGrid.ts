module GYLite
{
					
			
	export class GameExcelGrid extends ExcelGrid
	{
		public constructor()
		{
			super();
		}		
		protected initComponent():void
		{var s = this;
			super.initComponent();
			s._text.color = ColorConst.fontColor;			
			// s.cacheAsBitmap = true;
		}
		public set selected(b:boolean)
		{var s = this;
			egret.superSetter(GameExcelGrid, s, "selected", b);
			// super.selected = b;
			s.drawBg(b?0x00ff00:0xcccccc);
		}
		public get selected():boolean
		{var s = this;
			return egret.superGetter(GameExcelGrid, s, "selected");
		}
		protected widthAndHeightChange(e:GYViewEvent):void
		{var s = this;
			s._text.width = s.width;
			// s._img.draw(s._text);
			// s._text.y = (s.height - s._text.height >> 1) + 5;
			s.drawBg(s._selected?0x00ff00:0xcccccc);
		}
		protected drawBg(c:number=0xcccccc):void
		{var s = this;
			s.graphics.clear();			
			s.graphics.beginFill(0x3e342a,1);
			s.graphics.drawRect(0,0,s.width,s.height);
			s.graphics.beginFill(c,0.5);
			s.graphics.drawRect(1,1,s.width-2,1);
			s.graphics.drawRect(1,2,1,s.height - 2);
			s.graphics.drawRect(s.width-2,2,1,s.height - 2);
			s.graphics.drawRect(1,s.height-1,s.width-2,1);
		}
	}
}