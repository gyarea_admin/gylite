/**
 @author 迷途小羔羊
 2015.6.5
 */
module GYLite
{
							
					
	export class RadioButtonSkin extends GYSkin implements IRadioButtonSkin
	{
		protected _stsVec:egret.Texture[];
		protected _curSkin:any;
		protected _text:GYText;
		protected _hasLabel:boolean;
		protected _gap:number=3;
		/**按钮皮肤，自定义需实现接口IRadioButtonSkin
		 * 皮肤数组，包括8个状态的Bitmapdata
		 * */
		public constructor(skinVec:egret.Texture[])
		{
			super();
			var s = this;
			s._stsVec=skinVec;
			if(s._stsVec.length > 8 || s._stsVec.length == 0)
				throw new Error("按钮皮肤参数不对！#" + s._stsVec.length);
			s._curSkin=new GYScaleSprite;
			s._curSkin.texture = s._stsVec[0];
		}
		public release():void
		{var s = this;
			s._curSkin.bitmapData = null;
		}
		public drawSkin(state:number):void
		{var s = this;
			s._curSkin.texture=(state<s._stsVec.length && s._stsVec[state])? s._stsVec[state] : s._stsVec[0];
			if(s._hasLabel)
			{
				s._text.x = (s._curSkin.texture?s._curSkin.texture.textureWidth:0) + s._curSkin.x + s._gap;				
			}				
		}
		public set hostComponent(val:GYSprite)
		{var s = this;
			if(val==s._hostComponent)
				return;
			if(s._hostComponent)
			{
				if(s._curSkin.parent)
					s._hostComponent.removeElement(s._curSkin);
				if(s._hasLabel)
					s._hostComponent.removeElement(s._text);
			}
			s._hostComponent=val;
			if(s._hostComponent)
			{
				s._hostComponent.addElementAt(s._curSkin, 0);
				if(s._hasLabel)
				{
					s._hostComponent.addElement(s._text);
					s._text.x = s._curSkin.width + s._curSkin.x;
				}
			}
		}
		public get hostComponent():GYSprite
		{var s = this;
			return s._hostComponent;
		}
		public get width():number
		{var s = this;
			return s._curSkin.width;
		}
		public get height():number
		{var s = this;
			return s._curSkin.height;
		}
		public set width(value:number) 
		{var s = this;
			s._curSkin.width = value;
		}
		public set height(value:number) 
		{var s = this;
			s._curSkin.height = value;
		}
		public set gap(val:number)
		{var s = this;
			s._gap = val;
			if(s._hasLabel)
				s._text.x = (s._curSkin.texture?s._curSkin.texture.textureWidth:0) + s._curSkin.x + s._gap;				
		}
		public get gap():number
		{var s = this;
			return s._gap;
		}
		/**当s.label被赋值的时候自动产生Mytext文本(在此之前不存在s.labelDisplay)，在s.label为null时，文本对象不会被清除
		 * */
		public set label(val:string)
		{var s = this;
			if(val == null)
			{
				if(s._hasLabel)
				{
					s._hasLabel=false;
					if(s._hostComponent)
						s._hostComponent.removeElement(s._text);
				}
				return;
			}
			
			if(s._text==null)
			{
				s._text=new GYText;	
				s._text.verticalCenter = 3;				
				if(s._hostComponent)
				{
					s._hostComponent.addElement(s._text);
					s._text.x = s._curSkin.width + s._curSkin.x + s._gap;
				}
			}
			s._hasLabel=true;
			s._text.text=val;			
		}
		public get label():string
		{var s = this;
			if(s._hasLabel)
				return s._text.text;
			return "";
		}
		public get labelDisplay():GYText
		{var s = this;
			if(s._hasLabel)
				return s._text;
			return null;
		}
		public set labelDisplay(val:GYText)
		{var s = this;
			s._text = val;
			if(s._text)
			{
				s._hasLabel = true;
				if(s._text.parent != s._hostComponent)
				{
					s._hostComponent.addElement(s._text);
					s._text.x = s._curSkin.width + s._curSkin.x + s._gap;					
				}
			}
		}
		/**此函数只克隆s.labelDisplay的textFormat*/
		public cloneLabel():GYText
		{var s = this;
			if(s._hasLabel)
			{
				var newText:GYText=new GYText;
				var newFormat:TextFormat = new TextFormat;
				var f:TextFormat = s._text.textFormat;
				newFormat.align = f.align;
				newFormat.bold = f.bold;
				newFormat.textColor = f.textColor;
				newFormat.size = f.size;
				newFormat.font = f.font;
				newFormat.italic = f.italic;
				newFormat.underline = f.underline;
				newFormat.leading = f.leading;
				newText.textFormat = newFormat;
				newText.layoutMode = s._text.layoutMode.clone();
				newText.x = s._text.x;
				newText.y = s._text.y;
				newText.text = s._text.text;
				return newText;
			}
			return null;
		}
		/**此函数复制皮肤属性 如s.label s.gap等皮肤的属性*/
		public copy(skin:IGYSkin)
		{var s = this;
			if(skin == null)
				return;
			var sk:IRadioButtonSkin = skin as RadioButtonSkin
			s.gap = sk.gap;
			s.labelDisplay = sk.labelDisplay;
		}
		/**此函数不克隆s.labelDisplay*/
		public clone():IGYSkin
		{var s = this;
			var radioButtonSkin:RadioButtonSkin=new RadioButtonSkin(s._stsVec);
			return radioButtonSkin;
		}
	}
}