declare module GYLite {
    class GYThemeEvent extends GYEvent {
        /**主题改变*/
        static THEME_CHANGE: string;
        constructor(type: string, bubbles?: boolean, cancelable?: boolean);
    }
}
