declare namespace egret.web {
    /**
     * @private
     */
    function getOption(key: string): string;
}
