/**
 @author 迷途小羔羊
 2015.6.2
 */
module GYLite
{
		
	/**TabData是记录Tab组件按钮绑定的数据*/
	export class TabData
	{
		/**按钮标签*/
		public label:string;
		/**按钮控制的容器*/
		public groupCls:any;
		private _group:GYUIComponent;
		/**附带参数*/
		public param:any;
		private _parent:IGYContainer;
		private _selected:boolean;
		/**@s.param lab 按钮标签
		 * @s.param grp 容器类型
		 * @s.param pr 父级容器
		 * */
		public constructor(lab:string,grp:any,pr:IGYContainer)
		{
			let s = this;
			s._group=null;		
			s.param=null;		
			s._selected=false;
			s.label = lab;
			s.groupCls = grp;
			s._parent = pr;
		}
		public get parent():IGYContainer
		{
			let s = this;
			return s._parent;
		}
		public set selected(val:boolean)
		{
			let s = this;
			if(s._selected == val)return;
			s._selected = val;
			if(s.groupCls!=null)
			{
				if(s._group == null)
					s._group = new s.groupCls;
				if(s._selected)
					s.parent.addElement(s._group);
				else
					s.parent.removeElement(s._group);
			}
		}

		public get selected():boolean
		{
			let s = this;
			return s._selected;
		}

		public get group():GYUIComponent
		{
			let s = this;
			return s._group;
		}


	}
}