declare module GYLite {
    class ButtonSkin extends GYSkin implements IButtonSkin {
        static defaultTextVerticalCenter: number;
        protected _stsVec: egret.Texture[];
        protected _curSkin: any;
        protected _text: GYText;
        protected _hasLabel: boolean;
        protected _scale9GridRect: Scale9GridRect;
        /**按钮皮肤，自定义需实现接口IButtonSkin
         * 皮肤数组，包括8个状态的Texture
         * */
        constructor(skinVec: egret.Texture[], rect?: Scale9GridRect);
        release(): void;
        drawSkin(state: number): void;
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        get width(): number;
        get height(): number;
        set width(value: number);
        set height(value: number);
        set scale9GridRect(value: Scale9GridRect);
        get scale9GridRect(): Scale9GridRect;
        /**当s.label被赋值的时候自动产生Mytext文本(在此之前不存在s.labelDisplay)，在s.label为null时，文本对象不会被清除* */
        set label(val: string);
        get label(): string;
        get labelDisplay(): GYText;
        set labelDisplay(val: GYText);
        /**此函数只复制s.labelDisplay的textFormat部分属性 和复制布局*/
        cloneLabel(): GYText;
        /**此函数复制皮肤属性 如s.label等除按钮本身的其他属性（目前只有Label）*/
        copy(skin: IGYSkin): void;
        /**此函数不克隆s.labelDisplay*/
        clone(): IGYSkin;
    }
}
