declare module GYLite {
    class StatusUtil {
        statusSprite: GYSprite;
        private fpsText;
        private memoryText;
        private background;
        private loopCount;
        private rendMaxCount;
        private gyBitmapMemory;
        private flashVersionText;
        private parent;
        private lastTime;
        private fps;
        private static _instance;
        static getInstance(): StatusUtil;
        /**初始化*/
        init(p: egret.DisplayObjectContainer): void;
        protected getText(): GYText;
        /**显示*/
        show(): void;
        /**隐藏*/
        hide(): void;
        protected loop(e: egret.Event): void;
    }
}
