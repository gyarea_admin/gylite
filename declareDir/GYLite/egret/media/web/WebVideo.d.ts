declare namespace egret.web {
    /**
     * @private
     * @inheritDoc
     */
    class WebVideo extends egret.DisplayObject implements egret.Video {
        /**
         * @inheritDoc
         */
        src: string;
        /**
         * @inheritDoc
         */
        poster: string;
        /**
         * @private
         */
        private posterData;
        /**
         * @private
         */
        private video;
        /**
         * @private
         */
        private loaded;
        /**
         * @private
         */
        private closed;
        /**
         * @private
         */
        private heightSet;
        /**
         * @private
         */
        private widthSet;
        /**
         * @private
         * pc上视频卡住的时候不能暂停
         */
        private waiting;
        /**
         * @private
         * 用户是否设置了 pause
         */
        private userPause;
        /**
         * @private
         * 用户是否设置了 play
         */
        private userPlay;
        /**
         * @inheritDoc
         */
        constructor(url?: string, cache?: boolean);
        protected createNativeDisplayObject(): void;
        /**
         * @inheritDoc
         */
        load(url?: string, cache?: boolean): void;
        private isPlayed;
        /**
         * @inheritDoc
         */
        play(startTime?: number, loop?: boolean): void;
        private videoPlay;
        private checkFullScreen;
        private goFullscreen;
        private setFullScreenMonitor;
        private screenError;
        private screenChanged;
        private exitFullscreen;
        /**
         * @private
         *
         */
        private onVideoEnded;
        /**
         * @private
         *
         */
        private onVideoError;
        /**
         * @inheritDoc
         */
        close(): void;
        /**
         * @inheritDoc
         */
        pause(): void;
        /**
         * @inheritDoc
         */
        get volume(): number;
        /**
         * @inheritDoc
         */
        set volume(value: number);
        /**
         * @inheritDoc
         */
        get position(): number;
        /**
         * @inheritDoc
         */
        set position(value: number);
        private _fullscreen;
        /**
         * @inheritDoc
         */
        get fullscreen(): boolean;
        /**
         * @inheritDoc
         */
        set fullscreen(value: boolean);
        private _bitmapData;
        /**
         * @inheritDoc
         */
        get bitmapData(): BitmapData;
        private loadPoster;
        /**
         * @private
         *
         */
        private onVideoLoaded;
        /**
         * @private
         */
        $measureContentBounds(bounds: Rectangle): void;
        private getPlayWidth;
        private getPlayHeight;
        /**
         * @private
         */
        $updateRenderNode(): void;
        private markDirty;
        /**
         * @private
         * 设置显示高度
         */
        $setHeight(value: number): void;
        /**
         * @private
         * 设置显示宽度
         */
        $setWidth(value: number): void;
        get paused(): boolean;
        /**
         * @inheritDoc
         */
        get length(): number;
    }
}
