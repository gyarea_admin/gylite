declare namespace egret.web {
    /**
     * @private
     * WebGLRenderTarget
     * A WebGL render target with a frame buffer and texture
     */
    class WebGLRenderTarget extends HashObject {
        private gl;
        texture: WebGLTexture;
        private frameBuffer;
        private stencilBuffer;
        width: number;
        height: number;
        clearColor: number[];
        /**
         * If frame buffer is enabled, the default is true
         */
        useFrameBuffer: boolean;
        constructor(gl: WebGLRenderingContext, width: number, height: number);
        private _resize;
        resize(width: number, height: number): void;
        activate(): void;
        private getFrameBuffer;
        initFrameBuffer(): void;
        private createTexture;
        clear(bind?: boolean): void;
        enabledStencil(): void;
        dispose(): void;
    }
}
