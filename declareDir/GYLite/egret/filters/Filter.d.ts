declare namespace egret {
    /**
     * @private
     * @version Egret 2.4
     * @platform Web,Native
     */
    class Filter extends HashObject {
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        type: string;
        /**
         * @private
         */
        $id: number;
        /**
         * @private
         */
        $uniforms: any;
        /**
         * @private
         */
        protected paddingTop: number;
        /**
         * @private
         */
        protected paddingBottom: number;
        /**
         * @private
         */
        protected paddingLeft: number;
        /**
         * @private
         */
        protected paddingRight: number;
        /**
         * @private
         * @native Render
         */
        $obj: any;
        constructor();
        /**
         * @private
         */
        $toJson(): string;
        protected updatePadding(): void;
        onPropertyChange(): void;
    }
}
