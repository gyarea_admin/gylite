module GYLite
{
	export interface IPoolObject extends IResource
	{
		/**对象清理，回收池中，必须设置inPool为false，以标记回收*/clear():void;		
		/**是否在池中,在s.clear设置为false已标记回收*/inPool:boolean;
		/**对象唯一id*/poolId?:number;
		outPoolInit():void;		
	}
}