/**
 @author 迷途小羔羊
 2015.3.5
 */
module GYLite
{
												
			
	export class GYListV extends GYListBase
	{		
		/**垂直List组件
		 @inheritDoc*/
		public constructor(size:number,getGridFunc:Function=null, getThisObject:any=null, setGridFunc:Function=null, setThisObject:any=null)
		{
			super(size,getGridFunc,getThisObject,setGridFunc,setThisObject);
		}
		
		protected initComponent():void
		{
			let s = this;
			super.initComponent();			
			var grid:IItemRender;
			if(s._boxs != 0)
			{
				grid=s.getGrid();				
				s._boxH = grid.height;
				s._boxW = grid.width;
				grid.dispose();
				if(s._boxs > 0)
				{					
					s.height = s._boxs;
				}
				else
				{
					s.boxs = -s._boxs;
				}
			}
		}
		public set boxs(val:number)
		{var s=this;
			if(val < 0)return;
			if(val == s._boxs)return;
			s._boxs = s._virtual_layout?val:(s._dataProvider?s._dataProvider.length:0);
			s.invalidBoxNum();			
		}
		public get boxs():number
		{
			let s = this;
			return s._boxs;
		}
		protected boxNumChange(update:boolean=false):void
		{var s=this;
			var i:number,len:number;
			var grid:IItemRender;
			var val:number;
			var h:number;
			var box:number;
			var offset:number;
			val = s.vScroller.position;
			h=s._boxH + s._padding;			
			offset = s._virtual_layout?val % h:0;
			len = s._grids.length;
			for(i = update?0:len;i<s._boxs;++i)
			{
				if(i >= len)
				{
					grid=s.getGrid();
					s._grids.push(grid);
					s.addElementAt(grid,0);
				}
				else
					grid = s._grids[i];
				if(s._dataProvider)
				{
					var ind:number = s._lastBox + i;
					grid.itemIndex = ind;
					s.setGrid(grid,s._dataProvider[ind]);
					grid.y = h * i - offset;
					grid.selected = s._selectedData!=null && grid.getData() == s._selectedData;
				}
				else
					s.setGrid(grid,null);
			}
			if(s._boxs < len)
			{
				for(i = s._boxs;i<len;++i)
				{
					grid = s._grids[i];
					if(grid.parent)
						s.removeElement(grid);
				}
				s._grids.length = s._boxs;
			}
			
		}		
		/**@inheritDoc*/
		public scrollToIndex(val:number):void
		{
			let s = this;
			s.vScroller.position = val * s._boxH;
		}
		/**@inheritDoc*/
		public scrollToEndIndex(val:number):void
		{
			let s = this;
			s.vScroller.position = val * s._boxH - s._innerHeight + s._boxH;
		}
		protected selectLine(data:any,selectedData:any,ctrlKey:boolean,checkNext:boolean=false):void
		{
			let s = this;
			var ind:number=0,stInd:number=0,endInd:number=0;
			var tempData:any;
			var i:number=0,len:number=0;
			var tempVec:any[];
			var tempItem:IItemRender;
			var end:number=0;
			var flag:number=0,mY:number = s.mouseY;
			if(checkNext)
			{
				tempItem = s._dataToItemDict.getValue(data);
				if(tempItem == null)
				{
					if(mY > s.height)
						flag = 1;
					else if(mY < 0)
						flag = -1;
				}
				else
				{
					if(mY > tempItem.y + tempItem.height)
						flag = 1;
					else if(mY < tempItem.y)
						flag = -1;	
				}
				if(flag == 0)
					return;
			}
			stInd = s._dataProvider.indexOf(selectedData);
			endInd = s._dataProvider.indexOf(data);
			if(flag != 0)
			{
				if(flag == 1)
				{
					endInd = endInd < s._dataProvider.length - 1? endInd + 1: s._dataProvider.length - 1;
					data = s._dataProvider[endInd];
				}
				else if(flag == -1)
				{
					endInd = endInd > 0? endInd - 1: endInd;
					data = s._dataProvider[endInd];
				}
				s.scrollToEndIndex(endInd);
			}
			s._nextData = data;
			if(endInd < stInd)
			{
				ind = endInd;
				endInd = stInd;
				stInd = ind;
			}
			len = endInd + 1;
			end = s._selectList.length-1;
			for(i=stInd;i<len;++i)
			{
				tempData = s._dataProvider[i];
				ind = s._selectList.indexOf(tempData);
				if(ind == -1)
				{
					if(s._selectList.length >= s._canSelectNum)
						continue;
					if(tempData)
					{
						tempItem = s._dataToItemDict.getValue(tempData);
						s._selectList.push(tempData);
						if(tempItem)
							tempItem.selected = true;
					}
				}
				else if(!ctrlKey)
				{
					tempData = s._selectList[end];
					s._selectList[end] = s._selectList[ind];
					s._selectList[ind]= tempData;
					--end;
				}
			}
			if(!ctrlKey && end > -1)
			{
				tempVec = s._selectList.splice(0,end + 1);
				len = tempVec.length;
				while(--len>-1)
				{
					tempItem = s._dataToItemDict.getValue(tempVec[len]);
					if(tempItem)
					{
						tempItem.selected = false;
					}
				}
			}
			if(s.hasEventListener(GYViewEvent.SELECTED) && s._mouseSelect)
			{
				s.dispatchEvent(new GYViewEvent(GYViewEvent.SELECTED));
				s._mouseSelect = false;
			}
		}
		public updateGrid():void
		{
			let s = this;
			var i:number,j:number;
			var grid:IItemRender;
			var h:number;
			h = s._boxH + s._padding;
			s._max = s._dataProvider?Math.max(s._dataProvider.length * h - s._padding,s.baseHeight):s.baseHeight;//减去末尾padding
			s.vScroller.maximum = s._max - s.baseHeight + s._scrollPadTop + s._scrollPadBottom;
			s.vScroller.barPercent = s.baseHeight/s._max;
			for(i=0;i<s._boxs;++i)
			{
				grid=s._grids[i];
				grid.height = s._boxH;
				grid.width = s._boxW;
			}
			if(s.vScroller.value > s.vScroller.maximum)
				s.vScroller.value = s.vScroller.maximum;
			s.vScrollChange();
		}
		/**@inheritDoc*/
		public get dataProvider():Array<any>
		{
			let s = this;
			return s._dataProvider;
		}
		public set dataProvider(val:Array<any>)
		{
			let s = this;
			var h:number=s._boxH + s._padding;
			var offset:number;
			if(!s._keepSelected)
			{
				s._selectedData = null;
				s._selectIndex = -1;
				if(s._canSelectNum > 1)
					s._selectList.length = 0;
			}
			s._dataProvider=(val?val:[]);
			if(!s._virtual_layout)
			{
				s._boxs = s._dataProvider.length;
				if(!s._boxNumUpdate)
					s.boxNumChange(true);
			}
			if(s._boxNumUpdate)
				s._boxNumUpdateReset = true;
			s._max = Math.max(s._dataProvider.length * h - s._padding,s.baseHeight);
			s.vScroller.maximum=s._max-s.baseHeight + s._scrollPadTop + s._scrollPadBottom;
			s.vScroller.barPercent=s.baseHeight/s._max;
			if(s.vScroller.position > s.vScroller.maximum)
				s.vScroller.position = s.vScroller.maximum;
			s.scrollerPolicy = s._scrollerPolicy;
			s.vScrollChange(null,true);
		}
		protected vScrollChange(e:GYScrollerEvent=null, update:boolean=false,updatePos:boolean=true)
		{
			let s = this;
			if(s._dataProvider==null)
				return;
			if(s._boxNumUpdate)
				return;
			var val:number;
//			var w:number=s._boxW;
			var h:number;
			var box:number;
			var offset:number;
			var i:number;
			var grid:IItemRender;
			var nowBox:number;
			val = s.vScroller.position;
			if(!s._virtual_layout)
			{
				s.clipY = val;
				return;
			}
			h=s._boxH + s._padding;
			offset=val % h;
			nowBox=(val / h >> 0);			
			box = 0;
			if(!update)
			{
				box=s._lastBox-nowBox;
				if(box < 0)
				{
					if(-box<s._boxs)
					{
						for(i=0;i<-box;++i)
						{
							grid=s._grids.shift();
							s._grids.push(grid);
						}
					}
				}
				else if(box > 0)
				{
					if(box<s._boxs)
					{
						for(i=0;i<box;++i)
						{
							grid=s._grids.pop();
							s._grids.unshift(grid);
						}
					}
				}
			}
			var tempLine:number= s._boxs + box - 1;
			for(i=0;i<s._boxs;++i)
			{
				grid = s._grids[i];
				var ind:number = nowBox + i;
				grid.itemIndex = ind;
				if(box > 0 && i < box || box < 0 && i > tempLine || update)
					s.setGrid(grid,s._dataProvider[ind]);
				if(updatePos)
					grid.y = h * i - offset;
			}
			s._lastBox = nowBox;
		}
		/**刷新列表可见的所有项*/		
		public updateItems():void
		{
			let s = this;
			s.vScrollChange(null,true, false);
		}
		protected wheelRoll(e):void
		{
			let s = this;
			var val:number;
			if(!s._wheelScroll)return;			
			if(s._isDragging && s.dragForbiddenWheel)return;
			val = s._wheelStep == 0?s._boxH:s._wheelStep;
			s.vScroller.setPosition(s.vScroller.position+(e.deltaY > 0?val:-val),true);
		}
		public set height(val:number)
		{
			let s = this;			
			if(s.$height == val)
				return;
			s._innerHeight = s.vScroller.height = s.group_height = val;
			var h:number = s.baseHeight;			
			s._max = s._dataProvider?Math.max(s._dataProvider.length * (s._boxH + s._padding) - s._padding,h):h;//减去末尾padding
			s.vScroller.maximum = s._max - h + s._scrollPadTop + s._scrollPadBottom;
			s.vScroller.barPercent = h/s._max;
			s.scrollerPolicy = s._scrollerPolicy;
			s.boxs = (s._boxH + s._padding == 0)?1:(Math.ceil(s._innerHeight / (s._boxH + s._padding)) + 1);
		}
		public get height():number
		{
			let s = this;
			return egret.superGetter(GYListV, this, "height");
		}
		public set width(val:number)
		{
			let s = this;			
			if(s.$width == val)
				return;
			s._innerWidth = val;
			s.group_width = val + (s.vScroller.parent?s.vScroller.width:0);
			// s.boxW = s._innerWidth; //item宽度不由列表宽度决定
		}
		public get width():number
		{
			let s = this;			
			return egret.superGetter(GYListV, this, "width");
		}
		/**滚动条 0自动 1显示 2不显示*/
		public set scrollerPolicy(val:number)
		{
			let s = this;
			s._scrollerPolicy=val;
			var show:boolean;
			if(s._scrollerPolicy == 0)
			{
				if(s.vScroller.barPercent < 1)
					show = true;
				else
					show = false;
			}
			else if(s._scrollerPolicy == 1)
				show = true;
			else
				show = false;
			if(show)
			{
				if(s.vScroller.parent == null)
				{
					s.baseAdd(s.vScroller);					
					s.group_width = s._innerWidth + s.vScroller.width;
				}
			}
			else
			{
				if(s.vScroller.parent)
				{
					s.baseRemove(s.vScroller);
					s.group_width = s._innerWidth;
				}
			}
		}
		public get scrollerPolicy():number
		{
			let s = this;
			return s._scrollerPolicy;
		}
		protected downGroup(e:egret.TouchEvent)
		{
			let s = this;
			if(s.mouseX >= s.vScroller.x && s.mouseX <= s.vScroller.x + s.vScroller.width)
				return;
			s._touchId = e.touchPointID;
			if(GYSprite.pushGlobalDrag(this))
				s.groupDrag();
			else
				s.addEventListener(GYEvent.GLOABL_DRAG, s.globalDragCall,s);
				
		}
		protected dragLoop(t:number):void
		{
			let s = this;
			var mY:number;
			let nt:number;
			let frameParam:number;
			let yFlag:boolean;
			let temp:number;
			nt = Date.now();
			frameParam = 1000/egret.ticker.$frameRate|0;			
			yFlag = s._moveYTime < 100;			
			s._moveYTime = yFlag?s._moveYTime + Math.min(frameParam,nt - s._lastYDragTime):Math.min(frameParam,nt - s._lastYDragTime);
			mY = s.mouseY - s._clipRect.y;
			s._lastYDragTime = s._lastXDragTime = nt;			
			if(s.tempY < mY && !s._dragPreEnabled)return;
			if(s.tempY > mY && !s._dragNextEnabled)return;
			temp = s.tempY - mY;
			if(s._dragLock || Math.abs(temp) > s.dragValue)
			{
				s._isDragging = true;
				s.dragLock = true;
				s.easeSpeedY = yFlag?s.easeSpeedY + temp:temp;				
				s.vScroller.position += temp;
				s.tempY = mY;
				if(s.vScroller.position == 0 && s.easeSpeedY < 0 || s.vScroller.position == s.vScroller.maximum && s.easeSpeedY > 0)
				{
					if(GYSprite.shiftGlobalDrag(1))
						s.clearGlobalDrag();
				}
			}
			else if(GYLite.GYSprite.hasGlobalHDrag())
			{
				let dragMoveX:boolean;
				var mX:number = s.mouseX - s._clipRect.x;
				dragMoveX = Math.abs(s.tempX - mX) > s.dragValue;
				s.tempX = mX;
                if(dragMoveX)
					++GYLite.GYSprite.globalHDrag;
                if(GYLite.GYSprite.globalHDrag > GYLite.GYSprite.globalHDragCount)
                {
                    if (GYLite.GYSprite.shiftGlobalDrag(1))
                        s.clearGlobalDrag();
                }
			}
		}
		protected groupDrag():void
		{
			let s = this;
			if(s.vScroller.tween)
				s.dragLock = true;
			s.vScroller.stopScroll();
			// CommonUtil.addStageLoop(s.dragLoop,s);
			GYSprite.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE,s.touchMove,s);
			s._lastYDragTime = s._lastXDragTime = Date.now();
			s.tempY = s.mouseY - s._clipRect.y;
			s._moveYTime = s.easeSpeedY = 0;
			s._scrollToPos = NaN;
			GYSprite.addStageDown(s,s.upGroup, s);
			s.addEventListener(egret.TouchEvent.TOUCH_END, s.upGroup, s);
		}		
		protected groupDragStop():void
		{
			let s = this;
			if(s.tempY != s.tempY)return;
			s._isDragging = false;
			// CommonUtil.delStageLoop(s.dragLoop,s);
			GYSprite.stage.removeEventListener(egret.TouchEvent.TOUCH_MOVE,s.touchMove,s);
			s.removeEventListener(egret.TouchEvent.TOUCH_END, s.upGroup, s);			
			let dis:number,minDis:number;
			let temp:number,tarPos:number,addDis:number;
			let frameParam:number;
			frameParam = 1000/egret.ticker.$frameRate;			
			let nt:number = Date.now();
			let dt:number = s._moveYTime;
			let spd:number;
			if(dt > 0 && nt - s._lastYDragTime < s._easeTriggerTime && (s.easeSpeedY > s.easeValue || s.easeSpeedY < -s.easeValue))
			{
				spd = s.easeSpeedY / dt * frameParam * s.speedParam;
				dis = (s.vScroller.limitMax == s.vScroller.limitMax && s.vScroller.limitMax < s.vScroller.maximum)?s.vScroller.limitMax:s.vScroller.maximum;
				minDis = (s.vScroller.limitMin == s.vScroller.limitMin && s.vScroller.limitMin > 0)?s.vScroller.limitMin:0;
				addDis = Math.abs(spd) * spd * s._easeSpeedValue;
				temp = s.vScroller.position + addDis;				
				if(temp < minDis)
				{
					tarPos = minDis;
					addDis = tarPos - s.vScroller.position;
				}						
				else if(temp > dis)
				{
					tarPos = dis;
					addDis = tarPos - s.vScroller.position;
				}
				else
				{
					tarPos = temp;					
					// addMaxDis = addDis;
				}				
				s.vScroller.scrollToPosition(tarPos, (temp < minDis || temp > dis)?s._easeTime: Math.abs(addDis / spd * frameParam * 2));				
			}
			else if(s._scrollPosLimit == s._scrollPosLimit)
			{
				s.scrollEnd();
			}
			s.tempY = NaN;
			s.dragLock = false;
		}
		
		public scrollToNextItem():number
		{let s =this;			
			let pos:number;
			let pad:number;
			let nextPos:number;
			if(!s._overlying)
			{
				if(s.vScroller.tween)return;				
			}
			nextPos = s._scrollToPos == s._scrollToPos?s._scrollToPos:s.vScroller.position;
			if(nextPos > s.vScroller.maximum)return;
			pad = s._scrollPosLimit == s._scrollPosLimit?s._scrollPosLimit:(s._boxH + s._padding);
			pos = ((nextPos / pad >> 0) + 1) * pad;
			pos = pos > s.vScroller.limitMax?s.vScroller.limitMax:(pos > s.vScroller.maximum?s.vScroller.maximum:pos);
			s.vScroller.scrollToPosition(pos, s.scrollNextTime == s.scrollNextTime?s.scrollNextTime:pad);
			s._scrollToPos = pos;
			return pos;
		}		
		public scrollToPreItem():number
		{let s =this;
			let pos:number;
			let pad:number;
			let nextPos:number;
			if(!s._overlying)
			{
				if(s.vScroller.tween)return;
			}
			nextPos = s._scrollToPos == s._scrollToPos?s._scrollToPos:s.vScroller.position;
            if(nextPos < 0)return;
			pad = s._scrollPosLimit == s._scrollPosLimit?s._scrollPosLimit:(s._boxH + s._padding);
			if(nextPos % pad == 0)
				pos = ((nextPos / pad >> 0) - 1) * pad;
			else	
				pos = ((nextPos / pad >> 0) * pad);
			pos = pos < s.vScroller.limitMin?s.vScroller.limitMin:(pos < 0?0:pos);
			s.vScroller.scrollToPosition(pos, s.scrollNextTime == s.scrollNextTime?s.scrollNextTime:pad);
			s._scrollToPos = pos;
			return pos;
		}
		protected scrollEnd(e:GYScrollerEvent=null):void
		{let s =this;
			if(s._scrollPosLimit != s._scrollPosLimit)return;
			if(s._scrollToPos != s._scrollToPos || s._scrollToPos == s._scrollToPos && Math.abs(s._scrollToPos - s.vScroller.position) > 1)
			{
				s._scrollToPos = NaN;
				if(s.easeSpeedY > 0)
					s.scrollToNextItem();
				else if(s.easeSpeedY < 0)
					s.scrollToPreItem();
				else
				{
					let offset:number;
					offset = s.vScroller.position % s.scrollPosLimit;
					if(offset > 0)
					{
						if(offset > s.scrollPosLimit / 2)
						{
							s.scrollToNextItem();
						}
						else
						{
							s.scrollToPreItem();
						}
					}				
				}
				s.easeSpeedY = 0;
			}
			else
				s._scrollToPos = NaN;
		}
		
		public clone():GYListBase
		{
			let s = this;
			let list:GYListV;
			list = new GYListV(s._boxs, s.createGrid, s, s.setGrid, s);
			list.copy(s);
			return list;
		}
		public copy(list:GYListV):void
		{
			let s = this;
			if(list == null)return;
			s.width = list.width;
			s.height = list.height;
			s.dataProvider = list.dataProvider;
			s.canSelectNum = list.canSelectNum;
			s.scrollerPolicy = list.scrollerPolicy;
			s.keepSelected = list.keepSelected;
			s.dragSelect = list.dragSelect;
			s.virtual_layout = list.virtual_layout;
			s.overlying = list.overlying;
			s.wheelStep = list.wheelStep;
			s.wheelScroll = list.wheelScroll;
			s.selectInterval = list.selectInterval;
			
			s.vScroller.x = list.vScroller.x;
			s.vScroller.y = list.vScroller.y;
			s.vScroller.height = list.vScroller.height;			
			s.vScroller.value = list.vScroller.value;
			s.selectedData = list.selectedData;
		}
	}
}