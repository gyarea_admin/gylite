declare module GYLite {
    class CommonUtil {
        static loopTime: number;
        protected static _stage: egret.Stage;
        private static _loopFuncVec;
        private static _loopFuncObj;
        static frameRate: number;
        static frameDelta: number;
        /**标准帧率下的速度系数（以30帧为标准，如果提高到60，则是0.5）*/
        static rateParam: number;
        static Init(stage: egret.Stage): void;
        /**
         * 添加val到舞台帧频执行列表中，ENTERFRAME事件触发时执行
         * @param Function类型，无参数val();
         * */
        static addStageLoop(val: Function, thisObject: any): void;
        /**
         * 从舞台帧频执行列表中删除val函数
         * @param val 帧频函数
         * */
        static delStageLoop(val: Function, thisObject: any): void;
        private static stageLoop;
        /**帧频监听列表*/
        static get loopFuncVec(): Function[];
        static get loopFuncObj(): any[];
        /**类型判断，cls参数可以使用字符串(接口只能用字符串参数，因为在h5中只能使用字符串记录接口)，继承链15层以内有效
         * @param obj 对象
         * @param cls 类型
        */
        static GYIs(obj: any, cls: any): boolean;
        /**判断对象是否属于某种类型(包括接口，此方法是通过__type__属性查询，请务必保证ts编译代码附加了__type__继承链)
         * @param obj 对象
         * @param className 完全类名
        */
        static typeIs(obj: any, className: string): boolean;
        /**比较两个类是否同一个继承链上*/
        static classIs(cls: any, superCls: any): boolean;
        /**垃圾回收已经被销毁的对象的loop*/
        static gc(): void;
    }
}
