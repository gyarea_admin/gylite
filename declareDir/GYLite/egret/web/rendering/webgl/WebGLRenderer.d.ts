declare namespace egret.web {
    /**
     * @private
     * WebGL渲染器
     */
    class WebGLRenderer implements sys.SystemRenderer {
        constructor();
        /**
         * Do special treatment on wechat ios10
         */
        wxiOS10: boolean;
        private nestLevel;
        /**
         * 渲染一个显示对象
         * @param displayObject 要渲染的显示对象
         * @param buffer 渲染缓冲
         * @param matrix 要对显示对象整体叠加的变换矩阵
         * @param dirtyList 脏矩形列表
         * @param forRenderTexture 绘制目标是RenderTexture的标志
         * @returns drawCall触发绘制的次数
         */
        render(displayObject: DisplayObject, buffer: sys.RenderBuffer, matrix: Matrix, forRenderTexture?: boolean): number;
        /**
         * @private
         * 绘制一个显示对象
         */
        private drawDisplayObject;
        /**
         * @private
         */
        private drawWithFilter;
        private getRenderCount;
        /**
         * @private
         */
        private drawWithClip;
        /**
         * @private
         */
        private drawWithScrollRect;
        /**
         * 将一个RenderNode对象绘制到渲染缓冲
         * @param node 要绘制的节点
         * @param buffer 渲染缓冲
         * @param matrix 要叠加的矩阵
         * @param forHitTest 绘制结果是用于碰撞检测。若为true，当渲染GraphicsNode时，会忽略透明度样式设置，全都绘制为不透明的。
         */
        drawNodeToBuffer(node: sys.RenderNode, buffer: WebGLRenderBuffer, matrix: Matrix, forHitTest?: boolean): void;
        /**
         * 将一个DisplayObject绘制到渲染缓冲，用于RenderTexture绘制
         * @param displayObject 要绘制的显示对象
         * @param buffer 渲染缓冲
         * @param matrix 要叠加的矩阵
         */
        drawDisplayToBuffer(displayObject: DisplayObject, buffer: WebGLRenderBuffer, matrix: Matrix): number;
        /**
         * @private
         */
        private renderNode;
        /**
         * @private
         */
        private renderNormalBitmap;
        /**
         * @private
         */
        private renderBitmap;
        /**
         * @private
         */
        private renderMesh;
        private canvasRenderer;
        private canvasRenderBuffer;
        /**
         * @private
         */
        private ___renderText____;
        /**
         * @private
         */
        private renderText;
        /**
         * @private
         */
        private renderGraphics;
        private renderGroup;
        /**
         * @private
         */
        private createRenderBuffer;
        renderClear(): void;
    }
}
