declare module GYLite {
    class MathConst {
        static DOUBLE_PI: number;
        static TRIBLE_PI: number;
        static HALF_PI: number;
        static TRIBLE_HALF_PI: number;
        static QUATER_PI: number;
        static QUATER_TRIBLE_PI: number;
        static ANGLE_ROTATION: number;
        static ROTATION_ANGLE: number;
    }
}
