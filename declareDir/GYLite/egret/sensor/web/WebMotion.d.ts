declare namespace egret.web {
    /**
     * @private
     */
    class WebMotion extends EventDispatcher implements Motion {
        /**
         * @private
         *
         */
        start(): void;
        /**
         * @private
         *
         */
        stop(): void;
        /**
         * @private
         */
        protected onChange: (e: DeviceMotionEvent) => void;
    }
}
