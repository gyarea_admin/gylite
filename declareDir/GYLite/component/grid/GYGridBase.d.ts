declare module GYLite {
    class GYGridBase extends GYGroup implements IList {
        protected _vScroller: GYScrollBarV;
        protected _hScroller: GYScrollBarH;
        protected _innerHeight: number;
        protected _innerWidth: number;
        protected _cols: number;
        protected _rows: number;
        protected _paddingR: number;
        protected _paddingB: number;
        protected _dataProvider: Array<any>;
        protected _grids: Array<any>;
        protected _boxW: number;
        protected _boxH: number;
        protected createGrid: Function;
        protected setGridData: Function;
        protected _lastLine: number;
        protected _lastEndLine: number;
        protected _max: number;
        protected _hScrollerPolicy: number;
        protected _vScrollerPolicy: number;
        protected _gridUpdate: boolean;
        protected _dataUpdate: boolean;
        protected _boxNumUpdate: boolean;
        protected _boxNumUpdateReset: boolean;
        protected _canSelectNum: number;
        protected _selectList: any[];
        protected _selectedData: any;
        protected _selectIndex: number;
        protected _keepSelected: boolean;
        protected _dataToItemDict: Dictionary;
        protected _nextData: any;
        protected _selectMode: number;
        protected _wheelScroll: boolean;
        protected _wheelStep: number;
        protected _stopSelTarget: any;
        private _selectTime;
        private _selectInterval;
        protected _dragSelect: boolean;
        protected _mouseSelect: boolean;
        protected _addSelect: boolean;
        protected _getThisObject: any;
        protected _setThisObject: any;
        protected _virtual_layout: boolean;
        protected _scrollPosLimit: number;
        protected _scrollToPos: number;
        protected _overlying: boolean;
        /**列表是否可选(默认true)*/ selectable: boolean;
        /**速度系数，乘以滑动的速度*/ speedParam: number;
        /**区间限制的时候，恢复到区间位置的滚动花费时间*/ scrollNextTime: number;
        /**Grid组件基类，请继承自定义其逻辑，或者使用GYGrid GYGridV GYGridH
         * @param sizeW Grid宽度 设定后无法更，当为负数的时候则表示为列数cols，此时则需要另外设置Grid宽和项宽（s.boxW）
         * @param sizeH Grid高度 设定后无法更，当为负数的时候则表示为行数rows，此时则需要另外设置Grid高和项高（s.boxH）
         * @param getGridFunc():IItemRender 返回自定义格子对象的方法
         * @param setGridFunc(IItemRender, any) 设置格子数据的方法*/
        constructor(sizeW: number, sizeH: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        protected initComponent(): void;
        protected getGrid(): IItemRender;
        protected setGrid(grid: IItemRender, obj: any): void;
        protected selectCell(e: egret.TouchEvent): void;
        protected selectLine(data: any, selectedData: any, ctrlKey: boolean, checkNext?: boolean): void;
        /**跳转至某索引项*/
        scrollToIndex(val: number): void;
        /**跳转到row行col列(位置在末尾)*/
        scrollToEndIndex(val: number): void;
        protected selectIn(e: egret.TouchEvent): void;
        protected selectOut(e: egret.TouchEvent): void;
        private selectLoop;
        dataIsSelected(d: any): boolean;
        protected releaseOutSide(e: egret.TouchEvent): void;
        set boxW(val: number);
        set boxH(val: number);
        set paddingR(val: number);
        set paddingB(val: number);
        /**格子宽度*/
        get boxW(): number;
        /**格子高度*/
        get boxH(): number;
        /**格子右边间隙*/
        get paddingR(): number;
        /**格子底边间隙*/
        get paddingB(): number;
        set dataProvider(val: Array<any>);
        /**数据源*/
        get dataProvider(): Array<any>;
        set rows(val: number);
        get rows(): number;
        set cols(val: number);
        get cols(): number;
        set virtual_layout(val: boolean);
        get virtual_layout(): boolean;
        set canSelectNum(val: number);
        /**允许选择多少项(默认0，不能选择)*/
        get canSelectNum(): number;
        /**选中项的数据列表(当s.canSelectNum大于1时，将会提供此选择列表)*/
        get selectList(): any[];
        set selectList(val: any[]);
        /**选择的数据索引*/
        get selectedIndex(): number;
        set selectedIndex(val: number);
        set selectedItem(val: IItemRender);
        /**当前选择的ItemRender*/
        get selectedItem(): IItemRender;
        set selectedData(d: any);
        /**当前选择的数据，选择的数据不一定与选择的项相对应，由于内部使用的是渲染项循环滚动以节省资源，所以滚动之后ItemRender的数据不一定就是选择的数据*/
        get selectedData(): any;
        /**数据失效*/
        invalidData(): void;
        /**格子数量失效*/
        invalidBoxNum(): void;
        /**网格失效*/
        invalidGrids(): void;
        /**刷新格子布局*/
        updateGrid(): void;
        /**刷新数据*/
        updateData(): void;
        /**移除索引位置上的数据项（刷新）*/
        removeItemAt(ind: number): void;
        /**移除数据项（刷新）*/
        removeItem(d: any): void;
        /**添加索引位置数据项（刷新）*/
        addItemAt(d: any, ind: number): void;
        /**添加数据项（刷新）*/
        addItem(d: any): void;
        /**刷新数据项*/
        updateItem(d: any): void;
        /**刷新格子数*/
        protected boxNumChange(update?: boolean): void;
        /**刷新列表可见的所有项*/
        updateItems(): void;
        /**@inheritDoc*/
        updateView(): void;
        /**拖选的选择模式，0为行列矩阵的拖选，1逐行或者逐列的拖选，可重写s.selectLine方法自定义拖选规则*/
        get selectMode(): number;
        set selectMode(val: number);
        /**拖选时的滚动时间间隔(毫秒)*/
        get selectInterval(): number;
        set selectInterval(val: number);
        /**是否启用拖选功能*/
        get dragSelect(): boolean;
        set dragSelect(value: boolean);
        get stopSelTarget(): any;
        set stopSelTarget(value: any);
        /**是否保留选择的数据项*/
        get keepSelected(): boolean;
        set keepSelected(value: boolean);
        get innerWidth(): number;
        get innerHeight(): number;
        /**连选时末尾选中的数据*/
        get nextData(): any;
        get wheelStep(): number;
        set wheelStep(value: number);
        /**是否启用滚轮*/
        get wheelScroll(): boolean;
        set wheelScroll(value: boolean);
        protected wheelRoll(e: egret.TouchEvent): void;
        get dataToItemDict(): Dictionary;
        /**滚动区间限制,限制滚动停止的位置以相等的距离*/
        set scrollPosLimit(val: number);
        get scrollPosLimit(): number;
        /**滚动条 0自动 1显示 2不显示*/
        set vScrollerPolicy(val: number);
        get vScrollerPolicy(): number;
        /**滚动条 0自动 1显示 2不显示*/
        set hScrollerPolicy(val: number);
        get hScrollerPolicy(): number;
        /**滚动到下一项
         * @return 返回滚动的目标位置
        */
        scrollToNextItem(): number;
        /**滚动到上一项
         * @return 返回滚动的目标位置
        */
        scrollToPreItem(): number;
        protected scrollEnd(e?: GYScrollerEvent): void;
        protected vScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        protected hScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        /**scrollToNextItem、scrollToPreItem是否叠加滚动，默认true*/
        get overlying(): boolean;
        set overlying(val: boolean);
        get hScroller(): GYScrollBarH;
        set hScroller(val: GYScrollBarH);
        get vScroller(): GYScrollBarV;
        set vScroller(val: GYScrollBarV);
        /**拖选响应毫秒(默认值)*/ static default_selectInterval: number;
        /**滚轮步长(默认值)*/ static default_wheelStep: number;
        /**是否允许滚轮(默认值)*/ static default_wheelScroll: boolean;
    }
}
