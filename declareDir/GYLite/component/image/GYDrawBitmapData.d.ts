declare module GYLite {
    /**GYDrawBitmapData是继承BitmapData的位图数据，使用对象池管理的临时位图，使用固定宽高的BitmapData来临时使用
* 例如输入230,230，则提供使用256,256的BitmapData，进行裁切使用，所以尽量接近以下尺寸使用率会更高
* 1024,512,256,128,64,32,16,8*/
    class GYDrawBitmapData extends egret.RenderTexture {
        $bitmapDataId: number;
        id: number;
        /**设定的宽度*/ realWidth: number;
        /**设定的高度*/ realHeight: number;
        private _clearTime;
        private _refCount;
        private _width;
        private _height;
        private _transparent;
        private _fillColor;
        constructor(width: number, height: number, transparent?: boolean, fillColor?: number);
        get clearTime(): number;
        /**此对象的引用计数*/
        get refCount(): number;
        /**获取GYDrawBitmapdata引用，持有引用可保证GYDrawBitmapData不被回收，当引用计数为0则会被定期回收
         * 使用此临时位图的引用必须严格遵守一次getRef获得引用，一次s.relRef释放引用，不要重复操作，以免计数错误，当计数为0，仍然执行s.relRef释放引用的话会抛出异常，此时应检查引用获取与释放是不是严格一对一
         * */
        getRef(): GYDrawBitmapData;
        /**释放引用，引用数为0时，执行此函数会抛出异常*/
        relRef(): void;
        draw(source: any, clipRect?: egret.Rectangle, smoothing?: Boolean): void;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        get format(): any;
        set format(val: any);
        private getBitmapData;
        private static _sizeVec;
        private static _bmpVec;
        private static _checkInterval;
        private static _checkTime;
        private static _timeId;
        private static _timeoutMilSec;
        /**内存中GYDrawBitmapData的数量*/ static num: number;
        /**库存位图占用的内存*/ static memory: number;
        /**获取GYDrawBitmapData*/
        static getBitmapData(w: number, h: number): GYDrawBitmapData;
        /**设置位图清理的时间间隔
         * @param val 时间间隔(毫秒) 为0时不回收
         * */
        static setResCheck(val: number): void;
        private static CheckResUse;
    }
}
