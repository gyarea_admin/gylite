declare module GYLite {
    interface ISound {
        bytes: ArrayBuffer;
        mimeType: string;
        path: string;
        compress: string;
        play(startTime?: number, loops?: number): egret.SoundChannel;
        readonly channel: egret.SoundChannel;
        readonly originChannel: egret.SoundChannel;
        setOriginChannel(val: egret.SoundChannel): any;
        stop(): void;
    }
}
