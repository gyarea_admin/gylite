/**
 @author 迷途小羔羊
 2015.3.5
 */
declare module GYLite {
    class GYGridH extends GYGridBase {
        /**横向滚动Grid组件
         * @param cols 列数
         * @param rows 行数
         * @param getGridFunc():IItemRender 返回自定义格子对象的方法
         * @param setGridFunc(IItemRender, any) 设置格子数据的方法*/
        constructor(sizeW: number, sizeH: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        protected initComponent(): void;
        /**inheritDoc*/
        vcrollToIndex(val: number): void;
        /**inheritDoc*/
        scrollToEndIndex(val: number): void;
        protected selectLine(data: any, selectedData: any, ctrlKey: boolean, checkNext?: boolean): void;
        /**@inheritDoc*/
        updateGrid(): void;
        set dataProvider(val: Array<any>);
        get dataProvider(): Array<any>;
        protected hScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        /**刷新列表可见的所有项*/
        updateItems(): void;
        set height(val: number);
        get height(): number;
        set width(val: number);
        get width(): number;
        set rows(val: number);
        get rows(): number;
        set cols(val: number);
        get cols(): number;
        protected boxNumChange(update?: boolean): void;
        protected wheelRoll(e: any): void;
        set hScrollerPolicy(val: number);
        /**滚动条 0自动 1显示 2不显示*/
        get hScrollerPolicy(): number;
        protected downGroup(e: egret.TouchEvent): void;
        protected dragLoop(t: number): void;
        protected groupDrag(): void;
        protected groupDragStop(): void;
        scrollToNextItem(): number;
        scrollToPreItem(): number;
        protected scrollEnd(e?: GYScrollerEvent): void;
    }
}
