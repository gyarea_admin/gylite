module GYLite
{	
	export class GYScrollerEvent extends GYEvent
	{
		/**滚动条改变*/
		public static SCROLL_CHANGE:string="scrollChange";
		/**滚动结束*/
		public static SCROLL_TWEEN_END:string="scrollTweenEnd";
		public constructor(type:string, bubbles?:boolean, cancelable?:boolean)
		{
			super(type, bubbles, cancelable);
		}
	}
}