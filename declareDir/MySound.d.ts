declare class MySound implements GYLite.ISound {
    private _channel;
    private _originChannel;
    private _originSound;
    private _tempChannels;
    private _startTime;
    private _loops;
    private _checkId;
    bytes: ArrayBuffer;
    mimeType: string;
    compress: string;
    path: string;
    constructor();
    play(startTime?: number, loops?: number): egret.SoundChannel;
    /**播放字节流声音
     * @param bytes 字节数组ArrayBuffer
     * @param startTime 开始时间
     * @param loops 循环次数 0 不循环
    */
    private loadBytes;
    private bytesSoundLoaded;
    private endCheck;
    private doCheck;
    clearCheck(): void;
    get channel(): MyChannel;
    get originChannel(): egret.SoundChannel;
    setOriginChannel(val: egret.SoundChannel): void;
    stop(): void;
    close(): void;
    get length(): number;
    get type(): string;
    load(url: string): void;
}
declare class MyChannel extends egret.EventDispatcher implements egret.SoundChannel {
    private _volume;
    private _position;
    private _sound;
    private _listenerObj;
    private _addListenerObj;
    constructor(sound: MySound);
    get volume(): number;
    set volume(val: number);
    get position(): number;
    set position(val: number);
    get settingPosition(): number;
    set listenerObj(val: any);
    get listenerObj(): any;
    get addListenerObj(): any;
    stop(): void;
    clear(): void;
    once(type: string, listener: Function, thisObject: any, useCapture?: boolean, priority?: number): void;
    addEventListener(type: string, listener: Function, thisObject: any, useCapture?: boolean, priority?: number): void;
    hasEventListener(type: string): boolean;
    removeEventListener(type: string, listener: Function, thisObject: any, useCapture?: boolean): void;
}
