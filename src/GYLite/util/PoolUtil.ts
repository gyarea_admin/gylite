module GYLite
{
		
	
	export class PoolUtil
	{
		private static _idCount:number=0;
		private static _poolDict:any;
		private static _poolCfgDict:any={};
		/**设置对象池数量溢出警告的溢出临界值*/
		public static setPoolLengthWarn(cls:any,maxNum:number):void
		{			
			let className:string = cls.prototype.__class__;
			if(className == null)
			{
				egret.error("对象池类型必须有__class__!" + cls);
				return null;
			}
			if(PoolUtil._poolCfgDict[className] == null)PoolUtil._poolCfgDict[className]={};
			PoolUtil._poolCfgDict[className].max = maxNum;
		}
		/**获取对象池数量溢出警告的溢出临界值*/
		public static getPoolLengthWarn(cls:any):number
		{
			let className:string = cls.prototype.__class__;
			if(className == null)
			{
				egret.error("对象池类型必须有__class__!" + cls);
				return null;
			}
			if(PoolUtil._poolCfgDict[className] == null)PoolUtil._poolCfgDict[className]={};
			return PoolUtil._poolCfgDict[className].max?PoolUtil._poolCfgDict[className].max:100;
		}
		/**从对象池取出*/
		public static fromPool(cls:any):IPoolObject
		{
			if(PoolUtil._poolDict == null)PoolUtil._poolDict = {};
			let className:string = cls.prototype.__class__;
			if(className == null)
			{
				egret.error("对象池类型必须有__class__!" + cls);
				return null;
			}			
			if(PoolUtil._poolDict[className] == null)PoolUtil._poolDict[className]=new Array<IPoolObject>();
			var pool:IPoolObject[];
			pool = PoolUtil._poolDict[className];			
			var r:IPoolObject;
			r = pool.length==0?new cls():pool.pop();
			r.poolId = ++PoolUtil._idCount;
			r.inPool = false;
			r.outPoolInit();
			return r;
		}
		/**回收入对象池*/
		public static toPool(poolObject:IPoolObject,cls:any):void
		{
			if(poolObject.inPool)return;
			if(poolObject.disposed)return;
			poolObject.inPool = true;
			let className:string = cls.prototype.__class__;
			if(className == null)
			{
				egret.error("对象池类型必须有__class__!" + cls);
				return null;
			}
			var pool:IPoolObject[],poolCfg:any;
			var max:number;
			pool = PoolUtil._poolDict[className];
			if(pool)
			{
				poolCfg = PoolUtil._poolCfgDict[className];
				max = poolCfg && poolCfg.max?poolCfg.max:100;
				if(pool.length > max)
					console.warn("pool over " + max + "!#" + className + ",length:" + pool.length);
				pool.push(poolObject);
			}			
		}
		/**销毁池子*/
		public static destroyPool(cls:any, includeExtend:boolean=false):void
		{
			let i:number,len:number,len2:number;
			let arr:IPoolObject[];
			let className:string;
			if(cls == null)return;
			if(GYLite.CommonUtil.GYIs(cls, String))
			{
				className = cls;
			}
			else
			{
				className = cls.prototype.__class__;
				if(className == null)
				{
					egret.error("对象池类型必须有__class__!" + cls);
					return null;
				}
			}			
			let delArr:string[] = [];
			let objArr:string[];
			if(includeExtend)
			{
				let clsArr:any[];				
				for(var key in PoolUtil._poolDict)
				{
					let obj:any=window;
					objArr = key.split(".");
					len = objArr.length;
					for(i=0;i<len;++i)
					{						
						obj = obj[objArr[i]];
						if(obj == null)													
							break;													
					}
					if(obj == null)continue;
					clsArr = obj.prototype.__types__;
					if(clsArr == null)continue;
					if(clsArr.indexOf(className) > -1)
					{
						delArr.push(key);
					}
				}
				if(delArr.length == 0)
					return;
			}
			else
			{				
				arr = PoolUtil._poolDict[className];
				if(arr == null)return;
				delArr.push(className);
			}
			
			len2 = delArr.length;
			while(--len2 > -1)
			{
				arr = PoolUtil._poolDict[delArr[len2]];
				len = arr.length;
				while(--len>-1)
				{
					if(arr[len].dispose!=null)
						arr[len].dispose();
				}
				arr.length = 0;
				delete PoolUtil._poolCfgDict[delArr[len2]];
				delete PoolUtil._poolDict[delArr[len2]];
			}			
		}
	}
}