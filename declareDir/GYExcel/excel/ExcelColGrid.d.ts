declare module GYLite {
    class ExcelColGrid extends ItemRender implements IItemRender {
        protected _text: GYText;
        protected _drager: GYSprite;
        excel: GYExcel;
        constructor();
        protected initComponent(): void;
        protected drawBg(): void;
        protected dragSize(handle: DraggerHandle): void;
        protected widthAndHeightChange(e: GYViewEvent): void;
        /**@inheriDoc */
        setData(obj: any): void;
    }
}
