declare module GYLite {
    class GYViewEvent extends GYEvent {
        /**当GYUIComponent视图改变时，如width/height/scaleX/scaleY/x/y/right/bottom*/
        static VIEWCHANGE: string;
        /**动画播放完毕*/
        static ANIMATION_END: string;
        /**视图更新完毕*/
        static UPDATE_COMPLETE: string;
        /**radio组件被选择时，回调data [上次选择的radio，当前选择的radio]*/
        static RADIO_SELECTED: string;
        /**组件被选择时*/
        static SELECTED: string;
        /**组件取消选择时触发*/
        static UNSELECTED: string;
        /**列表格子刷新*/
        static GRID_UPDATE: string;
        /**列表数据刷新*/
        static DATA_UPDATE: string;
        /**垂直拖动到尽头*/
        static DRAGEND_VERTICAL: string;
        /**水平拖动到尽头*/
        static DRAGEND_HORIZONAL: string;
        /**垂直滚动(点击箭头按钮、滚轮)到尽头MIN*/
        static SCROLL_VERTICAL_MIN: string;
        /**水平滚动(点击箭头按钮、滚轮)到尽头MIN*/
        static SCROLL_HORIZONAL_MIN: string;
        /**垂直滚动(点击箭头按钮、滚轮)到尽头MAX*/
        static SCROLL_VERTICAL_MAX: string;
        /**水平滚动(点击箭头按钮、滚轮)到尽头MAX*/
        static SCROLL_HORIZONAL_MAX: string;
        /**垂直滚动到尽头*/
        static SCROLLEND_VERTICAL: string;
        /**水平滚动到尽头*/
        static SCROLLEND_HORIZONAL: string;
        /**拖动结束*/
        static DRAGSTOP: string;
        /**拖动开始*/
        static DRAGSTART: string;
        /**裁切滚动*/
        static CLIP_SCROLL: string;
        /**静态文本显示*/
        static STATIC_TEXT_SHOW: string;
        /**静态文本隐藏*/
        static STATIC_TEXT_HIDE: string;
        constructor(type: string, bubbles?: boolean, cancelable?: boolean, data?: any);
    }
}
