function ReflectPlugin(){
	
}
ReflectPlugin.prototype.exec = function (ts,root,fileName,text){
	var result = {};
	url = root + "proj.log";
	//ts.sys.write("fileexsit:"+ts.sys.fileExists(url)+","+url+","+sourceFile.fileName+","+sourceFile.isDeclarationFile+"\n");
	if(ts.sys.fileExists(url))
	{
		var buffer;
		try {
			buffer = ts.sys.readFile(url,"utf-8");
		}
		catch (e) {					
			result.error = {error:1,msg:"error TS:read log file fail:" + url};
			return result;
		}
		if(buffer)
		{						
			var json = JSON.parse(buffer);
			var i,len,arr;
			var obj;			
			arr = json.files;
			len = arr.length;
			//ts.sys.write("getbuffer:" + len);
			if(len > 0)
			{
				for(i=0;i<len;++i)
				{
					obj = arr[i];
					if(fileName == obj.nativePath.replace(/\\/g,"/"))															
						break;													
				}
				if(i != len)
				{
					var str = "";					
					arr = obj.defClasses;
					len = arr.length;
					for(i=0;i<len;++i)
					{
						obj = arr[i];						
						if(obj.type == "class" && (obj.export || obj.moduleName == ""))
						{		
							if(obj.implInterfaces && obj.implInterfaces.length > 0)
								str += "__reflect(" + obj.key + ".prototype, \"" + obj.key + "\",[\"" + obj.implInterfaces.join("\",\"") + "\"]);\n";
							else
								str += "__reflect(" + obj.key + ".prototype, \"" + obj.key + "\");\n";
						}
					}
				}
			}
		}
	}
	else
	{
		result.error = {error:2,msg:"error TS:log not exsit:" + url};
		return result;
	}
	if(str)
		return {text:str};
	//result.error = {error:3,msg:"error TS: defNum:"+len+", cannot find reflect:" + sourceFile.fileName + "," + m};
	return result;
}