/**
 @author 迷途小羔羊
 2014-11-19
 */
declare module GYLite {
    /**
     * 加载资源，用于记载资源时，记录资源的一些基本信息
     * 当要销毁时，请调用其Clear方法，并且把引用设置为null
     * */
    class LoadInfo {
        static SEND_RES: number;
        static SEND_DATA: number;
        static SEND_HTTP: number;
        save: boolean;
        path: string;
        bytes: any;
        startTime: number;
        private _content;
        contentType: string;
        header: Array<any>;
        type: any;
        method: string;
        thisObject: any;
        param: InfoParam;
        data: any;
        progressInfo: any[];
        retainBytes: boolean;
        lifeTime: number;
        /**http状态码*/ status: number;
        /**传送类型，参考LoadInfo常量*/ sendType: number;
        private _msg;
        private _progFunc;
        private _callBackFunc;
        private _loadVec;
        mimeType: string;
        version: string;
        private static _pool;
        constructor();
        get dataFormat(): string;
        set callBackFunc(val: Function);
        /**加载完成回调函数*/
        get callBackFunc(): Function;
        set msg(val: string);
        /**错误信息*/
        get error(): number;
        /**加载信息*/
        get msg(): string;
        set content(val: ResObject);
        get content(): ResObject;
        hasRes(): boolean;
        /**加载完成，会调用所有关联的回调*/
        callBack(): void;
        /**取消加载请求,当加载请求全部被取消时，自动清理这个LoadInfo
         * @return 是否已无加载请求
         * */
        cancel(callBackFunc: Function, thisObject: any): boolean;
        set progFunc(val: Function);
        /**加载进度回调函数*/
        get progFunc(): Function;
        prog(e: ProgressEvent): void;
        /**清理回收*/
        clear(): void;
        /**同一个url的加载请求，将被添加到第一个加载请求的LoadInfo下面*/
        addLoadInfo(val: LoadInfo): void;
        private copy;
        static create(): LoadInfo;
    }
    /**当加载atlas时，用户的param参数存放在InfoParam的param，用户无需理会InfoParam格式*/
    interface InfoParam {
        callBackFunc?: Function;
        thisObject?: any;
        /**加载资源集合时，原始附加参数*/ param?: any;
        /**加载资源集合时，图集图片的资源路径[路径，加载类型，路径，加载类型]*/
        assets?: any[];
        /**加载资源集合时，图集图片的资源引用*/ refResObjects?: ResObject[];
        [key: string]: any;
    }
}
