declare module GYLite {
    class CompressLoadInfo {
        private _progFunc;
        private _callBackFunc;
        private _resCount;
        private _resMax;
        curLoadInfo: LoadInfo;
        thisObject: any;
        param: any;
        msg: string;
        errorCode: number;
        startTime: number;
        progressInfo: any[];
        loader: GYLoader;
        constructor();
        /**加载url的媒体资源，图片、mp3
         * @param url url地址
         * @param type 类型 参考GYLoader常量
        */
        loadPath(url: string, type: number): void;
        /**加载url的数据资源
         * @param url ur地址
         * @param type 类型 参考GYLoader常量
        */
        loadData(url: string, type: number): void;
        /**读取bytes资源 png jpg bmp
         * @param bytes uint8Array或者ByteArray的buffer属性
         * @param callBackFunc 回调函数，返回参数为LoadInfo,返回后，loadInfo会自动被清理
         * @param thisObject callBackFunc的this指向
         * @param type 加载资源类型，参考GYLoader常量
         * @param mimeType blob资源 如"image/png" 图片文件 "audio/basic" 声音文件
         * @param param 附加参数
         * @param retainBytes 是否保留加载的文件字节数据
         * */
        loadBytes(bytes: ArrayBuffer, type?: number, mimeType?: string, param?: any, path?: string, retainBytes?: boolean): void;
        setResCount(cur: any, max: any): void;
        set callBackFunc(val: Function);
        /**加载完成回调函数*/
        get callBackFunc(): Function;
        set progFunc(val: Function);
        /**加载进度回调函数*/
        get progFunc(): Function;
        callBack(l?: LoadInfo, key?: string, startTime?: number): void;
        private bytesCallBack;
        /**资源总数*/
        get resMax(): number;
        /**未解压的资源数*/
        get resCount(): number;
    }
}
