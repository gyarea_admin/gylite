class Enter extends egret.DisplayObjectContainer{
	public constructor() {
		super();
		let s = this;
		GYLite.GYLoader.getDefaultLoader();
		if(s.stage)
			s.projInit(this);
		else
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);		
	}
	private addToStage(e:egret.Event):void
	{
		this.projInit(this);
	}
	private projInit(s):void
	{	
		var mainCls:any;		
		mainCls = window["Main"];
		if(mainCls)
		{
			GYLite.GYSprite.Init(s.stage, null);
			s.addChild(new mainCls());
		}
		else
			setTimeout(s.projInit,100, s);
	}
}