declare module GYLite {
    class ExcelSelFrame extends GYSprite implements IDragger {
        owner: ExcelFrame;
        constructor();
        protected _handle: DraggerHandle;
        /**@inheritDoc*/
        getBitmapData(): egret.Texture;
        /**@inheritDoc*/
        isLockCenter(): number;
        /**@inheritDoc*/
        getHandle(): DraggerHandle;
        /**@inheritDoc*/
        setHandle(val: DraggerHandle): void;
        /**@return excel的选框*/
        getData(): any;
        /**@inheritDoc*/
        draggingSet(draggerShape: GYScaleSprite): boolean;
        /**@inheritDoc*/
        draggingDraw(draggerShape: GYScaleSprite): boolean;
        /**@inheritDoc*/
        dragStop(): void;
    }
}
