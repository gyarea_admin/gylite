module GYLite
{					
	export class RenderGroup extends GYGroup
	{		
		private _renderSprites:IRenderTestElement[];
		private _paused:boolean;
		public txt:GYLite.GYText;
		private _sp:GYSprite;
		public constructor()
		{
			super();
			var s = this;			
			s._paused = false;			
			s.percentWidth = 1;			
			s.init();
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private addToStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			// s.run();
			// s.touchEnabled = true;
			// s.rectHit = true;
			// s.width = 1920;
			// s.height = 1080;
			// s.addEventListener(egret.TouchEvent.TOUCH_TAP,s.tap,s)
			// s.addRender(1,0);
			// s.addRender(1,1);
			// s.addRender(1,2);
			// s.addRender(1,3);
			// s.addRender(1,4);
			// s.addRender(0,5);
			// s.addRender(1,6);
			// s.addRender(1,7);
			s.addRender(0,8);
			// let sp:GYLite.GYSprite = new GYLite.GYSprite;
			// let g:GYLite.GYGraphics;
			// g = <GYLite.GYGraphics>sp.graphics;
			// g.beginFill(0xff0000,0.5);
			// g.drawCircle(300,300,5);
			// g.endFill();
			// s.addElement(sp);
		}
		private removeFromStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.stop();			
		}
		private init():void
		{var s = this;
			s._renderSprites = [];	
			s._sp= new GYSprite;			
			// s._sp.mouseThrough = true;
			s.addElement(s._sp);	
			s.txt = s.getTxt(s.width - 100,100,0,"当前数量：0");
			s.txt.right = 100;
		}
		private tap(e:egret.TouchEvent):void
		{
			let s = this;
			// s.stop();
			if(s._paused)
			{
				if(GYKeyboard.getInstance().isCtrlDown())
					s.addRenders(1);
				else 
					s.removeRenders(GYKeyboard.getInstance().isShiftDown()?10:1);
				return;
			}
			s.pause();
		}
		private run():void
		{
			let s= this;
			CommonUtil.addStageLoop(s.loop,s);
		}
		private loop(t:number):void
		{
			let s= this;
			let rSp:IRenderTestElement;
			let len:number;
			if(s._paused)
				return;
			len = s._renderSprites.length;			
			while(--len>-1)
			{
				rSp = s._renderSprites[len];				
				if(rSp.disposed)
					s._renderSprites.splice(len,1);
				else
					rSp.loop(t)
			}
			len = s._renderSprites.length;
			if(len > 800)
			{
				// CommonUtil.delStageLoop(s.loop,s);
				// console.log("max:"+len)
				return;
			}				
			len = 5;
			while(--len>-1)
				s.addRender(Math.random()*4|0,Math.random()*8|0);
				// s.addRender(2,0);
				
			s.txt.text = "当前数量：" + s._renderSprites.length;
			s.addElement(s.txt);
		}
		private stop():void
		{
			let s= this;
			s._paused = false;
			CommonUtil.delStageLoop(s.loop,s);
			let len:number;
			len = s._renderSprites.length;
			while(--len>-1)
			{
				s._renderSprites[len].dispose();
			}
			s._renderSprites.length = 0;
		}
		private pause():void
		{
			let s= this;
			s._paused = !s._paused;
		}		
		private addRender(fillType:number=0,drawType:number=0):void
		{
			let s = this;
			let rSp:IRenderTestElement;
			rSp = new RenderSprite(fillType,drawType);
			s._renderSprites.push(rSp);
			rSp.x = 500;//(Math.random() * 1400|0) + 200;			
			rSp.y = 200;
			rSp.rotation = 0;//Math.PI/4*MathConst.ANGLE_ROTATION;
			s._sp.addElement(rSp);
			
			rSp.touchEnabled = true;
			rSp.rectHit = true;
			rSp.addEventListener(egret.TouchEvent.TOUCH_TAP,function(e:MouseEvent):void{
				// rSp.alpha = 0.5;
				console.log(rSp.mouseX,rSp.mouseY);
			},s);
			// rSp.addEventListener(MouseEvent.ROLL_OUT,function(e:MouseEvent):void{
			// 	rSp.alpha = 1;
			// },s);
		}
		private addRenders(num:number=1):void
		{
			let s= this;
			let len:number;
			len = num;
			while(--len>-1)
			{
				// s.addRender(Math.random()*4|0,Math.random()*8|0);
				s.addRender(2,7);
			}
		}
		private removeRenders(num:number=1):void
		{
			let s= this;
			let len:number,ind:number;
			len = s._renderSprites.length;
			ind = len - num - 1;
			if(ind < -1)
				ind = -1;
			while(--len>ind)
			{
				s._renderSprites[len].dispose();
				s._renderSprites.splice(len,1);
			}
		}
		private getTxt(tx:number,ty:number,index:number=0,str:string = null):GYLite.GYText
		{
			let txt:GYText = new GYText;
			txt.size = 16;
			txt.color = 0;
			txt.text = str?str:String.fromCharCode(Math.random()*30000|0) + index;
			// txt.height = 30;                    
			txt.color = 0xff00ff;
			// txt.underline = true;
			txt.x = tx;
			txt.y = ty;				
			return txt;
		}
		
	}
}