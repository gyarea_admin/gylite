module egret {
    export class Capabilities
    {
        public static readonly language: string = "zh-CN";        
        public static readonly isMobile: boolean;        
        public static readonly os: string = "Unknown";        
        public static readonly runtimeType: string = "web";        
        public static readonly engineVersion: string = "5.4.1";        
        public static readonly renderMode: string = "Unknown";
        public static readonly boundingClientWidth: number = 0;        
        public static readonly boundingClientHeight: number = 0;
        public static _supportedCompressedTexture: SupportedCompressedTexture = <SupportedCompressedTexture>{};
        
    }
    export namespace RuntimeType {       
        export const WEB = "web";        
        export const NATIVE = "native";        
        export const RUNTIME2 = "runtime2";
        export const MYGAME = "mygame";        
        export const WXGAME = "wxgame";
        export const BAIDUGAME = "baidugame";        
        export const QGAME = "qgame";        
        export const OPPOGAME = "oppogame";       
        export const QQGAME = "qqgame";       
        export const VIVOGAME = "vivogame";        
        export const QHGAME = "qhgame";        
        export const TTGAME = "ttgame";       
        export const FASTGAME = "fastgame";        
        export const TBCREATIVEAPP = "tbcreativeapp";
    }
    export interface SupportedCompressedTexture {
        pvrtc: boolean;
        etc1: boolean;
    }
        
}