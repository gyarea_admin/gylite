declare module GYLite {
    class SanGuoTheme extends GYSkinTheme {
        rect_15_15_7_15: Scale9GridRect;
        rect_15_15_10_10: Scale9GridRect;
        rect_5_5_5_5: Scale9GridRect;
        constructor();
        /**获得默认的TextFormat*/
        GetTextTextFormat(): TextFormat;
        /**获得默认的GYTabButton皮肤*/
        GetTabButtonSkin(): IGYSkin;
        /**获得默认的GYProgressBar皮肤*/
        GetProgressBarSkin(): IGYSkin;
        /**获得默认的GYSlider皮肤*/
        GetSliderSkin(): IGYSkin;
        /**获得默认的s.toolTip*/
        GetToolTip(): GYToolTip;
        initRes(): void;
        protected setRes(): void;
        getDraggerBtn(): GYSprite;
    }
}
