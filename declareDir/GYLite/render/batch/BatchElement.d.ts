declare module GYLite {
    class BatchElement implements IResource, IPoolObject {
        /**宽度(包括增加的边距)*/ width: number;
        /**高度(包括增加的边距)*/ height: number;
        $disposed: boolean;
        constructor();
        get disposed(): boolean;
        dispose(): void;
        protected beforeToPool(): void;
        clear(): void;
        inPool: boolean;
        outPoolInit(): void;
    }
}
