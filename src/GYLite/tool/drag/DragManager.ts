module GYLite
{
									
						
	export class DragManager
	{
		// private static _draggerShape:GYScaleSprite = new GYScaleSprite;
		private static _dragGroup:GYSprite;		
		private static _dragVec:IDragger[];
		private static _dragShapes:GYImage[];
		/**拖动对象的s.x偏移*/public static offsetX:number;
		/**拖动对象的s.y偏移*/public static offsetY:number;
		/**拖动对象的透明度*/public static alpha:number;
		/**拖动对象的是否平滑处理*/public static smoothing:boolean;
		/**默认s.x偏移*/public static defaultOffsetX:number;
		/**默认s.y偏移*/public static defaultOffsetY:number;
		/**不锁定位置*/public static NONE_LCOK:number=0;
		/**锁定到对象中心*/public static CENTER_LCOK:number=1;
		/**锁定到对象左上角*/public static LEFT_TOP_LCOK:number=2;
		/**锁定到对象右上角*/public static RIGHT_TOP_LCOK:number=3;
		/**锁定到对象左下角*/public static LEFT_BOTTOM_LCOK:number=4;
		/**锁定到对象右下角*/public static RIGHT_BOTTOM_LCOK:number=5;
		public static Init():void
		{
			var s = this;
			s._dragShapes = new Array<GYImage>();
			// s._draggerShape = new GYScaleSprite;
			s._dragGroup = new GYSprite;			
			s._dragVec = new Array<IDragger>();
			s.offsetX = 0;
			s.offsetY = 0;
			s.alpha=0.5;
			s.defaultOffsetX = 0;
			s.defaultOffsetY = 0;
			s.smoothing = false;
			// s._dragGroup.addElement(s._draggerShape);
		}
		/**设置为拖动对象* */
		public static setDrag(dragger:IDragger):void
		{
			var d:DraggerHandle;
			d = DraggerHandle.getInstance(dragger);
			d.addBind(DragManager.dragging,dragger);
			dragger.setHandle(d);			
			dragger.addEventListener(GYViewEvent.DRAGSTART, DragManager.dragStart, dragger);
		}
		/**取消拖动* */
		public static cancelDrag(dragger:IDragger):void
		{
			var d:DraggerHandle;
			if(dragger.getHandle())
				dragger.getHandle().clear();
			dragger.setHandle(null);
			dragger.removeEventListener(GYViewEvent.DRAGSTART, DragManager.dragStart, dragger);
		}
		/**添加到为拖动组，任意对象触发拖动时，一并跟随, 释放拖动自动清除拖动组 
		 * */
		public static addToDragGroup(dragger:IDragger):void
		{
			if(DragManager._dragVec.indexOf(dragger) == -1)
			{
				DragManager._dragVec.push(dragger);				
			}
		}
		/**从拖动组删除
		 * @param dargger 拖动对象 
		 * */
		public static deleteFromDragGroup(dragger:IDragger):void
		{
			var ind:number;
			ind = DragManager._dragVec.indexOf(dragger);
			if(ind == -1)
				return;
			DragManager._dragVec.splice(ind, 1);
		}
		/**清除拖动组 */
		public static clearDraggerGroup():void
		{
			DragManager._dragVec.length = 0;
		}
		/**设置偏移量为默认值 */
		public static resetOffset():void
		{
			DragManager.offsetX = DragManager.defaultOffsetX;
			DragManager.offsetY = DragManager.defaultOffsetY;
		}
		private static dragStop(e:GYViewEvent):void
		{			
			var dragger:IDragger = e.currentTarget as IDragger;
			var receiver:IReceiver,pr:IGYContainer;
			let dropTarget:any;
			dropTarget = dragger.getHandle().dropTarget;
			DragManager.clearDraggerGroup();
			GYSprite.stage.removeChild(DragManager._dragGroup);						
			DragManager._dragGroup.removeAllElement();
			receiver = CommonUtil.GYIs(dropTarget, "GYLite.IReceiver")?dropTarget:null;
			dragger.removeEventListener(GYViewEvent.DRAGSTOP, DragManager.dragStop, dragger);
			if(receiver == null && dropTarget)
			{
				pr = dropTarget;
				while(pr)
				{
					receiver = <any>(CommonUtil.GYIs(pr,"GYLite.IReceiver")?pr:null);
					if(receiver)break;
					pr = <any>(CommonUtil.GYIs(pr.parent,"GYLite.IGYContainer")?pr.parent:null);
				}
			}
			while(receiver)
			{
				receiver.receiveData(dragger);
				receiver = CommonUtil.GYIs(receiver.parent, "GYLite.IReceiver")?<any>receiver.parent:null;
			}
		}
		private static dragStart(e:GYViewEvent):void
		{
			var toX:number,toY:number;
			var g:GYImage;
			var targetDragger:IDragger;
			var bitmapData:egret.Texture;	
			var mX:number, mY:number;
			targetDragger = e.currentTarget as IDragger;
			if(targetDragger.draggingDraw(DragManager._dragShapes[0]))
				return;
			mX = targetDragger.mouseX;
			mY = targetDragger.mouseY;
			targetDragger.addEventListener(GYViewEvent.DRAGSTOP, DragManager.dragStop,targetDragger);
			bitmapData = targetDragger.getBitmapData();
			if(targetDragger.isLockCenter() == DragManager.CENTER_LCOK)
			{
				GYSprite._matrix.tx = toX = -(bitmapData.textureWidth>>1) + DragManager.offsetX;
				GYSprite._matrix.ty = toY = -(bitmapData.textureHeight>>1) + DragManager.offsetY;
			}
			else if(targetDragger.isLockCenter() == DragManager.LEFT_TOP_LCOK)
			{
				GYSprite._matrix.tx = toX = DragManager.offsetX;
				GYSprite._matrix.ty = toY = DragManager.offsetY;
			}
			else if(targetDragger.isLockCenter() == DragManager.RIGHT_TOP_LCOK)
			{
				GYSprite._matrix.tx = toX = DragManager.offsetX - bitmapData.textureWidth;
				GYSprite._matrix.ty = toY = DragManager.offsetY;
			}
			else if(targetDragger.isLockCenter() == DragManager.LEFT_BOTTOM_LCOK)
			{
				GYSprite._matrix.tx = toX = DragManager.offsetX;
				GYSprite._matrix.ty = toY = DragManager.offsetY - bitmapData.textureHeight;
			}
			else if(targetDragger.isLockCenter() == DragManager.RIGHT_BOTTOM_LCOK)
			{
				GYSprite._matrix.tx = toX = DragManager.offsetX - bitmapData.textureWidth;
				GYSprite._matrix.ty = toY = DragManager.offsetY - bitmapData.textureHeight;
			}
			else
			{
				GYSprite._matrix.tx = toX = -mX;
				GYSprite._matrix.ty = toY = -mY;
			}
			DragManager.getDragShape(bitmapData, DragManager.smoothing, 0);	
			
			var i:number,len:number;
			var dragger:IDragger;
			len = DragManager._dragVec.length + 1;
			for(i=1;i<len;++i)
			{
				dragger = DragManager._dragVec[i];
				if(dragger == targetDragger)
					continue;				
				bitmapData = dragger.getBitmapData();
				g = DragManager.getDragShape(bitmapData, DragManager.smoothing, 0);	
				g.x = toX + dragger.x - targetDragger.x;
				g.y = toY + dragger.y - targetDragger.y;				
			}			
			DragManager._dragGroup.alpha = DragManager.alpha;
			GYSprite.stage.addChild(DragManager._dragGroup);
		}
		private static dragging(handle:DraggerHandle):void
		{
			var drag:IDragger = handle.handle as IDragger;
			var len:number;
			len = DragManager._dragShapes.length;
			while(--len>-1)
			{
				if(!drag.draggingSet(DragManager._dragShapes[len]))
				{
					DragManager._dragShapes[len].x = GYSprite.stageX;
					DragManager._dragShapes[len].y = GYSprite.stageY;
				}
			}			
		}
		private static getDragShape(b:egret.Texture, smoothing:boolean, ind:number):GYImage
		{
			var sp:GYImage;
			if(ind < DragManager._dragShapes.length)
				sp = DragManager._dragShapes[ind];
			else
			{
				sp = new GYImage;
				sp.enableBatch(false);
				DragManager._dragShapes[ind] = sp;
			}			
			sp.source = b;
			sp.smoothing = smoothing;
			if(sp.parent == null)
				DragManager._dragGroup.addElement(sp);
			return sp;
		}
	}
}
