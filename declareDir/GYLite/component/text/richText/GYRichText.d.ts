declare module GYLite {
    /**
     * 包含图片样式： <code>&lt;bmp s.width='' s.height='' target='**' render='**' renderParam='**' s.clear='**' &gt;</code>
     * 可自定义更多样式
     */
    class GYRichText extends GYGroup {
        static imageList: Array<any>;
        /**文本容器*/
        get richSp(): GYSprite;
        /**混排容器*/
        get imgGrp(): GYSprite;
        get maxLines(): number;
        set maxLines(value: number);
        set richTop(value: number);
        /**上边界*/
        get richTop(): number;
        set leading(value: number);
        /**行间距*/
        get leading(): number;
        /**文本格式*/
        get textFormat(): TextFormat;
        set letterSpacing(value: number);
        /**字间距*/
        get letterSpacing(): number;
        /**文本高度*/
        get textHeight(): number;
        /**文本宽度*/
        get textWidth(): number;
        /**点击函数*/
        get linkFun(): Function;
        setLinkFun(fun: Function, thisObject: any): void;
        get embedFont(): boolean;
        set embedFont(value: boolean);
        /**首行固定*/
        get firstLineFix(): boolean;
        set firstLineFix(value: boolean);
        /**鼠标所在的文本元素*/
        get whichLine(): number;
        constructor();
        getRichLabel(indent?: number): RichLabel;
        /**添加间距*/
        appendGap(value?: number): void;
        /**添加文本*/
        appendText(text: string): void;
        /**清理内容*/
        clear(): void;
        private splitStr;
        /**获取图片文本
         * @param w-宽度 h-高度 Id-图片id
         * */
        static getImageRich(Id?: number, offsetX?: number, offsetY?: number, w?: number, h?: number): string;
        /**获取url图片文本
         * @param w-宽度 h-高度 Id-图片id
         * */
        static getUrlImageRich(url: string, offsetX?: number, offsetY?: number, w?: number, h?: number): string;
        private static _endReg;
        private static _tagReg;
        private static _attrReg;
        private _lineVec;
        private _richSP;
        private _imgGrp;
        private _firstLineFix;
        private _embedFont;
        private _maxLines;
        private _y;
        private _padTop;
        private _linkFun;
        private _linkThisObject;
        private _tmpW;
        private _textFormat;
    }
}
