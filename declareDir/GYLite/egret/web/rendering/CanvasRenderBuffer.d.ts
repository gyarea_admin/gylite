declare namespace egret.web {
    /**
     * @private
     * Canvas2D渲染缓冲
     */
    class CanvasRenderBuffer implements sys.RenderBuffer {
        constructor(width?: number, height?: number, root?: boolean);
        /**
         * 渲染上下文
         */
        context: CanvasRenderingContext2D;
        /**
         * 呈现最终绘图结果的画布
         */
        surface: HTMLCanvasElement;
        /**
         * 渲染缓冲的宽度，以像素为单位。
         * @readOnly
         */
        get width(): number;
        /**
         * 渲染缓冲的高度，以像素为单位。
         * @readOnly
         */
        get height(): number;
        /**
         * 改变渲染缓冲的大小并清空缓冲区
         * @param width 改变后的宽
         * @param height 改变后的高
         * @param useMaxSize 若传入true，则将改变后的尺寸与已有尺寸对比，保留较大的尺寸。
         */
        resize(width: number, height: number, useMaxSize?: boolean): void;
        /**
         * 获取指定区域的像素
         */
        getPixels(x: number, y: number, width?: number, height?: number): number[];
        /**
         * 转换成base64字符串，如果图片（或者包含的图片）跨域，则返回null
         * @param type 转换的类型，如: "image/png","image/jpeg"
         */
        toDataURL(type?: string, encoderOptions?: number): string;
        /**
         * 清空缓冲区数据
         */
        clear(): void;
        /**
         * 销毁绘制对象
         */
        destroy(): void;
    }
}
