/**
 @author 迷途小羔羊
 2015.6.5
 */
declare module GYLite {
    /**复选框*/
    class GYCheckBox extends GYButton {
        constructor(skin?: any);
        protected initComponent(): void;
        /**获取主题皮肤，自定义皮肤请实现ICheckBoxSkin接口*/
        protected getThemeSkin(): IGYSkin;
        /**文本与复选框的间隙*/
        get gap(): number;
        set gap(val: number);
    }
}
