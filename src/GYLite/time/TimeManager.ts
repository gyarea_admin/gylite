module GYLite
{
											
	export class TimeManager 
	{
		private static timer:egret.Timer;
		public static dic:Array<any>;
		public static timeoutDic:Dictionary;
		public static timeIntervalDic:Dictionary;
		public static init():void
		{
			if (!TimeManager.timer)
			{
				TimeManager.timer=new egret.Timer(1000);
				TimeManager.timer.addEventListener(egret.TimerEvent.TIMER, TimeManager.timeEvent,TimeManager.timer);
				TimeManager.timer.start();
				TimeManager.dic=[];
				TimeManager.timeoutDic = new Dictionary;
				TimeManager.timeIntervalDic = new Dictionary;
			}
		}
		/**注册秒计监听
		 * @param func 回调函数func(t:number);t为程序启动距离现在的毫秒数
		 * @param thisObject
		 * @return id 监听函数的唯一id
		 * */
		public static registered(func:Function,thisObject:any):number
		{
			var id:number=TimeManager.getId(func,thisObject);
			TimeManager.dic[id]={func:func,thisObject:thisObject};
			return id;
		}
		
		private static getId(func:Function,thisObject:any):number
		{
			var len:number=TimeManager.dic.length;
			var index:number=len;
			for (var t_i:number=0; t_i < len; ++t_i)
			{
				var funData:any=TimeManager.dic[t_i];
				if (funData && funData.func == func && funData.thisObject == thisObject) //防止重复注册
				{
					return t_i;
				}
				if (funData == null)
				{
					index=t_i;
				}
			}
			return index;
		}
		
		private static timeEvent(e:egret.Event):void
		{
			var len:number=TimeManager.dic.length;
			var time:number=egret.getTimer();
			var funData:any;
			for (var t_i:number=0; t_i < len; ++t_i)
			{
				funData = TimeManager.dic[t_i];
				if (funData)
				{
					if(funData.thisObject && funData.thisObject.disposed)
					{
						TimeManager.dic[t_i] = null;
						continue;
					}
					funData.func.call(funData.thisObject, time);
				}					
			}
		}
		/**移除秒计监听
		 * @param id 唯一标识id
		 * @param func 监听函数
		 * */
		public static unRegistered(id:number,func:Function,thisObject:any):void
		{
			if (TimeManager.dic[id] && TimeManager.dic[id].func == func && TimeManager.dic[id].thisObject == thisObject) //防止错删
			{
				TimeManager.dic[id]=null;
			}
		}
		/**功能如同setTimeout，结束内部会自动调clearTimeout清理，重复调会自动停止之前未执行的timeout(请注意匿名函数不算重复的)*/
		public static timeOut(func:Function,thisObject:any, delay:number, ...rest):number
		{
			let key:string;
			let obj:any;
			if(thisObject.$hashCode==null)
				thisObject.$hashCode = ++GYSprite.hashCode;
			key = func.toString() + (<egret.DisplayObject>thisObject).$hashCode;
			obj = TimeManager.timeoutDic.getValue(key);
			if(obj)
			{
				clearTimeout(obj.timeId);
				TimeManager.timeoutDic.deleteKey(obj.key);
			}
			obj = {};
			obj.key = key;
			obj.delay = delay;
			obj.thisObject = thisObject;
			obj.rest = rest;
			obj.timeId = setTimeout(function(obj):void{
				obj = TimeManager.timeoutDic.getValue(obj.key);
				if(obj == null)return;
				clearTimeout(obj.timeId);
				TimeManager.timeoutDic.deleteKey(obj.key);
				if(obj.thisObject.disposed)									
					return;				
				func.apply(obj.thisObject,obj.rest);
			}, delay, obj);			
			TimeManager.timeoutDic.setValue(key, obj);
			return obj.timeId;
		}
		/**移除timeout
		 * @param id 唯一标识id
		 * @param func 监听函数
		 * */
		public static unTimeOut(id:number,func:Function,thisObject:any):void
		{
			let key:string;
			let obj:any;
			if(thisObject.$hashCode==null)
				thisObject.$hashCode = ++GYSprite.hashCode;
			key = func.toString() + (<egret.DisplayObject>thisObject).$hashCode;
			obj = TimeManager.timeoutDic.getValue(key);
			if(obj)
			{
				clearTimeout(obj.timeId);
				TimeManager.timeoutDic.deleteKey(obj.key);
			}
		}
		/**功能如同setInterval，重复调会自动停止之前未执行的timeInterval(请注意匿名函数不算重复的)*/
		public static timeInterval(func:Function,thisObject:any, delay:number, ...rest):number
		{
			let key:string;
			let obj:any;
			if(thisObject.$hashCode==null)
				thisObject.$hashCode = ++GYSprite.hashCode;
			key = func.toString() + thisObject.$hashCode;
			obj = TimeManager.timeIntervalDic.getValue(key);
			if(obj)
			{
				clearInterval(obj.timeId)
				TimeManager.timeIntervalDic.deleteKey(obj.key);
			}
			obj = {};			
			obj.key = key;
			obj.delay = delay;
			obj.thisObject = thisObject;
			obj.rest = rest;
			obj.timeId = setInterval(function(obj):void{
				obj = TimeManager.timeIntervalDic.getValue(obj.key);
				if(obj == null)return;				
				if(obj.thisObject.disposed)
				{
					clearInterval(obj.timeId);
					TimeManager.timeIntervalDic.deleteKey(obj.key);
					return;
				}					
				func.apply(obj.thisObject,obj.rest);
			}, delay, obj);
			TimeManager.timeIntervalDic.setValue(key, obj);
			return obj.timeId;
		}
		/**移除TimeManager.timeInterval监听
		 * @param id 唯一标识id
		 * @param func 监听函数
		 * */
		public static unTimeInterval(id:number,func:Function,thisObject:any):void
		{
			let key:string;
			let obj:any;
			if(thisObject.$hashCode==null)
				thisObject.$hashCode = ++GYSprite.hashCode;
			key = func.toString() + (<egret.DisplayObject>thisObject).$hashCode;
			obj = TimeManager.timeIntervalDic.getValue(key);			
			if(obj)
			{
				clearInterval(obj.timeId);
				TimeManager.timeIntervalDic.deleteKey(obj.key);
			}
		}
	}
}