/**
 @author 迷途小羔羊
 2015.6.5
 */
module GYLite
{
	/**单选按钮组*/
	export class GYRadioGroup extends egret.EventDispatcher
	{
		private _selectedButton:GYRadioButton;		
		/**设置此属性选中组内单选按钮*/
		public set selectedButton(val:GYRadioButton)
		{
			let s = this;
			if(val.radioGroup != this)
				return;
			if(s._selectedButton == val)
				return;
			let old:GYRadioButton = s._selectedButton;
			if(s._selectedButton)
				s._selectedButton.selected = false;
			s._selectedButton = val;
			s._selectedButton.selected = true;
			if(s.hasEventListener(GYViewEvent.RADIO_SELECTED))
				s.dispatchEventWith(GYViewEvent.RADIO_SELECTED, false, [old, s._selectedButton]);
		}
		public get selectedButton():GYRadioButton
		{
			let s = this;
			return s._selectedButton;
		}
	}
}
