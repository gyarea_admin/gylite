declare module GYLite {
    class GYGraphics extends egret.Graphics implements IBatch {
        x: number;
        y: number;
        width: number;
        height: number;
        /**动态合批时，小图使用的间隙*/ gap: number;
        constructor(target?: egret.DisplayObject);
        /**设置全局透明度*/
        setGlobalAlpha(val: number): void;
        clearBatch(): void;
        /**合批图集名称*/
        getBatchAtlasName(): string;
        setBatchAtlasName(val: string): void;
        /**合批绘制方式*/
        getBatchDrawParam(): BatchDrawParam;
        setBatchDrawParam(val: BatchDrawParam): void;
        /**是否合批状态*/
        isBatch(): boolean;
        enableBatch(val: boolean): void;
        $setTarget(target: egret.DisplayObject): void;
        protected setStrokeWidth(width: number): void;
        /**开始填充
         * @param color 颜色，默认NaN，表示清除填充
         * @param alpha 透明度
         * @param smoothing 控制在缩放时是否对位图进行平滑处理。
        */
        beginFill(color?: number, alpha?: number, smoothing?: boolean): void;
        /**渐变填充*/
        beginGradientFill(type: string, colors: number[], alphas: number[], ratios: number[], matrix?: egret.Matrix, smoothing?: boolean): void;
        /**位图填充*/
        beginBitmapFill(tex: egret.Texture, matrix: egret.Matrix, repeat?: boolean, alpha?: number, smoothing?: boolean): void;
        /**闭合填充*/
        endFill(): void;
        /**笔触样式
         * @param thickness 笔触大小，默认1
         * @param color 颜色，默认0
         * @param alpha 透明度，默认1
         * @param pixelHinting 是否完整像素 默认false（此功能暂时未提供）
         * @param scaleMode 拉伸模式 默认"normal"，此功能暂时未提供
         * @param cap 端点 默认"round"
         * @param joint 拐角 默认null，此功能暂时未提供
         * @param miterLimit 拐角最大像素,仅在joint为"miter"有效，默认0，此功能暂时未提供
         * @param lineDash 虚线值，默认[6,3]，此功能暂时未提供
         * @param tex 纹理填充
         * @param matrix 纹理的变换
        */
        lineStyle(thickness?: number, color?: number, alpha?: number, pixelHinting?: boolean, scaleMode?: string, cap?: string, joint?: string, miterLimit?: number, lineDash?: number[]): void;
        /**位图填充笔触
         * @param thickness 笔触大小
         * @param alpha 透明度
         * @param tex 填充的纹理
         * @param matrix 纹理的矩阵变换，默认null，则纹理垂直于线条方向
         * */
        lineBitmapStyle(thickness?: number, alpha?: number, tex?: egret.Texture, matrix?: egret.Matrix): void;
        /**渐变填充笔触*/
        lineGradientStyle(thickness: number, type: string, colors: number[], alphas: number[], ratios: number[], matrix?: egret.Matrix, cap?: CanvasLineCap, joint?: CanvasLineJoin): void;
        /**dataDraw-矩形绘制*/
        drawRect(x: number, y: number, width: number, height: number): void;
        /**dataDraw-圆角矩形绘制*/
        drawRoundRect(x: number, y: number, width: number, height: number, ellipseWidth: number, ellipseHeight?: number): void;
        /**dataDraw-圆绘制*/
        drawCircle(x: number, y: number, radius: number, density?: number, smoothing?: boolean): void;
        /**do-执行圆绘制（包含椭圆）*/
        private doDrawCircle;
        /**dataDraw-椭圆绘制*/
        drawEllipse(x: number, y: number, width: number, height: number, density?: number, smoothing?: boolean): void;
        /**dataDraw-绘制多边形*/
        drawPoly(path: number[]): void;
        /**do-执行绘制多边形
         * @param path 多边形路径
         * @param check 检测多边形顶点是否交错 默认true
         * @param stroke 描边 默认true
        */
        private doDrawPoly;
        /**dataDraw-绘制三角形
         * @param vertices 顶点坐标 例如 [10,10,30,10,30,30]
         * @param uvs uv贴图坐标，例如 [0,0,1,0,1,1]
         * @param indices 顶点索引，例如 [0,1,2]
        */
        drawTriangle(vertices: number[], uvs: number[], indices: number[]): void;
        private doDrawTriangle;
        /**笔触移动到(x,y)*/
        moveTo(x: number, y: number): void;
        private doMoveTo;
        /**dataDraw-从当前笔触位置绘制线段到(x,y)*/
        lineTo(x: number, y: number): void;
        /**执行绘制线段
         * @param x 目标坐标x
         * @param y 目标坐标y
         * @param blur 如果未产生锯齿，默认使用的模糊像素，默认0，不使用
         * @param lastLineNode 上一个线段节点
         * @param cap 端点 默认null，无端点
         * @param joint 拐角 默认null，无拐角
         * @param dataLine 是否graphics绘制数据，默认false
        */
        private dolineTo;
        private jointDraw;
        private capDraw;
        private pathDraw;
        private bezierDraw;
        /**dataDraw-从当前坐标开始绘制二次贝塞尔曲线*/
        curveTo(controlX: number, controlY: number, anchorX: number, anchorY: number): void;
        /**dataDraw-从当前坐标开始绘制三次贝塞尔曲线*/
        cubicCurveTo(controlX1: number, controlY1: number, controlX2: number, controlY2: number, anchorX: number, anchorY: number): void;
        private doCubicCurve;
        drawArc(x: number, y: number, radius: number, startAngle: number, endAngle: number, anticlockwise?: boolean, density?: number): void;
        /**do-执行绘制一段圆弧
         * @param x 圆心x
         * @param y 圆心y
         * @param radius 圆弧半径
         * @param startAngle 起始弧度
         * @param endAngle 结束弧度
         * @param anticlockwise 是否逆时针 默认false
        */
        protected doDrawArc(x: number, y: number, radius: number, startAngle: number, endAngle: number, anticlockwise?: boolean, density?: number): void;
        /**使用贝塞尔曲线画一段圆弧线
         * @param x 圆心x
         * @param y 圆心y
         * @param radiusX 横向半径
         * @param radiusY 纵向半径
         * @param startAngle 起始弧度
         * @param endAngle 结束弧度
         * @param anticlockwise 是否逆时针 默认false
         * @param density 采样密度 默认0.04
         * @param excludeEndPoint 是否忽略最后一个端点 默认false
         * @param result 采样结果数组 默认null，则直接绘制，否则result里存放采样的坐标
        */
        private arcToBezier;
        protected dirty(): void;
        protected arcBounds(x: number, y: number, radius: number, startAngle: number, endAngle: number): void;
        clear(): void;
        protected extendBoundsByPoint(x: number, y: number): void;
        protected extendBoundsByX(x: number): void;
        protected extendBoundsByY(y: number): void;
        protected updateNodeBounds(): void;
        protected updatePosition(x: number, y: number): void;
        $measureContentBounds(bounds: egret.Rectangle): void;
        $hitTest(stageX: number, stageY: number): egret.DisplayObject;
        $onAddToStage(): void;
        $onRemoveFromStage(): void;
        /**获取合批对象*/
        getBatch(param: any, type: GraphicsType, gap?: number): BatchInfo;
        get lastData(): GraphicsData;
        private createBitmapNode;
        private createMeshNode;
        private createTexMeshNode;
        private newLine;
        private _saves;
        private save;
        private restore;
        protected drawData(d: GraphicsData): void;
        protected removeNodes(nodes: egret.sys.RenderNode[]): void;
        protected removeNode(node: egret.sys.RenderNode): void;
        protected addData(d: GraphicsData): void;
        protected addNode(node: egret.sys.RenderNode): void;
        protected createGradientParam(param: any, ratio: number[], w: number, h: number): any;
        get fillRepeat(): boolean;
        get fillGradientType(): string;
        get lineGradientType(): string;
        get miterLimit(): number;
        get thickness(): number;
        get cap(): CanvasLineCap;
        get joint(): CanvasLineJoin;
        get color(): number[];
        get alpha(): number[];
        get ratio(): number[];
        get texture(): egret.Texture;
        get lineMatrix(): egret.Matrix;
        get fillMatrix(): egret.Matrix;
        get fillColor(): number[];
        get fillAlpha(): number[];
        get fillRatio(): number[];
        get fillTexture(): egret.Texture;
        get smoothing(): boolean;
        getBatchTexture(tex: egret.Texture, gap?: number): egret.Texture;
        private static _tempPt0;
        private static _tempPt1;
        private static _tempPt2;
        private static _tempPt3;
        private static _tempMatrix;
        protected _patternStyleMap: any;
        protected _batchDrawParam: BatchDrawParam;
        protected _smoothing: boolean;
        protected _data: GraphicsData[];
        protected _color: number[];
        protected _alpha: number[];
        protected _ratio: number[];
        protected _texture: egret.Texture;
        protected _lineMatrix: egret.Matrix;
        protected _cap: CanvasLineCap;
        protected _joint: CanvasLineJoin;
        protected _thickness: number;
        protected _miterLimit: number;
        protected _fillColor: number[];
        protected _fillAlpha: number[];
        protected _fillRatio: number[];
        protected _fillTexture: egret.Texture;
        protected _fillMatrix: egret.Matrix;
        protected _fillRepeat: boolean;
        protected _fillGradientType: string;
        protected _gradientMatrix: egret.Matrix;
        protected _densty: number;
        protected _lastLineNode: GYMeshNode;
        protected _lastLineData: LineData;
        protected _lineGradientType: string;
        protected _cacheDrawData: any[];
        protected _lastCapNodes: GYMeshNode[];
        protected _lastStartX: number;
        protected _lastStartY: number;
        protected _lastBezierNode: GYMeshNode;
        protected _isAllDraw: boolean;
        protected static _lineCaps: string[];
        protected _batch: boolean;
        protected _batchAtlasName: string;
    }
}
