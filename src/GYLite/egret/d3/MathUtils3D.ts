module GYLite
{
    export class MathUtils3D {
        constructor() {
        }
        static isZero(v:number) {
            return Math.abs(v) < MathUtils3D.zeroTolerance;
        }
        static nearEqual(n1:number, n2:number) {
            if (MathUtils3D.isZero(n1 - n2))
                return true;
            return false;
        }
        static fastInvSqrt(value:number) {
            if (MathUtils3D.isZero(value))
                return value;
            return 1.0 / Math.sqrt(value);
        }
        public static zeroTolerance:number = 1e-6;
        public static MaxValue:number = 3.40282347e+38;
        public static MinValue:number = -3.40282347e+38;
    }    
}