declare module GYLite {
    class GYSkin implements IGYSkin {
        protected _hostComponent: GYSprite;
        constructor();
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        clone(): IGYSkin;
        copy(val: IGYSkin): void;
        release(): void;
    }
}
