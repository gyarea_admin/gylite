declare namespace egret {
    /**
     * @version Egret 2.4
     * @platform Web,Native
     */
    class NumberUtils {
        /**
         * Judge whether it is a numerical value
         * @param value Parameter that needs to be judged
         * @returns
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 判断是否是数值
         * @param value 需要判断的参数
         * @returns
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static isNumber(value: any): boolean;
        /**
         * Obtain the approximate sin value of the corresponding angle value
         * @param value {number} Angle value
         * @returns {number} sin value
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 得到对应角度值的sin近似值
         * @param value {number} 角度值
         * @returns {number} sin值
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static sin(value: number): number;
        /**
         * @private
         *
         * @param value
         * @returns
         */
        private static sinInt;
        /**
         * Obtain the approximate cos value of the corresponding angle value
         * @param value {number} Angle value
         * @returns {number} cos value
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 得到对应角度值的cos近似值
         * @param value {number} 角度值
         * @returns {number} cos值
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static cos(value: number): number;
        /**
         * @private
         *
         * @param value
         * @returns
         */
        private static cosInt;
        static convertStringToHashCode(str: string): number;
    }
}
/**
 * @private
 */
declare let egret_sin_map: {};
/**
 * @private
 */
declare let egret_cos_map: {};
/**
 * @private
 */
declare let DEG_TO_RAD: number;
