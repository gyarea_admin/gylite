module GYLite
{
						
			
	export class GameToolTip extends GYToolTip
	{
//		[Embed(source="../gameImg/toolTipBack.png")]
//		private backCls:Class;
		public backBitData:egret.Texture;// = (new s.backCls as Bitmap).bitmapData;
		private scaleShape:GYScaleSprite;
		public constructor()
		{
			super();
		}
		protected initComponent():void
		{var s = this;
			if(s.backBitData == null)
				s.backBitData = GYLoader.getRes("toolTipBack","Img/ui.png").refRes(s);
			s.scaleShape = new GYScaleSprite(s.backBitData, (GYSprite.skinTheme as GYSkinTheme).commonRect);
			s.addElement(s.scaleShape);
			s.labelDisplay = new GYText(true);
			s.labelDisplay.paddingLeft = 10;
			s.labelDisplay.paddingRight = 5;
			s.labelDisplay.paddingTop = 9;
			s.labelDisplay.paddingBottom = 5;
			s.labelDisplay.color = 0xffffff;
			s.labelDisplay.autoWidth = true;
			s.addElement(s.labelDisplay);
		}
		protected drawBackground(w:number=0,h:number=0):void
		{var s = this;
			s.scaleShape.width = s.labelDisplay.width;
			s.scaleShape.height = s.labelDisplay.height;
		}
		private static _gameToolTip:GameToolTip;
		public static getInstance():GYToolTip
		{			
			if(GameToolTip._gameToolTip == null)
			{
				GameToolTip._gameToolTip = new GameToolTip;
			}
			return GameToolTip._gameToolTip;
		}
	}
}