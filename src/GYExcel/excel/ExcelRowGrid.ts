module GYLite
{
												
					
	
	export class ExcelRowGrid extends ItemRender implements IItemRender
	{
		protected _text:GYText;
		// protected _img:GYDrawImage;
		protected _drager:GYSprite;
		public excel:GYExcel;
		public constructor()
		{
			super();
			var s = this;
			s.width = 25;
			s.height = 30;
			s.initComponent();
		}
		protected initComponent():void
		{var s = this;
			s._text = new GYText;
			s._text.verticalCenter = 8;
			s._text.width = s.width;
			s._text.height = s.height;
			s._text.align = "center";
			s._text.touchEnabled = false;
			// s._text.selectable = false;
			s._text.font = "Microsoft YaHei";
			// s._text.embedFonts = false;
			s._text.color = 0;
			s._text.validFormat();
			// s._img = new GYDrawImage;
			// s._img.touchEnabled = s._img.touchChildren = false;
			// s._img.verticalCenter = 5;
			// s.addElement(s._img);
			s.addElement(s._text);
			s.addEventListener(GYViewEvent.VIEWCHANGE, s.widthAndHeightChange,s);
			s._drager = new GYSprite;
			s._drager.y = s.height - 5;
			DraggerHandle.getInstance(s._drager).addBind(s.dragSize,s);
			var g:egret.Graphics = s._drager.graphics;
			g.beginFill(0);
			g.drawRect(0,0,s.width,5);
			g.endFill();
			s.addElement(s._drager);
			s.touchEnabled = true;
			s._drager.touchEnabled = true;
		}
		protected drawBg():void
		{var s = this;
			s.graphics.clear();
			s.graphics.beginFill(0xcccccc,1);
			s.graphics.drawRect(0,0,s.width,s.height);
			s.graphics.beginFill(0xeeeeee,1);
			s.graphics.drawRect(1,1,s.width-2,s.height - 2);
			s.graphics.endFill();
		}
		protected widthAndHeightChange(e:GYViewEvent):void
		{var s = this;
			s.drawBg();
			s._text.width = s.width;
			// s._img.draw(s._text);
			s._drager.y = s.height - 5;
		}
		protected dragSize(handle:DraggerHandle):void
		{var s = this;
			var ind:number=Number(s._data) - 1;
			s.excel.setGridHeight(ind, s.mouseY);
		}
		/**@inheriDoc */
		public setData(obj:any):void
		{var s = this;
			s._data = obj;
			if(s._data==null)
				s._text.text ="无";
			else
				s._text.text =String(s._data);
			// s._img.draw(s._text);
			s.width = 25;
			s.height = 30;
		}
	}
}