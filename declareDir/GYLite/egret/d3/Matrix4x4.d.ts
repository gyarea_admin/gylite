declare module GYLite {
    class Matrix4x4 {
        private static _tempMatrix4x4;
        private static _tempVector0;
        private static _tempVector1;
        private static _tempVector2;
        private static _tempQuaternion;
        private static DEFAULT;
        private static ZERO;
        static get tempQuaternion(): Quaternion;
        elements: Float32Array;
        constructor(m11?: number, m12?: number, m13?: number, m14?: number, m21?: number, m22?: number, m23?: number, m24?: number, m31?: number, m32?: number, m33?: number, m34?: number, m41?: number, m42?: number, m43?: number, m44?: number);
        getElementByRowColumn(row: number, column: number): number;
        setElementByRowColumn(row: number, column: number, value: number): void;
        /**
         *判断两个4x4矩阵的值是否相等。
        *@param other 4x4矩阵
        */
        equalsOtherMatrix(other: Matrix4x4): boolean;
        /**
         *分解矩阵为平移向量、旋转四元数、缩放向量。
        *@param translation 平移向量。
        *@param rotation 旋转四元数。
        *@param scale 缩放向量。
        *@return 是否分解成功。
        */
        decomposeTransRotScale(translation: Vector3, rotation: Quaternion, scale: Vector3): boolean;
        /**
         *分解矩阵为平移向量、旋转矩阵、缩放向量。
        *@param translation 平移向量。
        *@param rotationMatrix 旋转矩阵。
        *@param scale 缩放向量。
        *@return 是否分解成功。
        */
        decomposeTransRotMatScale(translation: Vector3, rotationMatrix: Matrix4x4, scale: Vector3): boolean;
        /**
         *分解旋转矩阵的旋转为YawPitchRoll欧拉角。
        *@param out float yaw
        *@param out float pitch
        *@param out float roll
        *@return
        */
        decomposeYawPitchRoll(yawPitchRoll: Vector3): void;
        /**归一化矩阵 */
        normalize(): void;
        /**计算矩阵的转置矩阵*/
        transpose(): Matrix4x4;
        /**
         *计算一个矩阵的逆矩阵
        *@param out 输出矩阵
        */
        invert(out: Matrix4x4): void;
        /**设置矩阵为单位矩阵*/
        identity(): void;
        /**
         *克隆。
        *@param destObject 克隆源。
        */
        cloneTo(destObject: Matrix4x4): void;
        /**
         *克隆。
        *@return 克隆副本。
        */
        clone(): Matrix4x4;
        /**
         *获取平移向量。
        *@param out 平移向量。
        */
        getTranslationVector(out: Vector3): void;
        /**
         *设置平移向量。
        *@param translate 平移向量。
        */
        setTranslationVector(translate: Vector3): void;
        /**
         *获取前向量。
        *@param out 前向量。
        */
        getForward(out: Vector3): void;
        /**
         *设置前向量。
        *@param forward 前向量。
        */
        setForward(forward: Vector3): void;
        static createRotationX(rad: number, out: Matrix4x4): void;
        static createRotationY(rad: number, out: Matrix4x4): void;
        static createRotationZ(rad: number, out: Matrix4x4): void;
        static createRotationYawPitchRoll(yaw: number, pitch: number, roll: number, result: Matrix4x4): void;
        static createRotationAxis(axis: Vector3, angle: number, result: Matrix4x4): void;
        static createRotationQuaternion(rotation: Quaternion, result: Matrix4x4): void;
        static createTranslate(trans: Vector3, out: Matrix4x4): void;
        static createScaling(scale: Vector3, out: Matrix4x4): void;
        static multiply(left: Matrix4x4, right: Matrix4x4, out: Matrix4x4): void;
        static createFromQuaternion(rotation: Quaternion, out: Matrix4x4): void;
        static createAffineTransformation(trans: Vector3, rot: Quaternion, scale: Vector3, out: Matrix4x4): void;
        static createLookAt(eye: Vector3, target: Vector3, up: Vector3, out: Matrix4x4): void;
        static createPerspective(fov: number, aspect: number, near: number, far: number, out: Matrix4x4): void;
        static createOrthoOffCenterRH(left: number, right: number, bottom: number, top: number, near: number, far: number, out: Matrix4x4): void;
        translation(v3: Vector3, out: Matrix4x4): void;
    }
}
