﻿/**
@author 迷途小羔羊
2012.11.18
*/
module GYLite
{				
	export class GYImage extends GYUIComponent
	{
		protected static _imgLoader:GYLoader;
		public static init():void
		{
			GYImage._imgLoader=new GYLoader(5);
		}
		protected _mySource:GYBitmap;
		protected _anchorX:number;
		protected _anchorY:number;
		protected _pivotX:number;
		protected _pivotY:number;
		protected _smoothing:boolean;				
		protected _texture:egret.Texture;
		public constructor()
		{
			super();
			var s = this;
			s._anchorX = NaN;
			s._anchorY = NaN;
			s._pivotX = 0;
			s._pivotY = 0;
			s._mySource = new GYBitmap;
			s.addEventListener(MouseEvent.ROLL_OVER, s.RlOver,s);
			s.addChild(s._mySource);
			s._mySource.setBatchDrawParam(s._batchDrawParam);
		}					
		public validBatch():void
		{
			let s = this;			
			s._mySource.validBatch();
		}				
		public set smoothing(val:boolean)
		{
			let s = this;
			s._smoothing = val;
			s._mySource.smoothing = val;
		}
		public get smoothing():boolean
		{
			let s = this;
			return s._mySource.smoothing;
		}
		public set source(bmp:egret.Texture)
		{
			let s = this;
			if(bmp == s._texture)
				return;
			s._texture = bmp			
			if(bmp)
			{
				if(s.$width == s.$width)
					s.scaleX=s.$width/bmp.textureWidth;
				if(s.$height == s.$height)
					s.scaleY=s.$height/bmp.textureHeight;
			}			
			s._mySource.smoothing = s._smoothing;				
			s._mySource.texture = s._texture;
			s._mySource.x = s._anchorX == s._anchorX?-s.width*s._anchorX:-s._pivotX;
			s._mySource.y = s._anchorY == s._anchorY?-s.height*s._anchorY:-s._pivotY;
			if(s.isBatch())
				s.validBatch();
			s.invalidDisplay();			
		}
		/**位图*/		
		public get source():egret.Texture
		{
			let s = this;
			return s._mySource.texture;
		}
		public set width(val:number)
		{
			let s = this;			
			if(s.$width == val)
				return;
			s.$width=val;
			if(s.$width != s.$width)
				s.scaleX=1;
			else
			{
				if(s._mySource.texture)
					s.scaleX=s.$width/s._mySource.texture.textureWidth;
			}
		}	
		public get width():number
		{
			let s = this;
			if(s.$width == s.$width)
				return s.$width;
			return egret.superGetter(GYImage, s, "width");
		}
		public set height(val:number)
		{
			let s = this;	
			if(s.$height == val)
				return;
			s.$height=val;
			if(s.$height != s.$height)
				s.scaleY=1;
			else
			{
				if(s._mySource.texture)
					s.scaleY=s.$height/s._mySource.texture.textureHeight;
			}
		}	
		public get height():number
		{
			let s = this;			
			if(s.$height == s.$height)
				return s.$height;
			return egret.superGetter(GYImage, s, "height");
		}
		protected RlOver(e:egret.TouchEvent):void
		{
			let s = this;
			if (GYSprite.isStageDown(this))
				s.dispatchEvent(GYSprite.mouseDownEvent);
		}
		/**中心锚点（x百分比）*/
		public get anchorX():number
		{
			let s = this;
			return s._anchorX;
		}

		public set anchorX(value:number)
		{
			let s = this;
			if(s._anchorX == value)return;
			s._anchorX = value;
			s._mySource.x = value == value?-s.width*value:-s._pivotX;
		}
		/**中心锚点（y百分比）*/
		public get anchorY():number
		{
			let s = this;
			return s._anchorY;
		}

		public set anchorY(value:number)
		{
			let s = this;
			if(s._anchorY == value)return;
			s._anchorY = value;
			s._mySource.y = value == value?-s.height*value:-s._pivotY;
		}
		/**中心点X（像素值）*/
		public get pivotX():number
		{
			let s = this;
			return s._pivotX;
		}

		public set pivotX(value:number)
		{
			let s = this;
			if(s._pivotX == value)
				return;
			s._pivotX = value;
			if(s._anchorX == s._anchorX)
				return;
			s._mySource.x = -s._pivotX;
		}
		/**中心点Y（像素值）*/
		public get pivotY():number
		{
			let s = this;
			return s._pivotY;
		}

		public set pivotY(value:number)
		{
			let s = this;
			if(s._pivotY == value)
				return;
			s._anchorY = value;
			if(s._anchorY == s._anchorY)
				return;
			s._mySource.y = -s._anchorY;
		}
		public setBatchDrawParam(val:BatchDrawParam)
		{
			let s= this;			
			super.setBatchDrawParam(val);			
			if(s._mySource)s._mySource.setBatchDrawParam(val);
		}
		/**合批图像的绘制样式**/
		public getBatchDrawParam():BatchDrawParam
		{
			return super.getBatchDrawParam();
		}
		public enableBatch(val:boolean)
		{
			let s = this;
			s._mySource.enableBatch(val);
			super.enableBatch(val);
		}
		/**是否动态合批，请在文本渲染前设定*/
		public isBatch():boolean
		{
			let s = this;
			return super.isBatch();
		}
		public get bitmap():GYBitmap
		{let s = this;
			return s._mySource;
		}

		/**跟pivotX 一样功能(废弃)*/
		public get archorX():number
		{
			let s = this;
			return s._pivotX;
		}
		/**跟pivotY 一样功能(废弃)*/
		public set archorX(value:number)
		{
			let s = this;
			s.pivotX = value;
		}
		/**跟pivotY 一样功能(废弃)*/
		public get archorY():number
		{
			let s = this;
			return s._pivotY;
		}
		/**跟pivotY 一样功能(废弃)*/
		public set archorY(value:number)
		{
			let s = this;
			s.pivotY = value;
		}
		protected resetBound():void
		{
			let s= this;
			s._boundRect.y=s._boundRect.x=0;
			s._boundRect.width = s._texture?s._texture.textureWidth:0;
			s._boundRect.height = s._texture?s._texture.textureHeight:0;
			
		}
		protected resetElementsBound():void
		{
			let s= this;
			s._elementsRect.y=s._elementsRect.x=0;
			s._elementsRect.width = s._texture?s._texture.textureWidth:0;
			s._elementsRect.height = s._texture?s._texture.textureHeight:0;
		}
	}
}