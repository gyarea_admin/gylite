module GYLite
{
			
	export class CommonUtil
	{
		public static loopTime:number;
		protected static _stage:egret.Stage;
		private static _loopFuncVec:Function[];
		private static _loopFuncObj:IResource[];
		public static frameRate:number;
		public static frameDelta:number;
		/**标准帧率下的速度系数（以30帧为标准，如果提高到60，则是0.5）*/
		public static rateParam:number;
		public static Init(stage:egret.Stage):void
		{
			CommonUtil._stage=stage;
			CommonUtil.loopTime = Date.now();
			CommonUtil._loopFuncVec = new Array<Function>();
			CommonUtil._loopFuncObj = new Array<any>();
			CommonUtil.loopTime = Date.now();
			CommonUtil.frameRate = GYLite.GYSprite.stage.frameRate;
			CommonUtil.frameDelta = 1000 / GYLite.GYSprite.stage.frameRate|0;
			CommonUtil.rateParam = 30 / CommonUtil.frameRate;

			CommonUtil._stage.addEventListener(egret.Event.ENTER_FRAME, CommonUtil.stageLoop,CommonUtil._stage);
		}
		/**
		 * 添加val到舞台帧频执行列表中，ENTERFRAME事件触发时执行
		 * @param Function类型，无参数val();
		 * */
		public static addStageLoop(val:Function, thisObject:any):void
		{
			if(val == null)
			{
				throw(new Error("不能传入空函数！"));				
			}
			var ind:number = CommonUtil._loopFuncVec.indexOf(val);
			if(ind == -1 || CommonUtil._loopFuncObj[ind] != thisObject)
			{
				CommonUtil._loopFuncVec.push(val);
				CommonUtil._loopFuncObj.push(thisObject);
			}				
		}
		/**
		 * 从舞台帧频执行列表中删除val函数
		 * @param val 帧频函数
		 * */
		public static delStageLoop(val:Function, thisObject:any):void
		{			
			var len:number;
			len = CommonUtil._loopFuncVec.length;
			while(--len>-1)
			{
				if(CommonUtil._loopFuncVec[len] ==val && CommonUtil._loopFuncObj[len] == thisObject)
				{
					CommonUtil._loopFuncVec.splice(len, 1);
					CommonUtil._loopFuncObj.splice(len, 1);
				}
			}
		}
		private static stageLoop(e:egret.Event):void
		{
			CommonUtil.loopTime=Date.now();
			var len:number;
			len=CommonUtil._loopFuncVec.length;
			while(--len>-1)
			{
				if(len<CommonUtil._loopFuncVec.length)									
					CommonUtil._loopFuncVec[len].call(CommonUtil._loopFuncObj[len],CommonUtil.loopTime);									
			}
		}
		/**帧频监听列表*/
		public static get loopFuncVec():Function[]
		{
			return CommonUtil._loopFuncVec;
		}
		public static get loopFuncObj():any[]
		{
			return CommonUtil._loopFuncObj;
		}
		/**类型判断，cls参数可以使用字符串(接口只能用字符串参数，因为在h5中只能使用字符串记录接口)，继承链15层以内有效
		 * @param obj 对象
		 * @param cls 类型		 
		*/
		public static GYIs(obj,cls)
		{
			if(obj == null)return false;
			if(cls == "string")
				cls = String;
			else if(cls == "number")
				cls = Number;
			else if(cls.constructor == String)
			{					
				return egret.is(obj, <any>cls);
			}
			var proto;
			var c =0;
			proto = obj.__proto__;
			while(proto.constructor.name != "Object")
			{
				if(proto.constructor == cls)
					return true;
				proto = proto.__proto__;
				if(++c > 15)
					break;
			}
			return false;
		}
		/**判断对象是否属于某种类型(包括接口，此方法是通过__type__属性查询，请务必保证ts编译代码附加了__type__继承链)
		 * @param obj 对象
		 * @param className 完全类名
		*/
		public static typeIs(obj:any,className:string):boolean
		{
			if(obj == null)return false;
			if(obj["__type__"])
			{
				let arr:any[];
				arr = obj["__type__"];
				let len:number;
				len = arr.length;
				while(--len>-1)
				{
					if(arr[len] == className)
						return true;
				}				
			}
			return CommonUtil.GYIs(obj,className);
		}
		/**比较两个类是否同一个继承链上*/
		public static classIs(cls:any, superCls:any):boolean
		{
			if(cls == null || superCls == null)
				return null;
			if(superCls.prototype.__class__ == null || cls.prototype.__types__ == null)
				return false;
			let ind:number=cls.prototype.__types__.indexOf(superCls.prototype.__class__);
			return ind > -1;
		}

		/**垃圾回收已经被销毁的对象的loop*/
		public static gc():void
		{
			var len:number;
			len = CommonUtil._loopFuncObj.length;
			while(--len>-1)
			{
				if(CommonUtil._loopFuncObj[len].constructor == GYTween)continue;//缓动不进行回收，由GYTween里面的类进行gc
				if(CommonUtil._loopFuncObj[len].disposed)
				{
					CommonUtil._loopFuncVec.splice(len,1);
					CommonUtil._loopFuncObj.splice(len,1);
				}
			}
		}
	}
}