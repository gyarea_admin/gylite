module GYLite{
    export interface IRenderTestElement extends IGYDisplay{
        loop(t:number);
        get graphics():egret.Graphics;
    }
}