declare module GYLite {
    interface IButtonSkin extends IGYSkin {
        drawSkin(state: number): void;
        width: number;
        height: number;
        label: string;
        labelDisplay: GYText;
        cloneLabel(): GYText;
    }
}
