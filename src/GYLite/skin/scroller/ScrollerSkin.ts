module GYLite
{
									
		
	export class ScrollerSkin extends GYSkin implements IScrollerSkin
	{
		protected _arrowBtn1:GYButton;
		protected _arrowBtn2:GYButton;
		protected _scrollBar:GYButton;
		protected _sliderBg:GYScaleSprite;
		// protected _scaleImg:GYScaleSprite;
		public constructor(sliderSkin:GYButton|ButtonSkin,sliderBg:GYScaleSprite|egret.Texture,arrowSkin1:GYButton|ButtonSkin=null, arrowSkin2:GYButton|ButtonSkin=null,bgRect:Scale9GridRect=null)
		{
			super();
			var s = this;
			s._arrowBtn1 = GYLite.CommonUtil.GYIs(arrowSkin1, GYButton)?<GYButton>arrowSkin1:new GYButton(arrowSkin1);
			s._arrowBtn2 = GYLite.CommonUtil.GYIs(arrowSkin2, GYButton)?<GYButton>arrowSkin2:new GYButton(arrowSkin2);
			s._scrollBar = GYLite.CommonUtil.GYIs(sliderSkin, GYButton)?<GYButton>sliderSkin:new GYButton(sliderSkin);
			s._sliderBg =GYLite.CommonUtil.GYIs(sliderBg, GYScaleSprite)?<GYScaleSprite>sliderBg:new GYScaleSprite(<egret.Texture>sliderBg);
			s._sliderBg.touchEnabled = true;
			if(bgRect == null)
				s._sliderBg.scale9GridRect = new Scale9GridRect;
			else
				s._sliderBg.scale9GridRect = bgRect;
		}
		public release():void
		{var s = this;
			//s._scaleImg.bitmapData = null;
			s._arrowBtn1.skin.release();
			s._arrowBtn2.skin.release();
			s._scrollBar.skin.release();
		}
		public set hostComponent(val:GYSprite)
		{var s = this;
			if(val == s._hostComponent)
				return;
			if(s._hostComponent)
			{
				if(s._sliderBg.parent)
					s._hostComponent.removeElement(s._sliderBg);
				if(s._scrollBar.parent)
					s._hostComponent.removeElement(s._scrollBar);
				if(s._arrowBtn1.parent)
					s._hostComponent.removeElement(s._arrowBtn1);
				if(s._arrowBtn2.parent)
					s._hostComponent.removeElement(s._arrowBtn2);
			}
			s._hostComponent=val;
			if(s._hostComponent)
			{
				s._hostComponent.addElement(s._sliderBg);
				s._hostComponent.addElement(s._scrollBar);
				s._hostComponent.addElement(s._arrowBtn1);
				s._hostComponent.addElement(s._arrowBtn2);
			}
		}
		public get hostComponent():GYSprite
		{var s = this;
			return s._hostComponent;
		}
		/**第一个箭头按钮*/
		public get arrowBtn1():GYButton
		{var s = this;
			return s._arrowBtn1;
		}
		/**第二个箭头按钮*/
		public get arrowBtn2():GYButton
		{var s = this;
			return s._arrowBtn2;
		}
		/**滑块*/
		public get scrollBar():GYButton
		{var s = this;
			return s._scrollBar;
		}
		/**滑动区域*/
		public get sliderBg():GYScaleSprite
		{var s = this;
			return s._sliderBg;
		}
		public clone():IGYSkin 
		{var s = this;
			var scrollerSkin:ScrollerSkin=new ScrollerSkin(s._scrollBar.skin,s._sliderBg.bitmapData,s._arrowBtn1.skin, s._arrowBtn2.skin);	
			return scrollerSkin;
		}
	}
}