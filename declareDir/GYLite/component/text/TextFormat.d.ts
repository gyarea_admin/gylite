declare module GYLite {
    class TextFormat {
        font: any;
        bold: any;
        italic: any;
        size: any;
        textColor: any;
        letterSpacing: any;
        indent: any;
        underline: any;
        leading: any;
        align: any;
        constructor(font?: string, size?: any, color?: any, bold?: any, italic?: any, underline?: any, url?: string, target?: string, align?: string, leftMargin?: any, rightMargin?: any, indent?: any, leading?: any);
        clone(): TextFormat;
        setFormat(t: any): void;
    }
}
