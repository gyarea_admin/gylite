module GYLite
{
								
				
	export class LayoutGroup extends GYGroup
	{
		//s.layout
		private uic:GYUIComponent;
		private rightSp:GYImage;
		private bottomSp:GYImage;
		private horizSp:GYImage;
		private vertiSp:GYImage;
		private backImg:GYImage;
		private _v:number=2;

		public constructor()
		{
			super();
			var s = this;			
			s.init();
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private addToStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			CommonUtil.addStageLoop(s.layoutDown,s);
		}
		private removeFromStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			CommonUtil.delStageLoop(s.layoutDown,s);
		}
		private init():void
		{var s = this;
			/*------------Layout-------------*/
			s.uic=new GYUIComponent;
			s.uic.x = 120;
			s.uic.y = 180;
			s.uic.width = 500;
			s.uic.height = 500;
			s.uic.touchEnabled = true;
			s.addElement(s.uic);
			Main.instance.setTip(s.uic, "相对布局与百分比宽高\n骑宠为相对右布局\n信件为垂直居中布局\n天道为水平与垂直居中布局\n任务为相对底部布局\n强化为相对宽高");
			var background:GYImage;
			background = new GYImage;
			background.percentHeight = 1;
			background.percentWidth = 1;
			background.source = GYLoader.getRes("Img/7.jpg").refRes(s);
			s.uic.addElement(background);
			s.backImg=new GYImage;
			s.backImg.percentHeight = 0.5;
			s.backImg.left = 50;
			s.backImg.right = 50;
			s.backImg.source = GYLoader.getRes("1","Img/ui.png").refRes(s);
			s.uic.addElement(s.backImg);
			s.rightSp=new GYImage;
			s.rightSp.source = GYLoader.getRes("2","Img/ui.png").refRes(s);
			s.uic.addElement(s.rightSp);
			s.rightSp.right=20;
			s.bottomSp=new GYImage;
			s.bottomSp.source = GYLoader.getRes("3","Img/ui.png").refRes(s);
			s.bottomSp.bottom=30;
			s.uic.addElement(s.bottomSp);
			s.horizSp=new GYImage;
			s.horizSp.source = GYLoader.getRes("4","Img/ui.png").refRes(s);
			s.horizSp.horizonalCenter = 0;
			s.horizSp.verticalCenter = 0;
			s.uic.addElement(s.horizSp);
			s.vertiSp=new GYImage;
			s.vertiSp.source = GYLoader.getRes("6","Img/ui.png").refRes(s);
			s.vertiSp.verticalCenter = -50;
			s.uic.addElement(s.vertiSp);
			
		}
		
		private Draw(g:egret.Graphics,al:number,cl:number=0):void
		{var s = this;
			g.beginFill(cl,al);
			g.drawRect(0,0,100,100);
			g.endFill();
		}
		private Draw2(g:egret.Graphics,cl:number=0,w:number=0,h:number=0):void
		{var s = this;
			g.clear();
			g.beginFill(cl,1);
			g.drawRect(0,0,w,h);
			g.endFill();
		}
		private layoutDown(t:number):void
		{var s = this;
			if(s.uic.width > 500 && s._v > 0 || s.uic.width < 200 && s._v < 0)
				s._v = -s._v;
			s.uic.width += s._v;
			s.uic.height += s._v;
		}
	}
}