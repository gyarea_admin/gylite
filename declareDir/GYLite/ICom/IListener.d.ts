declare module GYLite {
    interface IListener {
        dataChange(listenId: number): void;
    }
}
