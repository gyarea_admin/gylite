declare namespace egret {
    /**
     * @private
     * @version Egret 2.4
     * @platform Web,Native
     */
    class TextFieldUtils {
        /**
         * 获取第一个绘制的行数
         * @param textfield 文本
         * @returns {number} 行数，从0开始
         * @private
         */
        static $getStartLine(textfield: egret.TextField): number;
        /**
         * 获取水平比例
         * @param textfield 文本
         * @returns {number} 水平比例
         * @private
         */
        static $getHalign(textfield: egret.TextField): number;
        /**
         * @private
         *
         * @param textfield
         * @returns
         */
        static $getTextHeight(textfield: egret.TextField): number;
        /**
         * 获取垂直比例
         * @param textfield 文本
         * @returns {number} 垂直比例
         * @private
         */
        static $getValign(textfield: egret.TextField): number;
        /**
         * 根据x、y获取文本项
         * @param textfield 文本
         * @param x x坐标值
         * @param y y坐标值
         * @returns 文本单项
         * @private
         */
        static $getTextElement(textfield: egret.TextField, x: number, y: number): ITextElement;
        /**
         * 获取文本点击块
         * @param textfield 文本
         * @param x x坐标值
         * @param y y坐标值
         * @returns 文本点击块
         * @private
         */
        static $getHit(textfield: egret.TextField, x: number, y: number): IHitTextElement;
        /**
         * 获取当前显示多少行
         * @param textfield 文本
         * @returns {number} 显示的行数
         * @private
         */
        static $getScrollNum(textfield: egret.TextField): number;
    }
}
