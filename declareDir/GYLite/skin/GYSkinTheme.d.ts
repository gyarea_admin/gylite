declare module GYLite {
    class GYSkinTheme implements ISkinTheme {
        /**主题名称，唯一标识*/
        get themeName(): string;
        /**主题图集名称*/
        get themeAlias(): string;
        /**获得默认的按钮皮肤*/
        GetButtonSkin(): IGYSkin;
        /**获得默认的GYLinkButton皮肤*/
        GetLinkButtonSkin(): IGYSkin;
        /**获得默认的GYScrollerV皮肤*/
        GetScrollBarSkinV(): IGYSkin;
        /**获得默认的GYScrollerH皮肤*/
        GetScrollBarSkinH(): IGYSkin;
        /**获得默认的List皮肤（这个没用到）*/
        GetListSkin(): IGYSkin;
        /**获得默认的GYTextInput皮肤*/
        GetTextInputSkin(): IGYSkin;
        /**获得默认的TextFormat*/
        GetTextTextFormat(): TextFormat;
        /**获得默认的GYProgressBar皮肤*/
        GetProgressBarSkin(): IGYSkin;
        /**获得默认的GYCheckBox皮肤*/
        GetCheckBoxSkin(): IGYSkin;
        /**获得默认的GYRadioButton皮肤*/
        GetRadioButtonSkin(): IGYSkin;
        /**获得默认的GYSlider皮肤*/
        GetSliderSkin(): IGYSkin;
        /**获得默认的GYTabButton皮肤*/
        GetTabButtonSkin(): IGYSkin;
        /**获得默认的GYCombo的下拉按钮皮肤*/
        GetComboButtonSkin(): IGYSkin;
        /**获得默认的GYCombo的下拉按钮皮肤*/
        GetMenuSkin(): IGYSkin;
        /**获得默认的窗口皮肤*/
        GetWindowSkin(): IGYSkin;
        /**获得默认的窗口皮肤*/
        GetTextAreaSkin(): IGYSkin;
        /**获得默认的s.toolTip*/
        GetToolTip(): GYToolTip;
        /**快速创建一个主题菜单Menu */
        GetMenu(w?: number, h?: number): GYMenu;
        /**快速创建一个主题组合输入菜单ComboBox */
        GetComboBox(w?: number, h?: number): GYComboBox;
        /**初始化主题*/
        initRes(): void;
        /**设置资源*/
        protected setRes(): void;
        getBmp(n: string): egret.Texture;
        commonBtn_upBD: egret.Texture;
        commonBtn_overBD: egret.Texture;
        commonBtn_downBD: egret.Texture;
        commonBtn_disabledBD: egret.Texture;
        commonBtn_selUpBD: egret.Texture;
        commonBtn_selOverBD: egret.Texture;
        commonBtn_selDownBD: egret.Texture;
        commonBtn_selDisabledBD: egret.Texture;
        scaleBtn_upBD: egret.Texture;
        scaleBtn_overBD: egret.Texture;
        scaleBtn_downBD: egret.Texture;
        scaleBtn_disabledBD: egret.Texture;
        scaleBtn_selUpBD: egret.Texture;
        scaleBtn_selOverBD: egret.Texture;
        scaleBtn_selDownBD: egret.Texture;
        scaleBtn_selDisabledBD: egret.Texture;
        check_upBD: egret.Texture;
        check_overBD: egret.Texture;
        check_downBD: egret.Texture;
        check_disabledBD: egret.Texture;
        check_selUpBD: egret.Texture;
        check_selOverBD: egret.Texture;
        check_selDownBD: egret.Texture;
        check_selDisabledBD: egret.Texture;
        radio_upBD: egret.Texture;
        radio_overBD: egret.Texture;
        radio_downBD: egret.Texture;
        radio_disabledBD: egret.Texture;
        radio_selUpBD: egret.Texture;
        radio_selOverBD: egret.Texture;
        radio_selDownBD: egret.Texture;
        radio_selDisabledBD: egret.Texture;
        upArrow_upBD: egret.Texture;
        upArrow_overBD: egret.Texture;
        upArrow_downBD: egret.Texture;
        upArrow_disabledBD: egret.Texture;
        downArrow_upBD: egret.Texture;
        downArrow_overBD: egret.Texture;
        downArrow_downBD: egret.Texture;
        downArrow_disabledBD: egret.Texture;
        rightArrow_upBD: egret.Texture;
        rightArrow_overBD: egret.Texture;
        rightArrow_downBD: egret.Texture;
        rightArrow_disabledBD: egret.Texture;
        leftArrow_upBD: egret.Texture;
        leftArrow_overBD: egret.Texture;
        leftArrow_downBD: egret.Texture;
        leftArrow_disabledBD: egret.Texture;
        sliderBtn_upVBD: egret.Texture;
        sliderBtn_overVBD: egret.Texture;
        sliderBtn_downVBD: egret.Texture;
        sliderBtn_disabledVBD: egret.Texture;
        scrollerBackVBD: egret.Texture;
        sliderBtn_upHBD: egret.Texture;
        sliderBtn_overHBD: egret.Texture;
        sliderBtn_downHBD: egret.Texture;
        sliderBtn_disabledHBD: egret.Texture;
        scrollerBackHBD: egret.Texture;
        tab_upBD: egret.Texture;
        tab_overBD: egret.Texture;
        tab_downBD: egret.Texture;
        tab_disabledBD: egret.Texture;
        tab_selUpBD: egret.Texture;
        tab_selOverBD: egret.Texture;
        tab_selDownBD: egret.Texture;
        tab_selDisabledBD: egret.Texture;
        slider_upBD: egret.Texture;
        slider_overBD: egret.Texture;
        slider_downBD: egret.Texture;
        slider_disabledBD: egret.Texture;
        slider_backBD: egret.Texture;
        slider_progBD: egret.Texture;
        progressBD: egret.Texture;
        progress_backBD: egret.Texture;
        textInputBD: egret.Texture;
        textAreaBD: egret.Texture;
        menuBackBD: egret.Texture;
        windowBackBD: egret.Texture;
        closeBtn_upBD: egret.Texture;
        closeBtn_overBD: egret.Texture;
        closeBtn_downBD: egret.Texture;
        closeBtn_disabledBD: egret.Texture;
        commonBtnSkinVec: egret.Texture[];
        linkBtnSkinVec: TextFormat[];
        scaleBtnSkinVec: egret.Texture[];
        checkBoxSkinVec: egret.Texture[];
        tabBtnSkinVec: egret.Texture[];
        radioBtnSkinVec: egret.Texture[];
        sliderBtnSkinVec: egret.Texture[];
        scrollUpArrowSkinVec: egret.Texture[];
        scrollDownArrowSkinVec: egret.Texture[];
        scrollLeftArrowSkinVec: egret.Texture[];
        scrollRightArrowSkinVec: egret.Texture[];
        scrollSliderVSkinVec: egret.Texture[];
        scrollSliderHSkinVec: egret.Texture[];
        closeBtnVec: egret.Texture[];
        inputSkin: TextInputSkin;
        barTextFormat: TextFormat;
        commonRect: Scale9GridRect;
        smallRect: Scale9GridRect;
        sizeRect3_3_2_2: Scale9GridRect;
        sizeRect20_20_50_20: Scale9GridRect;
        protected toolTip: IGYDisplay;
        protected toolTipMatrix: egret.Matrix;
        disposed: boolean;
        dispose(): void;
    }
}
