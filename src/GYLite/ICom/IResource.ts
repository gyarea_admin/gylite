module GYLite
{
				
							
	/**羔羊组件的可销毁的资源对象*/
	export interface IResource
	{		
		dispose():void;
		disposed:boolean;		
	}
}