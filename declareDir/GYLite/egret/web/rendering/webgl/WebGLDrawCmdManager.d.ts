declare namespace egret.web {
    /**
     * @private
     * draw类型，所有的绘图操作都会缓存在drawData中，每个drawData都是一个drawable对象
     * $renderWebGL方法依据drawable对象的类型，调用不同的绘制方法
     */
    const enum DRAWABLE_TYPE {
        TEXTURE = 0,
        RECT = 1,
        PUSH_MASK = 2,
        POP_MASK = 3,
        BLEND = 4,
        RESIZE_TARGET = 5,
        CLEAR_COLOR = 6,
        ACT_BUFFER = 7,
        ENABLE_SCISSOR = 8,
        DISABLE_SCISSOR = 9,
        SMOOTHING = 10
    }
    interface IDrawData {
        type: number;
        count: number;
        texture: WebGLTexture;
        filter: Filter;
        value: string;
        buffer: WebGLRenderBuffer;
        width: number;
        height: number;
        textureWidth: number;
        textureHeight: number;
        smoothing: boolean;
        x: number;
        y: number;
    }
    /**
     * @private
     * 绘制指令管理器
     * 用来维护drawData数组
     */
    class WebGLDrawCmdManager {
        /**
         * 用于缓存绘制命令的数组
         */
        readonly drawData: IDrawData[];
        drawDataLen: number;
        constructor();
        /**
         * 压入绘制矩形指令
         */
        pushDrawRect(): void;
        /**
         * 压入绘制texture指令
         */
        pushDrawTexture(texture: any, count?: number, filter?: any, textureWidth?: number, textureHeight?: number): void;
        pushChangeSmoothing(texture: WebGLTexture, smoothing: boolean): void;
        /**
         * 压入pushMask指令
         */
        pushPushMask(count?: number): void;
        /**
         * 压入popMask指令
         */
        pushPopMask(count?: number): void;
        /**
         * 压入混色指令
         */
        pushSetBlend(value: string): void;
        pushResize(buffer: WebGLRenderBuffer, width: number, height: number): void;
        pushClearColor(): void;
        /**
         * 压入激活buffer命令
         */
        pushActivateBuffer(buffer: WebGLRenderBuffer): void;
        pushEnableScissor(x: number, y: number, width: number, height: number): void;
        pushDisableScissor(): void;
        /**
         * 清空命令数组
         */
        clear(): void;
    }
}
