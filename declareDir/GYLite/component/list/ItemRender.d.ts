declare module GYLite {
    class ItemRender extends GYUIComponent implements IItemRender {
        protected _selected: boolean;
        protected _owner: IList;
        protected _data: any;
        protected _labelDisplay: GYText;
        protected _itemIndex: number;
        protected _row: number;
        protected _col: number;
        constructor();
        setData(obj: any): void;
        getData(): any;
        set owner(val: IList);
        get owner(): IList;
        set selected(val: boolean);
        get selected(): boolean;
        get itemIndex(): number;
        set itemIndex(value: number);
        get row(): number;
        set row(value: number);
        get col(): number;
        set col(value: number);
    }
}
