/**
@author 迷途小羔羊
2022.10.19
*/
module GYLite
{    
    export class GYMeshNode extends egret.sys.MeshNode implements IUpdate
    {        
        public left:number;
        public right:number;
        public top:number;
        public bottom:number;
        public color:number;
        constructor()
        {
            super();       
            let s= this;
            s.top = s.left = Number.MAX_VALUE;            
            s.bottom = s.right = Number.MIN_VALUE;
        }
        public setRect(l:number,r:number,t:number,b:number):void
        {
            let s= this;
            if(l < s.left)
                s.left = l;
            if(t < s.top)
                s.top = t;
            if(r > s.right)
                s.right = r;
            if(b > s.bottom)
                s.bottom = b;
        }
        public updatePosBound(x:number,y:number):void
        {
            let s= this;
            if(x < s.left)
                s.left = x;
            if(y < s.top)
                s.top = y;
            if(x > s.right)
                s.right = x;
            if(y > s.bottom)
                s.bottom = y;            
        }
        /**添加顶点*/
        public addVertex(x:number,y:number,u:number,v:number):void
        {
            let s= this;
            s.vertices.push(x,y);
            s.uvs.push(u,v);
        }
        /**添加批量顶点*/
        public addVertices(vertices:number[],uvs:number[],indices:number[]):void
        {
            let s= this;
            let ind:number,ind2:number,id:number;
            let i:number,len:number;
            ind = indices.length;
            for(i=0;i<len;++i)
            {
                id = indices[i];
                s.indices[ind] = id;
                ind2 = ind + id * 2;
                s.uvs[ind2] = uvs[id];
                s.uvs[ind2 + 1] = uvs[id + 1];
                s.vertices[ind2] = vertices[id];
                s.vertices[ind2 + 1] = vertices[id + 1];
                ++ind;
            }            
        }
        
        protected _updating:boolean;
        /**是否刷新中（内部使用请勿修改）*/
        public get updating():boolean
        {var s = this;
            return s._updating;
        }

        public set updating(value:boolean)
        {var s = this;
            s._updating = value;
        }
        public updateView():void
		{			
		}
        dispose():void
        {
            if(this.disposed)return;
            this.disposed = true;
        }
		disposed:boolean;
    }   
 }
    