declare module GYLite {
    class GraphicsType {
        static POINT: GraphicsType;
        static LINE: GraphicsType;
        static RECT: GraphicsType;
        static CIRCLE: GraphicsType;
        static ELLIPSE: GraphicsType;
        static TRIANGLE: GraphicsType;
        static POLY: GraphicsType;
        static ROUNDRECT: GraphicsType;
        static CURVE: GraphicsType;
        static CUBIC_CURVE: GraphicsType;
        static ARC: GraphicsType;
        private _id;
        private _name;
        constructor(id: number, name: string);
        get id(): number;
        get name(): string;
    }
}
