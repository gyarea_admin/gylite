class GameSocket extends MsgBase implements GYLite.IResource{
		private _socket:WebSocket;
		private _disposed:boolean;
		private _decoder:any;
		private _heartTick:number;
		private _heartId:number;
		public constructor() {
			super();
			let s =this;
			s.connect();
			s._heartId= -1;
		}
		private disconnect():void
		{
			let s= this;
			s.clearTimeTick();
			if(s._socket)
			{
				s._socket.removeEventListener("open", s.open);			
				s._socket.removeEventListener("error", s.error);
				s._socket.removeEventListener("message", s.message);
				s._socket.close();
				s._socket["owner"] = null;
				s._socket = null;
			}
		}
		private connect():void
		{
			let s= this;
			if(s._socket)
				Log.writeLog("游戏重连...", Log.IMPORTANT);
			else
				Log.writeLog("游戏开始连接...", Log.IMPORTANT);
			s.disconnect();
			s._socket = new WebSocket("ws://43.248.119.160:9898/");
			s._socket.binaryType = "arraybuffer";
			s._socket.addEventListener("open", s.open);			
			s._socket.addEventListener("error", s.error);
			s._socket.addEventListener("message", s.message);
			s._socket["owner"] = s;
		}
		private open(e:Event):void
		{
			let s= <GameSocket>e.currentTarget["owner"];
			s.recvMsg({protocol:GameSocket.SOCKET_STATE,data:{readyState:s._socket.readyState}});
			if(s._socket.readyState == 1)
			{
				Log.writeLog("连接socket成功");
				s._heartTick = Date.now();				
				s._heartId = GYLite.TimeManager.timeOut(s.heartTick,s, 30000);
			}			
		}		
		private error(e:Event):void
		{
			Log.writeLog("连接端口",Log.ERROR);
			let s= <GameSocket>e.currentTarget["owner"];
			s.disconnect();
		}
		private message(e:MessageEvent):void
		{	
			let s = <GameSocket>e.currentTarget["owner"];
			let buffer:ArrayBuffer;			
			let contentPos:number,contentLength:number;	
			let obj:any;
			let protocol:number;
			buffer = e.data;
			obj = s.convertToObject(buffer);
			protocol = obj.op;			
			if(s._dict[protocol])
				s.recvMsg({protocol:protocol,data:obj.body});
			
		}		
		public get disposed():boolean
		{
			return this._disposed;
		}
		public dispose():void
		{
			let s= this;
			if(s._disposed)return;
			s._disposed = true;
			s.disconnect();
		}

		public sendMsg(protocol: string, data: any = null, logLevel:number=Log.VERBOSE): void {
			let s = this;
			super.sendMsg(protocol,data,logLevel);
			let bytes:GYLite.GYByteArray;
			let contentBytes:GYLite.GYByteArray;
			let content:string;
			// contentBytes = new GYLite.GYByteArray;			
			// content = JSON.stringify(data);
			// contentBytes.writeUTFBytes(content);
            // contentBytes.position = 0;
			bytes = new GYLite.GYByteArray;            
            // bytes.writeByte(0x82);
            bytes.writeByte(1);            
            // bytes.writeUnsignedInt(1)
			// bytes.writeUnsignedInt(contentBytes.length)			
			// bytes.writeUnsignedInt(Number(protocol));			
			// bytes.writeUTFBytes(content);
            // bytes.writeBytes(contentBytes,0,contentBytes.length);
			bytes.position = 0;
			s._socket.send(bytes.buffer);
		}
		protected recvMsg(data: any): void {
			let s = this;			
			super.recvMsg(data);
		}	
		protected heartTick():void
		{
			let s = this;
			if(Date.now() - s._heartTick > GameSocket.RECONNECT_TIME)//超时重连
			{				
				s.connect();
				return;
			}
			s._heartId = GYLite.TimeManager.timeOut(s.heartTick,s, 30000);
			if(s._socket)
				s.sendMsg(GameSocket.WS_OP_HEARTBEAT, {});
		}	
		private clearTimeTick():void
		{
			let s= this;
			if(s._heartId > -1)
			{
				GYLite.TimeManager.unTimeOut(s._heartId,s.heartTick,s);
				s._heartId = -1;
			}
		}
        private convertToObject(buffer:ArrayBuffer):void
        {
            let obj:any;
            return obj;
        }
		
		public static SOCKET_STATE:string = "0";
		public static RECONNECT_TIME:number = 40000;

		/**操作码-心跳发送*/public static WS_OP_HEARTBEAT:string="2";
		/**操作码-心跳回复*/public static WS_OP_HEARTBEAT_REPLY:string="3";
		/**操作码-信息接收*/public static WS_OP_MESSAGE:string="5";
		/**操作码-身份验证*/public static WS_OP_USER_AUTHENTICATION:string="7";
		/**操作码-连接成功*/public static WS_OP_CONNECT_SUCCESS:string="8";
		/**头部大小*/public static WS_PACKAGE_HEADER_TOTAL_LENGTH:number=16;
		/**包体大小偏移位*/public static WS_PACKAGE_OFFSET:number=0;
		/**头部偏移位*/public static WS_HEADER_OFFSET:number=4;
		/**版本偏移位*/public static WS_VERSION_OFFSET:number=6;
		/**操作码偏移位*/public static WS_OPERATION_OFFSET:number=8;
		/**序号偏移位*/public static WS_SEQUENCE_OFFSET:number=12;
		/**协议版本*/public static WS_BODY_PROTOCOL_VERSION_NORMAL:number=0;
		/**解压器版本*/public static WS_BODY_PROTOCOL_VERSION_BROTLI:number=3;
		/**默认版本号*/public static WS_HEADER_DEFAULT_VERSION:number=1;
		/**默认操作码*/public static WS_HEADER_DEFAULT_OPERATION:number=1;
		/**默认头部序列*/public static WS_HEADER_DEFAULT_SEQUENCE:number=1;
		/**验证成功*/public static WS_AUTH_OK:number=0;
		/**验证失败*/public static WS_AUTH_TOKEN_ERROR:number=-101;

		public static wsBinaryHeaderList:any[] = [
			{key:"headerLen",offset:4,bytes:2},
			{key:"ver",offset:6,bytes:2},
			{key:"op",offset:8,bytes:4},
			{key:"seq",offset:12,bytes:4}
		];


		/**收到弹幕*/public static CMD_DANMU_MSG:string = "DANMU_MSG"
		
	}
