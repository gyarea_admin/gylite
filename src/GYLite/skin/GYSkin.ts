module GYLite
{
		
	export class GYSkin implements IGYSkin
	{
		protected _hostComponent:GYSprite;
		public constructor()
		{var s = this;
		}
		
		public set hostComponent(val:GYSprite)
		{var s = this;
			s._hostComponent = val;
		}
		
		public get hostComponent():GYSprite
		{var s = this;
			return s._hostComponent;
		}
		
		public clone():IGYSkin
		{var s = this;
			return new GYSkin;
		}
		
		public copy(val:IGYSkin):void
		{var s = this;
		}
		
		public release():void
		{var s = this;
		}
	}
}