class Log {
	public constructor() {
		
	}
	public static LONG_LOADTIME:number = 100;
	public static LONG_UNCOMPRESSTIME:number = 200;
	public static LONG_EVALTIME:number = 50;
	private static _startTime:number;
	public static ERROR:number = 1;
	public static WARN:number = (1<<1);
	public static IMPORTANT:number = (1<<2);
	public static INFO:number = (1<<3);
	public static VERBOSE:number = (1<<4);
	public static uploadLevel:number = 1+2+4;
	public static printLevel:number = 1+2+4+8;
	public static userDataInited:boolean;
	private static _date:Date = new Date;
	private static logsArr:string[] = [];
	private static _reportCloseCount:number=0;
	private static _errorCount:number=0;
	private static _reportTime:number;
	public static init():void
	{
		Log.printLevel = 1+2+4+8+16;
			
		if (!window['vConsole'])
		{
			window.onerror = function(message, source, lineno, colno, error):void{
				var str:string|Event = message;
				++Log._errorCount;
				source && (str += "\n" + source.replace(location.origin, "")),
				(lineno || colno) && (str += ":" + lineno + ":" + colno)
				str += "\n" + (error?error.stack:"no Info!");
				str = "[" + Log._date.toUTCString() + "]" + str;
				//后面做堆栈报错上报	
				// Log.reportLog("ERROR_"+str);
			}
		}			
	}
	public static writeLog(content:string, level:number = 8, upload:boolean=false):void
	{
		Log._date.setTime(Date.now());
		content = "[" + Log._date.toUTCString() + "-" + egret.getTimer() + "]" + content;
		if(level == Log.ERROR)
		{
			if((Log.printLevel & level) > 0)
				console.error(content);
		}
		else if(level == Log.WARN)
		{
			if((Log.printLevel & level) > 0)
				console.warn(content);
		}
		else if((Log.printLevel & level) > 0)
			console.log(content);
		//后面做打印信息上报		
		if((Log.uploadLevel & level) > 0 || upload)
		{
			
		}
	}

	
	public static readProgressLog(progressInfo:any[]):string
	{
		let i:number,len:number;
		let str:string = "";
		let obj:any;
		len = progressInfo.length;
		for(i=0;i<len;++i)
		{
			obj = progressInfo[i];
			str += "state:" + obj.state + "  status:" + obj.status + "  time:" + obj.time + "\n";
		}
		return str;
	}
	/**打印加载详情*/
	public static loadLog(l:GYLite.LoadInfo):void
	{
		let t:number = Date.now() - l.startTime;
		if(t > Log.LONG_LOADTIME)
		{		
			let str:string;
			str = Log.readProgressLog(l.progressInfo);
			Log.writeLog("TIME_当前加载耗时过长："+l.path+" -> " + t + "ms\n详情："+str, Log.IMPORTANT);
		}
		else
			Log.writeLog("TIME_加载完成："+l.path+" -> "+t+"ms", Log.IMPORTANT);
	}
	public static uncompressLog(l:GYLite.CompressLoadInfo,key:string=""):void
	{
		let t:number = Date.now() - l.startTime;
		let i:number,len:number;
		let str:string = "";
		let obj:any;
		len = l.progressInfo.length;
		obj = l.progressInfo[0];
		str += "文件数量:" + obj.length + "  版本:" + obj.version + "  大小:" + obj.size + "\n";
		if(t > Log.LONG_UNCOMPRESSTIME)
		{			
			for(i=1;i<len;++i)
			{
				obj = l.progressInfo[i];
				str += "compress:" + obj.compress + "  key:" + obj.key + "  time:" + obj.time + "\n";
			}		
			Log.writeLog("TIME_解压耗时过长："+key+" -> "+ t + "ms\n详情："+str, Log.IMPORTANT);
		}
		else
			Log.writeLog("TIME_解压完成："+key+" -> " + t + "ms\n详情：" + str, Log.IMPORTANT);
	}
	public static evalLog(key:string):void
	{
		let t:number = Date.now() - Log._startTime;
		if(t > Log.LONG_EVALTIME)
			Log.writeLog("TIME_解析耗时过长："+key+" -> "+t+"ms", Log.IMPORTANT);
		else
			Log.writeLog("TIME_解析完成："+key+" -> "+t+"ms", Log.IMPORTANT);
	}
	public static recordTime():void
	{
		Log._startTime = Date.now();
	}
}