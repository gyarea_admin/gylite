declare namespace egret {
    /**
     * TextFieldInputType class is an enumeration of constant value used in setting the inputType property of the TextField class.
     * @version Egret 3.1.2
     * @platform Web,Native
     * @language en_US
     */
    /**
     * TextFieldInputType 类是在设置 TextField 类的 inputType 属性时使用的常数值的枚举。
     * @version Egret 3.1.2
     * @platform Web,Native
     * @language zh_CN
     */
    class TextFieldInputType {
        /**
         * The default
         * @version Egret 3.1.2
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 默认 input 类型
         * @version Egret 3.1.2
         * @platform Web,Native
         * @language zh_CN
         */
        static TEXT: string;
        /**
         * Telephone Number Inputs
         * @version Egret 3.1.2
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 电话号码 input 类型
         * @version Egret 3.1.2
         * @platform Web,Native
         * @language zh_CN
         */
        static TEL: string;
        /**
         * Password Inputs
         * @version Egret 3.1.2
         * @platform Web,Native
         * @language en_US
         */
        /**
         * password 类型
         * @version Egret 3.1.2
         * @platform Web,Native
         * @language zh_CN
         */
        static PASSWORD: string;
    }
}
