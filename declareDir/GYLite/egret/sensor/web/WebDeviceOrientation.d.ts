declare namespace egret.web {
    /**
     * @private
     */
    class WebDeviceOrientation extends EventDispatcher implements DeviceOrientation {
        /**
         * @private
         *
         */
        start(): void;
        /**
         * @private
         *
         */
        stop(): void;
        /**
         * @private
         */
        protected onChange: (e: DeviceOrientationEvent) => void;
    }
}
