module GYLite
{		
	export class TitleWindowSkin implements IWindowSkin
	{
		protected _backgroundImage:GYScaleSprite;
		protected _icon:GYImage;
		protected _title:GYText;
		protected _closeBtn:GYButton;
		protected _hostComponent:GYSprite;
		public constructor(background:egret.Texture = null, scale9GridRect:Scale9GridRect = null,closeSkin:IButtonSkin=null)
		{var s = this;
			s._backgroundImage = new GYScaleSprite(background, scale9GridRect);
			s._backgroundImage.percentHeight = s._backgroundImage.percentWidth = 1;
			s._closeBtn = new GYButton(closeSkin);
			s._closeBtn.y = 2;
			s._closeBtn.right = 10;
		}
		
		public set hostComponent(val:GYSprite)
		{var s = this;
			if(val==s._hostComponent)
				return;
			if(s._hostComponent)
			{
				s._hostComponent.removeElement(s._backgroundImage);
				s._hostComponent.removeElement(s._closeBtn);
			}
			s._hostComponent=val;
			if(s._hostComponent)
			{
				s._hostComponent.addElementAt(s._backgroundImage, 0);
				s._hostComponent.addElement(s._closeBtn);
			}
		}
		
		public get hostComponent():GYSprite
		{var s = this;
			return null;
		}
		public setTitle(val:string)
		{var s = this;
			if(s._title == null)
			{
				s._title = new GYText;
				s._hostComponent.addElementAt(s._title, 1);
			}
			s._title.text = val;
		}
		public get title():GYText
		{var s = this;
			return s._title;
		}
		public setIcon(val:egret.Texture):void
		{var s = this;
			if(s._icon == null)
			{
				s._icon = new GYImage;
				s._hostComponent.addElement(s._icon);
			}
			s._icon.source = val;
		}
		public get icon():GYImage
		{var s = this;
			return s._icon;
		}
		public clone():IGYSkin
		{var s = this;
			return new TitleWindowSkin(s._backgroundImage.bitmapData,s._backgroundImage.scale9GridRect,s.closeBtn.skin.clone() as IButtonSkin);
		}
		public copy(val:IGYSkin):void{var s = this;}
		public release():void
		{var s = this;
			s._backgroundImage.bitmapData = null;
			if(s._icon)s._icon.source = null;
		}

		public set icon(value:GYImage)
		{var s = this;
			s._icon = value;
		}

		public set title(value:GYText)
		{var s = this;
			s._title = value;
		}

		public get closeBtn():GYButton
		{var s = this;
			return s._closeBtn;
		}

		public get backgoundImage():GYScaleSprite
		{var s = this;
			return s._backgroundImage;
		}


	}
}