declare module GYLite {
    class GYTextElement extends BatchElement {
        /**当前字体格式*/ $format: Format;
        /**字符*/ $char: string;
        /**字体css字符串*/ $font: string;
        /**加边距之后的居中偏移量x*/ $canvasWidthOffset: number;
        /**加边距之后的居中偏移量y*/ $canvasHeightOffset: number;
        /**描边值*/ $stroke: number;
        /**两倍描边值*/ $stroke2: number;
        /**文本测量后的宽度*/ private $measureWidth;
        /**文本测量后的高度*/ $measureHeight: number;
        constructor();
        reset(char?: string, font?: string, element?: GYTextBase, format?: any): void;
        dispose(): void;
    }
    class Format {
        /**字体颜色*/ textColor: number;
        /**描边颜色*/ strokeColor: number;
        /**描边大小*/ stroke: number;
        /**阴影颜色*/ shadowColor: number;
        /**阴影偏移X*/ shadowOffsetX: number;
        /**阴影偏移Y*/ shadowOffsetY: number;
        /**阴影大小*/ shadowBlur: number;
        /**字号*/ size: number;
        /**加粗*/ bold: boolean;
        /**倾斜*/ italic: boolean;
        /**字体*/ fontFamily: string;
        /**
         * @param element 文本组件
         * @param format 组件的附加文本格式信息
        */
        constructor();
        clear(): void;
        reset(element: GYTextBase, format: any): Format;
    }
}
