module GYLite
{																																	
										
	export class GYSkinTheme implements ISkinTheme
	{
		/**主题名称，唯一标识*/
		public get themeName():string
		{
			let s = this;
			return "com";
		}
		/**主题图集名称*/	
		public get themeAlias():string
		{
			return null;
		}
		/**获得默认的按钮皮肤*/
		public GetButtonSkin():IGYSkin
		{
			let s = this;
			return new ButtonSkin(s.scaleBtnSkinVec,s.commonRect);
		}
		/**获得默认的GYLinkButton皮肤*/
		public GetLinkButtonSkin():IGYSkin
		{
			let s = this;
			return new LinkButtonSkin(s.linkBtnSkinVec);
		}
		/**获得默认的GYScrollerV皮肤*/
		public GetScrollBarSkinV():IGYSkin
		{
			let s = this;
			return new ScrollerSkin(new ButtonSkin(s.scrollSliderVSkinVec,s.commonRect),s.scrollerBackVBD,new ButtonSkin(s.scrollUpArrowSkinVec),new ButtonSkin(s.scrollDownArrowSkinVec));
		}
		/**获得默认的GYScrollerH皮肤*/
		public GetScrollBarSkinH():IGYSkin
		{
			let s = this;
			return new ScrollerSkin(new ButtonSkin(s.scrollSliderHSkinVec,s.commonRect),s.scrollerBackHBD,new ButtonSkin(s.scrollLeftArrowSkinVec),new ButtonSkin(s.scrollRightArrowSkinVec));
		}
		/**获得默认的List皮肤（这个没用到）*/
		public GetListSkin():IGYSkin
		{
			let s = this;
			return null;
		}
		/**获得默认的GYTextInput皮肤*/
		public GetTextInputSkin():IGYSkin
		{
			let s = this;
			return new TextInputSkin(s.textInputBD,s.commonRect);
		}
		/**获得默认的TextFormat*/
		public GetTextTextFormat():TextFormat
		{
			let s = this;
			return new TextFormat(GYTextBase.defualtSysFont, 12, 0, false, false, false,null,null,"left", null,null,0,5);
		}
		/**获得默认的GYProgressBar皮肤*/
		public GetProgressBarSkin():IGYSkin
		{
			let s = this;
			var back:GYScaleSprite = new GYScaleSprite(s.progress_backBD, s.commonRect);
			var bar:GYScaleSprite = new GYScaleSprite(s.progressBD,s.commonRect);
			return new ProgressBarSkin(back, bar, s.barTextFormat);
		}
		/**获得默认的GYCheckBox皮肤*/
		public GetCheckBoxSkin():IGYSkin
		{
			let s = this;
			return new CheckBoxSkin(s.checkBoxSkinVec);
		}
		/**获得默认的GYRadioButton皮肤*/
		public GetRadioButtonSkin():IGYSkin
		{
			let s = this;
			return new RadioButtonSkin(s.radioBtnSkinVec);
		}
		/**获得默认的GYSlider皮肤*/
		public GetSliderSkin():IGYSkin
		{
			let s = this;
			var back:GYScaleSprite = new GYScaleSprite(s.slider_backBD, s.sizeRect3_3_2_2);
			var bar:GYScaleSprite = new GYScaleSprite(s.slider_progBD,s.sizeRect3_3_2_2);
			var buttonSkin:IButtonSkin = new ButtonSkin(s.sliderBtnSkinVec);
			return new SliderSkin(back, bar, buttonSkin);
		}
		/**获得默认的GYTabButton皮肤*/
		public GetTabButtonSkin():IGYSkin
		{
			let s = this;
			return new ButtonSkin(s.tabBtnSkinVec, s.commonRect);
		}
		
		/**获得默认的GYCombo的下拉按钮皮肤*/
		public GetComboButtonSkin():IGYSkin
		{
			let s = this;
			return new ButtonSkin(s.scrollDownArrowSkinVec);
		}
		
		/**获得默认的GYCombo的下拉按钮皮肤*/
		public GetMenuSkin():IGYSkin
		{
			let s = this;
			let list:GYLite.GYListV=new GYListV(92);			
			list.viewChangeCallParent = true;			
			return new MenuSkin(s.menuBackBD,s.commonRect,list);
		}
		/**获得默认的窗口皮肤*/
		public GetWindowSkin():IGYSkin
		{
			let s = this;
			return new TitleWindowSkin(s.windowBackBD,s.sizeRect20_20_50_20, new ButtonSkin(s.closeBtnVec));
		}
		/**获得默认的窗口皮肤*/
		public GetTextAreaSkin():IGYSkin
		{
			let s = this;
			return new TextAreaSkin(s.textAreaBD,s.commonRect);
		}
		
		/**获得默认的s.toolTip*/
		public GetToolTip():GYToolTip
		{
			return GYToolTip.getInstance();
		}
		/**快速创建一个主题菜单Menu */
		public GetMenu(w:number = 50,h:number = 92):GYMenu
		{
			let s = this;
			var gyMenu:GYMenu;
			let menuSkin:GYLite.MenuSkin;
			let list:GYLite.GYListV;
			list = new GYLite.GYListV(h);
			menuSkin = new GYLite.MenuSkin(s.menuBackBD, s.commonRect, list);
			gyMenu=new GYMenu(menuSkin);			
			gyMenu.list.width = w;			
			gyMenu.list.paddingLeft = 5;
			gyMenu.list.paddingTop = 7;
			gyMenu.list.paddingRight = 4;
			gyMenu.list.paddingBottom = 5;
			gyMenu.vScroller.right = 10;
			gyMenu.vScroller.height = h;
			return gyMenu;
		}
		/**快速创建一个主题组合输入菜单ComboBox */
		public GetComboBox(w:number = 50,h:number = 92):GYComboBox
		{
			let s = this;
			var gyMenu:GYMenu;
			var gyComboBox:GYComboBox;
			gyMenu = s.GetMenu(w,h);
			gyMenu.list.canSelectNum = 1;
			
			var txt:GYTextInput=new GYTextInput(null,false);			
			txt.paddingLeft = 5;			
			txt.width=gyMenu.list.width + 16;
			txt.height=20;
			txt.textInput.color = 0xff0000;
			txt.paddingTop = 6;
			gyComboBox = new GYComboBox();
			gyComboBox.textInput = txt;
			gyComboBox.menu = gyMenu;
			gyComboBox.comboBoxBtn = new GYButton(s.GetComboButtonSkin());
			txt.paddingRight = gyComboBox.comboBoxBtn.width;
			return gyComboBox;
		}
		/**初始化主题*/
		public initRes():void
		{
			let s = this;
			if(s.commonBtn_upBD)
				return;

			s.linkBtnSkinVec=new Array<TextFormat>();
			s.linkBtnSkinVec.push(new TextFormat(GYTextBase.defualtSysFont,12,0xff0000,false,null,true,null,null,"center"),
				new TextFormat(GYTextBase.defualtSysFont,12,0x00ff00,false,null,true,null,null,"center"),
				new TextFormat(GYTextBase.defualtSysFont,12,0xffff00,false,null,true,null,null,"center"),
				new TextFormat(GYTextBase.defualtSysFont,12,0x666666,false,null,true,null,null,"center"),
				new TextFormat(GYTextBase.defualtSysFont,12,0xffffff,true,null,true,null,null,"center"),
				new TextFormat(GYTextBase.defualtSysFont,12,0x00ff00,true,null,true,null,null,"center"),
				new TextFormat(GYTextBase.defualtSysFont,12,0xffff00,true,null,true,null,null,"center"));
			s.barTextFormat=new TextFormat(GYTextBase.defualtSysFont,12,0xff,true,null,null,null,null,"center");
			s.setRes();			
			
			s.commonBtnSkinVec=new Array<egret.Texture>();			
			s.scaleBtnSkinVec=new Array<egret.Texture>();
			s.checkBoxSkinVec=new Array<egret.Texture>();
			s.tabBtnSkinVec=new Array<egret.Texture>();
			s.radioBtnSkinVec=new Array<egret.Texture>();
			s.sliderBtnSkinVec=new Array<egret.Texture>();
			s.scrollUpArrowSkinVec =new Array<egret.Texture>();
			s.scrollDownArrowSkinVec =new Array<egret.Texture>();
			s.scrollLeftArrowSkinVec =new Array<egret.Texture>();
			s.scrollRightArrowSkinVec =new Array<egret.Texture>();
			s.scrollSliderVSkinVec =new Array<egret.Texture>();
			s.scrollSliderHSkinVec =new Array<egret.Texture>();
			s.closeBtnVec = new Array<egret.Texture>();
			
			s.tabBtnSkinVec.push(s.tab_upBD,s.tab_overBD,s.tab_downBD,s.tab_disabledBD,s.tab_selUpBD,s.tab_selOverBD,s.tab_selDownBD,s.tab_selDisabledBD);
			s.commonBtnSkinVec.push(s.commonBtn_upBD,s.commonBtn_overBD,s.commonBtn_downBD,s.commonBtn_disabledBD,s.commonBtn_selUpBD,s.commonBtn_selOverBD,s.commonBtn_selDownBD,s.commonBtn_selDisabledBD);
			s.scaleBtnSkinVec.push(s.scaleBtn_upBD,s.scaleBtn_overBD,s.scaleBtn_downBD,s.scaleBtn_disabledBD,s.scaleBtn_selUpBD,s.scaleBtn_selOverBD,s.scaleBtn_selDownBD,s.scaleBtn_selDisabledBD);
			s.checkBoxSkinVec.push(s.check_upBD,s.check_overBD,s.check_downBD,s.check_disabledBD,s.check_selUpBD,s.check_selOverBD,s.check_selDownBD,s.check_selDisabledBD);
			s.radioBtnSkinVec.push(s.radio_upBD,s.radio_overBD,s.radio_downBD,s.radio_disabledBD,s.radio_selUpBD,s.radio_selOverBD,s.radio_selDownBD,s.radio_selDisabledBD);
			s.sliderBtnSkinVec.push(s.slider_upBD,s.slider_overBD,s.slider_downBD,s.slider_disabledBD);
			s.scrollUpArrowSkinVec.push(s.upArrow_upBD,s.upArrow_overBD,s.upArrow_downBD,s.upArrow_disabledBD);
			s.scrollDownArrowSkinVec.push(s.downArrow_upBD,s.downArrow_overBD,s.downArrow_downBD,s.downArrow_disabledBD);
			s.scrollLeftArrowSkinVec.push(s.leftArrow_upBD,s.leftArrow_overBD,s.leftArrow_downBD,s.leftArrow_disabledBD);
			s.scrollRightArrowSkinVec.push(s.rightArrow_upBD,s.rightArrow_overBD,s.rightArrow_downBD,s.rightArrow_disabledBD);
			s.scrollSliderVSkinVec.push(s.sliderBtn_upVBD,s.sliderBtn_overVBD,s.sliderBtn_downVBD,s.sliderBtn_disabledVBD);
			s.scrollSliderHSkinVec.push(s.sliderBtn_upHBD,s.sliderBtn_overHBD,s.sliderBtn_downHBD,s.sliderBtn_disabledHBD);
			s.closeBtnVec.push(s.closeBtn_upBD,s.closeBtn_overBD,s.closeBtn_downBD,s.closeBtn_disabledBD);
			
			if(s.tab_upBD == null)throw(new Error("tab_upBD主题皮肤为null"));
			if(s.commonBtn_upBD == null)throw(new Error("commonBtn_upBD主题皮肤为null"));
			if(s.scaleBtn_upBD == null)throw(new Error("scaleBtn_upBD主题皮肤为null"));
			if(s.check_upBD == null)throw(new Error("check_upBD主题皮肤为null"));
			if(s.radio_upBD == null)throw(new Error("radio_upBD主题皮肤为null"));
			if(s.slider_upBD == null)throw(new Error("slider_upBD主题皮肤为null"));
			if(s.upArrow_upBD == null)throw(new Error("upArrow_upBD主题皮肤为null"));
			if(s.downArrow_upBD == null)throw(new Error("downArrow_upBD主题皮肤为null"));
			if(s.leftArrow_upBD == null)throw(new Error("leftArrow_upBD主题皮肤为null"));
			if(s.rightArrow_upBD == null)throw(new Error("rightArrow_upBD主题皮肤为null"));
			if(s.sliderBtn_upVBD == null)throw(new Error("sliderBtn_upVBD主题皮肤为null"));
			if(s.sliderBtn_upHBD == null)throw(new Error("sliderBtn_upHBD主题皮肤为null"));
			if(s.closeBtn_upBD == null)throw(new Error("closeBtn_upBD主题皮肤为null"));
		}
		/**设置资源*/
		protected setRes():void
		{
			let s = this;
			var testShape:GYSprite;
			var g:egret.Graphics;
			//箭头按钮
			//上箭头
			testShape = new GYSprite;g = testShape.graphics;
			g.beginFill(0xcccccc);
			g.drawRect(0,0,16,16);
			g.beginFill(0xf9f9f9);
			g.drawRect(1,1,14,14);			
			g.endFill();
			g.beginFill(0);
			g.lineStyle(1,0,1,true,"normal",egret.CapsStyle.SQUARE,egret.JointStyle.MITER);
			g.moveTo(8, 6);
			g.lineTo(6,9);
			g.lineTo(10,9);
			g.lineTo(8,6);			
			g.endFill();
			s.upArrow_upBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.upArrow_upBD).draw(testShape);
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x3c7fb1);
			g.drawRect(0,0,16,16);
			g.beginFill(0xe3f4fc);
			g.drawRect(1,1,14,14);			
			g.endFill();
			g.beginFill(0);
			g.lineStyle(1,0,1,true,"normal",egret.CapsStyle.SQUARE,egret.JointStyle.MITER);
			g.moveTo(8, 6);
			g.lineTo(6,9);
			g.lineTo(10,9);
			g.lineTo(8,6);			
			g.endFill();
			s.upArrow_overBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.upArrow_overBD).draw(testShape);
			s.upArrow_downBD = s.upArrow_upBD;
			s.upArrow_disabledBD = s.upArrow_upBD;
			
			//下箭头
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xcccccc);
			g.drawRect(0,0,16,16);
			g.beginFill(0xf9f9f9);
			g.drawRect(1,1,14,14);			
			g.endFill();
			g.beginFill(0x333333);
			g.lineStyle(1,0,1,true,"normal",egret.CapsStyle.SQUARE,egret.JointStyle.MITER);
			g.moveTo(6, 6);
			g.lineTo(10,6);
			g.lineTo(8,9);
			g.lineTo(6,6);				
			g.endFill();
			s.downArrow_upBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.downArrow_upBD).draw(testShape);
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x3c7fb1);
			g.drawRect(0,0,16,16);
			g.beginFill(0xe3f4fc);
			g.drawRect(1,1,14,14);			
			g.endFill();
			g.beginFill(0x333333);
			g.lineStyle(1,0,1,true,"normal",egret.CapsStyle.SQUARE,egret.JointStyle.MITER);
			g.moveTo(6, 6);
			g.lineTo(10,6);
			g.lineTo(8,9);
			g.lineTo(6,6);				
			g.endFill();
			s.downArrow_overBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.downArrow_overBD).draw(testShape);
			s.downArrow_downBD = s.downArrow_upBD;
			s.downArrow_disabledBD = s.downArrow_upBD;
			//左箭头
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xcccccc);
			g.drawRect(0,0,16,16);
			g.beginFill(0xf9f9f9);
			g.drawRect(1,1,14,14);			
			g.endFill();
			g.beginFill(0);
			g.lineStyle(1,0,1,true,"normal",egret.CapsStyle.SQUARE,egret.JointStyle.MITER);
			g.moveTo(6, 8);
			g.lineTo(9, 6);
			g.lineTo(9, 10);
			g.lineTo(6, 8);			
			g.endFill();
			s.leftArrow_upBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.leftArrow_upBD).draw(testShape);
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x3c7fb1);
			g.drawRect(0,0,16,16);
			g.beginFill(0xe3f4fc);
			g.drawRect(1,1,14,14);			
			g.endFill();
			g.beginFill(0);
			g.lineStyle(1,0,1,true,"normal",egret.CapsStyle.SQUARE,egret.JointStyle.MITER);
			g.moveTo(6, 8);
			g.lineTo(9, 6);
			g.lineTo(9, 10);
			g.lineTo(6, 8);			
			g.endFill();
			s.leftArrow_overBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.leftArrow_overBD).draw(testShape);
			s.leftArrow_downBD = s.leftArrow_upBD;
			s.leftArrow_disabledBD = s.leftArrow_upBD;
			//右箭头
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xcccccc);
			g.drawRect(0,0,16,16);
			g.beginFill(0xf9f9f9);
			g.drawRect(1,1,14,14);			
			g.endFill();
			g.beginFill(0);
			g.lineStyle(1,0,1,true,"normal",egret.CapsStyle.SQUARE,egret.JointStyle.MITER);
			g.moveTo(9, 8);
			g.lineTo(6, 6);
			g.lineTo(6, 10);
			g.lineTo(9, 8);		
			g.endFill();
			s.rightArrow_upBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.rightArrow_upBD).draw(testShape);
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x3c7fb1);
			g.drawRect(0,0,16,16);
			g.beginFill(0xe3f4fc);
			g.drawRect(1,1,14,14);			
			g.endFill();
			g.beginFill(0);
			g.lineStyle(1,0,1,true,"normal",egret.CapsStyle.SQUARE,egret.JointStyle.MITER);
			g.moveTo(9, 8);
			g.lineTo(6, 6);
			g.lineTo(6, 10);
			g.lineTo(9, 8);
			g.endFill();
			s.rightArrow_overBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.rightArrow_overBD).draw(testShape);
			s.rightArrow_downBD = s.rightArrow_upBD;
			s.rightArrow_disabledBD = s.rightArrow_upBD;
			//纵向滑块
			testShape = new GYSprite;g = testShape.graphics;
			var matrix:egret.Matrix = new egret.Matrix;
			matrix.createGradientBox(14,14);
			g.clear();
			g.beginFill(0x999999);
			g.drawRect(0,0,16,16);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xffffff, 0xf6f6f6],[1,1],[0,255],matrix);
			g.drawRect(1,1,14,14);			
			g.endFill();
			s.sliderBtn_upVBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.sliderBtn_upVBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x999999);
			g.drawRect(0,0,16,16);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xe3f4fc, 0xa4d5ef],[1,1],[0,255],matrix);
			g.drawRect(1,1,14,14);			
			g.endFill();
			s.sliderBtn_overVBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.sliderBtn_overVBD).draw(testShape);
			s.sliderBtn_downVBD = s.sliderBtn_upVBD;
			s.sliderBtn_disabledVBD = s.sliderBtn_upVBD;
			//横向滑块
			testShape = new GYSprite;g = testShape.graphics;
			matrix.createGradientBox(14,14,Math.PI/2);
			g.clear();
			g.beginFill(0x999999);
			g.drawRect(0,0,16,16);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xffffff, 0xf6f6f6],[1,1],[0,255],matrix);
			g.drawRect(1,1,14,14);			
			g.endFill();
			s.sliderBtn_upHBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.sliderBtn_upHBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x999999);
			g.drawRect(0,0,16,16);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xe3f4fc, 0xa4d5ef],[1,1],[0,255],matrix);
			g.drawRect(1,1,14,14);			
			g.endFill();
			s.sliderBtn_overHBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.sliderBtn_overHBD).draw(testShape);
			s.sliderBtn_downHBD = s.sliderBtn_upHBD;
			s.sliderBtn_disabledHBD = s.sliderBtn_upHBD;
			//滑块背景
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xaaaaaa);
			g.drawRect(0,0,16,16);
			g.beginFill(0x999999);
			g.drawRect(1,1,14,14);			
			g.endFill();
			s.scrollerBackHBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.scrollerBackHBD).draw(testShape);
			s.scrollerBackVBD = s.scrollerBackHBD;
			//普通按钮
			testShape = new GYSprite;g = testShape.graphics;
			matrix.createGradientBox(14,14,Math.PI/2);
			g.clear();
			g.beginFill(0x999999);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xffffff, 0xf9f9f9],[1,1],[0,255],matrix);
			g.drawRoundRect(1,1,14,14,5,5);			
			g.endFill();
			s.commonBtn_upBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.commonBtn_upBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x999999);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xe3f4fc, 0xa4d5ef],[1,1],[0,255],matrix);
			g.drawRoundRect(1,1,14,14,5,5);
			g.endFill();
			s.commonBtn_overBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.commonBtn_overBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x333333);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginGradientFill(egret.GradientType.LINEAR,[0x666666, 0x999999],[1,1],[0,255],matrix);
			g.drawRoundRect(1,1,14,14,5,5);			
			g.endFill();
			s.commonBtn_downBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.commonBtn_downBD).draw(testShape);
			s.commonBtn_disabledBD = s.commonBtn_upBD;
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x996666);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xffbbbb, 0xddaaaa],[1,1],[0,255],matrix);
			g.drawRoundRect(1,1,14,14,5,5);			
			g.endFill();
			s.commonBtn_selUpBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.commonBtn_selUpBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x996666);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xffcccc, 0xddbbbb],[1,1],[0,255],matrix);
			g.drawRoundRect(1,1,14,14,5,5);			
			g.endFill();
			s.commonBtn_selOverBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.commonBtn_selOverBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x996666);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xffbbbb, 0xddaaaa],[1,1],[0,255],matrix);
			g.drawRoundRect(1,1,14,14,5,5);			
			g.endFill();
			s.commonBtn_selDownBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.commonBtn_selDownBD).draw(testShape);
			s.commonBtn_selDisabledBD = s.commonBtn_selDownBD;
			
			//输入框背景
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0x333333);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginFill(0x444444);
			g.drawRoundRect(1,1,14,14,5,5);			
			g.endFill();
			s.textInputBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.textInputBD).draw(testShape);
			
			//复选框
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xcccccc);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginFill(0xf0f0f0);
			g.drawRoundRect(1,1,14,14,5,5);			
			g.endFill();
			s.check_upBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.check_upBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xdddddd);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginFill(0xffffff);
			g.drawRoundRect(1,1,14,14,5,5);			
			g.endFill();
			s.check_overBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.check_overBD).draw(testShape);
			s.check_downBD = s.check_upBD;
			s.check_disabledBD = s.check_upBD;
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xcccccc);
			g.drawRoundRect(0,0,16,16,5,5);
			g.beginFill(0xf0f0f0);
			g.drawRoundRect(1,1,14,14,5,5);			
			g.endFill();
			g.lineStyle(1,0xff0000,1,true,"normal",egret.CapsStyle.SQUARE,egret.JointStyle.MITER);
			g.moveTo(2, 8);
			g.lineTo(6,13);
			g.lineTo(13,3);
			s.check_selUpBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.check_selUpBD).draw(testShape);
			s.check_selOverBD = s.check_selDownBD = s.check_selDisabledBD = s.check_selUpBD;
			//单选框
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xf0f0f0);
			g.drawCircle(8,8,8);
			g.endFill();
			s.radio_upBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.radio_upBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xffffff);
			g.drawCircle(8,8,8);
			g.endFill();
			s.radio_overBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.radio_overBD).draw(testShape);
			s.radio_downBD = s.radio_upBD;
			s.radio_disabledBD = s.radio_upBD;
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xf0f0f0);
			g.drawCircle(8,8,8);
			g.beginFill(0xdd0000);
			g.drawCircle(8,8,3);
			s.radio_selUpBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.radio_selUpBD).draw(testShape);
			s.radio_selOverBD = s.radio_selUpBD;
			s.radio_selDownBD = s.radio_selUpBD;
			s.radio_selDisabledBD = s.radio_selUpBD;
			
			//进度条
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xcccccc);
			g.drawRoundRect(0,0,32,16,15,15);
			//g.beginGradientFill(egret.GradientType.LINEAR,[0xff3333, 0x663333],[1,1],[0,255],matrix);
			g.beginFill(0x999999);
			g.drawRoundRect(1,1,30,14,15,15);	
			g.endFill();			
			s.progress_backBD = GYDrawBitmapData.getBitmapData(32,16).getRef();
			(<GYDrawBitmapData>s.progress_backBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xee3333);
			g.drawRoundRect(0,0,32,16,15,15);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xff3333, 0x663333],[1,1],[0,255],matrix);
			g.drawRoundRect(1,1,30,14,15,15);	
			g.endFill();
			s.progressBD = GYDrawBitmapData.getBitmapData(32,16).getRef();
			(<GYDrawBitmapData>s.progressBD).draw(testShape);		
			//滑动条
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xcccccc);
			g.drawRoundRect(0,0,8,8,5,5);
			//g.beginGradientFill(egret.GradientType.LINEAR,[0xff3333, 0x663333],[1,1],[0,255],matrix);
			g.beginFill(0x999999);
			g.drawRoundRect(1,1,6,6,5,5);	
			g.endFill();
			s.slider_backBD = GYDrawBitmapData.getBitmapData(8,8).getRef();
			(<GYDrawBitmapData>s.slider_backBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xb2d4e0);
			g.drawRoundRect(0,0,8,8,5,5);
			g.beginGradientFill(egret.GradientType.LINEAR,[0xb2d4ee, 0x7cb6e1],[1,1],[0,255],matrix);
			g.drawRoundRect(1,1,6,6,5,5);		
			g.endFill();
			s.slider_progBD = GYDrawBitmapData.getBitmapData(8,8).getRef();
			(<GYDrawBitmapData>s.slider_progBD).draw(testShape);
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xf9f9f9);
			g.drawCircle(8,8,8);
			g.beginFill(0);
			g.drawCircle(8,8,3);
			s.slider_upBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.slider_upBD).draw(testShape);
			
			//window背景
			testShape = new GYSprite;g = testShape.graphics;
			matrix.createGradientBox(126,28,Math.PI/2);
			g.clear();
			g.beginFill(0);
			g.drawRect(0,0,128,30);
			g.beginGradientFill(egret.GradientType.LINEAR,[0x88d0ee, 0xabc9eb],[1,1],[0,255],matrix);
			g.drawRect(1,1,126,28);
			g.beginFill(0);
			g.drawRect(0,29,128,90);
			g.beginFill(0);
			g.drawRect(1,30,126,88);
			s.windowBackBD = GYDrawBitmapData.getBitmapData(128,128).getRef();
			(<GYDrawBitmapData>s.windowBackBD).draw(testShape);
			
			//closeBtn
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xee6666);
			g.drawRect(0,0,20,20);
			g.lineStyle(2,0);
			g.moveTo(6,6);
			g.lineTo(14,14);
			g.moveTo(14,6);
			g.lineTo(6,14);
			g.endFill();
			s.closeBtn_upBD = GYDrawBitmapData.getBitmapData(20,20).getRef();
			(<GYDrawBitmapData>s.closeBtn_upBD).draw(testShape);
			s.closeBtn_upBD = s.closeBtn_overBD = s.closeBtn_downBD = s.closeBtn_disabledBD = s.closeBtn_upBD;
			
			testShape = new GYSprite;g = testShape.graphics;
			g.clear();
			g.beginFill(0xf9f9f9);
			g.drawCircle(8,8,8);
			g.beginFill(0);
			g.drawCircle(8,8,3);
			s.slider_overBD = GYDrawBitmapData.getBitmapData(16,16).getRef();
			(<GYDrawBitmapData>s.slider_overBD).draw(testShape);
			s.slider_downBD = s.slider_upBD;
			s.slider_disabledBD = s.slider_upBD;
			
			s.textAreaBD = s.textInputBD;
			s.scaleBtn_upBD = s.commonBtn_upBD;
			s.scaleBtn_overBD = s.commonBtn_overBD;
			s.scaleBtn_downBD = s.commonBtn_downBD;
			s.scaleBtn_disabledBD = s.commonBtn_disabledBD;
			s.scaleBtn_selUpBD = s.commonBtn_selUpBD;
			s.scaleBtn_selOverBD = s.commonBtn_selOverBD;
			s.scaleBtn_selDownBD = s.commonBtn_selDownBD;
			s.scaleBtn_selDisabledBD = s.commonBtn_selDisabledBD;
			s.tab_upBD = s.commonBtn_upBD;
			s.tab_overBD = s.commonBtn_overBD;
			s.tab_downBD = s.commonBtn_downBD;
			s.tab_disabledBD = s.commonBtn_disabledBD;
			s.tab_selUpBD = s.commonBtn_selUpBD;
			s.tab_selOverBD = s.commonBtn_selOverBD;
			s.tab_selDownBD = s.commonBtn_selDownBD;
			s.tab_selDisabledBD = s.commonBtn_selDisabledBD;
			s.menuBackBD = s.textInputBD;
			
			GYTextBase.defualtSysFont = (navigator.userAgent.toLowerCase().indexOf("chrome") == - 1) ? "Microsoft YaHei" : "微软雅黑";			
			GYTextInput.promptFormat=new TextFormat(GYTextBase.defualtSysFont, 12, 0xcccccc, false, false, false,null,null,"left", null,null,0,5);
		}
		public getBmp(n:string):egret.Texture
		{
			let s = this;
			return null;
		}
		public commonBtn_upBD:egret.Texture;
		public commonBtn_overBD:egret.Texture;
		public commonBtn_downBD:egret.Texture;
		public commonBtn_disabledBD:egret.Texture;
		public commonBtn_selUpBD:egret.Texture;
		public commonBtn_selOverBD:egret.Texture;
		public commonBtn_selDownBD:egret.Texture;
		public commonBtn_selDisabledBD:egret.Texture;
		public scaleBtn_upBD:egret.Texture;
		public scaleBtn_overBD:egret.Texture;
		public scaleBtn_downBD:egret.Texture;
		public scaleBtn_disabledBD:egret.Texture;
		public scaleBtn_selUpBD:egret.Texture;
		public scaleBtn_selOverBD:egret.Texture;
		public scaleBtn_selDownBD:egret.Texture;
		public scaleBtn_selDisabledBD:egret.Texture;
		public check_upBD:egret.Texture;
		public check_overBD:egret.Texture;
		public check_downBD:egret.Texture;
		public check_disabledBD:egret.Texture;
		public check_selUpBD:egret.Texture;
		public check_selOverBD:egret.Texture;
		public check_selDownBD:egret.Texture;
		public check_selDisabledBD:egret.Texture;
		public radio_upBD:egret.Texture;
		public radio_overBD:egret.Texture;
		public radio_downBD:egret.Texture;
		public radio_disabledBD:egret.Texture;
		public radio_selUpBD:egret.Texture;
		public radio_selOverBD:egret.Texture;
		public radio_selDownBD:egret.Texture;
		public radio_selDisabledBD:egret.Texture;
		
		public upArrow_upBD:egret.Texture;
		public upArrow_overBD:egret.Texture;
		public upArrow_downBD:egret.Texture;
		public upArrow_disabledBD:egret.Texture;
		
		public downArrow_upBD:egret.Texture;
		public downArrow_overBD:egret.Texture;
		public downArrow_downBD:egret.Texture;
		public downArrow_disabledBD:egret.Texture;
		
		public rightArrow_upBD:egret.Texture;
		public rightArrow_overBD:egret.Texture;
		public rightArrow_downBD:egret.Texture;
		public rightArrow_disabledBD:egret.Texture;
		
		public leftArrow_upBD:egret.Texture;
		public leftArrow_overBD:egret.Texture;
		public leftArrow_downBD:egret.Texture;
		public leftArrow_disabledBD:egret.Texture;
		
		public sliderBtn_upVBD:egret.Texture;
		public sliderBtn_overVBD:egret.Texture;
		public sliderBtn_downVBD:egret.Texture;
		public sliderBtn_disabledVBD:egret.Texture;
		public scrollerBackVBD:egret.Texture;
		
		public sliderBtn_upHBD:egret.Texture;
		public sliderBtn_overHBD:egret.Texture;
		public sliderBtn_downHBD:egret.Texture;
		public sliderBtn_disabledHBD:egret.Texture;
		public scrollerBackHBD:egret.Texture;
		
		public tab_upBD:egret.Texture;
		public tab_overBD:egret.Texture;
		public tab_downBD:egret.Texture;
		public tab_disabledBD:egret.Texture;
		public tab_selUpBD:egret.Texture;
		public tab_selOverBD:egret.Texture;
		public tab_selDownBD:egret.Texture;
		public tab_selDisabledBD:egret.Texture;
		
		public slider_upBD:egret.Texture;
		public slider_overBD:egret.Texture;
		public slider_downBD:egret.Texture;
		public slider_disabledBD:egret.Texture;
		public slider_backBD:egret.Texture;
		public slider_progBD:egret.Texture;
		
		public progressBD:egret.Texture;
		public progress_backBD:egret.Texture;
		
		public textInputBD:egret.Texture;
		public textAreaBD:egret.Texture;
		public menuBackBD:egret.Texture;
		public windowBackBD:egret.Texture;
		
		public closeBtn_upBD:egret.Texture;
		public closeBtn_overBD:egret.Texture;
		public closeBtn_downBD:egret.Texture;
		public closeBtn_disabledBD:egret.Texture;
		
		public commonBtnSkinVec:egret.Texture[];
		public linkBtnSkinVec:TextFormat[];
		public scaleBtnSkinVec:egret.Texture[];
		public checkBoxSkinVec:egret.Texture[];
		public tabBtnSkinVec:egret.Texture[];
		public radioBtnSkinVec:egret.Texture[];
		public sliderBtnSkinVec:egret.Texture[];
		public scrollUpArrowSkinVec:egret.Texture[];
		public scrollDownArrowSkinVec:egret.Texture[];
		public scrollLeftArrowSkinVec:egret.Texture[];
		public scrollRightArrowSkinVec:egret.Texture[];
		public scrollSliderVSkinVec:egret.Texture[];
		public scrollSliderHSkinVec:egret.Texture[];
		public closeBtnVec:egret.Texture[];
		public inputSkin:TextInputSkin;
		public barTextFormat:TextFormat;
		
		public commonRect:Scale9GridRect = new Scale9GridRect;
		public smallRect:Scale9GridRect = new Scale9GridRect(3,3,3,3);
		public sizeRect3_3_2_2:Scale9GridRect = new Scale9GridRect(3,3,2,2);
		public sizeRect20_20_50_20:Scale9GridRect = new Scale9GridRect(20,20,50,20);
		protected toolTip:IGYDisplay;
		protected toolTipMatrix:egret.Matrix = new egret.Matrix;

		public disposed: boolean;
		public dispose(): void
		{
			let s= this;
			if(s.disposed)return;
			s.disposed = true;
			let arr:egret.Texture[][] = [
				s.commonBtnSkinVec,
				s.scaleBtnSkinVec,
				s.checkBoxSkinVec,
				s.tabBtnSkinVec,
				s.radioBtnSkinVec,
				s.sliderBtnSkinVec,
				s.scrollUpArrowSkinVec,
				s.scrollDownArrowSkinVec,
				s.scrollLeftArrowSkinVec,
				s.scrollRightArrowSkinVec,
				s.scrollSliderVSkinVec,
				s.scrollSliderHSkinVec,				
				s.closeBtnVec
			];
			let len:number,len2:number;
			len = arr.length;
			while(--len > -1)
			{
				len2 = arr[len].length;
				while(--len2 > -1)
				{
					arr[len][len2].dispose();
				}
				arr[len].length = 0;
			}
			s.progressBD.dispose();
			s.progress_backBD.dispose();		
			s.textInputBD.dispose();
			s.textAreaBD.dispose();
			s.menuBackBD.dispose();
			s.windowBackBD.dispose();
			s.toolTip.dispose();
		}
	}
}