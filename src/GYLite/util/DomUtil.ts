/***2024-12-17 迷途小羔羊
* 用于编写网页dom操作的一些工具类
*/
module GYLite
{
    export class DomUtil
    {
        /**主动抛出一个鼠标事件
         * @param obj 抛出事件的dom对象
        */
        public static dispatch_click(obj:HTMLElement) {
            var ev = document.createEvent("MouseEvents");
            ev.initMouseEvent(
                "click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null
            );
            obj.dispatchEvent(ev);
        }
        /**下载文件通过url数组 ,调用方法 download([save.txt],[下载路径]);     
        */
        public static downloadUrls(names:string[], urls:string[], type:any = null) {        
            var save_link:any;
            let len:number;
            len = urls.length;
            while(--len>-1)
            {
                save_link = document.createElementNS("http://www.w3.org/1999/xhtml", "a")
                save_link.href = urls[len];
                save_link.download = (names && names[len])?names[len]:"f"+len;
                DomUtil.dispatch_click(save_link);
            }        
        }
        /**下载文件 ,调用方法 download("save.txt","内容");
         * @param name 下载的文件名
         * @param data ArrayBuffer 或者 Blob|Blob[] 或者 String
         * @param type mimeType {type:"image/png"}等
        */
        public static download(name:string, data:any, type:any = null) {
            var urlObject:any = window.URL || window["webkitURL"] || window;
            var downloadData = GYLite.CommonUtil.GYIs(data,Blob)?data:new Blob(GYLite.CommonUtil.GYIs(data,Array)?data:[data],type);
            var save_link:any = document.createElementNS("http://www.w3.org/1999/xhtml", "a")
            save_link.href = urlObject.createObjectURL(downloadData);
            save_link.download = name;
            DomUtil.dispatch_click(save_link);
        }
        /**上传文件
         * @param callBack 选择文件之后的回调，返回参数blob，function(blob:Blob):void{},multi为true的情况，参数为数组[blob,blob……]
         * @param thisObj 回调this指向
         * @param accept 接受的文件类型 如png图片 "image/png",任意类型图片"image/*"，多个文件类型逗号隔开
         * @param param 附件回调的参数
         * @param multi 是否上传多个文件
        */
        public static upload(callBack:Function,thisObj:any,accept:string="image/png",param:any=null,multi:boolean=false):void
        {
            var upload_link:any = document.createElementNS("http://www.w3.org/1999/xhtml", "input")
            upload_link.type = "file";
            upload_link.id = "file";
            upload_link.accept=accept;
            upload_link.param = param;
            if(multi)upload_link.multiple="multiple";
            upload_link.onchange = function(e):void{
                if(callBack!=null)
                {                    
                    callBack.call(thisObj,multi?e.target.files:e.target.files[0],e.target.param);
                }
                    
            };		
            upload_link.style = "filter:alpha(opacity=0);opacity:0;width: 0;height: 0;";		
            DomUtil.dispatch_click(upload_link);
        }
    }
}