declare namespace egret.web {
    /**
     * @private
     */
    class WebCapability {
        /**
         * @private
         * 检测系统属性
         */
        static detect(): void;
        static injectUIntFixOnIE9(): void;
    }
}
