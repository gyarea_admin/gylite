module GYLite
{
							
						
	export class CanvasGroup extends GYGroup
	{
		//s.canvas
		private scrollGrp:GYScrollGroup;
		
		
		//s.scroller
		private testScroller:GYScroller;
		private scrollerGrp:GYUIComponent;
		private scrollerViewPortGrp:GYGroup;		
		//group
		private gyGrp:GYGroup;		

		private img9BitData:egret.Texture;
		public constructor()
		{
			super();
			var s = this;			
			s.init();
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);			
		}
		private addToStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
		}
		private removeFromStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private init():void
		{var s = this;
			s.img9BitData = GYLoader.getRes("Img/10.jpg").refRes(s);
			/**------------  以下是容器组件 ----------------------*/
			var img:GYImage;
			/*Group*/
			s.gyGrp=new GYGroup;
			s.gyGrp.clipAndEnableScrolling=true;
			// s.gyGrp.scrollPadLeft = s.gyGrp.scrollPadRight = s.gyGrp.scrollPadTop = s.gyGrp.scrollPadBottom = 0;
			s.gyGrp.canDrag = true;
			s.gyGrp.easeGapX = true;
			s.gyGrp.easeGapY = true;
			s.gyGrp.followTarget = s.gyGrp;
			s.gyGrp.width=520;
			s.gyGrp.toolTipOffsetX = s.gyGrp.width + 20;
			s.gyGrp.height=150
			s.gyGrp.x = 120;
			s.gyGrp.y = 170;						
			s.addElement(s.gyGrp);
			img = new GYImage;			
			img.source = s.img9BitData;
			s.gyGrp.addElement(img);
			Main.instance.setTip(s.gyGrp, "裁切的Group，带回弹效果");
			s.gyGrp.touchEnabled = true;			
			
			/*GYScroller*/
			s.scrollerGrp=new GYUIComponent;
			s.scrollerGrp.touchEnabled = true;
			s.scrollerGrp.x = s.gyGrp.x;
			s.scrollerGrp.y = s.gyGrp.y + s.gyGrp.height + 20;
			s.addElement(s.scrollerGrp);
			s.scrollerViewPortGrp=new GYGroup;
			s.scrollerViewPortGrp.canDrag = true;
			s.scrollerViewPortGrp.easeGapY = true;
			s.scrollerViewPortGrp.easeGapX = true;
			s.scrollerViewPortGrp.touchEnabled = true;
			s.scrollerGrp.width = s.scrollerViewPortGrp.width=250;
			s.scrollerGrp.height = s.scrollerViewPortGrp.height=220;
			s.scrollerGrp.addElement(s.scrollerViewPortGrp);
			img = new GYImage;
			img.source = s.img9BitData;
			s.scrollerViewPortGrp.addElement(img);
			s.testScroller=new GYScroller;
			s.testScroller.parent = s.scrollerGrp;
			s.testScroller.viewport = s.scrollerViewPortGrp;			
			s.scrollerGrp.paddingRight = s.testScroller.scrollBarV.width;
			s.scrollerGrp.paddingBottom = s.testScroller.scrollBarH.height;
			Main.instance.setTip(s.scrollerGrp, "使用GYScroller组合的Goup");
			s.scrollerGrp.touchEnabled = true;			
			
			/*Canvas-----*/
			s.scrollGrp=new GYScrollGroup;
			s.scrollGrp.followTarget = s.scrollGrp;
			s.scrollGrp.scrollerViewPort.touchEnabled = true;
			s.scrollGrp.scrollerViewPort.canDrag = true;
			s.scrollGrp.width=250;
			s.scrollGrp.height=220;			
			s.scrollGrp.x = s.scrollerGrp.x + s.scrollerGrp.width + 20;
			s.scrollGrp.y = s.scrollerGrp.y;
			// s.scrollGrp.scrollerViewPort.limitXMin = s.scrollGrp.scrollerViewPort.limitYMin = -100;
			// s.scrollGrp.scrollerViewPort.limitXMax = s.scrollGrp.scrollerViewPort.limitYMax = 200;
			img = new GYImage;
			img.source = s.img9BitData;
			s.scrollGrp.addElement(img);
			s.addElement(s.scrollGrp);			
			Main.instance.setTip(s.scrollGrp, "自带滚动条的容器");
			s.scrollGrp.touchEnabled = true;
			// s.scrollGrp.scroller.verticalPolicy = 2;
			// s.scrollGrp.scroller.horizonPolicy = 2;

			// img = new GYImage;
			// img.x = 1200;
			// img.source = s.img9BitData;
			// s.scrollGrp.addElement(img);
			// img = new GYImage;
			// img.x = 2400;
			// img.source = s.img9BitData;
			// s.scrollGrp.addElement(img);
			// img = new GYImage;
			// img.y = 600;
			// img.source = s.img9BitData;
			// s.scrollGrp.addElement(img);
			// img = new GYImage;
			// img.x = 1200;
			// img.y = 600;
			// img.source = s.img9BitData;
			// s.scrollGrp.addElement(img);
			// img = new GYImage;
			// img.x = 2400;
			// img.y = 600;
			// img.source = s.img9BitData;
			// s.scrollGrp.addElement(img);
			
			
			// GYLite.GYSprite.stage.addEventListener(GYLite.MouseEvent.MOUSE_MOVE, function():void{
			// 	console.log(1);
			// },s);
		}
		/*Group*/
		private clkGroup(e:egret.TouchEvent):void
		{var s = this;
			s.gyGrp.clipX+=2;
			s.gyGrp.clipY+=2;
		}
		private clkScrollerGroup(e:egret.TouchEvent):void
		{var s = this;
			s.testScroller.scrollPosX+=2;
			s.testScroller.scrollPosY+=2;
		}
	}
}