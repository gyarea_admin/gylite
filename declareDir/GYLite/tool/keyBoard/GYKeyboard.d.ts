declare module GYLite {
    class GYKeyboard {
        static createId: number;
        private static _instance;
        static getInstance(): GYKeyboard;
        private _keyListeners;
        private _keyDownDict;
        private _typing;
        /**是否在输入框输入时仍然发出按键事件*/
        keyEnabledWhenTyping: boolean;
        constructor();
        protected resort(): void;
        protected sort(a: IKeyBoardData, b: IKeyBoardData): number;
        protected find(k: IKeyBoardObject): number;
        /**添加键盘监听，重复添加会覆盖
         * @param k 监听键盘的对象
         * @param priority 优先级，数字越大优先级越高
        */
        addKeyListener(k: IKeyBoardObject, priority?: number): void;
        /**移除键盘监听
         * @param k 移除监听键盘的对象
        */
        removeKeyListener(k: IKeyBoardObject): void;
        private kDown;
        private kUp;
        /**按下键的时间戳，0为未按下*/
        isKeyDown(keyCode: number): number;
        releaseKey(keyCode: number): void;
        isCtrlDown(): boolean;
        isAltDown(): boolean;
        isShiftDown(): boolean;
        isTyping(val: boolean): void;
        isCapsLock(): boolean;
    }
    interface IKeyBoardData {
        /**监听按键的对象*/ keyObject: IKeyBoardObject;
        /**优先级，数字越大优先级越高*/ priority: number;
        /**创建的id，id越小创建时间越早*/ createId: number;
    }
}
