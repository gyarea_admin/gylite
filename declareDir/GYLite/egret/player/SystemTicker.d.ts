declare namespace egret.sys {
    /**
     * @private
     */
    let $START_TIME: number;
    /**
     * @private
     * 是否要广播Event.RENDER事件的标志。
     */
    let $invalidateRenderFlag: boolean;
    /**
     * @private
     * 需要立即刷新屏幕的标志
     */
    let $requestRenderingFlag: boolean;
    /**
     * Egret心跳计时器
     */
    class SystemTicker {
        /**
         * @private
         */
        constructor();
        /**
         * @private
         */
        private playerList;
        /**
         * @private
         * 注册一个播放器实例并运行
         */
        $addPlayer(player: Player): void;
        /**
         * @private
         * 停止一个播放器实例的运行。
         */
        $removePlayer(player: Player): void;
        /**
         * @private
         */
        private callBackList;
        /**
         * @private
         */
        private thisObjectList;
        /**
         * @private
         */
        $startTick(callBack: (timeStamp: number) => boolean, thisObject: any): void;
        /**
         * @private
         */
        $stopTick(callBack: (timeStamp: number) => boolean, thisObject: any): void;
        /**
         * @private
         */
        private getTickIndex;
        /**
         * @private
         *
         */
        private concatTick;
        /**
         * @private
         * 全局帧率
         */
        $frameRate: number;
        /**
         * @private
         */
        private frameInterval;
        /**
         * @private
         */
        private frameDeltaTime;
        /**
         * @private
         */
        private lastTimeStamp;
        /**
         * @private
         * 设置全局帧率
         */
        $setFrameRate(value: number): boolean;
        /**
         * @private
         */
        private lastCount;
        /**
         * @private
         * ticker 花销的时间
         */
        private costEnterFrame;
        /**
         * @private
         * 是否被暂停
         */
        private isPaused;
        /**
         * Pause the ticker.
         * @version Egret 5.0.2
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 暂停心跳
         * @version Egret 5.0.2
         * @platform Web,Native
         * @language zh_CN
         */
        pause(): void;
        /**
         * Resume the ticker.
         * @version Egret 5.0.2
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 恢复心跳
         * @version Egret 5.0.2
         * @platform Web,Native
         * @language zh_CN
         */
        resume(): void;
        /**
         * @private
         * 执行一次刷新
         */
        update(forceUpdate?: boolean): void;
        /**
         * @private
         * 执行一次屏幕渲染
         */
        private render;
        /**
         * @private
         * 广播EnterFrame事件。
         */
        private broadcastEnterFrame;
        /**
         * @private
         * 广播Render事件。
         */
        private broadcastRender;
        /**
         * @private
         */
        private callLaters;
        /**
         * @private
         */
        private callLaterAsyncs;
        /**
         * @private
         */
        $beforeRender: () => void;
        /**
         * @private
         */
        $afterRender: () => void;
    }
}
declare module egret {
    namespace lifecycle {
        type LifecyclePlugin = (context: LifecycleContext) => void;
        /**
         * @private
         */
        let stage: egret.Stage;
        /**
         * @private
         */
        let contexts: LifecycleContext[];
        class LifecycleContext {
            pause(): void;
            resume(): void;
            onUpdate?: () => void;
        }
        let onResume: () => void;
        let onPause: () => void;
        function addLifecycleListener(plugin: LifecyclePlugin): void;
    }
    /**
     * 心跳计时器单例
     */
    let ticker: sys.SystemTicker;
}
/**
 * @private
 */
declare let egret_stages: egret.Stage[];
