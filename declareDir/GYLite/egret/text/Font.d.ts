declare namespace egret.sys {
    let fontResourceCache: {
        [url: string]: any;
    };
    function registerFontMapping(name: string, path: string): void;
}
declare namespace egret {
    /**
     * Register font mapping.
     * @param name The font family name to register.
     * @param path The font path.
     * @version Egret 5.3
     * @platform Web,Native
     * @language en_US
     */
    /**
     * 注册字体映射
     * @param name 要注册的字体名称
     * @param path 注册的字体地址
     * @version Egret 5.3
     * @platform Web,Native
     * @language zh_CN
     */
    function registerFontMapping(name: string, path: string): void;
}
