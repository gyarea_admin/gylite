declare module GYLite {
    class ExcelRowGrid extends ItemRender implements IItemRender {
        protected _text: GYText;
        protected _drager: GYSprite;
        excel: GYExcel;
        constructor();
        protected initComponent(): void;
        protected drawBg(): void;
        protected widthAndHeightChange(e: GYViewEvent): void;
        protected dragSize(handle: DraggerHandle): void;
        /**@inheriDoc */
        setData(obj: any): void;
    }
}
