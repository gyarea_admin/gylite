package preloader              
{              
	import flash.display.*;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**              
	 * 功能：SWF自身的加载条<br/>              
	 * 用法：在需要实现自加载的类声明前加上<code>[Frame(factoryClass="cc.lomo.preloader.Preloader")]</code>              
	 * @author vincent              
	 */
	public class Preloader extends MovieClip              
	{              
		/**              
		 * 是否本地运行               
		 */
		protected var mNative:Boolean;              
		/**              
		 * 模拟当前加载量(本地运行时)              
		 */
		protected var mIndex:int    = 0;              
		/**              
		 * 模拟最大加载量(本地运行时)              
		 */
		protected const mMax:int    = 100;              
		private var _textf:TextField;
		private var _drawBitmap:Bitmap;
		private var _shape:Shape;
		public function Preloader()              
		{              
			addEventListener("addedToStage", addedToStageHandler);    
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			_drawBitmap = new Bitmap(new BitmapData(stage.stageWidth,stage.stageHeight));
			addChild(_drawBitmap);
			_shape = new Shape;
			addChild(_shape);
		}              
		protected function addedToStageHandler(e:*):void
		{              
			removeEventListener("addedToStage", addedToStageHandler);              
			//如果已经加载完，那估计就是本地运行了，这时候我们只有搞个假的Preloader了              
			mNative = loaderInfo.bytesLoaded == loaderInfo.bytesTotal;              
			
			addListeners();              
		}              
		/**              
		 *               
		 * 侦听加载事件              
		 */  
		protected function addListeners():void
		{              
			if(mNative)              
				addEventListener("enterFrame", enterFrameHandler);              
			else
			{              
				loaderInfo.addEventListener("progress", progressHandler);              
				loaderInfo.addEventListener("complete", completeHandler);              
			}              
		}              
		/**              
		 *               
		 * 移除加载事件              
		 */  
		protected function removeListeners():void
		{              
			if(mNative)              
				removeEventListener("enterFrame", enterFrameHandler);              
			else
			{              
				loaderInfo.removeEventListener("progress", progressHandler);              
				loaderInfo.removeEventListener("complete", completeHandler);              
			}              
		}              
		/**              
		 * 用ENTER_FRAME模拟加载事件(本地运行时)              
		 * @param e              
		 *               
		 */  
		protected function enterFrameHandler(e:*):void
		{              
			mIndex ++;              
			setProgress(mIndex / mMax);              
			mIndex > mMax && completeHandler();              
		}              
		/**              
		 * 显示进度条              
		 * @param value 进度比 0.0 ~ 1.0              
		 *               
		 */  
		private var _drawVec:Vector.<DrawLine>;
		private var _c:int;
		protected function setProgress(value:Number):void
		{              
			if(value == 1)              
				return;              
			if(_textf == null)
			{
				_textf = new TextField;
				_textf.width = 200;
				_textf.height = 100;
				_textf.defaultTextFormat = new TextFormat((Capabilities.manufacturer.toLowerCase().indexOf("google") == - 1) ? "Microsoft YaHei" : "微软雅黑",60,null,true,null,null,null,null,"center");
				_textf.x = stage.stageWidth - _textf.width >> 1;
				_textf.y = stage.stageHeight - _textf.height >> 1;
				_textf.blendMode = BlendMode.INVERT;
				addChild(_textf);
				_drawVec = new Vector.<DrawLine>(25,true);
				var len:int;
				len = _drawVec.length;
				while(--len>-1)
				{
					_drawVec[len]=new DrawLine(_shape.graphics,stage,Math.random()*2 + 2, 0);
				}
			}
			_textf.text = int(value * 100) + "%";
			len = _drawVec.length;
			while(--len>-1)
			{
				_drawVec[len].draw(value);
			}
			if(_c % 10 == 0)
			{
				_drawBitmap.bitmapData.draw(_shape);
				_shape.graphics.clear();
			}
		}              
		/**              
		 * 加载事件              
		 * @param e              
		 *               
		 */  
		protected function progressHandler(e:*):void
		{              
			setProgress(loaderInfo.bytesLoaded/loaderInfo.bytesTotal)              
		}              
		protected function completeHandler(e:*=null):void
		{              
			removeListeners();              
			
			addEventListener("enterFrame", init);              
		}              
		/**              
		 * 加载完成后 构造主程序              
		 */
		protected function init(e:*):void
		{              
			//currentLabels[1].name 获得第二帧的标签 也就是主程序的类名以"_"连接如：com_adobe_class_Main,我们需要将其转换为com.adobe.class::Main这样的格式              
			var prefix:Array = currentLabels[1].name.split("_");              
			var suffix:String = prefix.pop();              
			var cName:String =  prefix.join(".") + "::" + suffix;              
			//判断是否存在主程序的类              
			if(loaderInfo.applicationDomain.hasDefinition(cName))              
			{              
				//知道存在主程序的类了，删除enterFrame的侦听              
				removeEventListener("enterFrame", init);              
				
				var cls:Class = loaderInfo.applicationDomain.getDefinition(cName) as Class;              
				var main:DisplayObject = new cls();              
				parent.addChild( main );              
				parent.removeChild(this);              
			}              
		}              
		
	}              
}
import flash.display.Graphics;
import flash.display.Stage;

class DrawLine{
	private var _toX:Number,_toY:Number;
	private var _count:int;
	private var _n:int;
	private var _angle:Number;
	private var _limitLow:Number,_limitUp:Number,r:Number;
	private var _graphics:Graphics;
	private var _stage:Stage;
	private var _color:uint;
	private var _colorStep:int;
	private var _back:Boolean;
	private var _colorIndex:int;
	private var _colorVec:Vector.<uint>;
	private var _drawStep:int;
	private static var colorArr:Array=[0,1,0,2,1,0,2];
	private static var colorStsArr:Array=[1,1,-1,1,-1,1,-1];
	public function DrawLine(graphics:Graphics,s:Stage,aglParam:Number=2.1,color:uint=0xff0000)
	{
		_graphics = graphics;
		_stage = s;
		_toX = 0;
		_toY = _stage.stageHeight >> 1;
		_angle = Math.PI/aglParam;
		_limitLow = 1;
		_limitUp = 1;
		_color = color;
		r = 0.5;
		_colorVec = new Vector.<uint>;
		_colorVec[0] = color >> 16;
		_colorVec[1] = (color >> 8) & 0x0000ff;
		_colorVec[2] = color & 0x0000ff;
		_colorStep = 5;
		_drawStep = 2;
		_back = false;
		_colorVec = new Vector.<uint>(3,true);
	}
	private function colorChange():void
	{
		var ind:int = colorArr[_colorIndex];
		var sts:int = colorStsArr[_colorIndex];
		_colorVec[ind]
		if(sts > 0)
		{
			_colorVec[ind]+=_colorStep;
			if(_colorVec[ind] > 255)
			{
				_colorVec[ind] = 255;
				++_colorIndex;
			}
		}
		else
		{
			if(_colorVec[ind] < _colorStep)
			{
				_colorVec[ind] = 0;
				++_colorIndex;
			}
			else
				_colorVec[ind]-=_colorStep;
		}
		if(_colorIndex > colorArr.length)
			_colorIndex = 1;
	}
	public function draw(value:Number):void
	{
		temp = value * _stage.stageWidth;
		var i:int,len:int;
		var temp:Number;
		var temp2:int,temp3:int;
		temp2 = temp - _toX;
		if(temp2 < _drawStep)return;
		temp3 = temp2;
		len = Math.ceil((temp - _toX)/_drawStep);
		temp2 = 0;
		for(i=0;i<len;++i)
		{
			if(i == len -1)
				temp2 = temp3 % _drawStep;
			else
				temp2 = _drawStep;
			if(_count <= 0)
			{
				_count = Math.random() * _limitUp + _limitLow;
				_n = (Math.random() < r? -1:1);
				r += (_n < 0?-0.05:0.05);
			}
			else
				--_count;
			
			colorChange();
			_graphics.lineStyle(1,_colorVec[0] << 16 | _colorVec[1] << 8 | _colorVec[2]);
			_graphics.moveTo(_toX,_toY);
			_toX += temp2;
			_toY += _n * (temp2) * Math.tan(_angle);
			_graphics.lineTo(_toX,_toY);
		}
		_toX = temp;
	}
}