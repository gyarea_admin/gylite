module GYLite
{
				
		
	export class IconButtonSkin extends ButtonSkin
	{
		public constructor(skinVec:egret.Texture[], rect:Scale9GridRect=null)
		{						
			super(skinVec, rect);
		}
		public drawSkin(state:number=0):void
		{var s = this;
			s._curSkin.texture=(state<s._stsVec.length && s._stsVec[state])? s._stsVec[state] : s._stsVec[0];
			if(state == ButtonBase.STATE_DOWN)
				s._text.verticalCenter = 3;
			else
				s._text.verticalCenter = 0;
		}
	}
}