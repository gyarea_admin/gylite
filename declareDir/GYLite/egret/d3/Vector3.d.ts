declare module GYLite {
    class Vector3 {
        elements: Float32Array;
        constructor(x?: number, y?: number, z?: number);
        /**
         *从Array数组拷贝值。
         *@param array 数组。
         *@param offset 数组偏移。
         */
        fromArray(array: number[], offset?: number): void;
        /**
         *克隆。
         *@param destObject 克隆源。
         */
        cloneTo(destObject: Vector3): void;
        /**
         *克隆。
         *@return 克隆副本。
         */
        clone(): Vector3;
        toDefault(): void;
        reset(x: number, y: number, z: number): void;
        /**
         *设置X轴坐标。
         *@param value X轴坐标。
         */
        set x(value: number);
        /**
         *获取X轴坐标。
         *@return X轴坐标。
         */
        get x(): number;
        /**
         *设置Y轴坐标。
         *@param value Y轴坐标。
         */
        /**
         *获取Y轴坐标。
         *@return Y轴坐标。
         */
        get y(): number;
        set y(value: number);
        /**
         *设置Z轴坐标。
         *@param value Z轴坐标。
         */
        set z(value: number);
        /**
         *获取Z轴坐标。
         *@return Z轴坐标。
         */
        get z(): number;
        static distanceSquared(value1: Vector3, value2: Vector3): number;
        static distance(value1: Vector3, value2: Vector3): number;
        static min(a: Vector3, b: Vector3, out: Vector3): void;
        static max(a: Vector3, b: Vector3, out: Vector3): void;
        static transformQuat(source: Vector3, rotation: Quaternion, out: Vector3): void;
        static scalarLength(a: Vector3): number;
        static scalarLengthSquared(a: Vector3): number;
        static normalize(s: Vector3, out: Vector3): void;
        static multiply(a: Vector3, b: Vector3, out: Vector3): void;
        static scale(a: Vector3, b: number, out: Vector3): void;
        static lerp(a: Vector3, b: Vector3, t: number, out: Vector3): void;
        static transformV3ToV3(vector: Vector3, transform: Matrix4x4, result: Vector3): void;
        static transformV3ToV4(vector: Vector3, transform: Matrix4x4, result: Vector4): void;
        static TransformNormal(normal: Vector3, transform: Matrix4x4, result: Vector3): void;
        static transformCoordinate(coordinate: Vector3, transform: Matrix4x4, result: Vector3): void;
        static Clamp(value: Vector3, min: Vector3, max: Vector3, out: Vector3): void;
        static add(a: Vector3, b: Vector3, out: Vector3): void;
        static subtract(a: Vector3, b: Vector3, o: Vector3): void;
        static cross(a: Vector3, b: Vector3, o: Vector3): void;
        static dot(a: Vector3, b: Vector3): number;
        static equals(a: Vector3, b: Vector3): boolean;
        static ZERO: Vector3;
        static ONE: Vector3;
        static NegativeUnitX: Vector3;
        static UnitX: Vector3;
        static UnitY: Vector3;
        static UnitZ: Vector3;
        static ForwardRH: Vector3;
        static ForwardLH: Vector3;
        static Up: Vector3;
        static NAN: Vector3;
        static _tempVector4: Vector4;
    }
}
