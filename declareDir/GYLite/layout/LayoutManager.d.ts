declare module GYLite {
    class LayoutManager {
        protected static _renderFuncList: IUpdate[];
        protected static _tempRenderFuncList: IUpdate[];
        protected static _rending: Boolean;
        protected static _stage: egret.Stage;
        static max: number;
        static lastNum: number;
        static Init(stage: egret.Stage): void;
        /**
         * 舞台渲染前执行
         * */
        static addRenderFunc(up: IUpdate): void;
        private static renderFunc;
        private static update;
    }
}
