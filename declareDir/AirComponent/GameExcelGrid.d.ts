declare module GYLite {
    class GameExcelGrid extends ExcelGrid {
        constructor();
        protected initComponent(): void;
        set selected(b: boolean);
        get selected(): boolean;
        protected widthAndHeightChange(e: GYViewEvent): void;
        protected drawBg(c?: number): void;
    }
}
