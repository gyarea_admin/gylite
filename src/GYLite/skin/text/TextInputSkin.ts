module GYLite
{
							
			
	export class TextInputSkin extends GYSkin implements IGYSkin
	{
		protected _background:egret.Texture;
		protected _backScaleShape:GYScaleSprite;
		protected _rect:Scale9GridRect;
		public constructor(back:egret.Texture|GYScaleSprite=null,rect:Scale9GridRect=null)
		{
			super();
			var s = this;
			if(CommonUtil.GYIs(back, egret.Texture))
			{
				s.background = <egret.Texture>back;
				s.scale9GridRect=rect;
			}				
			else
			{
				s._backScaleShape = <GYScaleSprite>back;
				if(rect)
					s._backScaleShape.scale9GridRect = rect;
			}
			
		}
		public release():void
		{var s = this;
			if(s._backScaleShape)s._backScaleShape.bitmapData = null;
		}
		public set hostComponent(val:GYSprite)
		{var s = this;
			if(val==s._hostComponent)
				return;
			if(s._backScaleShape == null)
			{
				s._hostComponent=val;
				return;
			}
			if(s._hostComponent)
			{
				if(s._backScaleShape.parent)
					s._hostComponent.removeElement(s._backScaleShape);
			}
			s._hostComponent=val;
			if(s._hostComponent)
			{
				s._hostComponent.addElementAt(s._backScaleShape, 0);
				if(!isNaN(s.hostComponent.width))
					s._backScaleShape.width = s._hostComponent.width;
				if(!isNaN(s.hostComponent.height))
					s._backScaleShape.height = s._hostComponent.height;
			}
		}
		public get hostComponent():GYSprite
		{var s = this;
			return s._hostComponent;
		}
		public set width(value:number) 
		{var s = this;
			if(s._backScaleShape)s._backScaleShape.width = value;
		}
		public set height(value:number) 
		{var s = this;
			if(s._backScaleShape)s._backScaleShape.height = value;
		}
		public set background(val:egret.Texture)
		{var s = this;
			if(s._background == val)
				return;
			s._background = val
			if(val==null)
			{
				if(s._backScaleShape)
					s._backScaleShape.bitmapData=null;
				return;
			}
			if(s._backScaleShape == null)
			{
				s._backScaleShape = new GYScaleSprite(val);
				s._backScaleShape.scale9GridRect = s._rect;
				s._backScaleShape.touchEnabled = false;
			}
			s._backScaleShape.bitmapData = val;
			
		}
		public get background():egret.Texture
		{
			return this._background;
		}
		public set scale9GridRect(val:Scale9GridRect)
		{var s = this;
			s._rect=val;
			if(s._backScaleShape)
				s._backScaleShape.scale9GridRect = s._rect;
		}
		public get scale9GridRect():Scale9GridRect
		{
			return this._rect;
		}
		public clone():IGYSkin
		{var s = this;
			return new TextInputSkin(s._background,s._rect);
		}
	}
}