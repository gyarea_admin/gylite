module GYLite
{			
	export class GYTitleWindow extends GYSkinContainer
	{
		private _canDrag:boolean;
		private _canResize:boolean;
		/**窗口响应拖拽的y方向范围*/public dragRange:number;
		/**窗口是否可拉伸*/public dragResizeRange:number;
		/**窗口响应拉伸的x方向范围*/public minDragWidth:number;
		/**窗口响应拉伸的y方向范围*/public minDragHeight:number;
		private _tempX:number;
		private _tempY:number;
		public constructor(s:any=null)
		{
			super();			
			this.skin = s;
			this.touchEnabled = true;
			this.rectHit = true;
			this.initComponent();
		}
		protected initComponent():void
		{var s = this;
			s.closeBtn.setOnClick(s.oper,s);
			s.dragRange = 40;
			s.dragResizeRange = 20;
			s.minDragWidth = s.minDragHeight = 100;
			s.canDrag = true;
		}
		private dragDown(e:egret.TouchEvent):void
		{var s = this;
			if(s.mouseY < s.dragRange)
			{
				GYSprite.$stage.addEventListener(egret.TouchEvent.TOUCH_END, s.dragUp,s);
				CommonUtil.addStageLoop(s.dragLoop,s);
				s._tempX = (<GYSprite>s.parent).mouseX - s.x;
				s._tempY = (<GYSprite>s.parent).mouseY - s.y;
				(<GYSprite>s.parent).addElement(s);				
			}
		}
		private dragUp(e:egret.TouchEvent):void
		{var s = this;
			GYSprite.$stage.removeEventListener(egret.TouchEvent.TOUCH_END, s.dragUp,s);
			CommonUtil.delStageLoop(s.dragLoop,s);			
		}
		private dragLoop(t:number):void
		{var s = this;
			s.x = (<GYSprite>s.parent).mouseX - s._tempX;
			s.y = (<GYSprite>s.parent).mouseY - s._tempY;
		}

		private dragResizeDown(e:egret.TouchEvent):void
		{var s = this;
			if(s.width - s.mouseX < s.dragResizeRange && s.height - s.mouseY < s.dragResizeRange)
			{
				GYSprite.$stage.addEventListener(egret.TouchEvent.TOUCH_END, s.dragResizeUp,s);
				CommonUtil.addStageLoop(s.dragResizeLoop,s);
				s._tempX = (<GYSprite>s.parent).mouseX - s.width;
				s._tempY = (<GYSprite>s.parent).mouseY - s.height;
				(<GYSprite>s.parent).addElement(s);				
			}
		}
		private dragResizeUp(e:egret.TouchEvent):void
		{var s = this;
			GYSprite.$stage.removeEventListener(egret.TouchEvent.TOUCH_END, s.dragResizeUp,s);
			CommonUtil.delStageLoop(s.dragResizeLoop,s);			
		}
		private dragResizeLoop(t:number):void
		{var s = this;
			s.width = Math.max(s.minDragWidth,(<GYSprite>s.parent).mouseX - s._tempX);
			s.height = Math.max(s.minDragHeight,(<GYSprite>s.parent).mouseY - s._tempY);
		}
		
		/**自定义皮肤请实现IWindowSkin接口 */
		protected getThemeSkin():IGYSkin
		{var s = this;
			return GYSprite.skinTheme.GetWindowSkin();
		}
		protected oper(e:egret.TouchEvent):void
		{var s = this;
			if(e.currentTarget == s.closeBtn)
			{
				s.hide();
			}
		}
		public show(pr:IGYContainer):void
		{var s = this;
			if((<any>s.parent) != pr)
				pr.addElement(this);
		}
		public hide():void
		{var s = this;
			if(s.parent)
				(<any>s.parent).removeElement(this);
		}

		public get closeBtn():GYButton
		{var s = this;
			return s._skin.closeBtn;
		}
		/**窗体是否可拖动*/
		public get canDrag():boolean
		{var s = this;
			return s._canDrag;
		}

		public set canDrag(value:boolean)
		{var s = this;
			if(s._canDrag == value)return;
			if(s._canDrag)
				s._skin.backgoundImage.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, s.dragDown, s);
			s._canDrag = value;
			if(s._canDrag)
				s._skin.backgoundImage.addEventListener(egret.TouchEvent.TOUCH_BEGIN, s.dragDown, s);
			s._skin.backgoundImage.touchEnabled = s._canDrag;
		}
		/**窗体是否可拉伸*/
		public get canResize():boolean
		{var s = this;
			return s._canDrag;
		}

		public set canResize(value:boolean)
		{var s = this;
			if(s._canResize == value)return;
			if(s._canResize)
				s.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, s.dragResizeDown, s);
			s._canResize = value;
			if(s._canResize)
				s.addEventListener(egret.TouchEvent.TOUCH_BEGIN, s.dragResizeDown, s);
			s.rectHit = s.rectHit || s._canResize;
		}
		public setBackgound(b:egret.BitmapData):void
		{var s = this;
			s._skin.backgoundImage.bitmapData = b;
		}

		public get backgoundImage():GYScaleSprite
		{var s = this;
			return s._skin.backgoundImage;
		}
		public setIcon(val:egret.Texture):void
		{var s = this;
			(s._skin as IWindowSkin).setIcon(val);
		}
		public setTitle(val:string):void
		{var s = this;
			(s._skin as IWindowSkin).setTitle(val);	
		}
		public get icon():GYImage
		{var s = this;
			return (s._skin as IWindowSkin).icon;
		}
		public get title():GYText
		{var s = this;
			return (s._skin as IWindowSkin).title;
		}
	}
}