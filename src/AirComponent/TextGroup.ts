module GYLite
{					
	export class TextGroup extends GYGroup
	{
		private richText:GYRichText;
		private richScroller:GYScroller;
		private richGrp:GYGroup;
		private _txt2:GYTextInput;
		private gyText:GYText;
		private gyTextArea:GYTextArea;
		private _richTimeId:number=-1;
		public constructor()
		{
			super();
			var s = this;			
			s.init();
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private addToStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
//			s._richTimeId = TimeManager.registered(s.addText);
		}
		private removeFromStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
//			TimeManager.unRegistered(s._richTimeId,s.addText);
//			s._richTimeId = -1;
		}
		private init():void
		{var s = this;
			/**------------  以下是文本组件 ----------------------*/
			
			
			/*------------MyText-------------*/
			
			s.gyText=new GYText(false);
			s.gyText.width = 200;
			s.gyText.height = 50;
			s.gyText.color = ColorConst.fontColor;
			s.gyText.backgroundColor = 0xfff000;
			s.gyText.x = 40;
			s.gyText.y = 150;			
			s.gyText.size = 14;	
			s.gyText.indent = 50;
			s.gyText.letterSpacing = 10;			
			// s.gyText.underline = true;	
			s.gyText.border = true;
			s.gyText.borderColor = 0xff0000;			
			s.gyText.htmlText = "这个是一个GYText文本，缩进50，行高25，可以<b>加粗</b>、<u>下划线</u>、<font color='#ff0000' size='20'>颜色大小</font>、可单行、多行，当然也可以文本缩进，而且是支持html的，还有边框。当然，你想要富文本，用下面的这个组件也可以。";				
			// s.addElement(s.gyText);
			s.gyText.toolTipString = "一个文本";
			s.gyText.touchEnabled = true;
			s.addElement(s.gyText);						
			
		
			/*------------TextInput-------------*/
			s._txt2=new GYTextInput(null,false);
			s.addElement(s._txt2);
			s._txt2.textInput.color = ColorConst.inputColor;
			s._txt2.textInput.maxChars=300;
			s._txt2.paddingTop = 6;
			s._txt2.paddingLeft = 5;
			s._txt2.x = 40;
			s._txt2.y = s.gyText.y + s.gyText.height + 30;
			s._txt2.width=400;
			s._txt2.height = 25;			
			s._txt2.prompt = "输入内容";
			s._txt2.promptText.x = 5;
			Main.instance.setTip(s._txt2,"输入文本");
			s._txt2.touchEnabled = true;
			// setTimeout(function(s) {
			// 	let input:egret.InputController = (<any>s._txt2.textInput).inputUtils;
			// 	let stageTxt:any = input.stageText;
			// 	input.$onFocus();
			// 	stageTxt.executeShow();
			// 	stageTxt.htmlInput.show();							
			// }, 0, this);
			s._txt2.addEventListener(egret.TouchEvent.TOUCH_TAP,function(e):void{
				console.log(1111);
			},s);
			
			/*------------RichText---------------*/
			//外层容器
			s.richGrp = new GYGroup;
			s.richGrp.x = 40;
			s.richGrp.y = s._txt2.y + s._txt2.height + 20;
			s.richGrp.width = 200;
			s.richGrp.height = 150;
			s.richGrp.touchEnabled = true;
			s.richGrp.rectHit = true;
			//富文本
			s.richText = new GYRichText;
			s.richText.textFormat.size = 14;
			s.richText.textFormat.font = GYTextBase.defualtSysFont;
			s.richText.richTop = 0;
			s.richText.maxLines = 10;
			s.richText.textFormat.textColor = ColorConst.fontColor;
			//滚动条
			s.richScroller=new GYScroller;
			s.richScroller.viewport = s.richText;
			s.richScroller.parent = s.richGrp;
			//背景
			var back:GYScaleSprite = new GYScaleSprite((GYSprite.skinTheme as GYSkinTheme).textInputBD, (GYSprite.skinTheme as GYSkinTheme).commonRect);
			back.percentHeight = back.percentWidth = 1;
			back.touchEnabled = true;
			s.richGrp.addElementAt(back, 0);
			s.richGrp.addElement(s.richText);
			//调整位置
			var padding:number = 5;
			s.richText.setPadding(padding);
			s.richText.paddingRight = padding + s.richScroller.scrollBarV.width;
			s.richText.width = s.richGrp.width - padding - s.richText.paddingRight;
			s.richText.height = s.richGrp.height - padding * 2;			
			s.addElement(s.richGrp);
			s.richScroller.scrollBarV.right = 5;
			s.richScroller.scrollBarV.y = padding;			
			Main.instance.setTip(s.richGrp,"richText富文本，图文并茂，可自定义混排元素");
			s.addText();
			let id=setInterval(function() {
				if(s.richTest > 10)
				{					
					return;
				}
				++s.richTest;
				s.addText();
			}, 1000,s);
			// "测<bmp s.width='30' s.height='30' target='GYLite.RichImage' render='getUrlInstance' renderParam='liveStream/img/expression/1.jpg,0,0' clear='clear' >试<bmp s.width='30' s.height='30' target='GYLite.RichImage' render='getUrlInstance' renderParam='liveStream/img/expression/2.jpg,0,0' clear='clear' >啊<bmp s.width='30' s.height='30' target='GYLite.RichImage' render='getUrlInstance' renderParam='liveStream/img/expression/1.jpg,0,0' clear='clear' ><bmp s.width='30' s.height='30' target='GYLite.RichImage' render='getUrlInstance' renderParam='liveStream/img/expression/1.jpg,0,0' clear='clear' >"			
			s.checkOutSize();
			// s.touchEnabled = s.touchChildren = false;

			/*------------TextArea-------------*/
			s.gyTextArea =new GYTextArea;
			s.gyTextArea.x = 40;
			s.gyTextArea.y = s.richGrp.y + s.richGrp.height + 20;			
			s.gyTextArea.setPadding(10);
			s.gyTextArea.editable = true;
			s.gyTextArea.width = 400;
			s.gyTextArea.height = 150;
			s.gyTextArea.textArea.color = ColorConst.fontColor;						
			s.gyTextArea.textArea.size = 24;
			s.gyTextArea.textArea.fontFamily = "SimSun";
			s.gyTextArea.text = "羔羊引擎是一个开发利器，如何利，羔羊引擎是一个开发利器，如何利，羔羊引擎是一个开发利器，如何利，羔羊引擎是一个开发利器，如何利，羔羊引擎是一个开发利器，如何利，";
			s.addElement(s.gyTextArea);			
			Main.instance.setTip(s.gyTextArea,"textArea，可编辑多行文本");
			s.gyTextArea.touchEnabled = true;
		}
		
		/*RichText*/
		private richTest:number=0;
		private addText(sysTime:number = 0):void
		{var s = this;						
			
			var len:number = 1;			
			var r:number=-0.2;
			var Id:number=0;
			var str:string;
			var imgStr:string;
			var faceNum:number = RichSeqImage.imageList.length;
			while(--len > -1)
			{
				imgStr= RichSeqImage.getImageRich(Id,0);
				r = Math.random();
				Id=(Math.random() * faceNum >> 0);				
				if(r < 0.2)
					str = "大家<font color='#ff0000'>好，我是</font>纯洁的羔羊" + imgStr + "，听说羔羊开发了一个羔羊引擎，牛逼了";					
				else if(r < 0.4)
					str = "大家好，我" + imgStr + "是帅气的羔羊，必须去顶啊，羔羊引擎会了能飞天，做游戏不在话下";					
				else if(r < 0.6)
					str = "大家好，我是" + imgStr + "聪明的羔羊，有建议请指教";					
				else if(r < 0.8)
					str = "大家<font size='20'>好，</font>" + imgStr + "我是友善的羔羊，如果你感觉自己编码寸步难行，就要学习羔羊引擎了，让你代码起飞";
				else
					str = "大家好，芋圆好吃啊，有空一块去吃，" + imgStr + "大家好，<font color='#00ff00'>我是</font>一只杠杠<font color='#ffff00'>的</font>羔羊" + imgStr + "不学不要紧啊，不要抹黑就好了，当我透明的";
				// str = "大家好，<font color='#00ff00'>我是</font>一只杠杠<font color='#ffff00'>的</font>羔羊" + imgStr + "不学不要紧啊，不要抹黑就好了，当我透明的";
				s.richText.appendText(str);
			}			
			// s.richScroller.checkBar();
			s.richText.checkOutSize(null,true);
		}
	}
}