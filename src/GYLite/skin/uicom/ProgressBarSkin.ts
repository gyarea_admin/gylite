module GYLite
{			
	/**进度条皮肤*/
	export class ProgressBarSkin implements IProgressBarSkin
	{
		protected _backgroundSkin:GYScaleSprite;
		protected _barSkin:GYScaleSprite;
		protected _textSkin:GYText;
		protected _hostComponent:GYSprite;
//		protected _rotationOffSet:number;
		protected _rotation:number;
		protected _barX:number=0;
		protected _barY:number=0;
		/**@param s.backgroundSkin 背景皮肤
		 * @param s.barSkin 进度条皮肤
		 * @param s.textSkin 文本皮肤 默认null，没有进度文本*/
		public constructor(backgroundSkin:GYScaleSprite,barSkin:GYScaleSprite,t:GYText|TextFormat=null)
		{			
			var s = this;
			s._rotation = 0;
			s._backgroundSkin = backgroundSkin;
			s._barSkin = barSkin;
			s._barSkin.touchChildren = s._barSkin.touchEnabled = false;
			if(t)
			{
				if(CommonUtil.GYIs(t, GYText))
				{
					s._textSkin = <GYText>t;
				}
				else
				{
					s._textSkin = new GYText;
					//s._textSkin.selectable = false;
					s._textSkin.textFormat = <TextFormat>t;
				}
			}
		}
		public release():void
		{var s = this;
			s._backgroundSkin.bitmapData = null;
			s._barSkin.bitmapData = null;
		}
		public get width():number
		{var s = this;
			return s._barSkin.width;
		}
		public get height():number
		{var s = this;
			return s._barSkin.height;
		}
		public set width(val:number)
		{var s = this;
			s._backgroundSkin.width = val;
		}
		public set height(val:number)
		{var s = this;
			s._backgroundSkin.height = val;
		}
		public get textSkin():GYText
		{var s = this;
			return s._textSkin;
		}
		public set barX(val:number)
		{var s = this;
			s._barX = val;
			s._barSkin.x = s._barX;// + s._rotationOffSet;
		}
		public set barY(val:number)
		{var s = this;
			s._barY = val;
			s._barSkin.y = s._barY;
		}
		public set backgroundWidth(val:number)
		{var s = this;
			s._backgroundSkin.width = val;
		}
		public set backgroundHeight(val:number)
		{var s = this;
			s._backgroundSkin.height = val;
		}
		public set barWidth(val:number)
		{var s = this;
			s._barSkin.width = val;
		}
		public set barHeight(val:number)
		{var s = this;
			s._barSkin.height = val;
		}
		public set clipX(val:number)
		{var s = this;
			if(s._barSkin.mode == ScaleMode.SCALE)
				s._barSkin.x = s._barX + val;
			else
				s._barSkin.drawX = s._barSkin.clipX = val;
		}
		public set clipY(val:number)
		{var s = this;
			if(s._barSkin.mode == ScaleMode.SCALE)
				s._barSkin.y = s._barY + val;
			else
				s._barSkin.drawY = s._barSkin.clipY = val;
		}
		public set mode(val:number)
		{var s = this;
			s._barSkin.mode = val;
		}
		public get barX():number
		{var s = this;
			return s._barX;
		}
		public get barY():number
		{var s = this;
			return s._barY;
		}
		public get barWidth():number
		{var s = this;
			return s._barSkin.width;
		}
		public get barHeight():number
		{var s = this;
			return s._barSkin.height;
		}
		public get backgroundWidth():number
		{var s = this;
			return s._backgroundSkin.width;
		}
		public get backgroundHeight():number
		{var s = this;
			return s._backgroundSkin.height;
		}
		public get clipX():number
		{var s = this;
			return s._barSkin.clipX;
		}
		public get clipY():number
		{var s = this;
			return s._barSkin.clipY;
		}
		public get mode():number
		{var s = this;
			return s._barSkin.mode;
		}
		public set rotation(val:number)
		{var s = this;
			// if(s._barSkin.rotation == val)
			// 	return;
//			if(val == 90)
//				s._rotationOffSet = s._backgroundSkin.height;
//			else
//				s._rotationOffSet = 0;
			s._backgroundSkin.rotation = s._rotation = s._barSkin.rotation = val;
//			s._backgroundSkin.x =s._rotationOffSet;
			s.barX = s._barX;
			s.barY = s._barY;
		}
		public get rotation():number
		{var s = this;
			return s._rotation;
		}
		public set hostComponent(val:GYSprite)
		{var s = this;
			if(val==s._hostComponent)
				return;
			if(s._hostComponent)
			{
				if(s._backgroundSkin.parent)
					s._hostComponent.removeElement(s._backgroundSkin);
				if(s._barSkin.parent)
					s._hostComponent.removeElement(s._barSkin);
				if(s._textSkin && s._textSkin.parent)
					s._hostComponent.removeElement(s._textSkin);
			}
			s._hostComponent=val;
			if(s._hostComponent)
			{
				s._hostComponent.addElementAt(s._backgroundSkin, 0);
				s._hostComponent.addElement(s._barSkin);
				
				if(s._textSkin)
					s._hostComponent.addElement(s._textSkin);
			}
		}		
		public get hostComponent():GYSprite
		{var s = this;
			return s._hostComponent;
		}
		/**此函数复制皮肤属性 如s.label gap等皮肤的属性*/
		public copy(skin:IGYSkin)
		{
			if(skin == null)
				return;
			var sk:IProgressBarSkin = skin as IProgressBarSkin;
			var s = this;
			s._textSkin = sk.textSkin;
			s._barSkin.x = sk.barX;
			s._barSkin.y = sk.barY;			
			s._barSkin.clipX = sk.clipX;
			s._barSkin.clipY = sk.clipY;
			s._barSkin.mode = sk.mode;
			s._barSkin.width = sk.barWidth;
			s._barSkin.height = sk.barHeight;
			s._backgroundSkin.width = sk.backgroundWidth;
			s._backgroundSkin.height = sk.backgroundHeight;
			s.rotation = sk.rotation;			
		}
		/**克隆皮肤*/
		public clone():IGYSkin
		{var s = this;
			var proggressSkin:ProgressBarSkin=new ProgressBarSkin(new GYScaleSprite(s._backgroundSkin.bitmapData),new GYScaleSprite(s._barSkin.bitmapData),s._textSkin.textFormat);
			return proggressSkin;
		}

		public get backgroundSkin():GYScaleSprite
		{var s = this;
			return s._backgroundSkin;
		}

		public get barSkin():GYScaleSprite
		{var s = this;
			return s._barSkin;
		}


	}
}