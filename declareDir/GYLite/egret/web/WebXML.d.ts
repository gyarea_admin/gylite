declare namespace egret.web {
    /**
     * @private
     * XML节点基类
     */
    class XMLNode {
        /**
         * @private
         */
        constructor(nodeType: number, parent: XML);
        /**
         * @private
         * 节点类型，1：XML，2：XMLAttribute，3：XMLText
         */
        nodeType: number;
        /**
         * @private
         * 节点所属的父级节点
         */
        parent: XML;
    }
    /**
     * @private
     * XML节点对象
     */
    class XML extends XMLNode {
        /**
         * @private
         */
        constructor(localName: string, parent: XML, prefix: string, namespace: string, name: string);
        /**
         * @private
         * 当前节点上的属性列表
         */
        attributes: {
            [key: string]: string;
        };
        /**
         * @private
         * 当前节点的子节点列表
         */
        children: XMLNode[];
        /**
         * @private
         * 节点完整名称。例如节点 <s:Button/> 的 name 为：s:Button
         */
        name: string;
        /**
         * @private
         * 节点的命名空间前缀。例如节点 <s:Button/> 的 prefix 为：s
         */
        prefix: string;
        /**
         * @private
         * 节点的本地名称。例如节点 <s:Button/> 的 localName 为：Button
         */
        localName: string;
        /**
         * @private
         * 节点的命名空间地址。例如节点 <s:Skin xmlns:s="http://ns.egret.com/eui"/> 的 namespace 为： http://ns.egret.com/eui
         */
        namespace: string;
    }
    /**
     * @private
     * XML文本节点
     */
    class XMLText extends XMLNode {
        /**
         * @private
         */
        constructor(text: string, parent: XML);
        /**
         * @private
         * 文本内容
         */
        text: string;
    }
}
