declare module GYLite {
    class RenderSprite extends GYSprite implements IRenderTestElement {
        private _v;
        private _a;
        private _fillType;
        private _drawType;
        private _colors;
        constructor(fillType?: number, drawType?: number, tex?: egret.Texture);
        private drawGradientLine;
        private drawLine;
        private drawCurve;
        private drawRect;
        private drawRoundRect;
        private drawCircle;
        private drawEllipse;
        private drawTriangle;
        private drawTriangles;
        private drawPoly;
        getRandomColor(): number;
        loop(t: number): void;
        dispose(disposeChild?: boolean, removeChild?: boolean, forceDispose?: boolean): void;
    }
}
