declare namespace egret.web {
    /**
     * @private
     */
    function convertImageToCanvas(texture: egret.Texture, rect?: egret.Rectangle): HTMLCanvasElement;
}
