declare module GYLite {
    class TextInputSkin extends GYSkin implements IGYSkin {
        protected _background: egret.Texture;
        protected _backScaleShape: GYScaleSprite;
        protected _rect: Scale9GridRect;
        constructor(back?: egret.Texture | GYScaleSprite, rect?: Scale9GridRect);
        release(): void;
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        set width(value: number);
        set height(value: number);
        set background(val: egret.Texture);
        get background(): egret.Texture;
        set scale9GridRect(val: Scale9GridRect);
        get scale9GridRect(): Scale9GridRect;
        clone(): IGYSkin;
    }
}
