declare module GYLite {
    class AtlasManager {
        private _atlasSetting;
        private _atlasVec;
        private _atlasId;
        /**允许同时缓存的图集数量，默认20*/ maxAtlasNum: number;
        /**创建图集的宽度*/ atlasWidth: number;
        /**创建图集的高度*/ atlasHeight: number;
        constructor();
        /**对atlasName图集进行预设信息*/
        setAtlasSetting(atlasName: string, setting: IAtlasSetting): void;
        get atlasNum(): number;
        /**获取图集矩形数组*/
        get atlasVec(): Array<AtlasRect>;
        /**从图集管理器的图集队列中移除图集*/
        removeAtlas(atlasRect: AtlasRect): void;
        /**插入纹理
         * @param texName 小图名称
         * @param atlasName 图集名称
         * @param w 图片宽度
         * @param h 图片高度
         * @param type 裁剪策略 排列方式0 偏向正方形 1 优先横向 2 优先纵向,默认1
         * @param gap 图片之间的间隙，默认1
        */
        addTexture(texName: string, atlasName: string, w: number, h: number, type?: number, gap?: number): {
            atlasRect: AtlasRect;
            atlasInfo: AtlasInfo;
            error: SysError;
        };
        /**创建一个虚拟图集
         * @param w 宽度
         * @param h 高度
         * @param atlasName 图集名称
        */
        createAtlas(w: number, h: number, atlasName: string, result?: {
            atlasRect: AtlasRect;
            error: SysError;
        }, check?: boolean): {
            atlasRect: AtlasRect;
            error: SysError;
        };
        /**删除纹理
         * @param texName 小图名称
        */
        removeTexture(texName: string): void;
        private createSheetJson;
        /**输入虚拟图集数据*/
        inputJson(atlasArray: any[]): void;
        /**输出虚拟图集数据*/
        outputJson(): any[];
    }
    interface IAtlasSetting {
        /**图集宽度*/ atlasWidth: number;
        /**图集高度*/ atlasHeight: number;
    }
}
