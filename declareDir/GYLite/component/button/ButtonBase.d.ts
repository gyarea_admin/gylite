/**
 @author 迷途小羔羊
 2014.12.5
 */
declare module GYLite {
    class ButtonBase extends GYSkinContainer {
        constructor(skin?: any);
        protected initComponent(): void;
        set label(val: string);
        /**当s.label被赋值的时候自动产生Mytext文本(在此之前不存在s.labelDisplay)，在s.label再赋值为null时，文本不会被清除，暂时从显示列表移除*/
        get label(): string;
        /**获取文本对象*/
        get labelDisplay(): GYText;
        set width(val: number);
        set height(val: number);
        get width(): number;
        get height(): number;
        /**按钮状态改变
         * 状态请参考ButtonBase内部常量定义
         * */
        stateChange(): void;
        /**s.skin皮肤Class类型，自定义皮肤请实现IButtonSkin接口，传入实例将自动clone副本(不包括s.labelDisplay) */
        set skin(val: any);
        get skin(): any;
        /**获取主题皮肤，自定义皮肤请实现IButtonSkin接口*/
        protected getThemeSkin(): IGYSkin;
        updateView(): void;
        /**刷新状态*/
        invalidState(): void;
        protected _toggle: boolean;
        protected _selected: boolean;
        protected _enabled: boolean;
        protected _state: number;
        protected _clickFunc: Function;
        protected _clickThisObject: any;
        protected _stsChange: boolean;
        /**抬起*/ static STATE_UP: number;
        /**经过*/ static STATE_OVER: number;
        /**按下*/ static STATE_DOWN: number;
        /**不可用*/ static STATE_DISABLE: number;
        /**选中时经过*/ static STATE_SELECTOVER: number;
        /**选中时按下*/ static STATE_SELECTDOWN: number;
        /**选中时*/ static STATE_SELECT: number;
        /**选中时不可用*/ static STATE_SELECTDISABLE: number;
    }
}
