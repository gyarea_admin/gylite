module GYLite
{
	export interface IUpdate extends IResource
	{
		updateView():void;		
		updating:boolean
	}
}