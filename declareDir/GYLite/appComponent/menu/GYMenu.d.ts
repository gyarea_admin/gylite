declare module GYLite {
    /**下拉菜单, 可以使用ISkinTheme的GetMenu快速构建
     * */
    class GYMenu extends GYSkinContainer {
        static default_autoSize: boolean;
        static default_autoClose: boolean;
        protected _filterArr: Array<any>;
        protected _sourceArr: Array<any>;
        protected _autoSize: boolean;
        protected _autoClose: boolean;
        protected _filterFunc: Function;
        /***
         * @param skin 参见MenuSkin
         * */
        constructor(skin?: IMenuSkin);
        protected initComponent(): void;
        /**设置数据源*/
        set dataProvider(val: Array<any>);
        get dataProvider(): Array<any>;
        /**纵向滚动条*/
        get vScroller(): GYScrollBarV;
        /**菜单列表*/
        get list(): GYListV;
        set list(val: GYListV);
        /**菜单背景*/
        get menuBack(): GYListV;
        set menuBack(val: GYListV);
        set autoSize(val: boolean);
        /**是否自适应高度*/
        get autoSize(): boolean;
        /**滚动条 0自动 1显示 2不显示*/
        set scrollerPolicy(val: number);
        get scrollerPolicy(): number;
        /**设置过滤函数s.setFilterFunc,此函数有一个参数Array源数组，一个参数Array过滤数组、一个参数result输入框的内容，过滤数组放入符合的选项
         * val(Array;Array;String)
         * */
        setFilterFunc(val: Function): void;
        /**使用过滤函数s.filterFunc过滤无用的选项
         * @param 过滤的字符串参数
         * */
        userFilter(result: string): Array<any>;
        /**重写此方法，提供菜单项*/
        protected getMenuItem(): IItemRender;
        /**重写此方法，设置菜单项数据*/
        protected setMenuItem(item: IItemRender, data: any): void;
        /**获取主题皮肤，自定义皮肤请实现IMenuSkin接口*/
        protected getThemeSkin(): IGYSkin;
        /**外部点击自动关闭*/
        get autoClose(): boolean;
        set autoClose(value: boolean);
        private addToStage;
        protected outsideRelease(e: GYTouchEvent): void;
    }
}
