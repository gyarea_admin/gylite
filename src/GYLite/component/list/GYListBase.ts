/**
 @author 迷途小羔羊
 2015.3.5
 */
module GYLite
{
	export class GYListBase extends GYGroup implements IList
	{
		protected _hScroller:GYScrollBarH;
		protected _vScroller:GYScrollBarV;
		protected _innerHeight:number;
		protected _innerWidth:number;
		protected _boxs:number;
		protected _padding:number;
		protected _dataProvider:Array<any>;
		protected _grids:Array<any>;
		protected _boxW:number;
		protected _boxH:number;
		protected createGrid:Function;
		protected setGridData:Function;
		protected _lastBox:number;
		protected _max:number;
		protected _scrollerPolicy:number;//滚动条 0自动 1显示 2不显示
		protected _selectedData:any;
		protected _dataUpate:boolean;
		protected _gridUpdate:boolean;
		protected _gridSizeChange:boolean;
		protected _gridDataUpdate:boolean;
		protected _canSelectNum:number;
		protected _boxNumUpdate:boolean;
		protected _boxNumUpdateReset:boolean;
		protected _selectList:any[];
		protected _dataToItemDict:Dictionary;
		protected _keepSelected:boolean;
		protected _nextData:any;
		protected _selectIndex:number;
		protected _wheelScroll:boolean;
		protected _wheelStep:number;
		protected _stopSelTarget:any;
		private _selectTime:number;
		private _selectInterval:number;
		private _dragSelect:boolean;
		protected _mouseSelect:boolean;
		private _addSelect:boolean;
		protected _setThisObject:any;
		protected _getThisObject:any;
		protected _dragPreEnabled:boolean;
		protected _dragNextEnabled:boolean;
		protected _virtual_layout:boolean;
		protected _scrollPosLimit:number;
		protected _scrollToPos:number;
		protected _overlying:boolean;		
		/**列表是否可选(默认true)*/public selectable:boolean;
		/**速度系数，乘以滑动的速度*/public speedParam:number;
		/**区间限制的时候，恢复到区间位置的滚动花费时间*/public scrollNextTime:number;
		/**List组件的基类 继承后自定义其逻辑或者直接使用GYListV GYListH GYDataListV GYDataListH
		 * @param size list尺寸（纵向ListV为高度，横向ListH则为宽度）设定后无法更，当为负数的时候则表示为boxCount，此时则需要另外设置list宽高和项宽高（s.boxH和s.boxW） 
		 * @param getGridFunc():IItemRender 返回自定义格子对象的方法 
		 * @param setGridFunc(IItemRender, any) 设置格子数据的方法*/
		public constructor(size:number,getGridFunc:Function=null, getThisObject:any=null, setGridFunc:Function=null, setThisObject:any=null)
		{
			super();
			var s = this;
			s.selectable = true;
			s.speedParam = 1;
			s.scrollNextTime = NaN;
			s._overlying = true;
			s._scrollPosLimit = NaN;
			s._dragPreEnabled = true;
			s._dragNextEnabled = true;
			s._boxs=size;
			s.createGrid=getGridFunc;
			s.setGridData=setGridFunc;
			s._getThisObject = getThisObject;
			s._setThisObject = setThisObject;
			s.virtual_layout = true;					
			s.initComponent();			
		}
		protected initComponent():void
		{
			let s = this;
			s._innerHeight=NaN;
			s._innerWidth=NaN;			
			s._padding=0;
			s._dataProvider=null;
			s._grids=null;
			s._boxW=NaN;
			s._boxH=NaN;			
			s._lastBox=0;
			s._max=0;
			s._scrollerPolicy=0;
			s._selectedData=null;
			s._dataUpate=false;
			s._gridUpdate=false;
			s._gridSizeChange=false;
			s._gridDataUpdate=false;
			s._canSelectNum=1;
			s._selectList=null;
			s._dataToItemDict=null;
			s._keepSelected=false;
			s._nextData=null;								
			s._stopSelTarget=null;
			s._selectTime=NaN;			
			s._dragSelect=false;
			s._mouseSelect=false;
			s._addSelect=false;

			s._selectIndex = -1;
			s._selectInterval = GYListBase.default_selectInterval;
			s._wheelStep = GYListBase.default_wheelStep;
			s.wheelScroll = GYListBase.default_wheelScroll;
			s.clipAndEnableScrolling = true;
			s.touchEnabled = true;
			s._lastBox=0;
			s._grids = [];
		}
		protected getGrid():IItemRender
		{
			let s = this;
			var item:IItemRender;
			if(s.createGrid!=null)
				item = s.createGrid.call(s._getThisObject);
			else
				item = new ItemRender;
			if(s._boxW == s._boxW)
				item.width = s._boxW;
			if(s._boxH == s._boxH)
				item.height = s._boxH;
			item.owner = this;
			if(s._dragSelect)
			{
				item.addEventListener(egret.TouchEvent.TOUCH_BEGIN, s.selectCell,s);
				item.addEventListener(MouseEvent.ROLL_OVER, s.selectCell,s);
			}
			else
				item.addEventListener(egret.TouchEvent.TOUCH_TAP, s.selectCell,s);
			return item;
		}
		protected setGrid(grid:IItemRender, obj:any):void
		{
			let s = this;
			var d:any = grid.getData();
			if(s._dataToItemDict)
			{
				if(s._dataToItemDict.getValue(d) == grid)
					s._dataToItemDict.deleteKey(d);
				if(obj)
					s._dataToItemDict.setValue(obj, grid);
			}
			if(s.dataIsSelected(obj))
			{
				if(!grid.selected)grid.selected = true;
			}
			else
				if(grid.selected)grid.selected = false;
			if(s.setGridData != null)
				s.setGridData.call(s._setThisObject,grid,obj);
			else
				grid.setData(obj);
		}
		protected selectCell(e:egret.TouchEvent):void
		{
			let s = this;
			if(!s.selectable)return;
			if(s._stopSelTarget == e.target)
			{
				s._stopSelTarget = null;
				return;
			}
			if(s._canSelectNum == 0)
				return;
			var item:IItemRender = e.currentTarget as IItemRender;
			var stInd:number,endInd:number;
			var ind:number;
			var i:number,j:number,len:number;
			var isFind:boolean;
			var tempItem:IItemRender;
			var data:any,tempData:any;
			var end:number;
			var tempVec:any[];
			if(s._dragSelect)
			{
				if((e.type == "rollOver"  && !GYSprite.isStageDown(s))|| !e.touchDown)
					return;
				GYSprite.addStageDown(s, s.releaseOutSide, s);
				s.addEventListener(MouseEvent.ROLL_OUT, s.selectOut,s);
				s.addEventListener(MouseEvent.ROLL_OVER, s.selectIn,s);
				s.addEventListener(egret.TouchEvent.TOUCH_END,s.releaseOutSide,s);
			}
			if(s._canSelectNum > 1)
			{
				data = item.getData();
				if((e.type == "rollOver" || GYKeyboard.getInstance().isShiftDown()) && s._selectedData)
				{
					if(data)
					{
						s._mouseSelect = true;
						s.selectLine(data,s._selectedData,GYKeyboard.getInstance().isCtrlDown());
					}
					return;
				}
				else if(GYKeyboard.getInstance().isCtrlDown())
				{
					if(data)
					{	
						ind = s._selectList.indexOf(data);
						if(ind == -1)
						{
							if(s._canSelectNum <= s._selectList.length)
								return;
							s._selectList.push(data);
							item.selected = true;
						}
						else
						{
							s._selectList.splice(ind,1);
							item.selected = false;
						}
					}
					s._addSelect = true;
				}
				else
				{
					len = s._selectList.length;
					while(--len>-1)
					{
						if(s._selectList[len] == data)
						{
							isFind = true;
							continue;
						}
						tempItem = s.dataToItemDict.getValue(s._selectList[len]);
						if(tempItem)
						{
							tempItem.selected = false;
						}
					}
					s._selectList.length = 0;
					s._selectList.push(data);
					if(!isFind && s.dataToItemDict.getValue(data))
						s.dataToItemDict.getValue(data).selected = true;
				}
			}
			s._mouseSelect = true;
			s.selectedItem = item;
		}
		protected selectLine(data:any,selectedData:any,ctrlKey:boolean,checkNext:boolean=false):void
		{
			let s = this;
		}
		private selectIn(e:egret.TouchEvent):void
		{
			let s = this;
			CommonUtil.delStageLoop(s.selectLoop,s);
		}
		private selectOut(e:egret.TouchEvent):void
		{
			let s = this;
			s._selectTime = egret.getTimer();
			CommonUtil.addStageLoop(s.selectLoop,s);
		}
		private selectLoop(t:number):void
		{
			let s = this;
			if(t - s._selectTime > s._selectInterval)
			{
				s._mouseSelect = true;
				s.selectLine(s._nextData, s._selectedData, false, true);
			}
		}
		/**数据是否被选中*/
		public dataIsSelected(d:any):boolean
		{
			let s = this;
			if(d == null)return false;
			if(s.canSelectNum == 1)
			{
				return s._selectedData == d;
			}
			if(s.canSelectNum > 1)
			{
				return s._selectList.indexOf(d) > -1;
			}
			return false;
		}
		/**跳转至某索引项*/
		public scrollToIndex(val:number):void
		{
			let s = this;
			
		}
		/**跳转至某索引项(位置在末尾)*/
		public scrollToEndIndex(val:number):void
		{
			let s = this;
			
		}
		private releaseOutSide(e:egret.TouchEvent):void
		{
			let s = this;
			CommonUtil.delStageLoop(s.selectLoop,s);
			s.removeEventListener(MouseEvent.ROLL_OUT, s.selectOut,s);
			s.removeEventListener(MouseEvent.ROLL_OVER, s.selectIn,s);
		}
		public set virtual_layout(val)
		{let s= this;
			if(s._virtual_layout == val)return;
			s._virtual_layout = val;
			s.outSideOptimize = !val;
			if(s._dataProvider)s.boxs = s._dataProvider.length;
		}
		public get virtual_layout()
		{
			return this._virtual_layout;
		}
		public set canSelectNum(val:number)
		{
			let s = this;
			if(val > 0)
			{
				s.dataToItemDict;
			}
			else
			{
				s.selectedItem = null;
				if(s._selectList)
					s._selectList.length = 0;
				s.invalidData();
			}
				
			s._canSelectNum = val;
		}
		/**同时能选择的数量*/
		public get canSelectNum():number
		{
			let s = this;
			return s._canSelectNum;
		}
		/**多选时的选择列表*/
		public get selectList():any[]
		{
			let s = this;
			return s._selectList;
		}
		public set selectList(val:any[])
		{
			let s = this;
			let i:number,len:number;
			if(s._canSelectNum < 1)return;
			if(s._selectList && s._selectList.length > 0)
			{
				len = s._selectList.length;
				for(i=0;i<len;++i)
				{
					if(s._selectList[i])
					{
						s._selectIndex = -1;
						s._selectedData = null;
						if(s._selectList[i] && s.dataToItemDict.getValue(s._selectList[i]))
							s.dataToItemDict.getValue(s._selectList[i]).selected = false;
					}
				}	
				s._selectList.length = 0;
			}						
			if(val)
			{
				len = val.length;
				for(i=0;i<len;++i)
				{
					if(val[i])
						s.selectedData = val[i];
				}
			}			
		}
		/**选择的数据索引*/
		public get selectedIndex():number
		{
			let s = this;
			return s._selectIndex;
		}
		public set selectedIndex(val:number)
		{
			let s = this;
			var len:number;
			if(s.canSelectNum < 1)return;
			if(s._selectIndex == val)return;
			if(val >= s._dataProvider.length)return;
			if(val < 0)return;
			s._selectIndex = val;
			s.selectedData = s._dataProvider[val];
		}		
		public set selectedItem(val:IItemRender)
		{
			let s = this;
			var oldData:any;
			oldData = s._selectedData;
			s._nextData = s._selectedData = val?val.getData():null;
			if(!s._addSelect && oldData && oldData != s._selectedData && s.dataToItemDict.getValue(oldData))
				s.dataToItemDict.getValue(oldData).selected = false;
			if(val == null || val.getData() == null)
			{
				if(s._selectList)s._selectList.length = 0;
				s._selectIndex = -1;
			}
			else
			{
				s._selectIndex = s.dataToItemDict.getValue(s._selectedData).itemIndex;
				if(!s._addSelect)
					val.selected = true;
			}
			s._addSelect = false;
			if(s.hasEventListener(GYViewEvent.SELECTED) && s._mouseSelect)
			{
				s.dispatchEvent(new GYViewEvent(GYViewEvent.SELECTED));
				s._mouseSelect = false;
			}
		}
		/**当前选中的ItemRender*/
		public get selectedItem():IItemRender
		{
			let s = this;
			return s.dataToItemDict.getValue(s._selectedData);
		}
		public getItemByData(d:any):IItemRender
		{
			let s = this;
			return s.dataToItemDict?s._dataToItemDict.getValue(d):null;
		}
		public set selectedData(d:any)
		{
			let s = this;
			if(s._canSelectNum == 0)return;
			if(s._selectedData == d)return;
			var oldData:any;
			oldData = s._selectedData;
			s._selectedData = d;
			if(s.canSelectNum > 1)
			{
				var ind:number = s._selectList.indexOf(s._selectedData);
				if(ind == -1)
				{
					s._selectList.push(s._selectedData);
				}
			}
			else if(oldData && s.dataToItemDict.getValue(oldData))
			{
				s.dataToItemDict.getValue(oldData).selected = false;
			}
			s._selectIndex = d == null?-1:(CommonUtil.GYIs(s.dataToItemDict.getValue(d),"GYLite.IItemRender")?s.dataToItemDict.getValue(d).itemIndex:-1);
			if(d && s.dataToItemDict.getValue(s._selectedData))
				s.dataToItemDict.getValue(s._selectedData).selected = true;
		}
		public get selectedData():any
		{
			let s = this;
			return s._selectedData;
		}
		public set boxW(val:number)
		{
			let s = this;
			s._boxW=val;
			s._gridSizeChange = true;
			s.invalidGrids();
		}
		public set boxH(val:number)
		{
			let s = this;
			s._boxH=val;
			s._gridSizeChange = true;
			s.invalidGrids();
		}
		/**格子宽度(ItemRender不等宽高的列表时，仅为默认值)*/
		public get boxW():number
		{
			let s = this;
			return s._boxW;
		}
		/**格子高度(ItemRender不等宽高的列表时，仅为默认值)*/
		public get boxH():number
		{
			let s = this;
			return s._boxH;
		}
		public set padding(val:number)
		{
			let s = this;
			s._padding=val;
			s._gridSizeChange = true;
			s.invalidGrids();
		}
		/**间距*/
		public get padding():number
		{
			let s = this;
			return s._padding;
		}
		public set boxs(val:number)
		{
			this._boxs = val;
		}
		public get boxs():number
		{
			let s = this;
			return s._boxs;
		}
		
		public set dataProvider(val:Array<any>)
		{
			let s = this;
			s._dataProvider =val;
		}
		/**数据源*/
		public get dataProvider():Array<any>
		{
			let s = this;
			return s._dataProvider;
		}
		
		/**数据失效*/
		public invalidData():void
		{
			let s = this;
			if(s._dataUpate)
				return;
			s._dataUpate = true;
			s.displayChg();
		}

		/**格子数量失效*/
		public invalidBoxNum():void
		{
			let s = this;
			if(s._boxNumUpdate)
				return;
			s._boxNumUpdate = true;
			s.displayChg();
		}
		
		/**网格布局失效*/
		public invalidGrids():void
		{
			let s = this;
			if(s._gridUpdate)
				return;
			s._gridUpdate = true;
			s.displayChg();
		}
		
		/**网格数据失效*/
		public invalidGridData():void
		{
			let s = this;
			if(s._gridDataUpdate)
				return;
			s._gridDataUpdate = true;
			s.displayChg();
		}
		
		public updateView():void
		{
			let s = this;
			if(s._gridDataUpdate)
			{
				s.updateGridData();
				s._gridDataUpdate = false;
			}
			if(s._boxNumUpdate)
			{
				s.boxNumChange(s._boxNumUpdateReset);
				s._boxNumUpdateReset = s._boxNumUpdate = false;
			}
			if(s._dataUpate)
			{
				s.updateData();
				s._dataUpate = false;
			}
			if(s._gridUpdate)
			{
				s.updateGrid();
				s._gridUpdate = false;
				s._gridSizeChange = false;
			}
			super.updateView();
		}
		/**刷新格子布局*/
		public updateGrid():void
		{
			let s = this;
			
		}
		/**刷新数据*/
		public updateData():void
		{
			let s = this;
			s.dataProvider = s._dataProvider;
		}
		/**刷新格子数据*/
		public updateGridData():void
		{
			let s = this;
			
		}
		/**移除索引位置上的数据项（刷新）*/
		public removeItemAt(ind:number):void
		{
			let s = this;
			if(ind < s._dataProvider.length)
			{
				s._dataProvider.splice(ind, 1);
				s.updateData();
			}
		}
		/**移除数据项（刷新）*/
		public removeItem(d:any):void
		{
			let s = this;
			var ind:number = s._dataProvider.indexOf(d);
			if(ind > -1)
			{
				s._dataProvider.splice(ind, 1);
				s.updateData();
			}
		}
		/**添加索引位置数据项（刷新）*/
		public addItemAt(d:any, ind:number):void
		{
			let s = this;
			if(ind <= s._dataProvider.length)
			{
				s._dataProvider.splice(ind,0,d);
				s.updateData();
			}
		}
		/**添加数据项（刷新）*/
		public addItem(d:any):void
		{
			let s = this;
			s._dataProvider.push(d);
			s.updateData();
		}
		/**刷新数据项*/
		public updateItem(d:any):void
		{
			let s = this;
			if(s.dataToItemDict.getValue(d))
				(s.dataToItemDict.getValue(d) as IItemRender).setData(d);
		}
		/**刷新格子数*/
		protected boxNumChange(update:boolean=false):void
		{
			
		}
		/**刷新列表可见的所有项*/
		public updateItems():void
		{
			
		}

		/**拖选时的滚动时间间隔(毫秒)*/
		public get selectInterval():number
		{
			let s = this;
			return s._selectInterval;
		}

		public set selectInterval(value:number)
		{
			let s = this;
			s._selectInterval = value;
		}
		/**是否启用拖选功能*/
		public get dragSelect():boolean
		{
			let s = this;
			return s._dragSelect;
		}

		public set dragSelect(value:boolean)
		{
			let s = this;
			s._dragSelect = value;
			var len:number;
			var item:IItemRender;
			len = s._grids.length;
			while(--len>-1)
			{
				item = s._grids[len];
				if(s._dragSelect)
				{
					item.removeEventListener(egret.TouchEvent.TOUCH_TAP, s.selectCell,s);
					item.addEventListener(egret.TouchEvent.TOUCH_BEGIN, s.selectCell,s);
					item.addEventListener(MouseEvent.ROLL_OVER, s.selectCell,s);
				}
				else
				{
					item.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, s.selectCell,s);
					item.removeEventListener(MouseEvent.ROLL_OVER, s.selectCell,s);
					item.addEventListener(egret.TouchEvent.TOUCH_TAP, s.selectCell,s);
				}
			}
		}
		/**当次选择排除的点击对象，如ItemRender内部有按钮，避免点击当成选择，可临时设置此属性，点击后将被设置为null*/
		public get stopSelTarget():any
		{
			let s = this;
			return s._stopSelTarget;
		}

		public set stopSelTarget(value:any)
		{
			let s = this;
			s._stopSelTarget = value;
		}
		/**是否保留选择的数据项*/
		public get keepSelected():boolean
		{
			let s = this;
			return s._keepSelected;
		}

		public set keepSelected(value:boolean)
		{
			let s = this;
			s._keepSelected = value;
		}

		public get nextData():any
		{
			let s = this;
			return s._nextData;
		}
		/**是否启用滚轮*/
		public get wheelScroll():boolean
		{
			let s = this;
			return s._wheelScroll;
		}

		public set wheelScroll(value:boolean)
		{
			let s = this;
			s._wheelScroll = value;
			if(s._wheelScroll)			
				s.setWheelFunc(s.wheelRoll,s);						
			else		
				s.setWheelFunc(null);
			
		}
		protected wheelRoll(e:egret.TouchEvent):void
		{
			let s = this;
			
		}

		public get wheelStep():number
		{
			let s = this;
			return s._wheelStep;
		}

		public set wheelStep(value:number)
		{
			let s = this;
			s._wheelStep = value;
		}

		/**除去滚动条的宽度*/
		public get innerWidth():number
		{
			return this._innerWidth;
		}
		/**除去滚动条的高度度*/
		public get innerHeight():number
		{
			return this._innerHeight;
		}
		/**获取数据对应的Item字典*/
		public get dataToItemDict():Dictionary
		{
			let s = this;
			if(s._dataToItemDict==null)
			{
				s._selectList = new Array<any>();
				s._dataToItemDict = new Dictionary();
				var len:number = s._grids.length;
				while(--len>-1)
				{
					var obj:any = s._grids[len].getData();
					if(obj)
						s._dataToItemDict.setValue(obj, s._grids[len]);
				}
			}
			return s._dataToItemDict;
		}

		public set dragNextEnabled(val:boolean)
		{let s = this;
			s._dragNextEnabled = val;
		}
		/**禁止拖动到下一项*/
		public get dragNextEnabled():boolean
		{let s = this;
			return s._dragNextEnabled;
		}
		/**禁止拖动到上一项*/
		public set dragPreEnabled(val:boolean)
		{let s = this;
			s._dragPreEnabled = val;
		}
		public get dragPreEnabled():boolean
		{let s = this;
			return s._dragPreEnabled;
		}
		/**滚动区间限制,限制滚动停止的位置以相等的距离*/
		public set scrollPosLimit(val:number)
		{let s = this;
			s._scrollPosLimit = val;
		}
		public get scrollPosLimit():number
		{let s = this;
			return s._scrollPosLimit;
		}
		/**滚动条 0自动 1显示 2不显示*/
		public set scrollerPolicy(val:number)
		{
		}
		public get scrollerPolicy():number
		{
			return 0;
		}
		/**滚动到下一项
		 * @return 返回滚动的目标位置
		*/
		public scrollToNextItem():number
		{
			return 0;
		}
		/**滚动到上一项
		 * @return 返回滚动的目标位置
		*/
		public scrollToPreItem():number
		{
			return 0;
		}
		protected scrollEnd(e:GYScrollerEvent=null):void
		{
			
		}
		/**scrollToNextItem、scrollToPreItem是否叠加滚动，默认true*/
		public get overlying():boolean
		{
			return this._overlying;
		}
		public set overlying(val:boolean)
		{
			this._overlying = val;
		}		
		protected vScrollChange(e:GYScrollerEvent=null, update:boolean = false, updatePos:boolean = true){}
		protected hScrollChange(e:GYScrollerEvent=null, update:boolean = false, updatePos:boolean = true){}
		public get vScroller():GYScrollBarV
		{
			let s= this;
			if(s._vScroller == null)			
				s.vScroller=new GYScrollBarV;			
			return s._vScroller;
		}
		public set vScroller(val:GYScrollBarV)
		{
			let s= this;			
			if(s._vScroller)
			{
				if(val)
				{
					val.height = s._vScroller.height;
					val.value = s._vScroller.value;		
					val.maximum = s._vScroller.maximum;
					val.barPercent = s._vScroller.barPercent;				
					val.x = s._vScroller.x;
					val.y = s._vScroller.y;
					val.layoutMode = s._vScroller.layoutMode;			
				}
				s._vScroller.removeEventListener(GYScrollerEvent.SCROLL_CHANGE, s.vScrollChange,s);
				s._vScroller.removeEventListener(GYScrollerEvent.SCROLL_TWEEN_END,s.scrollEnd,s);				
				s._vScroller.dispose();
			}
			else
			{
				val.right = 0;
				val.maximum = 0;
				val.value = 0;				
			}
			s._vScroller = val;
			val.addEventListener(GYScrollerEvent.SCROLL_CHANGE, s.vScrollChange,s);
			val.addEventListener(GYScrollerEvent.SCROLL_TWEEN_END,s.scrollEnd,s);			
			s.scrollerPolicy = s._scrollerPolicy;
		}
		public get hScroller():GYScrollBarH
		{
			let s= this;
			if(s._hScroller == null)			
				s.hScroller=new GYScrollBarH;
			return s._hScroller;
		}
		public set hScroller(val:GYScrollBarH)
		{
			let s= this;			
			if(s._hScroller)
			{
				if(val)
				{
					val.width = s._hScroller.width;					
					val.value = s._hScroller.value;	
					val.maximum = s._hScroller.maximum;
					val.barPercent = s._hScroller.barPercent;				
					val.x = s._hScroller.x;
					val.y = s._hScroller.y;
					val.layoutMode = s._hScroller.layoutMode;				
				}
				s._hScroller.removeEventListener(GYScrollerEvent.SCROLL_CHANGE, s.hScrollChange,s);
				s._hScroller.removeEventListener(GYScrollerEvent.SCROLL_TWEEN_END,s.scrollEnd,s);				
				s._hScroller.dispose();
			}
			else
			{
				val.maximum = 0;
				val.value = 0;
				val.bottom = 0;				
			}
			s._hScroller = val;
			val.addEventListener(GYScrollerEvent.SCROLL_CHANGE, s.hScrollChange,s);
			val.addEventListener(GYScrollerEvent.SCROLL_TWEEN_END,s.scrollEnd,s);			
			s.scrollerPolicy = s._scrollerPolicy;
		}

		public clone():GYListBase
		{
			return null;
		}

		/**拖选响应毫秒(默认值)*/public static default_selectInterval:number = 50;
		/**滚轮步长(默认值)*/public static default_wheelStep:number = 0;
		/**是否允许滚轮(默认值)*/public static default_wheelScroll:boolean = true;
	}
}