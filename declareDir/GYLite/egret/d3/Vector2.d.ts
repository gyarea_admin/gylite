declare module GYLite {
    class Vector2 {
        elements: Float32Array;
        constructor(x?: number, y?: number);
        /**
         *从Array数组拷贝值。
            *@param array 数组。
            *@param offset 数组偏移。
            */
        fromArray(array: number[], offset?: number): void;
        /**
         *克隆。
            *@param destObject 克隆源。
            */
        cloneTo(destObject: Vector2): void;
        /**
         *克隆。
            *@return 克隆副本。
            */
        clone(): Vector2;
        /**
         *获取X轴坐标。
            *@return X轴坐标。
        */
        get x(): number;
        /**
         *设置X轴坐标。
            *@param value X轴坐标。
            */
        set x(value: number);
        /**
         *获取Y轴坐标。
            *@return Y轴坐标。
        */
        get y(): number;
        /**
         *设置Y轴坐标。
            *@param value Y轴坐标。
        */
        set y(value: number);
        static scale(a: Vector2, b: number, out: Vector2): void;
        static ZERO: Vector2;
        static ONE: Vector2;
    }
}
