/**
 @author 迷途小羔羊
 2015.5.25
 */
module GYLite
{
											
			

	export class GYTab extends GYDataGrid
	{
		protected _lastGroup:GYUIComponent;
		protected _lastTab:TabData;
		protected _selGroup:GYUIComponent;
		protected _selTab:TabData;
		protected _autoTop:boolean;
		public constructor(cols:number=0, rows:number=0, getGridFunc:Function=null, getThisObject:any=null, setGridFunc:Function=null, setThisObject:any=null)
		{
			super(cols, rows, getGridFunc, getThisObject, setGridFunc, setThisObject);
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();
			s._lastGroup = null;
			s._selGroup = null;
			s._autoTop = false;
			s.canSelectNum = 1;
		}
		protected getGrid():IItemRender
		{
			let s = this;
			var item:IItemRender;
			if(s.createGrid!=null)
				item = s.createGrid.call(s._getThisObject);
			else
				item = new GYTabItemRender;
			item.addEventListener(egret.TouchEvent.TOUCH_BEGIN, s.selectCell,s);
			item.owner = this;
			return item;
		}
		public set selectedData(d:any)
		{
			let s = this;
			var oldData:any = s._selectedData;
			egret.superSetter(GYTab, s, "selectedData", d);			
			s.selDataChange(oldData, d);
		}
		public get selectedData():any
		{
			let s = this;
			return egret.superGetter(GYTab, s, "selectedData");	
		}
		public set selectedItem(val:IItemRender)
		{
			let s = this;
			var oldData:any = s._selectedData;
			egret.superSetter(GYTab, s, "selectedItem", val);					
			s.selDataChange(oldData, s._selectedData);
		}
		public get selectedItem():IItemRender
		{
			let s = this;
			return egret.superGetter(GYTab, s, "selectedItem");
		}
		protected selDataChange(oldData:TabData, newData:TabData):void
		{
			let s = this;
			if(oldData)
			{		
				if(oldData != newData)		
				{
					oldData.selected = false;
					s._lastTab = oldData;
					s._lastGroup = s._lastTab.group;
				}
			}
			else
				s._lastGroup = null;
			if(newData)
			{
				newData.selected = true;
				s._selTab = newData;
				s._selGroup = s._selTab.group;
			}
			else
				s._selGroup = null;
			if(s._autoTop)
			{
				if(s.selectedItem)
				{
					var p:GYSprite = (s.selectedItem.parent as GYSprite);
					p.setElementIndex(s.selectedItem,p.numElement - 1);
				}
			}
		}

		/**GYTab只允许选择一个*/
		public get canSelectNum():number
		{let s = this;
			return egret.superGetter(GYTab, s, "canSelectNum");		
		}
		public set canSelectNum(val:number)
		{
			let s = this;
			egret.superSetter(GYTab, s, "canSelectNum", 1);				
		}
		/**当前选中的组*/
		public get selGroup():GYUIComponent
		{
			let s = this;
			return s._selGroup;
		}
		/**上次选中的组*/
		public get lastGroup():GYUIComponent
		{
			let s = this;
			return s._lastGroup;
		}
		/**当前选中的Tab*/
		public get selTab():TabData
		{
			let s = this;
			return s._selTab;
		}
		/**上次选中的Tab*/
		public get lastTab():TabData
		{
			let s = this;
			return s._lastTab;
		}
		/**是否选中自动置顶*/
		public get autoTop():boolean
		{
			let s = this;
			return s._autoTop;
		}
		public set autoTop(val:boolean)
		{
			let s = this;
			s._autoTop = val;
		}
	}
}