declare namespace egret.sys {
    let $TempStage: egret.Stage;
    /**
     * @private
     * Egret播放器
     */
    class Player extends HashObject {
        /**
         * @private
         * 实例化一个播放器对象。
         */
        constructor(buffer: RenderBuffer, stage: Stage, entryClassName: string);
        /**
         * @private
         */
        private createDisplayList;
        /**
         * @private
         */
        private screenDisplayList;
        /**
         * @private
         * 入口类的完整类名
         */
        private entryClassName;
        /**
         * @private
         * 舞台引用
         */
        stage: Stage;
        /**
         * @private
         * 入口类实例
         */
        private root;
        /**
         * @private
         */
        private isPlaying;
        /**
         * @private
         * 启动播放器
         */
        start(): void;
        /**
         * @private
         */
        private initialize;
        /**
         * @private
         * 停止播放器，停止后将不能重新启动。
         */
        stop(): void;
        /**
         * @private
         * 暂停播放器，后续可以通过调用start()重新启动播放器。
         */
        pause(): void;
        /**
         * @private
         * 渲染屏幕
         */
        $render(triggerByFrame: boolean, costTicker: number): void;
        /**
         * @private
         * 更新舞台尺寸
         * @param stageWidth 舞台宽度（以像素为单位）
         * @param stageHeight 舞台高度（以像素为单位）
         */
        updateStageSize(stageWidth: number, stageHeight: number): void;
        /**
         * @private
         * 显示FPS。
         */
        displayFPS(showFPS: boolean, showLog: boolean, logFilter: string, styles: Object): void;
        /**
         * @private
         */
        private showFPS;
        /**
         * @private
         */
        private showLog;
        /**
         * @private
         */
        private stageDisplayList;
    }
    /**
     * @private
     */
    let $logToFPS: (info: string) => void;
    /**
     * @private
     */
    let $warnToFPS: (info: string) => void;
    /**
     * @private
     */
    let $errorToFPS: (info: string) => void;
    class FPSImpl {
        private showFPS;
        private showLog;
        private logFilter;
        private styles?;
        private infoLines;
        private totalTime;
        private totalTick;
        private lastTime;
        private drawCalls;
        private costRender;
        private costTicker;
        private _stage;
        private fpsDisplay;
        private filter;
        constructor(stage: egret.Stage, showFPS: boolean, showLog: boolean, logFilter: string, styles?: Object);
        update(drawCalls: number, costRender: any, costTicker: any): void;
        updateInfo(info: any): void;
        updateWarn(info: any): void;
        updateError(info: any): void;
    }
    let setRenderMode: (renderMode: string) => void;
    let WebGLRenderContext: {
        new (width?: number, height?: number, context?: WebGLRenderingContext): RenderContext;
    };
}
/**
 * @private
 */
declare module egret {
    /**
     * @private
     */
    var nativeRender: boolean;
}
