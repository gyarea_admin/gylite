/***2025-01-26 迷途小羔羊
* 用于编写字符串操作
*/
module GYLite
{
    export class StringUtil
    {
        /**去掉左边的空字符*/
        public static leftTrim(val:string):string
        {
            return val.replace(/^\s+/,"");
        }
        /**去掉右边的空字符*/
        public static rightTrim(val:string):string
        {
            return val.replace(/\s*$/,"");
        }
        /**去掉左右两边的空字符*/
        public static trim(val:string):string
        {
            return StringUtil.rightTrim(StringUtil.leftTrim(val));
        }
    }
}