/**
 @author 迷途小羔羊
 2015.6.5
 */
declare module GYLite {
    /**单选按钮组*/
    class GYRadioGroup extends egret.EventDispatcher {
        private _selectedButton;
        /**设置此属性选中组内单选按钮*/
        set selectedButton(val: GYRadioButton);
        get selectedButton(): GYRadioButton;
    }
}
