declare module GYLite {
    /**进度条皮肤*/
    class ProgressBarSkin implements IProgressBarSkin {
        protected _backgroundSkin: GYScaleSprite;
        protected _barSkin: GYScaleSprite;
        protected _textSkin: GYText;
        protected _hostComponent: GYSprite;
        protected _rotation: number;
        protected _barX: number;
        protected _barY: number;
        /**@param s.backgroundSkin 背景皮肤
         * @param s.barSkin 进度条皮肤
         * @param s.textSkin 文本皮肤 默认null，没有进度文本*/
        constructor(backgroundSkin: GYScaleSprite, barSkin: GYScaleSprite, t?: GYText | TextFormat);
        release(): void;
        get width(): number;
        get height(): number;
        set width(val: number);
        set height(val: number);
        get textSkin(): GYText;
        set barX(val: number);
        set barY(val: number);
        set backgroundWidth(val: number);
        set backgroundHeight(val: number);
        set barWidth(val: number);
        set barHeight(val: number);
        set clipX(val: number);
        set clipY(val: number);
        set mode(val: number);
        get barX(): number;
        get barY(): number;
        get barWidth(): number;
        get barHeight(): number;
        get backgroundWidth(): number;
        get backgroundHeight(): number;
        get clipX(): number;
        get clipY(): number;
        get mode(): number;
        set rotation(val: number);
        get rotation(): number;
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        /**此函数复制皮肤属性 如s.label gap等皮肤的属性*/
        copy(skin: IGYSkin): void;
        /**克隆皮肤*/
        clone(): IGYSkin;
        get backgroundSkin(): GYScaleSprite;
        get barSkin(): GYScaleSprite;
    }
}
