/***2025-01-26 迷途小羔羊
* 用于编写字符串操作
*/
declare module GYLite {
    class StringUtil {
        /**去掉左边的空字符*/
        static leftTrim(val: string): string;
        /**去掉右边的空字符*/
        static rightTrim(val: string): string;
        /**去掉左右两边的空字符*/
        static trim(val: string): string;
    }
}
