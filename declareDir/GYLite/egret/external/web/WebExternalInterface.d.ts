declare namespace egret.web {
    /**
     * @private
     */
    class WebExternalInterface implements ExternalInterface {
        /**
         * @private
         * @param functionName
         * @param value
         */
        static call(functionName: string, value: string): void;
        /**
         * @private
         * @param functionName
         * @param listener
         */
        static addCallback(functionName: string, listener: (value: any) => void): void;
    }
}
declare namespace egret.web {
    /**
     * @private
     */
    class NativeExternalInterface implements ExternalInterface {
        static call(functionName: string, value: string): void;
        static addCallback(functionName: string, listener: (value: any) => void): void;
    }
}
declare namespace egret.web {
    /**
     * @private
     */
    class WebViewExternalInterface implements ExternalInterface {
        static call(functionName: string, value: string): void;
        static addCallback(functionName: string, listener: (value: any) => void): void;
        static invokeCallback(functionName: string, value: string): void;
    }
}
