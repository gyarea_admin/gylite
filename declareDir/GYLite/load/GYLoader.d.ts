/**
 * @author 迷途小羔羊
 * 2014-11-19
 */
declare module GYLite {
    /**MyLoader是一个loader和urlloader封装的多任务加载器*/
    class GYLoader {
        private static _loader;
        static getDefaultLoader(): GYLoader;
        /**支持的解析类型1 blob 2 base64*/ static supportBlobMP3: number;
        /**支持的解析类型1 blob 2 base64*/ static supportBlobWAV: number;
        /**自定义压缩的音频解析类*/ static defCompressSound: any;
        static imageToGPU: boolean;
        /**是否微信http请求接口优先，默认false*/ static wxreq: any;
        private static _resDict;
        private static _resDataDict;
        private _seqLoaderList;
        private _seqURLLoaderList;
        private _loadList;
        private _urlLoadList;
        private _loaderReq;
        private _httpReq;
        private static _checkTime;
        private static _checkInterval;
        private static _timeId;
        /**
         * @param loaderThreads Loader的线程数量 默认为1
         * @param urlLoaderThreads UrlLoader的线程数量 默认为0
         * @param 微信加载器，若存在则使用微信加载器方式加载
         * */
        constructor(loaderThreads?: number, urlLoaderThreads?: number);
        private getLoader;
        private getURLLoader;
        /**读取bytes资源 png jpg bmp
         * @param bytes uint8Array或者ByteArray的buffer属性
         * @param callBackFunc 回调函数，返回参数为LoadInfo,返回后，loadInfo会自动被清理
         * @param thisObject callBackFunc的this指向
         * @param type 加载资源html blob类型 image(如image/png)，默认1
         * @param mimeType blob资源 如"image/png" 图片文件 "audio/basic" 声音文件，默认"image/png"
         * @param param 附加参数，默认null
         * @param retainBytes 是否保留加载的文件字节数据，默认false
         * @param lifeTime 资源存活周期，默认NaN，永久存活
         * */
        loadBytes(bytes: ArrayBuffer, callBackFunc: Function, thisObject: any, type?: number, mimeType?: string, param?: any, retainBytes?: boolean, lifeTime?: number): LoadInfo;
        /**读取GYPackager打包的资源
         * @param bytes uint8Array或者ByteArray的buffer属性
         * @param callBackFunc 回调函数，返回参数为CompressLoadInfo
         * @param callBackFunc指向
         * @param version 资源版本号
         * @param progFunc 回调函数，返回参数为CompressLoadInfo
         * @param param 附加参数 在CompressLoadInfo.param处返回
         * @param retainBytes 是否保留加载的文件字节数据
         * */
        loadGYCompressBytes(buffer: ArrayBuffer, callBackFunc: Function, thisObject: any, version?: string, progFunc?: Function, param?: any, retainBytes?: boolean): CompressLoadInfo;
        /**加载，自动区分是图片或声音、数据文件、图集，参数参考loadPath和loadData
         * @param path 资源url
         * @param callBackFunc 回调函数，返回参数为LoadInfo,返回后，loadInfo会自动被清理
         * @param thisObject callBackFunc的this指向
         * @param type 资源类型 默认LoadType.TYPE_IMAGE
         * @param method request方法 默认"get"
         * @param param 附加参数 默认null
         * @param progFunc 进度回调 默认null
         * @param lifeTime 资源存活周期，默认NaN，永久存活
        */
        load(path: string, callBackFunc: Function, thisObject: any, type?: LoadType, method?: string, param?: InfoParam, progFunc?: Function, lifeTime?: number): LoadInfo;
        /**加载资源集合，请对最后加载回调返回的ResObject进行资源引用，以保持对整组资源的引用
         * @param assets 资源集合 [路径，加载类型，路径，加载类型]，例如图集 [a.png,a.json]，注意把最重要的主资源放在最后（因为是按列表顺序加载的）
         * @param callBackFunc 回调函数，返回参数为LoadInfo,返回后，loadInfo会自动被清理
         * @param thisObject callBackFunc的this指向
         * @param param 附加参数，默认null
         * @param lifeTime 资源存活周期，默认NaN，永久存活
        */
        loadAssets(assets: any[], callBackFunc: Function, thisObject: any, param?: InfoParam, lifeTime?: number): LoadInfo;
        private assetsLoadComp;
        private nextAsset;
        /**读取网络资源 swf png jpg bmp
         * @param path 读取路径
         * @param callBackFunc 回调函数，返回参数为LoadInfo,返回后，loadInfo会自动被清理
         * @param thisObject callBackFunc的this指向
         * @param type 加载资源类型 image(png,jpg), swf, font
         * @param param 附加参数，默认null
         * @param lifeTime 资源存活周期，默认NaN，永久存活
         * */
        loadPath(path: string, callBackFunc: Function, thisObject: any, type?: LoadType, param?: any, lifeTime?: number): LoadInfo;
        /**取消加载，当调用loadPath加载资源时，调用此方法取消加载，此方法根据path和callBackFunc寻找此前的加载请求
         * @param path	加载路径
         * @param callBackFunc 加载完成的回调
         * */
        cancelLoadPath(path: any, callBackFunc: any, thisObject: any): void;
        private LoadError;
        private ErrorNext;
        private LoadComp;
        private LoadNext;
        /**httpSend请求
         * @param url 请求的url
         * @param callBackFunc 回调函数，返回参数为LoadInfo
         * @param thisObject callBackFunc的this指向     *
         * @param type  二进制(binary)、文本(text)、URL 编码变量(variables) 参考GYLoader常量
         * @param method 访问方式 get 或者 post
         * @param header 头部数据(Array[{key,value}])
         * @param data 发送数据
         * @param param 回调附加参数object
         * */
        httpSend(url: string, callBackFunc: Function, thisObject: any, type?: LoadType, method?: string, header?: Array<any>, data?: any, contentType?: string, param?: any): LoadInfo;
        /**加载数据
         * @param path 读取路径
         * @param callBackFunc 回调函数，返回参数为LoadInfo
         * @param thisObject callBackFunc的this指向
         * @param progFunc 回调函数，返回参数为ProgressEvent
         * @param type  二进制(binary)、文本(text)、URL 编码变量(variables)
         * @param method 访问方式 get 或者 post
         * @param param 附加参数，默认null
         * @param lifeTime 资源存活周期，默认NaN，永久存活
         * */
        loadData(path: string, callBackFunc: Function, thisObject: any, progFunc?: any, type?: LoadType, method?: string, param?: any, lifeTime?: number): LoadInfo;
        /**取消加载，当调用loadPath加载数据时，调用此方法取消加载，此方法根据path和callBackFunc寻找此前的加载请求
         * @param path	加载路径
         * @param callBackFunc 加载完成的回调
         * */
        cancelLoadData(path: any, callBackFunc: any, thisObject: any): void;
        private UrlLoadComp;
        private URLLoadError;
        private UrlErrorNext;
        private UrlProgFunc;
        private UrlLoadNext;
        static getMimeType(type: number, path?: string): string;
        static getImageMemory(): number;
        static getImageGPUMemory(): number;
        /**检测销毁对象的引用资源回收的时间间隔(毫秒)，小于等于0时不回收*/
        static setResCheck(val: number): void;
        /**检测销毁对象的引用，进行回收**/
        static checkResUse(sysTime: number): void;
        /**获取包含key键的数据资源,假设data文件夹下面的资源，如 data/,结尾带上/，以便区分文件夹节点
         * @param searchKey 资源键名
         * @param exact 是否完全匹配 默认false
         * @param ignoreVersion 匹配忽略版本号
        */
        static getDataResByKey(searchKey: string, exact?: boolean, ignoreVersion?: boolean): Array<ResObject>;
        /**获取包含key键的资源,假设img文件夹下面的资源，如 img/,结尾带上/，以便区分文件夹节点
         * @param searchKey 资源键名
         * @param exact 是否完全匹配 默认false
        */
        static getResByKey(searchKey: string, exact?: boolean): Array<ResObject>;
        /**销毁资源
         * @param resObj ResObject资源
         * @param dictType 资源类型 默认3 媒体资源和数据 1媒体资源 2数据
        */
        static deleteRes(resObj: ResObject, dictType?: number): void;
        /**删除包含key键的资源,删除img文件夹下面的资源，如 img/,结尾带上/，以便区分文件夹节点, deleteKey为空则删除全部资源
         * @param deleteKey 资源键名
         * @param dictType 资源类型 默认3 媒体资源和数据 1媒体资源 2数据
         * @param fuzzy 是否模糊匹配，默认true
        */
        static deleteResByKey(deleteKey: string, dictType?: number, fuzzy?: boolean): void;
        /**获取位图、动画资源
         * @param key 资源路径，需要后缀
         * @param aliasKey 图集资源路径，如果存在图集，则key为图集内小图名称，需要后缀
        */
        static getRes(key: string, aliasKey?: string): ResObject;
        /**获取二进制数据资源*/
        static getDataRes(val: string): ResObject;
        static setRes(key: string, val: ResObject): void;
        static setDataRes(key: string, val: ResObject): void;
        /**获取媒体资源数组*/
        static resArray(): ResObject[];
        /**获取数据资源数组*/
        static dataResArray(): ResObject[];
        /**是否资源组(如图集、spine这类带多个资源一组的类型)*/
        static isAssets(type: LoadType): boolean;
        /**销毁加载系统*/
        static dispose(): void;
        /**加载系统是否已销毁*/
        static get disposed(): boolean;
        static _disposed: boolean;
    }
    class SeqURLLoader extends egret.EventDispatcher {
        private _loadInfo;
        private _intervalId;
        private _isLoading;
        private _xmlHttpRequest;
        private _data;
        private _cancelLockTime;
        progressData: any;
        private _laterFunc;
        private _laterObject;
        private _bindHandleEvent;
        constructor();
        set loadInfo(val: LoadInfo);
        get loadInfo(): LoadInfo;
        private handleEvent;
        close(): void;
        load(req: URLRequest): void;
        get data(): any;
        private loadComp;
        private loadError;
        /**由于连续加载时，上一个失败立即使用这个loader会产生加载中断的bug，所以进行延时处理*/
        callLater(func: Function, thisObject: any, delay?: number): void;
        private laterCall;
        /**清理延时*/
        clearCallLater(): void;
        get cancelLockTime(): number;
        set cancelLockTime(value: number);
        get isLoading(): number;
        set isLoading(val: number);
        breakLock(): void;
    }
    class SeqLoader extends egret.EventDispatcher {
        private _loadInfo;
        private _intervalId;
        private _isLoading;
        private _imageLoader;
        private _soundLoader;
        private _cancelLockTime;
        private _data;
        private _laterFunc;
        private _laterObject;
        private _bytes;
        constructor();
        set loadInfo(val: LoadInfo);
        get loadInfo(): LoadInfo;
        get isLoading(): number;
        set isLoading(val: number);
        loadBytes(bytes: ArrayBuffer): void;
        loadSound(url: string): void;
        private loadSoundComp;
        private loadSoundError;
        load(url: string): void;
        loadBitmap(blob: Blob): void;
        get data(): any;
        private loadBitmapComp;
        private loadComp;
        private loadError;
        /**由于连续加载时，上一个失败立即使用这个loader会产生加载中断的bug，所以进行延时处理*/
        callLater(func: Function, thisObject: any, delay?: number): void;
        private laterCall;
        /**清理延时*/
        clearCallLater(): void;
        get cancelLockTime(): number;
        set cancelLockTime(value: number);
        breakLock(): void;
    }
    class URLRequest {
        method: any;
        dataFormat: any;
        url: any;
        contentType: any;
        requestHeaders: any;
        data: any;
        constructor();
    }
    class URLRequestHeader {
        key: string;
        value: string;
        constructor(key: any, value: any);
    }
    class URLVariables {
        variables: any;
        constructor(source: any);
        decode(source: any): void;
        toString(): string;
        encodeValue(key: any, value: any): any;
        encodeArray(key: any, value: any): any;
    }
    class URLLoaderDataFormat {
        static BINARY: string;
        static TEXT: string;
        static VARIABLES: string;
        static JSON: string;
    }
}
