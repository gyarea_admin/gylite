declare module GYLite {
    interface IMenuSkin extends IGYSkin {
        backgroundImage: GYScaleSprite;
        list?: GYLite.GYListV;
    }
}
