module GYLite
{
															
	
	/**进度条*/
	export class GYProgressBar extends GYSkinContainer
	{
		/**从上到下*/public static UP_DOWN:number=2;
		/**从下到上*/public static DOWN_UP:number=3;
		/**从左到右*/public static LEFT_RIGHT:number=0;
		/**从右到左*/public static RIGHT_LEFT:number=1;
		protected _value:number;
		protected _max:number;
		protected _min:number;
		protected _barMax:number;
		protected _barMin:number;
		protected _barVal:number;
		protected _type:number;
		protected _barX:number;
		protected _barY:number;
		protected _invalidValue:boolean;		
		//动画部分
		protected _moveTime:number;
		protected _startTime:number;
		protected _tarVal:number;
		protected _lastVal:number;
		protected _tempVal:number;
		protected _tempVal2:number;
		protected _moveBack:boolean;
		private _commitEvent:GYEvent;
		/**格式化显示的文本 */public formatValueFunc:(cur:number,max:number)=>string;
		/**@param s.skin 皮肤请使用ProgressBarSkin 自定义请实现IProgressBarSkin接口
		 * @param s.type 类型，进度条的变化方向 GYProgressBar.UP_DOWN等方向
		 * */
		public constructor(skin:IProgressBarSkin=null,type:number = 0)
		{
			super();
			
			let s = this;
			this.skin = skin;
			this.type = type;
			s.initComponent();
		}
		protected initComponent():void
		{
			let s = this;			
			s._commitEvent = new GYEvent(GYEvent.VALUE_COMMIT);
			s._barVal=NaN;			
			s._barX=0;
			s._barY=0;
			s._invalidValue=false;			
			//动画部分
			s._moveTime=NaN;
			s._startTime=NaN;
			s._tarVal=NaN;
			s._lastVal=NaN;
			s._tempVal=NaN;
			s._tempVal2=NaN;
			s._moveBack=false;

			s._value = 0;
			s._max = 0;
			s._min = 0;
			s._barMax = 0;
			s._barMin = 0;
			s.barX = GYProgressBar.default_barX;
			s.barY = GYProgressBar.default_barY;
			s._moveBack = GYProgressBar.default_moveBack;
			s._moveTime = GYProgressBar.default_moveTime;
		}
		public moveTo(val:number):void
		{
			let s = this;
			s._lastVal = s._value;
			s._tarVal = val;
			if(val < s._value)
			{
				if(s._moveBack)
					s._tempVal = s._tarVal - s._lastVal;
				else
				{
					s._tempVal = s._max - s._value;
					s._tempVal2 = val;
				}
			}
			else
				s._tempVal = s._tarVal - s._lastVal;
			s._startTime = egret.getTimer();
			CommonUtil.addStageLoop(s.moveLoop,s);
		}
		protected moveLoop(t:number):void
		{
			let s = this;
			var t:number;
			var gt:number = egret.getTimer();
			var end:boolean;
			if(s._lastVal < s._tarVal)
			{
				t = s._lastVal + s._tempVal*(gt-s._startTime)/s._moveTime;
				if(t > s._tarVal)
				{
					s.setValue(s._tarVal, true);
					CommonUtil.delStageLoop(s.moveLoop,s);
					end = true;
				}
				else
					s.setValue(t, true);
			}
			else
			{
				if(s._moveBack)
				{
					t = s._lastVal + s._tempVal*(gt-s._startTime)/s._moveTime;
					if(t < s._tarVal)
					{
						s.setValue(s._tarVal, true);
						CommonUtil.delStageLoop(s.moveLoop,s);
						end = true;
					}
					else
						s.setValue(t, true);
					return;
				}
				//前进至最大
				t = s._lastVal + (s._tempVal + s._tempVal2)*(gt-s._startTime)/s._moveTime;
				if(t > s._max)
				{
					t = t - s._max;
					if(t > s._tarVal)
					{
						s.setValue(s._tarVal, true);
						CommonUtil.delStageLoop(s.moveLoop,s);
						end = true;
					}
					else
						s.setValue(t, true);
				}
				else
					s.setValue(t, true);
			}
			if(end)
			{
				if(s.hasEventListener(GYViewEvent.ANIMATION_END))
					s.dispatchEvent(new GYViewEvent(GYViewEvent.ANIMATION_END));
			}
		}
		/**进度条当前值*/
		public set value(val:number)
		{
			let s = this;
			s.setValue(val);
		}
		public get value():number
		{
			let s = this;
			return s._value;
		}
		/**@param commit 设置进度条当前值(会派发VALUECOMMIT通知)*/
		public setValue(val:number, commit:boolean = false):void
		{
			let s = this;
			if(s._value == val)
				return;			
			if(val > s._max)
				s._value = s._max;
			else if(val < s._min)
				s._value = s._min;
			else
				s._value = val;
			s.validValue(commit);
		}
		/**进度条最大值*/
		public set max(val:number)
		{
			let s = this;
			if(s._max == val)return;
			s._max = val;
			if(s._value > s._max)
				s._value = s._max;
			s.invalidValue();
		}
		public get max():number
		{
			let s = this;
			return s._max;
		}
		public set min(val:number)
		{
			let s = this;
			if(s._min == val)return;
			s._min = val;
			if(s._value < s._min)
				s._value = s._min;
			s.invalidValue();
		}
		public get min():number
		{
			let s = this;
			return s._min;
		}
		/**进度条最大长度*/
		public get barMax():number
		{
			let s = this;
			return s._barMax;
		}
		public set barMax(val:number)
		{
			let s = this;
			s._barMax = val;
			s.invalidValue();
		}
		/**进度条最小长度*/
		public get barMin():number
		{
			let s = this;			
			return s._barMin;
		}
		public set barMin(val:number)
		{
			let s = this;
			s._barMin = val;
			s.invalidValue();
		}
		protected setBar():void
		{
			let s = this;
			s._barVal = ((s._value-s._min)/(s._max-s._min) * (s._barMax - s._barMin) >> 0);			
			s._skin.barWidth = s._barMin + s._barVal;
			s._skin.barHeight = s.height;
			if(s._type == GYProgressBar.RIGHT_LEFT)
				s._skin.clipX = s._barMax - s._barVal;
			else if( s._type == GYProgressBar.DOWN_UP)
				s._skin.clipY = s._barMax - s._barVal;			
		}
		/**设置进度条方向，请参照GYProgressBar定义的常量，如GYProgressBar.RIGHT_LEFT*/
		public set type(val:number)
		{
			let s = this;
			s._type = val;
			if(s._type == GYProgressBar.DOWN_UP || s._type == GYProgressBar.UP_DOWN)
				s._skin.rotation = 90;
			else
				s._skin.rotation = 0;
			s.invalidValue();
		}
		public get type():number
		{
			let s = this;
			return s._type;
		}
		/**进度条当前长度*/
		public get barVal():number
		{
			let s = this;
			return s._barVal;
		}
		/**文本*/
		public get labelDisplay():GYText
		{
			let s = this;
			return s._skin.textSkin;
		}
		/**进度条初始X坐标*/
		public get barX():number
		{
			let s = this;			
			return s._barX;
		}
		public set barX(val:number)
		{
			let s = this;
			if(s._barX == val)
				return;
			s._skin.barX = s._barX = val;
		}
		/**进度条初始Y坐标*/
		public get barY():number
		{
			let s = this;			
			return s._barY;
		}
		public set barY(val:number)
		{
			let s = this;
			if(s._barY == val)
				return;
			s._skin.barY = s._barY = val;
		}
		/**刷新s.value*/
		public invalidValue():void
		{
			let s = this;
			if(s._invalidValue)
				return;
			s._invalidValue = true;
			s.displayChg();
		}
		protected validValue(commit:boolean = false):void
		{
			let s = this;
			s.setBar();
			let txt:GYLite.GYText = s.labelDisplay;
			if(txt && txt.text != "")txt.text = s.formatValueFunc!=null?s.formatValueFunc(s._value,s._max):(s._value + "/" + s._max);
			if(commit && s.hasEventListener(GYEvent.VALUE_COMMIT))
				s.dispatchEvent(s._commitEvent);
		}
		public updateView():void
		{
			let s = this;
			if(s._invalidDisplay)
			{
				s.viewChange();
				s._skin.width = s.width;
				s._skin.height = s.height;
				s._invalidDisplay = false;
			}
			if(s._invalidValue)
			{
				s.validValue();
				s._invalidValue = false;
			}
		}
		public set moveBack(val:boolean)
		{
			let s = this;
			s._moveBack = val;
		}
		/**是否达到尽头从起点重新开始*/
		public get moveBack():boolean
		{
			let s = this;
			return s._moveBack;
		}
		public get mode():number
		{
			return this._skin.mode;
		}
		public set mode(val:number)
		{
			let s = this;
			s._skin.mode = val;
		}
		public set label(val:string)
		{
			let s = this;
			s._skin.textSkin.text = val;
		}
		public get label():string
		{
			return this._skin.textSkin.text;
		}
		/**进度条动画播放时长*/
		public set moveTime(val:number)
		{
			let s = this;
			s._moveTime = val;
		}		
		
		public set skin(val:any)
		{
			let s = this;
			egret.superSetter(GYProgressBar, s, "skin", val);			
			s.invalidValue();
		}
		/**自定义皮肤请实现IProgressBarSkin或继承ProgressBarSkin*/
		protected getThemeSkin():IGYSkin
		{
			let s = this;
			return GYSprite.skinTheme.GetProgressBarSkin();
		}
		public set width(val:number)
		{
			let s = this;			
			if(s._skin.rotation == 90)
				egret.superSetter(GYProgressBar, s, "height", val);			
			else
				egret.superSetter(GYProgressBar, s, "width", val);
		}
		public get width():number
		{
			let s = this;
			return egret.superGetter(GYProgressBar, s, "width");
		}
		public set height(val:number)
		{
			let s = this;	
			if(s._skin.rotation == 90)
				egret.superSetter(GYProgressBar, s, "width", val);
			else
				egret.superSetter(GYProgressBar, s, "height", val);	
		}
		public get height():number
		{
			let s = this;
			return egret.superGetter(GYProgressBar, s, "height");
		}

		public get barSkin():GYScaleSprite
		{
			let s = this;
			return s._skin.barSkin;
		}

		public get backgroundSkin():GYScaleSprite
		{
			let s = this;
			return s._skin.backgroundSkin;
		}

		public static default_barX:number = 0;
		public static default_barY:number = 0;
		//动画部分
		public static default_moveTime:number = 500;
		public static default_moveBack:boolean = true;
	}
}