﻿/**
@author 迷途小羔羊
2012.11.18
*/
module GYLite
{
	export class GYButton extends ButtonBase
	{
		private _longDownTime:number=-1;
		private _longDownInterval:number = 1000;
		private _continueDownTime:number=-1;
		private _continueDownInterval:number = 100;
		private _continueParam:number = 1;
		/**按钮附加的参数*/public param:any;
		public constructor(skin:any=null)
		{
			super(skin);
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();
			s.addListener(null);
			s.touchEnabled = true;
		}
		private addListener(e:egret.Event):void
		{
			let s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE,s.addListener,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE,s.removeListener,s);			
			s.addEventListener(egret.TouchEvent.TOUCH_BEGIN, s.mDown,s);
			s.addEventListener(egret.TouchEvent.TOUCH_END, s.mUp,s);
			s.addEventListener(egret.TouchEvent.TOUCH_TAP, s.mClk,s);
			if(!GYSprite.isMobile)
			{
				s.addEventListener(MouseEvent.ROLL_OVER, s.rlOver,s);
				s.addEventListener(MouseEvent.ROLL_OUT, s.rlOut,s);
			}			
		}
		private removeListener(e:egret.Event):void
		{
			let s = this;
			s.addEventListener(egret.Event.ADDED_TO_STAGE,s.addListener,s);
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE,s.removeListener,s);			
			s.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, s.mDown,s);
			s.removeEventListener(egret.TouchEvent.TOUCH_END, s.mUp,s);
			s.removeEventListener(egret.TouchEvent.TOUCH_TAP, s.mClk,s);
			if(!GYSprite.isMobile)
			{
				s.removeEventListener(MouseEvent.ROLL_OVER, s.rlOver,s);
				s.removeEventListener(MouseEvent.ROLL_OUT, s.rlOut,s);				
			}			
			if(s._state == ButtonBase.STATE_SELECTDISABLE || s._state == ButtonBase.STATE_DISABLE)return;
			s.state = (s._selected?	ButtonBase.STATE_SELECT:ButtonBase.STATE_UP);
		}

		protected rlOver(e:egret.TouchEvent):void
		{
			let s = this;
			if (GYSprite.isStageDown(this))
			{
				s.state = s._selected?	ButtonBase.STATE_SELECTDOWN:ButtonBase.STATE_DOWN;
				s.dispatchEvent(new egret.TouchEvent(egret.TouchEvent.TOUCH_BEGIN));
			}
			else
			{
				if(e.touchDown)return;
				s.state = s._selected?	ButtonBase.STATE_SELECTOVER:ButtonBase.STATE_OVER;
			}
		}
		protected rlOut(e:egret.TouchEvent):void
		{
			let s = this;
			if(s.touchEnabled == false)
				return;
			if (GYSprite.isStageDown(this))
				return;
			s.state = s._selected?	ButtonBase.STATE_SELECT:ButtonBase.STATE_UP;
		}
		protected mDown(e:egret.TouchEvent):void
		{
			let s = this;
			GYSprite.addStageDown(this, s.releaseOutside, s);
			s.state = s._selected?	ButtonBase.STATE_SELECTDOWN:ButtonBase.STATE_DOWN;
			if(s.hasEventListener(GYTouchEvent.LONG_MOUSEDOWN))
			{
				s._longDownTime = CommonUtil.loopTime;
				CommonUtil.addStageLoop(s.longDownLoop,s);
			}
			if(s.hasEventListener(GYTouchEvent.CONTINUE_MOUSEDOWN))
			{
				s._continueDownTime = CommonUtil.loopTime;
				CommonUtil.addStageLoop(s.continueDownLoop,s);
				s._continueParam = 1;
				s.dispatchEvent(new GYTouchEvent(GYTouchEvent.CONTINUE_MOUSEDOWN));
			}
		}

		protected mUp(e:egret.TouchEvent):void
		{
			let s = this;
			if(s.stage == null || e == null || GYSprite.isMobile)
				s.state = s._selected?ButtonBase.STATE_SELECT:ButtonBase.STATE_UP;
			else
				s.state = s._selected?ButtonBase.STATE_SELECTOVER:ButtonBase.STATE_OVER;
			s.clearLongDown();
			s.clearContinueDown();
		}
		
		protected releaseOutside(e:GYTouchEvent):void
		{
			let s = this;
			if(s.state == ButtonBase.STATE_DISABLE)return;
			s.mUp(null);
		}
		
		private longDownLoop(t:number):void
		{
			let s = this;
			if(t - s._longDownTime > s._longDownInterval)
			{
				s.clearLongDown();
				s.dispatchEvent(new GYTouchEvent(GYTouchEvent.LONG_MOUSEDOWN));
			}
		}
		private clearLongDown():void
		{
			let s = this;
			s._longDownTime = -1;
			CommonUtil.delStageLoop(s.longDownLoop,s);
		}
		private continueDownLoop(t:number):void
		{
			let s = this;
			if(t - s._continueDownTime > s._continueDownInterval*s._continueParam)
			{
				s._continueParam *= 0.95;
				if(s._continueParam < 0.02)
					s._continueParam = 0.02;
				s._continueDownTime = t;
				s.dispatchEvent(new GYTouchEvent(GYTouchEvent.CONTINUE_MOUSEDOWN));
			}
		}
		private clearContinueDown():void
		{
			let s = this;
			s._continueDownTime = -1;
			CommonUtil.delStageLoop(s.continueDownLoop,s);
		}

		protected mClk(e:egret.TouchEvent):void
		{
			let s = this;
			if(s._toggle)
				s._selected = ! s._selected;
			if(s.stage == null || GYSprite.isMobile)
				s.state = s._selected?ButtonBase.STATE_SELECT:ButtonBase.STATE_UP;				
			else
				s.state = s._selected?ButtonBase.STATE_SELECTOVER:ButtonBase.STATE_OVER;
			if(s._clickFunc!=null)
				s._clickFunc.call(s._clickThisObject, e);
		}
		/**点击按钮
		 * @param func(e:egret.TouchEvent)
		 * */
		public getOnClick():Function
		{
			let s = this;
			return s._clickFunc;
		}
		public setOnClick(func:Function, thisObject:any)
		{
			let s = this;
			s._clickFunc = func;
			s._clickThisObject = thisObject;
		}
		
		public set enabled(val:boolean)
		{
			let s = this;
			if(s._enabled==val)
				return;
			s._enabled=val;
			s.touchChildren=val;
			s.touchEnabled=val;
			if(s._enabled)
				s.state = s._selected?	ButtonBase.STATE_SELECT:ButtonBase.STATE_UP;
			else
				s.state = s._selected?	ButtonBase.STATE_SELECTDISABLE:ButtonBase.STATE_DISABLE;
		}
		/**是否可点 */
		public get enabled():boolean
		{
			let s = this;
			return s._enabled;
		}
		
		public set toggle(tog:boolean)
		{
			let s = this;
			s._toggle = tog;
		}
		/**是否自动切换状态*/
		public get toggle():boolean
		{
			let s = this;
			return s._toggle;
		}
		
		public set selected(sel:boolean)
		{
			let s = this;
			s._selected = sel;
			if(s._selected)
			{
				if(s._state == ButtonBase.STATE_OVER)s.state = ButtonBase.STATE_SELECTOVER;
				else if(s._state == ButtonBase.STATE_UP)s.state = ButtonBase.STATE_SELECT;
				else if(s._state == ButtonBase.STATE_DOWN)s.state = ButtonBase.STATE_SELECTDOWN;
				else if(s._state == ButtonBase.STATE_DISABLE)s.state = ButtonBase.STATE_SELECTDISABLE;
			}
			else
			{
				if(s._state == ButtonBase.STATE_SELECTOVER)s.state = ButtonBase.STATE_OVER;
				else if(s._state == ButtonBase.STATE_SELECT)s.state = ButtonBase.STATE_UP;
				else if(s._state == ButtonBase.STATE_SELECTDOWN)s.state = ButtonBase.STATE_DOWN;
				else if(s._state == ButtonBase.STATE_SELECTDISABLE)s.state = ButtonBase.STATE_DISABLE;
			}
		}
		/**是否选中*/
		public get selected():boolean
		{
			let s = this;
			return s._selected;
		}
		
		/**设置按钮状态*/
		public set state(st:number)
		{
			let s = this;
			if(s._state == st)
				return;				
			s._state=st;
			s.invalidState();
		}
		public get state():number
		{
			let s = this;
			return s._state;
		}
		/**长按按钮时，经过多长时间触发长按事件(毫秒)*/
		public get longDownInterval():number
		{
			let s = this;
			return s._longDownInterval;
		}

		public set longDownInterval(value:number)
		{
			let s = this;
			s._longDownInterval = value;
		}

		
	}
}