declare module GYLite {
    class GYTouchEvent extends egret.TouchEvent {
        /**外部释放鼠标*/
        static RELEASE_OUTSIDE: string;
        /**长按鼠标*/
        static LONG_MOUSEDOWN: string;
        /**连续按下鼠标*/
        static CONTINUE_MOUSEDOWN: string;
        /**双击*/
        static DOUBLE_CLICK: string;
        outsideTarget: any;
        result: number;
        constructor(type: string, bubbles?: boolean, cancelable?: boolean, stageX?: number, stageY?: number, touchPtId?: number);
    }
}
