declare module GYLite {
    class ColorUtil {
        /**翻转rgb的顺序为bgr*/
        static revertColor(color: number): number;
        /**获取0-color之间的百分比颜色*/
        static lerpColor(color: number, per: number): number;
        static getRGBColor(r: number, g: number, b: number): number;
        static getARGBColor(r: number, g: number, b: number, a?: number): number;
        static getColorARGB(color: number): number[];
        /**16进制数值转成16进制颜色值
         * @param c 如 #ffffff，返回0xffffff
        */
        static numberTo16Str(c: number): string;
        /**16进制颜色值转成16进制数值
         * @param c 如 #ffffff，返回0xffffff
        */
        static str16ToNumber(c: string): number;
        /**argb16进制颜色值转成16进制数值
         * @param c 如 #ffffffff，返回[0xff,0xffffff]
        */
        static argb16ToNumber(c: string): number[];
        /**调整颜色color(argb)亮度，亮度系数 brightness（建议范围 -1.0~1.0）*/
        static brightness(color: number, brightness: number): number;
        /**调整颜色color(argb)对比度，对比度系数 contrast（建议范围 0.0~2.0，1.0为原图）*/
        static contrast(color: number, contrast: number): number;
        /**调整颜色color(argb)饱和度，saturation（0.0为灰度，1.0为原图）*/
        static saturation(color: number, saturation: number): number;
        /**调整颜色color(argb)色相
        * 输入：原像素值 R, G, B，色相偏移角度 hue_shift（0-360度）
        * 步骤：将 RGB 转换为 HSL/HSV → 调整 H → 转回 RGB
        */
        static hue(color: number, hue_shift: number): number;
        /**RGB转hls，RGB值传入前要归一化*/
        static rgb_to_hls(R: number, G: number, B: number): number[];
        static hls_to_rgb(h: number, l: number, s: number): number[];
        /**颜色转矩阵
         * @param color ARGB 原色
         * @param offset ARGB 颜色偏移量
        */
        static colorToMatrix(color: number, offset?: number): number[];
    }
}
