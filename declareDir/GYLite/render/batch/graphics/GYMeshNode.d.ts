/**
@author 迷途小羔羊
2022.10.19
*/
declare module GYLite {
    class GYMeshNode extends egret.sys.MeshNode implements IUpdate {
        left: number;
        right: number;
        top: number;
        bottom: number;
        color: number;
        constructor();
        setRect(l: number, r: number, t: number, b: number): void;
        updatePosBound(x: number, y: number): void;
        /**添加顶点*/
        addVertex(x: number, y: number, u: number, v: number): void;
        /**添加批量顶点*/
        addVertices(vertices: number[], uvs: number[], indices: number[]): void;
        protected _updating: boolean;
        /**是否刷新中（内部使用请勿修改）*/
        get updating(): boolean;
        set updating(value: boolean);
        updateView(): void;
        dispose(): void;
        disposed: boolean;
    }
}
