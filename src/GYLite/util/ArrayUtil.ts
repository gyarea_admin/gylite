/***2024-12-24 迷途小羔羊
* 用于编写数组工具类
*/
module GYLite
{
    export class ArrayUtil
    {
        /**缩放数组数值
         * @param arr 数组
         * @param mulValue 缩放值
        */
        public static MulNumberArray(arr:number[], mulValue:number):number[]
        {
            let i:number,len:number;            
            len = arr.length;
            for(i=0;i<len;++i)            
                arr[i]*=mulValue;
            return arr;
        }
        /**查询数组中的对象
         * @param arr 数组
         * @param key 比较数组对象内的key字段的值
         * @param value 比较的value值
         * @return 找不到返回null
        */
        public static findObject(arr:any[], key:string, value:any):any
        {
            let len:number;
            len = arr.length;            
            while(--len>-1)
            {
                if(arr[len][key] == value)
                    return arr[len];
            }                
            return null;
        }
        /**查询数组中的对象的索引
         * @param arr 数组
         * @param key 比较数组对象内的key字段的值，默认null，表示直接比较数组对象
         * @param value 比较的value值
         * @return 找不到返回-1
        */
        public static findObjectIndex(arr:any[], key:string=null, value:any=null):number
        {
            let len:number;
            len = arr.length;            
            if(key == null)
            {
                while(--len>-1)
                {
                    if(arr[len] == value)
                        return len;
                }                
            }
            else
            {
                while(--len>-1)
                {
                    if(arr[len][key] == value)
                        return len;
                }                
            }
            return -1;
        }
        /**比较两个数组元素是否不一样（以长的数组为准）
         * @param arr1 数组1
         * @param arr2 数组2
         * @return 返回 结果数组
        */
        public static diffArray(arr1:any[],arr2:any[]):boolean[]
        {
            let i:number,len:number;
            let result:boolean[] = [];
            len = Math.max(arr1.length,arr2.length);
            for(i=0;i<len;++i)
            {
                result[i] = arr1[i] != arr2[i];
            }
            return result;
        }
        /**查询最大值*/
        public static max(arr:any[], key:string=null):number
        {
            let len:number;
            let max:number = Number.MIN_VALUE;
            let maxInd:number=-1;
            len = arr.length;
            while(--len>-1)
            {
                if(key)
                {
                    if(arr[len][key] > max)
                    {
                        maxInd = len;
                        max = arr[len][key];
                    }
                }
                else
                {
                    if(arr[len] > max)
                    {
                        maxInd = len;
                        max = arr[len];
                    }
                }
            }
            return maxInd;
        }
        /**查询最小值*/
        public static min(arr:any[], key:string=null):number
        {
            let len:number;
            let min:number = Number.MAX_VALUE;
            let minInd:number=-1;
            len = arr.length;
            while(--len>-1)
            {
                if(key)
                {
                    if(arr[len][key] < min)
                    {
                        minInd = len;
                        min = arr[len][key];
                    }
                }
                else
                {
                    if(arr[len] < min)
                    {
                        minInd = len;
                        min = arr[len];
                    }
                }
            }
            return minInd;
        }
    }
}