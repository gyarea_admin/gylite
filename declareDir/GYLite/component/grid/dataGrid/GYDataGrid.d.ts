/**
 @author 迷途小羔羊
 2015.11.20
 */
declare module GYLite {
    class GYDataGrid extends GYGrid {
        protected _rowsData: GridData[];
        protected _colsData: GridData[];
        protected _maxGridWidth: number;
        protected _maxGridHeight: number;
        protected _rowsDataUpdate: boolean;
        protected _colsDataUpdate: boolean;
        protected _oldRows: number;
        protected _oldCols: number;
        /**行列宽高可变的Grid组件，行列数量随组件宽高自适应，若行列宽高固定，建议使用GYGrid
         * @param cols 初始化列数据 0初始化 非0 则需要外部提供s.rowsData的行数据
         * @param rows 初始化行数据 0初始化 非0 则需要外部提供s.colsData的列数据
         * @param getGridFunc():IItemRender 返回自定义格子对象的方法
         * @param setGridFunc(IItemRender, any) 设置格子数据的方法*/
        constructor(cols: number, rows: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        protected initComponent(): void;
        /**@inheritDoc*/
        scrollToPos(row?: number, col?: number): void;
        /**@inheritDoc*/
        scrollToEndPos(row?: number, col?: number): void;
        /**每行的格子信息*/
        get rowsData(): GridData[];
        /**每列格子信息*/
        get colsData(): GridData[];
        protected getRowGridData(val: number): GridData;
        protected getColGridData(val: number): GridData;
        /**@inheritDoc*/
        updateGrid(): void;
        /**@inheritDoc*/
        get dataProvider(): Array<any>;
        set dataProvider(val: Array<any>);
        protected vScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        protected hScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        protected unshiftRows(): void;
        protected addRows(): void;
        protected addChildRows(arr: Array<any>): void;
        protected removeChildRows(arr: Array<any>): void;
        /**清除列宽度设定*/
        clearColGridsSizeSet(): void;
        /**清除行高度设定*/
        clearRowGridsSizeSet(): void;
        /**设置某列宽度
         * @param ind 列索引 val 宽度值
         * */
        setGridWidth(ind: number, val: number): void;
        /**设置某行高度
         * @param ind 行索引 val 高度值
         * */
        setGridHeight(ind: number, val: number): void;
        protected getNowRow(val: number): GridData;
        protected getNowCol(val: number): GridData;
        /**刷新网格行数据*/
        invalidRowsData(): void;
        /**刷新网格列数据*/
        invalidColsData(): void;
        protected updateColsData(): void;
        protected updateRowsData(): void;
        updateView(): void;
        static default_boxH: number;
        static default_boxW: number;
    }
}
