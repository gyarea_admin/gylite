module GYLite
{
	export interface IItemRender extends IGYDisplay
	{
		/**设置格子数据 */setData(obj:any):void;
		/**获取格子数据 */getData():any;
		/**渲染项的拥有者 */owner:IList;		
		selected:boolean;				
		/**渲染项的数据索引 */itemIndex:number;		
		/**二维表渲染项的数据行 */row:number;		
		/**二维表渲染项的数据列 */col:number;
	}
}