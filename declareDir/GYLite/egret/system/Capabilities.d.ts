declare module egret {
    class Capabilities {
        static readonly language: string;
        static readonly isMobile: boolean;
        static readonly os: string;
        static readonly runtimeType: string;
        static readonly engineVersion: string;
        static readonly renderMode: string;
        static readonly boundingClientWidth: number;
        static readonly boundingClientHeight: number;
        static _supportedCompressedTexture: SupportedCompressedTexture;
    }
    namespace RuntimeType {
        const WEB = "web";
        const NATIVE = "native";
        const RUNTIME2 = "runtime2";
        const MYGAME = "mygame";
        const WXGAME = "wxgame";
        const BAIDUGAME = "baidugame";
        const QGAME = "qgame";
        const OPPOGAME = "oppogame";
        const QQGAME = "qqgame";
        const VIVOGAME = "vivogame";
        const QHGAME = "qhgame";
        const TTGAME = "ttgame";
        const FASTGAME = "fastgame";
        const TBCREATIVEAPP = "tbcreativeapp";
    }
    interface SupportedCompressedTexture {
        pvrtc: boolean;
        etc1: boolean;
    }
}
