declare module GYLite {
    class Vector4 {
        elements: Float32Array;
        constructor(x?: number, y?: number, z?: number, w?: number);
        /**
         *从Array数组拷贝值。
         *@param array 数组。
         *@param offset 数组偏移。
         */
        fromArray(array: number[], offset?: number): void;
        /**
         *克隆。
         *@param destObject 克隆源。
         */
        cloneTo(destObject: Vector4): void;
        /**
         *克隆。
         *@return 克隆副本。
         */
        clone(): Vector4;
        /**
         *求四维向量的长度。
         *@return 长度。
         */
        length(): number;
        /**
         *求四维向量长度的平方。
         *@return 长度的平方。
         */
        lengthSquared(): number;
        /**
         *设置X轴坐标。
         *@param value X轴坐标。
         */
        /**
         *获取X轴坐标。
         *@return X轴坐标。
         */
        get x(): number;
        set x(value: number);
        /**
         *设置Y轴坐标。
         *@param value Y轴坐标。
         */
        /**
         *获取Y轴坐标。
         *@return Y轴坐标。
         */
        get y(): number;
        set y(value: number);
        /**
         *设置Z轴坐标。
         *@param value Z轴坐标。
         */
        /**
         *获取Z轴坐标。
         *@return Z轴坐标。
         */
        get z(): number;
        set z(value: number);
        /**
         *设置W轴坐标。
         *@param value W轴坐标。
         */
        /**
         *获取W轴坐标。
         *@return W轴坐标。
         */
        get w(): number;
        set w(value: number);
        reset(x: number, y: number, z: number, w: number): void;
        static lerp(a: Vector4, b: Vector4, t: number, out: Vector4): void;
        static transformByM4x4(vector4: Vector4, m4x4: Matrix4x4, out: Vector4): void;
        static equals(a: Vector4, b: Vector4): boolean;
        static normalize(s: Vector4, out: Vector4): void;
        static add(a: Vector4, b: Vector4, out: Vector4): void;
        static subtract(a: Vector4, b: Vector4, out: Vector4): void;
        static multiply(a: Vector4, b: Vector4, out: Vector4): void;
        static scale(a: Vector4, b: number, out: Vector4): void;
        static Clamp(value: Vector4, min: Vector4, max: Vector4, out: Vector4): void;
        static distanceSquared(value1: Vector4, value2: Vector4): number;
        static distance(value1: Vector4, value2: Vector4): number;
        static dot(a: Vector4, b: Vector4): number;
        static min(a: Vector4, b: Vector4, out: Vector4): void;
        static max(a: Vector4, b: Vector4, out: Vector4): void;
        static ZERO: Vector4;
        static ONE: Vector4;
        static UnitX: Vector4;
        static UnitY: Vector4;
        static UnitZ: Vector4;
        static UnitW: Vector4;
    }
}
