/**
 * @private
 */
interface CanvasRenderingContext2D {
    imageSmoothingEnabled: boolean;
    $imageSmoothingEnabled: boolean;
    $offsetX: number;
    $offsetY: number;
}
declare namespace egret {
    class CanvasRenderer {
        private nestLevel;
        render(displayObject: DisplayObject, buffer: sys.RenderBuffer, matrix: Matrix, forRenderTexture?: boolean): number;
        /**
         * @private
         * 绘制一个显示对象
         */
        private drawDisplayObject;
        private drawWithFilter;
        private drawWithClip;
        private drawWithScrollRect;
        drawNodeToBuffer(node: sys.RenderNode, buffer: sys.RenderBuffer, matrix: Matrix, forHitTest?: boolean): void;
        /**
         * 将一个DisplayObject绘制到渲染缓冲，用于RenderTexture绘制
         * @param displayObject 要绘制的显示对象
         * @param buffer 渲染缓冲
         * @param matrix 要叠加的矩阵
         */
        drawDisplayToBuffer(displayObject: DisplayObject, buffer: sys.RenderBuffer, matrix: Matrix): number;
        private renderNode;
        private renderNormalBitmap;
        private renderBitmap;
        private renderMesh;
        private drawMesh;
        renderText(node: sys.TextNode, context: CanvasRenderingContext2D): void;
        private renderingMask;
        /**
         * @private
         */
        renderGraphics(node: sys.GraphicsNode, context: CanvasRenderingContext2D, forHitTest?: boolean): number;
        private renderPath;
        private renderGroup;
        private createRenderBuffer;
        renderClear(): void;
        /**
         * Do special treatment on wechat ios10
         */
        wxiOS10: boolean;
    }
    /**
     * @private
     * 获取字体字符串
     */
    function getFontString(node: sys.TextNode, format: sys.TextFormat): string;
    /**
     * @private
     * 获取RGBA字符串
     */
    function getRGBAString(color: number, alpha: number): string;
}
