module GYLite
{		
	/**下拉菜单, 可以使用ISkinTheme的GetMenu快速构建
	 * */
	export class GYMenu extends GYSkinContainer
	{
		public static default_autoSize:boolean = true;
		public static default_autoClose:boolean;		
		protected _filterArr:Array<any>;
		protected _sourceArr:Array<any>;
		protected _autoSize:boolean;
		protected _autoClose:boolean;
		protected _filterFunc:Function;		
		/*** 
		 * @param skin 参见MenuSkin		 
		 * */
		public constructor(skin:IMenuSkin=null)
		{
			super();
			var s = this;
			this.skin = skin;
			s.initComponent();
		}
		protected initComponent():void
		{
			let s = this;
			s._autoSize = GYMenu.default_autoSize;
			s._autoClose = GYMenu.default_autoClose;
		}
		/**设置数据源*/
		public set dataProvider(val:Array<any>)
		{
			let s = this;
			s._sourceArr = val;
			s._filterArr = s._sourceArr.concat();			
			s.list.dataProvider=s._filterArr;
			s.autoSize = s._autoSize;
		}
		public get dataProvider():Array<any>
		{
			let s = this;
			return s.list.dataProvider;
		}
		/**纵向滚动条*/
		public get vScroller():GYScrollBarV
		{
			let s = this;
			return s.list.vScroller;
		}
		/**菜单列表*/
		public get list():GYListV
		{
			let s = this;
			return s._skin.list;
		}
		public set list(val:GYListV)
		{
			let s = this;
			if(s._skin.list == val)return;
			s._skin.list = val;
		}
		/**菜单背景*/
		public get menuBack():GYListV
		{
			let s = this;
			return s._skin.backgroundImage;
		}
		public set menuBack(val:GYListV)
		{
			let s = this;
			s._skin.backgroundImage = val;
		}
		public set autoSize(val:boolean)
		{
			let s = this;
			s._autoSize = val;
			let list:GYLite.GYListV;
			list = s.list;
			if(s._autoSize)
			{
				if(s._filterArr && s._filterArr.length < list.boxs)
					s.height = s._filterArr.length * (list.boxH + list.padding) + list.paddingBottom + list.paddingTop;
				else
					s.height = s.dataProvider.length * (list.boxH + list.padding) + list.paddingBottom + list.paddingTop;
			}
		}
		/**是否自适应高度*/
		public get autoSize():boolean
		{
			let s = this;
			return s._autoSize;
		}
		/**滚动条 0自动 1显示 2不显示*/
		public set scrollerPolicy(val:number)
		{
			let s = this;
			s.list.scrollerPolicy = val;
		}
		public get scrollerPolicy():number
		{
			let s = this;
			return s.list.scrollerPolicy;
		}
		/**设置过滤函数s.setFilterFunc,此函数有一个参数Array源数组，一个参数Array过滤数组、一个参数result输入框的内容，过滤数组放入符合的选项
		 * val(Array;Array;String)
		 * */
		public setFilterFunc(val:Function)
		{
			let s = this;
			s._filterFunc = val;
		}
		/**使用过滤函数s.filterFunc过滤无用的选项
		 * @param 过滤的字符串参数
		 * */
		public userFilter(result:string):Array<any>
		{
			let s = this;
			if(s._filterFunc!=null)
				s._filterFunc(s._sourceArr,s._filterArr,result);
			s.autoSize = s._autoSize;
			s.list.dataProvider = s._filterArr;
			return s._filterArr;
		}
		/**重写此方法，提供菜单项*/
		protected getMenuItem():IItemRender
		{			
			var item:MenuItem;
			item = new MenuItem;
			return item;
		}
		/**重写此方法，设置菜单项数据*/
		protected setMenuItem(item:IItemRender,data:any):void
		{
			let s = this;
			item.setData(data);
		}
		
		/**获取主题皮肤，自定义皮肤请实现IMenuSkin接口*/
		protected getThemeSkin():IGYSkin
		{
			let s = this;
			return GYSprite.skinTheme.GetMenuSkin();
		}
		/**外部点击自动关闭*/
		public get autoClose():boolean
		{
			let s = this;
			return s._autoClose;
		}

		public set autoClose(value:boolean)
		{
			let s = this;
			if(s._autoClose)
				s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s._autoClose = value;
			if(s._autoClose)
				s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private addToStage(e:egret.Event):void
		{
			let s = this;
			GYSprite.addStageDown(this, s.outsideRelease, s, true);
		}
		protected outsideRelease(e:GYTouchEvent):void
		{
			let s = this;
			if(e.currentTarget == e.target)
			{
				if(s.parent)
					(<any>s.parent).removeElement(this);
				return;
			}
			e.result = 1;
		}

	}
}