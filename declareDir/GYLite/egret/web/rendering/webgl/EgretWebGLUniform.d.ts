declare namespace egret.web {
    /**
     * @private
     */
    class EgretWebGLUniform {
        private gl;
        private name;
        type: WEBGL_UNIFORM_TYPE;
        private size;
        private location;
        constructor(gl: WebGLRenderingContext, program: WebGLProgram, uniformData: any);
        value: any;
        private setDefaultValue;
        setValue: Function;
        private generateSetValue;
        upload: Function;
        private generateUpload;
    }
}
