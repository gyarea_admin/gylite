module GYLite
{
			
						/**GYDrawBitmapData是继承BitmapData的位图数据，使用对象池管理的临时位图，使用固定宽高的BitmapData来临时使用
	 * 例如输入230,230，则提供使用256,256的BitmapData，进行裁切使用，所以尽量接近以下尺寸使用率会更高
	 * 1024,512,256,128,64,32,16,8*/
	export class GYDrawBitmapData extends egret.RenderTexture
	{
		public $bitmapDataId:number;
		public id:number;
		/**设定的宽度*/public realWidth:number;
		/**设定的高度*/public realHeight:number;
		private _clearTime:number;
		private _refCount:number;		
		private _width:number;
		private _height:number;
		private _transparent:boolean;
		private _fillColor:number;
		// private _texture:egret.RenderTexture;
		public constructor(width:number, height:number, transparent:boolean=true, fillColor:number=4.294967295E9)
		{
			super();			
			var s = this;
			s._width = width;
			s._height = height;
			s._transparent = transparent;
			s._fillColor = fillColor;		
			s._refCount = 0;	
			// s._texture = new egret.RenderTexture;
		}
		public get clearTime():number
		{
			let s = this;
			return s._clearTime;
		}
		/**此对象的引用计数*/
		public get refCount():number
		{
			let s = this;
			return s._refCount;
		}
		/**获取GYDrawBitmapdata引用，持有引用可保证GYDrawBitmapData不被回收，当引用计数为0则会被定期回收
		 * 使用此临时位图的引用必须严格遵守一次getRef获得引用，一次s.relRef释放引用，不要重复操作，以免计数错误，当计数为0，仍然执行s.relRef释放引用的话会抛出异常，此时应检查引用获取与释放是不是严格一对一
		 * */
		public getRef():GYDrawBitmapData
		{
			let s = this;
			++s._refCount;
			return this;
		}
		/**释放引用，引用数为0时，执行此函数会抛出异常*/
		public relRef():void
		{
			let s = this;
			--s._refCount;
			if(s._refCount == 0)
			{
				// s.dispose();
				GYDrawBitmapData._bmpVec[s.id].push(s);
				s._clearTime = CommonUtil.loopTime;
			}
			else
				throw(new Error("此对象已经被清理"));
		}
		public draw(source:any, clipRect:egret.Rectangle = null, smoothing:Boolean = false):void
		{
			var s = this;
			s.drawToTexture(source,clipRect);
		}
		
		public set width(val:number)
		{
			this._width = val;
		}
		public get width():number
		{
			return this._width;
		}
		public set height(val:number)
		{
			this._height = val;
		}
		public get height():number
		{
			return this._height;
		}
		public get format(): any
		{
			let s = this;
			return s.getBitmapData().format;
		}
		public set format(val:any)
		{
			let s = this;
			s.getBitmapData().format = val;
		}

		private getBitmapData():any
		{
			let s = this;
			return s.bitmapData;
		}
		private static _sizeVec:number[]= new Array<number>(1920,1440,1280,1024,512,256,128,64,32,16,8);
		private static _bmpVec:GYDrawBitmapData[][]=new Array<GYDrawBitmapData[]>(121);		
		private static _checkInterval:number;
		private static _checkTime:number;
		private static _timeId:number;
		private static _timeoutMilSec:number = 150000;
		/**内存中GYDrawBitmapData的数量*/public static num:number = 0;
		/**库存位图占用的内存*/public static memory:number=0;
		/**获取GYDrawBitmapData*/
		public static getBitmapData(w:number,h:number):GYDrawBitmapData
		{
			var len:number = GYDrawBitmapData._sizeVec.length;
			var i:number=0,j:number=0;
			var newW:number=0,newH:number=0;
			while(--len > -1)
			{
				if(w <= GYDrawBitmapData._sizeVec[len])
				{
					newW = GYDrawBitmapData._sizeVec[len];
					i = len;
					break;
				}
			}
			len = GYDrawBitmapData._sizeVec.length;
			while(--len > -1)
			{
				if(h <= GYDrawBitmapData._sizeVec[len])
				{
					newH = GYDrawBitmapData._sizeVec[len];
					j = len;
					break;
				}
			}
			var ind:number = i*10 + j;
			var b:GYDrawBitmapData;
			var vec:GYDrawBitmapData[] = GYDrawBitmapData._bmpVec[ind];
			if(!vec)vec = GYDrawBitmapData._bmpVec[ind] = new Array<GYDrawBitmapData>();
			if(vec.length == 0)
			{
				b = new GYDrawBitmapData(newW,newH,true,0);//宽高为0则超尺寸
				b.id = ind;
				++GYDrawBitmapData.num;
			}
			else
				b = vec.pop();
			GYSprite._rect.width = newW;
			GYSprite._rect.height = newH;
			// b.fillRect(GYSprite._rect,0);			
			b.realWidth = w;
			b.realHeight = h;
			return b;
		}
		/**设置位图清理的时间间隔
		 * @param val 时间间隔(毫秒) 为0时不回收
		 * */
		public static setResCheck(val:number):void
		{
			if(GYDrawBitmapData._checkInterval > 0)
				TimeManager.unRegistered(GYDrawBitmapData._timeId,GYDrawBitmapData.CheckResUse, null);
			GYDrawBitmapData._checkInterval = val;
			if(val > 0)
			{
				GYDrawBitmapData._checkTime = egret.getTimer();
				GYDrawBitmapData._timeId = TimeManager.registered(GYDrawBitmapData.CheckResUse, null);
			}
		}
		
		private static CheckResUse(sysTime:number):void
		{
			if(sysTime - GYDrawBitmapData._checkTime < GYDrawBitmapData._checkInterval)
				return;
			GYDrawBitmapData._checkTime = sysTime;
			var len:number,len2:number;
			var vec:GYDrawBitmapData[];
			var bitmapData:GYDrawBitmapData;
			GYDrawBitmapData.memory = 0;
			len = GYDrawBitmapData._bmpVec.length;
			while(--len > -1)
			{
				vec = GYDrawBitmapData._bmpVec[len];
				if(vec)
					len2 = vec.length;
				else
					continue;
				while(--len2 > -1)
				{
					bitmapData = vec[len2];
					if(sysTime - bitmapData.clearTime  > GYDrawBitmapData._timeoutMilSec)
					{
						bitmapData.dispose();
						vec.splice(len2,1);
						--GYDrawBitmapData.num;
					}
					else
						GYDrawBitmapData.memory +=bitmapData.width*bitmapData.height*4;
				}
			}
		}
		
	}
}