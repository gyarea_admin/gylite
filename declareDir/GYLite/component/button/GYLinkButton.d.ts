/**
 @author 迷途小羔羊
 2015.3.22
 */
declare module GYLite {
    class GYLinkButton extends GYButton {
        constructor(skin?: any);
        protected getThemeSkin(): IGYSkin;
    }
}
