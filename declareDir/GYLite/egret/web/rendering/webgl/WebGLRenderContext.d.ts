declare namespace egret.web {
    /**
     * @private
     * WebGL上下文对象，提供简单的绘图接口
     * 抽象出此类，以实现共用一个context
     */
    class WebGLRenderContext implements egret.sys.RenderContext {
        static antialias: boolean;
        _defaultEmptyTexture: WebGLTexture;
        /**
         * 渲染上下文
         */
        context: WebGLRenderingContext;
        /**
         * 呈现最终绘图结果的画布
         */
        surface: HTMLCanvasElement;
        /**
         * WebGLRenderContext单例
         */
        private static instance;
        static getInstance(width?: number, height?: number, context?: WebGLRenderingContext): WebGLRenderContext;
        $maxTextureSize: number;
        /**
         * 顶点数组管理器
         */
        private vao;
        /**
         * 绘制命令管理器
         */
        drawCmdManager: WebGLDrawCmdManager;
        /**
         * render buffer 堆栈
         */
        $bufferStack: WebGLRenderBuffer[];
        /**
         * 当前绑定的render buffer
         */
        private currentBuffer;
        /**
         * 推入一个RenderBuffer并绑定
         */
        pushBuffer(buffer: WebGLRenderBuffer): void;
        /**
         * 推出一个RenderBuffer并绑定上一个RenderBuffer
         */
        popBuffer(): void;
        private bindIndices;
        /**
         * 启用RenderBuffer
         */
        private activateBuffer;
        /**
         * 上传顶点数据
         */
        private uploadVerticesArray;
        /**
         * 上传索引数据
         */
        private uploadIndicesArray;
        private vertexBuffer;
        private indexBuffer;
        constructor(width?: number, height?: number, context?: WebGLRenderingContext);
        /**
         * 销毁绘制对象
         */
        destroy(): void;
        onResize(width?: number, height?: number): void;
        /**
         * 改变渲染缓冲的大小并清空缓冲区
         * @param width 改变后的宽
         * @param height 改变后的高
         * @param useMaxSize 若传入true，则将改变后的尺寸与已有尺寸对比，保留较大的尺寸。
         */
        resize(width: number, height: number, useMaxSize?: boolean): void;
        static glContextId: number;
        glID: number;
        projectionX: number;
        projectionY: number;
        contextLost: boolean;
        private _supportedCompressedTextureInfo;
        pvrtc: any;
        etc1: any;
        private _buildSupportedCompressedTextureInfo;
        private initWebGL;
        getSupportedCompressedTexture(): void;
        private handleContextLost;
        private handleContextRestored;
        private getWebGLContext;
        private setContext;
        /**
         * 开启模版检测
         */
        enableStencilTest(): void;
        /**
         * 关闭模版检测
         */
        disableStencilTest(): void;
        /**
         * 开启scissor检测
         */
        enableScissorTest(rect: egret.Rectangle): void;
        /**
         * 关闭scissor检测
         */
        disableScissorTest(): void;
        /**
         * 获取像素信息
         */
        getPixels(x: any, y: any, width: any, height: any, pixels: any): void;
        /**
         * 创建一个WebGLTexture
         */
        createTexture(bitmapData: BitmapData | HTMLCanvasElement): WebGLTexture;
        private checkCompressedTextureInternalFormat;
        private $debugLogCompressedTextureNotSupported;
        private createCompressedTexture;
        /**
         * 更新材质的bitmapData
         */
        updateTexture(texture: WebGLTexture, bitmapData: BitmapData): void;
        get defaultEmptyTexture(): WebGLTexture;
        getWebGLTexture(bitmapData: BitmapData): WebGLTexture;
        /**
         * 清除矩形区域
         */
        clearRect(x: number, y: number, width: number, height: number): void;
        /**
         * 设置混色
         */
        setGlobalCompositeOperation(value: string): void;
        /**
         * 绘制图片，image参数可以是BitmapData或者renderTarget //GYLite 增加color
         */
        drawImage(image: BitmapData, sourceX: number, sourceY: number, sourceWidth: number, sourceHeight: number, destX: number, destY: number, destWidth: number, destHeight: number, imageSourceWidth: number, imageSourceHeight: number, rotated: boolean, smoothing?: boolean, color?: number): void;
        /**
         * 绘制Mesh //GYLite 增加color
         */
        drawMesh(image: BitmapData, sourceX: number, sourceY: number, sourceWidth: number, sourceHeight: number, destX: number, destY: number, destWidth: number, destHeight: number, imageSourceWidth: number, imageSourceHeight: number, meshUVs: number[], meshVertices: number[], meshIndices: number[], bounds: Rectangle, rotated: boolean, smoothing: boolean, color?: number): void;
        /**
         * 绘制材质
         */
        drawTexture(texture: WebGLTexture, sourceX: number, sourceY: number, sourceWidth: number, sourceHeight: number, destX: number, destY: number, destWidth: number, destHeight: number, textureWidth: number, textureHeight: number, meshUVs?: number[], meshVertices?: number[], meshIndices?: number[], bounds?: Rectangle, rotated?: boolean, smoothing?: boolean, color?: number): void;
        /**
         * 绘制矩形（仅用于遮罩擦除等）
         */
        drawRect(x: number, y: number, width: number, height: number): void;
        /**
         * 绘制遮罩
         */
        pushMask(x: number, y: number, width: number, height: number): void;
        /**
         * 恢复遮罩
         */
        popMask(): void;
        /**
         * 清除颜色缓存
         */
        clear(): void;
        $scissorState: boolean;
        /**
         * 开启scissor test
         */
        enableScissor(x: number, y: number, width: number, height: number): void;
        /**
         * 关闭scissor test
         */
        disableScissor(): void;
        /**
         * 执行目前缓存在命令列表里的命令并清空
         */
        activatedBuffer: WebGLRenderBuffer;
        private readonly vertexCountPerTriangle;
        private readonly triangleCountPerQuad;
        private readonly dataCountPerVertex;
        $drawWebGL(): void;
        /**
         * 执行绘制命令
         */
        private drawData;
        currentProgram: EgretWebGLProgram;
        private activeProgram;
        private syncUniforms;
        /**
         * 画texture
         **/
        private drawTextureElements;
        /**
         * @private
         * 画rect
         **/
        private drawRectElements;
        /**
         * 画push mask
         **/
        private drawPushMaskElements;
        /**
         * 画pop mask
         **/
        private drawPopMaskElements;
        private vertSize;
        /**
         * 设置混色
         */
        private setBlendMode;
        $filter: ColorMatrixFilter;
        /**
         * 应用滤镜绘制给定的render target
         * 此方法不会导致input被释放，所以如果需要释放input，需要调用此方法后手动调用release
         */
        drawTargetWidthFilters(filters: Filter[], input: WebGLRenderBuffer): void;
        /**
         * 向一个renderTarget中绘制
         * */
        private drawToRenderTarget;
        static blendModesForGL: any;
        static initBlendMode(): void;
        /**
         * @private
         */
        $beforeRender: () => void;
    }
}
