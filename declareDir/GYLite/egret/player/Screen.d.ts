declare namespace egret.sys {
    /**
     * @private
     * 设备屏幕
     */
    interface Screen {
        /**
         * @private
         * 更新屏幕视口尺寸
         */
        updateScreenSize(): any;
        /**
         * @private
         * 更新触摸数量
         */
        updateMaxTouches(): any;
        /**
         * @private
         * 设置分辨率尺寸
         */
        setContentSize(width: number, height: number): any;
    }
}
