declare module GYLite {
    class ImageLabelData {
        disposed: boolean;
        private _inPool;
        bitmapData: egret.Texture;
        width: number;
        height: number;
        offsetX: number;
        offsetY: number;
        constructor();
        setTo(b: egret.Texture, w: number, h: number, ox: number, oy: number): void;
        clear(): void;
        get inPool(): boolean;
        set inPool(val: boolean);
        outPoolInit(): void;
        dispose(): void;
    }
}
