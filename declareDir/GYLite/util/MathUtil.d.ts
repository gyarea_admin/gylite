/**
 * @author [迷途小羔羊]
 * 创建于：2018-1-25
 * 最后修改：2018-1-25
 * contact:[qq:229449388,mail:229449388@qq.com]
 */
declare module GYLite {
    class MathUtil {
        private static sin_map;
        private static cos_map;
        static DEG_TO_RAD: number;
        static tempPt: egret.Point;
        private static _instance;
        constructor();
        /**计算过一点做平行线的另外一点坐标
         * @param p1x 直线上的A点X坐标
         * @param p1y 直线上的A点Y坐标
         * @param p2x 直线上的B点X坐标
         * @param p2y 直线上的B点Y坐标
         * @param p3x 平行线上的C点X坐标
         * @param p3y 平行线上的C点Y坐标
         * @param val 乘量
         * @param 返回的平行点P，赋值null则创建新的
         * @return 返回平行点P
         * */
        static calParallelPoint(p1x: number, p1y: number, p2x: number, p2y: number, p3x: number, p3y: number, val?: number, p?: egret.Point): egret.Point;
        /**创建一个等边多边形
         * @param r 半径
         * @param num 边数量
        */
        static calPolygonByRadius(r: number, num?: number, startDegree?: number): number[];
        /**计算多边形最大外接圆半径(凸多边形)
         * @param arr 轮廓数组
         * @param minDis 最小范围
         * @param maxDis 最大范围
         * @param result 当前值
        */
        static calMaxRadiusInShape(arr: number[], minDis?: number, maxDis?: number, result?: number): number;
        /**计算多边形最小内切圆半径
         * @param arr 轮廓数组
         * @param minDis 最小范围
         * @param maxDis 最大范围
         * @param result 当前值
        */
        static calMinRadiusInShape(arr: number[], minDis?: number, maxDis?: number, result?: number): number;
        /**计算多边形最小边框*/
        static calMaxRect(arr: number[]): number[];
        /**计算过一点做垂线的垂点坐标
         * @param p1x 直线上的A点X坐标
         * @param p1y 直线上的A点Y坐标
         * @param p2x 直线上的B点X坐标
         * @param p2y 直线上的B点Y坐标
         * @param p3x 垂线上的C点X坐标
         * @param p3y 垂线上的C点Y坐标
         * @param 返回的垂点P，赋值null则创建新的
         * @return 返回垂点P
         * */
        static calVerticalPoint(p1x: number, p1y: number, p2x: number, p2y: number, p3x: number, p3y: number, p?: egret.Point): egret.Point;
        /**计算两直线交点
         * @param p1StartX 直线A的起点x
         * @param p1StartY 直线A的起点y
         * @param p1EndX 直线A的终点x
         * @param p1EndY 直线A的终点y
         * @param p2StartX 直线B的起点x
         * @param p2StartY 直线B的起点y
         * @param p2EndX 直线B的终点x
         * @param p2EndY 直线B的终点y
         * @param p 接受返回的交点Point对象，传null则创建新的Point对象
         * @param return 返回交点p 不相交则返回null
        */
        static calCrossPoint(p1StartX: number, p1StartY: number, p1EndX: number, p1EndY: number, p2StartX: number, p2StartY: number, p2EndX: number, p2EndY: number, p?: egret.Point, segment?: boolean): egret.Point;
        /**计算线段上指定长度的点的坐标
         * @param p1x 起点x
         * @param p1y 起点y
         * @param p2x 终点x
         * @param p2y 终点y
         * @param len 长度
         * @param angle 偏移角度
         * */
        static calLinePoint(p1x: number, p1y: number, p2x: number, p2y: number, len?: number, angle?: number): egret.Point;
        static polar(r: number, angle: number, result?: egret.Point): egret.Point;
        /**计算出多边形面积
         * @param shapeArr 图形描点
         * @param hasDirect 面积是否有向，默认false，面积小于0 顶点逆时针 大于0 顶点顺时针 等于0  (注意2d坐标系是y轴翻转的，所以图形的顶点顺序应该跟数学坐标系计算相反)
        */
        static calPolygonArea(shapeArr: number[], hasDirect?: boolean): number;
        /**计算三角形面积*/
        static calTriangleArea(x1: number, y1: number, x2: number, y2: number, x3: number, y3: number, hasDirect?: boolean): number;
        /**快速算出角度value的sin值(近似值)
         * @param value 角度 注意此参数为角度
        */
        static sin(value: number): number;
        /**快速算出整数角度的sin值
         * @param value 角度 注意此参数为角度
        */
        static sinInt(value: number): number;
        /**快速算出角度value的cos值(近似值)
         * @param value 角度 注意此参数为角度
        */
        static cos(value: number): number;
        /**快速算出整数角度的cos值
         * @param value 角度 注意此参数为角度
        */
        static cosInt(value: number): number;
        /**角度0-2PI内取模*/
        static clampAngle(value: number): number;
        /**角度0-2PI内取模*/
        static clampRotaion(value: number): number;
        /**点集排序，作用是排序后，按顺序连线可以形成不错位的多边形
         * @param vPoints 顶点数组[x1,y1,x2,y2……]
         * @param clockwise 是否顺时针 默认true
        */
        static clockwiseSortPoints(vPoints: number[], clockwise?: boolean): void;
        /**计算多边形交集
         * @param poly1 多边形顶点数组1 [x1,y1,x2,y2……]
         * @param poly2 多边形顶点数组2 [x1,y1,x2,y2……]
         * @param result 存放结果的顶点数组 [x1,y1,x2,y2……]
        */
        static polygonClip(poly1: number[], poly2: number[], result?: number[]): boolean;
        static indexOfPoint(arr: number[], x: number, y: number): number;
        /**通过点积比较a和b的距离重心center的向量大小*/
        static pointCmp(ax: number, ay: number, bx: number, by: number, centerX: number, centerY: number): boolean;
        /**点积*/
        static dot(vx1: number, vy1: number, vx2: number, vy2: number): number;
        /**叉积*/
        static cross(vx1: number, vy1: number, vx2: number, vy2: number): number;
        /**3d点积*/
        static d3Dot(vx1: number, vy1: number, vz1: number, vx2: number, vy2: number, vz2: number): number;
        /**3d叉积*/
        static d3Cross(vx1: number, vy1: number, vz1: number, vx2: number, vy2: number, vz2: number, result?: GYLite.Vector3): GYLite.Vector3;
        /**求3个点所在的面的法向量*/
        /**求射线在平面上相交的一点
         * @param 射线的近点a
         * @param 射线的远点b
         * @param 平面上的一点 pt
         * @param 平面的法向量 n 求3个点所在屏幕的法向量请使用接口normalVector
         * **/
        static rayPoint(a: GYLite.Vector3, b: GYLite.Vector3, pt: GYLite.Vector3, n: GYLite.Vector3, result?: GYLite.Vector3): GYLite.Vector3;
        /**计算直线ab的夹角(或者向量[bx - ax,by - ay])*/
        static calAngle(ax: number, ay: number, bx: number, by: number): number;
        /**计算向量v1和v2的夹角*/
        static calVecAngle(v1x: number, v1y: number, v2x: number, v2y: number): number;
        /**根据权重随机获取，例如权重[3,3,4]，总和10，每个区间分别[0.3,0.6,1]，返回随机数小于的对应值索引*/
        static randomGet(arr: number[]): number;
        /**计算撞墙后延墙壁的偏转角度
         * @param vecX 墙体向量x
         * @param vecY 墙体向量y
         * @param vx 移动向量x
         * @param vy 移动向量y
         * @param agl 移动角度
         **/
        static calDeflectionAngle(vecX: number, vecY: number, vx: number, vy: number): number;
        /**取区间限制值**/
        static clamp(value: number, min: number, max: number): number;
    }
    class MathPoint {
        x: number;
        y: number;
        k: number;
        hash: number;
        constructor(x?: number, y?: number);
    }
}
