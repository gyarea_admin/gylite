declare namespace egret {
    class CompressedTextureData {
        glInternalFormat: number;
        width: number;
        height: number;
        byteArray: Uint8Array;
        face: number;
        level: number;
    }
    const etc_alpha_mask = "etc_alpha_mask";
    const engine_default_empty_texture = "engine_default_empty_texture";
    const is_compressed_texture = "is_compressed_texture";
    const glContext = "glContext";
    const UNPACK_PREMULTIPLY_ALPHA_WEBGL = "UNPACK_PREMULTIPLY_ALPHA_WEBGL";
    /**
     * A BitmapData object contains an array of pixel data. This data can represent either a fully opaque bitmap or a
     * transparent bitmap that contains alpha channel data. Either type of BitmapData object is stored as a buffer of 32-bit
     * integers. Each 32-bit integer determines the properties of a single pixel in the bitmap.<br/>
     * Each 32-bit integer is a combination of four 8-bit channel values (from 0 to 255) that describe the alpha transparency
     * and the red, green, and blue (ARGB) values of the pixel. (For ARGB values, the most significant byte represents the
     * alpha channel value, followed by red, green, and blue.)
     * @see egret.Bitmap
     * @version Egret 2.4
     * @platform Web,Native
     * @language en_US
     */
    /**
     * BitmapData 对象是一个包含像素数据的数组。此数据可以表示完全不透明的位图，或表示包含 Alpha 通道数据的透明位图。
     * 以上任一类型的 BitmapData 对象都作为 32 位整数的缓冲区进行存储。每个 32 位整数确定位图中单个像素的属性。<br/>
     * 每个 32 位整数都是四个 8 位通道值（从 0 到 255）的组合，这些值描述像素的 Alpha 透明度以及红色、绿色、蓝色 (ARGB) 值。
     * （对于 ARGB 值，最高有效字节代表 Alpha 通道值，其后的有效字节分别代表红色、绿色和蓝色通道值。）
     * @see egret.Bitmap
     * @version Egret 2.4
     * @platform Web,Native
     * @language zh_CN
     */
    class BitmapData extends HashObject {
        /**是否canvas画布资源*/
        isCanvas: boolean;
        /**
         * The width of the bitmap image in pixels.
         * @readOnly
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 位图图像的宽度，以像素为单位。
         * @readOnly
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        width: number;
        /**
         * The height of the bitmap image in pixels.
         * @readOnly
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 位图图像的高度，以像素为单位。
         * @readOnly
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        height: number;
        /**
         * Original bitmap image.
         * HTMLImageElement|HTMLCanvasElement|HTMLVideoElement
         * @version Egret 2.4
         * @platform Web,Native
         * @private
         * @language en_US
         */
        /**
         * 原始位图图像。
         * HTMLImageElement|HTMLCanvasElement|HTMLVideoElement
         * @version Egret 2.4
         * @platform Web,Native
         * @private
         * @language zh_CN
         */
        $source: any;
        /**
         * WebGL texture.
         * @version Egret 2.4
         * @platform Web,Native
         * @private
         * @language en_US
         */
        /**
         * WebGL纹理。
         * @version Egret 2.4
         * @platform Web,Native
         * @private
         * @language zh_CN
         */
        webGLTexture: any;
        /**
         * Texture format.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 纹理格式。
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        format: string;
        /**
         * @private
         * webgl纹理生成后，是否删掉原始图像数据
         */
        $deleteSource: boolean;
        /**
         * @private
         * id
         */
        $nativeBitmapData: egret_native.NativeBitmapData;
        /**
         * @private
         *
         */
        readonly compressedTextureData: Array<Array<CompressedTextureData>>;
        debugCompressedTextureURL: string;
        $etcAlphaMask: Nullable<BitmapData>;
        /**
         * Initializes a BitmapData object to refer to the specified source object.
         * @param source The source object being referenced.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 创建一个引用指定 source 实例的 BitmapData 对象
         * @param source 被引用的 source 实例
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        constructor(source: any);
        get source(): any;
        set source(value: any);
        static create(type: "arraybuffer", data: ArrayBuffer, callback?: (bitmapData: BitmapData) => void): BitmapData;
        static create(type: "base64", data: string, callback?: (bitmapData: BitmapData) => void): BitmapData;
        $dispose(): void;
        private static _displayList;
        static $addDisplayObject(displayObject: DisplayObject, texture: Texture): void;
        static $removeDisplayObject(displayObject: DisplayObject, texture: Texture): void;
        static $invalidate(texture: Texture): void;
        static $dispose(texture: Texture): void;
        private _getCompressedTextureData;
        getCompressed2dTextureData(): CompressedTextureData;
        $setCompressed2dTextureData(levelData: egret.CompressedTextureData[]): void;
        hasCompressed2d(): boolean;
        clearCompressedTextureData(): void;
        set etcAlphaMask(value: any);
        get etcAlphaMask(): any;
    }
}
