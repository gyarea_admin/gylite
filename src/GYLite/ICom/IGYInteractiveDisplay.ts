module GYLite
{
	
	/**羔羊组件的交互显示对象基础接口*/
	export interface IGYInteractiveDisplay extends IGYDisplay
	{
		toolTip:GYToolTip;		
		toolTipString:string;		
	}
}