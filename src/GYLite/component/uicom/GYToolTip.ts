module GYLite
{
							
								
	/**当鼠标悬停在显示对象的时候，设置s.toolTipString会弹出GYToolTip类的面板提示信息*/
	export class GYToolTip extends GYSprite
	{
		private background:GYSprite;
		public labelDisplay:GYText;
		private gradientMatrix:egret.Matrix;
		private colorArr:Array<any>;
		private alphaArr:Array<any>;
		private percentArr:Array<any>;
		public tipfollowTarget:GYSprite;
		/**当前提示的显示对象*/
		public user:IGYInteractiveDisplay;
		/**tip的s.x偏移,请在s.show方法调用前设置一次，s.hide方法调用将会被重置为默认值*/
		public offsetX:number=-16;
		/**tip的s.y偏移,请在s.show方法调用前设置一次，s.hide方法调用将会被重置为默认值*/
		public offsetY:number=-32;
		public constructor()
		{
			super();
			
			let s = this;
			s.touchChildren = s.touchEnabled = false;
			s.initComponent();
		}
		protected initComponent():void
		{
			let s = this;
			s.background = new GYSprite;
			s.addElement(s.background);
			s.labelDisplay = new GYText(true);
			s.labelDisplay.color = 0;
			s.labelDisplay.autoWidth = true;
			s.labelDisplay.paddingRight = s.labelDisplay.paddingLeft = s.labelDisplay.paddingTop = 5;
			
			s.addElement(s.labelDisplay);
			s.gradientMatrix =  new egret.Matrix;
			s.colorArr = [0xffffff, 0xe0e0ef];
			s.alphaArr = [1,1];
			s.percentArr = [0,255];
		}
		public updateView():void
		{
			let s = this;
			super.updateView();
			s.drawBackground(s.labelDisplay.width,s.labelDisplay.height);
		}
		protected drawBackground(w:number,h:number):void
		{
			let s = this;
			var g:egret.Graphics = s.graphics;
			g.clear();
			s.gradientMatrix.createGradientBox(w,h,Math.PI/2);
			g.beginFill(0x333333,1);
			g.drawRoundRect(0,0,w,h,5,5);
			g.beginGradientFill(egret.GradientType.LINEAR,s.colorArr,s.alphaArr,s.percentArr,s.gradientMatrix);
			g.drawRoundRect(1,1,w-2,h-2,5,5);
			g.endFill();
		}
		public setText(val:string):GYToolTip
		{
			let s = this;
			s.labelDisplay.htmlText = val;
			s.labelDisplay.validFormat();
			s.updateView();
			return this;
		}
		/**显示tip*/
		public show(layer:egret.DisplayObjectContainer,isFollow:boolean=false):void
		{
			let s = this;
			if(s.parent==null)
			{
				layer.addChild(this);
				if(isFollow)
					CommonUtil.addStageLoop(s.FollowLoop,s);
				s.updatePos();
			}
		}
		public hide():void
		{
			let s = this;
			if(s.parent)
			{
				s.parent.removeChild(this);
				CommonUtil.delStageLoop(s.FollowLoop,s);
				s.offsetX = -16;
				s.offsetY = -32;
				s.user = null;
			}
		}
		private FollowLoop(t:number):void
		{
			let s = this;
			if(s.parent)
				s.updatePos();
			else
				CommonUtil.delStageLoop(s.FollowLoop,s);
		}
		/**重写此方法，可以自定义布局的位置*/
		protected updatePos():void
		{
			let s = this;
			var mX:number,mY:number;
			var pt:egret.Point;
			mX = (<any>s.parent).mouseX;
			mY = (<any>s.parent).mouseY;
			if(s.tipfollowTarget)
			{				
				pt = s.tipfollowTarget.localToGlobal(0,0);
				pt = s.parent.globalToLocal(pt.x,pt.y);
				mX = pt.x;
				mY = pt.y;
			}
			else
			{
				if(s.parent == GYSprite.stage)
				{
					mX = GYSprite.stageX;
					mY = GYSprite.stageY;
				}
				else
				{
					mX = (<any>s.parent).mouseX;
					mY = (<any>s.parent).mouseY;
				}				
			}
			mX|=0;
			mY|=0;
			s.x = mX + s.offsetX;
			if(s.stage.stageWidth < s.x + s.width)
				s.x = s.stage.stageWidth - s.width;
			else if(s.x < 0)
				s.x = 0;
			s.y = mY + s.offsetY;
			if(s.stage.stageHeight < s.y + s.height)
				s.y = s.stage.stageHeight - s.height;
			else if(s.y < 0)
				s.y = 0;			
		}
		
		private static _toolTip:GYToolTip;
		public static getInstance():GYToolTip
		{
			if(GYToolTip._toolTip == null)
			{
				GYToolTip._toolTip = new GYToolTip;
			}
			return GYToolTip._toolTip;
		}
	}
}