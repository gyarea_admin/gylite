/**
@author 迷途小羔羊
2022.09.27
*/
module GYLite
{
    export class GraphicsTexture extends BatchTexture{                 
        public constructor(){
            super();            
        }            
        protected beforeToPool():void
        {
            let s= this;            
            delete GraphicsTexture._graphicsTextureMap[s.hash];
            super.beforeToPool();
        }


        public static batchParam:BatchDrawParam = new BatchDrawParam(BatchDrawType.GRAPHICS);
        private static _graphicsTextureMap:any = {};
        public static getGraphicsDesc(param:any,type:GraphicsType):string
        {
            let str:string;
            let cap:CanvasLineCap;
            str = type.id + "";
            if(param.lineWidth)str += "," + param.lineWidth;
            if(param.lineCap)            
                cap = param.lineCap;                            
            // cap = param.lineWidth ==1?"square":(cap?cap:"round");
            // str += ","+cap;
            if(param.color)str += ","+param.color;            
            if(param.blurSize)str += ","+param.blurSize;
            if(param.scaleSizeX)str += ","+param.scaleSizeX;
            if(param.scaleSizeY)str += ","+param.scaleSizeY;
            if(param.radiusX)str += ","+param.radiusX;
            if(param.radiusY)str += ","+param.radiusY;
            if(param.gradientMatrix)str += ","+param.gradientMatrix.toString();
            if(param.gradientType)str += ","+param.gradientType;
            return str;
        }
        /**获取矢量绘制数据源*/
        public static getGraphicsTexture(param:any,type:GraphicsType):GraphicsTexture
        {
            let tex:GraphicsTexture;
            var hsCode:number;                        
            hsCode = egret.NumberUtils.convertStringToHashCode(GraphicsTexture.getGraphicsDesc(param,type));
            tex = GraphicsTexture._graphicsTextureMap[hsCode];            
            if(tex == null)
            {
                let graphicsElement:GraphicsElement;
                graphicsElement = <GraphicsElement>PoolUtil.fromPool(GraphicsElement);
                graphicsElement.reset(param, type);                
                tex = <GraphicsTexture>PoolUtil.fromPool(GraphicsTexture);
                tex.reset(hsCode, graphicsElement);           
                GraphicsTexture._graphicsTextureMap[hsCode] = tex;
            } 
            return tex;
        }

    }
}