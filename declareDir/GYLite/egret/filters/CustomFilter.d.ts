declare namespace egret {
    /**
     * custom filter, now support WebGL mode only.
     * @version Egret 4.1.0
     * @platform Web
     * @language en_US
     */
    /**
     * 自定义滤镜，目前仅支持WebGL模式
     * @version Egret 4.1.0
     * @platform Web
     * @language zh_CN
     */
    class CustomFilter extends Filter {
        /**
         * @private
         */
        $vertexSrc: string;
        /**
         * @private
         */
        $fragmentSrc: string;
        /**
         * @private
         */
        $shaderKey: string;
        /**
         * @private
         */
        type: string;
        private $padding;
        /**
         * The inner margin of the filter.
         * If the desired area of the custom filter is larger than the original area (stroke, etc.), you need to set it manually.
         * @version Egret 4.1.0
         * @platform Web
         * @language en_US
         */
        /**
         * 滤镜的内边距
         * 如果自定义滤镜所需区域比原区域大（描边等），需要手动设置
         * @version Egret 4.1.0
         * @platform Web
         * @language zh_CN
         */
        get padding(): number;
        set padding(value: number);
        /**
         * The initial value of the uniform in the shader (key, value one-to-one correspondence), currently only supports numbers and arrays.
         * @version Egret 4.1.0
         * @platform Web
         * @language en_US
         */
        /**
         * 着色器中uniform的初始值（key，value一一对应），目前仅支持数字和数组。
         * @version Egret 4.1.0
         * @platform Web
         * @language zh_CN
         */
        get uniforms(): any;
        /**
         * Initialize the CustomFilter object.
         * @param vertexSrc Custom vertex shader program.
         * @param fragmentSrc Custom fragment shader program.
         * @param uniforms The initial value of the uniform in the shader (key, value one-to-one correspondence), currently only supports numbers and arrays.
         * @version Egret 4.1.0
         * @platform Web
         * @language en_US
         */
        /**
         * 初始化 CustomFilter 对象
         * @param vertexSrc 自定义的顶点着色器程序。
         * @param fragmentSrc 自定义的片段着色器程序。
         * @param uniforms 着色器中uniform的初始值（key，value一一对应），目前仅支持数字和数组。
         * @version Egret 4.1.0
         * @platform Web
         * @language zh_CN
         */
        constructor(vertexSrc: string, fragmentSrc: string, uniforms?: any);
        /**
         * When native rendering acceleration is turned on, custom shaders need to be called manually when creating and updating properties
         * @version Egret 5.0.3
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 开启原生渲染加速后，自定义shader需要在创建和更新属性时手动调用
         * @version Egret 5.0.3
         * @platform Web,Native
         * @language zh_CN
         */
        onPropertyChange(): void;
    }
}
