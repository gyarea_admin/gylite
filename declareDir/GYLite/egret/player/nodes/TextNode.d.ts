declare namespace egret.sys {
    /**
     * @private
     * 文本渲染节点
     */
    class TextNode extends RenderNode {
        constructor();
        /**
         * 颜色值
         */
        textColor: number;
        /**
         * 描边颜色值
         */
        strokeColor: number;
        /**
         * 字号
         */
        size: number;
        /**
         * 描边大小
         */
        stroke: number;
        /**
         * 是否加粗
         */
        bold: boolean;
        /**
         * 是否倾斜
         */
        italic: boolean;
        /**
         * 字体名称
         */
        fontFamily: string;
        /**
         * 绘制一行文本
         */
        drawText(x: number, y: number, text: string, format: any): void;
        /**
         * 绘制x偏移
         */
        x: number;
        /**
         * 绘制y偏移
         */
        y: number;
        /**
         * 绘制宽度
         */
        width: number;
        /**
         * 绘制高度
         */
        height: number;
        /**
         * 脏渲染标记
         */
        dirtyRender: boolean;
        $texture: WebGLTexture;
        $textureWidth: number;
        $textureHeight: number;
        $canvasScaleX: number;
        $canvasScaleY: number;
        /**
         * 清除非绘制的缓存数据
         */
        clean(): void;
        /**
         * 在显示对象的$updateRenderNode()方法被调用前，自动清空自身的drawData数据。
         */
        cleanBeforeRender(): void;
        display: any;
    }
}
