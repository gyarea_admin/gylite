declare module GYLite {
    class Dictionary {
        dict: any;
        constructor();
        getValue(key: any): any;
        setValue(key: any, val: any): void;
        deleteKey(key: any): void;
        private getKeyCode;
    }
}
