declare module GYLite {
    class GYScrollGroup extends GYUIComponent {
        scrollerViewPort: GYGroup;
        scroller: GYScroller;
        constructor();
        protected initComponent(): void;
        /**@inheritDoc */
        getElementAt(index: number): IGYDisplay;
        /**@inheritDoc */
        getElementIndex(child: IGYDisplay): number;
        /**@inheritDoc*/
        delElement(child: IGYDisplay): void;
        /**@inheritDoc*/
        addElement(disObj: IGYDisplay): IGYDisplay;
        /**@inheritDoc*/
        addElementAt(disObj: IGYDisplay, index?: number): IGYDisplay;
        /**@inheritDoc*/
        removeElement(disObj: IGYDisplay): IGYDisplay;
        /**@inheritDoc*/
        removeElementAt(index?: number): IGYDisplay;
        /**@inheritDoc*/
        setElementIndex(child: IGYDisplay, index: number): void;
        /**@inheritDoc*/
        swapElementIndex(child1: any, child2: any): void;
        get numElement(): number;
        /**@inheritDoc*/
        set width(val: number);
        get width(): number;
        /**@inheritDoc*/
        set height(val: number);
        get height(): number;
    }
}
