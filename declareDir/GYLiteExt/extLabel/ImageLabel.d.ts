declare module GYLite {
    class ImageLabel extends GYSprite {
        private _dict;
        private _text;
        private _invalidText;
        private _bitmapVec;
        private _formatVec;
        private _align;
        private _clipBitmap;
        private _textWidth;
        private _textHeight;
        constructor();
        protected initComponent(): void;
        getBitmap(ind: number): GYScaleSprite;
        setData(val: ILabelData): void;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        get text(): string;
        set text(value: string);
        set align(val: string);
        get align(): string;
        set clipBitmap(val: egret.Texture);
        get clipBitmap(): egret.Texture;
        get textWidth(): number;
        get textHeight(): number;
        setForamt(ind: number, t: TextFormat): void;
        updateView(): void;
        invalidText(): void;
        validText(): void;
    }
}
