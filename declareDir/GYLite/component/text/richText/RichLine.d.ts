declare module GYLite {
    /**富文本的行单元 @see GYRichText*/
    class RichLine {
        private _lineArr;
        constructor(pGroup: GYRichText, w: number);
        /**@param index 插入索引，-1为放到末尾*/
        Add(obj: Object, index?: number): void;
        Show(): void;
        /**清理回收*/
        clear(): void;
        ResetY(value?: number): void;
        set y(value: number);
        get width(): number;
        private clearHTML;
        private _lineVec;
        private _nowBottom;
        private _preBottom;
        private _tmpX;
        private _y;
        bottom: number;
        top: number;
        private _pGroup;
        private _width;
        private _tmpW;
    }
}
