module GYLite
{
											
						
		
	export class ButtonGroup extends GYGroup
	{
		private commonBtn:GYButton;
		private tween:GYTween;
		public constructor()
		{
			super();
			var s = this;
			s.init();
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private addToStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
		}
		private removeFromStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private init():void
		{var s = this;
			/*button*/
			var vec:egret.Texture[] = new Array<egret.Texture>();
			vec.push(GYLoader.getRes("iconBtn_up","Img/ui.png").refRes(s),GYLoader.getRes("iconBtn_over","Img/ui.png").refRes(s),GYLoader.getRes("iconBtn_down","Img/ui.png").refRes(s));
			s.commonBtn=new GYButton(new IconButtonSkin(vec));
			s.commonBtn.label="点我啊笨";
			s.commonBtn.x = 20;
			s.commonBtn.y = 120;
			// s.commonBtn.labelDisplay.size = 14;
			// s.commonBtn.labelDisplay.glow = true;
			s.commonBtn.labelDisplay.paddingLeft = 20;
			console.log("paddingLeft", s.commonBtn.labelDisplay.paddingLeft);
			console.log("不加粗4，commonBtn.x",s.commonBtn.labelDisplay.x);
			s.commonBtn.labelDisplay.bold = true;
			s.commonBtn.labelDisplay.verticalCenter = -2;
			// s.commonBtn.buttonModeForMouse = "url('https://wx.wit-learn.com/s/platform/interactive/common/interactiveTemplate/mainScratch/mouse.png') 5 5, pointer";
			s.commonBtn.setOnClick(function(e:egret.TouchEvent):void{
				s.tween.run();					
			},s);
			s.tween = GYTween.to(s.commonBtn,[
				TweenData.getInstance("x",s.commonBtn.x + 300,NaN,GYTween.reduceEase),
			],1000,0,s,s.tweenComp,s.tweenStart,s.tweenUpdate,false,false);//创建缓动
			s.commonBtn.addEventListener(GYTouchEvent.LONG_MOUSEDOWN, s.longMouseDown,s);
			s.addElement(s.commonBtn);
			Main.instance.setTip(s.commonBtn, "点它可是会跑到，长按还会喊你笨蛋");
			/*scalebutton*/
			var scaleBtn:GYButton=new GYButton;
			scaleBtn.toggle=true;
			scaleBtn.x = 20;
			scaleBtn.y = s.commonBtn.y + s.commonBtn.height + 30;
			scaleBtn.width=200;
			scaleBtn.height=200;
			scaleBtn.label="拉伸按钮";
			s.addElement(scaleBtn);
			Main.instance.setTip(scaleBtn, "大家都知道的九宫格拉伸");
			
			/*linkButton*/
			var linkBtn:GYLinkButton=new GYLinkButton;
			linkBtn.label="链接按钮";
			linkBtn.x=20;
			linkBtn.y = scaleBtn.y + scaleBtn.height + 30;
			linkBtn.width = 100;
			linkBtn.toggle=true;
			linkBtn.setOnClick(s.clkLink, s);
			// linkBtn.buttonMode=true;
			s.addElement(linkBtn);
		}
		
		/*s.clkLink*/
		private clkLink(e:egret.TouchEvent):void
		{var s = this;
			var linkT:GYLinkButton = (e.currentTarget as GYLinkButton);
			if(linkT.selected)
				linkT.label = "选中薄荷姐姐";
			else
				linkT.label = "薄荷姐姐";
		}
		private tweenComp():void
		{var s = this;
			// egret.log("complete");
			s.commonBtn.enabled = true;
			if(!s.tween.isReserve)
				s.tween.run(true);
		}
		private tweenStart():void
		{var s = this;
			// egret.log("start");
			s.commonBtn.enabled = false;
		}
		private tweenUpdate():void
		{var s = this;
//			s.commonBtn.y = 80 + s.commonBtn.x;
		}
		
		private longMouseDown(e:egret.TouchEvent):void
		{var s = this;
			if(s.commonBtn.label == "笨蛋")
				s.commonBtn.label = "还是笨蛋";
			else
				s.commonBtn.label = "笨蛋";
		}
	}
}