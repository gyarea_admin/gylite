declare module GYLite {
    class GYLoadImage extends GYImage {
        /**默认加载失败图片*/ static defaultErrorData: egret.Texture;
        protected _url: string;
        protected _resObj: ResObject;
        protected _isLoading: boolean;
        errorData: egret.Texture;
        /**是否切换时立即清理*/ clearImmedia: boolean;
        constructor();
        protected baseSource(bmp: any): void;
        get source(): any;
        set source(bmp: any);
        private release;
        get url(): string;
        protected setBitmapdata(loadInfo: LoadInfo): void;
    }
}
