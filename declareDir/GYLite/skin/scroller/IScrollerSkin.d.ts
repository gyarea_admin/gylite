declare module GYLite {
    interface IScrollerSkin extends IGYSkin {
        arrowBtn1: GYButton;
        arrowBtn2: GYButton;
        scrollBar: GYButton;
        sliderBg: GYScaleSprite;
    }
}
