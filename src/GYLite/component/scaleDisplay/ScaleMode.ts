module GYLite
{
	export class ScaleMode
	{
		/**缩放*/public static SCALE:number = 0;
		/**裁切*/public static CLIP:number = 1;
		/**重复*/public static REPEAT:number = 2;
		/**自由*/public static FREE:number = 3;
	}
}