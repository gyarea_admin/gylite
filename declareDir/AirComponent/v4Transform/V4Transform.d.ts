declare module GYLite {
    class V4Transform {
        $simpleRate: number;
        v0p: TransformPoint;
        v1p: TransformPoint;
        v2p: TransformPoint;
        v3p: TransformPoint;
        constructor(simpleRate: number, x: number, y: number, w: number, h: number);
    }
    class TransformPoint {
        x: number;
        y: number;
        constructor(x: number, y: number);
    }
}
