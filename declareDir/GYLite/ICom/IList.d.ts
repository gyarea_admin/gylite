declare module GYLite {
    interface IList extends IGYDisplay {
        /**允许选择多少项(默认0，不能选择)*/ canSelectNum: number;
        /**当前选择的项*/ selectedItem: IItemRender;
        /**不参与列表选择的对象*/ stopSelTarget: any;
        /**选中项的数据列表(当canSelectNum大于1时，将会提供此选择列表)*/
        selectList: any[];
        /**当前选择的数据，选择的数据不一定与选择的项相对应，由于内部使用的是渲染项循环滚动以节省资源，所以滚动之后选择的项的数据不一定就是选择的数据*/
        selectedData: any;
        /**列表是否可选*/ selectable: boolean;
    }
}
