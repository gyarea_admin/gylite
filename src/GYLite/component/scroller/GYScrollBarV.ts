﻿/**
@author 迷途小羔羊
2012.12.30
*/
module GYLite
{							
	export class GYScrollBarV extends GYScrollBase
	{

		public constructor(s:any=null)
		{
			super(s);
		}
		/**@inheritDoc*/
		public setBar():void
		{
			let s = this;
			var temp:number;
			if(s._hideBtn)
			{
				s._stOffset=temp=0;
				s._stOffset=0;
			}
			else
			{
				s._stOffset=s._skin.arrowBtn1.height;
				temp=s._stOffset * 2;
			}
			if (s.height < temp)
				s._scrollRange=0;
			else
				s._scrollRange=s.height-temp;
			
			s._sliderH=(s._barPercent * s._scrollRange >> 0);
			if(s._scrollRange < s._sliderMinSize)
				s._sliderH = 0;
			else if(s._sliderH < s._sliderMinSize)
				s._sliderH = s._sliderMinSize;
			if (s._scrollRange <= s._sliderH)
				s._sliderH = 0;
			s._sliderW = s.width;
			
			s._max=s.height - s._stOffset - s._sliderH;
			s._min=s._stOffset;
			s._scrollMax=s._max-s._min;
			
			//s.skin-----------------
			s._skin.arrowBtn2.visible=s._skin.arrowBtn1.visible=!s._hideBtn;
			s._skin.arrowBtn2.y = s.height - s._skin.arrowBtn2.height;
//			s._skin.arrowBtn1.y = 0;
			s._skin.arrowBtn2.width = s._skin.arrowBtn1.width = s.width;
			s._skin.sliderBg.width = s.width;
			s._skin.scrollBar.width = s.width;
			s._skin.scrollBar.height = s._sliderH;
			//滑块
			//warning！！！设置按钮高度
			if(s._sliderH >= s._scrollRange || s._sliderH <= 0)
				s._skin.scrollBar.visible =false;
			else
				s._skin.scrollBar.visible = true;
			//滚动部分
			if (s._scrollRange!=0)
			{
				s._skin.sliderBg.y = s._stOffset;
				s._skin.sliderBg.height = s._scrollRange;
				s._skin.scrollBar.y = s._stOffset + s._per * s._scrollMax;
			}
			s.setSlider(s.resetMode == 0?s._per*s._maximum:s.position);
		}

		//拖动滚动条
		private scrollDown(e:egret.TouchEvent)
		{
			let s = this;
			e.stopPropagation();
			s._oldMousePos = s.mouseY - s._skin.scrollBar.y;
			GYSprite.addStageDown(e.target as GYSprite,s.outsideUp, s);
			s._clkCount=0;
			CommonUtil.addStageLoop(s.enterFrame,s);
			s._clkFunc=s.moveBar;
		}
		//箭头部分 点击
		private arrowDown1(e:egret.TouchEvent):void
		{
			let s = this;
			e.stopPropagation();
			s._clkCount=5;
			GYSprite.addStageDown(e.target as GYSprite,s.outsideUp, s);
			CommonUtil.addStageLoop(s.enterFrame,s);
			s._nowStep=s._scrollStep*s._step;
			s._clkFunc=s.clkArrowUp;
			s._clkFunc();
		}
		private arrowDown2(e:egret.TouchEvent):void
		{
			let s = this;
			e.stopPropagation();
			s._clkCount=5;
			GYSprite.addStageDown(e.target as GYSprite,s.outsideUp, s);
			CommonUtil.addStageLoop(s.enterFrame,s);
			s._nowStep=s._scrollStep*s._step;
			s._clkFunc=s.clkArrowDown;
			s._clkFunc();
		}
		/**向上滚动
		 * @param step 步长(像素)
		 * */
		public scrollToTop(step:number):void
		{
			let s = this;
			s._nowStep=step;
			s.clkArrowUp();
		}
		/**向下滚动
		 * @param step 步长(像素)
		 * */
		public scrollToBottom(step:number):void
		{
			let s = this;
			s._nowStep=step;
			s.clkArrowDown();
		}
		//鼠标经过轨道
		private rollOverBarBack(e:egret.TouchEvent):void
		{
			let s = this;
			if(GYSprite.isStageDown(e.currentTarget as GYSprite))
			{
				s.downBar(e);
			}
		}
		//滑块部分 点击
		private downBar(e:egret.TouchEvent):void
		{
			let s = this;
			e.stopPropagation();
			if(s._sliderH == 0 || s._sliderW == 0)
				return;
			GYSprite.addStageDown(e.currentTarget as GYSprite);
			s._clkCount=5;
			CommonUtil.addStageLoop(s.enterFrame,s);
			s._nowStep=s._scrollStep*s._step;
			if(s.mouseY <= s._skin.scrollBar.y)
				s._clkFunc=s.clkBackUp;
			else
				s._clkFunc=s.clkBackDown;
			s._clkFunc.call(s);
		}
		private arrowOut(e:egret.TouchEvent):void
		{
			let s = this;
			if(e!=null && !GYSprite.isStageDown(e.currentTarget as GYSprite))
				return;
			if(s._clkFunc!=null)
			{
				CommonUtil.delStageLoop(s.enterFrame,s);
				s._clkFunc=null;
			}
		}
		private enterFrame(t:number):void
		{
			let s = this;
			if(s._clkCount > 0)
			{
				--s._clkCount;
				return;
			}
			s._clkFunc();
		}
		//点击上箭头
		private clkArrowUp():void
		{
			let s = this;
			s.setSlider(s._value - s._nowStep, true);			
		}
		//点击下箭头
		private clkArrowDown():void
		{
			let s = this;
			s.setSlider(s._value + s._nowStep, true);
		}
		//点击背景上方
		private clkBackUp():void
		{
			let s = this;
			GYSprite.addStageDown(s._skin.sliderBg);
			if(s.mouseY <= s._skin.scrollBar.y)
				s.setSlider(s._value - s._nowStep);
			else
				s.arrowOut(null);
		}
		//点击背景下方
		private clkBackDown():void
		{
			let s = this;
			if(s.mouseY - s._skin.scrollBar.y >= s._sliderH)
				s.setSlider(s._value + s._nowStep);
			else
				s.arrowOut(null);
		}
		//移动滑块
		private moveBar():void
		{
			let s = this;
			var val:number;
			val = s._oldMousePos + s._stOffset;
			if(val == s.mouseY)return;
			val = (s.mouseY - val + 1) / s._scrollMax * s._maximum;
			s.setSlider(val-val%s._scrollStep);
		}
		
		protected setSlider(val:number, min_max_event:boolean=false):number
		{
			let s = this;
			let flag = super.setSlider(val);
			if(s._maximum == 0)
				s._per = 0;
			else
				s._per=s._value/s._maximum;
			s._skin.scrollBar.y=(s._stOffset + s._scrollMax * s._per >> 0);						
			s.dispatchEvent(s._scrollEvent);
			if(min_max_event && flag != 0)
			{
				let event:string = flag==1?GYViewEvent.SCROLL_VERTICAL_MIN:GYViewEvent.SCROLL_VERTICAL_MAX;
				if(s.hasEventListener(event))
					s.dispatchEventWith(event);
			}
			return flag;
		}
		private outsideUp(e:egret.TouchEvent=null)
		{
			let s = this;
			s.arrowOut(null);
		}		
				
		public set width(val:number)
		{
			let s = this;
			if(s.set_width(val))
				s.invalidBarView();			
		}
		public get width():number
		{
			return this.get_width();			
		}
		public set height(val:number)
		{
			let s= this;
			if(s.set_height(val))
				s.invalidBarView();			
		}
		public get height():number
		{
			return this.get_height();			
		}
		/**滑块高度*/
		public get sliderH():number
		{
			let s = this;
			return s._sliderH;
		}
		public set sliderW(val:number)
		{
			let s = this;
			var t:number;
			if(val <= 0)
				t = 0;
			else if(val < 5)
				t = 5;
			else
				t = val;
			if(t == s._sliderW)
				return;
			t = s._sliderW;
			s.invalidBarView();
		}
		/**滑块宽度*/
		public get sliderW():number
		{
			let s = this;
			return s._sliderW;
		}
		/**百分比*/
		public get per():number
		{
			let s = this;
			return s._per;
		}
		public set maximum(val:number)
		{
			let s = this;
			var t:number = val<0?0:val;
			if(s._maximum == t)
				return;
			s._maximum = t;
			s.invalidBarView();
		}
		/**滚动最大值(像素)*/
		public get maximum():number
		{
			let s = this;
			return s._maximum;
		}
		public set barPercent(val:number)
		{
			let s = this;
			if(s._barPercent == val)
				return;
			s._barPercent=val;
			s.invalidBarView();
		}
		/**滑块高度相对于总长的百分比*/
		public get barPercent():number
		{
			let s = this;
			return s._barPercent;
		}
		public set hideBtn(val:boolean)
		{
			let s = this;
			if(s._hideBtn == val)
				return;
			s._hideBtn=val;
			s.invalidBarView();
		}
		
		/**@inheritDoc */
		public set skin(val:any)
		{
			let s = this;
			egret.superSetter(GYScrollBarV, s, "skin", val);			
			s.invalidBarView();
		}
		protected skinChange(oldSkin:any, newSkin:any):void
		{
			let s = this;
			//移除旧皮肤监听
			if(oldSkin)
			{
				oldSkin.arrowBtn1.removeEventListener(egret.TouchEvent.TOUCH_BEGIN,s.arrowDown1,s);
				oldSkin.arrowBtn1.removeEventListener(egret.TouchEvent.TOUCH_END,s.arrowOut,s);
				oldSkin.arrowBtn1.removeEventListener(MouseEvent.ROLL_OUT, s.arrowOut,s);
				oldSkin.arrowBtn2.removeEventListener(egret.TouchEvent.TOUCH_BEGIN,s.arrowDown2,s);
				oldSkin.arrowBtn2.removeEventListener(egret.TouchEvent.TOUCH_END,s.arrowOut,s);
				oldSkin.arrowBtn2.removeEventListener(MouseEvent.ROLL_OUT, s.arrowOut,s);
				oldSkin.scrollBar.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, s.scrollDown,s);
				oldSkin.scrollBar.removeEventListener(egret.TouchEvent.TOUCH_END, s.arrowOut,s);
				oldSkin.sliderBg.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, s.downBar,s);
				oldSkin.sliderBg.removeEventListener(MouseEvent.ROLL_OVER, s.rollOverBarBack,s);
				oldSkin.sliderBg.removeEventListener(MouseEvent.ROLL_OUT, s.arrowOut,s);
				oldSkin.sliderBg.removeEventListener(egret.TouchEvent.TOUCH_END, s.arrowOut,s);
			}
			
			//添加皮肤监听
			newSkin.arrowBtn1.addEventListener(egret.TouchEvent.TOUCH_BEGIN,s.arrowDown1,s);
			newSkin.arrowBtn1.addEventListener(egret.TouchEvent.TOUCH_END,s.arrowOut,s);
			newSkin.arrowBtn1.addEventListener(MouseEvent.ROLL_OUT, s.arrowOut,s);
			newSkin.arrowBtn2.addEventListener(egret.TouchEvent.TOUCH_BEGIN,s.arrowDown2,s);
			newSkin.arrowBtn2.addEventListener(egret.TouchEvent.TOUCH_END,s.arrowOut,s);
			newSkin.arrowBtn2.addEventListener(MouseEvent.ROLL_OUT, s.arrowOut,s);
			newSkin.scrollBar.addEventListener(egret.TouchEvent.TOUCH_BEGIN, s.scrollDown,s);
			newSkin.scrollBar.addEventListener(egret.TouchEvent.TOUCH_END, s.arrowOut,s);
			newSkin.sliderBg.addEventListener(egret.TouchEvent.TOUCH_BEGIN, s.downBar,s);
			newSkin.sliderBg.addEventListener(MouseEvent.ROLL_OVER, s.rollOverBarBack,s);
			newSkin.sliderBg.addEventListener(MouseEvent.ROLL_OUT, s.arrowOut,s);
			newSkin.sliderBg.addEventListener(egret.TouchEvent.TOUCH_END, s.arrowOut,s);
		}
		/**获取皮肤主题，自定义皮肤请实现IScrollerSkin*/
		protected getThemeSkin():IGYSkin
		{
			let s = this;
			return GYSprite.skinTheme.GetScrollBarSkinV();
		}
	}
}