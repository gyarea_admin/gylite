declare namespace egret.web {
    /**
     * @private
     * @inheritDoc
     */
    class HtmlSoundChannel extends egret.EventDispatcher implements egret.SoundChannel {
        /**
         * @private
         */
        $url: string;
        /**
         * @private
         */
        $loops: number;
        /**
         * @private
         */
        $startTime: number;
        /**
         * @private
         */
        private audio;
        private isStopped;
        /**
         * @private
         */
        constructor(audio: HTMLAudioElement);
        private canPlay;
        $play(): void;
        /**
         * @private
         */
        private onPlayEnd;
        /**
         * @private
         * @inheritDoc
         */
        stop(): void;
        /**
         * @private
         */
        private _volume;
        /**
         * @private
         * @inheritDoc
         */
        get volume(): number;
        /**
         * @inheritDoc
         */
        set volume(value: number);
        /**
         * @private
         * @inheritDoc
         */
        get position(): number;
    }
}
