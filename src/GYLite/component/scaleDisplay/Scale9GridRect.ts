module GYLite
{
	/**九切片、三切片*/
	export class Scale9GridRect
	{
		/**左边距*/public leftGap:number;
		/**右边距*/public rightGap:number;
		/**上边距*/public topGap:number;
		/**底边距*/public bottomGap:number;
		public gridMode:ScaleGridMode;
		/**
		 * @param l 左
		 * @param r 右
		 * @param t 上
		 * @param b 下 
		 * */
		public constructor(l:number=7,r:number=7,t:number=7,b:number=7,scale3Grid:ScaleGridMode=null)
		{
			let s = this;
			s.leftGap=l;
			s.rightGap=r;
			s.topGap=t;
			s.bottomGap=b;
			if(t == 0 && b == 0)
				s.gridMode = ScaleGridMode.h3Grid;
			else if(l == 0 && r == 0)
				s.gridMode = ScaleGridMode.v3Grid;
			else
				s.gridMode = scale3Grid?scale3Grid:ScaleGridMode.s9Grid;
		}
	}
	export class ScaleGridMode{
		/**垂直三切片*/public static v3Grid:ScaleGridMode = new ScaleGridMode;
		/**水平三切片*/public static h3Grid:ScaleGridMode = new ScaleGridMode;
		/**九切片*/public static s9Grid:ScaleGridMode = new ScaleGridMode;
	}
}