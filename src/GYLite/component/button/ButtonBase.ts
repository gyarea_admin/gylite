/**
 @author 迷途小羔羊
 2014.12.5
 */
module GYLite
{
								
	
	export class ButtonBase extends GYSkinContainer
	{
		
		public constructor(skin:any=null)
		{
			super();
			var s = this;
			s._toggle = false;
			s._selected = false;
			s._enabled = true;
			s._state = 0;
			s._clickFunc = null;
			s._clickThisObject = null;
			s._stsChange = false;
			s.skin=skin;
			s.initComponent();
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();
			s.touchChildren=false;
		}
		public set label(val:string)
		{
			let s = this;
			s._skin.label = val;
		}
		/**当s.label被赋值的时候自动产生Mytext文本(在此之前不存在s.labelDisplay)，在s.label再赋值为null时，文本不会被清除，暂时从显示列表移除*/
		public get label():string
		{
			let s = this;
			return s._skin.label;
		}
		/**获取文本对象*/
		public get labelDisplay():GYText
		{
			let s = this;
			return s._skin.labelDisplay
		}
		public set width(val:number)
		{
			let s = this;
			egret.superSetter(ButtonBase, s, "width", val);
			// s._width=val;
			s._skin.width=val;
		}
		public set height(val:number)
		{
			let s = this;
			egret.superSetter(ButtonBase, s, "height", val);
			// s._height=val;
			s._skin.height=val;
		}
		public get width():number
		{
			let s = this;
			return egret.superGetter(ButtonBase, s, "width");
		}
		public get height():number
		{
			let s = this;
			return egret.superGetter(ButtonBase, s, "height");
		}
		
		/**按钮状态改变
		 * 状态请参考ButtonBase内部常量定义
		 * */
		public stateChange():void
		{
			let s = this;
			s._skin && s._skin.drawSkin(s._state);
		}
		/**s.skin皮肤Class类型，自定义皮肤请实现IButtonSkin接口，传入实例将自动clone副本(不包括s.labelDisplay) */
		public set skin(val:any)
		{
			let s = this;
			egret.superSetter(ButtonBase, s, "skin", val);			
			s.invalidState();
		}
		public get skin():any
		{
			let s = this;
			return s._skin;
		}
		/**获取主题皮肤，自定义皮肤请实现IButtonSkin接口*/
		protected getThemeSkin():IGYSkin
		{
			let s = this;
			return GYSprite.skinTheme.GetButtonSkin();
		}
		public updateView():void
		{
			let s = this;
			super.updateView();
			if(s._stsChange)
			{
				s.stateChange();
				s._stsChange = false;
			}
		}
		/**刷新状态*/
		public invalidState():void
		{
			let s = this;
			if(s._stsChange)
				return;
			s._stsChange = true;
			s.displayChg();
		}
		protected _toggle:boolean;
		protected _selected:boolean;
		protected _enabled:boolean;
		protected _state:number;
		protected _clickFunc:Function;
		protected _clickThisObject:any;
		protected _stsChange:boolean;
		
		/**抬起*/public static STATE_UP:number=0;
		/**经过*/public static STATE_OVER:number=1;
		/**按下*/public static STATE_DOWN:number=2;
		/**不可用*/public static STATE_DISABLE:number=3;
		/**选中时经过*/public static STATE_SELECTOVER:number=5;
		/**选中时按下*/public static STATE_SELECTDOWN:number=6;
		/**选中时*/public static STATE_SELECT:number=4;
		/**选中时不可用*/public static STATE_SELECTDISABLE:number=7;
		
	}
}