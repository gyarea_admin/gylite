declare module GYLite {
    class TitleWindowSkin implements IWindowSkin {
        protected _backgroundImage: GYScaleSprite;
        protected _icon: GYImage;
        protected _title: GYText;
        protected _closeBtn: GYButton;
        protected _hostComponent: GYSprite;
        constructor(background?: egret.Texture, scale9GridRect?: Scale9GridRect, closeSkin?: IButtonSkin);
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        setTitle(val: string): void;
        get title(): GYText;
        setIcon(val: egret.Texture): void;
        get icon(): GYImage;
        clone(): IGYSkin;
        copy(val: IGYSkin): void;
        release(): void;
        set icon(value: GYImage);
        set title(value: GYText);
        get closeBtn(): GYButton;
        get backgoundImage(): GYScaleSprite;
    }
}
