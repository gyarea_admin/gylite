declare module GYLite {
    class Quaternion {
        static DEFAULT: Quaternion;
        static TEMPVector30: Vector3;
        static TEMPVector31: Vector3;
        static TEMPVector32: Vector3;
        static TEMPVector33: Vector3;
        static TEMPMatrix0: Matrix4x4;
        static TEMPMatrix1: Matrix4x4;
        static _tempMatrix3x3: Matrix3x3;
        static NAN: Quaternion;
        elements: Float32Array;
        constructor(x?: number, y?: number, z?: number, w?: number);
        /**
         *根据缩放值缩放四元数
        *@param scale 缩放值
        *@param out 输出四元数
        */
        scaling(scaling: number, out: Quaternion): void;
        /**
         *归一化四元数
        *@param out 输出四元数
        */
        normalize(out: Quaternion): void;
        /**
         *计算四元数的长度
        *@return 长度
        */
        length(): number;
        /**
         *根据绕X轴的角度旋转四元数
        *@param rad 角度
        *@param out 输出四元数
        */
        rotateX(rad: number, out: Quaternion): void;
        /**
         *根据绕Y轴的制定角度旋转四元数
        *@param rad 角度
        *@param out 输出四元数
        */
        rotateY(rad: any, out: any): void;
        /**
         *根据绕Z轴的制定角度旋转四元数
        *@param rad 角度
        *@param out 输出四元数
        */
        rotateZ(rad: number, out: Quaternion): void;
        /**
         *分解四元数到欧拉角（顺序为Yaw、Pitch、Roll），参考自http://xboxforums.create.msdn.com/forums/p/4574/23988.aspx#23988,问题绕X轴翻转超过±90度时有，会产生瞬间反转
        *@param quaternion 源四元数
        *@param out 欧拉角值
        */
        getYawPitchRoll(out: Vector3): void;
        /**
         *求四元数的逆
        *@param out 输出四元数
        */
        invert(out: Quaternion): void;
        /**
         *设置四元数为单位算数
        *@param out 输出四元数
        */
        identity(): void;
        /**
         *从Array数组拷贝值。
        *@param array 数组。
        *@param offset 数组偏移。
        */
        fromArray(array: number[], offset?: number): void;
        /**
         *克隆。
        *@param destObject 克隆源。
        */
        cloneTo(destObject: Quaternion): Quaternion;
        /**
         *克隆。
        *@return 克隆副本。
        */
        clone(): Quaternion;
        equals(b: Quaternion): boolean;
        /**
         *计算长度的平方。
        *@return 长度的平方。
        */
        lengthSquared(): number;
        /**
         *获取四元数的x值
        */
        get x(): number;
        /**
         *获取四元数的y值
        */
        get y(): number;
        /**
         *获取四元数的z值
        */
        get z(): number;
        /**
         *获取四元数的w值
        */
        get w(): number;
        static createFromYawPitchRoll(yaw: number, pitch: number, roll: number, out: Quaternion): void;
        static multiply(left: Quaternion, right: Quaternion, out: Quaternion): void;
        static arcTanAngle(x: number, y: number): number;
        static angleTo(from: Vector3, location: Vector3, angle: Vector3): void;
        static createFromAxisAngle(axis: Vector3, rad: number, out: Quaternion): void;
        static createFromMatrix3x3(sou: Matrix3x3, out: Quaternion): void;
        static createFromMatrix4x4(mat: Matrix4x4, out: Quaternion): void;
        static slerp(left: Quaternion, right: Quaternion, t: number, out: Quaternion): Float32Array;
        static lerp(left: Quaternion, right: Quaternion, t: number, out: Quaternion): void;
        static add(left: Quaternion, right: Quaternion, out: Quaternion): void;
        static dot(left: Vector3, right: Vector3): number;
        static rotationLookAt(forward: Vector3, up: Vector3, out: Quaternion): void;
        static lookAt(eye: Vector3, target: Vector3, up: Vector3, out: Quaternion): void;
        static invert(value: Quaternion, out: Quaternion): void;
        static rotationMatrix(matrix3x3: Matrix3x3, out: Quaternion): void;
    }
}
