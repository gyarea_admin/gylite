declare module GYLite {
    class GraphicsElement extends BatchElement {
        /**实际原图开始颜色*/ startColor: number;
        /**实际原图结束颜色*/ endColor: number;
        /**颜色数组，大于1个颜色则为渐变*/ $color: number[];
        /**透明度数组*/ $alpha: number[];
        /**渐变比例*/ $ratio: number[];
        /**端点宽度*/ $capWidth: number;
        /**间隙*/ $gap: number;
        /**2倍间隙*/ $gap2: number;
        /**X轴半径*/ $radiusX: number;
        /**Y轴半径*/ $radiusY: number;
        /**原点X*/ $originX: number;
        /**原点Y*/ $originY: number;
        /**中间拉伸部分X的尺寸*/ $scaleSizeX: number;
        /**中间拉伸部分Y的尺寸*/ $scaleSizeY: number;
        /**圆角半径X*/ $rectRadiusX: number;
        /**圆角半径Y*/ $rectRadiusY: number;
        /**边缘模糊像素*/ $blurSize: number;
        /**线条9切片*/ $lineRect: GYLite.Scale9GridRect;
        /**细线条9切片*/ $lineTinyRect: GYLite.Scale9GridRect;
        /**边距*/ $padding: number;
        /**矢量类型*/ $type: GraphicsType;
        /**渐变类型*/ $gradientType: string;
        /**渐变矩阵*/ $gradientMatrix: egret.Matrix;
        /**矢量线段拐角类型 "bevel" | "miter" | "round"*/
        lineJoin: CanvasLineJoin;
        /**笔触宽度*/
        lineWidth: number;
        /**线段端点的类型 "butt" | "round" | "square"*/
        lineCap: CanvasLineCap;
        /**全局透明度*/ $globalAlpha: number;
        constructor();
        reset(param: any, type?: GraphicsType): void;
        get globalAlpha(): number;
        /**填充的颜色、渐变或模式*/
        getFillStyle(ctx: CanvasRenderingContext2D): string | CanvasGradient | CanvasPattern;
        /**笔触的颜色、渐变或模式*/
        getStrokeStyle(ctx: CanvasRenderingContext2D): string | CanvasGradient | CanvasPattern;
        get blurSizeY(): number;
        get blurSizeX(): number;
        /**设置圆角*/
        private setRoundRectRadius;
        getScaleRect(type?: GraphicsType, thickness?: number): GYLite.Scale9GridRect;
    }
}
