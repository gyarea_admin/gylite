/**
@author 迷途小羔羊
2022.09.27
*/
declare module GYLite {
    class GraphicsTexture extends BatchTexture {
        constructor();
        protected beforeToPool(): void;
        static batchParam: BatchDrawParam;
        private static _graphicsTextureMap;
        static getGraphicsDesc(param: any, type: GraphicsType): string;
        /**获取矢量绘制数据源*/
        static getGraphicsTexture(param: any, type: GraphicsType): GraphicsTexture;
    }
}
