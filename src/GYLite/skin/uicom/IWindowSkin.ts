module GYLite
{
						
		
	export interface IWindowSkin extends IGYSkin
	{		
		setIcon(val:egret.Texture):void;
		setTitle(val:string):void;
		// readonly backgoundImage:GYScaleSprite;
		// readonly closeBtn:GYButton;
		// readonly icon:GYImage;
		// readonly title:GYText;
		backgoundImage:GYScaleSprite;
		closeBtn:GYButton;
		icon:GYImage;
		title:GYText;
	}
}