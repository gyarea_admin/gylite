/***2025-02-18 迷途小羔羊
* 用于编写加载的资源解析
*/
declare module GYLite {
    class ResParser {
        private static fontLoadingDict;
        /**文件字节数据解析*/
        static bytesParse(l: SeqLoader | SeqURLLoader): GYLite.ResObject;
        /**资源文件解析(网页多媒体加载器加载的内容，如Image、Audio)*/
        static resParse(l: SeqLoader | SeqURLLoader): GYLite.ResObject;
        static createFontStyle(name: string, data: Blob): HTMLStyleElement;
    }
}
