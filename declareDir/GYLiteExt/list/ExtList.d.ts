declare module GYLite {
    class ExtList extends GYListV implements IKeyBoardObject {
        protected _dragData: any;
        protected _dropData: any;
        protected _dropItem: GYLite.IItemRender;
        protected _dragHandle: GYLite.GYSprite;
        protected _dataCanDrag: boolean;
        constructor(size: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        private addToStage;
        private removeFromStage;
        get dataCanDrag(): boolean;
        set dataCanDrag(val: boolean);
        protected createDragShape(touchId: number, item: GYLite.GYSprite): GYLite.GYSprite;
        protected removeDragShape(): void;
        protected getGrid(): GYLite.IItemRender;
        private itemTouchBegin;
        private rollOut;
        private itemtouchOutside;
        private itemTouchUp;
        private touchListMove;
        keyFocus(): boolean;
        kDown(keyCode: number): void;
        kUp(keyCode: number): void;
    }
    class ListEvent {
        /**列表项拖拽开始*/ static DRAG_START: string;
        /**列表项拖拽到外部，如果拖拽到非列表项上，会调用非列表项的receiveData方法以便传递数据*/ static DRAG_OUT: string;
        /**列表项拖拽进入其他列表项*/ static DRAG_ENTER: string;
        /**列表项放置到其他列表项上*/ static DROP: string;
    }
    class ListDragData {
        /**拖拽的数据，列表项的数据**/ dragData: any;
        /**放置目标的数据，通常是另外一个列表项的数据**/ dropData: any;
        /**相对放置目标的放置坐标Y**/ offsetX: number;
        /**相对放置目标的放置坐标Y**/ offsetY: number;
        constructor(dragData: any, dropData?: any, offsetX?: number, offsetY?: number);
    }
}
