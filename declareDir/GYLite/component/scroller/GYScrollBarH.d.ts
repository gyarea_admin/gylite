/**
@author 迷途小羔羊
2012.12.30
*/
declare module GYLite {
    class GYScrollBarH extends GYScrollBase {
        constructor(s?: any);
        /**@inheritDoc*/
        setBar(): void;
        private scrollDown;
        private arrowDown1;
        private arrowDown2;
        /**向左滚动
         * @param step 步长(像素)
         * */
        scrollToLeft(step: number): void;
        /**向右滚动
         * @param step 步长(像素)
         * */
        scrollToRight(step: number): void;
        private downBar;
        private arrowOut;
        private enterFrame;
        private clkArrowUp;
        private clkArrowDown;
        private clkBackUp;
        private clkBackDown;
        private moveBar;
        protected setSlider(val: number, min_max_event?: boolean): number;
        private OutsideUp;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        set sliderH(val: number);
        /**滑块高度*/
        get sliderH(): number;
        /**滑块宽度*/
        get sliderW(): number;
        /**滚动条位置百分比*/
        get per(): number;
        set maximum(val: number);
        /**滚动条滚动最大值(像素)*/
        get maximum(): number;
        set barPercent(val: number);
        /**滑块相对于总长的百分比*/
        get barPercent(): number;
        set hideBtn(val: boolean);
        /**@inheritDoc */
        set skin(val: any);
        protected skinChange(oldSkin: any, newSkin: any): void;
        /**获取皮肤主题，自定义皮肤请实现IScrollerSkin*/
        protected getThemeSkin(): IGYSkin;
    }
}
