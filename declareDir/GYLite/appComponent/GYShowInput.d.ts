declare module GYLite {
    /**<code>GYShowInput</code>是一个全局公用的输入文本，覆盖在顶层作为临时输入*/
    class GYShowInput extends GYText {
        private static _instance;
        /**显示输入文本
         * @param txt 设置文本的对象
         * @param pr 显示的层级
         * */
        static show(txt: GYText, pr: GYSprite): GYShowInput;
        private _txtSource;
        private _deactiveFunc;
        private _deactiveObj;
        /**@param mul 是否多行*/
        constructor(mul?: boolean);
        /**设置绑定的文本，输入结束后自动设置文本内容*/
        inputSet(txt: GYText): void;
        get inputText(): string;
        private deactive;
        listenFocusOut(func: Function, thisObj: any): void;
        layout(): void;
    }
}
