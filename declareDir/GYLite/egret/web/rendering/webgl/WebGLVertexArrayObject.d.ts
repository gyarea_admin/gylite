declare namespace egret.web {
    /**
     * @private
     * 顶点数组管理对象
     * 用来维护顶点数组
     */
    class WebGLVertexArrayObject {
        private readonly vertSize;
        private readonly vertByteSize;
        private readonly maxQuadsCount;
        private readonly maxVertexCount;
        private readonly maxIndicesCount;
        vertices: Float32Array;
        private indices;
        private indicesForMesh;
        private vertexIndex;
        private indexIndex;
        private hasMesh;
        private _vertices;
        private _verticesFloat32View;
        private _verticesUint32View;
        constructor();
        /**
         * 是否达到最大缓存数量
         */
        reachMaxSize(vertexCount?: number, indexCount?: number): boolean;
        /**
         * 获取缓存完成的顶点数组
         */
        getVertices(): Float32Array;
        /**
         * 获取缓存完成的索引数组
         */
        getIndices(): Uint16Array;
        /**
         * 获取缓存完成的mesh索引数组
         */
        getMeshIndices(): any;
        /**
         * 切换成mesh索引缓存方式
         */
        changeToMeshIndices(): void;
        isMesh(): boolean;
        /**
         * 默认构成矩形
         */
        /**
         * 缓存一组顶点
         */
        cacheArrays(buffer: WebGLRenderBuffer, sourceX: number, sourceY: number, sourceWidth: number, sourceHeight: number, destX: number, destY: number, destWidth: number, destHeight: number, textureSourceWidth: number, textureSourceHeight: number, meshUVs?: number[], meshVertices?: number[], meshIndices?: number[], rotated?: boolean, color?: number): void;
        clear(): void;
    }
    var isIOS14Device: () => boolean;
}
