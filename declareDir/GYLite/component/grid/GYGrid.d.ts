/**
 @author 迷途小羔羊
 2015.3.5
 */
declare module GYLite {
    /**行列宽高固定的Grid组件，行列数量随组件宽高自适应，若单纯纵向建议使用GYGridV，单纯横向建议使用GYGridH*/
    class GYGrid extends GYGridBase {
        protected _lastLineH: number;
        protected _lastEndLineH: number;
        protected _hScrollerPolicy: number;
        protected _horizonalMax: number;
        protected _contentGrp: GYGroup;
        /**行列宽高固定的Grid组件，行列数量随组件宽高自适应，若单列建议使用GYGridV，单行建议使用GYGridH
         * @param cols 无效
         * @param rows 无效
         * @param getGridFunc():IItemRender 返回自定义格子对象的方法
         * @param setGridFunc(IItemRender, any) 设置格子数据的方法*/
        constructor(cols: number, rows: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        protected initComponent(): void;
        set selectedIndex(val: number);
        /**选择row行col列指定项*/
        selectedPos(row: number, col: number): void;
        /**跳转到row行col列*/
        scrollToPos(row?: number, col?: number): void;
        /**跳转到row行col列(位置在末尾)*/
        scrollToEndPos(row?: number, col?: number): void;
        protected selectLine(data: any, selectedData: any, ctrlKey: boolean, checkNext?: boolean): void;
        /**@inheritDoc*/
        set dataProvider(val: Array<any>);
        get dataProvider(): Array<any>;
        protected vScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        protected hScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        /**@inheritDoc*/
        updateItems(): void;
        /**@inheritDoc*/
        updateGrid(): void;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        set canSelectNum(val: number);
        get canSelectNum(): number;
        set dragSelect(value: boolean);
        get dragSelect(): boolean;
        /**获取列表视图范围内可见的最大最小行列
         * @return [minRow, maxRow, minCol, maxCol]
         * */
        getRowColInView(): number[];
        protected wheelRoll(e: any): void;
        /**水平滚动条 0自动 1显示 2不显示*/
        get hScrollerPolicy(): number;
        set hScrollerPolicy(val: number);
        /**垂直滚动条 0自动 1显示 2不显示*/
        get vScrollerPolicy(): number;
        set vScrollerPolicy(val: number);
        protected downGroup(e: egret.TouchEvent): void;
        protected dragLoop(t: number): void;
        protected groupDrag(): void;
        protected groupDragStop(): void;
    }
}
