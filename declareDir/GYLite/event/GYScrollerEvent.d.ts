declare module GYLite {
    class GYScrollerEvent extends GYEvent {
        /**滚动条改变*/
        static SCROLL_CHANGE: string;
        /**滚动结束*/
        static SCROLL_TWEEN_END: string;
        constructor(type: string, bubbles?: boolean, cancelable?: boolean);
    }
}
