declare namespace egret {
    /** !!!!!!!!inspired by Babylon.js!!!!!!!!!!!!!
     * for description see https://www.khronos.org/opengles/sdk/tools/KTX/
     * for file layout see https://www.khronos.org/opengles/sdk/tools/KTX/file_format_spec/
     * Current families are astc, dxt, pvrtc, etc2, & etc1.
     * @returns The extension selected.
     */
    class KTXContainer {
        arrayBuffer: any;
        private static readonly HEADER_LEN;
        private static readonly COMPRESSED_2D;
        private static readonly COMPRESSED_3D;
        private static readonly TEX_2D;
        private static readonly TEX_3D;
        /**
         * Gets the openGL type
         */
        glType: number;
        /**
         * Gets the openGL type size
         */
        glTypeSize: number;
        /**
         * Gets the openGL format
         */
        glFormat: number;
        /**
         * Gets the openGL internal format
         */
        glInternalFormat: number;
        /**
         * Gets the base internal format
         */
        glBaseInternalFormat: number;
        /**
         * Gets image width in pixel
         */
        pixelWidth: number;
        /**
         * Gets image height in pixel
         */
        pixelHeight: number;
        /**
         * Gets image depth in pixels
         */
        pixelDepth: number;
        /**
         * Gets the number of array elements
         */
        numberOfArrayElements: number;
        /**
         * Gets the number of faces
         */
        numberOfFaces: number;
        /**
         * Gets the number of mipmap levels
         */
        numberOfMipmapLevels: number;
        /**
         * Gets the bytes of key value data
         */
        bytesOfKeyValueData: number;
        /**
         * Gets the load type
         */
        loadType: number;
        /**
         * If the container has been made invalid (eg. constructor failed to correctly load array buffer)
         */
        isInvalid: boolean;
        /**
         * Creates a new KhronosTextureContainer
         * @param arrayBuffer contents of the KTX container file
         * @param facesExpected should be either 1 or 6, based whether a cube texture or or
         * @param threeDExpected provision for indicating that data should be a 3D texture, not implemented
         * @param textureArrayExpected provision for indicating that data should be a texture array, not implemented
         */
        constructor(/** contents of the KTX container file */ arrayBuffer: any, facesExpected: number, threeDExpected?: boolean, textureArrayExpected?: boolean);
        /**
         * Uploads KTX content to a Babylon Texture.
         * It is assumed that the texture has already been created & is currently bound
         * @hidden
         */
        uploadLevels(bitmapData: egret.BitmapData, loadMipmaps: boolean): void;
        private _upload2DCompressedLevels;
    }
}
