declare module GYLite {
    /**组合输入菜单, 可以使用ISkinTheme的GetComboBox快速构建
     * <p>
     * combobox的s.menu.listV为其下拉列表，当选中列表项，自动调用s.menu.listV.selectedItem.toString()显示选中内容到输入框，请重写s.menu.listV.selectedItem 的toString()方法以显示选择内容
     * 请在创建BomboBox之后设置s.comboBoxBtn、s.menu和s.textInput三个属性，构建完整的comboBox
     * </p>
     * */
    class GYComboBox extends GYUIComponent {
        protected _textInput: GYTextInput;
        protected _menu: GYMenu;
        protected _comboBoxBtn: GYButton;
        protected _autoClose: boolean;
        private _clickShow;
        menuParent: GYSprite;
        private _menuX;
        private _menuY;
        constructor();
        get list(): GYLite.GYListV;
        set menu(val: GYMenu);
        /**comboBox的下拉菜单GYMenu 不能为null*/
        get menu(): GYMenu;
        protected selectItem(e: GYViewEvent): void;
        set selectedData(val: any);
        get selectedData(): any;
        set selectedIndex(val: number);
        get selectedIndex(): number;
        set textInput(val: GYTextInput);
        /**comboBox的输入框TextInput，不能为null*/
        get textInput(): GYTextInput;
        set comboBoxBtn(val: GYButton);
        /***设置菜单位置*/
        setMenuPos(x: number, y: number): void;
        protected themeChange(e: GYThemeEvent): void;
        /**comboBox的下拉按钮GYButton，不能为null*/
        get comboBoxBtn(): GYButton;
        protected clkBtn(e: egret.TouchEvent): void;
        /**显示菜单*/
        showMenu(): void;
        /**隐藏菜单*/
        hideMenu(): void;
        protected textChange(e: egret.Event): void;
        get autoClose(): boolean;
        set autoClose(value: boolean);
        protected outsideRelease(e: GYTouchEvent): void;
        get clickShow(): boolean;
        set clickShow(value: boolean);
    }
}
