module GYLite
{
								
	
	/**滑块条*/
	export class SliderSkin extends ProgressBarSkin implements ISliderSkin
	{
		protected _sliderBtn:GYButton;
		protected _sliderX:number;
		protected _sliderY:number;
		/**
		 * @param s.backgroundSkin 背景皮肤
		 * @param s.barSkin 进度条皮肤
		 * @param textSkin 文本皮肤 默认null，没有进度文本*/
		public constructor(backgroundSkin:GYScaleSprite, barSkin:GYScaleSprite, sliderBtnSkin:GYButton|IButtonSkin=null)
		{
			super(backgroundSkin, barSkin);
			var s = this;
			if(sliderBtnSkin)
			{
				s._sliderBtn = <GYButton>(CommonUtil.GYIs(sliderBtnSkin, GYButton)?sliderBtnSkin:new GYButton(sliderBtnSkin));
				s._sliderBtn.followTarget = s._sliderBtn;
				s._sliderBtn.isTipFollow = true;
				s._sliderBtn.toolTipOffsetX = -5;
				s._sliderBtn.toolTipOffsetY = -30;
			}
		}
		public release():void
		{var s = this;
			super.release();
			s._sliderBtn.skin.release();
		}
		public set hostComponent(val:GYSprite)
		{var s = this;
			if(val==s._hostComponent)
				return;
			if(s._hostComponent)
			{
				if(s._backgroundSkin.parent)
					s._hostComponent.removeElement(s._backgroundSkin);
				if(s._barSkin.parent)
					s._hostComponent.removeElement(s._barSkin);
				if(s._sliderBtn && s._sliderBtn.parent)
					s._hostComponent.removeElement(s._sliderBtn);
			}
			s._hostComponent=val;
			if(s._hostComponent)
			{
				s._hostComponent.addElementAt(s._backgroundSkin, 0);
				s._hostComponent.addElement(s._barSkin);
				
				if(s._sliderBtn)
					s._hostComponent.addElement(s._sliderBtn);
			}
		}
		public get hostComponent():GYSprite
		{var s = this;
			return s._hostComponent;
		}
		public set rotation(val:number)
		{var s = this;
			if(s._barSkin.rotation == val)
				return;
			s._sliderBtn.rotation = s._backgroundSkin.rotation = s._rotation = s._barSkin.rotation = val;
			s.barX = s._barX;
			s.barY = s._barY;
		}
		public get rotation():number
		{var s = this;
			return egret.superGetter(SliderSkin, s, "rotation");
		}
		
		public set sliderX(val:number)
		{var s = this;
			s._sliderX = val;
			s._sliderBtn.x = s._sliderX;
		}
		
		public set sliderY(val:number)
		{var s = this;
			s._sliderY = s._sliderBtn.y = val;	
		}
		public get sliderX():number
		{var s = this;
			return s._sliderX;
		}
		
		public get sliderY():number
		{var s = this;
			return s._sliderY;
		}
		public get sliderButton():GYButton
		{var s = this;
			return s._sliderBtn;
		}
		/**克隆皮肤*/
		public clone():IGYSkin
		{var s = this;
			var buttonSkin:IButtonSkin = s._sliderBtn ? s._sliderBtn.skin.clone() : null;
			var sliderSkin:ISliderSkin=new SliderSkin(new GYScaleSprite(s._backgroundSkin.bitmapData,s._backgroundSkin.scale9GridRect),new GYScaleSprite(s._barSkin.bitmapData,s._barSkin.scale9GridRect),buttonSkin);
			return sliderSkin;
		}
		/**此函数复制皮肤属性 如s.label gap等皮肤的属性*/
		public copy(skin:IGYSkin)
		{var s = this;
			if(skin == null)
				return;
			super.copy(skin);
			var sk:ISliderSkin = skin as ISliderSkin;
			s.sliderX = sk.sliderX;
			s.sliderY = sk.sliderY;
		}
	}
}