declare module GYLite {
    class FilterUtil {
        /**黑色描边滤镜*/ static blackGlow: egret.GlowFilter;
        static blackGlowArr: Array<any>;
        /**红色描边滤镜*/ static redGlow: egret.GlowFilter;
        static redGlowArr: Array<any>;
        /**灰度滤镜*/
        static grayColorMatrix: egret.ColorMatrixFilter;
        static grayColorArr: Array<any>;
    }
}
