module GYLite {
	export class Dictionary
	{
		public dict:any;
		public constructor() {
			let s= this;
			s.dict = {};
		}
		public getValue(key)
		{
			if(key == null)return null;
			var s = this;
			let code;
			code = s.getKeyCode(key);
			if(code!=null)
				return s.dict[code];
			return null;
		}
		public setValue(key, val)
		{
			if(key == null)return;
			var s = this;
			let code;
			code = s.getKeyCode(key);
			// if(key["hashCode"] == null)
			// 	key["hashCode"] = ++GYSprite.hashCode;
			s.dict[code] = val;
		}
		public deleteKey(key)
		{let s = this;
			let code;
			if(key == null)return;
			code = s.getKeyCode(key);
			if(code == null)
				return;
			delete s.dict[code];
		}
		private getKeyCode(key)
		{
			let code;
			if(GYLite.CommonUtil.GYIs(key,Number) || GYLite.CommonUtil.GYIs(key,String))
			{
				return key;
			}
			else if(key.prototype)
			{
				if(key.prototype.__class__)//如果是压缩代码，类名有可能是一样的，所以优先判断__class__
					code = key.prototype.__class__;
				else
					code = key.name==""?key.toString():key.name;
			}				
			else
			{
				if(key["hashCode"]==null)
					key["hashCode"] = ++GYSprite.hashCode;
				code = key["hashCode"];
			}
			return code;
		}
	}
}