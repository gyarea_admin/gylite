/**
 @author 迷途小羔羊
 2015.6.5
 */
declare module GYLite {
    /**单选按钮*/
    class GYRadioButton extends GYButton {
        private _radioGroup;
        constructor(skin?: any);
        /**获取主题皮肤，自定义皮肤请实现IRadioButtonSkin接口*/
        protected getThemeSkin(): IGYSkin;
        protected mClk(e: egret.TouchEvent): void;
        set gap(val: number);
        /**文本与复选框的间隙*/
        get gap(): number;
        set radioGroup(val: GYRadioGroup);
        /**单选按钮组，同组互斥*/
        get radioGroup(): GYRadioGroup;
        dispose(disposeChild?: boolean, removeChild?: boolean, forceDispose?: boolean): void;
    }
}
