module GYLite
{
						
			
	export class ExcelSelFrame extends GYSprite implements IDragger
	{
		public owner:ExcelFrame;
		public constructor()
		{
			super();
			var s = this;
			DragManager.setDrag(s);
		}
		
		protected _handle:DraggerHandle;
		/**@inheritDoc*/
		public getBitmapData():egret.Texture
		{var s = this;
			var b:GYDrawBitmapData = GYDrawBitmapData.getBitmapData(s.width,s.height);
			b.draw(this)
			return b;
		}
		/**@inheritDoc*/
		public isLockCenter():number
		{var s = this;
			return DragManager.LEFT_TOP_LCOK;
		}
		/**@inheritDoc*/
		public getHandle():DraggerHandle
		{var s = this;
			return s._handle;
		}
		/**@inheritDoc*/
		public setHandle(val:DraggerHandle):void
		{var s = this;
			s._handle = val;
		}
		/**@return excel的选框*/
		public getData():any
		{var s = this;
			return s.owner;
		}
		/**@inheritDoc*/
		public draggingSet(draggerShape:GYScaleSprite):boolean
		{var s = this;
			return false;
		}
		/**@inheritDoc*/
		public draggingDraw(draggerShape:GYScaleSprite):boolean
		{var s = this;
			DragManager.offsetX = -5;
			DragManager.offsetY = -5;
			return false;
		}
		/**@inheritDoc*/
		public dragStop():void
		{var s = this;
			DragManager.resetOffset();
		}
	}
}