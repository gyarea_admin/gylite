module GYLite
{
    export enum LoadType
    {
        /**图片*/TYPE_IMAGE = 0,
		/**纹理*/TYPE_TEX,
		/**图集*/TYPE_ATLAS,
		/**swf指定类名的资源(弃用)*/TYPE_SWF,
		/**swf资源库(弃用)*/TYPE_SWFS,
		/**字体*/TYPE_FONT,
		/**二进制*/TYPE_BINARY,
		/**文本*/TYPE_TEXT,
		/**xml(未启用)*/TYPE_XML,
		/**json*/TYPE_JSON,
		/**variables(弃用)*/TYPE_VARIABLES,
		/**swf片段(弃用)*/TYPE_MOVIE,
		/**js片段*/TYPE_JS,
		/**声音文件*/TYPE_SOUND,
		/**url资源*/TYPE_URL_RES,
		/**spine资源*/TYPE_SPINE,
    }
}