module GYLite
{
	export interface IGYContainer extends IGYDisplay
	{
		addElement(child:IGYDisplay):IGYDisplay;
		removeElement(child:IGYDisplay):IGYDisplay;
		addElementAt(child:IGYDisplay, index:number):IGYDisplay;
		removeElementAt(index:number):IGYDisplay;
	}
}