module GYLite
{
				
	
	export interface IButtonSkin extends IGYSkin
	{
		drawSkin(state:number):void;
		width:number;
		height:number;
		label:string;				
		labelDisplay:GYText;
//		s.copy(val:IButtonSkin):void;
		cloneLabel():GYText;
//		s.clone():IButtonSkin;
	}
}