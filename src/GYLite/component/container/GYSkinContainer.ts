module GYLite
{
				
	export class GYSkinContainer extends GYUIComponent implements IGYSkinContainer
	{
		protected _skin:any;
		public constructor()
		{
			super();
			this._skin = null;
		}
		protected initComponent():void
		{
			
		}
		public set skin(val:any)
		{
			let s = this;
			if(s.hasEventListener(GYThemeEvent.THEME_CHANGE))
				GYSprite.$stage.removeEventListener(GYThemeEvent.THEME_CHANGE,s.themeChange,GYSprite.$stage);
			if(s._skin && s._skin == val)
				return;
			if(s._skin)
			{
				var oldSkin:any = s._skin;
				s._skin.hostComponent = null;
			}
			if(val instanceof Function)
				s._skin = new val;
			else if(CommonUtil.GYIs(val, "GYLite.IGYSkinContainer"))
				s._skin = val.skin.clone();
			else if(val == null)
			{
				s._skin = s.getThemeSkin();
				s._skin.copy(oldSkin);
				GYSprite.$stage.addEventListener(GYThemeEvent.THEME_CHANGE,s.themeChange,GYSprite.$stage);
			}
			else
				s._skin = val;
			s._skin.hostComponent = this;
			s.skinChange(oldSkin, s._skin);
			if(oldSkin)oldSkin.release();
		}
		protected themeChange(e:GYThemeEvent):void
		{
			let s = this;
			s.skin = null;
		}

		public get skin():any
		{
			let s = this;
			return s._skin;
		}
		protected skinChange(oldSkin:any, newSkin:any):void
		{
			let s = this;
			
		}
		protected getThemeSkin():IGYSkin
		{
			let s = this;
			return new GYSkin;
		}
	}
}