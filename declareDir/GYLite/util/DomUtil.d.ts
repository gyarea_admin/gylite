/***2024-12-17 迷途小羔羊
* 用于编写网页dom操作的一些工具类
*/
declare module GYLite {
    class DomUtil {
        /**主动抛出一个鼠标事件
         * @param obj 抛出事件的dom对象
        */
        static dispatch_click(obj: HTMLElement): void;
        /**下载文件通过url数组 ,调用方法 download([save.txt],[下载路径]);
        */
        static downloadUrls(names: string[], urls: string[], type?: any): void;
        /**下载文件 ,调用方法 download("save.txt","内容");
         * @param name 下载的文件名
         * @param data ArrayBuffer 或者 Blob|Blob[] 或者 String
         * @param type mimeType {type:"image/png"}等
        */
        static download(name: string, data: any, type?: any): void;
        /**上传文件
         * @param callBack 选择文件之后的回调，返回参数blob，function(blob:Blob):void{},multi为true的情况，参数为数组[blob,blob……]
         * @param thisObj 回调this指向
         * @param accept 接受的文件类型 如png图片 "image/png",任意类型图片"image/*"，多个文件类型逗号隔开
         * @param param 附件回调的参数
         * @param multi 是否上传多个文件
        */
        static upload(callBack: Function, thisObj: any, accept?: string, param?: any, multi?: boolean): void;
    }
}
