module GYLite{
    export class GraphicsType{
        public static POINT:GraphicsType = new GraphicsType(0,"point");
        public static LINE:GraphicsType = new GraphicsType(1,"line");
        public static RECT:GraphicsType = new GraphicsType(2,"rect");
        public static CIRCLE:GraphicsType = new GraphicsType(3,"circle");
        public static ELLIPSE:GraphicsType = new GraphicsType(4,"ellipse");
        public static TRIANGLE:GraphicsType = new GraphicsType(5,"triangle");
        public static POLY:GraphicsType = new GraphicsType(6,"poly");
        public static ROUNDRECT:GraphicsType = new GraphicsType(7,"roundrect");
        public static CURVE:GraphicsType = new GraphicsType(8,"curve");
        public static CUBIC_CURVE:GraphicsType = new GraphicsType(9,"cubicCurve");
        public static ARC:GraphicsType = new GraphicsType(10,"arc");
        private _id:number;
        private _name:string;
        constructor(id:number,name:string)
        {
            this._id = id;
            this._name = name;
        }
        public get id():number
        {
            return this._id;
        }
        public get name():string
        {
            return this._name;
        }
    }
}