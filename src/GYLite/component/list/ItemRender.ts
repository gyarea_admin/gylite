module GYLite
{
						
	export class ItemRender extends GYUIComponent implements IItemRender
	{
		protected _selected:boolean;
		protected _owner:IList;
		protected _data:any;
		protected _labelDisplay:GYText;
		protected _itemIndex:number;
		protected _row:number;
		protected _col:number;
		public constructor()
		{
			super();
			var s = this;
			s.touchEnabled = true;
			s._selected=false;
			s._owner=null;
			s._data=null;
			s._labelDisplay=null;
			s._itemIndex=-1;
			s._row=0;
			s._col=0;
			s.width = 60;
			s.height = 25;
		}
		
		public setData(obj:any):void
		{
			let s = this;
			s._data = obj;
			if(s._data == null)
			{
				s.visible = false;
				return;
			}
			s.visible = true;
			if(s._labelDisplay==null)
			{
				s._labelDisplay = new GYText;
				s.addElement(s._labelDisplay);
			}				
			s._labelDisplay.text = s._data.toString();
		}
		
		public getData():any
		{
			let s = this;
			return s._data;
		}
		
		public set owner(val:IList)
		{
			let s = this;
			s._owner = val;
		}
		
		public get owner():IList
		{
			let s = this;
			return s._owner;
		}
		
		public set selected(val:boolean)
		{
			let s = this;
			s._selected = val;
		}
		
		public get selected():boolean
		{
			let s = this;
			return s._selected;
		}

		public get itemIndex():number
		{
			let s = this;
			return s._itemIndex;
		}

		public set itemIndex(value:number)
		{
			let s = this;
			s._itemIndex = value;
		}

		public get row():number
		{
			let s = this;
			return s._row;
		}

		public set row(value:number)
		{
			let s = this;
			s._row = value;
		}

		public get col():number
		{
			let s = this;
			return s._col;
		}

		public set col(value:number)
		{
			let s = this;
			s._col = value;
		}


	}
}