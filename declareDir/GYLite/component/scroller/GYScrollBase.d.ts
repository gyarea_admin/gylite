/**
 @author 迷途小羔羊
 * 2014-6-30
 */
declare module GYLite {
    class GYScrollBase extends GYSkinContainer {
        protected _sliderW: number;
        protected _sliderH: number;
        protected _scrollRange: number;
        protected _scrollMax: number;
        protected _max: number;
        protected _min: number;
        protected _per: number;
        protected _scrollStep: number;
        protected _step: number;
        protected _nowStep: number;
        protected _clkFunc: Function;
        protected _stOffset: number;
        protected _oldMousePos: number;
        protected _hideBtn: boolean;
        protected _barPercent: number;
        protected _value: number;
        protected _maximum: number;
        protected _sliderMinSize: number;
        protected _clkCount: number;
        protected _barChange: boolean;
        protected _tween: GYTween;
        protected _scrollSpeed: number;
        protected _scrollEvent: GYScrollerEvent;
        /**变更模式，决定当滚动条恢复时，保持的位置，默认是0，按百分比保持，1按坐标位置保持*/
        resetMode: number;
        /**限制滚动条最小滚动位置，默认NaN，不限制*/
        limitMin: number;
        /**限制滚动条最大滚动位置，默认NaN，不限制*/
        limitMax: number;
        /**滚动条改变调用的方法，返回Scroller的s.value*/
        constructor(skin?: any);
        protected initComponent(): void;
        updateView(): void;
        /**刷新滚动条*/
        invalidBarView(): void;
        setBar(): void;
        /**单次移动数量，默认1*/
        set step(val: number);
        get step(): number;
        /**滚动分度值，默认为5(像素)*/
        set scrollStep(val: number);
        get scrollStep(): number;
        set value(step: number);
        /**当前进度条位置值(位置/步长)，注意与s.position区分，请勿用此值判断是否超过maximum，应该使用s.position */
        get value(): number;
        /**当前进度条位置值(像素) */
        get position(): number;
        set position(val: number);
        /**设置滚动条位置
         * @param val 位置
         * @param min_max_event 达到尽头是否派发事件
        */
        setPosition(val: number, min_max_event?: boolean): void;
        scrollToValue(val: number, time?: number): void;
        scrollToPosition(val: number, time?: number): void;
        protected setSlider(val: number): number;
        protected scrollTweenEnd(): void;
        stopScroll(): void;
        /**滑块最小尺寸 */
        get sliderMinSize(): number;
        set sliderMinSize(val: number);
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        get tween(): GYTween;
        set scrollSpeed(val: number);
        /**拖拽滚动的时间*/
        get scrollSpeed(): number;
        /**滚动分度值(默认值)*/ static default_scrollStep: number;
        /**单次移动数量(默认值)*/ static default_step: number;
        /**隐藏按钮(默认值)*/ static default_hideBtn: boolean;
        /**当前位置(默认值)*/ static default_value: number;
        /**最大位置(默认值)*/ static default_maximum: number;
        /**滑块最小尺寸(默认值)*/ static default_sliderMinSize: number;
    }
}
