module GYLite
{
							
		
	/**富文本的图片单元 @see GYRichText*/
	export class RichSeqImage extends GYSeqImage implements IPoolObject
	{
		public owner:GYRichText;
		public constructor()
		{
			super();
		}
		public getInit():void
		{
			let s = this;
			s.owner.addEventListener(GYViewEvent.CLIP_SCROLL, s.clipScroll,s);
			s.clipScroll();
		}
		$onAddToStage(stage:egret.Stage, nestLevel:number):void
		{
			super.$onAddToStage(stage, nestLevel);
			let s = this;
			s.clipScroll();
		}
		$onRemoveFromStage():void
		{
			super.$onRemoveFromStage();
			let s = this;
			s.stop(s._seqIndex);			
		}
		protected clipScroll(e:GYViewEvent=null):void
		{
			let s = this;
			if(s.y + s.height < s.owner.clipY || s.y > s.owner.clipY + s.owner.height)
				s.stop(s._seqIndex);
			else
				s.start(null, s._seqIndex);
		}
		public get inPool():boolean
		{
			let s = this;
			return s._inPool;
		}
		public set inPool(val:boolean)
		{
			let s = this;
			s._inPool = val;
		}
		public outPoolInit():void
		{
			let s = this;
			
		}
		private _inPool:boolean;
		/**清理回收*/
		public clear():void
		{
			let s = this;
			if(s._inPool)return;
			PoolUtil.toPool(s,RichSeqImage);
			s.owner.imgGrp.removeElement(this);
			s.source = null;
			s.owner.removeEventListener(GYViewEvent.CLIP_SCROLL, s.clipScroll,s);
			// RichSeqImage._pool.push(this);
		}
		public static imageList:Array<any>=[];
		// private static _pool:RichSeqImage[];		
		/**获取图片实例 
		 * @param pr 父级对象
		 * @param posX s.x坐标
		 * @param posY s.y坐标
		 * @param param 参数*/
		public static getInstance(pr:GYRichText,posX:number,posY:number,param:string):RichSeqImage
		{
			var richImg:RichSeqImage;			
			richImg = <RichSeqImage>PoolUtil.fromPool(RichSeqImage);
			var arr:Array<any> = param.split(",");
			richImg.owner = pr;
			pr.imgGrp.addElement(richImg);
			richImg.x = posX + Number(arr[1])>>0;
			richImg.y = posY + Number(arr[1])>>0;
			richImg.source = RichSeqImage.imageList[arr[0]];
			richImg.getInit();
			return richImg;
		}
		/**获取图片文本
		 * @param w-宽度 h-高度 Id-图片id 
		 * */
		public static getImageRich(Id:number=1,offsetX:number = 0,offsetY:number = 0,w:number=30,h:number=23):string
		{
			return "<bmp s.width='" + w + "' s.height='" + h + "' target='GYLite.RichSeqImage' render='getInstance' renderParam='" + Id + "," + offsetX + "," + offsetY + "' clear='clear' >";
		}
	}
}
