declare module GYLite {
    class GYScroller {
        private _scrollBarV;
        private _scrollBarH;
        private _verticalPolicy;
        private _horizonPolicy;
        private _rect;
        private _viewport;
        private _parent;
        private _wheelScrollStep;
        /**滚轮是否启用*/ wheelEnabled: boolean;
        /**滚轮方向 1 纵向 2 横向*/ wheelDirection: number;
        constructor();
        /**滚动条控制的视图，必须是GYGroup*/
        set viewport(val: GYGroup);
        /**检测滚动条*/
        checkBar(e?: GYViewEvent): void;
        private checkRectV;
        private checkRectH;
        private showBar;
        private hideBar;
        set verticalPolicy(val: number);
        /**垂直滚动条 0自动 1显示 2不显示*/
        get verticalPolicy(): number;
        set horizonPolicy(val: number);
        /**水平滚动条 0自动 1显示 2不显示*/
        get horizonPolicy(): number;
        set wheelScrollStep(val: number);
        /**滚轮滚动步长*/
        get wheelScrollStep(): number;
        private scrollFunc;
        set scrollPosY(val: number);
        /**纵向滚动条当前位置value值(位置/步长)*/
        get scrollPosY(): number;
        set scrollPosX(val: number);
        /**横向滚动条当前位置value值(位置/步长)*/
        get scrollPosX(): number;
        get scrollBarH(): GYScrollBarH;
        get scrollBarV(): GYScrollBarV;
        private SetScrollX;
        private SetScrollY;
        set parent(val: GYSprite);
        /**添加到的显示对象*/
        get parent(): GYSprite;
        get viewport(): GYGroup;
    }
}
