declare module GYLite {
    class GYMouseTip {
        static t: GYText;
        private static flyTxt;
        private static count;
        static showFlyTip(val: string): void;
        private static flyLoop;
        static showTip(val: string): void;
        private static tipLoop;
        static hideTip(): void;
    }
}
