module GYLite {	
	/**小图的动态合批信息**/
	export class BatchInfo extends egret.HashObject implements IResource, IPoolObject{
		public $disposed:boolean;
		/**引用合批的显示对象列表*/
		protected _displayMap:any;
		/**合批图集信息*/
		public atlasInfo:AtlasInfo;
		/**合批图集*/
		public atlas:Atlas;		
		/**合批后产生的资源对象*/
		public batchRes:ResObject;
		/**关联合批的源纹理对象*/public $sourceTexture:egret.Texture;
		/**数据源*/public $source:HTMLImageElement|HTMLCanvasElement|GYTextElement;
		/**是否已经进行texSubImage上传gpu*/public isTexSubImage:boolean;
		/**合批后图集中的小图纹理的资源对象*/public $batchTexture:egret.Texture;
		protected _mgr:BatchManager;		
		/**显示对象对batchInfo的引用数*/public $referenceCount:number;
		public constructor(atlasInfo:AtlasInfo,atlas:Atlas,batchRes:ResObject,srcTex:egret.Texture,mgr:BatchManager=null) {
			super();
			let s = this;
			s._displayMap = egret.createMap();
			s.atlasInfo = atlasInfo;
			s.atlas = atlas;			
			s.batchRes = batchRes;	
			s.$sourceTexture = srcTex;
			s.$source = s.$sourceTexture.bitmapData.source;
			s.$batchTexture = s.batchRes.res;
			s._mgr = mgr;
			s.$referenceCount = 0;
		}				
		public get disposed(): boolean
		{
			return this.$disposed;
		}
		/**测量绘制gpu纹理*/
		public measureAndDraw():void
		{
			let s= this;
			if(s.$disposed)return;
			if(!s.isTexSubImage)
			{
				s.atlas.measureAndDraw(s.atlasInfo.rectInfo,s.$source);
				s.isTexSubImage = true;
			}			
		}
		/**添加引用的显示对象
		* @param display 显示对象
		*/
		public addReference(display:IBatch):void
		{
			let s = this;
			let hashCode:number;
			hashCode = display.$hashCode;
			if(s._displayMap[hashCode] == null)
			{
				s._displayMap[hashCode] = hashCode;
				++s.$referenceCount;
				if(s.$referenceCount == 1)
					s.atlas.removeFreeBatchInfo(s);
			}
			
		}
		/**移除引用的显示对象
		 * @param display 显示对象
		 * @return 返回是否无显示对象引用此batch
		*/
		public removeReference(display:IBatch):void
		{
			let s = this;
			let hashCode:number;
			hashCode = display.$hashCode;	
			if(s._displayMap[hashCode])
			{
				delete s._displayMap[hashCode];
				--s.$referenceCount;
				if(s.$referenceCount == 0)
					s.atlas.addFreeBatchInfo(s);				
			}
		}
		/**合批销毁*/
		public dispose():void
		{
			let s= this;
			if(s.$disposed)
				return;			
			s.clear();
			s.$disposed = true;				
		}		
		protected beforeToPool():void
        {
            let s= this;
			AtlasRender.getInstance().removeBatch(s);
			s._mgr.removeBatch(s);
			s.isTexSubImage = false;
			s.atlas = null;
			s.atlasInfo = null;
			s.$sourceTexture = null;
			s.$source = null;
			s.$batchTexture = null;
			s.batchRes = null;
			s._displayMap = null;             
        }
        public clear():void
        {
            let s = this;
            if(s.inPool)
                return;
            s.beforeToPool();            
            GYLite.PoolUtil.toPool(s, s.constructor);
        }
		public inPool:boolean;
		public outPoolInit():void
        {
			let s= this;
            s._displayMap = {};
        }
	}
}