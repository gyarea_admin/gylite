declare module GYLite {
    class IconButtonSkin extends ButtonSkin {
        constructor(skinVec: egret.Texture[], rect?: Scale9GridRect);
        drawSkin(state?: number): void;
    }
}
