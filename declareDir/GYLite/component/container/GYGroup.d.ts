declare module GYLite {
    class GYGroup extends GYUIComponent {
        protected _scrollPadLeft: number;
        protected _scrollPadRight: number;
        protected _scrollPadTop: number;
        protected _scrollPadBottom: number;
        protected _clipAndEnableScrolling: boolean;
        protected _clipRect: egret.Rectangle;
        $clipX: number;
        $clipY: number;
        protected _canDrag: boolean;
        protected _verticalDrag: boolean;
        protected _horizonDrag: boolean;
        protected _scroller: GYScroller;
        protected _easeGapY: boolean;
        protected _easeGapX: boolean;
        protected _easeXFix: number;
        protected _easeYFix: number;
        protected _easeSpeedValue: number;
        protected _clipChange: boolean;
        protected _tweenX: GYTween;
        protected _tweenY: GYTween;
        protected _touchId: number;
        /**产生缓动的速度*/ easeValue: number;
        /**拖动敏感度*/ dragValue: number;
        /**边界拖拽系数*/ protected _dragEaseValue: number;
        /**拖拽步长最大值*/ protected _dragEaseMax: number;
        /**缓动越界最大值*/ protected _easeMoveMax: number;
        /**触发缓动的时间阈值*/ protected _easeTriggerTime: number;
        /**弹性恢复时间*/ protected _easeTime: number;
        protected _innerSprite: InnerSprite;
        protected _endPointer: GYSprite;
        protected _outSideSp: InnerSprite;
        protected _indexArray: Array<IGYDisplay>;
        protected _outSideOptimize: boolean;
        protected _lastXDragTime: number;
        protected _lastYDragTime: number;
        protected _moveXTime: number;
        protected _moveYTime: number;
        protected _scrollSpeedX: number;
        protected _scrollSpeedY: number;
        protected _limitXMin: number;
        protected _limitXMax: number;
        protected _limitYMin: number;
        protected _limitYMax: number;
        protected _isDragging: boolean;
        /**是否拖拽时禁止滚轮，默认true*/ dragForbiddenWheel: boolean;
        constructor();
        /**本容器添加，非内层容器*/
        baseAdd(child: IGYDisplay): void;
        /**本容器添加，非内层容器*/
        baseRemove(child: IGYDisplay): void;
        /**@inheritDoc */
        getElementAt(index: number): IGYDisplay;
        /**@inheritDoc */
        getElementIndex(child: IGYDisplay): number;
        /**@inheritDoc*/
        delElement(child: IGYDisplay): void;
        /**@inheritDoc*/
        addElement(disObj: IGYDisplay): IGYDisplay;
        /**@inheritDoc*/
        addElementAt(disObj: IGYDisplay, index: number): IGYDisplay;
        /**@inheritDoc*/
        removeElement(disObj: IGYDisplay): IGYDisplay;
        /**@inheritDoc*/
        removeElementAt(index: number): IGYDisplay;
        /**@inheritDoc*/
        removeAllElement(): void;
        /**@inheritDoc*/
        setElementIndex(child: IGYDisplay, index: number): void;
        /**@inheritDoc*/
        swapElementIndex(child1: any, child2: any): void;
        get numElement(): number;
        getElementList(): Array<IGYDisplay>;
        /**@inheritDoc*/
        updateView(): void;
        /**刷新裁剪*/
        invalidClip(): void;
        set clipAndEnableScrolling(val: boolean);
        /**是否允许裁切边界进行滚动*/
        get clipAndEnableScrolling(): boolean;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        set group_width(val: number);
        get group_width(): number;
        set group_height(val: number);
        get group_height(): number;
        validClip(): void;
        /**请使用s.clipAndEnableScrolling以实现*/
        get scrollRect(): egret.Rectangle;
        set scrollRect(rect: egret.Rectangle);
        set clipX(val: number);
        set clipY(val: number);
        /**s.clipAndEnableScrolling为true时有效，s.scrollRect滚动的s.x坐标*/
        get clipX(): number;
        /**s.clipAndEnableScrolling为true时有效，s.scrollRect滚动的s.y坐标*/
        get clipY(): number;
        get scroller(): GYScroller;
        set scroller(val: GYScroller);
        /**能否拖动,仅在s.clipAndEnableScrolling为true时有效* */
        get canDrag(): boolean;
        set canDrag(val: boolean);
        /**是否允许纵向拖动，当s.canDrag为true时才有效*/
        get verticalDrag(): boolean;
        set verticalDrag(val: boolean);
        /**是否允许横向拖动，当s.canDrag为true时才有效*/
        get horizonDrag(): boolean;
        set horizonDrag(val: boolean);
        protected downGroup(e: egret.TouchEvent): void;
        protected upGroup(e?: egret.TouchEvent): void;
        protected globalDragCall(e: GYEvent): void;
        protected groupDrag(): void;
        clearGlobalDrag(): void;
        protected groupDragStop(): void;
        protected touchMove(e: egret.TouchEvent): void;
        protected tempY: number;
        protected tempX: number;
        protected easeSpeedX: number;
        protected easeSpeedY: number;
        protected dragLoop(t: number): void;
        private dragEnd_Horizonal;
        private dragEnd_Vertical;
        /**缓动到某Y坐标
         * @param val 坐标值Y
         * @param time 缓动时间
        */
        clipYEaseTo(val: number, time?: number): void;
        private startYAni;
        private easeYComp;
        /**缓动到某X坐标*/
        clipXEaseTo(val: number, time?: number): void;
        private startXAni;
        private easeXComp;
        /**弹性恢复停止值(上下占位像素)，大于等于0*/
        get easeYFix(): number;
        set easeYFix(val: number);
        /**弹性恢复停止值(左右占位像素)，大于等于0*/
        get easeXFix(): number;
        set easeXFix(val: number);
        /**超出闪现边界范围值，上边界为负数，下边界为正数*/
        get easeOutY(): number;
        /**超出左右边界范围值，左边界为负数，右边界为正数*/
        get easeOutX(): number;
        set easeGapY(val: boolean);
        set easeGapX(val: boolean);
        /**是否超出底边界弹性恢复*/
        get easeGapY(): boolean;
        /**是否超出底边界弹性恢复*/
        get easeGapX(): boolean;
        protected _oldMouseChildren: boolean;
        protected _dragLock: boolean;
        /**拖动group时锁上内容不响应鼠标事件*/
        protected set dragLock(val: boolean);
        protected get dragLock(): boolean;
        /**css层级显示*/
        invalidZIndex(): void;
        validZIndex(): void;
        protected updateContentSize(child: IGYDisplay): void;
        set outSideOptimize(val: boolean);
        get outSideOptimize(): boolean;
        /**检测屏内外元素
         * @param checkChild 需要进行检测的显示对象
         * @param updateZIndex 是否更新层级
        */
        checkOutSize(checkChild?: IGYDisplay, updateZIndex?: boolean): void;
        /**覆盖getAllBounds,以便得到真实内容的宽高*/
        getAllBounds(t: IGYDisplay): egret.Rectangle;
        get scrollPadLeft(): number;
        set scrollPadLeft(val: number);
        get scrollPadRight(): number;
        set scrollPadRight(val: number);
        get scrollPadTop(): number;
        set scrollPadTop(val: number);
        get scrollPadBottom(): number;
        set scrollPadBottom(val: number);
        set limitXMin(val: number);
        get limitXMin(): number;
        /**限制滚动x方向最大值，默认NaN，不限制*/
        set limitXMax(val: number);
        /**限制滚动x方向最小值，默认NaN，不限制*/
        get limitXMax(): number;
        set limitYMin(val: number);
        /**限制滚动y方向最小值，默认NaN，不限制*/
        get limitYMin(): number;
        set limitYMax(val: number);
        /**限制滚动y方向最大值，默认NaN，不限制*/
        get limitYMax(): number;
        /**停止拖动响应*/
        stopDrag(): void;
        /**是否拖拽中*/
        get isDragging(): boolean;
        /**产生缓动的速度*/ static default_easeValue: number;
        /**拖动敏感度*/ static default_dragValue: number;
        /**加速度系数1/2a*/ static default_easeSpeedValue: number;
        /**边界拖拽系数*/ static default_dragEaseValue: number;
        /**拖拽步长最大值*/ static default_dragEaseMax: number;
        /**缓动越界最大值*/ static default_easeMoveMax: number;
        /**触发缓动的时间阈值*/ static default_easeTriggerTime: number;
        /**弹性恢复时间*/ static default_easeTime: number;
    }
    class InnerSprite extends GYUIComponent {
        private _virtualParent;
        constructor(pr: GYLite.GYGroup);
        get parent(): GYLite.GYGroup;
    }
}
