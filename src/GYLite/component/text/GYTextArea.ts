module GYLite
{
									
			
	export class GYTextArea extends GYSkinContainer
	{
		private _textArea:GYText=null;
		private _vScroller:GYScroller=null;
		private _textGrp:GYGroup=null;
		private _editable:boolean=false;
		private _invalidTextArea:boolean=false;
		public constructor(skin:any=null)
		{
			super();
			var s = this;
			this.skin = skin;
			s.initComponent();
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();
			s._textArea = new GYText(true);
			s._textArea.x = GYTextBase.default_clipXPadding;
			s._textArea.y = GYTextBase.default_clipYPadding;
			s._textArea.addEventListener(GYViewEvent.UPDATE_COMPLETE, s.updateComp, s);					
			s._textArea.addEventListener(egret.FocusEvent.FOCUS_IN, s.staticTextShow,s);	
			s._textGrp = new GYGroup;
			s._textArea.clipGroup = s._textGrp;
			s._textGrp.addElement(s._textArea);
			s._textGrp.clipAndEnableScrolling = true;
			s._textGrp.touchEnabled = true;			
			s._textGrp.addEventListener(GYViewEvent.UPDATE_COMPLETE, s.groupUpdateComp, s);
			s.addElement(s._textGrp);
			s._vScroller =new GYScroller;
			s._vScroller.parent = this;
			s._vScroller.viewport = s._textGrp;	
			s.textOver=function(event:any):void{
				GYLite.dispatch(MouseEvent.ROLL_OVER, true, s.mouseX, s.mouseY, s._textGrp);				
			}
			s.textRollOver=function(event:any):void
			{
				CommonUtil.delStageLoop(s.selectedLoop,s);				
			}
			s.textRollOut=function(event:any):void
			{
				CommonUtil.addStageLoop(s.selectedLoop,s);
			}
			s.textUp=function(event:any):void
			{
				let input:any = (<any>s._textArea).inputUtils.stageText.inputElement;
				if(input==null)input=event.currentTarget;				
				document.removeEventListener("mouseup", s.textUp);				
				CommonUtil.delStageLoop(s.selectedLoop,s);				
			}
			s.domMouseDown = function(e):void{				
				let input:any = (<any>s._textArea).inputUtils.stageText.inputElement;				
				document.addEventListener("mouseup", s.textUp);				
				GYSprite.addStageDown(s._textArea,s.textUp, s);	
				CommonUtil.addStageLoop(s.selectedLoop,s);
			};
			s.stageTextShow=function():void{				
				if(!s._textArea.$isTyping)return;
				let input:any = (<any>s._textArea).inputUtils.stageText.inputElement;				
				if(input==null)
					return;				
				input.addEventListener("mousedown", s.domMouseDown);
				input.addEventListener("mouseover", s.textOver);				
									
			}
		}
		
		protected updateComp(event:GYViewEvent):void
		{
			let s = this;
			s._textGrp.invalidZIndex();
			s.invalidTextArea();			
		}
		private domMouseDown:any;
		private stageTextShow:any;
		private textUp:any;
		private textRollOver:any;
		private textRollOut:any;
		private textOver:any;
		private staticTextShow(event:GYViewEvent):void
		{
			let s = this;			
			s.stageTextShow();
		}	
		private selectedLoop(t:number):void
		{
			let s = this;			
			
			if(s.mouseY < 0)
			{				
				s._vScroller.scrollBarV.scrollToTop(5);
			}				
			else if(s.mouseY > s.height)
			{				
				s._vScroller.scrollBarV.scrollToBottom(5);
			}
		}
		
		public set width(val:number)
		{
			let s = this;
			egret.superSetter(GYTextArea, this, "width", val);
			//super.width = val;
			s._textGrp.width = s.width - s._textGrp.paddingLeft - s._textGrp.paddingRight;
			s._textArea.width = s._textGrp.baseWidth - s._textArea.$x;
			s._skin.width = s.width;
			s.invalidTextArea();
		}
		public get width():number
		{
			let s = this;			
			return egret.superGetter(GYTextArea, this, "width");
		}
		public set height(val:number)
		{
			let s = this;
			egret.superSetter(GYTextArea, this, "height", val);
//			s._textGrp.height = val;
			s._textGrp.height = s.height - s._textGrp.paddingTop - s._textGrp.paddingBottom;
			s._textArea.minHeight = s._textGrp.baseHeight - s._textArea.y;
			s._skin.height = s.height;
			s.invalidTextArea();
		}
		public get height():number
		{
			let s = this;			
			return egret.superGetter(GYTextArea, this, "height");
		}
		public get textArea():GYText
		{
			let s = this;
			return s._textArea;
		}
		public get vScroller():GYScroller
		{
			let s = this;
			return s._vScroller;
		}
		public set editable(val:boolean)
		{
			let s = this;
			if(s._editable == val)
				return;
			s._editable =val;
			if(s._editable)
				s._textArea.type = egret.TextFieldType.INPUT;
			else
				s._textArea.type = egret.TextFieldType.DYNAMIC;
		}
		/**是否可编辑*/
		public get editable():boolean
		{
			let s = this;
			return s._editable;
		}

		public get textGrp():GYGroup
		{
			let s = this;
			return s._textGrp;
		}
		/**获取主题皮肤，自定义请实现IGYSkin*/
		protected getThemeSkin():IGYSkin
		{
			let s = this;
			return GYSprite.skinTheme.GetTextAreaSkin();
		}

		public get text():string
		{
			let s = this;
			return s._textArea.text;
		}

		public set text(value:string)
		{
			let s = this;
			s._textArea.text = value;
		}
		
		public setPadding(val:number):void
		{
			let s = this;
			s.paddingLeft = val;
			s.paddingTop = val;
			s.paddingRight = val + s._vScroller.scrollBarV.width;
			s.paddingBottom = val;
			s._vScroller.scrollBarV.right = 5;
			s._vScroller.scrollBarV.y = val;
		}
		public set paddingTop(val:number)
		{
			let s = this;
			s._textGrp.paddingTop = val;
			s._textGrp.height = s.height - val - s._textGrp.paddingBottom;
			s._textArea.minHeight = s._textGrp.baseHeight - s._textArea.$y;
		}
		public set paddingLeft(val:number)
		{
			let s = this;
			s._textGrp.paddingLeft = val;
			s._textGrp.width = s.width - val - s._textGrp.paddingRight;
			s._textArea.width = s._textGrp.baseWidth - s._textArea.$x;
		}
		public set paddingRight(val:number)
		{
			let s = this;
			s._textGrp.paddingRight = val;
			s._textGrp.width = s.width - s._textGrp.paddingLeft - val;
			s._textArea.width = s._textGrp.baseWidth - s._textArea.$x;
		}
		public set paddingBottom(val:number)
		{
			let s = this;
			s._textGrp.paddingBottom = val;
			s._textGrp.height = s.height - s._textGrp.paddingTop - val;
			s._textArea.minHeight = s._textGrp.baseHeight - s._textArea.$y;
		}
		public get paddingTop():number
		{
			let s = this;			
			return s._textGrp.paddingTop;
		}
		public get paddingLeft():number
		{
			let s = this;			
			return s._textGrp.paddingLeft;
		}
		public get paddingRight():number
		{
			let s = this;			
			return s._textGrp.paddingRight
		}
		public get paddingBottom():number
		{
			let s = this;			
			return s._textGrp.paddingBottom;
		}
		public updateView():void
		{
			let s = this;
			super.updateView();
			if(s._invalidTextArea)
			{
				s._vScroller.checkBar();
				s._invalidTextArea = false;
			}
		}
		public invalidTextArea():void
		{
			let s = this;
			if(!s._invalidTextArea)
			{
				s._invalidTextArea = true;
				s.displayChg();
			}
		}
		private groupUpdateComp(e:GYViewEvent)
		{
			var s = this;
			if(!s._textArea.$isTyping)return;
			(<any>s._textArea).inputUtils.stageText._initElement();
			(<any>s._textArea).inputUtils.stageText.$resetStageText();
		}
	}
}