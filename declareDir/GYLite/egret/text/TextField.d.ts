declare namespace egret.sys {
    /**
     * @private
     */
    const enum TextKeys {
        /**
         * @private
         */
        fontSize = 0,
        /**
         * @private
         */
        lineSpacing = 1,
        /**
         * @private
         */
        textColor = 2,
        /**
         * @private
         */
        textFieldWidth = 3,
        /**
         * @private
         */
        textFieldHeight = 4,
        /**
         * @private
         */
        textWidth = 5,
        /**
         * @private
         */
        textHeight = 6,
        /**
         * @private
         */
        textDrawWidth = 7,
        /**
         * @private
         */
        fontFamily = 8,
        /**
         * @private
         */
        textAlign = 9,
        /**
         * @private
         */
        verticalAlign = 10,
        /**
         * @private
         */
        textColorString = 11,
        /**
         * @private
         */
        fontString = 12,
        /**
         * @private
         */
        text = 13,
        /**
         * @private
         */
        measuredWidths = 14,
        /**
         * @private
         */
        bold = 15,
        /**
         * @private
         */
        italic = 16,
        /**
         * @private
         */
        fontStringChanged = 17,
        /**
         * @private
         */
        textLinesChanged = 18,
        /**
         * @private
         */
        wordWrap = 19,
        /**
         * @private
         */
        displayAsPassword = 20,
        /**
         * @private
         */
        maxChars = 21,
        /**
         * @private
         */
        selectionActivePosition = 22,
        /**
         * @private
         */
        selectionAnchorPosition = 23,
        /**
         * @private
         */
        type = 24,
        /**
         * @private
         */
        strokeColor = 25,
        /**
         * @private
         */
        strokeColorString = 26,
        /**
         * @private
         */
        stroke = 27,
        /**
         * @private
         */
        scrollV = 28,
        /**
         * @private
         */
        numLines = 29,
        /**
         * @private
         */
        multiline = 30,
        /**
         * @private
         */
        border = 31,
        /**
         * @private
         */
        borderColor = 32,
        /**
         * @private
         */
        background = 33,
        /**
         * @private
         */
        backgroundColor = 34,
        /**
         * @private
         */
        restrictAnd = 35,
        /**
         * @private
         */
        restrictNot = 36,
        /**
         * @private
         */
        inputType = 37,
        /**
         * @private
         */
        textLinesChangedForNativeRender = 38
    }
}
declare namespace egret {
    /**
     * add new language word wrap regex and use it
     * if languageKey already exists,the existing regex is replaced
     * if the pattern is not passed,it will be found and enabled int the existing word wrap map
     * @param languageKey
     * @param pattern
     * @version Egret 5.3.11
     * @platform Web
     * @language en_US
     */
    /**
     * 添加新的自动换行的语言正则表达式匹配并启用
     * 如果已经有该语言了，则会替换现有正则表达式
     * 不传入正则表达式则会在已有的语言自动换行匹配串中寻找并启用
     * @param languageKey
     * @param pattern
     * @version Egret 5.3.11
     * @platform Web
     * @language zh_CN
     */
    function addLanguageWordWrapRegex(languageKey: string, pattern?: string): void;
    /**
     * return the existing word wrap keys
     * @version Egret 5.3.11
     * @platform Web
     * @language en_US
     */
    /**
     * 获取当前已有的自动换行映射键值组
     * @version Egret 5.3.11
     * @platform Web
     * @language zh_CN
     */
    function getAllSupportLanguageWordWrap(): string[];
    /**
     * return the using word wrap keys
     * @version Egret 5.3.11
     * @platform Web
     * @language en_US
     */
    /**
     * 获取当前正在使用中的自动换行映射键值组
     * @version Egret 5.3.11
     * @platform Web
     * @language zh_CN
     */
    function getUsingWordWrap(): string[];
    /**
     * cancels the using word wrap regex by the languageKey
     * @version Egret 5.3.11
     * @platform Web
     * @language en_US
     */
    /**
     * 根据languageKey取消正在启用的自动换行正则表达式
     * @version Egret 5.3.11
     * @platform Web
     * @language zh_CN
     */
    function cancelLanguageWordWrapRegex(languageKey: string): void;
    /**
     * TextField is the text rendering class of egret. It conducts rendering by using the browser / device API. Due to different ways of font rendering in different browsers / devices, there may be differences in the rendering
     * If developers expect  no differences among all platforms, please use BitmapText
     * @see http://edn.egret.com/cn/docs/page/141 Create Text
     *
     * @event egret.Event.CHANGE Dispatched when entering text user input。
     * @event egret.FocusEvent.FOCUS_IN Dispatched after the focus to enter text.
     * @event egret.FocusEvent.FOCUS_OUT Enter the text loses focus after dispatch.
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/text/TextField.ts
     * @language en_US
     */
    /**
     * TextField是egret的文本渲染类，采用浏览器/设备的API进行渲染，在不同的浏览器/设备中由于字体渲染方式不一，可能会有渲染差异
     * 如果开发者希望所有平台完全无差异，请使用BitmapText
     * @see http://edn.egret.com/cn/docs/page/141 创建文本
     *
     * @event egret.Event.CHANGE 输入文本有用户输入时调度。
     * @event egret.FocusEvent.FOCUS_IN 聚焦输入文本后调度。
     * @event egret.FocusEvent.FOCUS_OUT 输入文本失去焦点后调度。
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/text/TextField.ts
     * @language zh_CN
     */
    class TextField extends DisplayObject {
        /**
         * default fontFamily
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 默认文本字体
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static default_fontFamily: string;
        /**
         * default size in pixels of text
         * @version Egret 3.2.1
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 默认文本字号大小
         * @version Egret 3.2.1
         * @platform Web,Native
         * @language zh_CN
         */
        static default_size: number;
        /**
         * default color of the text.
         * @version Egret 3.2.1
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 默认文本颜色
         * @version Egret 3.2.1
         * @platform Web,Native
         * @language zh_CN
         */
        static default_textColor: number;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        constructor();
        protected createNativeDisplayObject(): void;
        /**
         * @private
         */
        $TextField: Object;
        /**
         * @private
         */
        private isInput;
        $inputEnabled: boolean;
        $setTouchEnabled(value: boolean): void;
        /**
         * The name of the font to use, or a comma-separated list of font names.
         * @default "Arial"
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 要使用的字体的名称或用逗号分隔的字体名称列表。
         * @default "Arial"
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get fontFamily(): string;
        set fontFamily(value: string);
        $setFontFamily(value: string): boolean;
        /**
         * The size in pixels of text
         * @default 30
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 文本的字号大小。
         * @default 30
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get size(): number;
        set size(value: number);
        $setSize(value: number): boolean;
        /**
         * Specifies whether the text is boldface.
         * @default false
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 是否显示为粗体。
         * @default false
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get bold(): boolean;
        set bold(value: boolean);
        $setBold(value: boolean): boolean;
        /**
         * Determines whether the text is italic font.
         * @default false
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 是否显示为斜体。
         * @default false
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get italic(): boolean;
        set italic(value: boolean);
        $setItalic(value: boolean): boolean;
        /**
         * @private
         *
         */
        private invalidateFontString;
        /**
         * Horizontal alignment of text.
         * @default：egret.HorizontalAlign.LEFT
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 文本的水平对齐方式。
         * @default：egret.HorizontalAlign.LEFT
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get textAlign(): string;
        set textAlign(value: string);
        $setTextAlign(value: string): boolean;
        /**
         * Vertical alignment of text.
         * @default：egret.VerticalAlign.TOP
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 文字的垂直对齐方式。
         * @default：egret.VerticalAlign.TOP
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get verticalAlign(): string;
        set verticalAlign(value: string);
        $setVerticalAlign(value: string): boolean;
        /**
         * An integer representing the amount of vertical space between lines.
         * @default 0
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 一个整数，表示行与行之间的垂直间距量
         * @default 0
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get lineSpacing(): number;
        set lineSpacing(value: number);
        $setLineSpacing(value: number): boolean;
        /**
         * Color of the text.
         * @default 0x000000
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 文本颜色
         * @default 0x000000
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get textColor(): number;
        set textColor(value: number);
        $setTextColor(value: number): boolean;
        /**
         * A Boolean value that indicates whether the text field word wrap. If the value is true, then the text field by word wrap;
         * if the value is false, the text field by newline characters.
         * @default false
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 一个布尔值，表示文本字段是否按单词换行。如果值为 true，则该文本字段按单词换行；
         * 如果值为 false，则该文本字段按字符换行。
         * @default false
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get wordWrap(): boolean;
        set wordWrap(value: boolean);
        $setWordWrap(value: boolean): void;
        protected inputUtils: InputController;
        /**
         * Type of the text field.
         * Any one of the following TextFieldType constants: TextFieldType.DYNAMIC (specifies the dynamic text field that users can not edit), or TextFieldType.INPUT (specifies the dynamic text field that users can edit).
         * @default egret.TextFieldType.DYNAMIC
         * @language en_US
         */
        /**
         * 文本字段的类型。
         * 以下 TextFieldType 常量中的任一个：TextFieldType.DYNAMIC（指定用户无法编辑的动态文本字段），或 TextFieldType.INPUT（指定用户可以编辑的输入文本字段）。
         * @default egret.TextFieldType.DYNAMIC
         * @language zh_CN
         */
        set type(value: string);
        /**
         * @private
         *
         * @param value
         */
        $setType(value: string): boolean;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get type(): string;
        /**
         * Pop-up keyboard type.
         * Any of a TextFieldInputType constants.
         * @language en_US
         */
        /**
         * 弹出键盘的类型。
         * TextFieldInputType 常量中的任一个。
         * @language zh_CN
         */
        set inputType(value: string);
        /**
         * @version Egret 3.1.2
         * @platform Web,Native
         */
        get inputType(): string;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get text(): string;
        /**
         * @private
         *
         * @returns
         */
        $getText(): string;
        /**
         * Serve as a string of the current text field in the text
         * @language en_US
         */
        /**
         * 作为文本字段中当前文本的字符串
         * @language zh_CN
         */
        set text(value: string);
        /**
         * @private
         *
         * @param value
         */
        $setBaseText(value: string): boolean;
        /**
         * @private
         *
         * @param value
         */
        $setText(value: string): boolean;
        /**
         * Specify whether the text field is a password text field.
         * If the value of this property is true, the text field is treated as a password text field and hides the input characters using asterisks instead of the actual characters. If false, the text field is not treated as a password text field.
         * @default false
         * @language en_US
         */
        /**
         * 指定文本字段是否是密码文本字段。
         * 如果此属性的值为 true，则文本字段被视为密码文本字段，并使用星号而不是实际字符来隐藏输入的字符。如果为 false，则不会将文本字段视为密码文本字段。
         * @default false
         * @language zh_CN
         */
        get displayAsPassword(): boolean;
        set displayAsPassword(value: boolean);
        /**
         * @private
         *
         * @param value
         */
        $setDisplayAsPassword(value: boolean): boolean;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get strokeColor(): number;
        /**
         * Represent the stroke color of the text.
         * Contain three 8-bit numbers with RGB color components; for example, 0xFF0000 is red, 0x00FF00 is green.
         * @default 0x000000
         * @language en_US
         */
        /**
         * 表示文本的描边颜色。
         * 包含三个 8 位 RGB 颜色成分的数字；例如，0xFF0000 为红色，0x00FF00 为绿色。
         * @default 0x000000
         * @language zh_CN
         */
        set strokeColor(value: number);
        /**
         * @private
         *
         * @param value
         */
        $setStrokeColor(value: number): boolean;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get stroke(): number;
        /**
         * Indicate the stroke width.
         * 0 means no stroke.
         * @default 0
         * @language en_US
         */
        /**
         * 表示描边宽度。
         * 0为没有描边。
         * @default 0
         * @language zh_CN
         */
        set stroke(value: number);
        /**
         * @private
         *
         * @param value
         */
        $setStroke(value: number): boolean;
        /**
         * The maximum number of characters that the text field can contain, as entered by a user. \n A script can insert more text than maxChars allows; the maxChars property indicates only how much text a user can enter. If the value of this property is 0, a user can enter an unlimited amount of text.
         * The default value is 0.
         * @default 0
         * @language en_US
         */
        /**
         * 文本字段中最多可包含的字符数（即用户输入的字符数）。
         * 脚本可以插入比 maxChars 允许的字符数更多的文本；maxChars 属性仅表示用户可以输入多少文本。如果此属性的值为 0，则用户可以输入无限数量的文本。
         * @default 0
         * @language zh_CN
         */
        get maxChars(): number;
        set maxChars(value: number);
        /**
         * @private
         *
         * @param value
         */
        $setMaxChars(value: number): boolean;
        /**
         * Vertical position of text in a text field. scrollV property helps users locate specific passages in a long article, and create scrolling text fields.
         * Vertically scrolling units are lines, and horizontal scrolling unit is pixels.
         * If the first displayed line is the first line in the text field, scrollV is set to 1 (instead of 0).
         * @language en_US
         */
        /**
         * 文本在文本字段中的垂直位置。scrollV 属性可帮助用户定位到长篇文章的特定段落，还可用于创建滚动文本字段。
         * 垂直滚动的单位是行，而水平滚动的单位是像素。
         * 如果显示的第一行是文本字段中的第一行，则 scrollV 设置为 1（而非 0）。
         * @language zh_CN
         */
        set scrollV(value: number);
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get scrollV(): number;
        /**
         * The maximum value of scrollV
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * scrollV 的最大值
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get maxScrollV(): number;
        /**
         * @private
         * @version Egret 2.4
         * @platform Web,Native
         */
        get selectionBeginIndex(): number;
        /**
         * @private
         * @version Egret 2.4
         * @platform Web,Native
         */
        get selectionEndIndex(): number;
        /**
         * @private
         * @version Egret 2.4
         * @platform Web,Native
         */
        get caretIndex(): number;
        /**
         * @private
         *
         * @param beginIndex
         * @param endIndex
         */
        $setSelection(beginIndex: number, endIndex: number): boolean;
        /**
         * @private
         *
         * @returns
         */
        $getLineHeight(): number;
        /**
         * Number of lines of text.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 文本行数。
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get numLines(): number;
        /**
         * Indicate whether field is a multiline text field. Note that this property is valid only when the type is TextFieldType.INPUT.
         * If the value is true, the text field is multiline; if the value is false, the text field is a single-line text field. In a field of type TextFieldType.INPUT, the multiline value determines whether the Enter key creates a new line (a value of false, and the Enter key is ignored).
         * @default false
         * @language en_US
         */
        /**
         * 表示字段是否为多行文本字段。注意，此属性仅在type为TextFieldType.INPUT时才有效。
         * 如果值为 true，则文本字段为多行文本字段；如果值为 false，则文本字段为单行文本字段。在类型为 TextFieldType.INPUT 的字段中，multiline 值将确定 Enter 键是否创建新行（如果值为 false，则将忽略 Enter 键）。
         * @default false
         * @language zh_CN
         */
        set multiline(value: boolean);
        /**
         * @private
         *
         * @param value
         */
        $setMultiline(value: boolean): boolean;
        get multiline(): boolean;
        /**
         * Indicates a user can enter into the text field character set. If you restrict property is null, you can enter any character. If you restrict property is an empty string, you can not enter any character. If you restrict property is a string of characters, you can enter only characters in the string in the text field. The string is scanned from left to right. You can use a hyphen (-) to specify a range. Only restricts user interaction; a script may put any text into the text field. <br/>
                  * If the string of characters caret (^) at the beginning, all characters are initially accepted, then the string are excluded from receiving ^ character. If the string does not begin with a caret (^) to, any characters are initially accepted and then a string of characters included in the set of accepted characters. <br/>
                  * The following example allows only uppercase characters, spaces, and numbers in the text field: <br/>
                  * My_txt.restrict = "A-Z 0-9"; <br/>
                  * The following example includes all characters except lowercase letters: <br/>
                  * My_txt.restrict = "^ a-z"; <br/>
                  * If you need to enter characters \ ^, use two backslash "\\ -" "\\ ^": <br/>
                  * Can be used anywhere in the string ^ to rule out including characters and switch between characters, but can only be used to exclude a ^. The following code includes only uppercase letters except uppercase Q: <br/>
                  * My_txt.restrict = "A-Z ^ Q"; <br/>
         * @version Egret 2.4
         * @platform Web,Native
         * @default null
         * @language en_US
         */
        /**
         * 表示用户可输入到文本字段中的字符集。如果 restrict 属性的值为 null，则可以输入任何字符。如果 restrict 属性的值为空字符串，则不能输入任何字符。如果 restrict 属性的值为一串字符，则只能在文本字段中输入该字符串中的字符。从左向右扫描该字符串。可以使用连字符 (-) 指定一个范围。只限制用户交互；脚本可将任何文本放入文本字段中。<br/>
         * 如果字符串以尖号 (^) 开头，则先接受所有字符，然后从接受字符集中排除字符串中 ^ 之后的字符。如果字符串不以尖号 (^) 开头，则最初不接受任何字符，然后将字符串中的字符包括在接受字符集中。<br/>
         * 下例仅允许在文本字段中输入大写字符、空格和数字：<br/>
         * my_txt.restrict = "A-Z 0-9";<br/>
         * 下例包含除小写字母之外的所有字符：<br/>
         * my_txt.restrict = "^a-z";<br/>
         * 如果需要输入字符 \ ^，请使用2个反斜杠 "\\-" "\\^" ：<br/>
         * 可在字符串中的任何位置使用 ^，以在包含字符与排除字符之间进行切换，但是最多只能有一个 ^ 用来排除。下面的代码只包含除大写字母 Q 之外的大写字母：<br/>
         * my_txt.restrict = "A-Z^Q";<br/>
         * @version Egret 2.4
         * @platform Web,Native
         * @default null
         * @language zh_CN
         */
        set restrict(value: string);
        get restrict(): string;
        /**
         * @private
         *
         * @param value
         */
        $setWidth(value: number): boolean;
        /**
         * @private
         *
         * @param value
         */
        $setHeight(value: number): boolean;
        /**
         * @private
         * 获取显示宽度
         */
        $getWidth(): number;
        /**
         * @private
         * 获取显示宽度
         */
        $getHeight(): number;
        /**
         * @protected //GYLite
         */
        protected textNode: sys.TextNode;
        /**
         * @private
         */
        $graphicsNode: sys.GraphicsNode;
        /**
         * Specifies whether the text field has a border.
         * If true, the text field has a border. If false, the text field has no border.
         * Use borderColor property to set the border color.
         * @default false
         * @language en_US
         */
        /**
         * 指定文本字段是否具有边框。
         * 如果为 true，则文本字段具有边框。如果为 false，则文本字段没有边框。
         * 使用 borderColor 属性来设置边框颜色。
         * @default false
         * @language zh_CN
         */
        set border(value: boolean);
        /**
         * @private
         */
        $setBorder(value: boolean): void;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get border(): boolean;
        /**
         * The color of the text field border.
         * Even currently is no border can be retrieved or set this property, but only if the text field has the border property is set to true, the color is visible.
         * @default 0x000000
         * @language en_US
         */
        /**
         * 文本字段边框的颜色。
         * 即使当前没有边框，也可检索或设置此属性，但只有当文本字段已将 border 属性设置为 true 时，才可以看到颜色。
         * @default 0x000000
         * @language zh_CN
         */
        set borderColor(value: number);
        /**
         * @private
         */
        $setBorderColor(value: number): void;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get borderColor(): number;
        /**
         * Specifies whether the text field has a background fill.
         * If true, the text field has a background fill. If false, the text field has no background fill.
         * Use the backgroundColor property to set the background color of the text field.
         * @default false
         * @language en_US
         */
        /**
         * 指定文本字段是否具有背景填充。
         * 如果为 true，则文本字段具有背景填充。如果为 false，则文本字段没有背景填充。
         * 使用 backgroundColor 属性来设置文本字段的背景颜色。
         * @default false
         * @language zh_CN
         */
        set background(value: boolean);
        /**
         * @private
         */
        $setBackground(value: boolean): void;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get background(): boolean;
        /**
         * Color of the text field background.
         * Even currently is no background, can be retrieved or set this property, but only if the text field has the background property set to true, the color is visible.
         * @default 0xFFFFFF
         * @language en_US
         */
        /**
         * 文本字段背景的颜色。
         * 即使当前没有背景，也可检索或设置此属性，但只有当文本字段已将 background 属性设置为 true 时，才可以看到颜色。
         * @default 0xFFFFFF
         * @language zh_CN
         */
        set backgroundColor(value: number);
        /**
         * @private
         */
        $setBackgroundColor(value: number): void;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get backgroundColor(): number;
        /**
         * @protected //GYLite
         *
         */
        protected fillBackground(lines?: number[]): void;
        /**
         * Enter the text automatically entered into the input state, the input type is text only and may only be invoked in the user interaction.
         * @version Egret 3.0.8
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 输入文本自动进入到输入状态，仅在类型是输入文本并且是在用户交互下才可以调用。
         * @version Egret 3.0.8
         * @platform Web,Native
         * @language zh_CN
         */
        setFocus(): void;
        /**
         * @private
         *
         */
        $onRemoveFromStage(): void;
        /**
         * @private
         *
         * @param stage
         * @param nestLevel
         */
        $onAddToStage(stage: Stage, nestLevel: number): void;
        $invalidateTextField(): void;
        $getRenderBounds(): Rectangle;
        /**
         * @private
         */
        $measureContentBounds(bounds: Rectangle): void;
        $updateRenderNode(): void;
        /**
         * @private
         */
        private isFlow;
        /**
         * Set rich text
         * @language en_US
         */
        /**
         * 设置富文本
         * @see http://edn.egret.com/cn/index.php/article/index/id/146
         * @language zh_CN
         */
        set textFlow(textArr: Array<egret.ITextElement>);
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        get textFlow(): Array<egret.ITextElement>;
        /**
         * @private
         *
         * @param text
         * @returns
         */
        private changeToPassText;
        /**
         * @protected //GYLite
         */
        protected textArr: Array<egret.ITextElement>;
        /**
         * @private
         *
         * @param textArr
         */
        private setMiddleStyle;
        /**
         * Get the text measured width
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 获取文本测量宽度
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get textWidth(): number;
        /**
         * Get Text measuring height
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 获取文本测量高度
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get textHeight(): number;
        /**
         * @private
         * @param text
         * @version Egret 2.4
         * @platform Web,Native
         */
        appendText(text: string): void;
        /**
         * @private
         * @param element
         * @version Egret 2.4
         * @platform Web,Native
         */
        appendElement(element: egret.ITextElement): void;
        /**
         * @private
         */
        linesArr: Array<egret.ILineElement>;
        $getLinesArr(): Array<egret.ILineElement>;
        /**
         * @private
         *
         * @returns
         */
        $getLinesArr2(): Array<egret.ILineElement>;
        /**
         * @private
         */
        $isTyping: boolean;
        /**
         * @private
         */
        $setIsTyping(value: boolean): void;
        /**
         * @protected GYLite
         * 返回要绘制的下划线列表
         */
        protected drawText(): number[];
        private addEvent;
        private removeEvent;
        private onTapHandler;
        /**是否合批*/
        isBatch(): boolean;
        enableBatch(val: boolean): void;
        stageTextShow(): void;
        stageTextHide(): void;
    }
    interface TextField {
        addEventListener<Z>(type: "link", listener: (this: Z, e: TextEvent) => void, thisObject: Z, useCapture?: boolean, priority?: number): any;
        addEventListener<Z>(type: "focusIn" | "focusOut", listener: (this: Z, e: FocusEvent) => void, thisObject: Z, useCapture?: boolean, priority?: number): any;
        addEventListener(type: string, listener: Function, thisObject: any, useCapture?: boolean, priority?: number): any;
    }
}
