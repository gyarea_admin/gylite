module GYLite
{
		
		
	/**IDragger是一个拖动接口，实现此接口，可通过调用DragManager.setDrag设定为可拖动对象
	 * */
	export interface IDragger extends IGYDisplay
	{
		/**获取拖动时显示的位图*/getBitmapData():egret.Texture;
		/**获取数据*/getData():any
		/**锁定相对鼠标的位置0不锁定 1中心 2左上 3右上 4左下 5右下，参考DragManager的常量定义*/
		isLockCenter():number;
		/**获取与其绑定的DraggerHandle*/getHandle():DraggerHandle;
		/**记录与其绑定的DraggerHandle*/setHandle(val:DraggerHandle):void;
		/**拖动时位图的跟随,这是每帧调用的函数，不断修改draggerShape位置达到效果，返回false将使用默认的方式跟随(固定位置)*/
		draggingSet(draggerShape:any):boolean;//draggerShape:egret.Shape
		/**自定义绘制拖动的Shape，拖动前执行，当返回true，则自定义要绘制的图案，getBitmapData()将不使用，返回false将使用getBitmapData()提供的位图进行绘制*/
		draggingDraw(draggerShape:any):boolean;//draggerShape:egret.Shape
		/**拖动结束*/
		dragStop():void;
		//		getOffsetX():number;
		//		getOffsetY():number;
	}
}