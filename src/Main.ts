class Main extends egret.DisplayObjectContainer 
{
	private uiSprite:GYLite.GYUIComponent;
	private tab:GYLite.GYTab;
	private textGrp:GYLite.TextGroup;		
	private buttonGrp:GYLite.ButtonGroup;
	private layoutGrp:GYLite.LayoutGroup;
	private canvasGrp:GYLite.CanvasGroup;
	private otherGrp:GYLite.OtherGroup;
	public excelGrp:GYLite.ExcelGroup;
	private win:GYLite.GYTitleWindow;		
	private tabList:Array<any>;
	private _skinData:Array<any>;		
	private _myLoader:GYLite.GYLoader=new GYLite.GYLoader(3,1);
	private _resCount:number=0;
	private _resMax:number=0;
	public static skinTheme:GYLite.ISkinTheme;
	public static instance:Main;
	
	private static backBitData:egret.Texture;
	public static logoData:egret.Texture;
	
	public constructor()
	{
		super();			
		var s = this;
		
		if(s.stage)
			s.projInit();
		else
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		// s.filters = [new GYLite.GYDefaultFilter];
		egret.web.EgretShaderLib.default_vert = GYLite.GYShader.getD2VertSrc();
		egret.web.EgretShaderLib.texture_frag = GYLite.GYShader.getD2FragSrc();
	}
	private addToStage(e:egret.Event):void
	{ 
		//this.stage.dirtyRegionPolicy = egret.DirtyRegionPolicy.ON;			
		this.projInit();
	}
	private projInit():void
	{			
		var s = this;
		s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage, s);
		// s.stage.scaleMode = egret.Capabilities.isMobile?egret.StageScaleMode.SHOW_ALL:egret.StageScaleMode.NO_SCALE;						
		GYLite.GYSprite.Init(s.stage, new GYLite.GYSkinTheme);		
		Main.instance = this;
		egret.web.textAtlasRenderEnable = true;
		s._myLoader.loadData("GYLite_res",s.resLoadComp,s,s.resProg,GYLite.LoadType.TYPE_BINARY,"get").save = false;
		// GYLite.LoadUI.getInstance().show(s.stage);
	}
	public get myLoader():GYLite.GYLoader
	{var s = this;
		return s._myLoader;
	}
	private resProg(e:egret.Event):void{
		let l:GYLite.SeqURLLoader = e.currentTarget;			
		// GYLite.LoadUI.getInstance().setProg(l.progressData.loaded, l.progressData.total);
	}
	private resLoadComp(loadInfo:GYLite.LoadInfo):void
	{var s = this;			
		if(loadInfo.content == null)
		{
			console.log(loadInfo.msg);
			return;
		}

		s._myLoader.loadGYCompressBytes(loadInfo.content.res,s.resUncompressComp,s,null,s.resUncompressPorg);
	}	
	private resUncompressPorg(l:GYLite.CompressLoadInfo):void
	{
		// GYLite.LoadUI.getInstance().setProg(l.resMax - l.resCount, l.resMax, "解压中...");
	}
	private resUncompressComp(l:GYLite.CompressLoadInfo):void
	{let s = this;
		// GYLite.LoadUI.getInstance().setProg(l.resMax - l.resCount, l.resMax, "解压中...");
		if(l.resCount == 0)
		{
			// GYLite.LoadUI.getInstance().hide();
			GYLite.GYSprite.skinTheme = new GYLite.SanGuoTheme;
			s.initUI();
		}
	}
	
	private initUI():void
	{var s = this;			
		if(window["globalLoad"])
			window["globalLoad"].removeLoadingUI();
		s.uiSprite = new GYLite.GYUIComponent;		
		s.uiSprite.enableBatch(true);
		s.uiSprite.setBatchAtlasName("Img/ui.png");
		s.uiSprite.width = s.stage.stageWidth;
		s.uiSprite.height = s.stage.stageHeight;
		s.addChild(s.uiSprite);
		if(0)
		{				
			s.test1();
			return;
		}
		s.win = new GYLite.GYTitleWindow;

		if(Main.logoData == null)
		{
			Main.logoData = GYLite.GYLoader.getRes("logo","Img/ui.png").refRes(s.win);
			Main.backBitData = GYLite.GYLoader.getRes("Img/5.jpg").refRes(s.win);
		}			
		
		// var back:GYLite.GYScaleSprite = new GYLite.GYScaleSprite(Main.logoData);
		// back.width = s.uiSprite.width;
		// back.height = s.uiSprite.height;
		// back.touchEnabled = true;
		// back.addEventListener(egret.TouchEvent.TOUCH_TAP, function(e:egret.TouchEvent):void{
		// 	s.win.show(s.uiSprite);
		// }, s);
		// back.mode = GYLite.ScaleMode.REPEAT;
		// if(s.stage.stageWidth < s.stage.stageHeight)
		// {
		// 	back.width = s.stage.stageWidth;
		// 	back.height = s.stage.stageWidth / 1920 * 1080;
		// }
		// else
		// {
		// 	back.height = s.stage.stageHeight;
		// 	back.width = s.stage.stageHeight / 1080 * 1920;
		// }
		// s.uiSprite.addElement(back);			
		
				
		s.win.setTitle("羔羊引擎");
		s.win.setIcon(Main.logoData);
		s.win.icon.width = 18;
		s.win.icon.height = 18;
		s.win.icon.x = 10;
		s.win.icon.y = 6;
		s.win.title.size = 14;
		s.win.title.color = GYLite.ColorConst.titleColor;
		s.win.title.x = 30;
		s.win.title.y = 5;
		s.win.width = s.stage.stageWidth;
		s.win.height = s.stage.stageHeight;
		s.win.y = 0;
		s.win.closeBtn.right = 0;
		s.win.closeBtn.y = 0;
		s.win.x = 0;
		s.win.show(s.uiSprite);		
		s.win.canDrag = true;			

		// GYLite.StatusUtil.getInstance().show();
		// GYLite.StatusUtil.getInstance().statusSprite.x = s.stage.stageWidth - 190;
		
		s.tabList = [];
		s.tabList.push([new GYLite.TabData("Text",GYLite.TextGroup, s.win),new GYLite.TabData("List",GYLite.GameListGroup, s.win),
			new GYLite.TabData("Button",GYLite.ButtonGroup, s.win),new GYLite.TabData("Layout",GYLite.LayoutGroup, s.win),
			new GYLite.TabData("Canvas",GYLite.CanvasGroup, s.win),new GYLite.TabData("Other",GYLite.OtherGroup, s.win),
			new GYLite.TabData("Excel",GYLite.ExcelGroup, s.win),
			new GYLite.TabData("Render",GYLite.RenderGroup, s.win),
			new GYLite.TabData("TextRd",GYLite.TextRenderGroup, s.win)]);
		
		s.tab =new GYLite.GYTab(0,0,s.createTab,s);
		s.tab.x = 150;
		s.tab.y = 30;
		s.win.addElement(s.tab);
		s.tab.width = 700;
		s.tab.height = 40;
		s.tab.boxW = 63;
		s.tab.boxH = 30;
		s.tab.dataProvider = s.tabList;
		s.tab.addEventListener(GYLite.GYViewEvent.SELECTED, s.setTab,s);
		s.tab.selectedPos(0,0);
		s.setTip(s.tab, "导航条，GYTab", -100);
		// egret.log(navigator.userAgent);
		// egret.log("运行类型："+egret.Capabilities.runtimeType,"手机："+egret.Capabilities.isMobile,"渲染类型："+egret.Capabilities.renderMode)
		// egret.log("w:" + egret.Capabilities.boundingClientWidth,"h:" + egret.Capabilities.boundingClientHeight)
		// var str:string = "<font size='14' color='0xff0f0f'><b>html文本</b>，HTML在渲染文本方面的强大。我们要做的也许没有HTML渲染文本的那么完整的功能，但我们却在易用性方面做出很大的努力，可以说类似CSS样式的方式，但编写代码的角度来看，比CSS更简单易用</font>";
		// var tf:GYLite.RichLabel = new GYLite.RichLabel;			
		// tf.width = 120;
		// tf.size = 20;
		// tf.indent = 24;	
		// tf.htmlText = str;			
		// egret.log(tf.cutLastLine());
		// s.addChild(tf);			
		
		
		
		// GYLite.CommonUtil.addStageLoop(function(t:number):void{
		// 	s._testBmp.hitTestPoint(30,30,true);
		// },s)
		// s._myLoader.httpSend("https://wx.wit-learn.com/subProgStudentApi/userinfo/checkIsExistManyAccountByLoginId?loginId=k1Test11&userType=lk",function(l:GYLite.LoadInfo):void{
		// 	if(l && l.content)
		// 	{
		// 		eval("console.log(l.content.res)");
		// 	}
		// },s,GYLite.LoadType.TYPE_TEXT,"GET");
		// egret.ExternalInterface.addCallback("sendToJS", function (message:string) {
		// 	console.log("message form native : " + message);//message form native : message from native
		// });
		// egret.ExternalInterface.call("sendToNative", "你好，王宣");
		// SoundManager.instance.play("sound/backsound.mp3",0,0);
		GYLite.GYSprite.stage.addEventListener(egret.TouchEvent.TOUCH_TAP,s.f,s);
	}
	private _testBmp:GYLite.GYImage;
	private test():void{
		console.log(2);
	}
	private createTab():GYLite.IItemRender
	{var s = this;
		var t:GYLite.IItemRender = new GYLite.GameTabItemRender;
		t.owner = s.tab;
		return t;
	}
	private setTab(e:GYLite.GYViewEvent):void
	{var s = this;
		// egret.log(s.tab.selGroup,s.tab.lastGroup);
	}
	private linf(linf:egret.TextEvent):void
	{var s = this;
		// egret.log(s.linf.text);
	}
	private f(e:egret.Event):void
	{var s = this;
	}
	private mMove(e:egret.TouchEvent):void
	{var s = this;
	}
	private mUp(e:egret.TouchEvent):void
	{var s = this;
	}
	
	private getSkinData():Array<any>
	{var s = this;
		return s._skinData;
	}
	public setTip(sp:GYLite.GYSprite, tip:string, offsetX:number = 20, offsetY:number = 0):void
	{var s = this;
		sp.toolTipString = tip;
		sp.toolTipOffsetX = sp.width + offsetX;
		sp.toolTipOffsetY = offsetY;
		sp.followTarget = sp;
	}


	/**拖拽Group测试*/
	public grp:GYLite.GYGroup;
	public leftTxt:GYLite.GYText;
	public topTxt:GYLite.GYText;
	public bottomTxt:GYLite.GYText;
	public rightTxt:GYLite.GYText;
	public static outSp:GYLite.GYSprite
	private test1():void
	{let s = this;						
		//以下滚动测试
		Main.outSp = new GYLite.GYSprite;
		let b:egret.Texture = GYLite.GYLoader.getRes("3","Img/ui.png").refRes(s.win);
		let getTxt:Function =function(tx:number,ty:number,index:number=0,str:string = null):GYLite.GYText
		{
			let txt:GYLite.GYText = new GYLite.GYText;
			txt.size = 16;
			txt.color = 0;
			txt.text = str?str:String.fromCharCode(Math.random()*30000|0) + index;
			// txt.height = 30;                    
			txt.color = 0xff00ff;
			// txt.underline = true;
			txt.x = tx;
			txt.y = ty;				
			return txt;
		}			
		if(1)
		{
			// let socket:GameSocket = new GameSocket;
			// socket.regMsg(GameSocket.SOCKET_STATE,function(data){
			// 	socket.sendMsg("10000",1);
			// },this,new SocketConnetRet)
			// let stroke:number=2;
			// let txt:GYLite.GYText = getTxt(500,0,0,"");
			// txt.batchAtlasName = "Img/ui.png";
			// txt.text = "测试文本";
			// txt.stroke = stroke;

			let txt2:GYLite.GYText;
			// txt2 = getTxt(700,0,0,"");
			// txt2.batch = true;
			// txt2.htmlText = "<font color='#ff00ff'>测试</font><font color='#ffff00'>文本</font>";
			let i:number,len:number;			
			
			
			// txt2.stroke = stroke;

			// let txt3:GYLite.GYText = getTxt(0,700,0,"");
			// txt3.batchAtlasName = "Img/ui.png";
			// txt3.text = "测试文本";
			// txt3.stroke = stroke;

			// let txt4:GYLite.GYText = getTxt(0,700,0,"");
			// txt4.batch = false;
			// txt4.htmlText = "<font color='#ff00ff'>测试</font><font color='#ffff00'>文本</font>";
			// txt4.stroke = stroke;
			
			let tex:egret.Texture,tex2:egret.Texture,tex3:egret.Texture;
			let img1:GYLite.GYImage,img2:GYLite.GYImage;
			let sprite:GYLite.GYSprite;
			
			let arr:number[]=[400,400];
			
			// arr = [400, 500, 380, 537.3859522651596];
			// len = 12;
			// for(i=2;i<len;i+=2)
			// {
			// 	arr[i] = arr[i-2] + (-50 + 100*Math.random());
			// 	arr[i+1] = arr[i-1] + (-50 + 100*Math.random());
			// }
			// console.log(arr);
			let g:GYLite.GYGraphics;
			// sprite = new GYLite.GYSprite;
			// sprite.x = 0;
			// sprite.y = 0;
			// g = sprite.graphics;
			// g.lineStyle(5,0xff00ff,1,false,"normal","round");
			// g.moveTo(5,400);
			// // g.lineTo(805,400);
			// len = arr.length;
			// for(i=0;i<len;i+=2)
			// {				
			// 	g.lineTo(arr[i],arr[i+1]);
			// }
			
			// GYLite.GYSprite.stage.addChild(sprite);

			sprite = new GYLite.GYSprite;
			sprite.setBatchAtlasName("Img/ui.png");
			sprite.enableBatch(true);
			sprite.x = 0;
			sprite.y = 0;		
			sprite.alpha = 1;	
			g = <GYLite.GYGraphics>sprite.graphics;			
			// g.lineBitmapStyle(10,1,GYLite.GYLoader.getRes("4","Img/ui.png").res);
			
			// g.moveTo(550,400);			
			// g.lineTo(650,645);
			// g.lineStyle(5,0xff0000,1,false,"normal","round");
			// g.moveTo(550,400);			
			// g.lineTo(650,500);
			// g.lineStyle(1,0x00ff00,1,false,"normal","round");
			// g.moveTo(500,400);
			// g.lineTo(600,400);
			// g.moveTo(600,400);						
			// g.lineTo(500,380);
			let agl:number=0;
			let padX:number,padY:number,cos:number,sin:number;
			
			
			// len = 36;
			// for(i=0;i<len;++i)
			// {
			// 	agl = (i*10)*GYLite.MathConst.ROTATION_ANGLE + GYLite.MathConst.HALF_PI;
			// 	cos = Math.cos(agl);
			// 	sin = Math.sin(agl);
			// 	padX = 622 + cos * 80;
			// 	padY = -123 + sin * 80;
			// 	g.moveTo(padX + 750,padY + 400);
			// 	g.lineTo(padX + 750+cos*100,padY + 400+sin*100);
			// }
			
			// g.lineTo(551,700);
			// g.lineTo(130,720);
			// g.curveTo(20,200,400,500);
			// g.cubicCurveTo(20,400,220,300,1225,700);
			// g.drawArc(800,300,200,0,GYLite.MathConst.DOUBLE_PI);			
			let m:egret.Matrix;
			m = new egret.Matrix();
			// m.scale(2,1);
			// m.rotate(Math.PI/4);
			m.translate(0 - 40,500);
			// g.beginGradientFill(GYLite.GradientType.LINEAR.name,[0xffff00,0x00ff00,0xff0000,0x0000ff],[1,1,0.5,1],[0,0.33,0.66,1],m);
			// g.beginGradientFill(GYLite.GradientType.RADIAL.name,[0xffff00,0x00ff00,0xff0000,0x0000ff],[1,1,1,1],[0,0.33,0.66,1],m);
			g.beginBitmapFill(GYLite.GYLoader.getRes("logo","Img/ui.png").res,m,false);
			// g.beginFill(0xffff00, 1);
			// g.drawRoundRect(1+300,1+300,14,14,5,5);
			g.drawRect(0,500,76-40,200);
			// g.drawCircle(300,400,100);
			// g.drawEllipse(300,700,200,100);		
			// g.drawPoly([404,107,272,511,512,283,255,591,774,205,512,147]);
			// g.drawTriangle([300,400,550,700,330,500],[0,0,1,0,1,1],[0,1,2]);
			g.endFill();
			GYLite.GYSprite.stage.addChild(sprite);	

			// GYLite.TimeManager.timeOut(function():void{
			// 	GYLite.GYSprite.stage.removeChild(sprite);	
				// g.clear();
				// // g.lineStyle(2,0xffff00,1,false,"normal","round");
				// g.lineGradientStyle(3,GYLite.GradientType.LINEAR.name,[0xfff0f0,0x0ffff0,0xff0ff0,0xff00ff],[1,1,1,1],[0,0.33,0.66,1]);
				// g.beginGradientFill(GYLite.GradientType.RADIAL.name,[0xfff0f0,0x0ffff0,0xff0ff0,0xff00ff],[1,1,1,1],[0,0.33,0.66,1]);
				// g.drawRect(500,500,300,200);
			// },s,5000);
			// GYLite.TimeManager.timeOut(function():void{
			// 	GYLite.GYSprite.stage.addChild(sprite);	
			// },s,10000);
			
			// let str:string = "羔羊引擎是一个开发利器，如何利，羔羊引擎是一个开发利器，如何利，羔羊引擎是一个开发利器，如何利，羔羊引擎是一个开发利器，如何利，羔羊引擎是一个开发利器，如";
			// len = 20;
			// for(i=0;i<len;++i)
			// {
			// 	let ind = Math.random()*str.length - 4;
			// 	txt2 = getTxt(Math.random()*700,Math.random()*500,i,str.substring(ind,ind+4));
			// 	sprite.addElement(txt2);
			// }
			var testPath:number[]=[];
			// this.stage.addEventListener(egret.TouchEvent.TOUCH_BEGIN,function(e:egret.TouchEvent):void{
			// 	// testPath = [404,107,272,511,512,283,255,591,774,205,512,147];
			// 	// g.drawPoly(testPath);
			// 	if(testPath.length / 2 == 6)
			// 	{
			// 		g.drawPoly(testPath);					
			// 		console.log(testPath.toString());
			// 		// g.lineTo(testPath[0],testPath[1]);
					
			// 		return;
			// 	}
				
			// 	let x:number,y:number;
			// 	x = sprite.mouseX;
			// 	y = sprite.mouseY;
			// 	testPath.push(x,y);
			// 	// if(testPath.length == 2)				
			// 	// 	g.moveTo(x,y);
			// 	// else
			// 	// 	g.lineTo(x,y);					
			// }, this);
			// arr = [499, 279,
			// 	498.5, 284.1132043470137,
			// 	496.60041542691243, 288.73650819177783,
			// 	493.84127948454045, 292.70124853500226,
			// 	490.2562209040499, 295.8387623773979,
			// 	486.0054695347655, 298 + 0.5,
			// 	481.24925522601245, 299 + 0.5,
			// 	480, 299
			// ]	
			// len = arr.length;
			// for(i=0;i<len;i+=2)
			// {				
			// 	if(i==0)
			// 		g.moveTo(500+arr[i],arr[i+1]);
			// 	else
			// 		g.lineTo(500+arr[i],arr[i+1]);
			// 	console.log("test", arr[i],arr[i+1]);
			// }

			// tex = GYLite.GYLoader.getRes("Img/1.png").getRes();				
			// img1 = new GYLite.GYImage;
			// // img1.batchDrawParam = new GYLite.BatchDrawParam(GYLite.BatchDrawType.CIRCLE,tex.textureWidth/2,tex.textureHeight/2,1,1);
			// img1.x = 0;
			// img1.y = 300;
			// // img1.batch = true;				
			// sprite.addElement(img1);
			// img1.source = tex;

			// tex2 = GYLite.GYLoader.getRes("Img/2.png").getRes();
			// img2 = new GYLite.GYImage;
			// // img2.batchDrawParam = new GYLite.BatchDrawParam(GYLite.BatchDrawType.CIRCLE,tex2.textureWidth/2,tex2.textureHeight/2,1,1);
			// img2.x = 300;//img1.x+img1.width;
			// img2.y = 0;
			// // img2.batch = true;				
			// sprite.addElement(img2);				

			// img2.source = tex2;
			

			// var img3:GYLite.GYScaleSprite;
			// tex3 = GYLite.GYLoader.getRes("Img/12.png").getRes();
			// img3 = new GYLite.GYScaleSprite(tex3,new GYLite.Scale9GridRect(80,80,30,30));
			// img3.x = 500;
			// img3.y = 300;	
			// img3.width = 300;
			// img3.height = 300;			
			// sprite.addElement(img3);				
			

			// sprite.addElement(txt);			
			// sprite.addElement(txt3);
			// sprite.addElement(txt2);
			// sprite.addElement(txt4);
			// GYLite.TimeManager.timeOut(function():void{
			// 	txt.htmlText = "";
			// },this,3000);
			// GYLite.TimeManager.timeOut(function():void{
			// 	txt3.htmlText = "";
			// },this,6000);
			// GYLite.TimeManager.timeOut(function():void{
			// 	txt3.htmlText = "<font color='#ff00ff'>吃</font><font color='#ffff00'>薯片薯片薯片薯片薯片薯片</font><font color='#ff00ff'>吗</font>";
			// },this,9000);
			return;
		}
		if(1)
		{
			let sclImg:GYLite.GYScaleSprite;
			let theme:GYLite.SanGuoTheme = <GYLite.SanGuoTheme>GYLite.GYSprite.skinTheme;
			sclImg = new GYLite.GYScaleSprite(theme.progressBD)
			sclImg.scale9GridRect = new GYLite.Scale9GridRect(15,15,10,10);
			// sclImg.mode = GYLite.ScaleMode.REPEAT;
			sclImg.width = 500;
			sclImg.height = 27;
			GYLite.GYSprite.stage.addChild(sclImg);
			let egretSclImg:egret.Bitmap;
			egretSclImg = new egret.Bitmap(theme.progressBD);
			egretSclImg.x = 500;
			egretSclImg.scale9Grid = new egret.Rectangle(15,15,10,10);
			egretSclImg.width = 500;
			egretSclImg.height = 27;
			GYLite.GYSprite.stage.addChild(egretSclImg);
			// let tt:GYLite.GYText = getTxt(200,200);				
			// tt.size = 26;
			// tt.align = "center";
			// tt.text = "A（2，2），B（5，0）C（0，-4），D（-3，0）";
			// // tt.width = 400;
			// let renderTex:egret.RenderTexture;
			// renderTex = new egret.RenderTexture;
			// renderTex.drawToTexture(tt);
			// let img:GYLite.GYImage;
			// img = new GYLite.GYImage;
			// img.source = renderTex;				
			// img.x = 200;
			// img.y = 200;
			// GYLite.GYSprite.stage.addChild(img);
			return;
		}		
		if(1)	
		{				
			let sp:GYLite.GYSprite,sp2:GYLite.GYSprite,sp3:GYLite.GYSprite;
			sp = new GYLite.GYSprite;
			s.uiSprite.addElement(sp);
			sp2 = new GYLite.GYSprite;
			s.uiSprite.addElement(sp2);
			sp3 = new GYLite.GYSprite;
			s.uiSprite.addElement(sp3);
			sp.name= "red";
			sp2.name= "yellow";
			sp3.name= "pink";
			let g:egret.Graphics;
			g = sp.graphics;
			g.beginFill(0xff0000,1);
			g.drawRect(0,0,50,50);
			g.endFill();
			g = sp2.graphics;
			g.beginFill(0xffff00,1);
			g.drawRect(0,0,50,50);
			g.endFill();
			g = sp3.graphics;
			g.beginFill(0xff00ff,1);
			g.drawRect(0,0,50,50);
			g.endFill();
			sp.x = sp.y = 100;
			sp2.x = sp2.y = 110;
			sp3.x = sp3.y = 120;
			s.uiSprite.setElementIndex(sp,2);
			let i:number,len:number;
			let vec:GYLite.IGYDisplay[];
			vec = s.uiSprite.getElementList();
			len = vec.length;
			for(i=0;i<len;++i)
			{
				console.log(vec[i]["name"]);
			}
			return;
		}
		let getImage:Function =function(tx:number,ty:number):GYLite.GYImage
		{
			let img:GYLite.GYImage = new GYLite.GYImage;
			img.width = 30;
			img.height = 30;
			img.x = tx;
			img.y = ty;
			img.source = b;
			return img;
		}
		let loop:Function = function(t:number):void{
			s.grp.scrollRect.y += 5;
			s.grp.scrollRect = s.grp.scrollRect;
		}
		s.grp = new GYLite.GYGroup;
		s.uiSprite.addElement(s.grp);                
		s.grp.clipAndEnableScrolling = true;
		s.grp.canDrag = true;
		s.grp.easeGapY = true;
		s.grp.width = 500;
		s.grp.height = 500;
		s.grp.touchEnabled = true;
		s.leftTxt = getTxt(-50,300);
		s.leftTxt.text = "右拉刷新";
		s.leftTxt.offLayout = true;
		s.grp.addElement(s.leftTxt);
		s.topTxt = getTxt(300,-30);
		s.topTxt.text = "下拉刷新";
		s.topTxt.offLayout = true;
		s.grp.addElement(s.topTxt);
		s.rightTxt = getTxt(0,300);
		s.grp.addElement(s.rightTxt);
		s.rightTxt.text = "左拉刷新";
		s.rightTxt.offLayout = true;
		s.rightTxt.right = -s.rightTxt.width;                
		s.bottomTxt = getTxt(300,0);
		s.grp.addElement(s.bottomTxt);
		s.bottomTxt.text = "上拉刷新";
		s.bottomTxt.offLayout = true;                
		s.grp.addEventListener(GYLite.GYViewEvent.DRAGEND_VERTICAL, function(e:GYLite.GYViewEvent):void{                                                     
			let eY:number = s.grp.easeOutY;
			if(eY < -30)     
			{
				s.topTxt.text = "松开刷新";
			} 
			else
			{
				s.topTxt.text = "下拉刷新";
			}                       
			if(eY > 30)
			{
				s.bottomTxt.text = "松开刷新";
			} 
			else
			{
				s.bottomTxt.text = "上拉刷新";
			}                       
		},s);
		s.grp.addEventListener(GYLite.GYViewEvent.DRAGEND_HORIZONAL, function(e:GYLite.GYViewEvent):void{
			let eX:number = s.grp.easeOutX;
			if(s.grp.clipX < -50)     
			{
				s.leftTxt.text = "松开刷新";
			} 
			else
			{
				s.leftTxt.text = "右拉刷新";
			}
			if(eX > 50)
			{
				s.rightTxt.text = "松开刷新";
			} 
			else
			{
				s.rightTxt.text = "左拉刷新";
			}    
		},s);
		s.grp.addEventListener(GYLite.GYViewEvent.DRAGSTOP, function(e:GYLite.GYViewEvent):void{
			if(s.grp.clipX < -50)     
			{
				s.grp.easeXFix = 50;
				s.leftTxt.text = "刷新中...";
				setTimeout(function(s:Main):void{
					s.grp.easeXFix = 0;
					s.leftTxt.text = "右拉刷新";
				},5000,s);
			}
			else if(s.grp.clipX > 50)     
			{
				s.grp.easeXFix = 50;
				s.rightTxt.text = "刷新中...";
				setTimeout(function(s:Main):void{
					s.grp.easeXFix = 0;
					s.rightTxt.text = "左拉刷新";
				},5000,s);
			}
			let eY:number = s.grp.easeOutY;
			if(eY < -30)     
			{
				s.grp.easeYFix = 30;
				s.topTxt.text = "刷新中...";
				setTimeout(function(s:Main):void{
					s.grp.easeYFix = 0;
					s.topTxt.text = "下拉刷新";
				},5000,s);
			}
			if(eY > 30)
			{
				s.grp.easeYFix = 30;
				s.bottomTxt.text = "刷新中...";
				setTimeout(function(s:Main):void{
					s.grp.easeYFix = 0;
					s.bottomTxt.text = "下拉刷新";
				},5000,s);
			}                        
		},s);                
						
		let i:number,len:number;
		len = 1000;
		s.bottomTxt.y = len * 30;
		for(i=0;i<len;++i)
		{
			let sp:GYLite.GYSprite = new GYLite.GYSprite;
			sp.addElement(getTxt(0,0,i));
			sp.addElement(getImage(150,0));
			sp.addElement(getImage(200,0));
			sp.addElement(getImage(350,0));
			sp.addElement(getImage(400,0));
			sp.addElement(getImage(450,0));
			let g:egret.Graphics=sp.graphics;
			g.clear();
			g.beginFill(i%2==0?0x999000:0x000999);
			g.drawRect(0,0,500,30);
			g.endFill();
			sp.x = 0;
			sp.y = i*30;
			sp.height = 30;
			sp.width = 500;
			s.grp.addElement(sp)
		}				
		// GYLite.StatusUtil.getInstance().show();
		// GYLite.StatusUtil.getInstance().statusSprite.x = s.stage.stageWidth - 190;
	}
}	