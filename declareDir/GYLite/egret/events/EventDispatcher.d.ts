declare namespace egret {
    /**
     * The EventDispatcher class is the base class for all classes that dispatchEvent events. The EventDispatcher class implements
     * the IEventDispatcher interface and is the base class for the DisplayObject class. The EventDispatcher class allows
     * any object on the display list to be an event target and as such, to use the methods of the IEventDispatcher interface.
     * Event targets are an important part of the Egret event model. The event target serves as the focal point for how events
     * flow through the display list hierarchy. When an event such as a touch tap, Egret dispatches an event object into the
     * event flow from the root of the display list. The event object then makes its way through the display list until it
     * reaches the event target, at which point it begins its return trip through the display list. This round-trip journey
     * to the event target is conceptually divided into three phases: <br/>
     * the capture phase comprises the journey from the root to the last node before the event target's node, the target
     * phase comprises only the event target node, and the bubbling phase comprises any subsequent nodes encountered on
     * the return trip to the root of the display list. In general, the easiest way for a user-defined class to gain event
     * dispatching capabilities is to extend EventDispatcher. If this is impossible (that is, if the class is already extending
     * another class), you can instead implement the IEventDispatcher interface, create an EventDispatcher member, and write simple
     * hooks to route calls into the aggregated EventDispatcher.
     * @see egret.IEventDispatcher
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/events/EventDispatcher.ts
     * @language en_US
     */
    /**
     * EventDispatcher 是 Egret 的事件派发器类，负责进行事件的发送和侦听。
     * 事件目标是事件如何通过显示列表层次结构这一问题的焦点。当发生鼠标单击、触摸或按键等事件时，
     * 框架会将事件对象调度到从显示列表根开始的事件流中。然后该事件对象在显示列表中前进，直到到达事件目标，
     * 然后从这一点开始其在显示列表中的回程。在概念上，到事件目标的此往返行程被划分为三个阶段：
     * 捕获阶段包括从根到事件目标节点之前的最后一个节点的行程，目标阶段仅包括事件目标节点，冒泡阶段包括回程上遇到的任何后续节点到显示列表的根。
     * 通常，使用户定义的类能够调度事件的最简单方法是扩展 EventDispatcher。如果无法扩展（即，如果该类已经扩展了另一个类），则可以实现
     * IEventDispatcher 接口，创建 EventDispatcher 成员，并编写一些简单的映射，将调用连接到聚合的 EventDispatcher 中。
     * @see egret.IEventDispatcher
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/events/EventDispatcher.ts
     * @language zh_CN
     */
    class EventDispatcher extends HashObject implements IEventDispatcher {
        /**
         * create an instance of the EventDispatcher class.
         * @param target The target object for events dispatched to the EventDispatcher object. This parameter is used when
         * the EventDispatcher instance is aggregated by a class that implements IEventDispatcher; it is necessary so that the
         * containing object can be the target for events. Do not use this parameter in simple cases in which a class extends EventDispatcher.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 创建一个 EventDispatcher 类的实例
         * @param target 此 EventDispatcher 所抛出事件对象的 target 指向。此参数主要用于一个实现了 IEventDispatcher 接口的自定义类，
         * 以便抛出的事件对象的 target 属性可以指向自定义类自身。请勿在直接继承 EventDispatcher 的情况下使用此参数。
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        constructor(target?: IEventDispatcher);
        /**
         * @private
         */
        $EventDispatcher: Object;
        /**
         * @private
         *
         * @param useCapture
         */
        $getEventMap(useCapture?: boolean): any;
        /**
         * @inheritDoc
         * @version Egret 2.4
         * @platform Web,Native
         */
        addEventListener(type: string, listener: Function, thisObject: any, useCapture?: boolean, priority?: number): void;
        /**
         * @inheritDoc
         * @version Egret 2.4
         * @platform Web,Native
         */
        once(type: string, listener: Function, thisObject: any, useCapture?: boolean, priority?: number): void;
        /**
         * @private
         */
        $addListener(type: string, listener: Function, thisObject: any, useCapture?: boolean, priority?: number, dispatchOnce?: boolean): void;
        $insertEventBin(list: any[], type: string, listener: Function, thisObject: any, useCapture?: boolean, priority?: number, dispatchOnce?: boolean): boolean;
        /**
         * @inheritDoc
         * @version Egret 2.4
         * @platform Web,Native
         */
        removeEventListener(type: string, listener: Function, thisObject: any, useCapture?: boolean): void;
        $removeEventBin(list: any[], listener: Function, thisObject: any): boolean;
        /**
         * @inheritDoc
         * @version Egret 2.4
         * @platform Web,Native
         */
        hasEventListener(type: string): boolean;
        /**
         * @inheritDoc
         * @version Egret 2.4
         * @platform Web,Native
         */
        willTrigger(type: string): boolean;
        /**
         * @inheritDoc
         * @version Egret 2.4
         * @platform Web,Native
         */
        dispatchEvent(event: Event): boolean;
        /**
         * @private
         */
        $notifyListener(event: Event, capturePhase: boolean): boolean;
        /**
         * Distribute a specified event parameters.
         * @param type The type of the event. Event listeners can access this information through the inherited type property.
         * @param bubbles Determines whether the Event object bubbles. Event listeners can access this information through
         * the inherited bubbles property.
         * @param data {any} data
         * @param cancelable Determines whether the Event object can be canceled. The default values is false.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 派发一个指定参数的事件。
         * @param type {string} 事件类型
         * @param bubbles {boolean} 确定 Event 对象是否参与事件流的冒泡阶段。默认值为 false。
         * @param data {any} 事件data
         * @param cancelable {boolean} 确定是否可以取消 Event 对象。默认值为 false。
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        dispatchEventWith(type: string, bubbles?: boolean, data?: any, cancelable?: boolean): boolean;
    }
}
declare namespace egret.sys {
    /**
     * @private
     * 事件信息对象
     */
    interface EventBin {
        type: string;
        /**
         * @private
         */
        listener: Function;
        /**
         * @private
         */
        thisObject: any;
        /**
         * @private
         */
        priority: number;
        /**
         * @private
         */
        target: IEventDispatcher;
        /**
         * @private
         */
        useCapture: boolean;
        /**
         * @private
         */
        dispatchOnce: boolean;
    }
}
