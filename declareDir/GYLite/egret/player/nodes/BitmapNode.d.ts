declare namespace egret.sys {
    /**
     * @private
     * 位图渲染节点
     */
    class BitmapNode extends RenderNode {
        constructor();
        /**
         * 要绘制的位图
         */
        image: BitmapData;
        /**
         * 控制在缩放时是否对位图进行平滑处理。
         */
        smoothing: boolean;
        /**
         * 相对偏移矩阵。
         */
        matrix: egret.Matrix;
        /**
         * 图片宽度。WebGL渲染使用
         */
        imageWidth: number;
        /**
         * 图片高度。WebGL渲染使用
         */
        imageHeight: number;
        /**
         * 使用的混合模式
         */
        blendMode: number;
        /**
         * 相对透明度
         */
        alpha: number;
        /**
         * 颜色变换滤镜
         */
        filter: ColorMatrixFilter;
        /**
         * 翻转
         */
        rotated: boolean;
        /**
         * 绘制一次位图
         */
        drawImage(sourceX: number, sourceY: number, sourceW: number, sourceH: number, drawX: number, drawY: number, drawW: number, drawH: number): void;
        /**
         * 在显示对象的$updateRenderNode()方法被调用前，自动清空自身的drawData数据。
         */
        cleanBeforeRender(): void;
        static $updateTextureData(node: sys.NormalBitmapNode, image: BitmapData, bitmapX: number, bitmapY: number, bitmapWidth: number, bitmapHeight: number, offsetX: number, offsetY: number, textureWidth: number, textureHeight: number, destW: number, destH: number, sourceWidth: number, sourceHeight: number, fillMode: string, smoothing: boolean): void;
        /**
         * @private
         * 绘制九宫格位图
         */
        static $updateTextureDataWithScale9Grid(node: sys.NormalBitmapNode, image: BitmapData, scale9Grid: egret.Rectangle, bitmapX: number, bitmapY: number, bitmapWidth: number, bitmapHeight: number, offsetX: number, offsetY: number, textureWidth: number, textureHeight: number, destW: number, destH: number, sourceWidth: number, sourceHeight: number, smoothing: boolean): void;
        /**
         * @private
         */
        private static drawClipImage;
    }
}
