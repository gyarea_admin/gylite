module GYLite
{
	/**富文本的图片单元 @see GYRichText*/
	export class RichImage extends GYLoadImage implements IPoolObject
	{
		public owner:GYRichText;
		public constructor()
		{
			super();			
		}
		public get inPool():boolean
		{
			let s = this;
			return s._inPool;
		}
		public set inPool(val:boolean)
		{
			let s = this;
			s._inPool = val;
		}
		public outPoolInit():void
		{
			let s = this;
			
		}
		private _inPool:boolean;		
		/**清理回收*/
		public clear():void
		{
			let s = this;
			if(s._inPool)return;
			s.owner.imgGrp.removeElement(this);
			s.source = null;
			// RichImage._pool.push(this);
			PoolUtil.toPool(s,RichImage)
		}
		private static _pool:RichImage[];		
		/**获取图片实例 
		 * @param pr 父级对象
		 * @param posX s.x坐标
		 * @param posY s.y坐标
		 * @param param 参数*/
		public static getInstance(pr:GYRichText,posX:number,posY:number,param:string):RichImage
		{
			var richImg:RichImage;
			// if(!RichImage._pool)
			// {				
			// 	RichImage._pool=new Array<RichImage>();
			// }
			// if(RichImage._pool.length==0)
			// 	richImg = new RichImage();
			// else
			// 	richImg = RichImage._pool.pop();
			richImg = <RichImage>PoolUtil.fromPool(RichImage);
			var arr:Array<any> = param.split(",");
			richImg.owner = pr;
			pr.imgGrp.addElement(richImg);
			richImg.x = posX;
			richImg.y = posY;			
			richImg.source = GYRichText.imageList[arr[0]];
			return richImg;
		}
		/**获取图片实例 param url,s.width,s.height*/
		public static getUrlInstance(pr:GYRichText,posX:number,posY:number,param:string):RichImage
		{
			var richImg:RichImage;
			// if(!RichImage._pool)
			// {				
			// 	RichImage._pool=new Array<RichImage>();
			// }
			// if(RichImage._pool.length==0)
			// 	richImg = new RichImage();
			// else
			// 	richImg = RichImage._pool.pop();
			richImg = <RichImage>PoolUtil.fromPool(RichImage);
			var arr:Array<any> = param.split(",");
			richImg.owner = pr;
			pr.imgGrp.addElement(richImg);
			richImg.x = posX;
			richImg.y = posY;
			richImg.source = arr[0];
			return richImg;
		}
	}
}