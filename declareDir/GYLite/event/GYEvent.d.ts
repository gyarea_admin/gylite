declare module GYLite {
    class GYEvent extends egret.Event {
        /**加载完成*/
        static LOAD_COMPLETE: string;
        /**数据刷新完毕触发*/
        static VALUE_COMMIT: string;
        /**全局拖动通知*/
        static GLOABL_DRAG: string;
        data: any;
        constructor(type: string, bubbles?: boolean, cancelable?: boolean, data?: any);
    }
}
