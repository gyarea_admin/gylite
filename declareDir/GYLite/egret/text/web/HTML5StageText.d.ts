declare namespace egret.web {
    /**
     * @classdesc
     * @extends egret.StageText
     * @private
     */
    class HTML5StageText extends EventDispatcher implements StageText {
        /**
         * @private
         */
        private htmlInput;
        /**
         * @private
         */
        constructor();
        /**
         * @private
         */
        $textfield: egret.TextField;
        /**
         * @private
         *
         * @param textfield
         */
        $setTextField(textfield: egret.TextField): boolean;
        /**
         * @private
         */
        private _isNeedShow;
        /**
         * @private
         */
        private inputElement;
        /**
         * @private
         */
        private inputDiv;
        /**
         * @private
         */
        private _gscaleX;
        /**
         * @private
         */
        private _gscaleY;
        /**
         * @private
         *
         */
        $addToStage(): void;
        /**
         * @private
         *
         */
        private _initElement;
        /**
         * @private
         *
         */
        $show(active?: boolean): void;
        activeShowKeyboard(): void;
        /**
         * @private
         *
         */
        private onBlurHandler;
        /**
         * @private
         *
         */
        private onFocusHandler;
        /**
         * @private
         *
         */
        private executeShow;
        /**
         * @private
         */
        $hide(): void;
        /**
         * @private
         */
        private textValue;
        /**
         * @private
         *
         * @returns
         */
        $getText(): string;
        /**
         * @private
         *
         * @param value
         */
        $setText(value: string): boolean;
        /**
         * @private
         *
         */
        private resetText;
        /**
         * @private
         */
        private colorValue;
        $setColor(value: number): boolean;
        /**
         * @private
         *
         */
        private resetColor;
        $onBlur(): void;
        /**
         * @private
         *
         */
        _onInput(): void;
        private setAreaHeight;
        /**
         * @private
         *
         * @param e
         */
        _onClickHandler(e: any): void;
        /**
         * @private
         *
         */
        _onDisconnect(): void;
        /**
         * @private
         */
        private _styleInfoes;
        /**
         * @private
         *
         * @param style
         * @param value
         */
        private setElementStyle;
        /**
         * @private
         *
         */
        $removeFromStage(): void;
        /**
         * 修改位置
         * @private
         */
        $resetStageText(): void;
    }
}
declare namespace egret.web {
    /**
     * @private
     */
    class HTMLInput {
        /**
         * @private
         */
        private _stageText;
        /**
         * @private
         */
        private _simpleElement;
        /**
         * @private
         */
        private _multiElement;
        /**
         * @private
         */
        private _inputElement;
        /**
         * @private
         */
        _inputDIV: any;
        /**
         * @private
         *
         * @returns
         */
        isInputOn(): boolean;
        /**
         * @private
         *
         * @param stageText
         * @returns
         */
        isCurrentStageText(stageText: any): boolean;
        /**
         * @private
         *
         * @param dom
         */
        private initValue;
        /**
         * @private
         */
        _needShow: boolean;
        /**
         * @private
         */
        $scaleX: number;
        /**
         * @private
         */
        $scaleY: number;
        /**
         * @private
         *
         */
        $updateSize(): void;
        /**
         * @private
         */
        private StageDelegateDiv;
        /**
         * @private
         */
        private canvas;
        /**
         * @private
         *
         * @param container
         * @param canvas
         * @returns
         */
        _initStageDelegateDiv(container: any, canvas: any): any;
        private stageTextClickHandler;
        initInputElement(multiline: boolean): void;
        /**
         * @private
         *
         */
        show(): void;
        /**
         * @private
         *
         * @param stageText
         */
        disconnectStageText(stageText: any): void;
        /**
         * @private
         *
         */
        clearInputElement(): void;
        /**
         * @private
         *
         * @param stageText
         * @returns
         */
        getInputElement(stageText: any): HTMLInputElement | HTMLTextAreaElement;
        finishUserTyping: Function;
        /**
         * @private
         */
        blurInputElement(): void;
        /**
         * @private
         */
        disposeInputElement(): void;
    }
}
declare namespace egret.web {
    /**
     * @private
     * 获取
     */
    function $getTextAdapter(textfield: TextField): HTMLInput;
    /**
     * @private
     */
    function $cacheTextAdapter(adapter: HTMLInput, stage: any, container: HTMLDivElement, canvas: any): void;
}
