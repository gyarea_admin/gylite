declare module GYLite {
    class ExcelData {
        static TYPE_NONE: number;
        static TYPE_NUMBER: number;
        static TYPE_STRING: number;
        static TYPE_CURRENCY: number;
        static TYPE_DATE: number;
        protected static date: Date;
        protected _dataType: number;
        protected formatFunc: Function;
        private _value;
        col: number;
        row: number;
        index: number;
        /**格式化为数字*/
        static FormatValueByNumber(val: any): any;
        /**格式化为字符串*/
        static FormatValueByString(val: any): any;
        /**格式化为货币*/
        static FormatValueByCURRENCY(val: any): any;
        /**格式化为日期*/
        static FormatValueByDATE(val: any): any;
        /**无格式*/
        static FormatValueNone(val: any): any;
        set value(val: any);
        /**excel格子的数据*/
        get value(): any;
        set dataType(val: number);
        /**格子的格式,参照ExcelData常量*/
        get dataType(): number;
    }
}
