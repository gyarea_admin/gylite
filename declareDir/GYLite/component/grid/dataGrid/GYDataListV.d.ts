/**
 @author 迷途小羔羊
 2015.11.20
 */
declare module GYLite {
    class GYDataListV extends GYListV {
        protected _gridDataVec: GridData[];
        protected _maxGridHeight: number;
        protected _contentGrp: GYGroup;
        protected _lastEndBox: number;
        protected _gridControler: boolean;
        /**垂直List组件 格子高度自定义(格子高度固定建议使用GYListV) 项数根据高度自适应
         * @param boxCount 初始化项数据 0初始化 非0 则需要外部提供s.gridDataVec的行数据
         * @param getGridFunc():IItemRender 返回自定义格子对象的方法
         * @param setGridFunc(IItemRender, any) 设置格子数据的方法*/
        constructor(boxCount: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        protected initComponent(): void;
        /**是否格子数据的控制器（当GYDataList的gridDataVec引用外部数据源，则当前列表不是gridDataVec的控制器，则其不能修改此数据，否则会造成无法与外部数据同步）*/
        get gridControler(): boolean;
        set gridDataVec(val: GridData[]);
        /**每行的格子信息*/
        get gridDataVec(): GridData[];
        getGridData(val: number): GridData;
        /**@inheritDoc*/
        scrollToIndex(val: number): void;
        /**@inheritDoc*/
        scrollToEndIndex(val: number): void;
        /**@inheritDoc*/
        updateGrid(): void;
        /**@inheritDoc*/
        get dataProvider(): Array<any>;
        set dataProvider(val: Array<any>);
        private clearGridData;
        protected vScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        protected getNowGridData(val: number): GridData;
        /**设置某行高度
         * @param ind 行索引 val 高度值
         * */
        setGridHeight(ind: number, val: number): void;
        /**@inheritDoc*/
        updateGridData(): void;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        static default_boxH: number;
        static default_boxW: number;
    }
}
