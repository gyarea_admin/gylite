module GYLite{
    export class RenderSprite extends GYSprite implements IRenderTestElement{
        private _v:number;
        private _a:number;
        private _fillType:number;
        private _drawType:number;
        private _colors:number[];
        constructor(fillType:number=0,drawType:number=0,tex:egret.Texture=null)
        {
            super();
            let s =this;
            let g:GYGraphics;
            s._fillType = fillType;
            s._drawType = drawType;
            s._colors = [0xff0000,0x00ff00,0x0000ff,0xffff00,0xff00ff,0x00ffff,0xfff000,0x000fff];
            s.setBatchAtlasName("Img/ui.png");
            s._v = 0.5;
            s._a = 0.05;
            g = <GYGraphics>s.graphics;
            if(drawType > 1)
            {
                if(fillType == 0)
                    g.beginFill(s.getRandomColor(), 1);
                else if(fillType == 1)
                    g.beginBitmapFill(tex?tex:GYLite.GYLoader.getRes("13","Img/ui.png").res,null,true);
                else if(fillType == 2)
                    g.beginGradientFill(GYLite.GradientType.LINEAR.name,[0xffff00,0x00ff00,0xff0000,0x0000ff],[1,1,0.5,1],[0,0.33,0.66,1]);
                else if(fillType == 3)
                    g.beginGradientFill(GYLite.GradientType.RADIAL.name,[0xffff00,0x00ff00,0xff0000,0x0000ff],[1,1,1,1],[0,0.33,0.66,1]);
            }                        
            if(drawType == 0)
                s.drawLine();
            else if(drawType == 1)
                s.drawCurve();
            else if(drawType == 2)
                s.drawRect();
            else if(drawType == 3)
                s.drawRoundRect();
            else if(drawType == 4)
                s.drawEllipse();
            else if(drawType == 5)
                s.drawCircle();
            else if(drawType == 6)
                s.drawPoly();
            else if(drawType == 7)
                s.drawGradientLine();
            else if(drawType == 8)
                s.drawTriangles();
            else
                s.drawTriangle();
        }
        private drawGradientLine():void
		{
            let g:GYGraphics;
            let s= this;
            g = <GYGraphics>this.graphics;            
            g.lineGradientStyle(1 + Math.random()*10|0,GYLite.GradientType.LINEAR.name,[0xffff00,0x00ff00,0xff0000,0x0000ff],[1,1,0.5,1],[0,0.33,0.66,1]);
            // g.lineGradientStyle(10,GYLite.GradientType.LINEAR.name,[0xffff00,0x00ff00,0xff0000,0x0000ff],[1,1,1,1],[0,0.33,0.66,1],null,"round","round");
            // g.lineGradientStyle(10,GYLite.GradientType.LINEAR.name,[0xffff00,0x0000ff],[1,1],[0,1],null,"round","round");
            // g.lineStyle(10,0x00ffff,0.5);
            g.moveTo(0,0);
			g.lineTo(200,0);                
            g.lineTo(50,140);
            g.lineTo(0,0);            
		}
        private drawLine():void
		{
            let g:GYGraphics;
            let s= this;
            g = <GYGraphics>this.graphics;
            // g.lineStyle(1 + Math.random()*10|0,s.getRandomColor(),1);            
            g.lineStyle(10,0x00ffff,1,false,null,"square");
            g.moveTo(0,0);
			g.lineTo(100,0);
            // g.lineTo(50,140);
            // g.lineTo(0,0);            
            g.lineTo(50,140);
		}
		private drawCurve():void
		{
            let g:GYGraphics;
            let s = this;
            g = <GYGraphics>this.graphics;
            // g.lineStyle(10,0x00fffff,0.4);
            g.lineStyle(1 + Math.random()*10|0,s.getRandomColor(),0.5)
            g.moveTo(0,0);
			g.cubicCurveTo(100,-50,200,-100,300,100);
		}
		private drawRect():void
		{
			let g:GYGraphics;
            g = <GYGraphics>this.graphics;
			g.drawRect(0,0,200,100);
		}
		private drawRoundRect():void
		{
            let g:GYGraphics;
            g = <GYGraphics>this.graphics;
			g.drawRoundRect(0,0,200,150,20);
            g.endFill();
		}
        private drawCircle():void
        {
            let g:GYGraphics;
            g = <GYGraphics>this.graphics;
			g.drawCircle(0,0,5);
        }
        private drawEllipse():void
        {
            let g:GYGraphics;
            g = <GYGraphics>this.graphics;
			g.drawEllipse(0,0,200,100);
            g.endFill();
        }
		private drawTriangle():void
		{
            let g:GYGraphics;
            g = <GYGraphics>this.graphics;
			g.drawTriangle([0,0,120,0,60,168],[0,0,1,0,1,1],[0,1,2]);
            g.endFill();
		}
        private drawTriangles():void
		{
            let g:GYGraphics;
            g = <GYGraphics>this.graphics;
			g.drawTriangle([40,0,80,0,120,120,0,120],[0,0,1,0,1,1,0,1],[0,1,2,0,2,3]);
            g.endFill();
		}
		private drawPoly():void
		{
            let g:GYGraphics;
            g = <GYGraphics>this.graphics;
			g.drawPoly([0,58,54,50,79,0,104,50,158,58,120,98,129,153,79,127,29,153,38,98]);
            g.endFill();
		}	
        public getRandomColor():number
        {
            return this._colors[Math.random()*this._colors.length|0];
        }
        	
        public loop(t:number):void
        {
            let s= this;
            s.y += s._v;
            s._v += s._a;            
            if(s._v > 20)
                s._v = 20;
            if(s.y > 800)
            {
                s.dispose();
            }
        }
        public dispose(disposeChild:boolean=true, removeChild:boolean = true, forceDispose:boolean=false):void
        {
            super.dispose(disposeChild,removeChild,forceDispose);            
        }
    }
}