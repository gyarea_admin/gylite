declare module GYLite {
    /**羔羊缓动类*/
    class GYTween {
        private _startTime;
        delay: number;
        updateFunc: (tween: GYTween) => void;
        completeFunc: (tween: GYTween) => void;
        startFunc: (tween: GYTween) => void;
        thisObject: any;
        duration: number;
        target: IResource;
        /**缓动数组*/ tweenArr: TweenData[];
        /**动画组*/ aniArr: IAniData[];
        private _curAniIndex;
        private _intervalId;
        private _isRunning;
        private _isReserve;
        private _completeClear;
        private _loopsCount;
        isPlayEnd: boolean;
        /**是否保持每次播放从起点值开始*/ keepFrom: boolean;
        /**循环播放次数(若要永久循环则-1或者设置一个大数字)，默认1*/
        playLoops: number;
        constructor();
        /**执行tween动画
         * @param isReserve 是否翻转播放，默认false，不翻转
         * @param aniIndex 起始的当前动画组的索引，默认-1，从头开始，即将播放0，也就是传0，则即将播放1
         * */
        run(isReserve?: boolean, aniIndex?: number): void;
        /**运行tween
         * @param isReserve 是否翻转播放
         * @param curAniIndex 当前的动画索引，默认-1，表示从-1开始，即将播放0，也就是0，则即将播放1
        */
        toRun(isReserve?: boolean, curAniIndex?: number): void;
        stop(): void;
        get isRunning(): boolean;
        /**当前进行的动画索引（使用多组动画连播的时候，否则默认为-1）*/
        get curAniIndex(): number;
        private loop;
        clear(removeUse?: boolean): void;
        protected clearTweenData(): void;
        get startTime(): number;
        /**播放完成后自动清理tween*/
        get completeClear(): boolean;
        set completeClear(value: boolean);
        /**是否翻转播放*/
        get isReserve(): boolean;
        /**带速度运动的缓动，tData.param 代表速度值 像素/毫秒，不填则没有初速度，是一个匀加速运动*/
        static speedEase(tData: TweenData, t: GYTween): void;
        /**匀速缓动*/
        static commonEase(tData: TweenData, t: GYTween): void;
        /**加速缓动*/
        static addEase(tData: TweenData, t: GYTween): void;
        /**减速缓动*/
        static reduceEase(tData: TweenData, t: GYTween): void;
        private static _pool;
        private static _useTween;
        /**播放一个缓动，此接口已旧，请用aniTo代替更为方便，且两个接口只能同时用一个，否则会表现异常*/
        static to(target: any, tweenArr: Array<TweenData>, duration: number, delay?: number, thisObject?: any, completeFunc?: (tween: GYTween) => void, startFunc?: (tween: GYTween) => void, updateFunc?: (tween: GYTween) => void, runImmediate?: boolean, isClear?: boolean, loops?: number): GYTween;
        static aniTo(target: any, aniArr: IAniData[], duration: number, delay?: number, thisObject?: any, completeFunc?: (tween: GYTween) => void, startFunc?: (tween: GYTween) => void, updateFunc?: (tween: GYTween) => void, runImmediate?: boolean, isClear?: boolean, loops?: number): GYTween;
        /**销毁GYTween对象
         * @param tar 操控的显示对象
         * */
        static disposeByTarget(tar: any): void;
        /***对已经被销毁的对象的tween进行垃圾回收*/
        static gc(): void;
    }
}
