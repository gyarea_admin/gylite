declare module GYLite {
    interface IRenderTestElement extends IGYDisplay {
        loop(t: number): any;
        get graphics(): egret.Graphics;
    }
}
