/**
 * @author 迷途小羔羊
 * 2019-5-6
 */
declare module GYLite {
    class GYByteArray extends egret.ByteArray {
        constructor(buffer?: ArrayBuffer | Uint8Array);
        /**zlib算法解压 需要引用zlib.min.js*/
        uncompress(algorithm?: String): void;
        /**zlib算法压缩 需要引用zlib.min.js*/
        compress(algorithm?: String): void;
        /**获取字节数据的唯一识别key(此方法只平均取5位数据加长度做粗略区分，可能会存在碰巧一致的情况),0-0.25-0.5-0.75-1-length
         * **/
        static getBytesHash(bytes: any): string;
    }
}
