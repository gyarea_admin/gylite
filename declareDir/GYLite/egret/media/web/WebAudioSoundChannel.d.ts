declare namespace egret.web {
    /**
     * @private
     * @inheritDoc
     */
    class WebAudioSoundChannel extends egret.EventDispatcher implements egret.SoundChannel {
        /**
         * @private
         */
        $url: string;
        /**
         * @private
         */
        $loops: number;
        /**
         * @private
         */
        $startTime: number;
        /**
         * @private
         * audio音频对象
         * @member {any} egret.Sound#audio
         */
        $audioBuffer: AudioBuffer;
        /**
         * @private
         */
        private gain;
        /**
         * @private
         */
        private bufferSource;
        /**
         * @private
         */
        context: any;
        private isStopped;
        /**
         * @private
         */
        constructor();
        initGain(): void;
        /**
         * @private
         */
        private _currentTime;
        /**
         * @private
         */
        private _volume;
        $play(): void;
        stop(): void;
        /**
         * @private
         */
        private onPlayEnd;
        /**
         * @private
         * @inheritDoc
         */
        get volume(): number;
        /**
         * @inheritDoc
         */
        set volume(value: number);
        /**
         * @private
         */
        private _startTime;
        /**
         * @private
         * @inheritDoc
         */
        get position(): number;
    }
}
