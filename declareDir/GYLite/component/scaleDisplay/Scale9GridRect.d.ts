declare module GYLite {
    /**九切片、三切片*/
    class Scale9GridRect {
        /**左边距*/ leftGap: number;
        /**右边距*/ rightGap: number;
        /**上边距*/ topGap: number;
        /**底边距*/ bottomGap: number;
        gridMode: ScaleGridMode;
        /**
         * @param l 左
         * @param r 右
         * @param t 上
         * @param b 下
         * */
        constructor(l?: number, r?: number, t?: number, b?: number, scale3Grid?: ScaleGridMode);
    }
    class ScaleGridMode {
        /**垂直三切片*/ static v3Grid: ScaleGridMode;
        /**水平三切片*/ static h3Grid: ScaleGridMode;
        /**九切片*/ static s9Grid: ScaleGridMode;
    }
}
