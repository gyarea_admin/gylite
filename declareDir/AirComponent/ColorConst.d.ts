declare module GYLite {
    class ColorConst {
        static fontColor: number;
        static inputColor: number;
        static tabColor: number;
        static titleColor: number;
        static itemLabelColor: number;
    }
}
