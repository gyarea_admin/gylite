module GYLite
{
												
			
	export class GYSlider extends GYProgressBar
	{
		protected _sliderX:number;
		protected _sliderY:number;
		protected _scrollStep:number;
		protected _downOffsetX:number;
		protected _downOffsetY:number;
		protected _downX:number;
		protected _downY:number;
		/**格式化显示的文本*/public formatValueFunc:(cur:number,max:number)=>string;
		public constructor(skin:ISliderSkin=null, type:number=0)
		{
			super(skin, type);
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();						
			s._downOffsetX=0;
			s._downOffsetY=0;
			s._downX=NaN;
			s._downY=NaN;
			s._scrollStep = GYSlider.default_scrollStep;
			s.sliderX = GYSlider.default_sliderX;
			s.sliderY = GYSlider.default_sliderY;
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			var sk:ISliderSkin = s._skin as ISliderSkin;
			sk.sliderButton.followTarget = sk.sliderButton;
			s.touchEnabled = true;
		}
		protected addToStage(e:egret.Event):void
		{
			let s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.TouchEvent.TOUCH_BEGIN,s.downSlider,s);
			var sk:ISliderSkin = s._skin as ISliderSkin;
		}
		protected removeFromStage(e:egret.Event):void
		{
			let s = this;
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.removeEventListener(egret.TouchEvent.TOUCH_BEGIN,s.downSlider,s);			
		}
		protected downSlider(e:egret.TouchEvent):void
		{
			let s = this;
			var sk:ISliderSkin = s._skin as ISliderSkin;
			s._downX = s.mouseX;
			s._downY = s.mouseY;
			s._downOffsetX = 0;
			s._downOffsetY = 0;
			GYSprite.addStageDown(e.target as GYSprite,s.upSlider, s);
			CommonUtil.addStageLoop(s.sliderLoop,s);
			s.addEventListener(egret.TouchEvent.TOUCH_END,s.upSlider,s);
			sk.sliderButton.showTip(true);
			if(e.target != sk.sliderButton)
			s.setValueByPos(s._downX);
		}
		protected upSlider(e:egret.TouchEvent=null):void
		{
			let s = this;
			s.removeEventListener(egret.TouchEvent.TOUCH_END,s.upSlider,s);
			CommonUtil.delStageLoop(s.sliderLoop,s);
		}
		protected sliderLoop(t:number):void
		{
			let s = this;
			var val:number;
			if(s._type == GYProgressBar.LEFT_RIGHT || s._type == GYProgressBar.RIGHT_LEFT)
			{
				if(s._downX == s.mouseX)return;
				val = s.mouseX;
			}
			else
			{
				if(s._downY == s.mouseY)return;
				val = s.mouseY;
			}
			s.setValueByPos(val);
		}
		public setValueByPos(mX:number):void
		{
			let s = this;		
			if(s._type == GYProgressBar.LEFT_RIGHT)
				s.setValue((mX - s._downOffsetX - s._barX - s._barMin)/(s._barMax-s._barMin) * (s._max-s._min) + s._min, true);
			else if(s._type == GYProgressBar.RIGHT_LEFT)
			{
				mX += s._barMin;
				s.setValue((1-(mX - s._downOffsetX - s._barX - s._barMin)/(s._barMax-s._barMin)) * (s._max-s._min) + s._min,true);
			}
			else if(s._type == GYProgressBar.UP_DOWN)
				s.setValue((mX - s._downOffsetY - s._barY - s._barMin)/(s._barMax-s._barMin) * (s._max-s._min) + s._min,true);
			else
			{
				mX += s._barMin;
				s.setValue((1-(mX - s._downOffsetY - s._barY - s._barMin)/(s._barMax-s._barMin)) * (s._max-s._min) + s._min,true);
			}
		}
		protected setBar():void
		{
			let s = this;
			if(s._value >= s._max)
				s._value = s._max;
			else
				s._value -= s._value % s._scrollStep;
			s._value = (s._value*1000>>0)/1000;
			s._barVal = Math.floor((s._value-s._min)/(s._max-s._min) * (s._barMax - s._barMin));
			s._skin.barWidth = s._barVal + s._barMin;
			var val:number;
			if(s._type == GYProgressBar.LEFT_RIGHT)
				val = s._sliderX + s._barVal + s._barMin;
			else if(s._type == GYProgressBar.UP_DOWN)
				val = s._sliderY + s._barVal + s._barMin;
			else
			{
				val = s._barMax - s._barVal - s._barMin;
				if(s._type == GYProgressBar.RIGHT_LEFT)
				{
					s._skin.clipX = Math.ceil(val);
					val += s._sliderX;
				}
				else if( s._type == GYProgressBar.DOWN_UP)
				{
					s._skin.clipY = Math.ceil(val);
					val += s._sliderY;
				}
			}
			if(s._skin.rotation == 90)
				(s._skin as ISliderSkin).sliderY = val;
			else
				(s._skin as ISliderSkin).sliderX = val;
			(s._skin as ISliderSkin).sliderButton.toolTipString = s.formatValueFunc!=null?s.formatValueFunc(s._value, s._max):s._value.toString();
		}
		/**滑块初始X坐标*/
		public get sliderX():number
		{
			return this._sliderX;
		}
		public set sliderX(val:number)
		{
			let s = this;
			s._sliderX = (s._skin as ISliderSkin).sliderX = val;
		}
		/**滑块初始Y坐标*/
		public get sliderY():number
		{
			return this._sliderY;
		}
		public set sliderY(val:number)
		{
			let s = this;
			s._sliderY = (s._skin as ISliderSkin).sliderY = val;
		}
		/**分度值*/
		public set scrollStep(val:number)
		{
			this._scrollStep = val;
		}
		public get scrollStep():number
		{
			return this._scrollStep;
		}
		/**自定义皮肤请实现ISliderSkin或继承SliderSkin*/
		protected getThemeSkin():IGYSkin
		{
			return GYSprite.skinTheme.GetSliderSkin()
		}

		public get sliderBtn():GYButton
		{
			return this._skin.sliderButton;
		}
		
		public static default_sliderX:number = 0;
		public static default_sliderY:number = 0;
		public static default_scrollStep:number = 1;

	}
}