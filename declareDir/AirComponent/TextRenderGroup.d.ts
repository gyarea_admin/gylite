declare module GYLite {
    class TextRenderGroup extends GYGroup {
        private _renderSprites;
        private _paused;
        private _curNum;
        private _sp;
        private _curInd;
        constructor();
        private addToStage;
        private removeFromStage;
        private init;
        private tap;
        private run;
        private loop;
        swap(rSp: TextRenderSprite, t: number): void;
        private stop;
        private pause;
        private addRender;
        private addRenders;
        private removeRenders;
    }
}
