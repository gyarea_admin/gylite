module GYLite
{
				
					/**<code>GYShowInput</code>是一个全局公用的输入文本，覆盖在顶层作为临时输入*/
	export class GYShowInput extends GYText
	{
		private static _instance:GYShowInput;
		/**显示输入文本
		 * @param txt 设置文本的对象
		 * @param pr 显示的层级
		 * */
		public static show(txt:GYText,pr:GYSprite):GYShowInput
		{
			if(GYShowInput._instance == null)
			{
				GYShowInput._instance = new GYShowInput;				
			}
				
			if(GYShowInput._instance.parent == null)
				pr.addElement(GYShowInput._instance);	
			GYShowInput._instance.inputSet(txt);
			return GYShowInput._instance;
		}
		private _txtSource:GYText;
		private _deactiveFunc:Function;
		private _deactiveObj:any;
		/**@param mul 是否多行*/
		public constructor(mul:boolean=false)
		{
			super(mul);
			var s = this;
			s.type = egret.TextFieldType.INPUT;
			s.addEventListener(egret.FocusEvent.FOCUS_OUT, s.deactive, s);
		}
		/**设置绑定的文本，输入结束后自动设置文本内容*/
		public inputSet(txt:GYText):void
		{
			let s = this;
			s._txtSource = txt;
			s._txtSource.visible = false;
			s.x = s._txtSource.x;
			s.y = s._txtSource.y;
			s.right = s._txtSource.layoutMode.right;
			s.bottom = s._txtSource.bottom;
			s.verticalCenter = s._txtSource.verticalCenter;
			s.horizonalCenter = s._txtSource.horizonalCenter;
			s.restrict = s._txtSource.restrict;
			s._txtSource.textFormat.setFormat(s);			
			s.text = s._txtSource.text;
			s.width = s._txtSource.width;
			// s.embedFonts = s._txtSource.embedFonts;
			if(!s._txtSource.autoHeight)
				s.height = s._txtSource.height;
			else
				s.autoHeight = s._txtSource.autoHeight;
			s.multiline = txt.multiline;
			s.wordWrap = txt.wordWrap;
			s.setFocus();
			// var end:number = s.text.length;
			// s.setSelection(end,end);
			// s.stage.focus = this;
		}
		public get inputText():string
		{
			return (<any>this).inputUtils.stageText.$getText();
		}
		private deactive(e:egret.Event):void
		{
			let s = this;
			s._txtSource.text = (<any>s).inputUtils.stageText.$getText();
			s._txtSource.visible = true;
			s._txtSource = null;
			(s.parent as GYSprite).removeElement(this);
			if(s._deactiveFunc!=null)
			{
				s._deactiveFunc.call(s._deactiveObj, e);
				s._deactiveFunc = null;
			}			
		}
		public listenFocusOut(func:Function, thisObj:any):void
		{
			let s= this;
			s._deactiveFunc = func;
			s._deactiveObj = thisObj;
		}
		public layout():void
		{
			var s = this;
			super.layout();
			(<any>s).inputUtils.stageText._initElement();
			(<any>s).inputUtils.stageText.$resetStageText();
		}
	}
}