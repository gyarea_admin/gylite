declare namespace egret {
    /**
     * Bitmap font adopts the Bitmap+SpriteSheet mode to render text.
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/text/BitmapText.ts
     * @language en_US
     */
    /**
     * 位图字体采用了Bitmap+SpriteSheet的方式来渲染文字。
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/text/BitmapText.ts
     * @language zh_CN
     */
    class BitmapText extends DisplayObject {
        /**
         * Create an egret.BitmapText object
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 创建一个 egret.BitmapText 对象
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        constructor();
        protected createNativeDisplayObject(): void;
        private $smoothing;
        /**
         * Whether or not is smoothed when scaled.
         * @default true。
         * @version Egret 3.0
         * @platform Web
         * @language en_US
         */
        /**
         * 控制在缩放时是否进行平滑处理。
         * @default true。
         * @version Egret 3.0
         * @platform Web
         * @language zh_CN
         */
        get smoothing(): boolean;
        set smoothing(value: boolean);
        private $text;
        /**
         * A string to display in the text field.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 要显示的文本内容
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get text(): string;
        set text(value: string);
        /**
         * @private
         */
        $setText(value: string): boolean;
        protected $textFieldWidth: number;
        /**
         * @private
         */
        $getWidth(): number;
        /**
         * @private
         */
        $setWidth(value: number): boolean;
        private $textLinesChanged;
        /**
         * @private
         */
        $invalidateContentBounds(): void;
        protected $textFieldHeight: number;
        /**
         * @private
         */
        $getHeight(): number;
        /**
         * @private
         */
        $setHeight(value: number): boolean;
        protected $font: BitmapFont;
        protected $fontStringChanged: boolean;
        /**
         * The name of the font to use, or a comma-separated list of font names, the type of value must be BitmapFont.
         * @default null
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 要使用的字体的名称或用逗号分隔的字体名称列表，类型必须是 BitmapFont。
         * @default null
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get font(): Object;
        set font(value: Object);
        $setFont(value: any): boolean;
        private $lineSpacing;
        /**
         /**
         * An integer representing the amount of vertical space between lines.
         * @default 0
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 一个整数，表示行与行之间的垂直间距量
         * @default 0
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get lineSpacing(): number;
        set lineSpacing(value: number);
        $setLineSpacing(value: number): boolean;
        private $letterSpacing;
        /**
         * An integer representing the amount of distance between characters.
         * @default 0
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 一个整数，表示字符之间的距离。
         * @default 0
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get letterSpacing(): number;
        set letterSpacing(value: number);
        $setLetterSpacing(value: number): boolean;
        private $textAlign;
        /**
         * Horizontal alignment of text.
         * @default：egret.HorizontalAlign.LEFT
         * @version Egret 2.5.6
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 文本的水平对齐方式。
         * @default：egret.HorizontalAlign.LEFT
         * @version Egret 2.5.6
         * @platform Web,Native
         * @language zh_CN
         */
        get textAlign(): string;
        set textAlign(value: string);
        $setTextAlign(value: string): boolean;
        private $verticalAlign;
        /**
         * Vertical alignment of text.
         * @default：egret.VerticalAlign.TOP
         * @version Egret 2.5.6
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 文字的垂直对齐方式。
         * @default：egret.VerticalAlign.TOP
         * @version Egret 2.5.6
         * @platform Web,Native
         * @language zh_CN
         */
        get verticalAlign(): string;
        set verticalAlign(value: string);
        $setVerticalAlign(value: string): boolean;
        /**
         * A ratio of the width of the space character. This value is multiplied by the height of the first character is the space character width.
         * @default 0.33
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 一个空格字符的宽度比例。这个数值乘以第一个字符的高度即为空格字符的宽。
         * @default 0.33
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static EMPTY_FACTOR: number;
        /**
         * @private
         */
        $updateRenderNode(): void;
        /**
         * @private
         */
        $measureContentBounds(bounds: Rectangle): void;
        private $textWidth;
        /**
         * Get the BitmapText measured width
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 获取位图文本测量宽度
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get textWidth(): number;
        private $textHeight;
        /**
         * Get Text BitmapText height
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 获取位图文本测量高度
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get textHeight(): number;
        /**
         * @private
         */
        private $textOffsetX;
        /**
         * @private
         */
        private $textOffsetY;
        /**
         * @private
         */
        private $textStartX;
        /**
         * @private
         */
        private $textStartY;
        /**
         * @private
         */
        private textLines;
        /**
         * @private 每一行文字的宽度
         */
        private $textLinesWidth;
        /**
         * @private
         */
        $lineHeights: number[];
        /**
         * @private
         *
         * @returns
         */
        $getTextLines(): string[];
    }
}
