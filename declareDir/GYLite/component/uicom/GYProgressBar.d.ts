declare module GYLite {
    /**进度条*/
    class GYProgressBar extends GYSkinContainer {
        /**从上到下*/ static UP_DOWN: number;
        /**从下到上*/ static DOWN_UP: number;
        /**从左到右*/ static LEFT_RIGHT: number;
        /**从右到左*/ static RIGHT_LEFT: number;
        protected _value: number;
        protected _max: number;
        protected _min: number;
        protected _barMax: number;
        protected _barMin: number;
        protected _barVal: number;
        protected _type: number;
        protected _barX: number;
        protected _barY: number;
        protected _invalidValue: boolean;
        protected _moveTime: number;
        protected _startTime: number;
        protected _tarVal: number;
        protected _lastVal: number;
        protected _tempVal: number;
        protected _tempVal2: number;
        protected _moveBack: boolean;
        private _commitEvent;
        /**格式化显示的文本 */ formatValueFunc: (cur: number, max: number) => string;
        /**@param s.skin 皮肤请使用ProgressBarSkin 自定义请实现IProgressBarSkin接口
         * @param s.type 类型，进度条的变化方向 GYProgressBar.UP_DOWN等方向
         * */
        constructor(skin?: IProgressBarSkin, type?: number);
        protected initComponent(): void;
        moveTo(val: number): void;
        protected moveLoop(t: number): void;
        /**进度条当前值*/
        set value(val: number);
        get value(): number;
        /**@param commit 设置进度条当前值(会派发VALUECOMMIT通知)*/
        setValue(val: number, commit?: boolean): void;
        /**进度条最大值*/
        set max(val: number);
        get max(): number;
        set min(val: number);
        get min(): number;
        /**进度条最大长度*/
        get barMax(): number;
        set barMax(val: number);
        /**进度条最小长度*/
        get barMin(): number;
        set barMin(val: number);
        protected setBar(): void;
        /**设置进度条方向，请参照GYProgressBar定义的常量，如GYProgressBar.RIGHT_LEFT*/
        set type(val: number);
        get type(): number;
        /**进度条当前长度*/
        get barVal(): number;
        /**文本*/
        get labelDisplay(): GYText;
        /**进度条初始X坐标*/
        get barX(): number;
        set barX(val: number);
        /**进度条初始Y坐标*/
        get barY(): number;
        set barY(val: number);
        /**刷新s.value*/
        invalidValue(): void;
        protected validValue(commit?: boolean): void;
        updateView(): void;
        set moveBack(val: boolean);
        /**是否达到尽头从起点重新开始*/
        get moveBack(): boolean;
        get mode(): number;
        set mode(val: number);
        set label(val: string);
        get label(): string;
        /**进度条动画播放时长*/
        set moveTime(val: number);
        set skin(val: any);
        /**自定义皮肤请实现IProgressBarSkin或继承ProgressBarSkin*/
        protected getThemeSkin(): IGYSkin;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        get barSkin(): GYScaleSprite;
        get backgroundSkin(): GYScaleSprite;
        static default_barX: number;
        static default_barY: number;
        static default_moveTime: number;
        static default_moveBack: boolean;
    }
}
