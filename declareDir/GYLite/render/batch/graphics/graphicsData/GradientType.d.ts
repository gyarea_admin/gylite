declare module GYLite {
    class GradientType {
        /**线性渐变*/ static LINEAR: GradientType;
        /**径向渐变*/ static RADIAL: GradientType;
        private _id;
        private _name;
        constructor(id: number, name: string);
        get id(): number;
        get name(): string;
    }
}
