declare namespace egret {
    /**
     * @private
     */
    class Mesh extends Bitmap {
        constructor(value?: Texture);
        protected createNativeDisplayObject(): void;
        /**
         * @private
         */
        protected setBitmapDataToWasm(data?: Texture): void;
        /**
         * @private
         */
        $updateRenderNode(): void;
        /**
         * @private
         */
        private _verticesDirty;
        private _bounds;
        /**
         * @private
         */
        $updateVertices(): void;
        /**
         * @private
         */
        $measureContentBounds(bounds: Rectangle): void;
    }
}
