/**
@author 迷途小羔羊
2022.09.27
*/
declare module GYLite {
    class BatchTexture extends egret.Texture implements IResource, IPoolObject {
        $disposed: boolean;
        hash: number;
        constructor();
        /**重置参数
         * @param hsCode hash值
         * @param batchElement 合批数据源测量信息
        */
        reset(hsCode: number, batchElement: BatchElement): void;
        get element(): BatchElement;
        get disposed(): boolean;
        dispose(): void;
        protected beforeToPool(): void;
        clear(): void;
        inPool: boolean;
        outPoolInit(): void;
    }
}
