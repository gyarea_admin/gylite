/**
 @author 迷途小羔羊
 2015.3.5
 */
declare module GYLite {
    class GYListH extends GYListBase {
        /**水平List组件
         @inheritDoc*/
        constructor(size: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        protected initComponent(): void;
        set boxs(val: number);
        get boxs(): number;
        protected boxNumChange(update?: boolean): void;
        /**@inheritDoc*/
        scrollToIndex(val: number): void;
        /**@inheritDoc*/
        scrollToEndIndex(val: number): void;
        protected selectLine(data: any, selectedData: any, ctrlKey: boolean, checkNext?: boolean): void;
        updateGrid(): void;
        /**@inheritDoc*/
        get dataProvider(): Array<any>;
        set dataProvider(val: Array<any>);
        protected hScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        /**刷新列表可见的所有项*/
        updateItems(): void;
        protected wheelRoll(e: any): void;
        set height(val: number);
        get height(): number;
        set width(val: number);
        get width(): number;
        /**滚动条 0自动 1显示 2不显示*/
        get scrollerPolicy(): number;
        set scrollerPolicy(val: number);
        protected downGroup(e: egret.TouchEvent): void;
        protected dragLoop(t: number): void;
        protected groupDrag(): void;
        protected groupDragStop(): void;
        scrollToNextItem(): number;
        scrollToPreItem(): number;
        protected scrollEnd(e?: GYScrollerEvent): void;
        clone(): GYListBase;
        copy(list: GYListH): void;
    }
}
