declare module GYLite {
    class MenuItem extends GYLinkButton implements IItemRender {
        protected _data: any;
        protected _owner: IList;
        protected _itemIndex: number;
        constructor(skin?: any);
        /**设置数据*/
        setData(val: any): void;
        /**获得数据*/
        getData(): any;
        /**提取数据*/
        toString(): string;
        get owner(): IList;
        set owner(val: IList);
        get itemIndex(): number;
        set itemIndex(value: number);
        get col(): number;
        set col(value: number);
        get row(): number;
        set row(value: number);
    }
}
