module GYLite
{
		
	export class FilterUtil
	{
		/**黑色描边滤镜*/public static blackGlow:egret.GlowFilter = new egret.GlowFilter(0,1,3,3,6);
		public static blackGlowArr:Array<any>=[FilterUtil.blackGlow];
		/**红色描边滤镜*/public static redGlow:egret.GlowFilter = new egret.GlowFilter(0xff0000,1,3,3,6);
		public static redGlowArr:Array<any>=[FilterUtil.redGlow];
		/**灰度滤镜*/
		public static grayColorMatrix:egret.ColorMatrixFilter = new egret.ColorMatrixFilter([0.3086,0.6094,0.082,0,0,0.3086,0.6094,0.082,0,0,0.3086,0.6094,0.082,0,0,0,0,0,1,0]);
		public static grayColorArr:Array<any>=[FilterUtil.grayColorMatrix];
	}
}