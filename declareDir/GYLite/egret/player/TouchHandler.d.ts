declare namespace egret.sys {
    /**
     * @private
     * 用户交互操作管理器
     */
    class TouchHandler extends HashObject {
        private maxTouches;
        private useTouchesCount;
        /**
         * @private
         */
        constructor(stage: Stage);
        /**
         * @private
         * 设置同时触摸数量
         */
        $initMaxTouches(): void;
        /**
         * @private
         */
        private stage;
        /**
         * @private
         */
        private touchDownTarget;
        /**
         * @private
         * 触摸开始（按下）
         * @param x 事件发生处相对于舞台的坐标x
         * @param y 事件发生处相对于舞台的坐标y
         * @param touchPointID 分配给触摸点的唯一标识号
         * @param button GYLite 按下是键，0左键 1中键 2右键
         */
        onTouchBegin(x: number, y: number, touchPointID: number, button?: number): boolean;
        /**
         * @private
         */
        private lastTouchX;
        /**
         * @private
         */
        private lastTouchY;
        /**
         * @private
         * 触摸移动
         * @param x 事件发生处相对于舞台的坐标x
         * @param y 事件发生处相对于舞台的坐标y
         * @param touchPointID 分配给触摸点的唯一标识号
         * @param buttons GYLite 按下是键，1左键 2右键 4中键 8前进 16 后退
         */
        onTouchMove(x: number, y: number, touchPointID: number, buttons?: number): boolean;
        /**
         * @private
         * 触摸结束（弹起）
         * @param x 事件发生处相对于舞台的坐标x
         * @param y 事件发生处相对于舞台的坐标y
         * @param touchPointID 分配给触摸点的唯一标识号
         * @param button GYLite 按下是键，0左键 1中键 2右键
         */
        onTouchEnd(x: number, y: number, touchPointID: number, button?: number): boolean;
        /**
         * @private
         * 获取舞台坐标下的触摸对象 GYLite 优化stageX stageY判断
         */
        private findTarget;
        /**
         * @private
         * 设置同时触摸数量
         */
        $updateMaxTouches: (value: any) => void;
        static lastStageX: number;
        static lastStageY: number;
        static lastTime: number;
        static lastTarget: any;
    }
}
