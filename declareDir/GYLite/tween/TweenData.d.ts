declare module GYLite {
    class TweenData {
        /**需要tween的属性名称*/
        propertyName: string;
        /**起点值*/
        from: number;
        /**终点值*/
        to: number;
        /**补间函数，一个每帧会调度的函数，直到时间结束
         * ease(tData:TweenData, tweenDisplay:egret.DisplayObject)
        */
        ease: Function;
        /**设定的起点值，由于可以倒着播放，此值保存的是起始设定的起点*/
        initFrom: number;
        /**设定的终点值，由于可以倒着播放，此值保存的是起始设定的终点*/
        initTo: number;
        /**当前运动的计算值*/
        curVal: number;
        /**附加参数*/
        param: any;
        /**时长，默认NaN，则使用GYTween的时长*/
        duration: number;
        /**延迟，默认NaN，则使用GYTween的延迟*/
        delay: number;
        constructor();
        clear(): void;
        private static _pool;
        static getInstance(propertyName?: string, to?: number, from?: number, ease?: Function, param?: any): TweenData;
        static converAni(ani: IAniData, target?: any): TweenData[];
    }
}
