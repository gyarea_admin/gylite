module GYLite
{
							
		
	export class GYScroller
	{
		private _scrollBarV:GYScrollBarV;
		private _scrollBarH:GYScrollBarH;		
		private _verticalPolicy:number = 0;//垂直滚动条 0自动 1显示 2不显示
		private _horizonPolicy:number = 0;//水平滚动条 0自动 1显示 2不显示
		private _rect:egret.Rectangle=new egret.Rectangle;
		private _viewport:GYGroup;
		private _parent:GYSprite;
		private _wheelScrollStep:number=20;
		/**滚轮是否启用*/public wheelEnabled:boolean;
		/**滚轮方向 1 纵向 2 横向*/public wheelDirection:number;
		public constructor()
		{
			let s = this;
			s.wheelDirection = 1;
			s.wheelEnabled = true;
		}
		/**滚动条控制的视图，必须是GYGroup*/
		public set viewport(val:GYGroup)
		{
			let s = this;
			if(s._viewport == val)
				return;
			if(s._viewport != null)
			{
				s._viewport.removeEventListener(GYViewEvent.VIEWCHANGE,s.checkBar,s);								
				s._viewport.setWheelFunc(null);
				s._viewport.scroller = null;
				s._viewport=null;
			}
			s._viewport = val;
			if(s._viewport)
			{
				s._viewport.addEventListener(GYViewEvent.VIEWCHANGE,s.checkBar,s);				
				s._viewport.setWheelFunc(s.scrollFunc, s);				
				s._viewport.clipAndEnableScrolling = true;
				s._viewport.scroller = this;
				s.checkBar();
			}
		}
		/**检测滚动条*/
		public checkBar(e:GYViewEvent=null):void
		{
			let s = this;
			var rect:egret.Rectangle;
			if (s._verticalPolicy!=2)
			{
				rect=s._viewport.getAllBounds(s._viewport);
				if (s._verticalPolicy==1)
				{
					s.showBar(1);
				}
				else if (s.checkRectV(rect))
				{
					s.showBar(1);
				}
				else
				{
					s.hideBar(1);
				}
			}
			if (s._horizonPolicy!=2)
			{
				if(rect==null)
					rect=s._viewport.getAllBounds(s._viewport);
				if (s._horizonPolicy==1)
				{
					s.showBar(2);
				}
				else if (s.checkRectH(rect))
				{
					s.showBar(2);
				}
				else
				{
					s.hideBar(2);
				}
			}
		}
		private checkRectV(rect:egret.Rectangle):boolean
		{
			let s = this;
			var rect1:egret.Rectangle=s._viewport.scrollRect;
			var rect2:egret.Rectangle=rect;
			if(rect1 == null)
				return false;
			if(rect2.height==0)
				return false;
			else if(rect1.height < rect2.bottom)
				return true
			return false;
		}
		private checkRectH(rect:egret.Rectangle):boolean
		{
			let s = this;
			var rect1:egret.Rectangle=s._viewport.scrollRect;
			if(rect1 == null)
				return false;
			var rect2:egret.Rectangle=rect;
			if(rect2.width==0)
				return false;
			else if(rect1.width < rect2.right)
				return true;
			return false;
		}
		//显示滚动条
		private showBar(type:number):void
		{
			let s = this;
			var sliderVal:number, per:number;
			if (type == 1)
			{
				if (s._scrollBarV==null)
					s.scrollBarV;
				s._scrollBarV.maximum = s._viewport.contentHeight - s._viewport.baseHeight;
				s._scrollBarV.barPercent = s._viewport.baseHeight / s._viewport.contentHeight;
				s._scrollBarV.height = s._viewport.baseHeight;
				if (s._scrollBarV.parent == null)
				{
					s._parent.addElement(s._scrollBarV);
				}
			}
			else
			{
				if (s._scrollBarH==null)
					s.scrollBarH;
				s._scrollBarH.maximum = s._viewport.contentWidth - s._viewport.baseWidth;
				s._scrollBarH.barPercent = s._viewport.baseWidth / s._viewport.contentWidth;
				s._scrollBarH.width = s._viewport.baseWidth;
				if (s._scrollBarH.parent == null)
				{
					s._parent.addElement(s._scrollBarH);
				}
			}
		}
		//隐藏滚动条
		private hideBar(type:number):void
		{
			let s = this;
			if(s._viewport.scrollRect == null)
				return;
			if (type == 1)
			{
				if (s._scrollBarV!=null && s._scrollBarV.parent!=null)
				{
					s._scrollBarV.maximum = 0;
					s._scrollBarV.barPercent = 1;
					s._parent.removeElement(s._scrollBarV);
					if(s._scrollBarH!=null && s._scrollBarH.parent!=null)
						s.showBar(2);
				}
			}
			else
			{
				if (s._scrollBarH!=null && s._scrollBarH.parent!=null)
				{
					s._scrollBarH.maximum = 0;
					s._scrollBarH.barPercent = 1;
					s._parent.removeElement(s._scrollBarH);
					if(s._scrollBarV!=null && s._scrollBarV.parent!=null)
						s.showBar(1);
				}
			}
		}		
		public set verticalPolicy(val:number)
		{
			let s = this;
			if (val == s._verticalPolicy)
			{
				return;
			}
			if (val == 0)
			{
				s._verticalPolicy = 0;
				s.checkBar();
			}
			else if (val == 1)
			{
				s._verticalPolicy = 1;
				s.showBar(1);
			}
			else if (val == 2)
			{
				s._verticalPolicy = 2;
				s.hideBar(1);
			}
		}
		/**垂直滚动条 0自动 1显示 2不显示*/
		public get verticalPolicy():number
		{
			return this._verticalPolicy;
		}
		public set horizonPolicy(val:number)
		{
			let s = this;
			if (val == s._horizonPolicy)
			{
				return;
			}
			if (val == 0)
			{
				s._horizonPolicy = 0;
				s.checkBar();
			}
			else if (val == 1)
			{
				s._horizonPolicy = 1;
				s.showBar(2);
			}
			else if (val == 2)
			{
				s._horizonPolicy = 2;
				s.hideBar(2);
			}
		}
		/**水平滚动条 0自动 1显示 2不显示*/
		public get horizonPolicy():number
		{
			return this._horizonPolicy;
		}
		public set wheelScrollStep(val:number)
		{
			let s = this;
			s._wheelScrollStep = val;
		}
		/**滚轮滚动步长*/
		public get wheelScrollStep():number
		{
			let s = this;
			return s._wheelScrollStep;
		}
		//滑轮滚动
		private scrollFunc(e):void
		{
			let s = this;
			if(!s.wheelEnabled)return;
			if(s._scrollBarV == null)
				return;			
			if(e.wheelDelta!=null)e.deltaY = e.wheelDelta;
			if(s.wheelDirection == 1)
			{
				if(e.deltaY < 0)
					s._scrollBarV.scrollToTop(s._wheelScrollStep);
				else
					s._scrollBarV.scrollToBottom(s._wheelScrollStep);
			}
			else
			{
				if(e.deltaY < 0)
					s._scrollBarH.scrollToLeft(s._wheelScrollStep);
				else
					s._scrollBarH.scrollToRight(s._wheelScrollStep);
			}
			
		}
		public set scrollPosY(val:number)
		{
			let s = this;
			if(s._scrollBarV==null || s._scrollBarV.parent == null)
				return;
			s._scrollBarV.value=val;
		}
		/**纵向滚动条当前位置value值(位置/步长)*/
		public get scrollPosY():number
		{
			let s = this;
			if(s._scrollBarV==null || s._scrollBarV.parent == null)
			{
				return 0;
			}
			return s._scrollBarV.value;
		}
		public set scrollPosX(val:number)
		{
			let s = this;
			if(s._scrollBarH==null || s._scrollBarH.parent == null)
				return;
			s._scrollBarH.value=val;
		}
		/**横向滚动条当前位置value值(位置/步长)*/
		public get scrollPosX():number
		{
			let s = this;
			if(s._scrollBarH==null || s._scrollBarH.parent == null)
			{
				return 0;
			}
			return s._scrollBarH.value;
		}
		public get scrollBarH():GYScrollBarH
		{
			let s = this;
			if (s._scrollBarH==null)
			{
				s._scrollBarH = new GYScrollBarH();
				s._scrollBarH.y = s._viewport.baseHeight;
				s._scrollBarH.addEventListener(GYScrollerEvent.SCROLL_CHANGE, s.SetScrollX, s);
			}
			return s._scrollBarH
		}
		public get scrollBarV():GYScrollBarV
		{
			let s = this;
			if (s._scrollBarV==null)
			{
				s._scrollBarV = new GYScrollBarV();
				s._scrollBarV.x = s._viewport.baseWidth;
				s._scrollBarV.addEventListener(GYScrollerEvent.SCROLL_CHANGE, s.SetScrollY, s);
			}
			return s._scrollBarV
		}
		
		private SetScrollX(e:GYScrollerEvent):void
		{
			let s = this;
			s._viewport.clipX = (e.currentTarget as GYScrollBase).position;
		}
		private SetScrollY(e:GYScrollerEvent):void
		{
			let s = this;
			s._viewport.clipY = (e.currentTarget as GYScrollBase).position;
		}
		public set parent(val:GYSprite)
		{
			let s = this;
			s._parent = val;
		}
		/**添加到的显示对象*/
		public get parent():GYSprite
		{
			let s = this;
			return s._parent;
		}

		public get viewport():GYGroup
		{
			let s = this;
			return s._viewport;
		}

	}
}