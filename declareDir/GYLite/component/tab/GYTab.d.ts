/**
 @author 迷途小羔羊
 2015.5.25
 */
declare module GYLite {
    class GYTab extends GYDataGrid {
        protected _lastGroup: GYUIComponent;
        protected _lastTab: TabData;
        protected _selGroup: GYUIComponent;
        protected _selTab: TabData;
        protected _autoTop: boolean;
        constructor(cols?: number, rows?: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        protected initComponent(): void;
        protected getGrid(): IItemRender;
        set selectedData(d: any);
        get selectedData(): any;
        set selectedItem(val: IItemRender);
        get selectedItem(): IItemRender;
        protected selDataChange(oldData: TabData, newData: TabData): void;
        /**GYTab只允许选择一个*/
        get canSelectNum(): number;
        set canSelectNum(val: number);
        /**当前选中的组*/
        get selGroup(): GYUIComponent;
        /**上次选中的组*/
        get lastGroup(): GYUIComponent;
        /**当前选中的Tab*/
        get selTab(): TabData;
        /**上次选中的Tab*/
        get lastTab(): TabData;
        /**是否选中自动置顶*/
        get autoTop(): boolean;
        set autoTop(val: boolean);
    }
}
