declare namespace egret.web {
    /**
     * @private
     * ImageLoader 类可用于加载图像（JPG、PNG 或 GIF）文件。使用 load() 方法来启动加载。被加载的图像对象数据将存储在 ImageLoader.data 属性上 。
     */
    class WebImageLoader extends EventDispatcher implements ImageLoader {
        /**
         * @private
         * 使用 load() 方法加载成功的 BitmapData 图像数据。
         */
        data: BitmapData;
        /**
         * @private
         * 当从其他站点加载一个图片时，指定是否启用跨域资源共享(CORS)，默认值为null。
         * 可以设置为"anonymous","use-credentials"或null,设置为其他值将等同于"anonymous"。
         */
        private _crossOrigin;
        /**
         * @private
         * 标记crossOrigin有没有被设置过,设置过之后使用设置的属性
         */
        private _hasCrossOriginSet;
        set crossOrigin(value: string);
        get crossOrigin(): string;
        /**
         * @private
         * 指定是否启用跨域资源共享,如果ImageLoader实例有设置过crossOrigin属性将使用设置的属性
         */
        static crossOrigin: string;
        /**
         * @private
         */
        private currentImage;
        /**
         * @private
         */
        private currentURL;
        /**
         * @private
         */
        private request;
        /**
         * @private
         * 启动一次图像加载。注意：若之前已经调用过加载请求，重新调用 load() 将终止先前的请求，并开始新的加载。
         * @param url 要加载的图像文件的地址。
         */
        load(url: string): void;
        /**
         * @private
         */
        private onBlobLoaded;
        /**
         * @private
         */
        private onBlobError;
        /**
         * @private
         */
        private loadImage;
        /**
         * @private
         */
        private onImageComplete;
        /**
         * @private
         */
        private onLoadError;
        private dispatchIOError;
        /**
         * @private
         */
        private getImage;
    }
}
