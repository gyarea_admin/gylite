module GYLite
{
	export interface IKeyBoardObject extends IResource
	{
		keyFocus():boolean;
		kDown(keyCode:number,e?:KeyboardEvent):void;
		kUp(keyCode:number,e?:KeyboardEvent):void;
	}
}