declare module GYLite {
    /**自定义皮肤主题，需实现此接口
     * */
    interface ISkinTheme extends IResource {
        GetButtonSkin(): IGYSkin;
        GetTabButtonSkin(): IGYSkin;
        GetLinkButtonSkin(): IGYSkin;
        GetScrollBarSkinV(): IGYSkin;
        GetScrollBarSkinH(): IGYSkin;
        GetListSkin(): IGYSkin;
        GetTextInputSkin(): IGYSkin;
        GetCheckBoxSkin(): IGYSkin;
        GetRadioButtonSkin(): IGYSkin;
        GetProgressBarSkin(): IGYSkin;
        GetSliderSkin(): IGYSkin;
        GetTextTextFormat(): TextFormat;
        GetMenu(w: number, h: number): GYMenu;
        GetComboBox(w: number, h: number): GYComboBox;
        GetComboButtonSkin(): IGYSkin;
        GetMenuSkin(): IGYSkin;
        GetWindowSkin(): IGYSkin;
        GetTextAreaSkin(): IGYSkin;
        initRes(): void;
        getBmp(n: string): egret.Texture;
        GetToolTip(): GYToolTip;
    }
}
