module GYLite
{
						
								
	export class StatusUtil
	{
		public statusSprite:GYSprite;
		private fpsText:GYText;
		private memoryText:GYText;
		private background:GYSprite;
		private loopCount:GYText;
		private rendMaxCount:GYText;
		private gyBitmapMemory:GYText;
		private flashVersionText:GYText;
//		private cpuUseText:GYText;
		private parent:egret.DisplayObjectContainer;
		private lastTime:number;
		private fps:number=0;
		private static _instance:StatusUtil;
		public static getInstance():StatusUtil
		{
			if(StatusUtil._instance == null)
				StatusUtil._instance = new StatusUtil;
			return StatusUtil._instance;
		}
		/**初始化*/
		public init(p:egret.DisplayObjectContainer):void
		{var s = this;
			if(s.parent)
				return;
			s.parent = p;
			s.statusSprite = new GYSprite;
			s.statusSprite.touchEnabled = s.statusSprite.touchChildren = false;
			s.background = new GYSprite;
			s.statusSprite.addElement(s.background);
			s.fpsText = s.getText();
			// s.memoryText = s.getText();
			// s.memoryText.y = s.fpsText.y + s.fpsText.height;
			s.loopCount = s.getText();
			s.loopCount.y = s.fpsText.y + s.fpsText.height;
			s.rendMaxCount = s.getText();
			s.rendMaxCount.y = s.loopCount.y + s.loopCount.height;
			s.gyBitmapMemory = s.getText();
			s.gyBitmapMemory.y = s.rendMaxCount.y + s.rendMaxCount.height;
			s.flashVersionText = s.getText();
			s.flashVersionText.y = s.gyBitmapMemory.y + s.gyBitmapMemory.height;
			s.flashVersionText.text = "egretVersion:5.2.19";
//			s.cpuUseText = s.getText();
//			s.cpuUseText.y = s.flashVersionText.y + s.flashVersionText.height;
//			s.statusSprite.addEventListener(MouseEvent.ROLL_OVER, StatusUtil.rlOver);
//			s.statusSprite.addEventListener(MouseEvent.ROLL_OUT, s.rlOut);
		}
		protected getText():GYText
		{var s = this;
			var txt:GYText = new GYText;
			txt.width = 200;
			txt.height = 20;
			txt.color = 0xffffff;
			s.statusSprite.addElement(txt);
			return txt;
		}
//		private static rlOver(e:egret.TouchEvent):void
//		{
//			var g:Graphics;
//			g = s.background.graphics;
//			g.beginFill(0xffffff,0.5);
//			g.drawRoundRect(0,0,s.statusSprite.width,s.statusSprite.height,5,5);
//			g.endFill();
//		}
//		private rlOut(e:egret.TouchEvent):void
//		{var s = this;
//			s.background.graphics.clear();
//		}
		/**显示*/
		public show():void
		{var s = this;
			if(s.statusSprite.parent==null)
			{
				s.parent.addChild(s.statusSprite);
				s.statusSprite.x = s.parent.width - 200;
				s.statusSprite.addEventListener(egret.Event.ENTER_FRAME, s.loop, s);
				s.lastTime = egret.getTimer();
			}
		}
		/**隐藏*/
		public hide():void
		{var s = this;
			if(s.statusSprite.parent)
			{
				s.parent.removeChild(s.statusSprite);
				s.statusSprite.removeEventListener(egret.Event.ENTER_FRAME, s.loop, s);
			}
		}
		protected loop(e:egret.Event):void
		{var s = this;
			var t:number = egret.getTimer();
			++s.fps;
			if(t - s.lastTime >= 1000)
			{
				s.fpsText.text = "fps:" + s.fps;
				// s.memoryText.text = "mem:" + System.totalMemory + "/" + System.freeMemory;
//				s.cpuUseText.text = "cpu:" + System.processCPUUsage;
				s.loopCount.text = "loops:" + CommonUtil.loopFuncVec.length;
				s.rendMaxCount.text = "rendMax:" + LayoutManager.max + "/" + LayoutManager.lastNum;
				s.gyBitmapMemory.text = "gyBitData:" + GYDrawBitmapData.memory + "/" + GYDrawBitmapData.num;
				s.fps = 0;
				s.lastTime = t;
			}
		}
	}
}