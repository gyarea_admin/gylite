declare module GYLite {
    /**布局策略类*/
    class LayoutMode {
        /**绝对布局*/ static ABSOLUTE: number;
        /**相对布局*/ static RELATIVE: number;
        /**相对水平布局*/ static RELATIVE_HORIZON: number;
        /**相对垂直布局*/ static RELATIVE_VERTICAL: number;
        $horizonalCenter: number;
        $verticalCenter: number;
        $right: number;
        $bottom: number;
        $left: number;
        $top: number;
        /**宽度百分比[0,1]*/ percentWidth: number;
        /**高度百分比[0,1]*/ percentHeight: number;
        constructor();
        set horizonalCenter(val: number);
        get horizonalCenter(): number;
        set verticalCenter(val: number);
        get verticalCenter(): number;
        set right(val: number);
        /**相对左边*/
        get left(): number;
        set left(val: number);
        /**相对上边*/
        get top(): number;
        set top(val: number);
        /**相对右边*/
        get right(): number;
        set bottom(val: number);
        /**相对底边*/
        get bottom(): number;
        isPercentSize(): boolean;
        /**布局模式*/
        get layoutMode(): number;
        /**克隆布局策略*/
        clone(): LayoutMode;
    }
}
