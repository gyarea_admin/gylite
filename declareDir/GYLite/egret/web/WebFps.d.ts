declare namespace egret.web {
    /**
     * @private
     */
    class WebFps implements egret.FPSDisplay {
        private panelX;
        private panelY;
        private fontColor;
        private fontSize;
        private container;
        private fps;
        private log;
        private showPanle;
        private renderMode;
        constructor(stage: Stage, showFPS: boolean, showLog: boolean, logFilter: string, styles: Object);
        private containerFps;
        private fpsHeight;
        private divDatas;
        private divDraw;
        private divCost;
        private contextFps;
        private canvasFps;
        private WIDTH;
        private HEIGHT;
        private bgCanvasColor;
        private fpsFrontColor;
        private contextCost;
        private canvasCost;
        private WIDTH_COST;
        private cost1Color;
        private cost3Color;
        private addFps;
        private addLog;
        private arrFps;
        private arrCost;
        private lastNumDraw;
        update(datas: FPSData, showLastData?: boolean): void;
        private arrLog;
        updateInfo(info: string): void;
        updateWarn(info: string): void;
        updateError(info: string): void;
        private updateLogLayout;
    }
}
