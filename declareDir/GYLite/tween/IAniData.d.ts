/***2025-01-10 迷途小羔羊
* 用于编写动画组配置结构
*/
declare module GYLite {
    interface IAniData {
        propertys: {
            [key: string]: number;
        };
        startPropertys?: {
            [key: string]: number;
        };
        ease?: (tData: GYLite.TweenData, t: GYLite.GYTween) => void;
        param?: {
            [key: string]: any;
        };
        duration?: number;
        delay?: number;
    }
}
