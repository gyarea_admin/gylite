declare module GYLite {
    /**DraggerHandle类是一个简易的拖动类，快速创建一个拖动响应的操作
     * 绑定对象，当对象触发MOUSE_DOWN事件，就提供一个帧频调用的回调函数，使用s.addBind添加拖动的响应函数
     * */
    class DraggerHandle {
        private _bindList;
        private _bindObjList;
        private _handle;
        private _dragTarget;
        private _dropTarget;
        private _dragMouseX;
        private _dragMouseY;
        private _touchId;
        private _updateFunc;
        private _updateObj;
        private _stopTouchEvent;
        private _longTapTime;
        private _longTapId;
        private _dragState;
        private _isDragMove;
        /**是否其他手指抬起是否不判断其id，如果为true，则不判断id，其他手指id的触碰抬起也会触发结束拖拽，默认true*/ ignoreOtherTouch: boolean;
        constructor();
        /**当前响应的触碰id*/
        get touchId(): number;
        get dragState(): number;
        set longTapTime(val: number);
        /**长按响应*/
        get longTapTime(): number;
        /**长按延时id -1代表没有长按触发*/
        get longTapId(): number;
        /**当拖拽产生时，是否屏蔽鼠标点击事件(默认true)*/
        whenDragStopTouch(val: boolean): void;
        private touchMove;
        private draggingLoop;
        private rollOver;
        private rollOut;
        private tap;
        private startDragFunc;
        private doStartDrag;
        /**帧频刷新回调
         * @param func 回调函数 (d:DraggerHandler)=>void
         * @param obj this指向
        */
        setUpdateFunc(func: Function, obj: any): void;
        private frameLoop;
        private stopDragFunc;
        stopDrag(): void;
        startDrag(target: Object, touchId?: number, event?: boolean): void;
        /**添加s.handle拖动的绑定，拖动时回调binder函数来通知
         * @param binder(drager:DraggerHandle):void回调函数
         * */
        addBind(binder: Function, thisObject: any): void;
        /**清除s.handle拖动的绑定*/
        delBind(binder: Function, thisObject: any): void;
        /**启动拖动*/
        run(gySp: IGYDisplay): void;
        /**清除s.handle的拖动*/
        clear(): void;
        /**被点击绑定的GYSprite*/
        get handle(): IGYDisplay;
        /**被拖动的DisplayObject*/
        get dragTarget(): any;
        /**被放置的DisplayObject*/
        get dropTarget(): any;
        /**拖动开启时，s.handle的mouseX*/
        get dragMouseX(): number;
        /**拖动开启时，s.handle的mouseY*/
        get dragMouseY(): number;
        /**设置对象可拖拽*/
        static dragSet(sprite: GYLite.GYSprite, enabled?: boolean): DraggerHandle;
        private static dragLoop;
        private static handleVec;
        /**获取拖动监听实例*/
        static getInstance(gySp: IGYDisplay): DraggerHandle;
        /**对被销毁的对象的DraggerHandle进行垃圾回收*/
        static gc(): void;
    }
}
