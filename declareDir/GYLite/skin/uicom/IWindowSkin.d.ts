declare module GYLite {
    interface IWindowSkin extends IGYSkin {
        setIcon(val: egret.Texture): void;
        setTitle(val: string): void;
        backgoundImage: GYScaleSprite;
        closeBtn: GYButton;
        icon: GYImage;
        title: GYText;
    }
}
