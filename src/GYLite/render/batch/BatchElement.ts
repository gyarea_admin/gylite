module GYLite{
    export class BatchElement implements IResource,IPoolObject{                                
        /**宽度(包括增加的边距)*/public width:number;
        /**高度(包括增加的边距)*/public height:number;
        public $disposed:boolean;
        public constructor(){
            let s= this;
            
        }                

        public get disposed(): boolean
        {
            return this.$disposed;
        }
        public dispose(): void {
            let s= this;
            if(s.$disposed)
                return;
            s.$disposed = true;            
            s.beforeToPool();
        }
        protected beforeToPool():void
        {            
        }
        public clear():void
        {
            let s = this;
            if(s.inPool)
                return;
            s.beforeToPool();
            GYLite.PoolUtil.toPool(s, s.constructor);
        }
		public inPool:boolean;
		public outPoolInit():void
        {
            
        }      
    }    
}