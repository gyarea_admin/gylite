declare module GYLite {
    class PoolUtil {
        private static _idCount;
        private static _poolDict;
        private static _poolCfgDict;
        /**设置对象池数量溢出警告的溢出临界值*/
        static setPoolLengthWarn(cls: any, maxNum: number): void;
        /**获取对象池数量溢出警告的溢出临界值*/
        static getPoolLengthWarn(cls: any): number;
        /**从对象池取出*/
        static fromPool(cls: any): IPoolObject;
        /**回收入对象池*/
        static toPool(poolObject: IPoolObject, cls: any): void;
        /**销毁池子*/
        static destroyPool(cls: any, includeExtend?: boolean): void;
    }
}
