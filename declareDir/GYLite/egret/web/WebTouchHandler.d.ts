declare namespace egret.web {
    /**
     * @private
     */
    class WebTouchHandler extends HashObject {
        /**
         * @private
         */
        constructor(stage: egret.Stage, canvas: HTMLCanvasElement);
        /**
         * @private
         */
        private canvas;
        /**
         * @private
         */
        private touch;
        /**
         * @private
         * 添加事件监听
         */
        private addListeners;
        /**
         * @private
         *
         */
        private addMouseListener;
        /**
         * @private
         *
         */
        private addTouchListener;
        /**
         * @private
         */
        private prevent;
        /**
         * @private
         */
        private onTouchBegin;
        private onMouseMove;
        /**
         * @private
         */
        private onTouchMove;
        /**
         * @private
         */
        private onTouchEnd;
        /**
         * @private
         */
        private getLocation;
        /**
         * @private
         */
        private scaleX;
        /**
         * @private
         */
        private scaleY;
        /**
         * @private
         */
        private rotation;
        /**
         * @private
         * 更新屏幕当前的缩放比例，用于计算准确的点击位置。
         * @param scaleX 水平方向的缩放比例。
         * @param scaleY 垂直方向的缩放比例。
         */
        updateScaleMode(scaleX: number, scaleY: number, rotation: number): void;
        /**
         * @private
         * 更新同时触摸点的数量
         */
        $updateMaxTouches(): void;
    }
}
