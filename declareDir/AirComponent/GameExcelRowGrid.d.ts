declare module GYLite {
    class GameExcelRowGrid extends ExcelRowGrid {
        constructor();
        protected initComponent(): void;
        protected drawBg(): void;
        /**@inheriDoc */
        setData(obj: any): void;
    }
}
