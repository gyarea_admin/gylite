module GYLite
{
			
	export class GYScrollGroup extends GYUIComponent
	{
		public scrollerViewPort:GYGroup;
		public scroller:GYScroller;
		public constructor()
		{
			super();
			var s = this;
			s.initComponent();
		}
		protected initComponent():void
		{
			let s = this;
			s.scrollerViewPort = new GYGroup;
			s.scrollerViewPort.touchEnabled = true;
			s.scrollerViewPort.rectHit = true;
			super.addElement(s.scrollerViewPort);			
			s.scroller=new GYScroller;
			s.scroller.parent = this;
			s.scroller.viewport = s.scrollerViewPort;			
		}
		/**@inheritDoc */
		public getElementAt(index:number):IGYDisplay
		{let s = this;
			return s.scrollerViewPort.getElementAt(index);
		}
		/**@inheritDoc */
		public getElementIndex(child:IGYDisplay):number
		{let s = this;
			return s.scrollerViewPort.getElementIndex(child);
		}
		/**@inheritDoc*/
		public delElement(child:IGYDisplay):void
		{
			let s = this;
			if(child == s.scroller.scrollBarH || child == s.scroller.scrollBarV)
				super.delElement(child);
			s.scrollerViewPort.delElement(child);
		}
		/**@inheritDoc*/
		public addElement(disObj:IGYDisplay):IGYDisplay
		{
			let s = this;
			if(disObj == s.scroller.scrollBarH || disObj == s.scroller.scrollBarV)
				return super.addElement(disObj);
			s.scrollerViewPort.addElement(disObj);
			disObj.addLayout(s);
			s.invalidZIndex();
			return disObj;
		}
		/**@inheritDoc*/
		public addElementAt(disObj:IGYDisplay, index:number=0):IGYDisplay
		{
			let s = this;			
			s.scrollerViewPort.addElementAt(disObj, index);
			disObj.addLayout(s);
			s.invalidZIndex();
			return disObj;
		}
		/**@inheritDoc*/
		public removeElement(disObj:IGYDisplay):IGYDisplay
		{
			let s = this;
			if(disObj == s.scroller.scrollBarH || disObj == s.scroller.scrollBarV)
				return super.removeElement(disObj);
			let display:GYLite.IGYDisplay = s.scrollerViewPort.removeElement(disObj);
			s.invalidZIndex();
			return display;
		}
		/**@inheritDoc*/
		public removeElementAt(index:number=0):IGYDisplay
		{
			let s = this;			
			let display:GYLite.IGYDisplay = s.scrollerViewPort.removeElementAt(index);
			s.invalidZIndex();
			return display;
		}
		/**@inheritDoc*/
		public setElementIndex(child:IGYDisplay,index:number):void
		{let s = this;
			s.scrollerViewPort.setElementIndex(child,index);
			s.invalidZIndex();
		}
		/**@inheritDoc*/
		public swapElementIndex(child1,child2):void
		{let s = this;
			s.scrollerViewPort.swapElementIndex(child1,child2);
			s.invalidZIndex();
		}
		public get numElement():number
		{let s = this;
			return s.scrollerViewPort.numElement;
		}
		/**@inheritDoc*/
		public set width(val:number)
		{
			let s = this;			
			egret.superSetter(GYScrollGroup, s, "width", val);
			s.scrollerViewPort.width = val;
			s.scroller.scrollBarV.x = s.scrollerViewPort.baseWidth;
		}
		public get width():number
		{
			let s = this;
			return egret.superGetter(GYScrollGroup, s, "width");
		}
		/**@inheritDoc*/
		public set height(val:number)
		{
			let s = this;
			egret.superSetter(GYScrollGroup, s, "height", val);			
			s.scrollerViewPort.height = val;
			s.scroller.scrollBarH.y = s.scrollerViewPort.baseHeight;
		}
		public get height():number
		{
			let s = this;
			return egret.superGetter(GYScrollGroup, s, "height");
		}
	}
}