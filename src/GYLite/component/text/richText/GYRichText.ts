module GYLite
{
	/**
	 * 包含图片样式： <code>&lt;bmp s.width='' s.height='' target='**' render='**' renderParam='**' s.clear='**' &gt;</code>
	 * 可自定义更多样式
	 */
	export class GYRichText extends GYGroup
	{
		public static imageList:Array<any>=[];
		/**文本容器*/
		public get richSp():GYSprite
		{
			let s = this;
			return s._richSP;
		}
		/**混排容器*/
		public get imgGrp():GYSprite
		{
			let s = this;
			return s._imgGrp;
		}
		public get maxLines():number
		{
			let s = this;
			return s._maxLines;
		}
		public set maxLines(value:number)
		{
			let s = this;
			s._maxLines=value;
		}
		public set richTop(value:number)
		{
			let s = this;
			s._y = s._padTop = value;
		}
		/**上边界*/
		public get richTop():number
		{
			let s = this;
			return s._padTop;
		}
		
		public set leading(value:number)
		{
			let s = this;
			s._textFormat.leading=value;
		}
		/**行间距*/
		public get leading():number
		{
			let s = this;
			return Number(s._textFormat.leading);
		}
		/**文本格式*/
		public get textFormat():TextFormat
		{
			let s = this;
			return s._textFormat;
		}
		
		public set letterSpacing(value:number)
		{
			let s = this;
			s._textFormat.letterSpacing=value;
		}
		/**字间距*/
		public get letterSpacing():number
		{
			let s = this;
			return Number(s._textFormat.letterSpacing);
		}
		/**文本高度*/
		public get textHeight():number
		{
			let s = this;
			return s._y;
		}
		/**文本宽度*/
		public get textWidth():number
		{
			let s = this;
			return s._tmpW;
		}
		/**点击函数*/
		public get linkFun():Function
		{
			let s = this;
			return s._linkFun;
		}
		
		public setLinkFun(fun:Function, thisObject:any)
		{
			let s = this;
			s._linkFun = fun;
			s._linkThisObject = thisObject;
		}
		public get embedFont():boolean
		{
			let s = this;
			return s._embedFont;
		}
		
		public set embedFont(value:boolean)
		{
			let s = this;
			s._embedFont = value;
		}
		/**首行固定*/
		public get firstLineFix():boolean
		{
			let s = this;
			return s._firstLineFix;
		}
		
		public set firstLineFix(value:boolean)
		{
			let s = this;
			s._firstLineFix = value;
		}
		/**鼠标所在的文本元素*/
		public get whichLine():number
		{
			let s = this;
			var len:number=s._lineVec.length;
			for (var i:number=0;i<len;++i)
			{
				var line:RichLine=s._lineVec[i];
				if (s.mouseY >= line.top && s.mouseY <= line.bottom + 4)
					return i;
			}
			return -1;
		}
		
		public constructor()
		{
			super();
			var s = this;
			s.addElement(s._imgGrp);
			s.addElementAt(s._richSP,0);
		}
		public getRichLabel(indent:number=0):RichLabel
		{
			let s = this;
			var lab:RichLabel = PoolUtil.fromPool(RichLabel) as RichLabel;
			s.textFormat.indent = indent;
			lab.textFormat = s.textFormat;
			lab.setLinkFun(s._linkFun,s._linkThisObject);
			// lab.embedFonts = s.embedFont;
			return lab;
		}
		/**添加间距*/
		public appendGap(value:number=0):void
		{
			let s = this;
			s._y += value;
		}
		/**添加文本*/
		public appendText(text:string):void
		{
			let s = this;
			var len:number=s._lineVec.length;
			if (len == s._maxLines)
			{
				var first:RichLine=s._lineVec.shift();
				first.clear();
				s._y -= first.bottom;
				--len;
				for (var i:number=0;i<len;++i)
				{
					s._lineVec[i].ResetY(first.bottom);
				}
			}
			var line:RichLine=new RichLine(this,s.baseWidth);
			s._lineVec.push(line);
			line.y=s._y;
			s.splitStr(text, line);
			line.Show();
			s._y=line.bottom;// + s.leading;
			if (line.width > s._tmpW)
				s._tmpW=line.width;
		}
		/**清理内容*/
		public clear():void
		{
			let s = this;
			var len:number=s._lineVec.length;
			for (var i:number=0;i<len;++i)
			{
				s._lineVec[i].clear();
			}
			s._y=s._padTop;
			s._tmpW=0;
			s._lineVec.length=0;
		}
		
		private splitStr(text:string, line:RichLine):void
		{
			let s = this;
			while (text != "")
			{
				var tagBlock:any=GYRichText._tagReg.exec(text);
				if (!tagBlock)
				{
					line.Add({isLbl:1, text: text});
					return;
				}
				// 结束标签前移
				var preStr:string="";
				if (tagBlock.index > 0)
					preStr=text.substr(0, tagBlock.index);
				var nBlockEndIdx:number=tagBlock.index + tagBlock[0].length;
				text=text.substring(nBlockEndIdx);
				GYRichText._endReg.lastIndex=0;
				var tmpInx:number=0;
				var tagArr:Array<any>=[];
				var reSub:boolean=false;
				while (true)
				{
					var endObj:any=GYRichText._endReg.exec(text);
					if (!endObj)
						break;
					var tmp:string=text.substring(tmpInx,endObj.index);
					tmpInx = GYRichText._endReg.lastIndex;
					if (tmp == "")
					{
						preStr += endObj[0];
						nBlockEndIdx=GYRichText._endReg.lastIndex;
						reSub=true;
						continue;
					}
					var pos:number=tmp.indexOf("<");
					if (pos >= 0)
						break;
					preStr += endObj[0];
					tagArr.push("<" + endObj[1] + ">");
				}
				if (reSub)
					text=text.substring(nBlockEndIdx);
				var len:number=tagArr.length;
				for (var i:number=0;i<len;++i)
					text=tagArr[i] + text;
				if (preStr != "")
				{
					line.Add({isLbl: 1, text: preStr});
				}
				var obj:Object={};
				var sAttrName:string;
				var sAttrValue:string;
				GYRichText._attrReg.lastIndex=0;
				while (true)
				{
					var partten:Object=GYRichText._attrReg.exec(tagBlock[1]);
					if (!partten)
						break;
					sAttrName=partten[1];
					sAttrValue=partten[2];
					obj[sAttrName]=sAttrValue;
				}
				line.Add(obj);
			}
		}
		/**获取图片文本
		 * @param w-宽度 h-高度 Id-图片id 
		 * */
		public static getImageRich(Id:number=1,offsetX:number=0,offsetY:number=0,w:number=25,h:number=25):string
		{
			return "<bmp s.width='" + w + "' s.height='" + h + "' target='GYLite.RichImage' render='getInstance' renderParam='" + Id + "," + offsetX + "," + offsetY + "' clear='clear' >";
		}
		/**获取url图片文本
		 * @param w-宽度 h-高度 Id-图片id 
		 * */
		public static getUrlImageRich(url:string,offsetX:number=0,offsetY:number=0,w:number=25,h:number=25):string
		{
			return "<bmp s.width='" + w + "' s.height='" + h + "' target='GYLite.RichImage' render='getUrlInstance' renderParam='" + url + "," + offsetX + "," + offsetY + "' clear='clear' >";
		}
		private static _endReg:RegExp = new RegExp("</([^>]*)>","g");
		private static _tagReg:RegExp=new RegExp("<bmp([^>]*)>");		
		private static _attrReg:RegExp=new RegExp("([A-Za-z]*)='([^']*)'", "g");
		private _lineVec:RichLine[]=new Array<RichLine>();
		private _richSP:GYSprite = new GYSprite;
		private _imgGrp:GYSprite=new GYSprite;
		private _firstLineFix:boolean;
		private _embedFont:boolean;
		private _maxLines:number=50;
		private _y:number=0;
		private _padTop:number=0;
		private _linkFun:Function;
		private _linkThisObject:any;
		private _tmpW:number=0;
		private _textFormat:TextFormat=new TextFormat(GYTextBase.defualtSysFont, 12, 0xfff8a2, false, false, false, null,null,"left",null,null,0,5);
	}
}






