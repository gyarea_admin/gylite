module GYLite
{
			
	export interface ISliderSkin extends IProgressBarSkin
	{
		sliderX:number;
		sliderY:number;		
		// readonly sliderButton:GYButton;
		sliderButton:GYButton;
	}
}