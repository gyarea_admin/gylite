declare namespace egret.web {
    /**
     * @private
     */
    class EgretWebGLAttribute {
        private gl;
        private name;
        private type;
        private size;
        location: number;
        constructor(gl: WebGLRenderingContext, program: WebGLProgram, attributeData: any);
        count: number;
        private initCount;
        format: number;
        private initFormat;
    }
}
