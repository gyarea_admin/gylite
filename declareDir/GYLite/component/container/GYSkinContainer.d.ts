declare module GYLite {
    class GYSkinContainer extends GYUIComponent implements IGYSkinContainer {
        protected _skin: any;
        constructor();
        protected initComponent(): void;
        set skin(val: any);
        protected themeChange(e: GYThemeEvent): void;
        get skin(): any;
        protected skinChange(oldSkin: any, newSkin: any): void;
        protected getThemeSkin(): IGYSkin;
    }
}
