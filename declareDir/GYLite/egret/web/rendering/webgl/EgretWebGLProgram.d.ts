declare namespace egret.web {
    /**
     * @private
     */
    type ProgramCache = {
        [index: string]: EgretWebGLProgram;
    };
    /**
     * @private
     */
    type Uniforms = {
        [index: string]: EgretWebGLUniform;
    };
    /**
     * @private
     */
    type Attributes = {
        [index: string]: EgretWebGLAttribute;
    };
    /**
     * @private
     */
    class EgretWebGLProgram {
        private static programCache;
        /**
         * 获取所需的WebGL Program
         * @param key {string} 对于唯一的program程序，对应唯一的key
         */
        static getProgram(gl: WebGLRenderingContext, vertSource: string, fragSource: string, key: string): EgretWebGLProgram;
        static deleteProgram(gl: WebGLRenderingContext, vertSource: string, fragSource: string, key: string): void;
        private vshaderSource;
        private fshaderSource;
        private vertexShader;
        private fragmentShader;
        id: WebGLProgram;
        attributes: Attributes;
        uniforms: Uniforms;
        private constructor();
    }
}
