/**
 @author 迷途小羔羊
 2015.3.5
 */
declare module GYLite {
    class GYListBase extends GYGroup implements IList {
        protected _hScroller: GYScrollBarH;
        protected _vScroller: GYScrollBarV;
        protected _innerHeight: number;
        protected _innerWidth: number;
        protected _boxs: number;
        protected _padding: number;
        protected _dataProvider: Array<any>;
        protected _grids: Array<any>;
        protected _boxW: number;
        protected _boxH: number;
        protected createGrid: Function;
        protected setGridData: Function;
        protected _lastBox: number;
        protected _max: number;
        protected _scrollerPolicy: number;
        protected _selectedData: any;
        protected _dataUpate: boolean;
        protected _gridUpdate: boolean;
        protected _gridSizeChange: boolean;
        protected _gridDataUpdate: boolean;
        protected _canSelectNum: number;
        protected _boxNumUpdate: boolean;
        protected _boxNumUpdateReset: boolean;
        protected _selectList: any[];
        protected _dataToItemDict: Dictionary;
        protected _keepSelected: boolean;
        protected _nextData: any;
        protected _selectIndex: number;
        protected _wheelScroll: boolean;
        protected _wheelStep: number;
        protected _stopSelTarget: any;
        private _selectTime;
        private _selectInterval;
        private _dragSelect;
        protected _mouseSelect: boolean;
        private _addSelect;
        protected _setThisObject: any;
        protected _getThisObject: any;
        protected _dragPreEnabled: boolean;
        protected _dragNextEnabled: boolean;
        protected _virtual_layout: boolean;
        protected _scrollPosLimit: number;
        protected _scrollToPos: number;
        protected _overlying: boolean;
        /**列表是否可选(默认true)*/ selectable: boolean;
        /**速度系数，乘以滑动的速度*/ speedParam: number;
        /**区间限制的时候，恢复到区间位置的滚动花费时间*/ scrollNextTime: number;
        /**List组件的基类 继承后自定义其逻辑或者直接使用GYListV GYListH GYDataListV GYDataListH
         * @param size list尺寸（纵向ListV为高度，横向ListH则为宽度）设定后无法更，当为负数的时候则表示为boxCount，此时则需要另外设置list宽高和项宽高（s.boxH和s.boxW）
         * @param getGridFunc():IItemRender 返回自定义格子对象的方法
         * @param setGridFunc(IItemRender, any) 设置格子数据的方法*/
        constructor(size: number, getGridFunc?: Function, getThisObject?: any, setGridFunc?: Function, setThisObject?: any);
        protected initComponent(): void;
        protected getGrid(): IItemRender;
        protected setGrid(grid: IItemRender, obj: any): void;
        protected selectCell(e: egret.TouchEvent): void;
        protected selectLine(data: any, selectedData: any, ctrlKey: boolean, checkNext?: boolean): void;
        private selectIn;
        private selectOut;
        private selectLoop;
        /**数据是否被选中*/
        dataIsSelected(d: any): boolean;
        /**跳转至某索引项*/
        scrollToIndex(val: number): void;
        /**跳转至某索引项(位置在末尾)*/
        scrollToEndIndex(val: number): void;
        private releaseOutSide;
        set virtual_layout(val: boolean);
        get virtual_layout(): boolean;
        set canSelectNum(val: number);
        /**同时能选择的数量*/
        get canSelectNum(): number;
        /**多选时的选择列表*/
        get selectList(): any[];
        set selectList(val: any[]);
        /**选择的数据索引*/
        get selectedIndex(): number;
        set selectedIndex(val: number);
        set selectedItem(val: IItemRender);
        /**当前选中的ItemRender*/
        get selectedItem(): IItemRender;
        getItemByData(d: any): IItemRender;
        set selectedData(d: any);
        get selectedData(): any;
        set boxW(val: number);
        set boxH(val: number);
        /**格子宽度(ItemRender不等宽高的列表时，仅为默认值)*/
        get boxW(): number;
        /**格子高度(ItemRender不等宽高的列表时，仅为默认值)*/
        get boxH(): number;
        set padding(val: number);
        /**间距*/
        get padding(): number;
        set boxs(val: number);
        get boxs(): number;
        set dataProvider(val: Array<any>);
        /**数据源*/
        get dataProvider(): Array<any>;
        /**数据失效*/
        invalidData(): void;
        /**格子数量失效*/
        invalidBoxNum(): void;
        /**网格布局失效*/
        invalidGrids(): void;
        /**网格数据失效*/
        invalidGridData(): void;
        updateView(): void;
        /**刷新格子布局*/
        updateGrid(): void;
        /**刷新数据*/
        updateData(): void;
        /**刷新格子数据*/
        updateGridData(): void;
        /**移除索引位置上的数据项（刷新）*/
        removeItemAt(ind: number): void;
        /**移除数据项（刷新）*/
        removeItem(d: any): void;
        /**添加索引位置数据项（刷新）*/
        addItemAt(d: any, ind: number): void;
        /**添加数据项（刷新）*/
        addItem(d: any): void;
        /**刷新数据项*/
        updateItem(d: any): void;
        /**刷新格子数*/
        protected boxNumChange(update?: boolean): void;
        /**刷新列表可见的所有项*/
        updateItems(): void;
        /**拖选时的滚动时间间隔(毫秒)*/
        get selectInterval(): number;
        set selectInterval(value: number);
        /**是否启用拖选功能*/
        get dragSelect(): boolean;
        set dragSelect(value: boolean);
        /**当次选择排除的点击对象，如ItemRender内部有按钮，避免点击当成选择，可临时设置此属性，点击后将被设置为null*/
        get stopSelTarget(): any;
        set stopSelTarget(value: any);
        /**是否保留选择的数据项*/
        get keepSelected(): boolean;
        set keepSelected(value: boolean);
        get nextData(): any;
        /**是否启用滚轮*/
        get wheelScroll(): boolean;
        set wheelScroll(value: boolean);
        protected wheelRoll(e: egret.TouchEvent): void;
        get wheelStep(): number;
        set wheelStep(value: number);
        /**除去滚动条的宽度*/
        get innerWidth(): number;
        /**除去滚动条的高度度*/
        get innerHeight(): number;
        /**获取数据对应的Item字典*/
        get dataToItemDict(): Dictionary;
        set dragNextEnabled(val: boolean);
        /**禁止拖动到下一项*/
        get dragNextEnabled(): boolean;
        /**禁止拖动到上一项*/
        set dragPreEnabled(val: boolean);
        get dragPreEnabled(): boolean;
        /**滚动区间限制,限制滚动停止的位置以相等的距离*/
        set scrollPosLimit(val: number);
        get scrollPosLimit(): number;
        /**滚动条 0自动 1显示 2不显示*/
        set scrollerPolicy(val: number);
        get scrollerPolicy(): number;
        /**滚动到下一项
         * @return 返回滚动的目标位置
        */
        scrollToNextItem(): number;
        /**滚动到上一项
         * @return 返回滚动的目标位置
        */
        scrollToPreItem(): number;
        protected scrollEnd(e?: GYScrollerEvent): void;
        /**scrollToNextItem、scrollToPreItem是否叠加滚动，默认true*/
        get overlying(): boolean;
        set overlying(val: boolean);
        protected vScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        protected hScrollChange(e?: GYScrollerEvent, update?: boolean, updatePos?: boolean): void;
        get vScroller(): GYScrollBarV;
        set vScroller(val: GYScrollBarV);
        get hScroller(): GYScrollBarH;
        set hScroller(val: GYScrollBarH);
        clone(): GYListBase;
        /**拖选响应毫秒(默认值)*/ static default_selectInterval: number;
        /**滚轮步长(默认值)*/ static default_wheelStep: number;
        /**是否允许滚轮(默认值)*/ static default_wheelScroll: boolean;
    }
}
