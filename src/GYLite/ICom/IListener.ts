module GYLite
{
	export interface IListener
	{
		dataChange(listenId:number):void;
	}
}