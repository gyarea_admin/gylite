module GYLite
{
	export class GYGridEvent extends GYEvent
	{
		public static ROWGRID_DATACHANGE:string="rowGrid_DataChange";
		public static COLGRID_DATACHANGE:string="colGrid_DataChange";
		public static COLGRID_NUMCHANGE:string="colGrid_numChange";
		public static ROWGRID_NUMCHANGE:string="rowGrid_numChange";
		public constructor(type:string, bubbles?:boolean, cancelable?:boolean)
		{
			super(type, bubbles, cancelable);
		}
	}
}