declare namespace egret {
    /**
     * @private
     */
    interface MapLike<T> {
        [key: string]: T;
        [key: number]: T;
    }
    /**
     * @private
     */
    function createMap<T>(): MapLike<T>;
}
