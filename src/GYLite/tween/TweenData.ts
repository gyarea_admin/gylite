module GYLite
{
	export class TweenData
	{
		/**需要tween的属性名称*/
		public propertyName:string;
		/**起点值*/
		public from:number;
		/**终点值*/
		public to:number;
		/**补间函数，一个每帧会调度的函数，直到时间结束
		 * ease(tData:TweenData, tweenDisplay:egret.DisplayObject)
		*/
		public ease:Function;
		/**设定的起点值，由于可以倒着播放，此值保存的是起始设定的起点*/
		public initFrom:number;
		/**设定的终点值，由于可以倒着播放，此值保存的是起始设定的终点*/
		public initTo:number;
		/**当前运动的计算值*/
		public curVal:number;
		/**附加参数*/
		public param:any;
		/**时长，默认NaN，则使用GYTween的时长*/
		public duration:number;
		/**延迟，默认NaN，则使用GYTween的延迟*/
		public delay:number;
		public constructor()
		{
			var s = this;
			s.propertyName=null;
			s.from=NaN;
			s.to=NaN;
			s.ease=null;
			s.initFrom=NaN;
			s.initTo=NaN;
			s.duration = NaN;
			s.delay = NaN;
		}
		public clear():void
		{var s = this;
			TweenData._pool.push(this);
			s.propertyName = null;
			s.from = NaN;
			s.to = NaN;
			s.ease = null;
			s.initFrom=NaN;
			s.initTo=NaN;
			s.param = null;
			s.curVal = 0;
		}
		private static _pool:TweenData[];		
		public static getInstance(propertyName:string="x",to:number=0, from:number = NaN, ease:Function=null, param:any=null):TweenData
		{
			var t:TweenData;
			if(!TweenData._pool)
			{				
				TweenData._pool=new Array<TweenData>();
			}
			t = TweenData._pool.length==0?new TweenData():TweenData._pool.pop();
			t.propertyName = propertyName;
			t.initFrom = from;
			t.initTo = to;
			t.ease = ease;
			t.param = param;
			t.curVal = 0;
			return t;
		}
		public static converAni(ani:IAniData,target:any=null):TweenData[]
		{
			let t:TweenData;
			let list:TweenData[]=[];
			for(var key in ani.propertys)
			{
				if(target == null || key in target)
				{
					t = TweenData.getInstance(key, ani.propertys[key], (ani.startPropertys&&ani.startPropertys[key])?ani.startPropertys[key]:NaN, ani.ease, ani.param?ani.param[key]:0);
					t.duration = ani.duration!=null?ani.duration:NaN;
					t.delay = ani.delay!=null?ani.delay:NaN;
					list.push(t);
				}				
			}
			return list;			
		}
	}
}