declare namespace egret.web {
    class TextBlock extends HashObject {
        private readonly _width;
        private readonly _height;
        private readonly _border;
        line: Line;
        x: number;
        y: number;
        u: number;
        v: number;
        tag: string;
        readonly measureWidth: number;
        readonly measureHeight: number;
        readonly canvasWidthOffset: number;
        readonly canvasHeightOffset: number;
        readonly stroke2: number;
        constructor(width: number, height: number, measureWidth: number, measureHeight: number, canvasWidthOffset: number, canvasHeightOffset: number, stroke2: number, border: number);
        get border(): number;
        get width(): number;
        get height(): number;
        get contentWidth(): number;
        get contentHeight(): number;
        get page(): Page;
        updateUV(): boolean;
        get subImageOffsetX(): number;
        get subImageOffsetY(): number;
    }
    class Line extends HashObject {
        page: Page;
        private readonly textBlocks;
        dynamicMaxHeight: number;
        readonly maxWidth: number;
        x: number;
        y: number;
        constructor(maxWidth: number);
        isCapacityOf(textBlock: TextBlock): boolean;
        private lastTextBlock;
        addTextBlock(textBlock: TextBlock, needCheck: boolean): boolean;
    }
    class Page extends HashObject {
        readonly lines: Line[];
        readonly pageWidth: number;
        readonly pageHeight: number;
        webGLTexture: WebGLTexture;
        constructor(pageWidth: number, pageHeight: number);
        addLine(line: Line): boolean;
    }
    class Book extends HashObject {
        private readonly _pages;
        private _sortLines;
        private readonly _maxSize;
        private readonly _border;
        constructor(maxSize: number, border: number);
        addTextBlock(textBlock: TextBlock): boolean;
        private _addTextBlock;
        private createPage;
        private sort;
        createTextBlock(tag: string, width: number, height: number, measureWidth: number, measureHeight: number, canvasWidthOffset: number, canvasHeightOffset: number, stroke2: number): TextBlock;
    }
}
