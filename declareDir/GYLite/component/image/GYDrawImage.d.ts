declare module GYLite {
    /**GYDrawImage一个可以绘制显示对象的位图类，内部使用GYDrawBitmapData临时位图*/
    class GYDrawImage extends GYSprite {
        protected _mySource: egret.Bitmap;
        protected _bitData: GYDrawBitmapData;
        constructor();
        set source(bmp: GYDrawBitmapData);
        /**位图数据*/
        get source(): GYDrawBitmapData;
        /**释放资源，由于使用的是GYDrawBitmapData临时位图，回收时必须释放其持有的GYDrawBitmapData引用*/
        release(): void;
        draw(dis: any, clipRect?: egret.Rectangle, smooth?: boolean): void;
        set width(val: number);
        set height(val: number);
        get width(): number;
        get height(): number;
        protected RlOver(e: egret.TouchEvent): void;
        get smooth(): boolean;
        set smooth(value: boolean);
    }
}
