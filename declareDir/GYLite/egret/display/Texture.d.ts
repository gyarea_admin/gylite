declare namespace egret {
    let $TextureScaleFactor: number;
    /**
     * The Texture class encapsulates different image resources on different platforms.
     * In HTML5, resource is an HTMLElement object
     * In OpenGL / WebGL, resource is a texture ID obtained after the GPU is submitted
     * The Texture class encapsulates the details implemented on the underlayer. Developers just need to focus on interfaces
     * @see http://edn.egret.com/cn/docs/page/135 The use of texture packs
     * @see http://edn.egret.com/cn/docs/page/123 Several ways of access to resources
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/display/Texture.ts
     * @language en_US
     */
    /**
     * 纹理类是对不同平台不同的图片资源的封装
     * 在HTML5中，资源是一个HTMLElement对象
     * 在OpenGL / WebGL中，资源是一个提交GPU后获取的纹理id
     * Texture类封装了这些底层实现的细节，开发者只需要关心接口即可
     * @see http://edn.egret.com/cn/docs/page/135 纹理集的使用
     * @see http://edn.egret.com/cn/docs/page/123 获取资源的几种方式
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/display/Texture.ts
     * @language zh_CN
     */
    class Texture extends HashObject {
        /**GYLite
         * lock texture notToRefresh
        */
        $lock: boolean;
        lock(val: boolean): void;
        /**
         * Create an egret.Texture object
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 创建一个 egret.Texture 对象
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        constructor();
        /**
         * Whether to destroy the corresponding BitmapData when the texture is destroyed
         * @version Egret 5.0.8
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 销毁纹理时是否销毁对应BitmapData
         * @version Egret 5.0.8
         * @platform Web,Native
         * @language zh_CN
         */
        disposeBitmapData: boolean;
        /**
         * @private
         * 表示这个纹理在 bitmapData 上的 x 起始位置
         */
        $bitmapX: number;
        /**
         * @private
         * 表示这个纹理在 bitmapData 上的 y 起始位置
         */
        $bitmapY: number;
        /**
         * @private
         * 表示这个纹理在 bitmapData 上的宽度
         */
        $bitmapWidth: number;
        /**
         * @private
         * 表示这个纹理在 bitmapData 上的高度
         */
        $bitmapHeight: number;
        /**
         * @private
         * 表示这个纹理显示了之后在 x 方向的渲染偏移量
         */
        $offsetX: number;
        /**
         * @private
         * 表示这个纹理显示了之后在 y 方向的渲染偏移量
         */
        $offsetY: number;
        /**纹理的uv坐标*/
        $uv: number[];
        /**
         * @private
         * 纹理宽度
         */
        private $textureWidth;
        /**
         * Texture width, read only
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 纹理宽度，只读属性，不可以设置
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get textureWidth(): number;
        $getTextureWidth(): number;
        /**
         * @private
         * 纹理高度
         */
        private $textureHeight;
        /**
         * Texture height, read only
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 纹理高度，只读属性，不可以设置
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get textureHeight(): number;
        $getTextureHeight(): number;
        $getScaleBitmapWidth(): number;
        $getScaleBitmapHeight(): number;
        /**
         * @private
         * 表示bitmapData.width
         */
        $sourceWidth: number;
        /**
         * @private
         * 表示bitmapData.height
         */
        $sourceHeight: number;
        /**
         * @private
         */
        $bitmapData: BitmapData;
        /**
         * @private
         */
        $ktxData: ArrayBuffer;
        /**
         * @private
         */
        $rotated: boolean;
        /**
         * The BitmapData object being referenced.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 被引用的 BitmapData 对象。
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get bitmapData(): BitmapData;
        set bitmapData(value: BitmapData);
        /**
        * Set the BitmapData object.
        * @version Egret 3.2.1
        * @platform Web,Native
        * @language en_US
        */
        /**
         * 设置 BitmapData 对象。
         * @version Egret 3.2.1
         * @platform Web,Native
         * @language zh_CN
         */
        _setBitmapData(value: BitmapData): void;
        /**
         * The KTX object being referenced.
        * @version Egret 5.2.21
        * @platform Web,Native
        * @language en_US
        */
        /**
         * 被引用的 KTXData 对象。
         * @version Egret 5.2.21
         * @platform Web,Native
         * @language zh_CN
         */
        get ktxData(): ArrayBuffer;
        set ktxData(data: ArrayBuffer);
        /**
        * Set the KTXData object.
        * @version Egret 3.2.1
        * @platform Web,Native
        * @language en_US
        */
        /**
         * 设置 KTXData 对象。
         * @version Egret 3.2.1
         * @platform Web,Native
         * @language zh_CN
         */
        _setKtxData(value: ArrayBuffer): void;
        /**
         * @private
         * 设置Texture数据
         * @param bitmapX
         * @param bitmapY
         * @param bitmapWidth
         * @param bitmapHeight
         * @param offsetX
         * @param offsetY
         * @param textureWidth
         * @param textureHeight
         * @param sourceWidth
         * @param sourceHeight
         */
        $initData(bitmapX: number, bitmapY: number, bitmapWidth: number, bitmapHeight: number, offsetX: number, offsetY: number, textureWidth: number, textureHeight: number, sourceWidth: number, sourceHeight: number, rotated?: boolean): void;
        /**
         * @deprecated
         */
        getPixel32(x: number, y: number): number[];
        /**
         * Obtain the color value for the specified pixel region
         * @param x  The x coordinate of the pixel region
         * @param y  The y coordinate of the pixel region
         * @param width  The width of the pixel region
         * @param height  The height of the pixel region
         * @returns  Specifies the color value for the pixel region
         * @version Egret 3.2.1
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 获取指定像素区域的颜色值
         * @param x  像素区域的X轴坐标
         * @param y  像素区域的Y轴坐标
         * @param width  像素区域的宽度
         * @param height  像素区域的高度
         * @returns  指定像素区域的颜色值
         * @version Egret 3.2.1
         * @platform Web
         * @language zh_CN
         */
        getPixels(x: number, y: number, width?: number, height?: number): number[];
        /**
         * Convert base64 string, if the picture (or pictures included) cross-border or null
         * @param type Type conversions, such as "image / png"
         * @param rect The need to convert the area
         * @param smoothing Whether to convert data to the smoothing process
         * @returns {any} base64 string
         * @version Egret 2.4
         * @language en_US
         */
        /**
         * 转换成base64字符串，如果图片（或者包含的图片）跨域，则返回null
         * @param type 转换的类型，如  "image/png"
         * @param rect 需要转换的区域
         * @param {any} encoderOptions 编码用的参数
         * @returns {any} base64字符串
         * @version Egret 2.4
         * @language zh_CN
         */
        toDataURL(type: string, rect?: egret.Rectangle, encoderOptions?: any): string;
        /**
         * Crop designated area and save it as image.
         * native support only "image / png" and "image / jpeg"; Web browser because of the various implementations are not the same, it is recommended to use only these two kinds.
         * @param type Type conversions, such as "image / png"
         * @param filePath The path name of the image (the home directory for the game's private space, the path can not have "../",Web supports only pass names.)
         * @param rect The need to convert the area
         * @version Egret 2.4
         * @platform Native
         * @language en_US
         */
        /**
         * 裁剪指定区域并保存成图片。
         * native只支持 "image/png" 和 "image/jpeg"；Web中由于各个浏览器的实现不一样，因此建议也只用这2种。
         * @param type 转换的类型，如  "image/png"
         * @param filePath 图片的名称的路径（主目录为游戏的私有空间，路径中不能有 "../"，Web只支持传名称。）
         * @param rect 需要转换的区域
         * @version Egret 2.4
         * @platform Native
         * @language zh_CN
         */
        saveToFile(type: string, filePath: string, rect?: egret.Rectangle): void;
        /**
         * dispose texture
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 释放纹理
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        dispose(): void;
        $batchManager: GYLite.BatchManager;
        get batchManager(): GYLite.BatchManager;
        clear(): void;
    }
}
