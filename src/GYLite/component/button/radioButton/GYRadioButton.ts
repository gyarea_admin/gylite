/**
 @author 迷途小羔羊
 2015.6.5
 */
module GYLite
{
									
	
	/**单选按钮*/
	export class GYRadioButton extends GYButton
	{		
		private _radioGroup:GYRadioGroup;
		public constructor(skin:any=null)
		{
			super(skin);
		}
		/**获取主题皮肤，自定义皮肤请实现IRadioButtonSkin接口*/
		protected getThemeSkin():IGYSkin
		{
			let s = this;
			return GYSprite.skinTheme.GetRadioButtonSkin();
		}
		protected mClk(e:egret.TouchEvent):void
		{
			let s = this;
			if(s._radioGroup)
				s._radioGroup.selectedButton = this;
			super.mClk(e);			
		}
		
		public set gap(val:number)
		{
			let s = this;
			(s._skin as IRadioButtonSkin).gap = val;
		}
		/**文本与复选框的间隙*/
		public get gap():number
		{
			let s = this;
			return (s._skin as IRadioButtonSkin).gap;
		}
		public set radioGroup(val:GYRadioGroup)
		{
			let s = this;
			s._radioGroup = val;
		}
		/**单选按钮组，同组互斥*/
		public get radioGroup():GYRadioGroup
		{
			let s = this;
			return s._radioGroup;
		}
		public dispose(disposeChild:boolean=true, removeChild:boolean = true, forceDispose:boolean=false):void
		{
			super.dispose(disposeChild, removeChild, forceDispose);
			this.param = null;
		}
	}
}