class MsgBase {
	protected _dict: any;
    protected _protocolDict:any;
	protected _runProtocol: string;
	protected _waitUnReg: Array<any>;
	protected _localSensor:any;	
	private static _curMsg: MsgBase;
	private static _defaultMsg: MsgBase = new MsgBase;
	public constructor() {
		let s = this;
		s._dict = {};
        s._protocolDict = {};		
		s._waitUnReg = [];
		s._protocolDict[100150]={logLevel:(1<<2)};
	}	
	/**是否存在父级通讯*/
	// public static haveMsgHandle(): boolean {
	// 	MsgBase.getMsgHandle();
	// 	return MsgBase._curMsg != null;
	// }
	/**获取当前的父级通讯对象*/
	// public static getMsgHandle(): MsgBase {
	// 	if (MsgBase._curMsg == null)
	// 	{
	// 		let isPCDebug:boolean = (UserData.getInstance().getQueryVariable("device") == "PCDebug");			
	// 		if(isPCDebug)
	// 			MsgBase._curMsg = null;
	// 		else if(egret.nativeRender)
	// 			MsgBase._curMsg = (Browser.onMobile || Browser.onAndroid ? MsgMobile.getInstance() : null)
	// 		else 
	// 			MsgBase._curMsg = (window != window.parent || UserData.getInstance().getQueryVariable("device") == "PC") ? MsgDom.getInstance() : (Browser.onMobile || Browser.onAndroid ? MsgMobile.getInstance() : null);
	// 	}			
	// 	if (MsgBase._curMsg == null)
	// 		return MsgBase._defaultMsg;
	// 	return MsgBase._curMsg;
	// }
	/**注册协议监听
	 * @param protocol 协议号
	 * @param func 回调
	 * @param thisObj this
	 * @param ret 数据结构处理
	 * @param nativeData 是否使用本地模拟数据
	*/
	public regMsg(protocol, func: Function, thisObj: any, ret:BaseRet=null, native:boolean=false): void {
		let s = this;
		if (s._dict[protocol] == null)
			s._dict[protocol] = [];
		let arr: Array<any> = s._dict[protocol];
		let len: number;
		let obj: any;
		len = arr.length;
		while (--len > -1) {
			obj = arr[len];
			if (obj.msgCallBack == func && obj.thisObject == thisObj) {
				return;
			}
		}
		obj = { msgCallBack: func, thisObject: thisObj, ret:ret, native:native};
		arr.push(obj);
	}
    public sendMsg(protocol: string, data: any = null, logLevel:number=Log.VERBOSE): void {
        let s = this;
        if(s._protocolDict[protocol] == null)
            s._protocolDict[protocol] = {};
        s._protocolDict[protocol].logLevel = logLevel;		
	}
	protected recvMsg(data: any): void {
		let s = this;
		if (data.protocol) {			
			s._runProtocol = data.protocol;
			let len: number;			
			let arr: Array<any> = s._dict[data.protocol];
			if (arr) {
				let ret:BaseRet;
				len = arr.length;
				while (--len > -1) 
				{				
					ret = arr[len].ret;	
					if(ret)
					{
						ret.clear();
						data.data = arr[len].native?ret.nativeRet(data.data):ret.dataRet(data.data);
					}						
					arr[len].msgCallBack.call(arr[len].thisObject, data)
				}
			}
			//防止非异步的数据通知产生，延时到遍历结束后进行反注册
			if (s._waitUnReg.length > 0) {
				arr = s._dict[s._runProtocol];
				if (arr) {
					len = s._waitUnReg.length;
					while (--len > -1) {
						arr.splice(s._waitUnReg[len], 1);
					}
				}
				s._waitUnReg.length = 0;
			}
			s._runProtocol = null;
		}
	}	
	/**注销协议监听*/
	public unregMsg(protocol, func: Function, thisObj: any): void {
		let s = this;
		let arr: Array<any> = s._dict[protocol];
		if (arr) {
			let len: number;
			let obj: any;
			len = arr.length;
			while (--len > -1) {
				obj = arr[len];
				if (obj.msgCallBack == func && obj.thisObject == thisObj) {
					if (s._runProtocol == protocol)
						s._waitUnReg.unshift(len);
					else
						arr.splice(len, 1);
					return;
				}
			}
		}
	}		
}