declare module GYLite {
    class Thread {
        static threads: {
            [id: number]: Thread;
        };
        static idc: number;
        /**执行线程函数
         * @param id 线程id 固定线程参考ThreadID常量，如果传入-1，则创建一个唯一的id，注意自动创建id需要管理返回的线程避免内存泄露
         * @param threadFun 线程函数，线程函数的编写参考底部注释说明
         * @param threadFunParams 线程函数的传入参数数组，默认null
         * @param callBack 执行结束的回调，默认null
         * @param thisObj 线程函数和结束回调的this，默认null
         * @param immediate 是否立即执行
        */
        static runThread(id: number, threadFun: Function, threadObj: any, threadFunParams?: any[], callBack?: Function, thisObj?: any, immediate?: boolean): Thread;
        /**是否存在线程
         * @param id 线程id 参考ThreadID常量
         * @return 存在则返回线程
         */
        static hasThread(id: number): Thread;
        /**获取线程
         * @param id 线程id 参考ThreadID常量
         */
        static getThread(id: number): Thread;
        private _globalThis;
        private _func;
        varObj: any;
        private _params;
        private _cpuTime;
        private _destroyed;
        private _excCall;
        private _excCallObj;
        private _excTime;
        private _subThread;
        private _inited;
        private _runStartTime;
        private _costTime;
        private _id;
        private _running;
        private _parentThread;
        isEnd: boolean;
        returnValue: any;
        step: number;
        private _timeId;
        constructor(id: number, parentThread?: Thread);
        reset(func: Function, thisObj: any, params?: any[], cpuTime?: number): void;
        /**运行线程*/
        run(): void;
        /**设置线程结束回调*/
        setCallBack(callBack: Function, callBackObj: any): void;
        /**运行一个子级线程
         * @param index 线程索引
         * @param func 线程方法
         * @param thisObj 线程方法的this对象
         * @param params 线程方法的参数
         * @param cpuTime 线程方法每帧占用的cpu时间片
         * @return 运行成功
        */
        runSubThread(index: number, func: Function, thisObj: any, params?: any[], cpuTime?: number): boolean;
        /**获取子级线程
         * @param index 子级线程下标id
        */
        getSubThread(index: number): Thread;
        /**清理所有子级线程
         * @param indexArr 指定清理的子级线程下标id数组，默认null，清理所有
        */
        clearSubThread(indexArr?: number[]): void;
        /**线程是否已经初始化*/
        get inited(): boolean;
        init(variables?: any): void;
        /**线程变量定义集*/
        getVariable(n: string): any;
        excEnd(returnValue: any): void;
        /**线程是否执行超时*/
        isTimeout(): boolean;
        next(): void;
        destroy(): void;
        clear(): void;
        get running(): boolean;
        get params(): any[];
    }
}
