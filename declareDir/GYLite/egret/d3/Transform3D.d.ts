declare module GYLite {
    class Transform3D extends egret.EventDispatcher {
        static _tempVector30: Vector3;
        static _tempVector31: Vector3;
        static _tempVector32: Vector3;
        static _tempVector33: Vector3;
        static _tempQuaternion0: Quaternion;
        static _tempMatrix0: Matrix4x4;
        static _angleToRandin: number;
        private _localQuaternionUpdate;
        private _locaEulerlUpdate;
        private _localUpdate;
        private _worldUpdate;
        private _positionUpdate;
        private _rotationUpdate;
        private _scaleUpdate;
        private _parent;
        private _localPosition;
        private _localRotation;
        private _localScale;
        private _localRotationEuler;
        private _localMatrix;
        private _position;
        private _rotation;
        private _scale;
        private _worldMatrix;
        private _forward;
        private _up;
        private _right;
        private _owner;
        private _childs;
        /**变换中心点,注意:该中心点不受变换的影响。*/
        pivot: any;
        constructor(owner: any);
        /**
         *@private
        */
        private _updateLocalMatrix;
        /**
         *@private
        */
        private _onWorldPositionRotationTransform;
        /**
         *@private
        */
        private _onWorldPositionScaleTransform;
        /**
         *@private
        */
        private _onWorldPositionTransform;
        /**
         *@private
        */
        private _onWorldRotationTransform;
        /**
         *@private
        */
        private _onWorldScaleTransform;
        /**
         *@private
        */
        private _onWorldTransform;
        /**
         *平移变换。
        *@param translation 移动距离。
        *@param isLocal 是否局部空间。
        */
        translate: (translation: any, isLocal: any) => void;
        /**
         *旋转变换。
        *@param rotations 旋转幅度。
        *@param isLocal 是否局部空间。
        *@param isRadian 是否弧度制。
        */
        rotate: (rotation: any, isLocal: any, isRadian: any) => void;
        /**
         *观察目标位置。
        *@param target 观察目标。
        *@param up 向上向量。
        *@param isLocal 是否局部空间。
        */
        lookAt: (target: any, up: any, isLocal: any) => void;
        /**
         *@private
        */
        get _isFrontFaceInvert(): boolean;
        /**
         *获取所属精灵。
        */
        get owner(): any;
        /**
         *设置局部旋转。
        *@param value 局部旋转。
        */
        /**
         *获取局部旋转。
        *@return 局部旋转。
        */
        get localRotation(): Quaternion;
        set localRotation(value: Quaternion);
        /**
         *设置世界矩阵。
        *@param value 世界矩阵。
        */
        /**
         *获取世界矩阵。
        *@return 世界矩阵。
        */
        get worldMatrix(): Matrix4x4;
        set worldMatrix(value: Matrix4x4);
        /**
         *获取世界矩阵是否需要更新。
        *@return 世界矩阵是否需要更新。
        */
        worldNeedUpdate(): boolean;
        /**
         *设置局部矩阵。
        *@param value 局部矩阵。
        */
        /**
         *获取局部矩阵。
        *@return 局部矩阵。
        */
        get localMatrix(): Matrix4x4;
        set localMatrix(value: Matrix4x4);
        /**
         *设置关联虚拟变换。
        *@param value 虚拟变换。
        */
        /**
         *获取关联虚拟变换。
        *@return 虚拟变换。
        */
        /**
         *设置局部位置。
        *@param value 局部位置。
        */
        /**
         *获取局部位置。
        *@return 局部位置。
        */
        get localPosition(): Vector3;
        set localPosition(value: Vector3);
        /**
         *设置世界位置。
        *@param value 世界位置。
        */
        /**
         *获取世界位置。
        *@return 世界位置。
        */
        get position(): Vector3;
        set position(value: Vector3);
        /**
         *设置局部缩放。
        *@param value 局部缩放。
        */
        /**
         *获取局部缩放。
        *@return 局部缩放。
        */
        get localScale(): Vector3;
        set localScale(value: Vector3);
        /**
         *设置局部空间的旋转角度。
        *@param value 欧拉角的旋转值，顺序为x、y、z。
        */
        /**
         *获取局部空间的旋转角度。
        *@return 欧拉角的旋转值，顺序为x、y、z。
        */
        get localRotationEuler(): Vector3;
        set localRotationEuler(value: Vector3);
        /**
         *设置世界旋转。
        *@param value 世界旋转。
        */
        /**
         *获取世界旋转。
        *@return 世界旋转。
        */
        get rotation(): Quaternion;
        set rotation(value: Quaternion);
        /**
         *设置世界缩放。
        *@param value 世界缩放。
        */
        /**
         *获取世界缩放。
        *@return 世界缩放。
        */
        get scale(): Vector3;
        set scale(value: Vector3);
        /**
         *设置局部空间的旋转角度。
        *@param 欧拉角的旋转值，顺序为x、y、z。
        */
        set rotationEuler(value: Vector3);
        /**
         *获取向前方向。
        *@return 向前方向。
        */
        get forward(): Vector3;
        /**
         *获取向上方向。
        *@return 向上方向。
        */
        get up(): Vector3;
        /**
         *获取向右方向。
        *@return 向右方向。
        */
        get right(): Vector3;
        /**
         *设置父3D变换。
        *@param value 父3D变换。
        */
        /**
         *获取父3D变换。
        *@return 父3D变换。
        */
        get parent(): Transform3D;
        set parent(value: Transform3D);
    }
}
