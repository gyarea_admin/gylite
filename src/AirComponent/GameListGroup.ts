module GYLite
{
									
			
	export class GameListGroup extends GYUIComponent
	{
		private gyListV:GYListV;
		private gyGridV:GYGridV;
		private gyGridH:GYGridH;
		private win:GYTitleWindow;
		private leftGrp:GYGroup;
		public constructor()
		{
			super();
			var s = this;
			s.enableBatch(true);
			s.setBatchAtlasName("Img/ui.png");
			s.init();
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private addToStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.width = s.parent.width;
			s.height = s.parent.height;
			s.addElement(s.win);
		}
		private removeFromStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private init():void
		{var s = this;
			s.win = new GYTitleWindow;
			s.win.width = 560;
			s.win.height = 440;
			s.win.verticalCenter = 0;
			s.win.horizonalCenter = 0;
			s.win.setTitle("羔羊商店");
			s.win.title.color = 0xffffae;
			s.win.title.width = s.win.width;
			s.win.title.align = "center";			
			s.win.title.y = 5;
			s.win.title.size = 14;
			s.win.closeBtn.right = s.win.closeBtn.y = 0;
			s.win.touchEnabled = true;
			var selectedNum:number =1;
			s.leftGrp = new GYGroup;
			s.leftGrp.clipAndEnableScrolling = true;
			s.leftGrp.height = 400;
			s.leftGrp.width = 176;
			s.leftGrp.easeGapY = true;
			s.leftGrp.canDrag = true;
			s.leftGrp.x = 12;
			s.leftGrp.y = 30;
			s.win.addElement(s.leftGrp);
			var item:GameItem = new GameItem;
			item.x = 0;
			item.y = 0;
			item.setData({Name:"风弟头目",index:999});
			s.leftGrp.addElement(item);
			/*GYListV*/
			s.gyListV=new GYListV(335,s.CreateGrid,s);
			// s.gyListV.scrollPadTop = 20;
			// s.gyListV.scrollPadBottom = 20;
			// s.gyListV.virtual_layout = false;
			s.gyListV.width = s.leftGrp.width-16;
			s.gyListV.dataProvider = s.getData(40);
			s.gyListV.x = 0;
			s.gyListV.y = 65;			
			// s.gyListV.scrollerPolicy = 2;
			s.gyListV.canDrag = true;
			s.gyListV.canSelectNum = selectedNum;
			s.gyListV.scrollPosLimit = 60;
			// s.gyListV.vScroller.limitMin = 100;
			// s.gyListV.vScroller.limitMax = 400;
			s.gyListV.scrollPosLimit = s.gyListV.boxH;
			s.leftGrp.addElement(s.gyListV);
			Main.instance.setTip(s.gyListV, "纵向列表");
			s.touchEnabled = true;
			
			s.gyGridV=new GYGridV(320,400,s.CreateGrid,s);
			// s.gyGridV.scrollPadTop = 100;
			// s.gyGridV.scrollPadBottom = 100;
			// s.gyGridV.virtual_layout = false;
			s.gyGridV.dataProvider = s.getData(50);
			s.gyGridV.x = 212;
			s.gyGridV.y = 30;
			s.gyGridV.canDrag = true;
			// s.gyGridV.vScroller.limitMin = 100;
			// s.gyGridV.vScroller.limitMax = 400;
			s.win.addElement(s.gyGridV);
			s.gyGridV.canSelectNum = selectedNum;
			Main.instance.setTip(s.gyGridV, "纵向网格，其他网格功能看看excel就知道有没有");
			s.touchEnabled = true;
			s.gyGridV.addEventListener(egret.TouchEvent.TOUCH_TAP,function():void{
				s.gyListV.scrollToPreItem();
			},s);

			// s.gyGridH=new GYGridH(310,400,s.CreateGrid,s);			
			// s.gyGridH.dataProvider = s.getData(50);
			// s.gyGridH.x = 612;
			// s.gyGridH.y = 30;
			// s.gyGridH.canDrag = true;			
			// s.win.addElement(s.gyGridH);
			// s.gyGridH.canSelectNum = selectedNum;
			// Main.instance.setTip(s.gyGridH, "横向网格，其他网格功能看看excel就知道有没有");
			// s.touchEnabled = true;			

			// var gyListV:GYListV=new GYListV(400,s.CreateGrid);
			// gyListV.width = 160; 
			// gyListV.dataProvider = s.getData(10);
			// gyListV.x = 10;
			// gyListV.y = 100;
			// // gyListV.virtual_layout = false;
			// s.addElement(gyListV);
			// setTimeout(function(gyListV):void{
			// console.log(gyListV.width);
			// },1000,gyListV
			// );
			
			// s.win.setWheelFunc(function(e):void{
			// 	if(e.deltaY > 0)
			// 	{
			// 		gyListV.height += 5;
			// 	}
			// 	else
			// 	{
			// 		gyListV.height -= 5;
			// 	}
			// });

			// var gyListH:GYListH=new GYListH(300,s.CreateGrid);
			// gyListH.dataProvider = [
			// 	s.getData(10),
			// 	s.getData(10),
			// 	s.getData(10),
			// 	s.getData(10),
			// 	s.getData(10),s.getData(10),s.getData(10),s.getData(10),s.getData(10),s.getData(10),
			// 	s.getData(10)
			// ];
			// gyListH.x = 612;
			// gyListH.y = 30;						
			// s.win.addElement(gyListH);
			// s.win.setWheelFunc(function(e):void{
			// 	if(e.deltaY > 0)
			// 	{
			// 		gyListH.width += 5;
			// 	}
			// 	else
			// 	{
			// 		gyListH.width -= 5;
			// 	}
			// },s);
		}
		/*grid */
		private CreateGrid():IItemRender
		{var s = this;
			var t:GameItem;
			t=new GameItem;
			return t;
		}
		private getData(len:number=0):Array<any>
		{var s = this;
			var arr:Array<any>=[];
			var obj:any;
			while(--len>-1)
			{
				obj={Name:"风弟"+len + "号",index:len};
				arr.push(obj);
			}
			return arr;
		}
	}
}