declare module GYLite {
    class Listener {
        private _listenerVec;
        private _listenId;
        private static c;
        constructor();
        length(): number;
        addListener(listener: IListener): number;
        removeListener(listener: IListener): number;
        dataChange(): void;
        get listenId(): number;
        clear(): void;
        dispose(): void;
    }
}
