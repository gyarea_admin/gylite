module GYLite
{					
	export class TextRenderGroup extends GYGroup
	{		
		private _renderSprites:TextRenderSprite[];
		private _paused:boolean;
		private _curNum:number;
		private _sp:GYSprite;
		private _curInd:number;
		// public txt:GYLite.GYText;
		public constructor()
		{
			super();
			var s = this;			
			s._paused = false;
			s.mouseThrough = true;
			s.percentWidth = 1;
			s._curNum = 0;
			s.init();
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private addToStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.run();
			// s.touchEnabled = true;
			// s.rectHit = true;
			// s.width = 1920;
			// s.height = 1080;
			// s.addEventListener(egret.TouchEvent.TOUCH_TAP,s.tap,s)
			// s.addRender(3,6);
		}
		private removeFromStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.stop();			
		}
		private init():void
		{var s = this;
			s._renderSprites = [];		
			s._sp= new GYSprite;
			s.addElement(s._sp);
			// s.txt = s.getTxt(s.width - 100,100,0,"当前数量：0");
			// s.txt.right = 100;
		}
		private tap(e:egret.TouchEvent):void
		{
			let s = this;
			// s.stop();
			if(s._paused)
			{
				if(GYKeyboard.getInstance().isCtrlDown())
					s.addRenders(1);
				else 
					s.removeRenders(GYKeyboard.getInstance().isShiftDown()?10:1);
				return;
			}
			s.pause();
		}
		private run():void
		{
			let s= this;
			CommonUtil.addStageLoop(s.loop,s);
		}
		private loop(t:number):void
		{
			let s= this;
			let rSp:TextRenderSprite;
			let len:number,len2:number,ind:number;
			
			if(s._paused)
				return;
			len2 = len = s._renderSprites.length;			
			while(--len>-1)
			{
				rSp = s._renderSprites[len];				
				s._curInd = len;
				if(rSp.disposed)
					s._renderSprites.splice(len,1);
				else
				{
					rSp.loop(t);
				}
								
			}
			len = s._renderSprites.length;
			if(len > 11388)
			{
				// CommonUtil.delStageLoop(s.loop,s);
				console.log("max:"+len)
				return;
			}				
			len = Math.min(11388 - s._renderSprites.length, 5);
			while(--len>-1)
				s.addRender(0xffffff*Math.random()|0);
				// s.addRender(0xfff000);
			// s.txt.text = "当前数量：" + s._renderSprites.length;
			// s.addElement(s.txt);
		}
		public swap(rSp:TextRenderSprite,t:number):void
		{
			let x:number,y:number;
			let tempSp:TextRenderSprite;
			let ind:number;
			let s= this;
			let len2:number;
			len2 = s._renderSprites.length;
			ind = s._curInd + t%(len2 - s._curInd);
			tempSp = s._renderSprites[ind];	
			x = tempSp.$x;
			y = tempSp.$y;
			tempSp.$x = rSp.$x;
			tempSp.$y = rSp.$y;
			rSp.$x = x;
			rSp.$y = y;					
			tempSp.changeTime = t;			
		}
		private stop():void
		{
			let s= this;
			s._paused = false;
			CommonUtil.delStageLoop(s.loop,s);
			let len:number;
			len = s._renderSprites.length;
			while(--len>-1)
			{				
				s._renderSprites[len].dispose();
			}
			s._curNum = 0;
			s._renderSprites.length = 0;
		}
		private pause():void
		{
			let s= this;
			s._paused = !s._paused;
		}		
		private addRender(c:number,size:number=12):void
		{
			let s = this;
			let rSp:TextRenderSprite;
			let col:number,row:number;
			col = s._curNum % 156;
			row = s._curNum / 156 | 0;
			rSp = new TextRenderSprite(s,c,size);
			rSp.setBatchAtlasName("textBatch");
			rSp.setPos(20 + col * 12,80 + row * 12);			
			s._sp.addElement(rSp);
			s._renderSprites.push(rSp);
			++s._curNum;
		}
		private addRenders(num:number=1):void
		{
			let s= this;
			let len:number;
			len = num;
			while(--len>-1)
			{
				s.addRender(0xffffff*Math.random()|0);
				// s.addRender(0,1);
			}
		}
		private removeRenders(num:number=1):void
		{
			let s= this;
			let len:number,ind:number;
			len = s._renderSprites.length;
			ind = len - num - 1;
			if(ind < -1)
				ind = -1;
			while(--len>ind)
			{
				s._renderSprites[len].dispose();
				s._renderSprites.splice(len,1);
			}
			s._curNum -= num;
		}
		
		
	}
}