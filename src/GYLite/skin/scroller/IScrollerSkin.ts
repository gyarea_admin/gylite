module GYLite
{
						
	export interface IScrollerSkin extends IGYSkin
	{
		// readonly arrowBtn1:GYButton;
		// readonly arrowBtn2:GYButton;
		// readonly scrollBar:GYButton;
		// readonly sliderBg:GYScaleSprite;
		arrowBtn1:GYButton;
		arrowBtn2:GYButton;
		scrollBar:GYButton;
		sliderBg:GYScaleSprite;
	}
}