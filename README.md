# GYLite
#### 严正声明
羔羊引擎所有源码虽然开源，但并不主张支持任何以下类别的企业使用，如果在此类企业（不方便列举，你们懂的，此处用此类称呼）的开发者，请不要使用本作者的代码服务此类企业，自觉绕道而行，况且此类企业也不值得你们付出，有觉悟的开发者也应该远离这种公司，避免失去作为人应有的属性

1、存在欺诈、压榨、违法辞退、刻意拖欠工资，从而引起劳动纠纷的企业

2、对开发者不尊重的，不看开发细节质量，只看考勤加班时长的企业

3、公司制度处处针对员工，提防员工，社会风评极差的企业


#### 介绍
羔羊引擎，是一个开源引擎框架，可以移植到任何平台，此案例以白鹭引擎为例，使用白鹭引擎的底层渲染，构建上层应用架构，同时此处对白鹭做了性能优化

#### 软件架构
GYLite+egret


#### 安装教程
无需安装，直接使用vscode运行demo工程



#### 使用说明

1.  制作h5项目，只需要引入GYLite库文件夹即可
2.  demo工程内的GYLite_res是由羔羊打包工具GYPackage打包的，关于打包编译发布项目，可[点击此处跳转学习GYPackager的使用](https://gitee.com/gyarea_admin/gypacker)，测试可以直接使用GYLoader加载资源，用GYLite组件显示
#### 参与贡献

在羔羊引擎的加持下，白鹭引擎突破性能瓶颈，得到质的飞跃！

1、文字动态合批，可以合到任何指定UI图集中，只要不超过一张图集的空间，并且不会再受到文本数量限制，你可以使用无限数量文字
（PS:此前白鹭文字是整个文本框作为一个drawcall绘制的，并且文本框大小会受到限制，如果超过一定尺寸，白鹭缩小文本框绘制后再放大导致文字模糊）
2、graphics绘制，也参与合批，并且绘制大小也不受限制（PS:此前绘制尺寸不得大于1365），新增beginBitmapFill、位图线条、渐变线条等接口，仿照as3的Graphics类
3、图片、文字、graphis均能合在一个批次

下面看效果
组件里面带文字，drawcall只有9，未优化前是30左右
![输入图片说明](https://foruda.gitee.com/images/1669362158920525249/9c0063ba_2081937.png "text.png")
列表里面带文字，只有10，未优化前70多
![输入图片说明](https://foruda.gitee.com/images/1669362171616186906/a8e2bcb1_2081937.png "list.png")
这个巨大的excel，未优化前有400个drawcall，当前只有14左右
![输入图片说明](https://foruda.gitee.com/images/1669362182987313908/cff8f275_2081937.png "excel.png")
文字大量渲染，性能也是很高的
![输入图片说明](https://foruda.gitee.com/images/1669362198152128118/8f0e33cd_2081937.png "textRd.png")
各种graphics绘制，不会产生额外的drawcall
![输入图片说明](https://foruda.gitee.com/images/1669362208786953283/88008491_2081937.png "graphics.png")

目前此版本会产生drawcall的因素只有跨图集，滚动裁切和滤镜

合批使用方法，只需要用GYSprite或其子类作为父级容器，通过enableBatch方法打开容器的合批功能，并且通过setBatchAtlasName方法设置指定图集名称（请注意，如果不设置合批的图集名称，则内部会使用一个默认图集名称），则此容器下面的所有小于512的图将会合批到一个指定setBatchAtlasName指定名称的图集中（指定自己已经打好的图集名称也是可以的，注意前提是你的图集有足够多的空闲空间）
注意图集是否合满，默认创建图集是2048×2048，如果合满，则会报异常，可以使用下面方法查看atlasId的图集的情况
GYLite.AtlasRender.getInstance()._atlasDict[atlasId].debugShow()，如下图

![输入图片说明](https://foruda.gitee.com/images/1669362221655358888/1f281dc7_2081937.png "batch.png")

demo：http://zsh.freecao.com/GYLite_Batch/main.html?g_v=20221124224649&entry_v=
