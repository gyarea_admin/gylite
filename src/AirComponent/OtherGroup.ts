module GYLite
{
																
	
			
	export class OtherGroup extends GYGroup
	{
		private gyCheckBox:GYCheckBox;
		private gyRadioButton1:GYRadioButton;
		private gyRadioButton2:GYRadioButton;
		private gyRadioButton3:GYRadioButton;
		private gyRadioButton4:GYRadioButton;
		private gyProgressBar:GYProgressBar;
		private gyProgressBar2:GYProgressBar;
		private gySlider:GYSlider;
		private gySlider2:GYSlider;
		private gyMenu:GYMenu;
		private gyComboBox:GYComboBox;		
		public constructor()
		{
			super();
			var s = this;			
			s.init();
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private addToStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
		}
		private removeFromStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private init():void
		{var s = this;
			var initX:number = 100,initY:number = 100;
			/*GYMenu*/
			s.gyMenu=GYSprite.skinTheme.GetMenu(50,92);
			s.gyMenu.x = 25 + initX;
			s.gyMenu.y = 300 + initY;
			s.gyMenu.listV.canSelectNum = 1;
			s.gyMenu.listV.addEventListener(GYViewEvent.SELECTED, s.clkMenu,s);
			s.gyMenu.dataProvider = s.getMenuData();
			s.addElement(s.gyMenu);
			Main.instance.setTip(s.gyMenu, "菜单，内部其实用GYListV实现的\n不要我问什么是GYListV", 40);
			s.gyMenu.touchEnabled = true;
			
			/*GYComboBox*/
			
			s.gyCheckBox =new GYCheckBox;
			s.gyCheckBox.x = 25 + initX;
			s.gyCheckBox.y = 100 + initY;
			s.gyCheckBox.gap = 10;
			s.gyCheckBox.selected = true;
			s.gyCheckBox.label = "复选框";
			// s.gyCheckBox.labelDisplay.color = ColorUtil.fontColor;
			s.addElement(s.gyCheckBox);
			Main.instance.setTip(s.gyCheckBox, "就是一个复选框，勾勾有点大", 60, -30);			
			
			s.gyRadioButton1 = new GYRadioButton;
			s.gyRadioButton1.x = 25 + initX;
			s.gyRadioButton1.y = 150 + initY;
			s.gyRadioButton1.gap = 10;
			s.gyRadioButton1.label = "单选框1";
			// s.gyRadioButton1.labelDisplay.color = ColorUtil.fontColor;
			s.gyRadioButton2 = new GYRadioButton;
			s.gyRadioButton2.x = 125 + initX;
			s.gyRadioButton2.y = 150 + initY;
			s.gyRadioButton2.gap = 10;
			s.gyRadioButton2.label = "单选框2";
			// s.gyRadioButton2.labelDisplay.color = ColorUtil.fontColor;
			s.gyRadioButton3 = new GYRadioButton;
			s.gyRadioButton3.x = 225 + initX;
			s.gyRadioButton3.y = 150 + initY;			
			s.gyRadioButton3.gap = 10;
			s.gyRadioButton3.label = "单选框3";
			// s.gyRadioButton3.labelDisplay.color = ColorUtil.fontColor;
			s.gyRadioButton4 = new GYRadioButton;
			s.gyRadioButton4.x = 325 + initX;
			s.gyRadioButton4.y = 150 + initY;
			s.gyRadioButton4.gap = 10;
			s.gyRadioButton4.label = "单选框4";
			// s.gyRadioButton4.labelDisplay.color = ColorUtil.fontColor;
			s.gyRadioButton1.radioGroup = s.gyRadioButton2.radioGroup = s.gyRadioButton3.radioGroup = s.gyRadioButton4.radioGroup = new GYRadioGroup;
			s.gyRadioButton1.radioGroup.selectedButton = s.gyRadioButton1;
			s.addElement(s.gyRadioButton1);
			s.addElement(s.gyRadioButton2);
			s.addElement(s.gyRadioButton3);
			s.addElement(s.gyRadioButton4);
			Main.instance.setTip(s.gyRadioButton1, "单选框1，radioGroup属性限制组", 60, -30);
			Main.instance.setTip(s.gyRadioButton2, "单选框2，radioGroup属性限制组", 60, -30);
			Main.instance.setTip(s.gyRadioButton3, "单选框3，radioGroup属性限制组", 60, -30);
			Main.instance.setTip(s.gyRadioButton4, "单选框4，radioGroup属性限制组", 60, -30);
			
			s.gyProgressBar = new GYProgressBar(null,GYProgressBar.RIGHT_LEFT);
			s.gyProgressBar.mode = ScaleMode.SCALE;
			s.gyProgressBar.x = 25 + initX;
			s.gyProgressBar.y = 200 + initY;
			s.gyProgressBar.max = 100;
			s.gyProgressBar.value = 0;
			s.gyProgressBar.barX = 0;
			s.gyProgressBar.barY = 0;
			s.gyProgressBar.barMax = 283;
			s.gyProgressBar.width = 283;
			s.gyProgressBar.height = (GYSprite.skinTheme as GYSkinTheme).progress_backBD.textureHeight;
			s.gyProgressBar.moveBack = true;
			s.gyProgressBar.label = "50%";			
			s.gyProgressBar.labelDisplay.width = s.gyProgressBar.width;
			s.gyProgressBar.labelDisplay.color = ColorConst.fontColor;
			s.gyProgressBar.labelDisplay.y = s.gyProgressBar.height - s.gyProgressBar.labelDisplay.textHeight >> 1;
			s.gyProgressBar.labelDisplay.align = "center";
			s.gyProgressBar.addEventListener(GYEvent.VALUE_COMMIT, s.barChangeFunc,s);
			s.gyProgressBar.addEventListener(egret.TouchEvent.TOUCH_TAP,s.clkBar,s);
			s.addElement(s.gyProgressBar);
			Main.instance.setTip(s.gyProgressBar, "进度条，从左到右见得多了，我这个给看看从右到左的效果，点击随机变化");
			s.gyProgressBar.touchEnabled = true;
			
			// s.gyProgressBar2 = new GYProgressBar(null,GYProgressBar.DOWN_UP);
			// s.gyProgressBar2.mode = ScaleMode.SCALE;
			// s.gyProgressBar2.toolTipString = "点击随机变化";
			// s.gyProgressBar2.x = 50 + initX;
			// s.gyProgressBar2.y = 420 + initY;
			// s.gyProgressBar2.max = 100;
			// s.gyProgressBar2.value = 50;
			// s.gyProgressBar2.barMax = 283;
			// s.gyProgressBar2.width = (GYSprite.skinTheme as GYSkinTheme).progress_backBD.height;
			// s.gyProgressBar2.height = 283;
			// s.gyProgressBar2.moveBack = true;
			// s.gyProgressBar2.labelDisplay.x = (s.gyProgressBar2.height - 22 >> 1) - 30;
			// s.gyProgressBar2.labelDisplay.width = 30;
			// s.gyProgressBar2.labelDisplay.y = 119;
			// s.gyProgressBar2.labelDisplay.color = ColorUtil.fontColor;
			// s.gyProgressBar2.labelDisplay.align = "center";
			// s.gyProgressBar2.label = "50%";
			// s.gyProgressBar2.addEventListener(GYEvent.VALUE_COMMIT, s.barChangeFunc2,s);
			// s.gyProgressBar2.addEventListener(egret.TouchEvent.TOUCH_TAP,s.clkBar,s);
			// s.addElement(s.gyProgressBar2);
			// Main.instance.setTip(s.gyProgressBar2, "进度条，从左到右见得多了，我这个给看看从上到下的效果");
			// s.gyProgressBar2.visible = false;
			// s.gyProgressBar2.touchEnabled = true;
			
			s.gySlider = new GYSlider(null,GYProgressBar.LEFT_RIGHT);
			s.gySlider.mode = ScaleMode.SCALE;
			s.gySlider.x = 25 + initX;
			s.gySlider.y = 250 + initY;
			// s.gySlider.min = 30;
			s.gySlider.max = 150;
			s.gySlider.value = 50;
			s.gySlider.barMax = 105;
			// s.gySlider.barMin = 15;
			s.gySlider.width = 105;
			s.gySlider.height = (GYSprite.skinTheme as GYSkinTheme).slider_backBD.textureHeight;
			s.gySlider.sliderX = -s.gySlider.sliderBtn.width >> 1;
			s.gySlider.sliderY = s.gySlider.height - s.gySlider.sliderBtn.height >> 1;
			s.addElement(s.gySlider);
			Main.instance.setTip(s.gySlider, "滑块，其实也可以从右到左");
			s.gySlider.touchEnabled = true;
			
			// s.gySlider2 = new GYSlider(null,GYProgressBar.DOWN_UP);
			// s.gySlider2.mode = ScaleMode.SCALE;
			// s.gySlider2.x = 100 + initX;
			// s.gySlider2.y = 430 + initY;
			// s.gySlider2.max = 100;
			// s.gySlider2.value = 50;
			// s.gySlider2.barMax = 150;
			// s.gySlider2.width = (GYSprite.skinTheme as GYSkinTheme).slider_backBD.height;
			// s.gySlider2.height = 150;
			// s.gySlider2.sliderX = s.gySlider2.sliderBtn.height - s.gySlider2.height >> 1;
			// s.gySlider2.sliderY = -s.gySlider2.sliderBtn.width >> 1;
			// s.addElement(s.gySlider2);
			// s.gySlider2.visible = false;
			// Main.instance.setTip(s.gySlider2, "滑块，纵向的");
			// s.gySlider2.touchEnabled = true;
			
			s.gyComboBox = GYSprite.skinTheme.GetComboBox(50,92);
			s.gyComboBox.x = 125 + initX;
			s.gyComboBox.y = 300 + initY;
			s.gyComboBox.menu.dataProvider = s.getMenuData();
			s.gyComboBox.menu.setFilterFunc(s.menuFilter);
			s.gyComboBox.menu.listV.canSelectNum = 1;
			s.gyComboBox.menu.listV.addEventListener(GYLite.GYViewEvent.SELECTED, s.clkMenu,s);
			s.addElement(s.gyComboBox);
			Main.instance.setTip(s.gyComboBox, "组合框，真的是用menu、button、inputText组合的");			
			s.gyComboBox.touchEnabled = true;
			
		}
		/*menuGrid */
		private clkMenu(e:GYViewEvent):void
		{var s = this;
			egret.log(s.gyMenu.listV.selectedItem.getData().label);
		}
		private getMenuData():Array<any>
		{var s = this;
			var len:number=5;
			var arr:Array<any>=[];
			var obj:any;
			while(--len>-1)
			{
				obj={label:"风弟"+len + "号",index:len};
				arr.push(obj);
			}
			return arr;
		}
		private menuFilter(source:Array<any>,val:Array<any>,result:string):void
		{var s = this;
			var len:number=0;
			len = source.length;
			val.length = 0;
			while(--len > -1)
			{
				var obj:any = source[len];
				if(obj.label.indexOf(result) > -1)
					val.push(obj);
			}
		}
		
		private barChangeFunc(e:GYEvent):void
		{var s = this;
			s.gyProgressBar.label = (s.gyProgressBar.value / s.gyProgressBar.max * 100 >> 0) + "%";
		}
		private barChangeFunc2(e:GYEvent):void
		{var s = this;
			s.gyProgressBar2.label = (s.gyProgressBar2.value / s.gyProgressBar2.max * 100 >> 0) + "%";
		}
		/*progressBar*/
		private clkBar(e:egret.TouchEvent):void
		{var s = this;
			if(e.currentTarget == s.gyProgressBar)
			{
				s.gyProgressBar.moveTo(s.gyProgressBar.max*Math.random());
				s.gySlider.moveTo(s.gySlider.max*Math.random());
			}
			// else if(e.currentTarget == s.gyProgressBar2)
			// {
			// 	s.gyProgressBar2.moveTo(s.gyProgressBar2.max*Math.random());
			// 	s.gySlider2.moveTo(s.gySlider2.max*Math.random());
			// }			
		}
	}
}