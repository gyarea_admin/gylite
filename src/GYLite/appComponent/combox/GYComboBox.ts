module GYLite
{
											
				
	/**组合输入菜单, 可以使用ISkinTheme的GetComboBox快速构建
	 * <p>
	 * combobox的s.menu.listV为其下拉列表，当选中列表项，自动调用s.menu.listV.selectedItem.toString()显示选中内容到输入框，请重写s.menu.listV.selectedItem 的toString()方法以显示选择内容
	 * 请在创建BomboBox之后设置s.comboBoxBtn、s.menu和s.textInput三个属性，构建完整的comboBox
	 * </p>
	 * */
	export class GYComboBox extends GYUIComponent
	{
		protected _textInput:GYTextInput;
		protected _menu:GYMenu;
		protected _comboBoxBtn:GYButton;
		protected _autoClose:boolean;
		private _clickShow:boolean;
		public menuParent:GYSprite;
		private _menuX:number;
		private _menuY:number;
		public constructor()
		{
			super();
			var s = this;
			s._autoClose = true;
		}		
		public get list():GYLite.GYListV
		{
			return this._menu?this._menu.list:null;
		}
		public set menu(val:GYMenu)
		{
			let s = this;
			if(s._menu == val)return;
			if(s._menu)
			{				
				s.list && s.list.removeEventListener(GYViewEvent.SELECTED, s.selectItem,s);
				s._menu.dispose();
			}
			s._menu = val;
			s.list && s.list.addEventListener(GYViewEvent.SELECTED, s.selectItem,s);
			if(s._textInput)
			{
				s._menuY = s._menu.y = s._textInput.y + s._textInput.height;
				s._menuX = s._menu.x = s._textInput.x;
			}
		}
		/**comboBox的下拉菜单GYMenu 不能为null*/
		public get menu():GYMenu
		{
			let s = this;
			return s._menu;
		}
		protected selectItem(e:GYViewEvent):void
		{
			let s = this;
			s._textInput.text = s.list?s.list.selectedItem.toString():"";
			s.hideMenu();
		}		
		public set selectedData(val:any)
		{
			var s = this;
			if(s.list)s.list.selectedData = val;
			s._textInput.text = val?val.label:"";
		}
		public get selectedData():any
		{
			let s = this;
			return s.list?s.list.selectedData:null;
		}
		public set selectedIndex(val:number)
		{
			let s = this;
			if(s.list)s.list.selectedIndex = val;
			s._textInput.text = s.list && s.list.selectedData?s.list.selectedData.label:"";
		}
		public get selectedIndex():number
		{
			let s = this;
			return s.list?s.list.selectedIndex:-1;
		}
		public set textInput(val:GYTextInput)
		{
			let s = this;
			if(s._textInput == val)return;
			if(s._textInput)
			{				
				s._textInput.textInput.removeEventListener(egret.Event.CHANGE, s.textChange,s._textInput.textInput);
				s._textInput.dispose();
			}
			s._textInput=val;
			s._textInput.textInput.addEventListener(egret.Event.CHANGE, s.textChange,s._textInput.textInput);			
			s.addElement(s._textInput);
			if(s._menu)
			{
				s._menuY = s._menu.y = s._textInput.y + s._textInput.height;
				s._menuX = s._menu.x = s._textInput.x;
			}
			if(s._comboBoxBtn)
			{
				s._comboBoxBtn.y = s._textInput.y + (s._textInput.height - s._comboBoxBtn.height >> 1);
				s._comboBoxBtn.x = s._textInput.width - s._comboBoxBtn.width - 3;
			}
		}
		/**comboBox的输入框TextInput，不能为null*/
		public get textInput():GYTextInput
		{
			let s = this;
			return s._textInput;
		}		
		public set comboBoxBtn(val:GYButton)
		{
			let s = this;
			if(s._comboBoxBtn == val)
				return;
			if(s._comboBoxBtn)
			{
				s._comboBoxBtn.dispose();
			}				
			s._comboBoxBtn=val;
			s._comboBoxBtn.setOnClick(s.clkBtn,s);
			s.addElement(s._comboBoxBtn);
			s._comboBoxBtn.y = s._textInput.y + (s._textInput.height - s._comboBoxBtn.height >> 1);
			s._comboBoxBtn.x = s._textInput.width - s._comboBoxBtn.width - 3;
		}
		/***设置菜单位置*/
		public setMenuPos(x:number,y:number):void
		{
			let s= this;
			s._menuX = x;
			s._menuY = y;
			if(!s.menu.stage)
			{
				s.menu.x = x;
				s.menu.y = y;
			}			
		}
		protected themeChange(e:GYThemeEvent):void
		{
			let s = this;
			s._comboBoxBtn.skin = GYSprite.skinTheme.GetComboButtonSkin();
		}
		/**comboBox的下拉按钮GYButton，不能为null*/
		public get comboBoxBtn():GYButton
		{
			let s = this;
			return s._comboBoxBtn;
		}
		protected clkBtn(e:egret.TouchEvent):void
		{
			let s = this;
			if(s._menu.parent == null)
				s.showMenu();
			else
				s.hideMenu();
		}
		/**显示菜单*/
		public showMenu():void
		{
			let s = this;
			if(s._menu.parent == null)
			{
				if(s.menuParent)
				{
					s._menuX = s._menu.x;
					s._menuY = s._menu.y;
					var p:egret.Point = PositionUtil.convertPos(s._menuX,s._menuY,this,s.menuParent);
					s._menu.x = p.x;
					s._menu.y = p.y;
					s.menuParent.addElement(s._menu);
				}
				else
					s.addElement(s._menu);
				if(s._autoClose)
				{
					GYSprite.addStageDown(this, s.outsideRelease, s, true);
					if(s.menuParent)
						GYSprite.addStageDown(s.menu, s.outsideRelease, s, true);
				}
			}
		}
		/**隐藏菜单*/
		public hideMenu():void
		{
			let s = this;
			if(s.menuParent && s.menuParent == s._menu.parent)
				s.menuParent.removeElement(s._menu);
			else if(s._menu.parent == this)
				s.removeElement(s._menu);
			s._menu.x = s._menuX;
			s._menu.y = s._menuY;
		}
		protected textChange(e:egret.Event):void
		{
			let s = this;
			if(s._menu == null)
				return;
			var arr:Array<any> = s._menu.userFilter(s._textInput.text);
			if(arr.length == 0)
				s.hideMenu();
			else
				s.showMenu();
		}

		public get autoClose():boolean
		{
			let s = this;
			return s._autoClose;
		}

		public set autoClose(value:boolean)
		{
			let s = this;
			s._autoClose = value;
		}
		protected outsideRelease(e:GYTouchEvent):void
		{
			let s = this;
			if(e.outsideTarget == s._comboBoxBtn)return;
			if(s.menuParent == null && s == e.target || s.menu == e.target)			
			{
				s.hideMenu();
				return;
			}
			e.result = 1;
		}

		public get clickShow():boolean
		{
			let s = this;
			return s._clickShow;
		}

		public set clickShow(value:boolean)
		{
			let s = this;
			s._clickShow = value;
			if(s._clickShow)
				s._textInput.addEventListener(egret.TouchEvent.TOUCH_TAP, s.clkBtn, s);
			else
				s._textInput.removeEventListener(egret.TouchEvent.TOUCH_TAP, s.clkBtn, s);
		}
	}
}