declare module GYLite {
    class LinkButtonSkin extends GYSkin implements IButtonSkin {
        protected _stsVec: TextFormat[];
        protected _curSkin: GYText;
        constructor(skinVec: TextFormat[]);
        drawSkin(state: number): void;
        get width(): number;
        get height(): number;
        set width(val: number);
        set height(val: number);
        set label(val: string);
        get label(): string;
        get labelDisplay(): GYText;
        set labelDisplay(val: GYText);
        /**此函数只克隆s.labelDisplay的textFormat*/
        cloneLabel(): GYText;
        /**此函数复制皮肤属性 如s.label等皮肤的属性*/
        copy(skin: IGYSkin): void;
        clone(): IGYSkin;
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
    }
}
