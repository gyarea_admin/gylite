module GYLite{
    export class GradientType{
        /**线性渐变*/public static LINEAR:GradientType = new GradientType(1,"linear");
        /**径向渐变*/public static RADIAL:GradientType = new GradientType(2,"radial");
        private _id:number;
        private _name:string;
        constructor(id:number,name:string)
        {
            let s= this;
            s._id = id;
            s._name = name;
        }
        public get id():number
        {
            return this._id;            
        }
        public get name():string
        {
            return this._name;
        }
    }
}