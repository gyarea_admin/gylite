module GYLite {
	export class AtlasManager {
		private _atlasSetting:{[key:string]:IAtlasSetting};
		private _atlasVec:Array<AtlasRect>;
		private _atlasId:number;//图集id
		/**允许同时缓存的图集数量，默认20*/public maxAtlasNum:number = 20;
		/**创建图集的宽度*/public atlasWidth:number=2048;
		/**创建图集的高度*/public atlasHeight:number=2048;
		public constructor() {
			this._atlasVec = [];
			this._atlasId = 0;
			this._atlasSetting = {};
			// window["test_drawToCanvas"] = this.outputAtlas.bind(this);
			PoolUtil.setPoolLengthWarn(AtlasRect, 10000);
		}
		/**对atlasName图集进行预设信息*/
		public setAtlasSetting(atlasName:string, setting:IAtlasSetting):void
		{
			this._atlasSetting[atlasName] = setting;
		}
		public get atlasNum():number
		{
			return this._atlasId;
		}
		/**获取图集矩形数组*/
		public get atlasVec():Array<AtlasRect>
		{
			return this._atlasVec;
		}
		/**从图集管理器的图集队列中移除图集*/
		public removeAtlas(atlasRect:AtlasRect):void
		{
			let ind:number;
			let s= this;
			ind = s._atlasVec.indexOf(atlasRect);
			if(ind > -1)
			{				
				s._atlasVec.splice(ind,1);
			}
		}
		/**插入纹理
		 * @param texName 小图名称
		 * @param atlasName 图集名称
		 * @param w 图片宽度
		 * @param h 图片高度
		 * @param type 裁剪策略 排列方式0 偏向正方形 1 优先横向 2 优先纵向,默认1
		 * @param gap 图片之间的间隙，默认1
		*/
		public addTexture(texName:string,atlasName:string,w:number,h:number,type:number = 1,gap:number=1):{atlasRect:AtlasRect,atlasInfo:AtlasInfo,error:SysError}
		{
			let i:number,len:number;
			let rect:AtlasRect;						
			let s =this;
			let result:{atlasRect:AtlasRect,atlasInfo:AtlasInfo,error:SysError};
			result = {atlasRect:null,atlasInfo:null,error:null};
			if(w > AtlasRect.maxSize || h > AtlasRect.maxSize)
			{
				result.error = SysError.ATLAS_SIZE_LIMIT.throwError([atlasName,w,h]);
				return result;
			}
			//遍历已有图集，找到同名的获取子级区域，如果空间不足则报异常
			len = s._atlasVec.length;
			if(len > 0)
			{				
				for(i=0;i<len;++i)
				{
					if(s._atlasVec[i].atlasName != atlasName)
						continue;
					rect = s._atlasVec[i].cutRect(w,h,type,gap);
					if(rect)
					{
						rect.name = texName;
						result.atlasInfo = new AtlasInfo(rect.atlasName,rect.atlasId,s._atlasVec[i].width,s._atlasVec[i].height,new RectInfo(rect.name,rect.x,rect.y,rect.width,rect.height,rect.gap))						
						return result;
					}
					else
					{
						result.error = SysError.ATLAS_INSERT_SIZE_LIMIT.throwError([atlasName,texName,w,h]);				
						return result;
					}
				}
			}			
			//超出图集数量上限
			if(len == s.maxAtlasNum)
			{				
				result.error = SysError.ATLAS_NUM_LIMIT.throwError([atlasName,texName]);				
				return result;
			}
			let atlasResult:{atlasRect:AtlasRect,error:SysError};
			let atlasWidth:number,atlasHeight:number;
			if(s._atlasSetting[atlasName])
			{
				atlasWidth = s._atlasSetting[atlasName].atlasWidth;
				atlasHeight = s._atlasSetting[atlasName].atlasHeight;
			}
			else
			{
				atlasWidth = s.atlasWidth;
				atlasHeight = s.atlasHeight;
			}
			
			atlasResult = s.createAtlas(atlasWidth,atlasHeight,atlasName);
			result.atlasRect = atlasResult.atlasRect;
			rect = atlasResult.atlasRect.cutRect(w,h,type,gap);
			rect.name = texName;
			result.atlasInfo = new AtlasInfo(atlasName,rect.atlasId,atlasWidth,atlasHeight,new RectInfo(rect.name,rect.x,rect.y,rect.width,rect.height,rect.gap));			
			return result;
		}
		/**创建一个虚拟图集
		 * @param w 宽度
		 * @param h 高度
		 * @param atlasName 图集名称		 
		*/
		public createAtlas(w:number,h:number,atlasName:string,result:{atlasRect:AtlasRect,error:SysError}=null,check:boolean=false):{atlasRect:AtlasRect,error:SysError}
		{
			let rect:AtlasRect;
			let len:number;
			let s= this;			
			result = result?result:{atlasRect:null,error:null};
			if(check)
			{
				len = s._atlasVec.length;
				if(len > 0)
				{				
					while(--len>-1)					
					{
						if(s._atlasVec[len].atlasName == atlasName)
						{
							result.atlasRect = s._atlasVec[len];
							result.error = SysError.ATLAS_DUPLICATE.throwError([atlasName]);
							return result
						}
							
					}
				}
			}
			rect = s._atlasVec[s._atlasVec.length] = new AtlasRect(0,0,w,h,s,true);
			result.atlasRect = rect;
			// rect.emptyRects = [rect];
			// rect.textureRects = [];
			rect.atlasName = atlasName;
			rect.atlasId = s._atlasId++;			
			return result;
		}
		/**删除纹理
		 * @param texName 小图名称
		*/
		public removeTexture(texName:string):void
		{
			let len:number;
			let rect:AtlasRect;
			let s = this;
			len = s._atlasVec.length;
			while(--len>-1)
			{
				rect = s._atlasVec[len].findTextureRect(texName);
				if(rect)
				{
					rect.backToEmpty();
					return;
				}
			}
			// throw(new Error("移除不存在的纹理！" + texName));
		}
		private createSheetJson():any[]
		{			
			let i:number,len:number;
			let s = this;
			let arr:any[] = [];
			len = s._atlasVec.length;
			for(i=0;i<len;++i)							
				arr[arr.length] = s._atlasVec[i].createJsonSheet();
			return arr;	
		}		
		
		
		/**输入虚拟图集数据*/
		public inputJson(atlasArray:any[]):void
		{
			let i:number,len:number;
			let s = this;
			let rect:AtlasRect;
			len = atlasArray.length;
			for(i=0;i<len;++i)
			{
				rect = s._atlasVec[i] = <AtlasRect>GYLite.PoolUtil.fromPool(AtlasRect);
				rect.emptyRects = [];
				rect.textureRects = [];
				rect.atlasName = atlasArray[i].atlasName;
				rect.atlasId = atlasArray[i].altasId;
				rect.inputJson(atlasArray[i]);
			}
		}
		/**输出虚拟图集数据*/
		public outputJson():any[]
		{
			let i:number,len:number;
			let s = this;
			let arr:any[];
			arr = [];
			len = s._atlasVec.length;
			for(i=0;i<len;++i)
			{
				arr[arr.length] = s._atlasVec[i].outputJson();
			}
			return arr;
		}
	}
	export interface IAtlasSetting
	{
		/**图集宽度*/atlasWidth:number;
		/**图集高度*/atlasHeight:number;
	}
}