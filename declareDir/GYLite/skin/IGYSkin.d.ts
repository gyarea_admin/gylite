declare module GYLite {
    interface IGYSkin {
        hostComponent: GYSprite;
        clone(): IGYSkin;
        copy(val: IGYSkin): void;
        /**释放皮肤资源*/ release(): void;
    }
}
