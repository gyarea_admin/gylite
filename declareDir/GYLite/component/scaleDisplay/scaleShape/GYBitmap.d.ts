declare module GYLite {
    class GYBitmap extends egret.Bitmap implements IUpdate, IBatch, IResource {
        gyliteFill: boolean;
        protected _batchDrawParam: BatchDrawParam;
        private _batchAtlasName;
        private _batch;
        protected _invalidBatch: boolean;
        /**是否刷新中（内部使用请勿修改）*/
        updating: boolean;
        protected _WMidData: egret.Texture;
        protected _HMidData: egret.Texture;
        protected _imgWidth: number;
        protected _imgHeight: number;
        protected _scale9GridRect: Scale9GridRect;
        protected _clipX: number;
        protected _clipY: number;
        protected _drawX: number;
        protected _drawY: number;
        protected _mode: number;
        protected _invalidDraw: boolean;
        protected _renderMatrix: egret.Matrix;
        protected _repeat: boolean;
        protected _repeatWidth: number;
        protected _repeatHeight: number;
        protected _repeatX: number;
        protected _repeatY: number;
        protected _width: number;
        protected _height: number;
        protected _batchTexture: egret.Texture;
        protected _normalNode: egret.sys.NormalBitmapNode;
        protected _bitmapNode: egret.sys.BitmapNode;
        constructor(value?: egret.Texture, rect?: Scale9GridRect, batch?: boolean);
        clearBatch(): void;
        $setTexture(val: egret.Texture): boolean;
        $refreshImageData(): void;
        $updateRenderNode(): void;
        invalidBatch(): void;
        enableBatch(val: boolean): void;
        /**是否动态合批，请在文本渲染前设定，默认根据父级容器batch，如果父级也没设定，则默认false*/
        isBatch(): boolean;
        setBatchDrawParam(val: BatchDrawParam): void;
        /**合批图像的绘制样式**/
        getBatchDrawParam(): BatchDrawParam;
        validBatch(): void;
        updateView(): void;
        setBatchAtlasName(val: string): void;
        /**合批图集名称，不存在找父级容器的，如果都不存在，默认AtlasRender.defaultAtlasName**/
        getBatchAtlasName(): string;
        $getRenderNode(): egret.sys.RenderNode;
        validDraw(): void;
        private draw;
        beginBitmapFill(m: egret.Matrix, r: boolean): void;
        drawRect(tX: number, tY: number, w: number, h: number): void;
        clearBitmapFill(): void;
        set repeatWidth(val: number);
        /**重复模式的砖块宽度*/
        get repeatWidth(): number;
        set repeatHeight(val: number);
        /**重复模式的砖块高度*/
        get repeatHeight(): number;
        set repeatX(val: number);
        /**重复模式的起始X*/
        get repeatX(): number;
        set repeatY(val: number);
        /**重复模式的起始Y*/
        get repeatY(): number;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        set scale9GridRect(value: Scale9GridRect);
        /**绘制模式 @see ScaleMode
         * @param val 0 拉伸 1 裁切 2 重复 */
        set mode(val: number);
        get mode(): number;
        set clipX(val: number);
        set clipY(val: number);
        /**裁切的s.x坐标*/
        get clipX(): number;
        /**裁切的s.y坐标*/
        get clipY(): number;
        set drawX(val: number);
        set drawY(val: number);
        /**绘制点的s.x坐标*/
        get drawX(): number;
        /**绘制点的s.y坐标*/
        get drawY(): number;
        /**刷新绘图*/
        invalidDraw(): void;
        get scale9GridRect(): Scale9GridRect;
        _disposed: boolean;
        get disposed(): boolean;
        dispose(): void;
    }
}
