declare module GYLite {
    type SeparatorResult = {
        error: SysError;
        width: number;
        height: number;
        left: number;
        right: number;
        top: number;
        bottom: number;
        indices: number[];
        vertices: number[];
        uvs: number[];
    };
    class Separator {
        /**分割三角形
         * @param path 路径数组 [x1,y1,x2,y2……]
         * @param check 是否检测路径存在相交点
         * @param createUV 是否生成uv数据，生成的uv用于纹理填充 默认false
         * @param offsetX 纹理偏移量x
         * @param offsetY 纹理偏移量y
         * @param fillWidth 填充纹理的宽度 默认100
         * @param fillHeight 填充纹理的高度 默认100
         * @param matrix 纹理的matrix变换
         * @param repeat 是否重复填充 默认false
        */
        static separate(path: number[], check?: boolean, createUV?: boolean, offsetX?: number, offsetY?: number, fillWidth?: number, fillHeight?: number, matrix?: egret.Matrix, repeat?: boolean): SeparatorResult;
        /**计算矩形顶点和三角形顶点的交集路径
         * @param rectPoints 矩形顶点(顺时针)
         * @param trianglePoints 三角形顶点(顺时针)
         * @param repeat 填充图形是否重复
         * @param result 结果数组
        */
        private static triangleCutRect;
        private static tempPointsA;
        private static tempPointsB;
        private static tempPointsC;
        private static tempPointsD;
    }
}
