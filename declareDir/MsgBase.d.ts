declare class MsgBase {
    protected _dict: any;
    protected _protocolDict: any;
    protected _runProtocol: string;
    protected _waitUnReg: Array<any>;
    protected _localSensor: any;
    private static _curMsg;
    private static _defaultMsg;
    constructor();
    /**是否存在父级通讯*/
    /**获取当前的父级通讯对象*/
    /**注册协议监听
     * @param protocol 协议号
     * @param func 回调
     * @param thisObj this
     * @param ret 数据结构处理
     * @param nativeData 是否使用本地模拟数据
    */
    regMsg(protocol: any, func: Function, thisObj: any, ret?: BaseRet, native?: boolean): void;
    sendMsg(protocol: string, data?: any, logLevel?: number): void;
    protected recvMsg(data: any): void;
    /**注销协议监听*/
    unregMsg(protocol: any, func: Function, thisObj: any): void;
}
