declare module GYLite {
    class GridData {
        width: number;
        height: number;
        posX: number;
        posY: number;
        index: number;
        constructor();
    }
}
