/**
@author 迷途小羔羊
2022.09.27
*/
declare module GYLite {
    class TextTexture extends BatchTexture implements IResource, IPoolObject {
        constructor();
        protected beforeToPool(): void;
        static batchParam: BatchDrawParam;
        private static _textTextureMap;
        static getFontString(gytext: GYTextBase, format: any): string;
        static getDescription(gytext: GYTextBase, format?: any): {
            desc: string;
            font: string;
        };
        static getTextTexture(char: string, gytext: GYTextBase, format?: any): TextTexture;
    }
}
