declare namespace egret {
    /**
     * @private
     */
    let $locale_strings: any;
    /**
     * @private
     */
    let $language: string;
}
declare namespace egret.sys {
    /**
     * @private
     * 全局多语言翻译函数
     * @param code 要查询的字符串代码
     * @param args 替换字符串中{0}标志的参数列表
     * @returns 返回拼接后的字符串
     */
    function tr(code: number, ...args: any[]): string;
}
