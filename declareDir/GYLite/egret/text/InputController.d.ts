declare namespace egret {
    /**
     * @private
     * @version Egret 2.4
     * @platform Web,Native
     */
    class InputController extends HashObject {
        static typingShow: boolean;
        /**
         * @private
         */
        stageText: egret.StageText;
        /**
         * @private
         */
        private stageTextAdded;
        /**
         * @private
         */
        private _text;
        /**
         * @private
         */
        private _isFocus;
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        constructor();
        /**
         *
         * @param text
         * @version Egret 2.4
         * @platform Web,Native
         */
        init(text: TextField): void;
        /**
         * @private
         *
         */
        _addStageText(): void;
        /**
         * @private
         *
         */
        _removeStageText(): void;
        /**
         * @private
         *
         * @returns
         */
        _getText(): string;
        /**
         * @private
         *
         * @param value
         */
        _setText(value: string): void;
        /**
         * @private
         */
        _setColor(value: number): void;
        /**
         * @private
         *
         * @param event
         */
        private focusHandler;
        /**
         * @private
         *
         * @param event
         */
        private blurHandler;
        private tempStage;
        private onMouseDownHandler;
        private onMouseMoveHandler;
        $onFocus(active?: boolean): void;
        private onStageDownHandler;
        /**
         * @private
         *
         * @param event
         */
        private updateTextHandler;
        /**
         * @private
         *
         */
        private resetText;
        /**
         * @private
         *
         */
        _hideInput(): void;
        /**
         * @private
         *
         */
        private updateInput;
        /**
         * @private
         *
         */
        _updateProperties(): void;
    }
}
