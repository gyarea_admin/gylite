declare namespace egret {
    /**
     * Convert the text in html format to the object that can be assigned to the egret.TextField#textFlow property
     * @see http://edn.egret.com/cn/docs/page/146 Text mixed in a variety of style
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/text/HtmlTextParser.ts
     * @language en_US
     */
    /**
     * 将html格式文本转换为可赋值给 egret.TextField#textFlow 属性的对象
     * @see http://edn.egret.com/cn/docs/page/146 多种样式文本混合
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/text/HtmlTextParser.ts
     * @language zh_CN
     */
    class HtmlTextParser {
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        constructor();
        private replaceArr;
        private initReplaceArr;
        /**
         * @private
         *
         * @param value
         * @returns
         */
        private replaceSpecial;
        /**
         * @private
         */
        private resutlArr;
        /**
         * Convert the text in html format to the object that can be assigned to the egret.TextField#textFlow property
         * @param htmltext {string} Text in html
         * @returns {Array<egret.ITextElement>} 可赋值给 egret.TextField#textFlow Object that can be assigned to the egret.TextField#textFlow property
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 将html格式文本转换为可赋值给 egret.TextField#textFlow 属性的对象
         * @param htmltext {string} html文本
         * @returns {Array<egret.ITextElement>} 可赋值给 egret.TextField#textFlow 属性的对象
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        parse(htmltext: string): egret.ITextElement[];
        parser(htmltext: string): Array<egret.ITextElement>;
        /**
         * @private
         *
         * @param value
         */
        private addToResultArr;
        private changeStringToObject;
        /**
         * @private
         *
         * @returns
         */
        private getHeadReg;
        /**
         * @private
         *
         * @param info
         * @param head
         * @param value
         */
        private addProperty;
        /**
         * @private
         */
        private stackArray;
        /**
         * @private
         *
         * @param infoStr
         */
        private addToArray;
    }
}
