declare module GYLite {
    enum LoadType {
        /**图片*/ TYPE_IMAGE = 0,
        /**纹理*/ TYPE_TEX = 1,
        /**图集*/ TYPE_ATLAS = 2,
        /**swf指定类名的资源(弃用)*/ TYPE_SWF = 3,
        /**swf资源库(弃用)*/ TYPE_SWFS = 4,
        /**字体*/ TYPE_FONT = 5,
        /**二进制*/ TYPE_BINARY = 6,
        /**文本*/ TYPE_TEXT = 7,
        /**xml(未启用)*/ TYPE_XML = 8,
        /**json*/ TYPE_JSON = 9,
        /**variables(弃用)*/ TYPE_VARIABLES = 10,
        /**swf片段(弃用)*/ TYPE_MOVIE = 11,
        /**js片段*/ TYPE_JS = 12,
        /**声音文件*/ TYPE_SOUND = 13,
        /**url资源*/ TYPE_URL_RES = 14,
        /**spine资源*/ TYPE_SPINE = 15
    }
}
