/**
 @author 迷途小羔羊
 * 2014-6-30
 */
module GYLite
{
										
	
	export class GYScrollBase extends GYSkinContainer
	{
		protected _sliderW:number=NaN;				
		protected _sliderH:number=NaN;				
		protected _scrollRange:number=NaN;			
		protected _scrollMax:number=NaN;				
		protected _max:number=NaN;				
		protected _min:number=NaN;				
		protected _per:number=NaN;				
		
		protected _scrollStep:number=NaN;		
		protected _step:number=NaN;					
		protected _nowStep:number=NaN;			
		protected _clkFunc:Function;			
		protected _stOffset:number=NaN;				
		protected _oldMousePos:number=NaN;
		protected _hideBtn:boolean;	
		protected _barPercent:number=NaN;		
		protected _value:number=NaN;				
		protected _maximum:number=NaN;		
		protected _sliderMinSize:number=NaN;		
		protected _clkCount:number=NaN;
		protected _barChange:boolean;
		protected _tween:GYTween;
		protected _scrollSpeed:number = 1;
		protected _scrollEvent:GYScrollerEvent;
		/**变更模式，决定当滚动条恢复时，保持的位置，默认是0，按百分比保持，1按坐标位置保持*/
		public resetMode:number;
		/**限制滚动条最小滚动位置，默认NaN，不限制*/
		public limitMin:number=NaN;
		/**限制滚动条最大滚动位置，默认NaN，不限制*/
		public limitMax:number=NaN;
		/**滚动条改变调用的方法，返回Scroller的s.value*/
		//public sliderChgFunc:Function;    
//		protected _skin:IScrollerSkin;
		
		public constructor(skin:any=null)
		{
			super();
			var s = this;
			s._scrollEvent = new GYScrollerEvent(GYScrollerEvent.SCROLL_CHANGE);
			s.skin = skin;	
			s.initComponent();
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();
			s.resetMode = 0;
			s._per = 0;				//当前位置百分比
			s._scrollStep = GYScrollBase.default_scrollStep;		//滚动分度值
			s._step = GYScrollBase.default_step;					//单次移动数量
			s._hideBtn = GYScrollBase.default_hideBtn;				//隐藏按钮
			s._value = GYScrollBase.default_value;				//当前位置
			s._maximum = GYScrollBase.default_maximum;				//最大位置
			s._sliderMinSize = GYScrollBase.default_sliderMinSize;		//滑块最小尺寸
		}
		public updateView():void
		{
			let s = this;
			if(s._invalidDisplay)
			{
				s.viewChange();
				s._invalidDisplay = false;
			}
			if(s._barChange)
			{
				s.setBar();
				s._barChange = false;
			}
		}
		/**刷新滚动条*/
		public invalidBarView():void
		{
			let s = this;
			if(s._barChange)
				return;
			s._barChange = true;
			s.displayChg();
		}
		public setBar():void
		{
			let s = this;
			
		}
		/**单次移动数量，默认1*/
		public set step(val:number)
		{
			let s = this;
			s._step=val;
		}
		public get step():number
		{
			let s = this;
			return s._step;
		}
		/**滚动分度值，默认为5(像素)*/
		public set scrollStep(val:number)
		{
			let s = this;
			s._scrollStep=val;
		}
		public get scrollStep():number
		{
			let s = this;
			return s._scrollStep;
		}
		public set value(step:number)
		{
			let s = this;			
			s.setSlider(step * s._scrollStep);
		}
		/**当前进度条位置值(位置/步长)，注意与s.position区分，请勿用此值判断是否超过maximum，应该使用s.position */
		public get value():number
		{
			let s = this;
			return Math.ceil(s._value / s._scrollStep);
		}
		/**当前进度条位置值(像素) */
		public get position():number
		{
			return this._value;
		}
		public set position(val:number)
		{
			this.setSlider(val);
		}
		/**设置滚动条位置
		 * @param val 位置
		 * @param min_max_event 达到尽头是否派发事件
		*/
		public setPosition(val:number, min_max_event:boolean=false):void
		{
			let s = this;
			let flag:number = s.setSlider(val);
			if(min_max_event && flag != 0)
			{
				let event:string = flag==1?GYViewEvent.SCROLL_HORIZONAL_MIN:GYViewEvent.SCROLL_HORIZONAL_MAX;
				if(s.hasEventListener(event))
					s.dispatchEventWith(event);
				event = flag==1?GYViewEvent.SCROLL_VERTICAL_MIN:GYViewEvent.SCROLL_VERTICAL_MAX;
				if(s.hasEventListener(event))
					s.dispatchEventWith(event);
			}			
		}
		public scrollToValue(val:number,time:number=1000):void
		{
			let s= this;
			s.scrollToPosition(val * s._scrollStep,time);
		}
		public scrollToPosition(val:number,time:number=1000):void
		{
			let s = this;			
			s.stopScroll();			
			if(s.limitMin == s.limitMin && val < s.limitMin)
				val = s.limitMin;
			if(s.limitMax == s.limitMax && val > s.limitMax)
				val = s.limitMax;
			if(val <= 0)
				val = 0;
			else if(val >= s._maximum)
				val = s._maximum;
			if(s._value == val)return;
			time *= s._scrollSpeed;
			if(time!=time || time <= 0)return;
			s._tween = GYTween.to(this,[TweenData.getInstance("position",val,NaN,GYTween.reduceEase)],time,0,s,s.scrollTweenEnd,null,null,true,false);
		}
		protected setSlider(val:number):number
		{
			let s = this;			
			let flag:number=0;
			if(s.limitMin == s.limitMin && val < s.limitMin)
				val = s.limitMin;
			if(s.limitMax == s.limitMax && val > s.limitMax)
				val = s.limitMax;
			if(val <= 0)
			{
				val = 0;
				flag = 1;
			}				
			else if(val >= s._maximum)
			{
				val = s._maximum;
				flag = 2;
			}				
			s._value = val;
			return flag;
		}
		protected scrollTweenEnd():void
		{
			let s = this;
			s._tween = null;
			if(s.hasEventListener(GYScrollerEvent.SCROLL_TWEEN_END))
				s.dispatchEvent(new GYScrollerEvent(GYScrollerEvent.SCROLL_TWEEN_END));
		}
		public stopScroll():void
		{
			let s = this;
			if(s._tween)
			{
				s._tween.clear();
				s._tween = null;
			}
		}
		/**滑块最小尺寸 */
		public get sliderMinSize():number
		{
			let s = this;
			return s._sliderMinSize;
		}
		public set sliderMinSize(val:number)
		{
			let s = this;
			if(s._sliderMinSize == val)return;
			s._sliderMinSize = val;
			s.invalidBarView();
		}
		
		public set width(val:number)
		{
			let s = this;			
			if(s.$width == val)
				return;			
			s.$width = val;
			s.invalidDisplay();
		}	
		public get width():number
		{
			let s = this;	
			return egret.superGetter(GYScrollBase, s, "width");
		}	
		public set height(val:number)
		{
			let s = this;	
			if(s.$height == val)
				return;
			s.$height = val;
			s.invalidDisplay();
		}	
		public get height():number
		{
			let s = this;	
			return egret.superGetter(GYScrollBase, s, "height");
		}
		public get tween():GYTween
		{
			let s = this;	
			return s._tween;
		}	
		public set scrollSpeed(val:number)
		{let s =this;
			s._scrollSpeed = val;
		}
		/**拖拽滚动的时间*/
		public get scrollSpeed():number
		{let s =this;
			return s._scrollSpeed;
		}
		/**滚动分度值(默认值)*/public static default_scrollStep:number=1;		
		/**单次移动数量(默认值)*/public static default_step:number=20;					
		/**隐藏按钮(默认值)*/public static default_hideBtn:boolean=false;	
		/**当前位置(默认值)*/public static default_value:number=0;				
		/**最大位置(默认值)*/public static default_maximum:number=0;		
		/**滑块最小尺寸(默认值)*/public static default_sliderMinSize:number=16;		
		
	}
}