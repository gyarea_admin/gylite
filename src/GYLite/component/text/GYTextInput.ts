module GYLite
{
								
						
	export class GYTextInput extends GYSkinContainer
	{
		protected _textInput:GYText=null;
		protected _promptText:GYText=null;
		protected _noInput:boolean=true;
		protected _isPassword:boolean=false;
		protected _defaultPromptFormat:TextFormat=null;
		private _editable:boolean=false;
		/**请在主题类内设置默认格式*/public static promptFormat:TextFormat=null;
		public constructor(skin:TextInputSkin=null,mul:boolean=false)
		{
			super();			
			let s = this;
			this.skin = skin;
			s._textInput=new GYText(mul);
			s._editable = true;
			s.initComponent();
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();
			s._defaultPromptFormat=GYTextInput.promptFormat;
			s._textInput.type = egret.TextFieldType.INPUT;
			s._textInput.verticalCenter = 0;
			s.addElement(s._textInput);
			s.createPrompt();
		}
		protected createPrompt():void
		{
			let s = this;
			if(s._promptText==null)
			{
				s._promptText = new GYText;
				s._promptText.textFormat = s._defaultPromptFormat;
				s._promptText.verticalCenter = 0;
				s._promptText.visible = false;
				s.addElement(s._promptText);
				s.swapElementIndex(s._textInput,s._promptText);
			}
		}
		public get promptText():GYText
		{
			let s = this;
			return s._promptText;
		}
		public get textInput():GYText
		{
			let s = this;
			return s._textInput;
		}
		protected getThemeSkin():IGYSkin
		{
			let s = this;
			return GYSprite.skinTheme.GetTextInputSkin();
		}
		public set background(val:egret.BitmapData)
		{
			let s = this;
			s._skin.background = val;
		}
		public set scale9GridRect(val:Scale9GridRect)
		{
			let s = this;
			s._skin.scale9GridRect=val;
		}
		public set text(val:string)
		{
			let s = this;
			s._textInput.text = val;
			if(s.$height != s.$height)
				s._skin.height = s.height;
			s.promptState();
		}
		public get text():string
		{
			let s = this;
			return s._textInput.text;
		}
		/**输入提示的文本格式*/
		public set defaultPromptFormat(val:TextFormat)
		{
			let s = this;
			s._defaultPromptFormat=val;
			s._promptText.text = s._promptText.text;
			s._promptText.textFormat = s._defaultPromptFormat;
		}
		public set width(val:number)
		{
			let s = this;			
			if(s.$width == val)
				return;
			s.$width = val;
			s._textInput.width = s.width - s._textInput.paddingLeft - s._textInput.paddingRight;
			s._promptText.width = s._textInput.baseWidth;
			s._skin.width = s.width;
			s.invalidDisplay();
		}		
		public get width():number
		{
			let s = this;			
			return egret.superGetter(GYTextInput, this, "width");
		}
		public set height(val:number)
		{
			let s = this;	
			if(s.$height == val)
				return;
			s.$height = val;
			s._textInput.height = s.height - s._textInput.paddingTop - s._textInput.paddingBottom;
			s._promptText.height = s._textInput.baseHeight;
			s._skin.height = s.height;
			s.invalidDisplay();
		}
		public get height():number
		{
			let s = this;			
			return egret.superGetter(GYTextInput, this, "height");
		}
		public get prompt():string
		{
			return this._promptText.text;
		}
		public set prompt(val:string)
		{
			let s = this;
			val = val || "";
			if(s._promptText.text == val)return;
			s._promptText.text = val;
			if(s._promptText.text == "")
			{
				s._textInput.removeEventListener(egret.FocusEvent.FOCUS_OUT,s.textDeactive,s);
				s._textInput.removeEventListener(egret.FocusEvent.FOCUS_IN,s.textActive,s);
				return;
			}
			s.textDeactive();
			s._textInput.addEventListener(egret.FocusEvent.FOCUS_OUT,s.textDeactive,s);
			s._textInput.addEventListener(egret.FocusEvent.FOCUS_IN,s.textActive,s);
		}
		protected textDeactive(e:egret.Event=null):void
		{
			let s = this;
			s.promptState();
		}
		
		protected textActive(e:egret.Event):void
		{
			let s = this;
			s.promptState(false);
		}
		private promptState(val:boolean=true):void
		{
			let s = this;
			if(s._promptText)
			{
				s._noInput = (s._promptText.text != "" && s._textInput.text=="" && val);
				s._promptText.visible = s._noInput;
			}
			else
				s._noInput = false;
			s._textInput.alpha = s._noInput?0:1;
		}
		
		public set paddingTop(val:number)
		{
			let s = this;
			s._promptText.paddingTop = s._textInput.paddingTop = val;
			s._textInput.height = s.height - val - s._textInput.paddingBottom;
			s._promptText.height = s._textInput.baseHeight;
		}
		public set paddingLeft(val:number)
		{
			let s = this;
			s._promptText.paddingLeft = s._textInput.paddingLeft = val;
			s._textInput.width = s.width - val - s._textInput.paddingRight;
			s._promptText.height = s._textInput.baseHeight;
		}
		public set paddingRight(val:number)
		{
			let s = this;
			s._promptText.paddingRight = s._textInput.paddingRight = val;
			s._textInput.width = s.width - s._textInput.paddingLeft - val;
			s._promptText.height = s._textInput.baseHeight;
		}
		public set paddingBottom(val:number)
		{
			let s = this;
			s._promptText.paddingBottom = s._textInput.paddingBottom = val;
			s._textInput.height = s.height - s._textInput.paddingTop - val;
			s._promptText.height = s._textInput.baseHeight;
		}
		public get paddingTop():number
		{
			let s = this;			
			return s._textInput.paddingTop;
		}
		public get paddingLeft():number
		{
			let s = this;			
			return s._textInput.paddingLeft;
		}
		public get paddingRight():number
		{
			let s = this;			
			return s._textInput.paddingRight
		}
		public get paddingBottom():number
		{
			let s = this;			
			return s._textInput.paddingBottom;
		}

		public set editable(val:boolean)
		{
			let s = this;
			if(s._editable == val)
				return;
			s._editable =val;
			s._textInput.type = val?egret.TextFieldType.INPUT:egret.TextFieldType.DYNAMIC;				
		}
		public get editable():boolean
		{
			return this._editable;
		}

	}
}