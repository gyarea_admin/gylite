/***2024-12-17 迷途小羔羊
* 用于编写图集小图排列方式的枚举
*/
module GYLite
{
    export enum AtlasPlaceType
    {
        /**正方形排列*/SQUARE,
        /**水平排列*/HORICAL,
        /**垂直排列*/VERTICAL
    }
}