declare class BaseRet {
    /**本地模拟数据*/ nativeRet(data: any): BaseRet;
    /**远端数据*/ dataRet(data: any): BaseRet;
    /**清空数据*/ clear(): void;
}
