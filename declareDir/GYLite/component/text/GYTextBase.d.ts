declare module GYLite {
    class GYTextBase extends egret.TextField implements IGYInteractiveDisplay, IUpdate {
        static _tagReg: RegExp;
        /**设置此属性，初始化MyText时将默认使用嵌入式字体*/
        static defualtFont: string;
        static defualtSysFont: string;
        static defaultFormat: any;
        static default_clipXPadding: any;
        static default_clipYPadding: any;
        static textParser: egret.HtmlTextParser;
        static init(): void;
        protected _minWidth: number;
        protected _minHeight: number;
        protected _maxWidth: number;
        protected _maxHeight: number;
        $textFormat: TextFormat;
        protected _lineWidthArr: Array<number>;
        protected _lineHeightArr: Array<number>;
        $percentWidth: number;
        $percentHeight: number;
        $layoutParent: GYUIComponent;
        protected _layoutMode: LayoutMode;
        $paddingLeft: number;
        $paddingRight: number;
        $paddingTop: number;
        $paddingBottom: number;
        $invalidDisplay: boolean;
        updating: boolean;
        protected _elementsRect: egret.Rectangle;
        protected _toolTip: GYToolTip;
        protected _toolTipOffsetX: number;
        protected _toolTipOffsetY: number;
        protected _toolTipString: string;
        protected _toolTipKeep: boolean;
        protected _followTarget: GYSprite;
        protected _bound: egret.Rectangle;
        protected _elementbound: egret.Rectangle;
        $disposed: boolean;
        protected _graphics: GYGraphics;
        protected _htmlStr: string;
        protected _textStr: string;
        /**输入模式下，文本y轴偏移*/ inputOffsetY: number;
        /**是否以矩形范围也算入碰撞测试*/ rectHit: boolean;
        /**鼠标是否完全穿透，即不参与碰撞检测*/ mouseThrough: boolean;
        /**防销毁锁定标志，为true则不会被dispose销毁，除非参数forceDispose为true*/ disposeLock: boolean;
        /**是否tip跟随鼠标*/ private _toolTipOnKeep;
        /**是否tip直到鼠标抬起*/ isTipFollow: boolean;
        /**是否使用布局的失效机制*/
        useInvalidDisplay: boolean;
        /**是否不参与布局计算*/ offLayout: boolean;
        /**阴影颜色*/ _shadowColor: number;
        /**阴影偏移X*/ _shadowOffsetX: number;
        /**阴影偏移Y*/ _shadowOffsetY: number;
        /**阴影大小*/ _shadowBlur: number;
        protected _charStylesMap: any;
        protected _batchDrawParam: BatchDrawParam;
        protected _invalidBatch: boolean;
        protected _batch: boolean;
        protected _batchAtlasName: string;
        constructor();
        protected initComponent(): void;
        protected displayChg(): void;
        $setIsTyping(val: boolean): void;
        get mouseX(): number;
        get mouseY(): number;
        /**刷新相对布局*/
        layout(): void;
        /**刷新相对大小*/
        sizeChange(): void;
        addLayout(p: GYUIComponent): void;
        protected layoutChange(e: GYViewEvent): void;
        /**是否百分比布局*/
        isPercentSize(): boolean;
        get layoutParent(): GYUIComponent;
        set left(val: number);
        set top(val: number);
        set right(val: number);
        set bottom(val: number);
        set horizonalCenter(val: number);
        set verticalCenter(val: number);
        get left(): number;
        get top(): number;
        get right(): number;
        get bottom(): number;
        get horizonalCenter(): number;
        get verticalCenter(): number;
        /**获取边界矩形 */
        getAllBounds(t: IGYDisplay): egret.Rectangle;
        /**获取边界矩形 */
        getElementsBounds(t: IGYDisplay): egret.Rectangle;
        set paddingLeft(val: number);
        set paddingRight(val: number);
        set paddingTop(val: number);
        set paddingBottom(val: number);
        get paddingLeft(): number;
        get paddingRight(): number;
        get paddingTop(): number;
        get paddingBottom(): number;
        set x(val: number);
        get x(): number;
        set y(val: number);
        get y(): number;
        set width(val: number);
        set height(val: number);
        get width(): number;
        get height(): number;
        set_width(val: number): void;
        set_height(val: number): void;
        get_width(): number;
        get_height(): number;
        get baseWidth(): number;
        get baseHeight(): number;
        set percentWidth(val: number);
        set percentHeight(val: number);
        get percentWidth(): number;
        get percentHeight(): number;
        get minWidth(): number;
        set minWidth(value: number);
        get minHeight(): number;
        set minHeight(value: number);
        get maxWidth(): number;
        set maxWidth(value: number);
        get maxHeight(): number;
        set maxHeight(value: number);
        set layoutMode(val: LayoutMode);
        /**获取布局容器(因为存在双层容器如GYGroup，用此方法取相对布局容器更准确)*/
        getLayoutContainer(): GYUIComponent;
        get layoutMode(): LayoutMode;
        /**格式刷新*/
        invalidFormat(): void;
        updateView(): void;
        /**刷新显示*/
        invalidDisplay(): void;
        invalidLayout(): void;
        /**提示文本,可以通过s.toolTip设置自定义的GYToolTip(继承此类重写实现)*/
        set toolTipString(val: string);
        get toolTipString(): string;
        set toolTip(val: GYToolTip);
        /**s.toolTip，继承GYToolTip实现自定义@see GYToolTip*/
        get toolTip(): GYToolTip;
        protected rollOverTip(e?: egret.TouchEvent): void;
        showTip(keep?: boolean, followTarget?: GYSprite): void;
        protected rollOutTip(e?: egret.TouchEvent): void;
        protected mouseOutTip(e?: egret.TouchEvent): void;
        /**tip偏移量X*/
        get toolTipOffsetX(): number;
        set toolTipOffsetX(value: number);
        /**tip偏移量Y*/
        get toolTipOffsetY(): number;
        set toolTipOffsetY(value: number);
        /**是否保留tip直到鼠标抬起*/
        get toolTipKeep(): boolean;
        set toolTipKeep(value: boolean);
        /**tip跟随对象*/
        get followTarget(): GYSprite;
        set followTarget(value: GYSprite);
        get textWidth(): number;
        get textHeight(): number;
        /**是否在父级视图范围内(0,0,width,height),可用于做裁切视图时的离屏渲染优化*/
        inParentView(l?: number, r?: number, t?: number, b?: number): boolean;
        /**销毁
         * @param disposeChild 是否连同显示列表上的子级也销毁（文本组件没有子级，忽略此参数）
         * @param removeChild 是否从父级显示列表中移除
         * @param forceDispose 是否强制销毁，为true表示无视disposeLock标志，进行销毁
        */
        dispose(disposeChild?: boolean, removeChild?: boolean, forceDispose?: boolean): void;
        /**是否已经被销毁*/
        get disposed(): boolean;
        protected drawText(): number[];
        clearBatch(): void;
        enableBatch(val: boolean): void;
        /**是否动态合批，请在文本渲染前设定，默认根据父级容器batch，如果父级也没设定，则默认false*/
        isBatch(): boolean;
        setBatchDrawParam(val: BatchDrawParam): void;
        /**合批图像的绘制样式**/
        getBatchDrawParam(): BatchDrawParam;
        setBatchAtlasName(val: string): void;
        /**合批图集名称，不存在找父级容器的，如果都不存在，默认AtlasRender.defaultAtlasName**/
        getBatchAtlasName(): string;
        set underline(val: any);
        get underline(): any;
        setShadow(shadowBlur?: number, shadowColor?: number, shadowOffsetX?: number, shadowOffsetY?: number): void;
        protected setHTML(val: string): void;
        protected setText(val: string): void;
        invalidBatch(): void;
        validBatch(): void;
        /**背景填充
         * @param lines [坐标x,坐标y,宽度w,颜色color]
        */
        protected fillBackground(lines?: number[]): void;
        getLineWidth(ind?: number): number;
        getLineHeight(ind?: number): number;
        $updateRenderNode(): void;
        $getLinesArr2(): egret.ILineElement[];
    }
}
