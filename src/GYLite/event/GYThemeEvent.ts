module GYLite
{	
	export class GYThemeEvent extends GYEvent
	{
		/**主题改变*/
		public static THEME_CHANGE:string="themeChange";
		public constructor(type:string, bubbles?:boolean, cancelable?:boolean)
		{
			super(type, bubbles, cancelable);
		}
	}
}