declare class Log {
    constructor();
    static LONG_LOADTIME: number;
    static LONG_UNCOMPRESSTIME: number;
    static LONG_EVALTIME: number;
    private static _startTime;
    static ERROR: number;
    static WARN: number;
    static IMPORTANT: number;
    static INFO: number;
    static VERBOSE: number;
    static uploadLevel: number;
    static printLevel: number;
    static userDataInited: boolean;
    private static _date;
    private static logsArr;
    private static _reportCloseCount;
    private static _errorCount;
    private static _reportTime;
    static init(): void;
    static writeLog(content: string, level?: number, upload?: boolean): void;
    static readProgressLog(progressInfo: any[]): string;
    /**打印加载详情*/
    static loadLog(l: GYLite.LoadInfo): void;
    static uncompressLog(l: GYLite.CompressLoadInfo, key?: string): void;
    static evalLog(key: string): void;
    static recordTime(): void;
}
