/**
@author 迷途小羔羊
2022.09.27
*/
declare module GYLite {
    class Atlas {
        $disposed: boolean;
        /**图集纹理资源*/ $aliasRes: ResObject;
        $webglRenderContext: egret.sys.RenderContext;
        protected _canvas: HTMLCanvasElement;
        $webGLTexture: WebGLTexture;
        protected _atlasRect: AtlasRect;
        protected _freeBatchDict: any;
        /**空闲的batchInfo数量*/ $freeCount: number;
        protected _sheetsPool: SheetConfig[];
        protected _texturePool: egret.Texture[];
        protected _bitmapX: number;
        protected _bitmapY: number;
        /**图集名称*/ $atlasName: string;
        /**图集id*/ $atlasId: number;
        /**单张图集
         * @param atlasName 图集名称
         * @param atlasId 图集id
         * @param webglTex webgl纹理
         * @param w 图集宽度
         * @param h 图集高度
         * @param context 渲染上下文
         * @param source 图集引用的数据源，默认null，则会自动创建空白的图集
         * **/
        constructor(atlasRect: AtlasRect, context: egret.sys.RenderContext, source?: HTMLCanvasElement | HTMLImageElement | HTMLVideoElement | egret.BitmapData, createSpriteSheet?: boolean);
        createSpriteSheep(jsonRes?: ResObject): void;
        /**根据小图信息进行测量绘制，生成webgl纹理
         * @param rectInfo 描述小图信息，参考RectInfo类
         * @param elemtn 显示对象源
        */
        measureAndDraw(rectInfo: RectInfo, element: HTMLImageElement | HTMLCanvasElement | BatchElement): WebGLTexture;
        /**根据配置添加小图
         * @param subKey 小图名称
         * @param config 小图配置
         * @param atlasInfo 图集信息
        */
        addSubResByConfig(subKey: string, config: any): ResObject;
        /**给图集资源ResObject添加小图
         * @param atlasInfo 图集信息
         * @param srcTex 源小图纹理对象（未合批前），默认null，没有源小图，则自动创建一个
        */
        addSubRes(atlasInfo: AtlasInfo): ResObject;
        /**给图集资源ResObject移除小图*/
        removeSubRes(texName: string): void;
        getSheetConfig(x: number, y: number, w: number, h: number, offX: number, offY: number, sourceW: number, sourceH: number): SheetConfig;
        recycleSheetConfig(subKey: string): void;
        getTexture(bitmapX: number, bitmapY: number, bitmapWidth: number, bitmapHeight: number, offsetX: number, offsetY: number, textureWidth: number, textureHeight: number): egret.Texture;
        recycleTexture(delRes: ResObject): void;
        /**图集宽度*/
        get atlasId(): number;
        /**图集宽度*/
        get atlasName(): string;
        /**图集宽度*/
        get width(): number;
        /**图集高度*/
        get height(): number;
        /**图集webgl纹理*/
        get webglTexture(): WebGLTexture;
        /**图集资源对象*/
        get atlasRes(): ResObject;
        /**虚拟图集*/
        get atlasRect(): AtlasRect;
        private createTextTextureAtlas;
        /**添加当前空闲的合批信息(当图集位置紧张时会进行移除)*/
        addFreeBatchInfo(batchInfo: BatchInfo): void;
        /**移除当前空闲的合批信息*/
        removeFreeBatchInfo(batchInfo: BatchInfo): void;
        /**释放所有空闲的合批信息*/
        releaseFreeBatch(): void;
        dispose(): void;
        get disposed(): boolean;
        static curImage: GYGroup;
        debugImage: GYImage;
        debugGroup: GYGroup;
        debugShow(): void;
    }
    class SheetConfig {
        x: number;
        y: number;
        w: number;
        h: number;
        offX: number;
        offY: number;
        sourceW: number;
        sourceH: number;
        constructor();
        reset(x: number, y: number, w: number, h: number, offX: number, offY: number, sourceW: number, sourceH: number): void;
    }
}
