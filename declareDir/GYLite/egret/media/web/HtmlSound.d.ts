declare namespace egret.web {
    /**
     * @private
     * @inheritDoc
     */
    class HtmlSound extends egret.EventDispatcher implements egret.Sound {
        /**
         * Background music
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 背景音乐
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static MUSIC: string;
        /**
         * EFFECT
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 音效
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static EFFECT: string;
        /**
         * @private
         */
        type: string;
        /**
         * @private
         */
        private url;
        /**
         * @private
         */
        private originAudio;
        /**
         * @private
         */
        private loaded;
        /**
         * @private
         * @inheritDoc
         */
        constructor();
        get length(): number;
        private static loadingSoundMap;
        /**
         * @inheritDoc
         */
        load(url: string): void;
        /**
         * @inheritDoc
         */
        play(startTime?: number, loops?: number): SoundChannel;
        /**
         * @inheritDoc
         */
        close(): void;
        /**
         * @private
         */
        private static audios;
        private static clearAudios;
        static $clear(url: string): void;
        static $pop(url: string): HTMLAudioElement;
        static $recycle(url: string, audio: HTMLAudioElement): void;
    }
}
