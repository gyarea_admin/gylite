module GYLite
{		
					
	export class SanGuoTheme extends GYSkinTheme
	{
		public rect_15_15_7_15:Scale9GridRect=new Scale9GridRect(15,15,7,15);
		public rect_15_15_10_10:Scale9GridRect=new Scale9GridRect(15,15,10,10);
		public rect_5_5_5_5:Scale9GridRect=new Scale9GridRect(5,5,5,5);
		public constructor()
		{
			super();
		}
		/**获得默认的TextFormat*/
		public GetTextTextFormat():TextFormat
		{var s = this;
			return new TextFormat(GYTextBase.defualtSysFont, 12, 0xfff8a2, false, false, false, null,null,"left",null,null,0,5);
		}
		/**获得默认的GYTabButton皮肤*/
		public GetTabButtonSkin():IGYSkin
		{var s = this;
			return new ButtonSkin(s.tabBtnSkinVec, s.rect_15_15_7_15);
		}
		/**获得默认的GYProgressBar皮肤*/
		public GetProgressBarSkin():IGYSkin
		{var s = this;
			var back:GYScaleSprite = new GYScaleSprite(s.progress_backBD, s.rect_15_15_10_10);
			var bar:GYScaleSprite = new GYScaleSprite(s.progressBD,s.rect_15_15_10_10);
			return new ProgressBarSkin(back, bar, s.barTextFormat);
		}
		/**获得默认的GYSlider皮肤*/
		public GetSliderSkin():IGYSkin
		{var s = this;
			var back:GYScaleSprite = new GYScaleSprite(s.slider_backBD, s.rect_5_5_5_5);
			var bar:GYScaleSprite = new GYScaleSprite(s.slider_progBD,s.rect_5_5_5_5);
			var buttonSkin:IButtonSkin = new ButtonSkin(s.sliderBtnSkinVec);
			return new SliderSkin(back, bar, buttonSkin);
		}
		/**获得默认的s.toolTip*/
		public GetToolTip():GYToolTip
		{var s = this;
			return GameToolTip.getInstance();
		}
		
		public initRes():void
		{var s = this;
			if(GYLoader.getRes("tab_up","Img/ui.png") == null)return;
			super.initRes();
			var i:number=0,len:number=0;
			var j:number=0,len2:number=0;
			var seq:string;
			var seqArr:Array<any>;
			var numArr:Array<number>=[17,12,24,18]
			len = numArr.length;
			for(i=0;i<len;++i)
			{
				len2 = numArr[i];
				seqArr=[];
				for(j=0;j<len2;++j)
				{
					seq = (j+1 < 10?"000":"00");
					seqArr[j]=GYLoader.getRes(i + "" + seq + (j + 1) + "","Img/ui.png").refRes(s);
				}
				RichSeqImage.imageList.push(seqArr);
			}
		}
		protected setRes():void
		{var s = this;
			s.tab_upBD = GYLoader.getRes("tab_up","Img/ui.png").refRes(s);
			s.tab_overBD = GYLoader.getRes("tab_over","Img/ui.png").refRes(s);
			s.tab_downBD = GYLoader.getRes("tab_down","Img/ui.png").refRes(s);
			s.tab_selUpBD = GYLoader.getRes("tab_selUp","Img/ui.png").refRes(s);
			s.tab_selOverBD = GYLoader.getRes("tab_selOver","Img/ui.png").refRes(s);
			s.tab_selDownBD = GYLoader.getRes("tab_selDown","Img/ui.png").refRes(s);
			s.tab_selDisabledBD = s.tab_selUpBD;
			s.upArrow_upBD = GYLoader.getRes("upArrow_up","Img/ui.png").refRes(s);
			s.upArrow_overBD = GYLoader.getRes("upArrow_over","Img/ui.png").refRes(s);
			s.upArrow_downBD = GYLoader.getRes("upArrow_down","Img/ui.png").refRes(s);
			s.downArrow_upBD = GYLoader.getRes("downArrow_up","Img/ui.png").refRes(s);
			s.downArrow_overBD = GYLoader.getRes("downArrow_over","Img/ui.png").refRes(s);
			s.downArrow_downBD = GYLoader.getRes("downArrow_down","Img/ui.png").refRes(s);
			s.leftArrow_upBD = GYLoader.getRes("leftArrow_up","Img/ui.png").refRes(s);
			s.leftArrow_overBD = GYLoader.getRes("leftArrow_over","Img/ui.png").refRes(s);
			s.leftArrow_downBD = GYLoader.getRes("leftArrow_down","Img/ui.png").refRes(s);
			s.rightArrow_upBD = GYLoader.getRes("rightArrow_up","Img/ui.png").refRes(s);
			s.rightArrow_overBD = GYLoader.getRes("rightArrow_over","Img/ui.png").refRes(s);
			s.rightArrow_downBD = GYLoader.getRes("rightArrow_down","Img/ui.png").refRes(s);
			s.sliderBtn_upVBD = GYLoader.getRes("sliderBtn_up","Img/ui.png").refRes(s);
			s.sliderBtn_overVBD = GYLoader.getRes("sliderBtn_over","Img/ui.png").refRes(s);
			s.sliderBtn_downVBD = GYLoader.getRes("sliderBtn_down","Img/ui.png").refRes(s);
			s.sliderBtn_upHBD = s.sliderBtn_upVBD;
			s.sliderBtn_overHBD = s.sliderBtn_overVBD;
			s.sliderBtn_downHBD = s.sliderBtn_downVBD;
			s.scrollerBackVBD = GYLoader.getRes("scrollerBack","Img/ui.png").refRes(s);
			s.scrollerBackHBD = s.scrollerBackVBD;
			s.commonBtn_upBD = GYLoader.getRes("commonBtn_up","Img/ui.png").refRes(s);
			s.commonBtn_overBD = GYLoader.getRes("commonBtn_over","Img/ui.png").refRes(s);
			s.commonBtn_downBD = GYLoader.getRes("commonBtn_down","Img/ui.png").refRes(s);
			s.commonBtn_disabledBD = GYLoader.getRes("commonBtn_disabled","Img/ui.png").refRes(s);
			s.commonBtn_selUpBD = GYLoader.getRes("commonBtn_selUp","Img/ui.png").refRes(s);
			s.commonBtn_selOverBD = GYLoader.getRes("commonBtn_selOver","Img/ui.png").refRes(s);
			s.commonBtn_selDownBD = GYLoader.getRes("commonBtn_selDown","Img/ui.png").refRes(s);
			s.commonBtn_selDisabledBD = GYLoader.getRes("commonBtn_selDisabled","Img/ui.png").refRes(s);
			s.scaleBtn_upBD = s.commonBtn_upBD;
			s.scaleBtn_overBD = s.commonBtn_overBD;
			s.scaleBtn_downBD = s.commonBtn_downBD;
			s.scaleBtn_disabledBD = s.commonBtn_disabledBD;
			s.scaleBtn_selUpBD = s.commonBtn_selUpBD;
			s.scaleBtn_selOverBD = s.commonBtn_selOverBD;
			s.scaleBtn_selDownBD = s.commonBtn_selDownBD;
			s.scaleBtn_selDisabledBD = s.commonBtn_selDisabledBD;
			s.menuBackBD = s.textInputBD = GYLoader.getRes("textInput","Img/ui.png").refRes(s);
			s.check_upBD = GYLoader.getRes("check_up","Img/ui.png").refRes(s);
			s.check_overBD = GYLoader.getRes("check_over","Img/ui.png").refRes(s);
			s.check_downBD = GYLoader.getRes("check_down","Img/ui.png").refRes(s);
			s.check_disabledBD = GYLoader.getRes("check_disabled","Img/ui.png").refRes(s);
			s.check_selUpBD = GYLoader.getRes("check_selUp","Img/ui.png").refRes(s);
			s.check_selOverBD = GYLoader.getRes("check_selOver","Img/ui.png").refRes(s);
			s.check_selDownBD = GYLoader.getRes("check_selDown","Img/ui.png").refRes(s);
			s.check_selDisabledBD = GYLoader.getRes("check_selDisabled","Img/ui.png").refRes(s);
			s.radio_upBD = GYLoader.getRes("radio_up","Img/ui.png").refRes(s);
			s.radio_overBD = GYLoader.getRes("radio_over","Img/ui.png").refRes(s);
			s.radio_downBD = GYLoader.getRes("radio_down","Img/ui.png").refRes(s);
			s.radio_disabledBD = GYLoader.getRes("radio_disabled","Img/ui.png").refRes(s);
			s.radio_selUpBD = GYLoader.getRes("radio_selUp","Img/ui.png").refRes(s);
			s.radio_selOverBD = GYLoader.getRes("radio_selOver","Img/ui.png").refRes(s);
			s.radio_selDownBD = GYLoader.getRes("radio_selDown","Img/ui.png").refRes(s);
			s.radio_selDisabledBD = GYLoader.getRes("radio_selDisabled","Img/ui.png").refRes(s);
			s.progress_backBD = GYLoader.getRes("progress_back","Img/ui.png").refRes(s);
			s.progressBD = GYLoader.getRes("progress","Img/ui.png").refRes(s);
			s.slider_backBD = GYLoader.getRes("slider_back","Img/ui.png").refRes(s);
			s.slider_progBD = GYLoader.getRes("slider_prog","Img/ui.png").refRes(s);
			s.slider_upBD = GYLoader.getRes("slider_up","Img/ui.png").refRes(s);
			s.slider_overBD = GYLoader.getRes("slider_over","Img/ui.png").refRes(s);
			//			slider_downBD = (new slider_downCls as Bitmap).bitmapData;
			s.closeBtn_upBD = GYLoader.getRes("closeBtn_up","Img/ui.png").refRes(s);
			s.closeBtn_overBD = GYLoader.getRes("closeBtn_over","Img/ui.png").refRes(s);
			s.closeBtn_downBD = GYLoader.getRes("closeBtn_up","Img/ui.png").refRes(s);
			s.closeBtn_disabledBD = GYLoader.getRes("closeBtn_up","Img/ui.png").refRes(s);
			s.windowBackBD = GYLoader.getRes("ui","Img/ui.png").refRes(s);
			s.textAreaBD = s.textInputBD;
			
			GYTextBase.defualtSysFont = (navigator.userAgent.toLowerCase().indexOf("chrome") == - 1) ? "Microsoft YaHei" : "微软雅黑";			
			GYTextInput.promptFormat=new TextFormat(GYTextBase.defualtSysFont, 12, 0xcac582, false, false, false,null,null,"left", null,null,0,5);
		}
		
		public getDraggerBtn():GYSprite
		{var s = this;
			var btn:GYImage = new GYImage;
			btn.source = GYLoader.getRes("dragger","Img/ui.png").refRes(s);
			return btn;
		}
		
	}
}