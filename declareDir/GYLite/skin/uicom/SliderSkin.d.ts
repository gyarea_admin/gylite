declare module GYLite {
    /**滑块条*/
    class SliderSkin extends ProgressBarSkin implements ISliderSkin {
        protected _sliderBtn: GYButton;
        protected _sliderX: number;
        protected _sliderY: number;
        /**
         * @param s.backgroundSkin 背景皮肤
         * @param s.barSkin 进度条皮肤
         * @param textSkin 文本皮肤 默认null，没有进度文本*/
        constructor(backgroundSkin: GYScaleSprite, barSkin: GYScaleSprite, sliderBtnSkin?: GYButton | IButtonSkin);
        release(): void;
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        set rotation(val: number);
        get rotation(): number;
        set sliderX(val: number);
        set sliderY(val: number);
        get sliderX(): number;
        get sliderY(): number;
        get sliderButton(): GYButton;
        /**克隆皮肤*/
        clone(): IGYSkin;
        /**此函数复制皮肤属性 如s.label gap等皮肤的属性*/
        copy(skin: IGYSkin): void;
    }
}
