/**
@author 迷途小羔羊
2012.11.18
*/
declare module GYLite {
    class GYButton extends ButtonBase {
        private _longDownTime;
        private _longDownInterval;
        private _continueDownTime;
        private _continueDownInterval;
        private _continueParam;
        /**按钮附加的参数*/ param: any;
        constructor(skin?: any);
        protected initComponent(): void;
        private addListener;
        private removeListener;
        protected rlOver(e: egret.TouchEvent): void;
        protected rlOut(e: egret.TouchEvent): void;
        protected mDown(e: egret.TouchEvent): void;
        protected mUp(e: egret.TouchEvent): void;
        protected releaseOutside(e: GYTouchEvent): void;
        private longDownLoop;
        private clearLongDown;
        private continueDownLoop;
        private clearContinueDown;
        protected mClk(e: egret.TouchEvent): void;
        /**点击按钮
         * @param func(e:egret.TouchEvent)
         * */
        getOnClick(): Function;
        setOnClick(func: Function, thisObject: any): void;
        set enabled(val: boolean);
        /**是否可点 */
        get enabled(): boolean;
        set toggle(tog: boolean);
        /**是否自动切换状态*/
        get toggle(): boolean;
        set selected(sel: boolean);
        /**是否选中*/
        get selected(): boolean;
        /**设置按钮状态*/
        set state(st: number);
        get state(): number;
        /**长按按钮时，经过多长时间触发长按事件(毫秒)*/
        get longDownInterval(): number;
        set longDownInterval(value: number);
    }
}
