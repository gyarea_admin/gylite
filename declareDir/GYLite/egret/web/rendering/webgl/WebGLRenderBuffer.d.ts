declare namespace egret.web {
    /**
     * @private
     * WebGL渲染缓存
     */
    class WebGLRenderBuffer extends HashObject implements sys.RenderBuffer {
        static autoClear: boolean;
        /**
         * 渲染上下文
         */
        context: WebGLRenderContext;
        /**
         * 如果是舞台缓存，为canvas
         * 如果是普通缓存，为renderTarget
         */
        surface: any;
        /**
         * root render target
         * 根渲染目标，用来执行主渲染
         */
        rootRenderTarget: WebGLRenderTarget;
        /**
         * 是否为舞台buffer
         */
        private root;
        currentTexture: WebGLTexture;
        constructor(width?: number, height?: number, root?: boolean);
        globalAlpha: number;
        globalTintColor: number;
        /**
         * stencil state
         * 模版开关状态
         */
        private stencilState;
        $stencilList: {
            x: number;
            y: number;
            width: number;
            height: number;
        }[];
        stencilHandleCount: number;
        enableStencil(): void;
        disableStencil(): void;
        restoreStencil(): void;
        /**
         * scissor state
         * scissor 开关状态
         */
        $scissorState: boolean;
        private scissorRect;
        $hasScissor: boolean;
        enableScissor(x: number, y: number, width: number, height: number): void;
        disableScissor(): void;
        restoreScissor(): void;
        /**
         * 渲染缓冲的宽度，以像素为单位。
         * @readOnly
         */
        get width(): number;
        /**
         * 渲染缓冲的高度，以像素为单位。
         * @readOnly
         */
        get height(): number;
        /**
         * 改变渲染缓冲的大小并清空缓冲区
         * @param width 改变后的宽
         * @param height 改变后的高
         * @param useMaxSize 若传入true，则将改变后的尺寸与已有尺寸对比，保留较大的尺寸。
         */
        resize(width: number, height: number, useMaxSize?: boolean): void;
        /**
         * 获取指定区域的像素
         */
        getPixels(x: number, y: number, width?: number, height?: number): number[];
        /**
         * 转换成base64字符串，如果图片（或者包含的图片）跨域，则返回null
         * @param type 转换的类型，如: "image/png","image/jpeg"
         */
        toDataURL(type?: string, encoderOptions?: number): string;
        /**
         * 销毁绘制对象
         */
        destroy(): void;
        onRenderFinish(): void;
        /**
         * 交换frameBuffer中的图像到surface中
         * @param width 宽度
         * @param height 高度
         */
        private drawFrameBufferToSurface;
        /**
         * 交换surface的图像到frameBuffer中
         * @param width 宽度
         * @param height 高度
         */
        private drawSurfaceToFrameBuffer;
        /**
         * 清空缓冲区数据
         */
        clear(): void;
        $drawCalls: number;
        $computeDrawCall: boolean;
        globalMatrix: Matrix;
        savedGlobalMatrix: Matrix;
        $offsetX: number;
        $offsetY: number;
        setTransform(a: number, b: number, c: number, d: number, tx: number, ty: number): void;
        transform(a: number, b: number, c: number, d: number, tx: number, ty: number): void;
        useOffset(): void;
        saveTransform(): void;
        restoreTransform(): void;
        /**
         * 创建一个buffer实例
         */
        static create(width: number, height: number): WebGLRenderBuffer;
        /**
         * 回收一个buffer实例
         */
        static release(buffer: WebGLRenderBuffer): void;
    }
}
