module GYLite
{
				
	/**富文本的行单元 @see GYRichText*/
	export class RichLine
	{
		private _lineArr:Array<any>=[];
		public constructor(pGroup:GYRichText, w:number)
		{
			let s = this;
			s._pGroup = pGroup;
			s._width=w;
		}
		/**@param index 插入索引，-1为放到末尾*/
		public Add(obj:Object,index:number = -1):void
		{
			let s = this;
			if(index == -1)
				s._lineVec.push(obj);
			else
				s._lineVec.splice(index,0,obj);
		}
		
		public Show():void
		{
			let s = this;
			var len:number=s._lineVec.length,len2:number=0,j:number=0;
			var lbl:RichLabel;
			var obj:any,obj2:any;
			var endBr:boolean;
			var noHref:boolean;
			var maxBottom:number=0;
			var newLine:boolean=false;
			function checkMaxBottom():void
			{
				if(maxBottom > 0)
				{
					len2 = s._lineArr.length;
					for(j=0;j<len2;++j)
					{
						obj2 = s._lineArr[j];
						if(obj2.isLbl == 1)
						{
							lineH=(<RichLabel>obj2.ui).getLineHeight(0);
							obj2.ui.y=maxBottom - lineH + s._pGroup.leading;
							continue;
						}
						obj2.ui.y = maxBottom - Number(obj2.height);
					}
					if(obj2.ui.y + obj2.ui.height > maxBottom)
						maxBottom = obj2.ui.y + obj2.ui.height;
					s._preBottom=s._nowBottom;
					s._y =s._nowBottom=maxBottom + s._pGroup.leading;
					s.bottom=s._nowBottom;
				}
				maxBottom = s._lineArr.length = 0;
				newLine = false;
			}
			for (var i:number=0; i < len; ++i)
			{
				obj=s._lineVec[i];
				if (!newLine && s._tmpX + s._pGroup.textFormat.size > s._pGroup.baseWidth)
				{
					s._tmpX=0;
					s._nowBottom += s._pGroup.leading;
					s._y=s._nowBottom;
					newLine = true;
				}
				if(newLine)
					checkMaxBottom();
				s._lineArr.push(obj);
				if (obj.isLbl == 1)
				{
					var strvalue:string=obj.text.toUpperCase();
					if (strvalue == "<BR>")
					{
						s._tmpX=0;
						s._nowBottom += s._pGroup.leading;
						s._y=s._nowBottom;
						newLine = true;
						continue;
					}
					endBr=noHref=false;
					if (strvalue.length > 3)
					{
						if (strvalue.substr(0, 4) == "<BR>")
						{
							s._tmpX=0;
							s._nowBottom+=s._pGroup.leading;
							s._y=s._nowBottom;
							obj.text=obj.text.substr(4);
							newLine = true;
						}
						if (strvalue.substr(strvalue.length - 4, 4) == "<BR>")
						{
							obj.text=obj.text.substr(0, strvalue.length - 4);
							endBr=true;
						}
						// if (strvalue.indexOf("<A") >= 0)
						// 	noHref=true;
					}
					lbl=s._pGroup.getRichLabel(s._tmpX);//new RichLabel(s._pGroup.textFormat,_tmpX,s._pGroup.linkFun);
					// if (!noHref)
					// 	lbl.touchEnabled=false;
					obj.ui=lbl;
					lbl.width=s._pGroup.baseWidth;
					s._pGroup.richSp.addChild(lbl);
					lbl.htmlText=obj.newLine?s.clearHTML(obj.text):obj.text;
					if(lbl.numLines > 1)//大于1行，截取最后一行单独成行
					{						
						var htmlStr:string = lbl.cutLastLine();
						s.Add({isLbl:1,text:htmlStr,newLine:true},i+1);
						++len;
						endBr = true;
					}
					// var rect:TextLineMetrics=lbl.FirstMetrics;
					var lineW:number,lineH:number;
					lineW = lbl.getLineWidth(0);
					lineH = lbl.getLineHeight(0);
					lbl.y=s._nowBottom - lineH + s._pGroup.leading;
					if (lbl.y < s._y)
						lbl.y=s._y;
					var newBottom:number=lbl.y + lbl.height;
					s._preBottom=s._nowBottom;
					s._nowBottom=newBottom;
					if (lbl.numLines > 1)
					{
						s._tmpX=lbl.getLineWidth(lbl.numLines - 1) + s._pGroup.letterSpacing;
						if (lineW > s._tmpW)
							s._tmpW=lineW;
					}
					else
					{
						s._tmpX+=lineW + s._pGroup.letterSpacing;
						if (s._tmpX > s._tmpW)
							s._tmpW=s._tmpX;
					}					
					
					if (endBr)
					{
						s._tmpX=0;
						s._nowBottom += s._pGroup.leading;
						s._y=s._nowBottom;
						newLine = true;
					}
					else if(i == len - 1)
						s._nowBottom += s._pGroup.leading;
					s.bottom=s._nowBottom;
					continue;
				}
				if (s._tmpX + Number(obj.width) > s._width)
				{
					s._tmpX=0;
					var toY:number=0;
					if (s._nowBottom == s._y)
						toY=s._y;
					else
					{
						toY=s._nowBottom + s._pGroup.leading;
						s._y = toY;
					}
					var tmp:number=s._nowBottom;
					s._nowBottom=toY + Number(obj.height);
					s._preBottom=tmp;
					// s.bottom=s._nowBottom;
					if (Number(obj.width) > s._tmpW)
						s._tmpW=Number(obj.width);
					newLine = true;
				}
				else
				{
					if (s._nowBottom == s._y)
					{
						toY=s._y;
						s._preBottom=s._nowBottom;
						s._nowBottom=toY + Number(obj.height);
						// s.bottom=s._nowBottom;
					}
					else
						toY=s._nowBottom - Number(obj.height);
					if(s._y != s._pGroup.richTop || toY < s._pGroup.richTop)
						maxBottom = s._y + Number(obj.height)> maxBottom?s._y + Number(obj.height):maxBottom;
				}
				var cls:any=egret.getDefinitionByName(obj.target);
				obj.ui=cls[obj.render](s._pGroup,s._tmpX, toY, obj.renderParam);
				s._tmpX+=Number(obj.width);
				if (s._tmpX > s._tmpW)
					s._tmpW=s._tmpX;
				s.bottom=(i == len - 1)?s._nowBottom + s._pGroup.leading:s._nowBottom;
			}
			checkMaxBottom();
		}
		/**清理回收*/
		public clear():void
		{
			let s = this;
			var len:number=s._lineVec.length;
			var obj:any;
			var lbl:RichLabel;
			for (var i:number=0; i < len; ++i)
			{
				obj=s._lineVec[i];
				if (obj.ui == null)
					continue;
				lbl=obj.ui;
				if (CommonUtil.GYIs(lbl, GYLite.RichLabel))
				{					
					s._pGroup.richSp.removeChild(lbl);
					lbl.clear();
				}
				else
				{
					var cls:any=egret.getDefinitionByName(obj.target);
					if (obj.clear)
						obj.ui[obj.clear]();
				}
			}
			s._lineVec.length = 0;
			s._lineVec=null;
		}
		
		public ResetY(value:number=0):void
		{
			let s = this;
			var len:number=s._lineVec.length;
			for (var i:number=0; i < len; ++i)
			{
				s._lineVec[i].ui.y -= value;
			}
			s.bottom -= value;
		}
		
		public set y(value:number)
		{
			let s = this;
			s._y=s.top=value;
			s._preBottom=s._nowBottom=value;
			s._tmpX=s._tmpW=0;
			//s._addSeq=-1;
		}
		
		public get width():number
		{
			let s = this;
			return s._tmpW;
		}
		private clearHTML(val:string):string
		{
			let s = this;
			var ind:number=0;
			val=val.replace("<P","<S").replace("</P>","<S>");
			val=val.replace("<TEXTFORMAT","<S").replace("</TEXTFORMAT>","<S>");
			return val;
		}
		
		private _lineVec:any[]=[];
		private _nowBottom:number=0;
		private _preBottom:number=0;
		private _tmpX:number=0;
		private _y:number=0;
		//private _addSeq:number=-1;
		public bottom:number=0;
		public top:number=0;
		private _pGroup:GYRichText;
		private _width:number=0;
		private _tmpW:number;
	}
}