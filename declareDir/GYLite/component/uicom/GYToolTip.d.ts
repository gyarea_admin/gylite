declare module GYLite {
    /**当鼠标悬停在显示对象的时候，设置s.toolTipString会弹出GYToolTip类的面板提示信息*/
    class GYToolTip extends GYSprite {
        private background;
        labelDisplay: GYText;
        private gradientMatrix;
        private colorArr;
        private alphaArr;
        private percentArr;
        tipfollowTarget: GYSprite;
        /**当前提示的显示对象*/
        user: IGYInteractiveDisplay;
        /**tip的s.x偏移,请在s.show方法调用前设置一次，s.hide方法调用将会被重置为默认值*/
        offsetX: number;
        /**tip的s.y偏移,请在s.show方法调用前设置一次，s.hide方法调用将会被重置为默认值*/
        offsetY: number;
        constructor();
        protected initComponent(): void;
        updateView(): void;
        protected drawBackground(w: number, h: number): void;
        setText(val: string): GYToolTip;
        /**显示tip*/
        show(layer: egret.DisplayObjectContainer, isFollow?: boolean): void;
        hide(): void;
        private FollowLoop;
        /**重写此方法，可以自定义布局的位置*/
        protected updatePos(): void;
        private static _toolTip;
        static getInstance(): GYToolTip;
    }
}
