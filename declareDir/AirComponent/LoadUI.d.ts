declare module GYLite {
    class LoadUI extends GYSprite {
        private gyProgressBar;
        constructor();
        setProg(val: number, max: number, lab?: string): void;
        show(pr: any): void;
        hide(): void;
        private static _instance;
        static getInstance(): LoadUI;
    }
}
