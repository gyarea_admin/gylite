module GYLite
{
			
							
	/**GYDrawImage一个可以绘制显示对象的位图类，内部使用GYDrawBitmapData临时位图*/
	export class GYDrawImage extends GYSprite
	{
		protected _mySource:egret.Bitmap=new egret.Bitmap;
		protected _bitData:GYDrawBitmapData;
		public constructor()
		{
			super();
			var s = this;
			s.addEventListener(MouseEvent.ROLL_OVER, s.RlOver,s);
			s.addChild(s._mySource);
		}
		public set source(bmp:GYDrawBitmapData)
		{
			let s = this;
			if(bmp == s._bitData)
				return;
			s.release();
			s._bitData = bmp?bmp.getRef():null;
			s._mySource.texture=s._bitData;
			if(!bmp)
				return;
			if(s.$width == s.$width)
				s.scaleX=s.$width/bmp.realWidth;
			if(s.$height == s.$height)
				s.scaleY=s.$height/bmp.realHeight;
			GYSprite._rect.y =GYSprite._rect.x = 0;
			GYSprite._rect.width = s._bitData.realWidth;
			GYSprite._rect.height = s._bitData.realHeight;
			s.scrollRect = GYSprite._rect;
			s.invalidDisplay();
		}
		/**位图数据*/		
		public get source():GYDrawBitmapData
		{
			let s = this;
			return s._bitData;
		}
		/**释放资源，由于使用的是GYDrawBitmapData临时位图，回收时必须释放其持有的GYDrawBitmapData引用*/
		public release():void
		{
			let s = this;
			if(s._bitData)
			{
				s._bitData.relRef();
				s._bitData = null;
			}
		}
		public draw(dis:any,clipRect:egret.Rectangle = null,smooth:boolean = false):void
		{
			let s = this;
			if(dis)
			{
				s.source = GYDrawBitmapData.getBitmapData(dis["width"],dis["height"]);
				s._bitData.draw(dis,clipRect,s.smooth);
			}
			else
				s.source = null;
		}
		public set width(val:number)
		{
			let s = this;			
			if(s.width == val)
				return;
			s.$width=val;
			if(s.$width != s.$width)
				s.scaleX=1;
			else
			{
				if(s._mySource.texture)
					s.scaleX=s.$width/s._bitData.realWidth;
			}
		}		
		public set height(val:number)
		{
			let s = this;	
			if(s.height == val)
				return;
			s.$height=val;
			if(s.$height != s.$height)
				s.scaleY=1;
			else
			{
				if(s._mySource.texture)
					s.scaleY=s.$height/s._bitData.realHeight;
			}
		}
		public get width():number
		{
			let s = this;
			if(s.$width != s.$width)
				return s._bitData?s._bitData.realWidth:egret.superGetter(GYDrawImage,s,"width");
			return s.$width;
		}
		public get height():number
		{
			let s = this;
			if(s.$height != s.$height)
				return s._bitData?s._bitData.realHeight:egret.superGetter(GYDrawImage,s,"height");
			return s.$height;
		}
		protected RlOver(e:egret.TouchEvent):void
		{
			let s = this;
			if (GYSprite.isStageDown(this))
				s.dispatchEvent(GYSprite.mouseDownEvent);
		}

		public get smooth():boolean
		{
			let s = this;
			return s._mySource.smoothing;
		}

		public set smooth(value:boolean)
		{
			let s = this;
			s._mySource.smoothing = value;
		}

	}
}