/**
@author 迷途小羔羊
2012.1.22
*/
declare module GYLite {
    /**羔羊组件的基类容器，使用羔羊组件时，建议至少使用此容器或者此容器的继承容器
     * 羔羊组件并不屏蔽原生的sprite、shape(可以使用s.addChild s.addChildAt等)，但是原生的容器类不参与组件布局或者计算
     * 所以建议使用GYSprite代替Sprite*/
    class GYSprite extends egret.Sprite implements IGYInteractiveDisplay, IGYContainer, IUpdate, IBatch {
        protected static _mouseDownVec: MouseRelease[];
        protected static _mouseWheelVec: MouseWheel[];
        static $skinTheme: ISkinTheme;
        /**内部使用*/ static mouseDownEvent: egret.TouchEvent;
        static _matrix: egret.Matrix;
        static _rect: egret.Rectangle;
        static _pt: egret.Point;
        /**请在初始化之后下一帧读取此值,此前为null*/ static player: any;
        static _globalDragList: GYGroup[];
        static stageX: number;
        static stageY: number;
        private static _touchDict;
        static isMobile: boolean;
        static hashCode: number;
        static globalVDrag: number;
        static globalVDragCount: number;
        static globalHDrag: number;
        static globalHDragCount: number;
        protected static _root: egret.DisplayObjectContainer;
        /**暂时没用*/ static useStarling: boolean;
        /**版本号*/ static version: string;
        static $stage: egret.Stage;
        static $disposed: boolean;
        private static playerInited;
        /**初始化组件库，在组件库使用之前必须先调用此方法初始化
         * @param stg 舞台
         * @param theme 主题
         * @param rt 根容器容器，默认为null，使用stg作为根容器
        */
        static Init(stg: egret.Stage, theme: ISkinTheme, rt?: egret.DisplayObjectContainer): void;
        static get disposed(): boolean;
        static dispose(e?: egret.Event): void;
        /**对引擎进行垃圾回收*/
        static gc(): void;
        /**Stage舞台*/
        static get stage(): egret.Stage;
        /** 全局的鼠标按下回调,请使用GYSprite.addStageDown添加回调侦听 */
        private static stageMouseDown;
        static removeTouch(touchPointID: number): void;
        static addTouch(touchPointID: number, x: number, y: number): void;
        static getTouch(touchPointID: number): any;
        static getTouchX(touchPointID?: number): number;
        static getTouchY(touchPointID?: number): number;
        /** 全局的鼠标滚轮,请使用GYSprite.addStageWheel添加回调侦听 */
        protected static stageMouseWheel(e: any): void;
        /**添加舞台滚轮对象
         * @param sp 显示对象
         * @param mouseWheelFunc(e:GYTouchEvent):void 鼠标滚动时的回调函数
         * */
        static addStageWheel(sp: IGYDisplay, mouseWheelFunc?: Function, thisObject?: any): void;
        /**移除舞台滚轮对象
         * @param sp 显示对象
         * @param mouseWheelFunc(e:GYTouchEvent):void 鼠标滚动时的回调函数
         * */
        static removeStageWheel(d: IGYDisplay, func?: Function): void;
        protected static findMouseWheel(d: IGYDisplay, func?: Function): MouseWheel;
        /** 全局的鼠标抬起回调,请使用GYSprite.addStageDown添加回调侦听 */
        protected static stageMouseUp(e: any): void;
        /**移除舞台按下监听对象*/
        static removeStageDown(sp: IGYDisplay, mouseUpFunc?: Function, thisObject?: any): void;
        private static clearMouseReleaseByIndex;
        /**添加舞台点击对象
         * @param sp 显示对象
         * @param mouseUpFunc(e:GYTouchEvent):void 鼠标抬起时的回调函数
         * @param thisObject 回调this指向
         * @param uUp 是否监听直到鼠标在外部释放 GYTouchEvent的result为1时，则会一直监听，默认false
         * @param touchPointID 触摸id，默认为NaN，即任意id的手指抬起均触发
         * */
        static addStageDown(sp: IGYDisplay, mouseUpFunc?: Function, thisObject?: any, uUp?: boolean, touchPointID?: number): void;
        /**是否当前鼠标按下的对象
         * @param sp 显示对象
         * @return Boolean
         * */
        static isStageDown(sp: IGYDisplay): boolean;
        protected static findMouseRel(d: IGYDisplay, func?: Function, thisObject?: any): MouseRelease;
        protected static findMouseRelIndex(d: IGYDisplay, func?: Function, thisObject?: any): number;
        protected static hasGlobalVDrag(): boolean;
        protected static hasGlobalHDrag(): boolean;
        protected static pushGlobalDrag(sp: GYGroup): boolean;
        /**1纵向 2横向*/
        protected static shiftGlobalDrag(policy: number): boolean;
        protected static clearGlobalDrags(): void;
        /**
         * 设置主题皮肤，监听GYThemeEvent.THEME_CHANGE通知
         * */
        static set skinTheme(val: ISkinTheme);
        static get skinTheme(): ISkinTheme;
        protected _minWidth: number;
        protected _minHeight: number;
        protected _maxWidth: number;
        protected _maxHeight: number;
        $width: number;
        $height: number;
        $layoutParent: GYUIComponent;
        protected _layoutMode: LayoutMode;
        protected _invalidDisplay: boolean;
        protected _invalidLayout: boolean;
        protected _updating: boolean;
        /**拖拽时的偏移量X，默认NaN，拖拽按照原始位置拖，否则以左上角为基准偏移*/
        dragOffsetX: number;
        /**拖拽时的偏移量Y，默认NaN，拖拽按照原始位置拖，否则以左上角为基准偏移*/
        dragOffsetY: number;
        protected _dragHandler: DraggerHandle;
        protected _toolTip: GYToolTip;
        protected _toolTipOffsetX: number;
        protected _toolTipOffsetY: number;
        protected _toolTipString: string;
        protected _toolTipKeep: boolean;
        protected _followTarget: GYSprite;
        protected _toolTipOnKeep: boolean;
        private _doubleClkTime;
        private _doubelClk;
        /**GYSprite元素列表*/
        protected elementVec: IGYDisplay[];
        protected _bound: egret.Rectangle;
        protected _elementbound: egret.Rectangle;
        protected _invalidZIndex: boolean;
        protected _disabledZIndex: boolean;
        $disposed: boolean;
        /**防销毁锁定标志，为true则不会被dispose销毁，除非参数forceDispose为true*/ disposeLock: boolean;
        /**是否tip跟随鼠标*/ isTipFollow: boolean;
        /**是否不参与布局计算*/ offLayout: boolean;
        /**视图坐标变化通知很多时候不需通知父级，可用此控制是否使用，默认false*/
        posCallUpdate: any;
        /**视图变化通知很多时候不需通知父级，可用此控制是否使用，默认false*/
        viewChangeCallParent: any;
        /**鼠标经过时手型，true为默认手指或者指定一个网络图片，32X32以下*/ buttonModeForMouse: any;
        protected _invalidBatch: boolean;
        protected _batchDrawParam: BatchDrawParam;
        protected _batch: boolean;
        protected _batchAtlasName: string;
        constructor();
        clearBatch(): void;
        setBatchDrawParam(val: BatchDrawParam): void;
        /**合批图像的绘制样式**/
        getBatchDrawParam(): BatchDrawParam;
        setBatchAtlasName(val: string): void;
        /**合批图集名称，不存在找父级容器的，如果都不存在，默认AtlasRender.defaultAtlasName**/
        getBatchAtlasName(): string;
        /** 舞台渲染前执行 */
        protected displayChg(): void;
        get mouseX(): number;
        get mouseY(): number;
        get touch(): number;
        /**设置双击回调*/
        addEventListener(type: string, listener: Function, thisObject: any, useCapture?: boolean, priority?: number): void;
        removeEventListener(type: string, listener: Function, thisObject: any, useCapture?: boolean): void;
        protected $doubleClick(e: egret.TouchEvent): void;
        /**设置滚轮回调 */
        setWheelFunc(func: Function, thisObject?: any): void;
        /**获取所有对象边界矩形，在GYSprite容器getElementsBounds和getAllBounds是一样的功能*/
        getAllBounds(t: IGYDisplay): egret.Rectangle;
        /**获取所有对象边界矩形，在GYSprite容器getElementsBounds和getAllBounds是一样的功能，在GYUIComponent则有所区别**/
        getElementsBounds(t: IGYDisplay): egret.Rectangle;
        /**根据索引获取元素 */
        getElementAt(index: number): IGYDisplay;
        /**根据获取元素层级 */
        getElementIndex(child: IGYDisplay): number;
        /**删除元素*/
        delElement(child: IGYDisplay): void;
        /**添加元素 */
        addElement(child: IGYDisplay): IGYDisplay;
        /**添加元素到指定层级*/
        addElementAt(child: IGYDisplay, index: number): IGYDisplay;
        /**移除元素*/
        removeElement(child: IGYDisplay): IGYDisplay;
        /**移除所有元素*/
        removeAllElement(): void;
        /**移除指定层级的元素*/
        removeElementAt(index: number): IGYDisplay;
        /**设置元素层级*/
        setElementIndex(child: IGYDisplay, index: number): void;
        /**交换元素层级*/
        swapElementIndex(child1: IGYDisplay, child2: IGYDisplay): void;
        set x(val: number);
        set y(val: number);
        get x(): number;
        get y(): number;
        set width(val: number);
        set height(val: number);
        get width(): number;
        get height(): number;
        set_width(val: number): boolean;
        set_height(val: number): boolean;
        get_width(): number;
        get_height(): number;
        /**设置可拖拽*/
        dragSet(val: boolean, offsetX?: number, offsetY?: number): void;
        get dragEnabled(): boolean;
        /**设置百分比宽度,范围[0,1]*/
        get percentWidth(): number;
        set percentWidth(val: number);
        /**设置百分比高度,范围[0,1]*/
        get percentHeight(): number;
        set percentHeight(val: number);
        /**是否百分比的宽高*/
        isPercentSize(): boolean;
        set layoutMode(val: LayoutMode);
        /**布局对象*/
        get layoutMode(): LayoutMode;
        viewChange(): void;
        /**重写s.updateView可以为其添加延时计算的内容(做法仿照s.invalidDisplay)，GYSprite内置了布局s.layout以及子级布局s.childLayout的延时计算
         * 切记避免死循环的延时计算，例如延时执行A，在A的执行里面又触发了A的延时执行
         * */
        updateView(): void;
        /**css层级显示*/
        invalidZIndex(): void;
        validZIndex(): void;
        /**刷新显示*/
        invalidDisplay(): void;
        /**刷新子级布局*/
        invalidLayout(): void;
        /**禁止zindex层级排序*/
        get disabledZIndex(): boolean;
        set disabledZIndex(val: boolean);
        /**添加父级布局容器，以便父级变化时通知子级布局
         * @param p父级容器
         * */
        addLayout(p: GYUIComponent): void;
        /**获取布局容器(因为存在双层容器如GYGroup，用此方法取相对布局容器更准确)*/
        getLayoutContainer(): egret.DisplayObjectContainer;
        /**父级*/
        get layoutParent(): GYUIComponent;
        protected layoutChange(e: GYViewEvent): void;
        set left(val: number);
        set top(val: number);
        set right(val: number);
        set bottom(val: number);
        set horizonalCenter(val: number);
        set verticalCenter(val: number);
        /**相对左边布局*/
        get left(): number;
        /**相对顶部布局*/
        get top(): number;
        /**相对右边布局*/
        get right(): number;
        /**相对底边布局*/
        get bottom(): number;
        /**水平居中*/
        get horizonalCenter(): number;
        /**垂直居中*/
        get verticalCenter(): number;
        childLayout(): void;
        /**刷新相对布局*/
        layout(): void;
        /**刷新相对大小*/
        sizeChange(): void;
        get numElement(): number;
        /**获取设定的宽度*/
        get settingWidth(): number;
        /**获取设定的高度*/
        get settingHeight(): number;
        /**提示文本,可以通过s.toolTip设置自定义的GYToolTip(继承此类重写实现)*/
        set toolTipString(val: string);
        get toolTipString(): string;
        set toolTip(val: GYToolTip);
        /**s.toolTip，继承GYToolTip实现自定义@see GYToolTip*/
        get toolTip(): GYToolTip;
        protected rollOverTip(e?: egret.TouchEvent): void;
        showTip(keep?: boolean, followTarget?: GYSprite): void;
        protected rollOutTip(e?: egret.TouchEvent): void;
        protected mouseOutTip(e?: egret.TouchEvent): void;
        /**tip偏移量X*/
        get toolTipOffsetX(): number;
        set toolTipOffsetX(value: number);
        /**tip偏移量Y*/
        get toolTipOffsetY(): number;
        set toolTipOffsetY(value: number);
        /**是否保留tip直到鼠标抬起*/
        get toolTipKeep(): boolean;
        set toolTipKeep(value: boolean);
        /**tip跟随对象*/
        get followTarget(): GYSprite;
        set followTarget(value: GYSprite);
        /**是否刷新中（内部使用请勿修改）*/
        get updating(): boolean;
        set updating(value: boolean);
        getElementList(): Array<IGYDisplay>;
        /**是否在父级视图范围内(0,0,width,height),可用于做裁切视图时的离屏渲染优化*/
        inParentView(l?: number, r?: number, t?: number, b?: number): boolean;
        /**销毁
         * @param disposeChild 是否连同显示列表上的子级也销毁，默认true
         * @param removeChild 是否从父级显示列表中移除，默认true
         * @param forceDispose 是否强制销毁，为true表示无视disposeLock标志，进行销毁，默认false
        */
        dispose(disposeChild?: boolean, removeChild?: boolean, forceDispose?: boolean): void;
        /**是否已经被销毁*/
        get disposed(): boolean;
        invalidBatch(): void;
        validBatch(): void;
        enableBatch(val: boolean, batchAtlas?: string): void;
        /**是否动态合批，请在文本渲染前设定，默认根据父级容器batch，如果父级也没设定，则默认false*/
        isBatch(): boolean;
        /**设置对象坐标依赖对象a坐标布局
         * @param a 对象a
         * @param flag 标志 1 x坐标 2 y坐标 3 xy坐标
         * @param invert 是否反向，默认0 1 x反向 2 y反向 3 xy均反向
         * @param padX 间隔x
         * @param padY 间隔y
        */
        placeToB(a: GYSprite, flag?: number, invert?: number, padX?: number, padY?: number): GYLite.GYSprite;
        get minWidth(): number;
        set minWidth(value: number);
        get minHeight(): number;
        set minHeight(value: number);
        get maxWidth(): number;
        set maxWidth(value: number);
        get maxHeight(): number;
        set maxHeight(value: number);
    }
    class MouseWheel {
        private static _pool;
        static getInstance(d: IGYDisplay, callBack: Function, thisObject: any): MouseWheel;
        callBack: Function;
        display: IGYDisplay;
        thisObject: any;
    }
    class MouseRelease {
        private static _pool;
        static getInstance(d: IGYDisplay, callBack: Function, thisObject: any, touchPointID?: number): MouseRelease;
        callBack: Function;
        display: IGYDisplay;
        untilUp: boolean;
        thisObject: any;
        touchPointID: number;
        constructor();
        clear(): void;
    }
}
