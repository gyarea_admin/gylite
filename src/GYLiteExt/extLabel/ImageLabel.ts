module GYLite{
	export class ImageLabel extends GYSprite
	{	
		private _dict:ILabelData;
		private _text:string;
		private _invalidText:boolean;
		private _bitmapVec:Array<GYScaleSprite>;
		private _formatVec:Array<TextFormat>
		private _align:string;
		private _clipBitmap:egret.Texture;
		private _textWidth:number;
		private _textHeight:number;
		public constructor()
		{
			super();
			let s= this;
			s._dict = {};
			s._text = "";
			s._textWidth = s._textHeight = NaN;
			s._invalidText=false;			
			s._align = "left";
			s._formatVec = [];
			s.initComponent();
		}
		
		protected initComponent():void
		{
	//			_vec = new Vector.<ImageLabelData>;
		}
		public getBitmap(ind:number):GYScaleSprite
		{let s = this;
			if(s._bitmapVec[ind] == null)
				s._bitmapVec[ind] = new GYScaleSprite();
			s.addElement(s._bitmapVec[ind]);
			return s._bitmapVec[ind];
		}
		public setData(val:ILabelData):void
		{let s = this;
			s._dict = val;
			if(s._bitmapVec)
			{
				var len;
				len = s._bitmapVec.length;
				while(--len>-1)
				{
					if(s._bitmapVec[len] && s._bitmapVec[len].parent)
						s.removeElement(s._bitmapVec[len]);
				}
			}
			s._bitmapVec = [];
			s.invalidText();
		}

		public set width(val:number)
		{
			egret.superSetter(ImageLabel, this, "width", val);
			this.invalidText();
		}
		public get width():number
		{
			return egret.superGetter(ImageLabel, this, "width");
		}

		public set height(val:number)
		{
			egret.superSetter(ImageLabel, this, "height", val);			
			this.invalidText();
		}
		public get height():number
		{
			return egret.superGetter(ImageLabel, this, "height");
		}

		public get text():string
		{let s = this;
			return s._text;
		}

		public set text(value:string)
		{let s = this;
			s._text = value + "";
			// if(s._dict.length == 0)return;
			s.validText();
		}
		public set align(val:string)
		{let s= this;
			s._align= val;
		}
		public get align():string
		{
			return this._align;
		}

		public set clipBitmap(val:egret.Texture)
		{let s= this;
			s._clipBitmap= val;
		}
		public get clipBitmap():egret.Texture
		{let s = this;
			return s._clipBitmap;
		}

		public get textWidth():number
		{
			return this._textWidth;
		}
		public get textHeight():number
		{
			return this._textHeight;
		}

		public setForamt(ind:number,t:TextFormat):void
		{
			this._formatVec[ind] = t;
		}
		
		public updateView():void
		{let s = this;
			if(s._invalidText)
			{
				s._invalidText =false;
				s.validText();
			}
		}
		
		public invalidText():void
		{let s = this;
			if(s._invalidText)return;
			s._invalidText = true;
			LayoutManager.addRenderFunc(this);
		}
		
		public validText():void
		{let s = this;
			var i:number,j:number,len:number;			
			var charCode:number;
			var b:GYLite.GYScaleSprite,bit:egret.Texture;
			var toX:number = 0, startX:number = 0, startY:number = 0;
			let offsetX:number;
			len = s._text.length;
			for(i=0,j=0;i<len;++i)
			{				
				charCode = s._text.charCodeAt(i);
				if(!s._dict[charCode])
					continue;				
				b = s.getBitmap(j);
				let imgData:ImageLabelData = s._dict[charCode];		
				bit = s._dict[charCode].bitmapData;
				
				if(!s._clipBitmap)
				{
					b.mode = ScaleMode.SCALE;
					offsetX = imgData.offsetX;
				}
				else
				{
					b.mode = ScaleMode.CLIP;
					bit = s._clipBitmap;
					b.width = imgData.width;
					b.height = imgData.height;
					b.clipX = imgData.offsetX;
					offsetX = 0;
				}
				b.bitmapData = bit;
				let format:GYLite.TextFormat = s._formatVec[i] == null?s._formatVec[0]:s._formatVec[i];
				if(format)
				{
					b.scaleY = b.scaleX = format.size / b.width;
				}
				b.x = toX + offsetX;				
				b.y = imgData.height * (1-b.scaleY) + imgData.offsetY;
				if(i == 0)
				{
					startX = b.x;
					startY = b.y;
				}					
				toX = b.x + imgData.width*b.scaleX;				
				++j;
			}
			s._textWidth = toX - startX;
			s._textHeight = b?b.height - startY:0;
			len = s._bitmapVec.length;
			for(i=j;i<len;++i)
			{
				if(s._bitmapVec[i] && s._bitmapVec[i].parent)
					s.removeElement(s._bitmapVec[i]);
			}
			if(s._align != "left" && s.$width == s.$width)
			{
				len = s._bitmapVec.length;				
				offsetX = s._align == "center"?(s.$width - toX >> 1):(s.$width - toX);
				for(i=0;i<len;++i)
				{					
					b = s._bitmapVec[i];
					if(b.parent == null)break;
					b.x += offsetX;
				}
			}
		}
	}
}