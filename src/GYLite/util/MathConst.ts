module GYLite
{
	export class MathConst
	{
		public static DOUBLE_PI:number = Math.PI * 2;
		public static TRIBLE_PI:number = Math.PI * 3;
		public static HALF_PI:number = Math.PI / 2;
		public static TRIBLE_HALF_PI:number = MathConst.HALF_PI * 3;
		public static QUATER_PI:number = Math.PI / 4;
		public static QUATER_TRIBLE_PI:number = MathConst.TRIBLE_PI / 4;
		public static ANGLE_ROTATION:number = 180 / Math.PI;
		public static ROTATION_ANGLE:number = Math.PI / 180;
	}
}