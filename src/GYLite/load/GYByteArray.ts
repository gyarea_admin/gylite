/**
 * @author 迷途小羔羊
 * 2019-5-6
 */
module GYLite
{
	export class GYByteArray extends egret.ByteArray{
		public constructor(buffer?: ArrayBuffer | Uint8Array) {
			super(buffer);
		}
		/**zlib算法解压 需要引用zlib.min.js*/
		public uncompress(algorithm:String="zlib"):void
		{let s = this;
			var inflate = new Zlib.Inflate(new Uint8Array(s.buffer));
			var inbuffer = inflate.decompress();
			s.buffer = inbuffer;//.buffer;
			s._position = 0;
		}
		/**zlib算法压缩 需要引用zlib.min.js*/
		public compress(algorithm:String="zlib"):void
		{let s = this;
			let deflate = new Zlib.Deflate(new Uint8Array(s.buffer));
			let inbuffer = deflate.compress();
			s.buffer=inbuffer;//.buffer;
			s._position = 0;
		}
		/**获取字节数据的唯一识别key(此方法只平均取5位数据加长度做粗略区分，可能会存在碰巧一致的情况),0-0.25-0.5-0.75-1-length
		 * **/
		public static getBytesHash(bytes:any):string
		{
			let u8:Uint8Array;
			if(bytes instanceof GYByteArray || bytes instanceof egret.ByteArray)
			{
				u8 = bytes.bytes;
			}
			else if(bytes instanceof Uint8Array)
			{
				u8 = bytes;
			}
			else if(bytes instanceof ArrayBuffer)
			{
				u8 = new Uint8Array(bytes);
			}
			if(u8 == null)
				SysError.BYTES_ERROR.throwError(null);
			let len:number = u8.byteLength;
			return u8[0] + "-" + u8[len*0.25|0] + "-" + u8[len*0.5|0] + "-" + u8[len*0.75|0] + "-" + u8[len - 1] + "-" + len;
		}
	}
}