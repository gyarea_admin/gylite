declare module GYLite {
    interface ISliderSkin extends IProgressBarSkin {
        sliderX: number;
        sliderY: number;
        sliderButton: GYButton;
    }
}
