/**
@author 迷途小羔羊
2022.09.27
*/
declare module GYLite {
    class GYBitmapNode extends egret.sys.BitmapNode implements IUpdate {
        left: number;
        right: number;
        top: number;
        bottom: number;
        color: number;
        protected fillMode: string;
        protected scale9Grid: egret.Rectangle;
        protected _updating: boolean;
        protected _WMidData: egret.Texture;
        protected _HMidData: egret.Texture;
        protected _scale9GridRect: Scale9GridRect;
        protected _clipX: number;
        protected _clipY: number;
        protected _drawX: number;
        protected _drawY: number;
        protected _mode: number;
        protected _invalidDraw: boolean;
        protected _renderMatrix: egret.Matrix;
        protected _repeat: boolean;
        protected _repeatWidth: number;
        protected _repeatHeight: number;
        protected _repeatX: number;
        protected _repeatY: number;
        protected _width: number;
        protected _height: number;
        protected _texture: egret.Texture;
        protected _matrix: egret.Matrix;
        protected _angle: number;
        protected _tx: number;
        protected _ty: number;
        protected _alpha: number;
        constructor(value?: egret.Texture, rect?: Scale9GridRect);
        setRect(l: number, r: number, t: number, b: number): void;
        /**设置节点透明度*/
        setAlpha(a: number): void;
        /**x y位置偏移量*/
        translate(x: number, y: number): void;
        /**设置x y纹理偏移*/
        setOffset(x: number, y: number): void;
        set angle(agl: number);
        get angle(): number;
        set rotation(degree: number);
        get rotation(): number;
        private draw;
        beginBitmapFill(m: egret.Matrix, r: boolean): void;
        drawRect(tX: number, tY: number, w: number, h: number): void;
        clearBitmapFill(): void;
        set repeatWidth(val: number);
        /**重复模式的砖块宽度*/
        get repeatWidth(): number;
        set repeatHeight(val: number);
        /**重复模式的砖块高度*/
        get repeatHeight(): number;
        set repeatX(val: number);
        /**重复模式的起始X*/
        get repeatX(): number;
        set repeatY(val: number);
        /**重复模式的起始Y*/
        get repeatY(): number;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        /**绘制模式 @see ScaleMode
         * @param val 0 拉伸 1 裁切 2 重复 */
        set mode(val: number);
        set scale9GridRect(value: Scale9GridRect);
        set clipX(val: number);
        set clipY(val: number);
        /**裁切的s.x坐标*/
        get clipX(): number;
        /**裁切的s.y坐标*/
        get clipY(): number;
        set drawX(val: number);
        set drawY(val: number);
        /**绘制点的s.x坐标*/
        get drawX(): number;
        /**绘制点的s.y坐标*/
        get drawY(): number;
        /**刷新绘图*/
        invalidDraw(): void;
        get scale9GridRect(): Scale9GridRect;
        /**是否刷新中（内部使用请勿修改）*/
        get updating(): boolean;
        set updating(value: boolean);
        validDraw(): void;
        updateView(): void;
        dispose(): void;
        disposed: boolean;
    }
}
