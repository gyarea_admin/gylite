declare module GYLite {
    class GameToolTip extends GYToolTip {
        backBitData: egret.Texture;
        private scaleShape;
        constructor();
        protected initComponent(): void;
        protected drawBackground(w?: number, h?: number): void;
        private static _gameToolTip;
        static getInstance(): GYToolTip;
    }
}
