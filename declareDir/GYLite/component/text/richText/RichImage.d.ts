declare module GYLite {
    /**富文本的图片单元 @see GYRichText*/
    class RichImage extends GYLoadImage implements IPoolObject {
        owner: GYRichText;
        constructor();
        get inPool(): boolean;
        set inPool(val: boolean);
        outPoolInit(): void;
        private _inPool;
        /**清理回收*/
        clear(): void;
        private static _pool;
        /**获取图片实例
         * @param pr 父级对象
         * @param posX s.x坐标
         * @param posY s.y坐标
         * @param param 参数*/
        static getInstance(pr: GYRichText, posX: number, posY: number, param: string): RichImage;
        /**获取图片实例 param url,s.width,s.height*/
        static getUrlInstance(pr: GYRichText, posX: number, posY: number, param: string): RichImage;
    }
}
