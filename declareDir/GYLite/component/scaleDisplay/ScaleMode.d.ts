declare module GYLite {
    class ScaleMode {
        /**缩放*/ static SCALE: number;
        /**裁切*/ static CLIP: number;
        /**重复*/ static REPEAT: number;
        /**自由*/ static FREE: number;
    }
}
