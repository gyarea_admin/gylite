module GYLite
{
    export class Vector2{
        public elements:Float32Array;
        constructor(x:number=0, y:number=0) {
            this.elements = new Float32Array(2);            
            var v:Float32Array = this.elements;
            v[0] = x;
            v[1] = y;
        }
        
        /**
         *从Array数组拷贝值。
            *@param array 数组。
            *@param offset 数组偏移。
            */
        public fromArray(array:number[], offset:number=0):void {            
            this.elements[0] = array[offset + 0];
            this.elements[1] = array[offset + 1];
        }

        /**
         *克隆。
            *@param destObject 克隆源。
            */
        public cloneTo(destObject:Vector2):void {
            var destVector2:Vector2 = destObject;
            var destE:Float32Array = destVector2.elements;
            var s:Float32Array = this.elements;
            destE[0] = s[0];
            destE[1] = s[1];
        }

        /**
         *克隆。
            *@return 克隆副本。
            */
        public clone():Vector2 {
            let cls:any;
            cls = this.constructor;
            var destVector2:Vector2 = new cls();
            this.cloneTo(destVector2);
            return destVector2;
        }
        
        /**
         *获取X轴坐标。
            *@return X轴坐标。
        */
        public get x():number {
            return this.elements[0];
        }
        /**
         *设置X轴坐标。
            *@param value X轴坐标。
            */        
        public set x(value:number) {
            this.elements[0] = value;
        }

                
        /**
         *获取Y轴坐标。
            *@return Y轴坐标。
        */
        public get y():number {
            return this.elements[1];
        }
        /**
         *设置Y轴坐标。
            *@param value Y轴坐标。
        */
        public set y(value:number) {
            this.elements[1] = value;
        }

        public static scale(a:Vector2, b:number, out:Vector2):void {
            var e:Float32Array = out.elements;
            var f:Float32Array = a.elements;
            e[0] = f[0] * b;
            e[1] = f[1] * b;
        }
        
        public static ZERO:Vector2 = new Vector2(0, 0);
        public static ONE:Vector2 = new Vector2(1, 1);        
    }
    
}