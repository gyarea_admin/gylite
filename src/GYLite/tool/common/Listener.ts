module GYLite
{
	export class Listener
	{
		private _listenerVec:IListener[];
		private _listenId:number;		
		private static c:number=0;
		public constructor()
		{var s = this;
			s._listenerVec = [];
			s._listenId = Listener.c++;
		}
		public length():number
		{
			return this._listenerVec.length;
		}
		public addListener(listener:IListener):number
		{var s = this;
			if(s._listenerVec.indexOf(listener) == -1)
				s._listenerVec.push(listener);
			return s._listenId;
		}
		public removeListener(listener:IListener):number
		{var s = this;
			var ind:number;
			ind = s._listenerVec.indexOf(listener);
			if(ind == -1)return s._listenId;
			s._listenerVec.splice(ind, 1);
			return s._listenId;
		}
		public dataChange():void
		{var s = this;
			var len:number;
			len = s._listenerVec.length;
			if(len > 0)
			{
				while(--len>-1)
				{
					s._listenerVec[len].dataChange(s._listenId);
				}
			}
		}

		public get listenId():number
		{var s = this;
			return s._listenId;
		}
		public clear():void{
			this._listenerVec.length = 0;			
		}
		public dispose():void
		{
			this._listenerVec.length = 0;			
		}
	}
}