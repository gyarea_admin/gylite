declare namespace egret {
    /**
     * TextFieldType class is an enumeration of constant value used in setting the type property of the TextField class.
     * @version Egret 2.4
     * @platform Web,Native
     * @language en_US
     */
    /**
     * TextFieldType 类是在设置 TextField 类的 type 属性时使用的常数值的枚举。
     * @version Egret 2.4
     * @platform Web,Native
     * @language zh_CN
     */
    class TextFieldType {
        /**
         * Used to specify dynamic text
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 用于指定动态文本
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static DYNAMIC: string;
        /**
         * Used to specify the input text
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 用于指定输入文本
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static INPUT: string;
    }
}
