declare module GYLite {
    class GYTextArea extends GYSkinContainer {
        private _textArea;
        private _vScroller;
        private _textGrp;
        private _editable;
        private _invalidTextArea;
        constructor(skin?: any);
        protected initComponent(): void;
        protected updateComp(event: GYViewEvent): void;
        private domMouseDown;
        private stageTextShow;
        private textUp;
        private textRollOver;
        private textRollOut;
        private textOver;
        private staticTextShow;
        private selectedLoop;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        get textArea(): GYText;
        get vScroller(): GYScroller;
        set editable(val: boolean);
        /**是否可编辑*/
        get editable(): boolean;
        get textGrp(): GYGroup;
        /**获取主题皮肤，自定义请实现IGYSkin*/
        protected getThemeSkin(): IGYSkin;
        get text(): string;
        set text(value: string);
        setPadding(val: number): void;
        set paddingTop(val: number);
        set paddingLeft(val: number);
        set paddingRight(val: number);
        set paddingBottom(val: number);
        get paddingTop(): number;
        get paddingLeft(): number;
        get paddingRight(): number;
        get paddingBottom(): number;
        updateView(): void;
        invalidTextArea(): void;
        private groupUpdateComp;
    }
}
