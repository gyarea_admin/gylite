/**
@author 迷途小羔羊
2012.11.18
*/
declare module GYLite {
    class GYImage extends GYUIComponent {
        protected static _imgLoader: GYLoader;
        static init(): void;
        protected _mySource: GYBitmap;
        protected _anchorX: number;
        protected _anchorY: number;
        protected _pivotX: number;
        protected _pivotY: number;
        protected _smoothing: boolean;
        protected _texture: egret.Texture;
        constructor();
        validBatch(): void;
        set smoothing(val: boolean);
        get smoothing(): boolean;
        set source(bmp: egret.Texture);
        /**位图*/
        get source(): egret.Texture;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        protected RlOver(e: egret.TouchEvent): void;
        /**中心锚点（x百分比）*/
        get anchorX(): number;
        set anchorX(value: number);
        /**中心锚点（y百分比）*/
        get anchorY(): number;
        set anchorY(value: number);
        /**中心点X（像素值）*/
        get pivotX(): number;
        set pivotX(value: number);
        /**中心点Y（像素值）*/
        get pivotY(): number;
        set pivotY(value: number);
        setBatchDrawParam(val: BatchDrawParam): void;
        /**合批图像的绘制样式**/
        getBatchDrawParam(): BatchDrawParam;
        enableBatch(val: boolean): void;
        /**是否动态合批，请在文本渲染前设定*/
        isBatch(): boolean;
        get bitmap(): GYBitmap;
        /**跟pivotX 一样功能(废弃)*/
        get archorX(): number;
        /**跟pivotY 一样功能(废弃)*/
        set archorX(value: number);
        /**跟pivotY 一样功能(废弃)*/
        get archorY(): number;
        /**跟pivotY 一样功能(废弃)*/
        set archorY(value: number);
        protected resetBound(): void;
        protected resetElementsBound(): void;
    }
}
