module GYLite
{
														
							
	export class GYExcel extends GYSprite implements IKeyBoardObject
	{
		protected _dataGrid:GYDataGrid;
		protected _rowsDataList:GYDataListV;
		protected _colsDataList:GYDataListH;
		protected _selFrame:ExcelFrame;
		protected _frameRect:egret.Rectangle;
		protected _movePadding:number=0;
		public config:any
		/**GYExcel是由GYDataGrid组合而成是简易excel组件
		 * 由excel包下的类组合而成，可见GYDataGrid的功能强大，更多功能请自行扩展，或者自己构建一个
		 * */
		public constructor(cfg:any = null)
		{
			super();
			var s = this;
			s.touchEnabled = true;
			if(cfg == null)
				cfg={
					frameColor:0xff0000,
					frameCalColor:0x0000ff,
					frameHandlerColor:0xff0000,
					frameHandlerBorderColor:0xffffff
				};
			s.config = cfg;
			s.init();
		}
		protected init():void
		{var s = this;
			s._movePadding = 50;
			s._dataGrid = new GYDataGrid(0,0,s.createExcelGrid,s,s.setExcelGrid,s);
			s._dataGrid.boxW=100;
			s._dataGrid.boxH=30;
			s._dataGrid.width = 400;
			s._dataGrid.height = 120;
			s._dataGrid.hScroller.sliderMinSize = 16;
			s._dataGrid.vScroller.sliderMinSize = 16;
			s._dataGrid.canSelectNum = Number.MAX_VALUE;
			s._dataGrid.canDrag = false;
			s._dataGrid.dragSelect = true;
			s._dataGrid.addEventListener(GYViewEvent.VIEWCHANGE, s.dataGridViewChange,s);
			s._dataGrid.addEventListener(GYGridEvent.COLGRID_DATACHANGE, s.colChange,s);
			s._dataGrid.addEventListener(GYGridEvent.ROWGRID_DATACHANGE, s.rowChange,s);
			s._dataGrid.addEventListener(GYViewEvent.SELECTED, s.selectedItem,s);
			s._dataGrid.vScroller.addEventListener(GYScrollerEvent.SCROLL_CHANGE, s.vScrollerChange,s);
			s._dataGrid.hScroller.addEventListener(GYScrollerEvent.SCROLL_CHANGE, s.hScrollerChange,s);
			s._dataGrid.addEventListener(GYEvent.VALUE_COMMIT, s.valueCommit,s);
			s._dataGrid.touchEnabled = true;
			s.addElement(s._dataGrid);
			var gridScaleBtn:GYSprite = s.getExcelDrag();
			s._dataGrid.addElement(gridScaleBtn);
			gridScaleBtn.touchEnabled = true;
			gridScaleBtn.right = 0;
			gridScaleBtn.bottom = 0;
			DraggerHandle.getInstance(gridScaleBtn).addBind(s.gyGridDataSizeChange,s);
			
			s._selFrame = new ExcelFrame(this);
			s._selFrame.touchEnabled = true;
			s._selFrame.owner = s._dataGrid;
			s._frameRect = new egret.Rectangle(0,0,s._dataGrid.vScroller.parent?s._dataGrid.width - s._dataGrid.vScroller.width:s._dataGrid.width,s._dataGrid.hScroller.parent?s._dataGrid.height - s._dataGrid.hScroller.height:s._dataGrid.height);
			s._selFrame.scrollRect = s._frameRect;
			s._selFrame.drager.addBind(s.dragFrame,s);
			
			s._rowsDataList = new GYDataListV(1,s.createRowGrid,s,s.setRowGrid,s);
			s._rowsDataList.canDrag = false;
			s._rowsDataList.clipAndEnableScrolling=true;
			s._rowsDataList.boxH=s._dataGrid.boxH;
			s._rowsDataList.scrollerPolicy = 2;
			s._rowsDataList.gridDataVec = s._dataGrid.rowsData;
			s._rowsDataList.touchEnabled = true;
			s.addElement(s._rowsDataList);
			
			s._colsDataList = new GYDataListH(1,s.createColGrid,s,s.setColGrid,s);
			s._colsDataList.canDrag = false;
			s._colsDataList.clipAndEnableScrolling=true;
			s._colsDataList.boxW=s._dataGrid.boxW;
			s._colsDataList.scrollerPolicy = 2;
			s._colsDataList.gridDataVec = s._dataGrid.colsData;
			s._colsDataList.touchEnabled = true;
			s.addElement(s._colsDataList);
			s._dataGrid.x = s._rowsDataList.width;
			s._dataGrid.y = s._colsDataList.height;
			s._selFrame.x = s._colsDataList.x = s._rowsDataList.width;
			s._selFrame.y = s._rowsDataList.y = s._colsDataList.height;
			
			GYKeyboard.getInstance().addKeyListener(this);
		}
		protected getExcelDrag():GYSprite
		{var s = this;
			var gridScaleBtn:GYSprite;
			var g:egret.Graphics;
			gridScaleBtn = new GYSprite;
			gridScaleBtn.height = gridScaleBtn.width = 16;
			g = gridScaleBtn.graphics;
			g.beginFill(0xff0000,1);
			g.drawRect(0,0,gridScaleBtn.width,gridScaleBtn.height);
			g.beginFill(0x666666,1);
			g.drawRect(2,2,12,12);			
			g.endFill();
			return gridScaleBtn;
		}
		public keyFocus():boolean{
			return false;
		}
		public kDown(keyCode:number):void
		{
			var s = this;
			if(keyCode == 46)
			{
				if(s._dataGrid.selectList)
				{
					var len:number=0;
					len = s._dataGrid.selectList.length;
					while(--len > -1)
					{
						s._dataGrid.selectList[len].value = "";
					}
					s._dataGrid.invalidData();
				}
			}
		}
		public kUp(keyCode:number):void
		{

		}
		protected selectedItem(e:GYViewEvent):void
		{var s = this;
			if(s._dataGrid.selectedData == null || s._dataGrid.nextData == null)return;
			s.selStartData(s._dataGrid.selectedData);
			s.selEndData(s._dataGrid.nextData);
		}
		protected colChange(e:GYGridEvent):void
		{var s = this;
			s._colsDataList.invalidGrids();
		}
		
		protected rowChange(e:GYGridEvent):void
		{var s = this;
			s._rowsDataList.invalidGrids();
		}
		
		protected dragFrame(d:DraggerHandle):void
		{var s = this;
			var mX:number = s._dataGrid.mouseX, mY:number = s._dataGrid.mouseY;
			var dis:number=0;
			dis = s._dataGrid.innerWidth - mX;
			if(dis < s.movePadding && dis > 0)
				s._dataGrid.hScroller.position += s.movePadding;
			else
			{
				if(mX < s.movePadding && mX > 0)
					s._dataGrid.hScroller.position -= s.movePadding;
			}
			dis = s._dataGrid.innerHeight - mY;
			if(dis < s.movePadding)
				s._dataGrid.vScroller.position += s.movePadding;
			else
			{
				if(mY < s.movePadding && mY > 0)
					s._dataGrid.vScroller.position -= s.movePadding;
			}
		}
		
		protected valueCommit(e:GYEvent):void
		{var s = this;
			s.getRowListData(s._dataGrid.dataProvider.length);
			
			if(s._dataGrid.dataProvider[0])
			{
				s.getColListData(s._dataGrid.dataProvider[0].length);
			}
			else
				s.getColListData(0);
		}
		
		protected vScrollerChange(e:GYScrollerEvent):void
		{var s = this;
			s._rowsDataList.vScroller.maximum = s._dataGrid.vScroller.maximum;
			s._rowsDataList.vScroller.position = s._dataGrid.vScroller.position;
			s._selFrame.invalidFrame();
		}
		
		protected hScrollerChange(e:GYScrollerEvent):void
		{var s = this;
			s._colsDataList.hScroller.maximum = s._dataGrid.hScroller.maximum;
			s._colsDataList.hScroller.position = s._dataGrid.hScroller.position;
			s._selFrame.invalidFrame();
		}
		
		protected dataGridViewChange(e:GYViewEvent):void
		{var s = this;
			if(s._dataGrid.innerWidth != s._colsDataList.width)
			{
				s._colsDataList.width = s._dataGrid.innerWidth;
			}
			if(s._dataGrid.innerHeight != s._rowsDataList.height)
			{
				s._rowsDataList.height = s._dataGrid.innerHeight;
			}
			s._frameRect.width = s._dataGrid.innerWidth;
			s._frameRect.height = s._dataGrid.innerHeight;
			s._selFrame.scrollRect = s._frameRect;
			s._selFrame.invalidFrame();
		}
		protected gyGridDataSizeChange(drager:DraggerHandle):void
		{var s = this;
			var w:number=s._dataGrid.mouseX+drager.handle.width - drager.dragMouseX;
			var h:number=s._dataGrid.mouseY+drager.handle.height - drager.dragMouseY;
			if(s._dataGrid.width == w && s._dataGrid.height == h)
				return;
			w -= s._dataGrid.vScroller.width;
			h -= s._dataGrid.hScroller.height;
			if(w < drager.handle.width)w = drager.handle.width;
			s._dataGrid.width = w;
			if(h < drager.handle.height)h = drager.handle.height;
			s._dataGrid.height = h;
		}
		/**重写此方法，自定义表格的格子*/
		protected createExcelGrid():IItemRender
		{var s = this;
			var grid:ExcelGrid;
			grid=new ExcelGrid;
			grid.excel = s;
			grid.selFrame = s._selFrame;
			return grid;
		}
		/**重写此方法，自定义设置表格的格子
		 * @param grid 格子 obj 数据
		 * @param obj 数据ExcelData类型
		 * */
		protected setExcelGrid(grid:IItemRender,obj:any):void
		{var s = this;
			grid.setData(obj);
		}
		/**重写此方法，自定义创建行标的格子
		 * @return ExcelRowGrid
		 * */
		protected createRowGrid():IItemRender
		{var s = this;
			var grid:ExcelRowGrid;
			grid = new ExcelRowGrid;
			grid.excel = s;
			return grid;
		}
		/**重写此方法，自定义设置行标的格子
		 * @param grid 格子 obj 数据
		 * */
		protected setRowGrid(grid:IItemRender,obj:any):void
		{var s = this;
			grid.setData(obj);
		}
		/**重写此方法，自定义创建列标的格子
		 * @return ExcelColGrid
		 * */
		protected createColGrid():IItemRender
		{var s = this;
			var grid:ExcelColGrid;
			grid = new ExcelColGrid;
			grid.excel = s;
			return grid;
		}
		/**重写此方法，自定义设置列标的格子
		 * @param grid 格子 obj 数据
		 * */
		protected setColGrid(grid:IItemRender,obj:any):void
		{var s = this;
			grid.setData(obj);
		}
		/**重写此方法，自定义计算规则
		 * @param nowData 数据ExcelData
		 * @param nextData 数据ExcelData
		 * @add 递增的值（如果格子数据是数字）
		 * */
		protected calculateRule(nowData:ExcelData, nextData:ExcelData, add:number = 1):void
		{var s = this;
//			var str:string="姐姐赏脸一起吃芋圆不？";
//			var ind:number=0=-1;
//			if(nowData.value is String)
//			ind = str.indexOf(nowData.value as String);
//			if(ind > -1)
//			{
//				nextData.value = str.substr(ind + nowData.value.length,1);
//				return;
//			}
			if(nowData.dataType  == ExcelData.TYPE_NUMBER)
			{
				nextData.value = nowData.value + add;
			}
			else if(nowData.dataType  == ExcelData.TYPE_NONE)
			{
				if(typeof(nowData.value) == "number")
					nextData.value = nowData.value + add;
				else
					nextData.value = nowData.value;
			}
		}
		/**设置行标的宽度*/
		public set rowsListWidth(val:number)
		{var s = this;
			s._rowsDataList.boxW = s._rowsDataList.width = val;
			s._colsDataList.x = s._selFrame.x = s._dataGrid.x = s._rowsDataList.width;			
		}
		/**设置列标的宽度*/
		public set colsListHeight(val:number)
		{var s = this;
			s._colsDataList.boxH = s._colsDataList.height = val;
			s._selFrame.y = s._dataGrid.y = s._rowsDataList.y = s._colsDataList.height;
		}
		/**设置数据*/
		public set dataProvider(val:Array<any>)
		{var s = this;
			s._dataGrid.dataProvider = val;
			s._dataGrid.dispatchEvent(new GYViewEvent(GYEvent.VALUE_COMMIT));
		}
		/**设置某列宽度
		 * @param ind 列索引 val 宽度值
		 * */
		public setGridWidth(ind:number=0,val:number=0):void
		{var s = this;
			s._dataGrid.setGridWidth(ind,val);
			s._selFrame.invalidFrame();
		}
		/**设置某行高度
		 * @param ind 行索引 val 高度值
		 * */
		public setGridHeight(ind:number=0,val:number=0):void
		{var s = this;
			s._dataGrid.setGridHeight(ind,val);
			s._selFrame.invalidFrame();
		}
		/**获取行数据
		 * @param ind 行索引
		 * @return 格子信息
		 * */
		public getRowGridData(ind:number=0):GridData
		{var s = this;
			return s._dataGrid.rowsData[ind];
		}
		/**获取列数据
		 * @param ind 列索引
		 * @return 格子信息
		 * */
		public getColGridData(ind:number=0):GridData
		{var s = this;
			return s._dataGrid.colsData[ind];
		}
		/**获取行标的行数据
		 * @param 索引
		 * */
		public getRowListData(len:number=0):void
		{var s = this;
			var i:number=0;
			var arr:Array<any>;
			if(s._rowsDataList.dataProvider == null)
				s._rowsDataList.dataProvider = [];
			arr = s._rowsDataList.dataProvider;
			if(arr.length == len)
				return;
			for(i=arr.length;i<len;++i)
				arr[i] = i + 1;
			s._rowsDataList.invalidData();
		}
		/**获取列标的列数据
		 * @param 索引
		 * */
		public getColListData(len:number=0):void
		{var s = this;
			var i:number=0;
			var arr:Array<any>;
			if(s._colsDataList.dataProvider == null)
				s._colsDataList.dataProvider = [];
			arr = s._colsDataList.dataProvider;
			if(arr.length == len)
				return;
			for(i=arr.length;i<len;++i)
				arr[i] = i + 1;
			s._colsDataList.invalidData();
		}
		
		
		/**选择起始格子 */
		public selStartGrid(grid:IItemRender):void
		{var s = this;
			var d:any = grid.getData();
			s.selStartData(d);
		}
		/**选择结束数据 */
		public selStartData(d:any):void
		{var s = this;
			s._selFrame.calEndCol = s._selFrame.startCol = s._selFrame.endCol = d.col;
			s._selFrame.calEndRow = s._selFrame.startRow = s._selFrame.endRow = d.row;
			s.addElement(s._selFrame);
			s._selFrame.invalidFrame();
		}
		/**选择结束格子*/
		public selEndGrid(grid:IItemRender):void
		{var s = this;
			if(s._selFrame.parent == null)
				return;
			var d:any = grid.getData();
			s.setEndData(d);
		}
		/**选择结束数据*/
		public selEndData(d:any):void
		{var s = this;
			s._selFrame.calEndCol = s._selFrame.endCol = d.col;
			s._selFrame.calEndRow = s._selFrame.endRow = d.row;
			s._selFrame.invalidFrame();
		}
		/**选择计算的结束格子*/
		public selCalEndGrid(grid:IItemRender):void
		{var s = this;
			if(s._selFrame.parent == null)
				return;
			var d:any = grid.getData();
			s._selFrame.calEndCol = d.col;
			s._selFrame.calEndRow = d.row;
		}
		/**选择计算的结束数据*/
		public setEndData(d:any):void
		{var s = this;
			s._selFrame.calEndCol = d.col;
			s._selFrame.calEndRow = d.row;
		}
		/**清除选择*/
		public clearSel(grid:IItemRender):void
		{var s = this;
			s._selFrame.startCol = s._selFrame.endCol = 0;
			s._selFrame.startRow = s._selFrame.endRow = 0;
			if(s._selFrame.parent)
				s.removeElement(s._selFrame);
		}
		/**行计算 从第一行开始，根据运算规则s.calculateRule方法往下一行运算
		 * @param fromRow 起始行
		 * @param toRow 结束行
		 * */
		public calculateRow(fromRow:number=0,toRow:number=0,fromCol:number=0,toCol:number=0):void
		{var s = this;
			var arr:Array<any> = s._dataGrid.dataProvider;
			var nowData:ExcelData;
			var i:number=0,j:number=0;
			var add:number=0;
			add = toRow - fromRow;
			if(add == 0)
				return;
			if(add > 0)
				add = 1;
			else
				add = -1;
			toRow *= add;
			for(j=fromCol;j<=toCol;++j)
			{
				for(i=fromRow+add;i*add<=toRow;i+=add)
				{
					nowData = arr[i-add][j];
					s.calculateRule(nowData,arr[i][j],add);
				}
			}
			s._dataGrid.invalidData();
		}
		/**列计算 从第一列开始，根据运算规则s.calculateRule方法往下一列运算
		 * @param fromCol 起始列
		 * @param toCol 结束列
		 * */
		public calculateCol(fromRow:number=0,toRow:number=0,fromCol:number=0,toCol:number=0):void
		{var s = this;
			var arr:Array<any> = s._dataGrid.dataProvider;
			var nowData:ExcelData;
			var i:number=0,j:number=0;
			var add:number=0;
			add = toCol - fromCol;
			if(add == 0)
				return;
			if(add > 0)
				add = 1;
			else
				add = -1;
			toCol *= add;
			for(i=fromRow;i<=toRow;++i)
			{
				for(j=fromCol+add;j*add<=toCol;j+=add)	
				{
					nowData = arr[i][j-add];
					s.calculateRule(nowData,arr[i][j],add);
				}
			}
			s._dataGrid.invalidData();
		}
		
		/**行列移动 从startRow,endRow,startCol,endCol选中的数据，移动到位置起始格toRow toCol */
		public moveData(startRow:number=0,endRow:number=0,startCol:number=0,endCol:number=0,toRow:number=0,toCol:number=0):void
		{var s = this;
			var temp:number=0;
			var dirX:number=0,dirY:number=0;
			var i:number=0,j:number=0,len:number=0,len2:number=0;
			var sRow:number=0,sCol:number=0,eRow:number=0,eCol:number=0;
			var addRow:number=0,addCol:number=0;
			var arr:Array<any>;
			var data1:ExcelData,data2:ExcelData;
			if(endRow < startRow)
			{
				temp = startRow;
				startRow = endRow;
				endRow = temp;
			}
			if(endCol < startCol)
			{
				temp = startCol;
				startCol = endCol;
				endCol = temp;
			}
			if(endCol > s._dataGrid.colsData.length)return;
			if(endRow > s._dataGrid.rowsData.length)return;
			addRow = toRow - startRow;
			addCol = toCol - startCol;
			if(endCol + addCol >= s._dataGrid.colsData.length)return;
			if(endRow + addRow >= s._dataGrid.rowsData.length)return;
			
			if(toRow == startRow && toCol == startCol)
				return;
			if(toRow > startRow)
			{
				dirY = -1;
				sRow = endRow;
				eRow = startRow;
				len = eRow*dirY;
			}
			else
			{
				dirY = 1;
				sRow = startRow;
				eRow = endRow;
				len = eRow;
			}
			if(toCol > startCol)
			{
				dirX = -1;
				sCol = endCol;
				eCol = startCol;
				len2 = eCol*dirX;				
			}				
			else
			{
				dirX = 1;
				sCol = startCol;
				eCol = endCol;
				len2 = eCol;
			}
			arr = s._dataGrid.dataProvider;
			for(i=sRow;i*dirY<=len;i+=dirY)
			{
				for(j=sCol;j*dirX<=len2;j+=dirX)
				{
					data1 = arr[i][j];
					data2 = arr[i+addRow][j+addCol];
					data2.value = data1.value;
					data1.value = null;
				}
			}
			s._dataGrid.invalidData();
		}			
		/**获取当前滚动的s.x坐标位置*/
		public get posX():number
		{var s = this;
			return s._colsDataList.hScroller.position;
		}
		/**获取当前滚动的s.y坐标位置*/
		public get posY():number
		{var s = this;
			return s._rowsDataList.vScroller.position;
		}
		/**设置excel区域宽度*/
		public setDataGridWidth(val:number):void
		{var s = this;			
			s._dataGrid.width = val;
		}
		/**设置excel区域高度*/
		public setDataGridHeight(val:number):void
		{var s = this;	
			s._dataGrid.height = val;
		}
		/**设置滚动条高度 */
		public setScrollerHeight(val:number):void
		{var s = this;			
			s._dataGrid.vScroller.height = val;
		}
		/**设置滚动条宽度 */
		public setScrollerWidth(val:number):void
		{var s = this;	
			s._dataGrid.hScroller.width = val;
		}
		/**边缘移动的感应距离*/
		public get movePadding():number
		{var s = this;
			return s._movePadding;
		}

		public set movePadding(value:number)
		{var s = this;
			s._movePadding = value;
		}

	}
}