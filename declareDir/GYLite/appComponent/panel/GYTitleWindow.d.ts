declare module GYLite {
    class GYTitleWindow extends GYSkinContainer {
        private _canDrag;
        private _canResize;
        /**窗口响应拖拽的y方向范围*/ dragRange: number;
        /**窗口是否可拉伸*/ dragResizeRange: number;
        /**窗口响应拉伸的x方向范围*/ minDragWidth: number;
        /**窗口响应拉伸的y方向范围*/ minDragHeight: number;
        private _tempX;
        private _tempY;
        constructor(s?: any);
        protected initComponent(): void;
        private dragDown;
        private dragUp;
        private dragLoop;
        private dragResizeDown;
        private dragResizeUp;
        private dragResizeLoop;
        /**自定义皮肤请实现IWindowSkin接口 */
        protected getThemeSkin(): IGYSkin;
        protected oper(e: egret.TouchEvent): void;
        show(pr: IGYContainer): void;
        hide(): void;
        get closeBtn(): GYButton;
        /**窗体是否可拖动*/
        get canDrag(): boolean;
        set canDrag(value: boolean);
        /**窗体是否可拉伸*/
        get canResize(): boolean;
        set canResize(value: boolean);
        setBackgound(b: egret.BitmapData): void;
        get backgoundImage(): GYScaleSprite;
        setIcon(val: egret.Texture): void;
        setTitle(val: string): void;
        get icon(): GYImage;
        get title(): GYText;
    }
}
