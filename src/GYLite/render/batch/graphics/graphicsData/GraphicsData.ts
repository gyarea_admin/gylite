module GYLite{
    export class GraphicsData{
        public smoothing:boolean;
        public color:number[];
        public alpha:number[];
        public ratio:number[];
        public repeat:boolean;
        public matrix:egret.Matrix;     
        public fillColor:number[];
        public fillAlpha:number[];
        public fillRatio:number[];
        public fillMatrix:egret.Matrix;
        public fillRepeat:boolean;
        public width:number;
        public height:number;
        public x:number;
        public y:number;
        public type:GraphicsType;
        public density:number;
        public fillTexture:egret.Texture;
        public texture:egret.Texture;        
        public vertices:number[];
        public isDraw:boolean;
        public cap:CanvasLineCap;
        public joint:CanvasLineJoin;
        public thickness:number;
        public miterLimit:number;
        public lineGradientType:string;
        public fillGradientType:string;
        constructor(x:number,y:number,type:GraphicsType)
        {
            let s = this;
            s.isDraw = false;
            s.x = x;
            s.y = y;
            s.type = type;
            s.matrix = new egret.Matrix;
            s.fillMatrix = new egret.Matrix;
            s.miterLimit = 3;
        }
        public inputGraphics(g:GYGraphics):void
        {
            let s= this;
            s.fillAlpha = g.fillAlpha.concat();
            s.fillColor = g.fillColor.concat();
            s.fillRatio = g.fillRatio.concat();
            s.fillMatrix.copyFrom(g.fillMatrix);
            s.fillTexture = g.fillTexture;
            s.smoothing = g.smoothing;

            s.alpha = g.alpha.concat();
            s.color = g.color.concat();
            s.ratio = g.ratio.concat();   
            if(g.lineMatrix) 
                s.matrix.copyFrom(g.lineMatrix);
            s.texture = g.texture;

            s.cap = g.cap;
            s.joint = g.joint;
            s.thickness = g.thickness;
            s.miterLimit = g.miterLimit;
            s.lineGradientType = g.lineGradientType;
            s.fillGradientType = g.fillGradientType;
            s.fillRepeat = g.fillRepeat;
        }
    }
    export class LineData extends GraphicsData{                        
        constructor(x:number,y:number,type:GraphicsType){
            super(x,y,type);
        }
        public inputGraphics(g:GYGraphics):void
        {
            let s= this;
            s.cap = g.cap;
            s.joint = g.joint;
            s.thickness = g.thickness;
            s.miterLimit = g.miterLimit;

            s.alpha = g.alpha.concat();
            s.color = g.color.concat();
            s.ratio = g.ratio.concat();
            if(g.lineMatrix)    
                s.matrix.copyFrom(g.lineMatrix);
            s.texture = g.texture;

            s.cap = g.cap;
            s.joint = g.joint;
            s.thickness = g.thickness;
            s.miterLimit = g.miterLimit;
            s.lineGradientType = g.lineGradientType;
        }
    }
    export class CircleData extends GraphicsData{
        public radiusX:number;
        public radiusY:number;
        public smoothing:boolean;
        constructor(x:number,y:number,type:GraphicsType){
            super(x,y,type);
        }
    }
    export class PolyData extends GraphicsData{        
        constructor(x:number,y:number,type:GraphicsType){
            super(x,y,type);            
        }
    }
    export class TriangleData extends GraphicsData{        
        public indices:number[];
        public uvs:number[];
        constructor(x:number,y:number,type:GraphicsType){
            super(x,y,type);            
        }
    }
    export class ArcData extends GraphicsData{        
        public radius:number;
        public anticlockwise:boolean;
        public startAngle:number;
        public endAngle:number;
        public thickness:number;
        constructor(x:number,y:number,type:GraphicsType){
            super(x,y,type);            
        }
    }
}