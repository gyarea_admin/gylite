declare namespace egret.web {
    class EgretShaderLib {
        static readonly blur_frag: string;
        static readonly colorTransform_frag: string;
        static default_vert: string;
        static readonly glow_frag: string;
        static readonly primitive_frag: string;
        static texture_frag: string;
        static readonly texture_etc_alphamask_frag: string;
        static readonly colorTransform_frag_etc_alphamask_frag: string;
    }
}
