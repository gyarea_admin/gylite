module GYLite{
    export class TextRenderSprite extends GYText implements IRenderTestElement{  
        private _tick:number;      
        public changeTime:number=0;
        private _owner:TextRenderGroup;
        constructor(o:TextRenderGroup,color:number,size:number)
        {
            super();
            let s =this;          
            s._owner = o;              
            s.color = color;
            s.size = size;
            s._tick = GYLite.CommonUtil.loopTime;
            s.mouseThrough= true;
            s.useInvalidDisplay = false;
            s.useInvalidText = false;
            s.offLayout = true;
            s.stroke = 1;
            s.text = String.fromCharCode(21834 + (Math.random()*8728|0));
            // s.strokeColor = 0xffffff;
        }
        public get graphics():GYGraphics
        {
            return null;
        }        
        private _v:number=0;
        private _a:number=1;
        public initX:number;
        public initY:number;
        public setPos(x:number,y:number):void
        {
            let s= this;
            s.x = x;
            s.y = y;
            s.initX = x;
            s.initY = y;
        }
        public loop(t:number):void
        {
            let s= this;            
            // s.x = s.initX;
            // s.y = s.initY;           
            
            if(t - s._tick > 16)
            {                
                // s.underline = t%10==0;
                // s.bold = t%2 == 0;
                // s.stroke = t%2==0?0:1;
                
                if(t > s.changeTime)
                {
                    s.changeTime = t;
                    s._owner.swap(s,t);
                }
            //     // s.replaceTextAt(0,String.fromCharCode(21834 + (Math.random()*8728|0)),false,s.$textFormat);
            //     s._v += s._a;
            //     s.x+=s._v;  
            //     if(s._v == 5)
            //         s._a = -1;
            //     else if(s._v == -5)
            //         s._a = 1;                
                s._tick = t;
            }
        }        
    }
}