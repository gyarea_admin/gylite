declare namespace egret {
    /**
     * OrientationMode 类为舞台初始旋转模式提供值。
     */
    const OrientationMode: {
        /**
         * 适配屏幕
         */
        AUTO: string;
        /**
         * 默认竖屏
         */
        PORTRAIT: string;
        /**
         * 默认横屏，舞台顺时针旋转90度
         */
        LANDSCAPE: string;
        /**
         * 默认横屏，舞台逆时针旋转90度
         */
        LANDSCAPE_FLIPPED: string;
    };
}
