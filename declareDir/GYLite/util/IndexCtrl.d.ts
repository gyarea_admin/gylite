declare module GYLite {
    class DB_STORE {
        static readonly BATTLE: string;
    }
    enum DB_OPER {
        GET = 0,
        ADD = 1,
        PUT = 2,
        DELETE = 3,
        CLEAR = 4
    }
    class IndexDBCtrl {
        constructor();
        /**打开数据库操作
         * @param  dbName 数据库名称
         * @param storeName 仓库名称（表名）
         * @param type 操作类型 参考DB_OPER常量
         * @param callBack 回调
         * @param data type为DB_OPER.ADD或DB_OPER.PUT的时候，提供的数据，{id:0}，数据必须带id作为唯一标识
         * @param key 删除记录时提供
        */
        static openDBToOper(dbName: string, storeName: string, type: number, callBack?: (code: number, data?: any) => void, data?: any[], key?: IDBKeyRange | IDBValidKey, rangeKey?: string): void;
        /**添加记录（同名数据会报错）**/
        protected static add(store: IDBObjectStore, data: any[], func?: (errcode: number, data?: any[]) => void, key?: IDBValidKey): void;
        /**添加记录（同名数据会覆盖）**/
        protected static put(store: IDBObjectStore, data: any[], func?: (errcode: number, data?: any[]) => void, key?: IDBValidKey): void;
        /**删除记录
         * @param store 仓库
         * @param key 删除的行字段名，可以用IDBKeyRange指定范围，指定范围时,key默认为id
        */
        protected static delete(store: IDBObjectStore, key: IDBValidKey | IDBKeyRange, func?: (errcode: number, data?: any[]) => void, rangeKey?: string): void;
        /**获取仓库数据*/
        protected static getData(store: IDBObjectStore, func: (errcode: number, data?: any[]) => void, queryKey?: IDBKeyRange | IDBValidKey, count?: number): void;
        /**清理仓库*/
        protected static clearStore(store: IDBObjectStore, func: (errorCode: number) => void): void;
    }
}
