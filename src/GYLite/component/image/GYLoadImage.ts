module GYLite
{
					
			
	export class GYLoadImage extends GYImage
	{		
		/**默认加载失败图片*/public static defaultErrorData:egret.Texture;		
		protected _url:string;
		protected _resObj:ResObject;
		protected _isLoading:boolean;
		public errorData:egret.Texture;
		/**是否切换时立即清理*/public clearImmedia:boolean;
		
		public constructor()
		{
			super();
		}
		protected baseSource(bmp:any):void
		{
			let s = this;
			egret.superSetter(GYLoadImage,s,"source", bmp);						
		}
		public get source():any
		{
			let s = this;
			return egret.superGetter(GYLoadImage, s, "source");
		}
		public set source(bmp:any)
		{
			let s = this;
			if(bmp == "")bmp = null;
			if(CommonUtil.GYIs(bmp,String))
			{
				if(s._url == bmp)
					return;
				if(s.clearImmedia)
				{
					s.release();
					s.baseSource(null);
				}
				else
				{
					if(s._isLoading)
					{
						s._isLoading = false;
						GYImage._imgLoader.cancelLoadPath(s._url, s.setBitmapdata,s);
					}
				}
				s._url=bmp;
				s._isLoading = true;
				GYImage._imgLoader.loadPath(bmp,s.setBitmapdata,s,LoadType.TYPE_TEX);
			}
			else if(CommonUtil.GYIs(bmp, egret.Texture))
			{
				s.release();
				s.baseSource(bmp);
				s._url = null;
			}
			else if(bmp == null)
			{
				s.release();
				s.baseSource(null);
				s._url = null;
			}
		}
		private release():void
		{
			let s = this;
			if(s._isLoading)
			{
				s._isLoading = false;
				GYImage._imgLoader.cancelLoadPath(s._url, s.setBitmapdata,GYImage._imgLoader);
			}
			if(s._resObj)
			{
				s._resObj.relRes(s);
				s._resObj = null;
			}
		}
		public get url():string
		{
			let s = this;
			return s._url;
		}
		protected setBitmapdata(loadInfo:LoadInfo):void
		{
			let s = this;
			if(s._url==loadInfo.path)
			{
				s._isLoading = false;
				if(loadInfo.error != 0)
				{
					s.release();
					s.baseSource(s.errorData?s.errorData:GYLoadImage.defaultErrorData);
					console.log("加载失败！#" + loadInfo.path);
					return;
				}
				s.release();
				s._resObj = loadInfo.content;
				if(s._resObj == null)
					s.baseSource(null);
				else
					s.baseSource(s._resObj.refRes(s));
				if(s.hasEventListener(GYEvent.LOAD_COMPLETE))
					s.dispatchEvent(new GYEvent(GYEvent.LOAD_COMPLETE));
				s._isLoading = false;
			}
		}
	}
}