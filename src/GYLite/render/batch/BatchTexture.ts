/**
@author 迷途小羔羊
2022.09.27
*/
module GYLite
{
    export class BatchTexture extends egret.Texture implements IResource,IPoolObject{
        public $disposed:boolean;
        public hash:number;        
        public constructor(){
            super();            
        }
        /**重置参数
         * @param hsCode hash值
         * @param batchElement 合批数据源测量信息
        */
        public reset(hsCode:number,batchElement:BatchElement):void
        {
            let s = this;                
            let bmp:egret.BitmapData;
            s.hash = hsCode;        
            bmp = s.bitmapData;
            if(bmp)
            {
                if (egret.nativeRender) {
                    let nativeBitmapData = new egret_native.NativeBitmapData();
                    nativeBitmapData.$init();
                    bmp.$nativeBitmapData = nativeBitmapData;
                }
                else
                {
                    bmp.width = batchElement.width;
                    bmp.height = batchElement.height;
                    s.bitmapData.source = batchElement;                
                }                
            }
            else
                bmp = new egret.BitmapData(batchElement);
            s._setBitmapData(bmp);
        }
        public get element():BatchElement
        {
            return this.bitmapData.source;
        }                
        public get disposed(): boolean
        {
            return this.$disposed;
        }
        public dispose(): void {
            let s= this;
            if(s.$disposed)
                return;
            s.$disposed = true;
            s.$batchManager.dispose();
            s.beforeToPool();
        }
        protected beforeToPool():void
        {
            let s= this;
            s.hash=0;  
            let batchElement:BatchElement;
            if(s.bitmapData)
            {
                batchElement = s.bitmapData.source;
                if(batchElement)
                {
                    batchElement.clear();
                    s.bitmapData.source = null;
                }
                
            }                
        }
        public clear():void
        {
            let s = this;
            if(s.inPool)
                return;
            s.beforeToPool();            
            GYLite.PoolUtil.toPool(s, s.constructor);
        }
		public inPool:boolean;
		public outPoolInit():void
        {
            
        }
    }
}