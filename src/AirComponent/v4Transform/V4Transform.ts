module GYLite{
    export class V4Transform{
        public $simpleRate:number;
        public v0p:TransformPoint;
        public v1p:TransformPoint;
        public v2p:TransformPoint;
        public v3p:TransformPoint;
        constructor(simpleRate:number,x:number,y:number,w:number,h:number)
        {
            let s= this;
            s.$simpleRate = simpleRate;
            s.v0p = new TransformPoint(x,y);
            s.v1p = new TransformPoint(x + w,y);
            s.v2p = new TransformPoint(x + w,y + h);
            s.v3p = new TransformPoint(x,y + h);
        }
    }
    export class TransformPoint
    {
        public x:number;
        public y:number;
        constructor(x:number,y:number)
        {
            let s = this;
            s.x = x;
            s.y = y;
        }
    }
}