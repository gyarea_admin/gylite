declare module GYLite {
    class Matrix3x3 {
        static DEFAULT: Matrix3x3;
        private static _tempV30;
        private static _tempV31;
        private static _tempV32;
        elements: Float32Array;
        constructor(m11?: number, m12?: number, m13?: number, m21?: number, m22?: number, m23?: number, m31?: number, m32?: number, m33?: number);
        static createRotationQuaternion(rotation: Quaternion, out: Matrix3x3): void;
        static createFromTranslation(trans: Vector2, out: Matrix3x3): void;
        static createFromRotation(rad: number, out: Matrix3x3): void;
        static createFromScaling(scale: Vector3, out: Matrix3x3): void;
        static createFromMatrix4x4(sou: Matrix4x4, out: Matrix4x4): void;
        static multiply(left: Matrix3x3, right: Matrix3x3, out: Matrix3x3): void;
        determinant(): number;
        translate(trans: Vector2, out: Matrix3x3): void;
        rotate(rad: any, out: Matrix3x3): void;
        scale(scale: any, out: Matrix3x3): void;
        invert(out: Matrix3x3): void;
        transpose(out: Matrix3x3): void;
        identity(): void;
        cloneTo(destObject: Matrix3x3): Matrix3x3;
        clone(): Matrix3x3;
        static lookAt(eye: Vector3, target: Vector3, up: Vector3, out: Matrix3x3): void;
    }
}
