declare namespace egret.sys {
    /**
     * @private
     * 线条路径。
     * 注意：当线条宽度（lineWidth）为1或3像素时，需要特殊处理，往右下角偏移0.5像素，以显示清晰锐利的线条。
     */
    class StrokePath extends Path2D {
        constructor();
        /**
         * 线条宽度。
         * 注意：绘制时对1像素和3像素要特殊处理，整体向右下角偏移0.5像素，以显示清晰锐利的线条。
         */
        lineWidth: number;
        /**
         * 线条颜色
         */
        lineColor: number;
        /**
         * 线条透明度
         */
        lineAlpha: number;
        /**
         * 端点样式,"none":无端点,"round":圆头端点,"square":方头端点
         */
        caps: string;
        /**
         * 联接点样式,"bevel":斜角连接,"miter":尖角连接,"round":圆角连接
         */
        joints: string;
        /**
         * 用于表示剪切斜接的极限值的数字。
         */
        miterLimit: number;
        /**
         * 描述交替绘制线段和间距（坐标空间单位）长度的数字。
         */
        lineDash: number[];
    }
}
