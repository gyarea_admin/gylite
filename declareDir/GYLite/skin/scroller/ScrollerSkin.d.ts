declare module GYLite {
    class ScrollerSkin extends GYSkin implements IScrollerSkin {
        protected _arrowBtn1: GYButton;
        protected _arrowBtn2: GYButton;
        protected _scrollBar: GYButton;
        protected _sliderBg: GYScaleSprite;
        constructor(sliderSkin: GYButton | ButtonSkin, sliderBg: GYScaleSprite | egret.Texture, arrowSkin1?: GYButton | ButtonSkin, arrowSkin2?: GYButton | ButtonSkin, bgRect?: Scale9GridRect);
        release(): void;
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        /**第一个箭头按钮*/
        get arrowBtn1(): GYButton;
        /**第二个箭头按钮*/
        get arrowBtn2(): GYButton;
        /**滑块*/
        get scrollBar(): GYButton;
        /**滑动区域*/
        get sliderBg(): GYScaleSprite;
        clone(): IGYSkin;
    }
}
