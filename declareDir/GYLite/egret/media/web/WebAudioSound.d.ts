/**
 * @private
 */
interface AudioBufferSourceNodeEgret {
    buffer: any;
    context: any;
    onended: Function;
    stop(when?: number): void;
    noteOff(when?: number): void;
    addEventListener(type: string, listener: Function, useCapture?: boolean): any;
    removeEventListener(type: string, listener: Function, useCapture?: boolean): any;
    disconnect(): any;
}
declare namespace egret.web {
    /**
     * @private
     */
    class WebAudioDecode {
        /**
         * @private
         */
        static ctx: any;
        /**
         * @private
         */
        static decodeArr: any[];
        /**
         * @private
         */
        private static isDecoding;
        /**
         * @private
         *
         */
        static decodeAudios(): void;
        /** 解决 ios13 页面切到后台再拉起，声音无法播放 */
        static initAudioContext: Function;
    }
    /**
     * @private
     * @inheritDoc
     */
    class WebAudioSound extends egret.EventDispatcher implements egret.Sound {
        /**
         * Background music
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 背景音乐
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static MUSIC: string;
        /**
         * EFFECT
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 音效
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        static EFFECT: string;
        /**
         * @private
         */
        type: string;
        /**
         * @private
         */
        private url;
        /**
         * @private
         */
        private loaded;
        /**
         * @private
         * @inheritDoc
         */
        constructor();
        /**
         * @private
         */
        private audioBuffer;
        get length(): number;
        /**
         * @inheritDoc
         */
        load(url: string): void;
        /**
         * @inheritDoc
         */
        play(startTime?: number, loops?: number): SoundChannel;
        /**
         * @inheritDoc
         */
        close(): void;
    }
}
