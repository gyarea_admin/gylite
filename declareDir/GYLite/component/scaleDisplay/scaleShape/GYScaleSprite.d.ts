declare module GYLite {
    class GYScaleSprite extends GYUIComponent {
        protected _bitmap: GYBitmap;
        /**九切片 此类矢量绘制在Shape上面，不会产生新的BitmapData
         * @param texture 源位图数据
         * @param rect 九宫格
         * @param batch 是否自动合批
         * */
        constructor(texture?: egret.Texture, rect?: Scale9GridRect, batch?: boolean | null);
        set texture(val: egret.Texture);
        set bitmapData(bmp: egret.Texture);
        get texture(): egret.Texture;
        get bitmapData(): egret.Texture;
        get smoothing(): boolean;
        set smoothing(val: boolean);
        set repeatWidth(val: number);
        /**重复模式的砖块宽度*/
        get repeatWidth(): number;
        set repeatHeight(val: number);
        /**重复模式的砖块高度*/
        get repeatHeight(): number;
        set repeatX(val: number);
        /**重复模式的起始X*/
        get repeatX(): number;
        set repeatY(val: number);
        /**重复模式的起始Y*/
        get repeatY(): number;
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        set scale9GridRect(value: Scale9GridRect);
        /**绘制模式 @see ScaleMode
         * @param val 0 拉伸 1 裁切 2 重复 */
        set mode(val: number);
        get mode(): number;
        set clipX(val: number);
        set clipY(val: number);
        /**裁切的s.x坐标*/
        get clipX(): number;
        /**裁切的s.y坐标*/
        get clipY(): number;
        set drawX(val: number);
        set drawY(val: number);
        /**绘制点的s.x坐标*/
        get drawX(): number;
        /**绘制点的s.y坐标*/
        get drawY(): number;
        updateView(): void;
        /**刷新绘图*/
        invalidDraw(): void;
        get scale9GridRect(): Scale9GridRect;
        protected resetBound(): void;
        protected resetElementsBound(): void;
    }
}
