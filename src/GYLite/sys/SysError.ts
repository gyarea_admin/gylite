/**
@author 迷途小羔羊
2022.09.27
*/
module GYLite{
    export class SysError{
        public static NONE:SysError=new SysError(0);
        /**图集创建尺寸超出限制*/public static ATLAS_SIZE_LIMIT:SysError=new SysError(1);
        /**图集数量超出限制*/public static ATLAS_NUM_LIMIT:SysError=new SysError(2);
		/**不允许创建同名的图集*/public static ATLAS_DUPLICATE:SysError=new SysError(3);
		/**无数据源无法合批*/public static BATCH_SOURCE_LOST:SysError=new SysError(4);
		/**矢量绘制合批失败*/public static BATCH_GRAPHICS_FAIL:SysError=new SysError(5);
		/**贝塞尔计算溢出*/public static BEZIER_ERROR:SysError=new SysError(6);
		/**Separator Error 找不到最近端点*/public static SEPARATOR_CANNOT_FINDNEAREST:SysError=new SysError(7);
		/**Separator Error 非法端点*/public static SEPARATOR_INVALID_POINT:SysError=new SysError(7);
		/**Separator Error 端点数量不够*/public static SEPARATOR_NUM_NOT_ENOUGH:SysError=new SysError(8);
		/**Separator Error 路径是直线*/public static SEPARATOR_PATH_IS_LINE:SysError=new SysError(9);
		/**Separator Error 路径相交*/public static SEPARATOR_PATH_CROSS:SysError=new SysError(10);
		/**同名图集没有区域可插入*/public static ATLAS_INSERT_SIZE_LIMIT:SysError=new SysError(11);
		/**加载的非字节数据接*/public static BYTES_ERROR:SysError=new SysError(12);
		/**ResObject重复引用*/public static REF_REPEAT_ERROR:SysError=new SysError(13);
		/**ResObject释放未引用的资源*/public static REL_ERROR:SysError=new SysError(14);
		/**DBError*/public static DB_ERROR:SysError=new SysError(15);
		/**DBIndexError*/public static DB_INDEX_ERROR:SysError=new SysError(16);
        private _code:number;
		private _msg:string;
		public constructor(code:number)
		{
			this._code = code;
		}		
		public get code():number
		{
			return this._code;
		}
        public get msg():string
        {
            return this._msg;
        }
		public throwError(param:any[]=null):SysError
		{            
            var e:SysError = this;
			if(e == SysError.ATLAS_SIZE_LIMIT)			
				e._msg = "atlas size limit:" + param[0] + "," + param[1] + "," + param[2];
            else if(e == SysError.ATLAS_NUM_LIMIT)			
				e._msg = "atlas num limit:" + param[0] + "," + param[1];
			else if(e == SysError.ATLAS_DUPLICATE)
				e._msg = "atlas duplicate create:" + param[0];
			else if(e == SysError.BATCH_SOURCE_LOST)
				e._msg = "cannot batch without source:" + param[0];	
			else if(e == SysError.BATCH_GRAPHICS_FAIL)
				e._msg = "batch graphics fail!:" + param[0];	
			else if(e == SysError.BEZIER_ERROR)
				e._msg = "bezier error test over 500!:" + param[0];	
			else if(e == SysError.SEPARATOR_CANNOT_FINDNEAREST)
				e._msg = "Separator cannot find nearest Point!:Line-(" + param[0] + "," + param[1] + "),index:" + param[2] + ",(" + param[3] + "," + param[4] + "),index:" + param[5];	
			else if(e == SysError.SEPARATOR_INVALID_POINT)
				e._msg = "Separator Error invalid point!:Point-(" + param[0] + "," + param[1] + ")" +  ",index:" + param[2];	
			else if(e == SysError.SEPARATOR_NUM_NOT_ENOUGH)
				e._msg = "Separator Error point must more than three, current is " + param[0];	
			else if(e == SysError.SEPARATOR_PATH_IS_LINE)
				e._msg = "Separator Error line cannot be a line!A:(" + param[0] + "," + param[1] + "),B:(" + param[2] + "," + param[3] + "),C:(" + param[4] + "," + param[5] + "),";
			else if(e == SysError.SEPARATOR_PATH_CROSS)
				e._msg = "Separator Error line cross!lineA:("+param[1] + "," + param[2] + "):"+param[0]+"-("+param[4] + "," + param[5] + "):" + param[3] + ",lineB:("+param[7] + "," + param[8] + "):"+param[6]+"-("+param[10] + "," + param[11] + "):"+param[9];
			else if(e == SysError.ATLAS_INSERT_SIZE_LIMIT)
				e._msg = "no space to insert texture!atlasName:"+param[0] + ",texture:" + param[1] + "size:("+param[2]+","+param[3]+")";
			else if(e == SysError.BYTES_ERROR)
				e._msg = "loaded not bytes!";
			else if(e == SysError.REF_REPEAT_ERROR)
				e._msg = "resObject reference repeat:" + param[0];
			else if(e == SysError.REL_ERROR)
				e._msg = "resObject release but not reference:" + param[0];
			else if(e == SysError.DB_ERROR)
				e._msg = "DB error:" + param[0];
			else if(e == SysError.DB_INDEX_ERROR)
				e._msg = "DB index error, index not exsit:" + param[0];
            return e;
        }
    }
}