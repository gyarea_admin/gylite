module GYLite
{
	export class NumberUtil
	{
		public static LARGE_NUMBER:number = 10000000;
		public static TINY_NUMBER:number = 0.0000001;
		public static TINY_NUMBER_01:number = 0.00000001;
		/**四分之π*/public static QUTER_PI:number = Math.PI/4;
		/**二分之π*/public static HALF_PI:number = Math.PI/2;
		/**π*/public static PI:number = Math.PI;
		/**二倍π*/public static DOUBLE_PI:number = Math.PI*2;
		/**获取位为1的数量*/
		public static bitCount(val:number):number
		{
			var len:number = 32;
			var c:number = 0;
			while(--len > -1)
			{
				c += (val >> len) & 1;
			}
			return c;
		}

		/**保留多少位小数，小数值为0则不保留
		 * @param val 数值
		 * @param num n为小数则参数为10的n次方
		 **/
		public static fixed(val:number, num:number=100):number
		{
			return Math.round(val * num) / num;
		}
		/**求余，小数求余排除精度错误
		 * @param val 数值
		 * @param modVal 除数
		 * @return 返回结果
		 **/
		public static mod(val:number,modVal:number):number
		{
			var mod2:number;
			var mod:number;
			if((modVal >> 0) != modVal)
			{
				mod = mod2 = val % modVal;
				if(mod < 0)mod = -mod;
				if(mod > NumberUtil.TINY_NUMBER)
				{
					mod -= modVal;
					if(mod < 0)mod = -mod;
					if(mod > NumberUtil.TINY_NUMBER)
						val = NumberUtil.fixed(val - mod2, 100000000);
				}
			}
			else
			{
				val -= val % modVal;
			}
			return val;
		}
		/**判断是否相等(排除精度问题)*/
		public static isNumberEqual(valA:number,valB:number):boolean
		{
			var val:number = valA - valB;
			if(val < 0)val = -val;
			return val - NumberUtil.TINY_NUMBER <= NumberUtil.TINY_NUMBER;
		}
		/**排除小数后7位的小数*/
		public static accuracyInt(val:number):number
		{
			var n:number,m:number,k:number,l:number;			
			n = val % 1;//取出小数部分
			l = val - n;//整数部分
			k = (n * NumberUtil.LARGE_NUMBER>>0);
			k = k/NumberUtil.LARGE_NUMBER;//去掉多余的小数位
			n = n - k;//算出多余的小数部位
			if(NumberUtil.TINY_NUMBER - (n<0?-n:n)<NumberUtil.TINY_NUMBER_01)
				return l + k + (n<0?-NumberUtil.TINY_NUMBER:NumberUtil.TINY_NUMBER);
			return l + k;
		}
	}
}