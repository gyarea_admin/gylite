declare namespace egret.sys {
    /**
     * @private
     * 显示列表
     */
    class DisplayList extends HashObject {
        /**
         * 创建一个DisplayList对象，若内存不足或无法创建RenderBuffer，将会返回null。
         */
        static create(target: DisplayObject): DisplayList;
        /**
         * @private
         * 创建一个DisplayList对象
         */
        constructor(root: DisplayObject);
        private isStage;
        /**
         * 位图渲染节点
         */
        $renderNode: RenderNode;
        /**
         * @private
         * 获取渲染节点
         */
        $getRenderNode(): sys.RenderNode;
        /**
         * @private
         */
        renderBuffer: RenderBuffer;
        /**
         * @private
         */
        offsetX: number;
        /**
         * @private
         */
        offsetY: number;
        /**
         * @private
         */
        private offsetMatrix;
        /**
         * @private
         * 显示列表根节点
         */
        root: DisplayObject;
        /**
         * @private
         * 设置剪裁边界，不再绘制完整目标对象，画布尺寸由外部决定，超过边界的节点将跳过绘制。
         */
        setClipRect(width: number, height: number): void;
        $canvasScaleX: number;
        $canvasScaleY: number;
        /**
         * @private
         * 绘制根节点显示对象到目标画布，返回draw的次数。
         */
        drawToSurface(): number;
        private bitmapData;
        /**
         * @private
         * 改变画布的尺寸，由于画布尺寸修改会清空原始画布。所以这里将原始画布绘制到一个新画布上，再与原始画布交换。
         */
        changeSurfaceSize(): void;
        static $canvasScaleFactor: number;
        /**
         * @private
         */
        static $canvasScaleX: number;
        static $canvasScaleY: number;
        /**
         * @private
         */
        static $setCanvasScale(x: number, y: number): void;
        /**
         * @private
         * stage渲染
         */
        $stageRenderToSurface: () => void;
    }
}
