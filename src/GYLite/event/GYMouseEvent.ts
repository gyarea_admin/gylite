module GYLite
{	
	export class GYTouchEvent extends egret.TouchEvent
	{
		/**外部释放鼠标*/
		public static RELEASE_OUTSIDE:string="GY_release_outside";
		/**长按鼠标*/
		public static LONG_MOUSEDOWN:string="GY_LongDown";
		/**连续按下鼠标*/
		public static CONTINUE_MOUSEDOWN:string="GY_Continue_Down";
		/**双击*/
		public static DOUBLE_CLICK:string="GY_doubleClick";
		public outsideTarget:any;
		public result:number;//0成功 1失败
		public constructor(type:string, bubbles?:boolean, cancelable?:boolean, stageX?:number, stageY?:number, touchPtId?:number)
		{
			super(type, bubbles, cancelable, stageX, stageY, touchPtId);
		}
	}
}