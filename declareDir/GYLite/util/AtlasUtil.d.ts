/***2024-12-16 迷途小羔羊
* 用于编写图集工具类
*/
declare module GYLite {
    class AtlasUtil {
        private static _callBack;
        private static _thisObj;
        /**下载图集
         * @param entries 图集文件列表
         * @param transprentCutting 是否裁切透明区域，默认true
         * @param combineAtlas 是否动作合并图集，默认false，不合并(当打包动作图集，每个动作将单独输出图集，将使用到分开的功能)
         * @param gap 小图间隙，默认1
         * @param maxSize 最大接受打入图集的图片，默认512
         * @param callBack 打包结束回调 默认null
         * @param thisObj 回调指向 默认null
         * @param isEffect 是否特效序列 默认false
        */
        static downloadAtlas(entries: FileSystemFileEntry[], transprentCutting?: boolean, combineAtlas?: boolean, gap?: number, maxSize?: number, callBack?: (error: IAtlasError) => void, thisObj?: any, isEffect?: boolean): void;
        private static outputAtlas;
    }
    interface IAtlasError {
        /**错误的文件列表*/ failList: string[];
        /**错误信息*/ msg: string;
        /**0为正常，无错误，其他情况为错误码*/ code: number;
    }
}
