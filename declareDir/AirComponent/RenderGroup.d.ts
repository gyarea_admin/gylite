declare module GYLite {
    class RenderGroup extends GYGroup {
        private _renderSprites;
        private _paused;
        txt: GYLite.GYText;
        private _sp;
        constructor();
        private addToStage;
        private removeFromStage;
        private init;
        private tap;
        private run;
        private loop;
        private stop;
        private pause;
        private addRender;
        private addRenders;
        private removeRenders;
        private getTxt;
    }
}
