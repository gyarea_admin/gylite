/**
 @author 迷途小羔羊
 2015.6.5
 */
module GYLite
{
								
	/**复选框*/
	export class GYCheckBox extends GYButton
	{
		public constructor(skin:any=null)
		{
			super(skin);
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();
			this.toggle=true;
		}
		/**获取主题皮肤，自定义皮肤请实现ICheckBoxSkin接口*/
		protected getThemeSkin():IGYSkin
		{			
			return GYSprite.skinTheme.GetCheckBoxSkin();
		}
		/**文本与复选框的间隙*/
		public get gap():number
		{
			let s = this;
			return (s._skin as ICheckBoxSkin).gap;
		}
		public set gap(val:number)
		{
			let s = this;
			(s._skin as ICheckBoxSkin).gap = val;
		}
	}
}