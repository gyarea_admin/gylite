declare module GYLite {
    class GYTabItemRender extends GYButton implements IItemRender {
        protected _owner: IList;
        protected _data: TabData;
        protected _itemIndex: number;
        protected _col: number;
        protected _row: number;
        constructor(skin?: any);
        set selected(val: boolean);
        setData(obj: any): void;
        getData(): any;
        set owner(val: IList);
        get owner(): IList;
        get itemIndex(): number;
        set itemIndex(value: number);
        get col(): number;
        set col(value: number);
        get row(): number;
        set row(value: number);
        protected getThemeSkin(): IGYSkin;
    }
}
