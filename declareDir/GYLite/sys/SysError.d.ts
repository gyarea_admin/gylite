/**
@author 迷途小羔羊
2022.09.27
*/
declare module GYLite {
    class SysError {
        static NONE: SysError;
        /**图集创建尺寸超出限制*/ static ATLAS_SIZE_LIMIT: SysError;
        /**图集数量超出限制*/ static ATLAS_NUM_LIMIT: SysError;
        /**不允许创建同名的图集*/ static ATLAS_DUPLICATE: SysError;
        /**无数据源无法合批*/ static BATCH_SOURCE_LOST: SysError;
        /**矢量绘制合批失败*/ static BATCH_GRAPHICS_FAIL: SysError;
        /**贝塞尔计算溢出*/ static BEZIER_ERROR: SysError;
        /**Separator Error 找不到最近端点*/ static SEPARATOR_CANNOT_FINDNEAREST: SysError;
        /**Separator Error 非法端点*/ static SEPARATOR_INVALID_POINT: SysError;
        /**Separator Error 端点数量不够*/ static SEPARATOR_NUM_NOT_ENOUGH: SysError;
        /**Separator Error 路径是直线*/ static SEPARATOR_PATH_IS_LINE: SysError;
        /**Separator Error 路径相交*/ static SEPARATOR_PATH_CROSS: SysError;
        /**同名图集没有区域可插入*/ static ATLAS_INSERT_SIZE_LIMIT: SysError;
        /**加载的非字节数据接*/ static BYTES_ERROR: SysError;
        /**ResObject重复引用*/ static REF_REPEAT_ERROR: SysError;
        /**ResObject释放未引用的资源*/ static REL_ERROR: SysError;
        /**DBError*/ static DB_ERROR: SysError;
        /**DBIndexError*/ static DB_INDEX_ERROR: SysError;
        private _code;
        private _msg;
        constructor(code: number);
        get code(): number;
        get msg(): string;
        throwError(param?: any[]): SysError;
    }
}
