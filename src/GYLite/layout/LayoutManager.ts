module GYLite
{
	export class LayoutManager
	{
		protected static _renderFuncList:IUpdate[];
		protected static _tempRenderFuncList:IUpdate[];
		protected static _rending:Boolean=false;
		protected static _stage:egret.Stage;
		public static max:number=0;
		public static lastNum:number=0;
		public static Init(stage:egret.Stage):void
		{
			var s = LayoutManager;
			s._stage=stage;
			s._renderFuncList = [];
			s._tempRenderFuncList = [];
		}
		/**
		 * 舞台渲染前执行
		 * */
		public static addRenderFunc(up:IUpdate):void
		{
			var s = LayoutManager;
			if(up==null)
				return;
			if(up.updating)return;
			if(s._rending)
				s._tempRenderFuncList.push(up);
			else
				s._renderFuncList.push(up);
			up.updating = true;
			if(s._renderFuncList.length > 1)return;
			s._stage.invalidate();
			s._stage.addEventListener(egret.Event.RENDER,s.renderFunc,null);
		}
		private static renderFunc(e:Event):void
		{			
			let s = LayoutManager;
			s._rending = true;
			s.update();			
			s._rending = false;
		}
		private static update():void
		{
			let s = LayoutManager;
			let len:number;
			let up:IUpdate;
			let tempVec:IUpdate[];
			s._rending = true;
			len=s._renderFuncList.length;
			if(s.max < len)s.max = len;
			s.lastNum = len;
			while(--len > -1)
			{
				up = s._renderFuncList[len];
				up.updating = false;
				if(up.disposed)
				{
					s._renderFuncList.splice(len, 1);					
					continue;
				}					
				up.updateView();
			}		
			s._renderFuncList.length = 0;			
			len = s._tempRenderFuncList.length;
			if(len > 0)
			{
				tempVec = s._renderFuncList;
				s._renderFuncList = s._tempRenderFuncList;
				s._tempRenderFuncList = tempVec;
				s.update();
			}
		}
	}
}