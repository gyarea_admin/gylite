declare namespace egret {
    /**
     * Call setter properties of the parent class, instead of the other writing languages, such as super.alpha = 1;
     * @param currentClass The current class class name, non-string
     * @param thisObj The current object. Always this
     * @param type Setter property names need to call
     * @param values Value passed to the parent class
     *
     * @exmaple egret.superSetter(egret.Sprite, this, "alpha", 1);
     * @language en_US
     */
    /**
     * 调用父类的setter属性，代替其他语言的写法，如 super.alpha = 1;
     * @param currentClass 当前 class 类名，非字符串
     * @param thisObj 当前对象。永远都this
     * @param type 需要调用的setter属性名称
     * @param values 传给父类的值
     *
     * @exmaple egret.superSetter(egret.Sprite, this, "alpha", 1);
     * @language zh_CN
     */
    function superSetter(currentClass: any, thisObj: any, type: string, ...values: any[]): any;
    /**
     * Get getter property value of the parent class. Instead of writing in other languages, such as super.alpha;
     * @param currentClass The current class class name, non-string
     * @param thisObj The current object. Always this
     * @param type Setter property names need to call
     * @returns {any} The value returned by the parent
     *
     * @exmaple egret.superGetter(egret.Sprite, this, "alpha");
     * @language en_US
     */
    /**
     * 获取父类的getter属性值。代替其他语言的写法，如 super.alpha;
     * @param currentClass 当前 class 类名，非字符串
     * @param thisObj 当前对象。永远都this
     * @param type 需要调用的getter属性名称
     * @returns {any} 父类返回的值
     *
     * @exmaple egret.superGetter(egret.Sprite, this, "alpha");
     * @language zh_CN
     */
    function superGetter(currentClass: any, thisObj: any, type: string): any;
}
