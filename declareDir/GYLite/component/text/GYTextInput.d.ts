declare module GYLite {
    class GYTextInput extends GYSkinContainer {
        protected _textInput: GYText;
        protected _promptText: GYText;
        protected _noInput: boolean;
        protected _isPassword: boolean;
        protected _defaultPromptFormat: TextFormat;
        private _editable;
        /**请在主题类内设置默认格式*/ static promptFormat: TextFormat;
        constructor(skin?: TextInputSkin, mul?: boolean);
        protected initComponent(): void;
        protected createPrompt(): void;
        get promptText(): GYText;
        get textInput(): GYText;
        protected getThemeSkin(): IGYSkin;
        set background(val: egret.BitmapData);
        set scale9GridRect(val: Scale9GridRect);
        set text(val: string);
        get text(): string;
        /**输入提示的文本格式*/
        set defaultPromptFormat(val: TextFormat);
        set width(val: number);
        get width(): number;
        set height(val: number);
        get height(): number;
        get prompt(): string;
        set prompt(val: string);
        protected textDeactive(e?: egret.Event): void;
        protected textActive(e: egret.Event): void;
        private promptState;
        set paddingTop(val: number);
        set paddingLeft(val: number);
        set paddingRight(val: number);
        set paddingBottom(val: number);
        get paddingTop(): number;
        get paddingLeft(): number;
        get paddingRight(): number;
        get paddingBottom(): number;
        set editable(val: boolean);
        get editable(): boolean;
    }
}
