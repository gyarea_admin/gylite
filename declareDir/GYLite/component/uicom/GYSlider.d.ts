declare module GYLite {
    class GYSlider extends GYProgressBar {
        protected _sliderX: number;
        protected _sliderY: number;
        protected _scrollStep: number;
        protected _downOffsetX: number;
        protected _downOffsetY: number;
        protected _downX: number;
        protected _downY: number;
        /**格式化显示的文本*/ formatValueFunc: (cur: number, max: number) => string;
        constructor(skin?: ISliderSkin, type?: number);
        protected initComponent(): void;
        protected addToStage(e: egret.Event): void;
        protected removeFromStage(e: egret.Event): void;
        protected downSlider(e: egret.TouchEvent): void;
        protected upSlider(e?: egret.TouchEvent): void;
        protected sliderLoop(t: number): void;
        setValueByPos(mX: number): void;
        protected setBar(): void;
        /**滑块初始X坐标*/
        get sliderX(): number;
        set sliderX(val: number);
        /**滑块初始Y坐标*/
        get sliderY(): number;
        set sliderY(val: number);
        /**分度值*/
        set scrollStep(val: number);
        get scrollStep(): number;
        /**自定义皮肤请实现ISliderSkin或继承SliderSkin*/
        protected getThemeSkin(): IGYSkin;
        get sliderBtn(): GYButton;
        static default_sliderX: number;
        static default_sliderY: number;
        static default_scrollStep: number;
    }
}
