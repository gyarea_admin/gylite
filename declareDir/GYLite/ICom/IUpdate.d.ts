declare module GYLite {
    interface IUpdate extends IResource {
        updateView(): void;
        updating: boolean;
    }
}
