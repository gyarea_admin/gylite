declare module GYLite {
    class GraphicsData {
        smoothing: boolean;
        color: number[];
        alpha: number[];
        ratio: number[];
        repeat: boolean;
        matrix: egret.Matrix;
        fillColor: number[];
        fillAlpha: number[];
        fillRatio: number[];
        fillMatrix: egret.Matrix;
        fillRepeat: boolean;
        width: number;
        height: number;
        x: number;
        y: number;
        type: GraphicsType;
        density: number;
        fillTexture: egret.Texture;
        texture: egret.Texture;
        vertices: number[];
        isDraw: boolean;
        cap: CanvasLineCap;
        joint: CanvasLineJoin;
        thickness: number;
        miterLimit: number;
        lineGradientType: string;
        fillGradientType: string;
        constructor(x: number, y: number, type: GraphicsType);
        inputGraphics(g: GYGraphics): void;
    }
    class LineData extends GraphicsData {
        constructor(x: number, y: number, type: GraphicsType);
        inputGraphics(g: GYGraphics): void;
    }
    class CircleData extends GraphicsData {
        radiusX: number;
        radiusY: number;
        smoothing: boolean;
        constructor(x: number, y: number, type: GraphicsType);
    }
    class PolyData extends GraphicsData {
        constructor(x: number, y: number, type: GraphicsType);
    }
    class TriangleData extends GraphicsData {
        indices: number[];
        uvs: number[];
        constructor(x: number, y: number, type: GraphicsType);
    }
    class ArcData extends GraphicsData {
        radius: number;
        anticlockwise: boolean;
        startAngle: number;
        endAngle: number;
        thickness: number;
        constructor(x: number, y: number, type: GraphicsType);
    }
}
