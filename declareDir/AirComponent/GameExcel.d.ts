declare module GYLite {
    class GameExcel extends GYExcel {
        constructor(cfg?: any);
        protected getExcelDrag(): GYSprite;
        protected createExcelGrid(): IItemRender;
        protected createColGrid(): IItemRender;
        protected createRowGrid(): IItemRender;
    }
}
