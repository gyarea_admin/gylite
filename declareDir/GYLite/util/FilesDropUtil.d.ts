/***2024-12-15 迷途小羔羊
* 用于编写处理外部拽文件的工具类
*/
declare module GYLite {
    class FilesDropUtil {
        private static _dropList;
        /**添加拖拽文件接收监听对象，最后监听的对象优先接收，同时只能有一个对象接收
         * @param obj 拖拽接收对象
        */
        static addDropObj(obj: IDropFileObject): void;
        /**移除拖拽文件接收监听对象，最后监听的对象优先接收，同时只能有一个对象接收
         * @param obj 拖拽接收对象
        */
        static removeDropObj(obj: IDropFileObject): void;
        /**开启拖拽监听*/
        static openRecFile(): void;
        private static findDropIndex;
        /**关闭拖拽监听*/
        static closeRecFile(): void;
        protected static dropFiles(e: DragEvent): void;
        private static defaultOnDragOver;
        private static defaultOnDragEnter;
        private static dragCall;
        protected static readDir(entry: FileSystemEntry, result: FileSystemFileEntry[], callBack: Function, thisObj: any, param?: any): void;
        static disposed: boolean;
        static dispose(): void;
    }
    interface IDropFileObject {
        /**回调,延迟将会100毫秒，以便页面获得焦点*/ dropCallBack: (e: DragEvent, result: FileSystemFileEntry[]) => void;
        thisObj: GYLite.IResource;
        /**监听模式，参考DropFlag常量，1 放置 2拖入 4经过，监听多个标志值相加*/ flag: number;
    }
    enum DropFlag {
        DROP = 1,
        ENTER = 2,
        OVER = 4
    }
}
