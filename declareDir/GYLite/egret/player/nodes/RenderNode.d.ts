declare namespace egret.sys {
    /**
     * @private
     * 渲染节点类型
     */
    const enum RenderNodeType {
        /**
         * 位图渲染节点
         */
        BitmapNode = 1,
        /**
         * 文本渲染节点
         */
        TextNode = 2,
        /**
         * 矢量渲染节点
         */
        GraphicsNode = 3,
        /**
         * 组渲染节点
         */
        GroupNode = 4,
        /**
         * Mesh 节点
         */
        MeshNode = 5,
        /**
         * 普通位图渲染节点
         */
        NormalBitmapNode = 6
    }
    /**
     * @private
     * 渲染节点基类
     */
    class RenderNode {
        /**
         * 节点类型..
         */
        type: number;
        /**
         * 绘制数据
         */
        drawData: any[];
        /**
         * 绘制次数
         */
        protected renderCount: number;
        /**
         * 在显示对象的$updateRenderNode()方法被调用前，自动清空自身的drawData数据。
         */
        cleanBeforeRender(): void;
        $getRenderCount(): number;
        cacheDrawData: any;
        /**节点tint颜色值*/ color: number;
    }
}
