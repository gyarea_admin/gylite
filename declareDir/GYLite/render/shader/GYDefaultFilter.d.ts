declare module GYLite {
    class GYDefaultFilter extends egret.CustomFilter {
        constructor(vertexSrc?: string, fragmentSrc?: string, uniforms?: any);
    }
}
