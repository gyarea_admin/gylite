declare module GYLite {
    class GYSeqImage extends GYImage {
        static default_interval: number;
        protected _seqArr: Array<any>;
        protected _seqIndex: number;
        protected _startTime: number;
        protected _intervalTime: number;
        protected _isRunning: number;
        protected _startIndex: number;
        protected _waitSource: any;
        protected _sourceWaitRate: number;
        protected _loopTimes: number;
        protected _reserveEnd: Boolean;
        private _reserving;
        protected _loops: number;
        protected _frameNum: number;
        private _endFunc;
        private _updateFunc;
        private _endObject;
        private _updateObject;
        private _endIndex;
        get seqArr(): Array<any>;
        get frameNum(): number;
        get seqIndex(): number;
        constructor();
        protected initComponent(): void;
        get source(): any;
        set source(bmp: any);
        /**结束回调，
         * val endFunc(loopTimes) 参数为已播放几次,
         * */
        setEndFunc(val: Function, thisObject: any): void;
        /**更新回调
         *  val endFunc(index) 参数为当前第几帧
         * */
        setUpdateFunc(val: Function, thisObject: any): void;
        protected baseSource(bmp: any): void;
        private setSource;
        /**序列帧播放
         * @param seq 序列，默认null，使用原来设置的序列
         * @param seqInd 起始索引，默认0
         * */
        start(seq?: Array<any>, seqInd?: number): void;
        private reset;
        /**停止序列帧
         * @param ind 默认-1， 如果-1，这清理序列帧 如果是大于-1，则停止在ind索引的图片显示
         * @param clearSeq 是否清空序列，默认true
         * */
        stop(ind?: number, clearSeq?: boolean): void;
        private loop;
        /**序列帧间隔(毫秒)* */
        get intervalTime(): number;
        set intervalTime(value: number);
        get isRunning(): number;
        /**切换图片时需要达到的帧数（当是序列帧动画） */
        get sourceWaitRate(): number;
        set sourceWaitRate(value: number);
        get reserveEnd(): Boolean;
        set reserveEnd(value: Boolean);
        /**循环多少次停止(默认永久循环)*/
        get loops(): number;
        set loops(value: number);
        dispose(disposeChild?: boolean, removeChild?: boolean, forceDispose?: boolean): void;
    }
}
