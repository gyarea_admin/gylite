module GYLite {
	export class Keyboard {
		public static A:number = 65;  
        public static B:number = 66;  
        public static C:number = 67;  
        public static D:number = 68;  
        public static E:number = 69;  
        public static F:number = 70;  
        public static G:number = 71;  
        public static H:number = 72;  
        public static I:number = 73;  
        public static J:number = 74;  
        public static K:number = 75;  
        public static L:number = 76;  
        public static M:number = 77;  
        public static N:number = 78;  
        public static O:number = 79;  
        public static P:number = 80;  
        public static Q:number = 81;  
        public static R:number = 82;  
        public static S:number = 83;  
        public static T:number = 84;  
        public static U:number = 85;  
        public static V:number = 86;  
        public static W:number = 87;  
        public static X:number = 88;  
        public static Y:number = 89;  
        public static Z:number = 90;  
        public static BACKSPACE:number = 8;  
        public static CAPS_LOCK:number = 20;  
        public static CONTROL:number = 17;  
		public static ALTERNATE:number = 18;  
        public static DELETE:number = 46;  
        public static DOWN:number = 40;  
        public static END:number = 35;  
        public static ENTER:number = 13;  
        public static ESCAPE:number = 27;  
        public static F1:number = 112;  
        public static F10:number = 121;  
        public static F11:number = 122;  
        public static F12:number = 123;  
        public static F13:number = 124;  
        public static F14:number = 125;  
        public static F15:number = 126;  
        public static F2:number = 113;  
        public static F3:number = 114;  
        public static F4:number = 115;  
        public static F5:number = 116;  
        public static F6:number = 117;  
        public static F7:number = 118;  
        public static F8:number = 119;  
        public static F9:number = 120;  
        public static HOME:number = 36;  
        public static INSERT:number = 45;  
        public static LEFT:number = 37;  
        public static NUMPAD_0:number = 96;  
        public static NUMPAD_1:number = 97;  
        public static NUMPAD_2:number = 98;  
        public static NUMPAD_3:number = 99;  
        public static NUMPAD_4:number = 100;  
        public static NUMPAD_5:number = 101;  
        public static NUMPAD_6:number = 102;  
        public static NUMPAD_7:number = 103;  
        public static NUMPAD_8:number = 104;  
        public static NUMPAD_9:number = 105;  
        public static NUMPAD_ADD:number = 107;  
        public static NUMPAD_DECIMAL:number = 110;  
        public static NUMPAD_DIVIDE:number = 111;  
        public static NUMPAD_ENTER:number = 108;  
        public static NUMPAD_MULTIPLY:number = 106;  
        public static NUMPAD_SUBTRACT:number = 109;  
        public static NUM_0:number = 48;  
        public static NUM_1:number = 49;  
        public static NUM_2:number = 50;  
        public static NUM_3:number = 51;  
        public static NUM_4:number = 52;  
        public static NUM_5:number = 53;  
        public static NUM_6:number = 54;  
        public static NUM_7:number = 55;  
        public static NUM_8:number = 56;  
        public static NUM_9:number = 57;
        public static PAGE_DOWN:number = 34;  
        public static PAGE_UP:number = 33;  
        public static RIGHT:number = 39;  
        public static SHIFT:number = 16;  
        public static SPACE:number = 32;  
        public static TAB:number = 9;  
        public static UP:number = 38;

        /**是否输入内容的keycode*/
		public static isInputKeyCode(keyCode:number):boolean
		{
            if(Keyboard.NUMPAD_ENTER == keyCode)
                return false;
			if(Keyboard.NUMPAD_0 <= keyCode && Keyboard.NUMPAD_DIVIDE >= keyCode)
                return true;
            if(Keyboard.NUM_0 <= keyCode && Keyboard.NUM_9 >= keyCode)
                return true;
            if(Keyboard.A <= keyCode && Keyboard.Z >= keyCode)
                return true;
            if(186 <= keyCode && 222 >= keyCode)
                return true;
            return keyCode == Keyboard.SPACE;
		}      
        public static numSign:string[] = ["!","#","$","%","^","&","*","(",")"];
        public static otherSign:string[] = [";","=",",","-",".","/","\`","[","\\","]","'"];
        public static otherSign2:string[] = [":","+","<","_",">","?","~","{","|","}", "\""];
        public static getStrFromKeyCode(keyCode:number, isShift:boolean, capsLock:boolean):string
        {
            if(Keyboard.NUMPAD_ENTER == keyCode)
                return "";
            if(keyCode == Keyboard.SPACE)
                return " ";
            if(Keyboard.NUMPAD_0 <= keyCode && Keyboard.NUMPAD_9 >= keyCode)
                return ""+(keyCode - Keyboard.NUMPAD_0);
            if(Keyboard.NUM_0 <= keyCode && Keyboard.NUM_9 >= keyCode)
            {
                if(isShift)
                    return Keyboard.numSign[keyCode - Keyboard.NUM_0];
                return ""+(keyCode - Keyboard.NUM_0);
            }
                
            if(Keyboard.A <= keyCode && Keyboard.Z >= keyCode)
            {
                if(isShift || capsLock)
                    return String.fromCharCode(65+(keyCode - Keyboard.A));
                return String.fromCharCode(97+(keyCode - Keyboard.A));
            }
                
            if(186 <= keyCode && 222 >= keyCode)
            {
                if(isShift)
                    return Keyboard.otherSign2[keyCode - 186];
                return Keyboard.otherSign[keyCode - 186];
            }
            return "";   
        }
		
	}
}