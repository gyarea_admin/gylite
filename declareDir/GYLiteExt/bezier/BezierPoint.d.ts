/**
@author 迷途小羔羊
2022.09.27
*/
declare module GYLite {
    /**贝塞尔端点类，用于控制贝塞尔曲线*/
    export class BezierPoint {
        private _prePoint;
        private _nextPoint;
        private _frontControlPoint;
        private _backControlPoint;
        private _point;
        constructor(x?: number, y?: number, s?: Single);
        set x(val: number);
        set y(val: number);
        get x(): number;
        get y(): number;
        set frontControlX(val: number);
        set frontControlY(val: number);
        get frontControlX(): number;
        get frontControlY(): number;
        set backControlX(val: number);
        set backControlY(val: number);
        get backControlX(): number;
        get backControlY(): number;
        get prePoint(): BezierPoint;
        set prePoint(val: BezierPoint);
        get nextPoint(): BezierPoint;
        set nextPoint(val: BezierPoint);
        get frontControlPoint(): Point;
        get backControlPoint(): Point;
        get point(): Point;
        clear(): void;
        /**计算3次贝塞尔的长度
         * @param p0 起点
         * @param p1 起点的控制点
         * @param p2 终点的控制点
         * @param p3 终点
         * @param density 绘制点的密度
        */
        static getThreeTimesLength(p0: Point, p1: Point, p2: Point, p3: Point, density?: number): number;
        /**绘制3次贝塞尔曲线的线条路径
         * @param g Graphics
         * @param p0 起点
         * @param p1 起点的控制点
         * @param p2 终点的控制点
         * @param p3 终点
         * @param density 绘制点的密度
         * @param excludeEndPoint 不包括最后一个采样点，默认false，对于环绕的贝塞尔，我们可以剔除最后一个采样点，因为我们需要环接
        */
        static drawThreeTimes(g: egret.Graphics, p0: Point, p1: Point, p2: Point, p3: Point, density?: number, excludeEndPoint?: boolean): void;
        /**绘制3次贝塞尔曲线的波点路径
         * @param g Graphics
         * @param p0 起点
         * @param p1 起点的控制点
         * @param p2 终点的控制点
         * @param p3 终点
         * @param density 绘制点的密度
         * @param excludeEndPoint 不包括最后一个采样点，默认false，对于环绕的贝塞尔，我们可以剔除最后一个采样点，因为我们需要环接
        */
        static drawThreeTimes2(g: egret.Graphics, p0: Point, p1: Point, p2: Point, p3: Point, density?: number, c?: number, r?: number, excludeEndPoint?: boolean): void;
        /**绘制3次贝塞尔曲线的路径波点
         * @param g Graphics
         * @param p0 起点
         * @param p1 起点的控制点
         * @param p2 终点的控制点
         * @param p3 终点
         * @param density 绘制点的密度, 默认0.05
         * @param callBack (x:number,y:number,end:number); x,y坐标 end 0代表起点 1代表执行中 2代表结束
         * @param thisObj this指向
        */
        static drawThreeTimesCall(p0: Point, p1: Point, p2: Point, p3: Point, density?: number, callBack?: Function, thisObj?: any): void;
        /**贝塞尔采样的弦高最大值（像素）*/
        static chordMaxH: number;
        /**贝塞尔采样的弦高最小值（像素）*/
        static chordMinH: number;
        /**轻微倾斜最大值判断，用于矫正轻微倾斜时对衔接进行0.5辅助*/
        static tinyTanMax: number;
        /**轻微倾斜最小值判断，用于矫正轻微倾斜时对衔接进行0.5辅助*/
        static tinyTanMin: number;
        /**默认贝塞尔采样步进密度基准值*/
        static defaultDensty: number;
        private static tempPoint1;
        private static tempPoint2;
        private static _pool;
        private static _s;
        static getInstance(x?: number, y?: number): BezierPoint;
    }
    class Single {
    }
    export {};
}
