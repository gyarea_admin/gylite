declare module GYLite {
    /**UIComponent是有组件的基础组件，具有布局与监听视图变化功能，
     * 拥有视图改变的事件通知,以便通知子级父级容器变化，如只需要布局的普通容器建议使用GYSprite*/
    class GYUIComponent extends GYSprite {
        protected _boundRect: egret.Rectangle;
        protected _elementsRect: egret.Rectangle;
        $paddingLeft: number;
        $paddingRight: number;
        $paddingTop: number;
        $paddingBottom: number;
        $contentWidth: number;
        $contentHeight: number;
        constructor();
        /**视图改变，派发VIEWCHANGE事件并通知父级GYUIComponent调用s.viewChange*/
        viewChange(): void;
        addElement(child: IGYDisplay): IGYDisplay;
        addElementAt(child: IGYDisplay, index: number): IGYDisplay;
        removeElement(child: IGYDisplay): IGYDisplay;
        removeElementAt(index: number): IGYDisplay;
        set paddingLeft(val: number);
        set paddingRight(val: number);
        set paddingTop(val: number);
        set paddingBottom(val: number);
        set_paddingLeft(val: number): void;
        set_paddingRight(val: number): void;
        set_paddingTop(val: number): void;
        set_paddingBottom(val: number): void;
        updateView(): void;
        /**左边距*/
        get paddingLeft(): number;
        /**右边距*/
        get paddingRight(): number;
        /**上边距*/
        get paddingTop(): number;
        /**底边距*/
        get paddingBottom(): number;
        set x(val: number);
        get x(): number;
        set y(val: number);
        get y(): number;
        set_width(val: number): boolean;
        get_width(): number;
        set_height(val: number): boolean;
        get_height(): number;
        /**不带边距的宽度*/
        get baseWidth(): number;
        /**不带边距的高度*/
        get baseHeight(): number;
        set scaleX(val: number);
        get scaleX(): number;
        set scaleY(val: number);
        get scaleY(): number;
        /**获取element元素边框宽度*/
        get borderWidth(): number;
        /**获取element元素边框高度*/
        get borderHeight(): number;
        /**重置容器边界，将重新计算内容得到新边界*/
        protected resetBound(): void;
        /**获取容器内容边界大小，与getElementsBounds不一样的地方，此方法不包括padding*/
        getAllBounds(t: IGYDisplay): egret.Rectangle;
        protected resetElementsBound(): void;
        /**获取容器内容边界大小，与getAllBounds不一样的地方，此方法包括padding*/
        getElementsBounds(t: IGYDisplay): egret.Rectangle;
        /**同时设置4个边距*/
        setPadding(val: number): void;
        /**内容宽度*/
        get contentWidth(): number;
        /**内容高度*/
        get contentHeight(): number;
    }
}
