class BaseRet {
	/**本地模拟数据*/public nativeRet(data:any):BaseRet{return this;};
	/**远端数据*/public dataRet(data:any):BaseRet{return this;};
	/**清空数据*/public clear():void{
		let s= this;
		for(var key in s)
		{
			if(typeof s[key] == "function")
				continue;
			delete s[key];
		}
	};
}