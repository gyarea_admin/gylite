declare namespace egret.sys {
    /**
     * @private
     * 是否ios14
     */
    let isIOS14: boolean;
    /**
     * @private
     */
    let systemRenderer: SystemRenderer;
    /**
     * @private
     * 用于碰撞检测绘制
     */
    let canvasRenderer: SystemRenderer;
    /**
     * @private
     * 显示渲染器接口
     */
    interface SystemRenderer {
        /**
         * 渲染一个显示对象
         * @param displayObject 要渲染的显示对象
         * @param buffer 渲染缓冲
         * @param matrix 要叠加的矩阵
         * @param forRenderTexture 绘制目标是RenderTexture的标志
         * @returns drawCall触发绘制的次数
         */
        render(displayObject: DisplayObject, buffer: RenderBuffer, matrix: Matrix, forRenderTexture?: boolean): number;
        /**
         * 将一个RenderNode对象绘制到渲染缓冲
         * @param node 要绘制的节点
         * @param buffer 渲染缓冲
         * @param matrix 要叠加的矩阵
         * @param forHitTest 绘制结果是用于碰撞检测。若为true，当渲染GraphicsNode时，会忽略透明度样式设置，全都绘制为不透明的。
         */
        drawNodeToBuffer(node: sys.RenderNode, buffer: RenderBuffer, matrix: Matrix, forHitTest?: boolean): void;
        renderClear(): any;
        /**GYLite 是否ios10以上*/
        get wxiOS10(): boolean;
        set wxiOS10(val: boolean);
    }
    /**
     *
     */
    interface RenderContext {
    }
    /**
     * 创建一个canvas。
     */
    function mainCanvas(width?: number, height?: number): HTMLCanvasElement;
    function createCanvas(width?: number, height?: number): HTMLCanvasElement;
    /**
    * 重新设置主canvas的大小
    */
    function resizeContext(renderContext: RenderContext, width: number, height: number, useMaxSize?: boolean): void;
    /**
    * 获得系统的渲染运行时
    */
    function getContextWebGL(surface: HTMLCanvasElement): WebGLRenderingContext;
    function getContext2d(surface: HTMLCanvasElement): CanvasRenderingContext2D;
    /**
    * 仅通过bitmapData创建纹理
    */
    function createTexture(renderContext: RenderContext, bitmapData: BitmapData | HTMLCanvasElement): WebGLTexture;
    /**
    * 通过 width, height, data创建纹理
    */
    function _createTexture(renderContext: RenderContext, width: number, height: number, data: any): WebGLTexture;
    /**
     * 画texture
     **/
    function drawTextureElements(renderContext: RenderContext, data: any, offset: number): number;
    /**
     * 测量文本的宽度
     * @param context
     * @param text
     */
    function measureTextWith(context: CanvasRenderingContext2D, text: string): number;
    /**
     * 为CanvasRenderBuffer创建一个canvas
     * @param defaultFunc
     * @param width
     * @param height
     * @param root
     */
    function createCanvasRenderBufferSurface(defaultFunc: (width?: number, height?: number) => HTMLCanvasElement, width?: number, height?: number, root?: boolean): HTMLCanvasElement;
    /**
     * 改变渲染缓冲的大小并清空缓冲区
     * @param renderContext
     * @param width
     * @param height
     * @param useMaxSize
     */
    function resizeCanvasRenderBuffer(renderContext: RenderContext, width: number, height: number, useMaxSize?: boolean): void;
}
