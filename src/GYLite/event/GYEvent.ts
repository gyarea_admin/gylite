module GYLite
{	
	export class GYEvent extends egret.Event
	{
		/**加载完成*/
		public static LOAD_COMPLETE:string="GY_loadComplete";
		/**数据刷新完毕触发*/
		public static VALUE_COMMIT:string="GY_ValueCommit";
		/**全局拖动通知*/
		public static GLOABL_DRAG:string="GY_GlobalDrag";
		public data:any;
		public constructor(type:string, bubbles:boolean=false, cancelable:boolean=false, data:any=null)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}
	}
}