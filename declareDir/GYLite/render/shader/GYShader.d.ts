declare module GYLite {
    class GYShader {
        private static _d2_vert_src;
        private static _d2_frag_src;
        private static _d2_primitive_frag;
        static getD2VertSrc(): string;
        static getD2FragSrc(): string;
        static getD2_primitive_frag(): string;
    }
}
