module GYLite{
    export class GYDefaultFilter extends egret.CustomFilter{
        constructor(vertexSrc:string=null, fragmentSrc:string=null, uniforms:any=null)
        {
            super(GYShader.getD2VertSrc(),GYShader.getD2FragSrc(),uniforms);
        }
    }
}