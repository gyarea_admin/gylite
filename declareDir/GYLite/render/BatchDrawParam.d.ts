declare module GYLite {
    class BatchDrawParam {
        /**样式类型*/ type: BatchDrawType;
        /**X半径*/ radiusX: number;
        /**Y半径*/ radiusY: number;
        /**圆角半径*/ angleRadius: number;
        /**绘制偏移量X*/ offsetX: number;
        /**绘制偏移量Y*/ offsetY: number;
        /**缩放X*/ scaleX: number;
        /**缩放Y*/ scaleY: number;
        constructor(type: BatchDrawType, radiusX?: number, radiusY?: number, scaleX?: number, scaleY?: number, offsetX?: number, offsetY?: number);
        /**宽度*/
        getWidth(): number;
        /**高度*/
        getHeight(): number;
        draw(ctx: CanvasRenderingContext2D, element: HTMLImageElement | HTMLCanvasElement | BatchElement, gap?: number): void;
        private static drawCircle;
        private static drawRect;
        /**圆角绘制(未完成)*/
        private static drawRoundRect;
        /**文本绘制*/
        private static drawText;
        /**矢量绘制*/
        private static graphicsDraw;
        /**
         * 绘制一段圆弧路径
         * @param x 圆弧中心（圆心）的 x 轴坐标。
         * @param y 圆弧中心（圆心）的 y 轴坐标。
         * @param radiusX 圆弧的半径 x。
         * @param radiusY 圆弧的半径 y。
         * @param startAngle 圆弧的起始点， x轴方向开始计算，单位以弧度表示。
         * 注意：必须为正数。
         * @param endAngle 圆弧的终点， 单位以弧度表示。
         * 注意：与startAngle差值必须在0~2π之间。
         * @param anticlockwise 如果为 true，逆时针绘制圆弧，反之，顺时针绘制。
         * 注意：如果为true，endAngle必须小于startAngle，反之必须大于。
         */
        private static arcToBezier;
        private static drawRoundRectShape;
        private static drawRectShape;
        private static drawLineShape;
        private static drawCircleShape;
        private static _chineseCharacterMeasureFastMap;
        private static _chineseCharactersRegExp;
        static measure(text: string, font: string, format: Format): number;
        static draw(ctx: CanvasRenderingContext2D, element: HTMLImageElement | HTMLCanvasElement | BatchElement, param: BatchDrawParam, gap?: number): void;
    }
    class BatchDrawType {
        /**矩形裁切*/ static RECT: BatchDrawType;
        /**圆形裁切*/ static CIRCLE: BatchDrawType;
        /**圆角裁切*/ static ROUND_RECT: BatchDrawType;
        /**文本绘制*/ static TEXT: BatchDrawType;
        /**graphics绘制*/ static GRAPHICS: BatchDrawType;
        $id: number;
        $name: string;
        constructor(id: number, name: string);
    }
}
