module GYLite
{
					
			
	export class LinkButtonSkin extends GYSkin implements IButtonSkin
	{
		protected _stsVec:TextFormat[];
		protected _curSkin:GYText;
		public constructor(skinVec:TextFormat[])
		{
			super();
			var s = this;
			s._stsVec=skinVec;
			if(s._stsVec.length > 8 || s._stsVec.length == 0)
				throw new Error("按钮皮肤参数不对！#" + s._stsVec.length);
			s._curSkin=new GYText;
			s._curSkin.touchEnabled = false;
			//s._curSkin.selectable = false;
		}
		public drawSkin(state:number):void
		{var s = this;
			var t:TextFormat = ((state<s._stsVec.length && s._stsVec[state]) ? s._stsVec[state] : s._stsVec[0]);
			s._curSkin.textFormat = t;
		}
		public get width():number
		{var s = this;
			return s._curSkin.width;
		}
		public get height():number
		{var s = this;
			return s._curSkin.height;
		}
		
		public set width(val:number)
		{var s = this;
			s._curSkin.width = val;
		}
		
		public set height(val:number)
		{var s = this;
			s._curSkin.invalidFormat();
		}
		
		public set label(val:string)
		{var s = this;
			s._curSkin.text = val;
		}
		
		public get label():string
		{var s = this;
			return s._curSkin.text;
		}
		
		public get labelDisplay():GYText
		{var s = this;
			return s._curSkin;
		}
		public set labelDisplay(val:GYText)
		{var s = this;
			s._curSkin = val;
			if(s._hostComponent)
			{
				s._hostComponent.addElement(s._curSkin);
				s._curSkin.width=s._hostComponent.width;
				s._curSkin.verticalCenter = 0;
			}
		}
		/**此函数只克隆s.labelDisplay的textFormat*/
		public cloneLabel():GYText
		{var s = this;
			var newText:GYText=new GYText;
			newText.textFormat = s._curSkin.textFormat;
			newText.text = s._curSkin.text;
			return newText;
		}
		/**此函数复制皮肤属性 如s.label等皮肤的属性*/
		public copy(skin:IGYSkin)
		{var s = this;
			if(skin == null)
				return;
			s.labelDisplay = (skin as IButtonSkin).labelDisplay;
		}
		public clone():IGYSkin
		{var s = this;
			var buttonSkin:LinkButtonSkin=new LinkButtonSkin(s._stsVec);
			return buttonSkin;
		}
		
		public set hostComponent(val:GYSprite)
		{var s = this;
			if(val==s._hostComponent)
				return;
			if(s._hostComponent)
			{
				if(s._curSkin.parent)
					s._hostComponent.removeElement(s._curSkin);
			}
			s._hostComponent=val;
			if(s._hostComponent)
			{
				s._hostComponent.addElementAt(s._curSkin,0);
				if(!isNaN(s._hostComponent.settingWidth))
					s._curSkin.width = s._hostComponent.width;
				s._curSkin.invalidFormat();
			}
		}
		public get hostComponent():GYSprite
		{var s = this;
			return s._hostComponent;
		}
	}
}