declare module GYLite {
    interface IProgressBarSkin extends IGYSkin {
        width: number;
        height: number;
        barWidth: number;
        barHeight: number;
        backgroundWidth: number;
        backgroundHeight: number;
        barX: number;
        barY: number;
        clipX: number;
        clipY: number;
        mode: number;
        rotation: number;
        textSkin: GYText;
        backgroundSkin: GYScaleSprite;
        barSkin: GYScaleSprite;
    }
}
