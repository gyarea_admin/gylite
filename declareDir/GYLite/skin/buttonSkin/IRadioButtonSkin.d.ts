declare module GYLite {
    interface IRadioButtonSkin extends IButtonSkin {
        gap: number;
    }
}
