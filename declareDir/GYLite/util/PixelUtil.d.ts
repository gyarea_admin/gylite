/***2024-12-17 迷途小羔羊
* 用于编写处理像素的工具类
*/
declare module GYLite {
    class PixelUtil {
        /**获取像素图的轮廓
         * @param pixels 像素数组[r0,g0,b0,a0,r1,g1,b1,a1……]
         * @param width 图的宽度
         * @param height 图的高度
         * @param anglePrecision 角度精度(弧度) [0,2Π]，越小描点数越少，推荐10度0.1745329 或者 15度 0.2679491 或者 25度0.466307
         * @param distancePrecision 距离精度 [1,显示对象尺寸]，越小描点数越少，推荐5
         * @param step 查询点间隔距离 [1,显示对象尺寸/4]，越小越精确，运算性能越低，推荐4
         * @return number[][] 返回rect轮廓和多边形轮廓 [rectArray,polygonArray]
        */
        static getOutline(pixels: number[], width: number, height: number, anglePrecision?: number, distancePrecision?: number, step?: number): IOutLineResult;
        static getRectOutline(pixels: number[], width: number, height: number): number[];
    }
    interface IOutLineResult {
        /**矩形轮廓[minX,minY,maxX,minY,maxX,maxY,minX,maxY]*/
        0: number[];
        /**多边形轮廓[x0,y0,x1,y1……]*/
        1: number[];
    }
}
