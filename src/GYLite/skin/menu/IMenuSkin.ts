module GYLite
{
			
	export interface IMenuSkin extends IGYSkin
	{
		backgroundImage:GYScaleSprite;
		list?:GYLite.GYListV;
	}
}