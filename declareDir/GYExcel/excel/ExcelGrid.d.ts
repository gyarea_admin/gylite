declare module GYLite {
    class ExcelGrid extends ItemRender implements IReceiver, IItemRender {
        protected _text: GYText;
        excel: GYExcel;
        selFrame: ExcelFrame;
        constructor();
        protected initComponent(): void;
        protected drawBg(c?: number): void;
        set selected(b: boolean);
        get selected(): boolean;
        protected widthAndHeightChange(e: GYViewEvent): void;
        /**设置格子信息 */
        setData(obj: any): void;
        protected clkGrid(e: egret.TouchEvent): void;
        protected rollOver(e: MouseEvent): void;
        protected inputDeactive(e: egret.Event): void;
        receiveData(val: IDragger): void;
    }
}
