module GYLite
{	
	export class ExcelGroup extends GYGroup
	{
		//Excel
		public gyExcel:GameExcel;
		public constructor()
		{
			super();
			var s = this;			
			s.init();
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private addToStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
			s.addEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
		}
		private removeFromStage(e:egret.Event):void
		{var s = this;
			s.removeEventListener(egret.Event.REMOVED_FROM_STAGE, s.removeFromStage,s);
			s.addEventListener(egret.Event.ADDED_TO_STAGE, s.addToStage,s);
		}
		private init():void
		{var s = this;
			/*Excel*/
			s.gyExcel = new GameExcel;
			s.gyExcel.rowsListWidth = 35;
			s.gyExcel.colsListHeight = 25;
			s.gyExcel.x = 50;
			s.gyExcel.y = 100;
			s.gyExcel.dataProvider = s.getExcelData(150,100);
			s.gyExcel.setDataGridHeight(400);
			s.gyExcel.setDataGridWidth(400);
			s.gyExcel.setScrollerHeight(400);
			s.gyExcel.setScrollerWidth(400);
			// Main.instance.setTip(s.gyExcel, "用羔羊引擎拼接一个类Eexcel表格，意思意思");
			s.addElement(s.gyExcel);
		}
		private getExcelData(cols:number=0,rows:number=0):Array<any>
		{var s = this;
			var i:number=0,j:number=0,len:number=0;
			var arr:Array<any>=[];
			var obj:ExcelData;
			for(i=0;i<rows;++i)
			{
				var temp:Array<any>=arr[i]=[];
				for(j=0;j<cols;++j)
				{
					len = i*cols+j;
					obj=new ExcelData;
					//obj.dataType = ExcelData.TYPE_NUMBER;
					obj.value = len;
					obj.index = len;
					obj.col = j;
					obj.row = i;
					temp.push(obj);
				}
			}
			return arr;
		}
	}
}