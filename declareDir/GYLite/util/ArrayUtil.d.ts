/***2024-12-24 迷途小羔羊
* 用于编写数组工具类
*/
declare module GYLite {
    class ArrayUtil {
        /**缩放数组数值
         * @param arr 数组
         * @param mulValue 缩放值
        */
        static MulNumberArray(arr: number[], mulValue: number): number[];
        /**查询数组中的对象
         * @param arr 数组
         * @param key 比较数组对象内的key字段的值
         * @param value 比较的value值
         * @return 找不到返回null
        */
        static findObject(arr: any[], key: string, value: any): any;
        /**查询数组中的对象的索引
         * @param arr 数组
         * @param key 比较数组对象内的key字段的值，默认null，表示直接比较数组对象
         * @param value 比较的value值
         * @return 找不到返回-1
        */
        static findObjectIndex(arr: any[], key?: string, value?: any): number;
        /**比较两个数组元素是否不一样（以长的数组为准）
         * @param arr1 数组1
         * @param arr2 数组2
         * @return 返回 结果数组
        */
        static diffArray(arr1: any[], arr2: any[]): boolean[];
        /**查询最大值*/
        static max(arr: any[], key?: string): number;
        /**查询最小值*/
        static min(arr: any[], key?: string): number;
    }
}
