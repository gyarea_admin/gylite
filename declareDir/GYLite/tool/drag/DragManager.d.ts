declare module GYLite {
    class DragManager {
        private static _dragGroup;
        private static _dragVec;
        private static _dragShapes;
        /**拖动对象的s.x偏移*/ static offsetX: number;
        /**拖动对象的s.y偏移*/ static offsetY: number;
        /**拖动对象的透明度*/ static alpha: number;
        /**拖动对象的是否平滑处理*/ static smoothing: boolean;
        /**默认s.x偏移*/ static defaultOffsetX: number;
        /**默认s.y偏移*/ static defaultOffsetY: number;
        /**不锁定位置*/ static NONE_LCOK: number;
        /**锁定到对象中心*/ static CENTER_LCOK: number;
        /**锁定到对象左上角*/ static LEFT_TOP_LCOK: number;
        /**锁定到对象右上角*/ static RIGHT_TOP_LCOK: number;
        /**锁定到对象左下角*/ static LEFT_BOTTOM_LCOK: number;
        /**锁定到对象右下角*/ static RIGHT_BOTTOM_LCOK: number;
        static Init(): void;
        /**设置为拖动对象* */
        static setDrag(dragger: IDragger): void;
        /**取消拖动* */
        static cancelDrag(dragger: IDragger): void;
        /**添加到为拖动组，任意对象触发拖动时，一并跟随, 释放拖动自动清除拖动组
         * */
        static addToDragGroup(dragger: IDragger): void;
        /**从拖动组删除
         * @param dargger 拖动对象
         * */
        static deleteFromDragGroup(dragger: IDragger): void;
        /**清除拖动组 */
        static clearDraggerGroup(): void;
        /**设置偏移量为默认值 */
        static resetOffset(): void;
        private static dragStop;
        private static dragStart;
        private static dragging;
        private static getDragShape;
    }
}
