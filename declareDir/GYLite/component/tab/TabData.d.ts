/**
 @author 迷途小羔羊
 2015.6.2
 */
declare module GYLite {
    /**TabData是记录Tab组件按钮绑定的数据*/
    class TabData {
        /**按钮标签*/
        label: string;
        /**按钮控制的容器*/
        groupCls: any;
        private _group;
        /**附带参数*/
        param: any;
        private _parent;
        private _selected;
        /**@s.param lab 按钮标签
         * @s.param grp 容器类型
         * @s.param pr 父级容器
         * */
        constructor(lab: string, grp: any, pr: IGYContainer);
        get parent(): IGYContainer;
        set selected(val: boolean);
        get selected(): boolean;
        get group(): GYUIComponent;
    }
}
