module GYLite
{			
	
	export class ExcelColGrid extends ItemRender implements IItemRender
	{
		protected _text:GYText;
		// protected _img:GYDrawImage;
		protected _drager:GYSprite;
		public excel:GYExcel;
		public constructor()
		{
			super();
			var s = this;
			s.width = 100;
			s.height = 25;
			s.initComponent();
		}
		protected initComponent():void
		{var s = this;
			s._text = new GYText;
			s._text.width = s.width;
			s._text.height = s.height;
			s._text.align = "center";
			s._text.touchEnabled = false;
			// s._text.selectable = false;
			s._text.font = "Microsoft YaHei";
			s._text.color = 0;
			s._text.verticalCenter = 8;
			// s._text.embedFonts = false;
			s._text.validFormat();
			s.addElement(s._text);
			GYSprite._rect = new egret.Rectangle(0,0,s._text.width,s._text.height);
			// s._img = new GYDrawImage;
			// s._img.touchEnabled = s._img.touchChildren = false;
			// s._img.verticalCenter = 5;
			// s.addElement(s._img);
			s.addEventListener(GYViewEvent.VIEWCHANGE, s.widthAndHeightChange,s);	
			s._drager = new GYSprite;
			s._drager.x = s.width - 5;
			DraggerHandle.getInstance(s._drager).addBind(s.dragSize,s);
			var g:egret.Graphics = s._drager.graphics;
			g.beginFill(0);
			g.drawRect(0,0,5,s.height);
			g.endFill();
			s.addElement(s._drager);
			s.touchEnabled = true;
			s._drager.touchEnabled = true;
		}
		protected drawBg():void
		{var s = this;
			s.graphics.clear();
			s.graphics.beginFill(0xcccccc,1);
			s.graphics.drawRect(0,0,s.width,s.height);
			s.graphics.beginFill(0xeeeeee,1);
			s.graphics.drawRect(1,1,s.width-2,s.height - 2);
			s.graphics.endFill();
		}
		protected dragSize(handle:DraggerHandle):void
		{var s = this;
			var ind:number=Number(s._data) - 1;
			s.excel.setGridWidth(ind, s.mouseX);
		}
		protected widthAndHeightChange(e:GYViewEvent):void
		{var s = this;
			s.drawBg();
			s._text.width = s.width;
			// s._img.draw(s._text);
			s._drager.x = s.width - 5;
		}
		/**@inheriDoc */
		public setData(obj:any):void
		{var s = this;
			s._data = obj;
			if(obj==null)
				s._text.text ="无";
			else
				s._text.text =String(obj);
			// s._img.draw(s._text);
			s.width = 100;
			s.height = 25;
		}
	}
}