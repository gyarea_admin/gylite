module GYLite
{
								
		
	export class ExcelFrame extends GYSprite
	{
		private _operHand:GYSprite;
		public startCol:number=0;
		public startRow:number=0;
		public endCol:number=0;
		public endRow:number=0;
		public calEndCol:number=0;
		public calEndRow:number=0;
		public calStartCol:number=0;
		public calStartRow:number=0;
		public owner:GYDataGrid;
		public excel:GYExcel;
		
		private _nowCalStartCol:number=0;
		private _nowCalEndCol:number=0;
		private _nowStartCol:number=0;
		private _nowEndCol:number=0;
		private _nowCalStartRow:number=0;
		private _nowCalEndRow:number=0;
		private _nowStartRow:number=0;
		private _nowEndRow:number=0;
		private _updateFrame:boolean;
		private _selFrame:ExcelSelFrame;
		private _calFrame:GYScaleSprite;
		private _drager:DraggerHandle;
		private _calDisX:number=0;
		private _calDisY:number=0;
		private _calDir:number=0;//填充方向 1 行 2列
		private _selStartX:number=0
		private _selStartY:number=0
		private _selWidth:number=0
		private _selHeight:number=0;
		public constructor(e:GYExcel)
		{
			super();
			var s = this;
			s.excel = e;
			s.init();
		}
		private init():void
		{var s = this;
			s._selFrame = new ExcelSelFrame;
			s._selFrame.owner = this;
			s._selFrame.touchEnabled = true;
			s.addElement(s._selFrame);
			s._operHand = new GYSprite;
			s._operHand.touchEnabled = true;
			var g:egret.Graphics;
			g = s._operHand.graphics;
			// g.beginFill(s.excel.config.frameHandlerBorderColor,1);
			// g.drawRect(0,0,8,5);
			g.lineStyle(1, s.excel.config.frameHandlerBorderColor);
			g.moveTo(0,0);
			g.lineTo(8,0);
			g.moveTo(0,0);
			g.lineTo(0,8);
			// g.beginFill(s.excel.config.frameHandlerBorderColor,1);
			// g.drawRect(0,1,1,7);
			g.beginFill(s.excel.config.frameHandlerColor,1);
			g.drawRect(0, 0, 8, 8);
			g.endFill();
			s.addElement(s._operHand);
			s._calFrame = new GYScaleSprite;
			s.addElement(s._calFrame);
			s._drager = DraggerHandle.getInstance(s._operHand)
			s._drager.addBind(s.calDrag,s);
			s._drager.handle.addEventListener(GYViewEvent.DRAGSTOP, s.calDragEnd,s);
		}
		private drawCalFrame(toX:number=0,toY:number=0,w:number=0,h:number=0):void
		{var s = this;
			var g:egret.Graphics = s._calFrame.graphics;
			g.clear();
			g.beginFill(s.excel.config.frameCalColor,1);
			g.drawRect(-2,-2,w+4,2);
			g.drawRect(-2,0,2,h);
			g.drawRect(w,0,2,h);
			g.drawRect(-2,h,w+4,2);			
			g.endFill();
			s._calFrame.x = toX;
			s._calFrame.y = toY;
		}
		private drawBg(toX:number=0,toY:number=0,w:number=0,h:number=0):void
		{var s = this;
			var g:egret.Graphics = s._selFrame.graphics;
			g.clear();
			g.beginFill(s.excel.config.frameColor,1);
			g.drawRect(0,0,w+4,2);
			g.drawRect(0,2,2,h);
			g.drawRect(w+2,2,2,h);
			g.drawRect(0,h+2,w+4,2);	
			g.endFill();
			s._operHand.x = toX + w - 4;
			s._operHand.y = toY + h - 4;
			s._selFrame.x = toX - 2;
			s._selFrame.y = toY - 2;
		}
		private calDrag(drager:DraggerHandle):void
		{var s = this;
			var dir:number=0;
			var disX:number=0,disY:number=0,tan:number;
			disX = s._operHand.mouseX;
			disY = -s._operHand.mouseY;
			if(s._calDisX == disX && disY == s._calDisY)
				return;
			s._calDisX = disX;
			s._calDisY = disY;
			if(s._calDisX == 0)
				dir = s._calDisY > 0?1:3;
			else
			{
				tan = s._calDisY / s._calDisX;
				if(s._calDisY >= 0)
				{
					if(s._calDisX > 0)
						dir = tan > 1?0:1;
					else if(s._calDisX < 0)
						dir = tan < -1?0:3;
				}
				else if(s._calDisY < 0)
				{
					if(s._calDisX > 0)
						dir = tan < -1?2:1;
					else if(s._calDisX < 0)
						dir = tan > 1?2:3;
				}
			}
			
			var stX:number=0,endX:number=0,stY:number=0,endY:number=0;
			var sCol:number=0,eCol:number=0,sRow:number=0,eRow:number=0;
			if(dir == 1 || dir == 3)
			{
				if(s.startCol > s.endCol)
				{
					sCol = s.endCol;
					eCol = s.startCol;
				}
				else
				{
					sCol = s.startCol;
					eCol = s.endCol;
				}
				s._calDir = 2;
				if(s.startCol <= s.calEndCol)
				{
					s._nowCalStartCol = sCol;
					s._nowCalEndCol = s.calEndCol;
				}
				else
				{
					s._nowCalStartCol = s.calEndCol;
					s._nowCalEndCol = eCol;
				}
				s._nowCalStartRow = s._nowStartRow;
				s._nowCalEndRow = s._nowEndRow;
				stX = s.owner.colsData[s._nowCalStartCol].posX;
				endX = s.owner.colsData[s._nowCalEndCol].posX + s.owner.colsData[s._nowCalEndCol].width;
				s.drawCalFrame(stX - s.excel.posX,s._selStartY,endX - stX,s._selHeight);				
			}
			else
			{
				if(s.startRow > s.endRow)
				{
					sRow = s.endRow;
					eRow = s.startRow;
				}
				else
				{
					sRow = s.startRow;
					eRow = s.endRow;
				}
				s._calDir = 1;
				if(s.startRow <= s.calEndRow)
				{
					s._nowCalStartRow = sRow;
					s._nowCalEndRow = s.calEndRow;
				}
				else
				{
					s._nowCalStartRow = s.calEndRow;
					s._nowCalEndRow = eRow;
				}
				s._nowCalStartCol = s._nowStartCol;
				s._nowCalEndCol = s._nowEndCol;
				stY = s.owner.rowsData[s._nowCalStartRow].posY;
				endY = s.owner.rowsData[s._nowCalEndRow].posY + s.owner.rowsData[s._nowCalEndRow].height;
				s.drawCalFrame(s._selStartX,stY - s.excel.posY,s._selWidth,endY - stY);
			}
		}
		private calDragEnd(e:GYViewEvent):void
		{var s = this;
			s._calFrame.graphics.clear();
			s._calDisX = s._calDisY = 0;
			var temp:number=0;
			if(s._calDir == 1)
			{
				if(s._nowCalStartRow < s._nowStartRow)
					s.excel.calculateRow(s._nowStartRow,s._nowCalStartRow,s._nowCalStartCol,s._nowCalEndCol);
				else if(s._nowEndRow < s._nowCalEndRow)
					s.excel.calculateRow(s._nowEndRow,s._nowCalEndRow,s._nowCalStartCol,s._nowCalEndCol);
				else
				{
					return;
				}
			}
			else
			{
				if(s._nowCalStartCol < s._nowStartCol)
					s.excel.calculateCol(s._nowCalStartRow,s._nowCalEndRow,s._nowStartCol,s._nowCalStartCol);
				else if(s._nowEndCol < s._nowCalEndCol)
					s.excel.calculateCol(s._nowCalStartRow,s._nowCalEndRow,s._nowEndCol,s._nowCalEndCol);
				else
				{
					return;
				}
			}
			s.calStartCol = s.startCol;
			s.calStartRow = s.startRow;
			s.calEndCol = s.endCol;
			s.calEndRow = s.endRow;
		}

		public updateView():void
		{var s = this;
			super.updateView();
			if(s._updateFrame)
			{
				s.updateFrameView();
				s._updateFrame=false;
			}
		}
		/**选框重新布局*/
		public invalidFrame():void
		{var s = this;
			if(s._updateFrame)
				return;
			s.displayChg();
			s._updateFrame = true;
		}
		private updateFrameView():void
		{var s = this;
			var stX:number=0,endX:number=0,stY:number=0,endY:number=0;
			if(s.startRow > s.endRow)
			{
				s._nowStartRow = s.endRow;
				s._nowEndRow = s.startRow;
			}
			else
			{
				s._nowStartRow = s.startRow;
				s._nowEndRow = s.endRow;
			}
			if(s.startCol > s.endCol)
			{
				s._nowStartCol = s.endCol;
				s._nowEndCol = s.startCol;
			}
			else
			{
				s._nowStartCol = s.startCol;
				s._nowEndCol = s.endCol;
			}
			stX = s.owner.colsData[s._nowStartCol].posX;
			endX = s.owner.colsData[s._nowEndCol].posX + s.owner.colsData[s._nowEndCol].width;
			stY = s.owner.rowsData[s._nowStartRow].posY;
			endY = s.owner.rowsData[s._nowEndRow].posY + s.owner.rowsData[s._nowEndRow].height;
			s._selStartX = stX - s.excel.posX;
			s._selStartY = stY - s.excel.posY;
			s._selWidth = endX - stX;
			s._selHeight = endY - stY;
			s.drawBg(s._selStartX,s._selStartY, s._selWidth, s._selHeight);
		}
		/**选框手柄*/
		public get operHand():GYSprite
		{var s = this;
			return s._operHand;
		}
		/**选框起始行*/
		public get nowStartRow():number
		{var s = this;
			return s._nowStartRow;
		}
		/**选框起始列*/
		public get nowStartCol():number
		{var s = this;
			return s._nowStartCol;
		}
		/**选框结束行*/
		public get nowEndRow():number
		{var s = this;
			return s._nowEndRow;
		}
		/**选框结束列*/
		public get nowEndCol():number
		{var s = this;
			return s._nowEndCol;
		}

		public get drager():DraggerHandle
		{var s = this;
			return s._drager;
		}

	}
}