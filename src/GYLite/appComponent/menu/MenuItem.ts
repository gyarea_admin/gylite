module GYLite
{
				
	export class MenuItem extends GYLinkButton implements IItemRender
	{
		protected _data:any;
		protected _owner:IList;
		protected _itemIndex:number;
		public constructor(skin:any=null)
		{
			super(skin);
			var s= this;
			s._data = null;
			s._owner = null;
			s._itemIndex = -1;
			s.label = "";
			s.labelDisplay.verticalCenter = 2;
			s.height = 23;
			s.width = 100;
			s.rectHit = true;
		}
		/**设置数据*/
		public setData(val:any):void
		{
			let s = this;
			s._data=val;
			if(s._data==null)
			{
				s.visible = false;
				return;
			}
			s.visible = true;
			this.label = s._data.label;
		}
		/**获得数据*/
		public getData():any
		{
			let s = this;
			return s._data;
		}
		/**提取数据*/
		public toString():string
		{
			let s = this;
			if(s._data == null)
				return super.toString();
			return s._data.label;
		}
		public get owner():IList
		{
			let s = this;
			return s._owner;
		}
		public set owner(val:IList)
		{
			let s = this;
			s._owner = val;
		}
		public get itemIndex():number
		{
			let s = this;
			return s._itemIndex;
		}
		
		public set itemIndex(value:number)
		{
			let s = this;
			s._itemIndex = value;
		}
		public get col():number
		{
			return 0;
		}
		public set col(value:number)
		{
			
		}
		public get row():number
		{
			return 0;
		}
		public set row(value:number)
		{
			
		}
	}
}