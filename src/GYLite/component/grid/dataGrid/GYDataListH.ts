/**
 @author 迷途小羔羊
 2015.11.20
 */
module GYLite
{
								
	export class GYDataListH extends GYListH
	{
		protected _gridDataVec:GridData[];
		protected _maxGridWidth:number;
		protected _contentGrp:GYGroup;
		protected _lastEndBox:number;		
		protected _gridControler:boolean;
		
		/**水平List组件 格子宽度自定义(格子宽度固定建议使用GYListH) 项数根据宽度自适应
		 * @param boxCount 初始化项数据 0初始化 非0 外部赋值
		 * @param getGridFunc():IItemRender 返回自定义格子对象的方法 
		 * @param setGridFunc(IItemRender, any) 设置格子数据的方法*/
		public constructor(boxCount:number, getGridFunc:Function=null, getThisObject:any=null, setGridFunc:Function=null, setThisObject:any=null)
		{
			super(0, getGridFunc, getThisObject, setGridFunc, setThisObject);
			var s = this;
			s._gridControler = (boxCount == 0);
			if(s._gridControler)							
				s._gridDataVec = new Array<GridData>();				
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();			
			s._maxGridWidth=0;			
			s._lastEndBox=-1;			
			s._contentGrp = new GYGroup;
			s._contentGrp.canDrag = false;
			s._contentGrp.clipAndEnableScrolling = true;			
			s.addElement(s._contentGrp);
			if(s._boxH == 0)s._boxH = GYDataListH.default_boxH;
			if(s._boxW == 0)s._boxW = GYDataListH.default_boxW;
		}
		/**是否格子数据的控制器（当GYDataList的gridDataVec引用外部数据源，则当前列表不是gridDataVec的控制器，则其不能修改此数据，否则会造成无法与外部数据同步）*/
		public get gridControler():boolean
		{
			return this._gridControler;
		}
		public set gridDataVec(val:GridData[])
		{
			let s = this;
			s._gridDataVec = val;
		}
		/**每列的格子信息*/
		public get gridDataVec():GridData[]
		{
			let s = this;
			return s._gridDataVec;
		}
		public getGridData(val:number):GridData
		{
			let s = this;
			if(val < s._gridDataVec.length)
				return s._gridDataVec[val];
			var i:number,len:number;
			var gridData:GridData,oldGridData:GridData;
			len = val + 1;
			if(s._gridDataVec.length > 0)
				oldGridData = s._gridDataVec[s._gridDataVec.length - 1];
			for(i=s._gridDataVec.length;i<len;++i)
			{
				gridData = s._gridDataVec[i] = new GridData;
				gridData.index = i;
				gridData.width = s._boxW;
				gridData.height = s._boxH;
				if(s._boxW > s._maxGridWidth)
					s._maxGridWidth= s._boxW;
				if(oldGridData)
				{
					gridData.posX = oldGridData.posX + oldGridData.width;
				}
				oldGridData = gridData;
			}
			return gridData;
		}
		/**@inheritDoc*/
		public scrollToIndex(val:number)
		{
			let s = this;
			var gridData:GridData;
			gridData = s._gridDataVec[val];
			s.hScroller.position = gridData.posX;
		}
		/**@inheritDoc*/
		public scrollToEndIndex(val:number):void
		{
			let s = this;
			var gridData:GridData;
			gridData = s._gridDataVec[val];
			s.hScroller.position = gridData.posX - s._innerWidth + gridData.width;
		}
		/**@inheritDoc*/
		public updateGrid():void
		{
			let s = this;
			var gridData:GridData;
			if(s._dataProvider && s._dataProvider.length > 0)
			{
				gridData = s.getGridData(s._dataProvider.length - 1);
				s._max=gridData.posX + gridData.width;//Math.max(gridData.posX + gridData.width,s._boxs * s._boxW);
			}
			else
				s._max = s._contentGrp.baseWidth;
			s.hScroller.maximum = s._max-s._contentGrp.baseWidth;
			s.hScroller.barPercent = s._contentGrp.baseWidth/s._max;
			s.scrollerPolicy = s._scrollerPolicy;
			s.vScrollChange();
		}
		/**@inheritDoc*/
		public get dataProvider():Array<any>
		{
			let s = this;
			return s._dataProvider;
		}
		public set dataProvider(val:Array<any>)
		{
			let s = this;
			if(!s._keepSelected)
			{
				s._selectedData = null;
				s._selectIndex = -1;
				if(s._canSelectNum > 1)
					s._selectList.length = 0;
			}
			s._dataProvider=(val?val:[]);			
			var endGridData:GridData;						
			s.clearGridData();
			if(s._dataProvider && s._dataProvider.length > 0)
			{
				endGridData = s.getGridData(s._dataProvider.length - 1);
				s._max=endGridData.posX + endGridData.width;
			}
			else
				s._max = s._contentGrp.baseWidth;
			s.hScroller.maximum = s._max-s._contentGrp.baseWidth;
			s.hScroller.barPercent = s._contentGrp.baseWidth/s._max;
			s.scrollerPolicy = s._scrollerPolicy;
			if(s.hScroller.position > s.hScroller.maximum)
				s.hScroller.position = s.hScroller.maximum;			
			
			s.vScrollChange(null, true);
		}
		private clearGridData()
		{
			let s = this;
			if(s._gridControler)
				s._gridDataVec.length = 0;
		}
		protected vScrollChange(e:GYScrollerEvent=null, update:boolean = false, updatePos:boolean = true):void
		{
			let s = this;
			if(s._dataProvider==null)
				return;
			var val:number;
			var w:number=0,offsetX:number=0,i:number=0,len:number=0,len2:number=0;
			var nowLine:number=0,nowEndLine:number=0,iLine:number=0,posX:number=0,maxSize:number=0,oldBoxs:number=0;
			var grid:IItemRender;
			var nowGridData:GridData;
			var isShift:boolean;
			var n:number=0,nLen:number=0,m:number=0,mLen:number=0;
			val = s.hScroller.position;
			
			nowGridData = s.getNowGridData(val);
			if(nowGridData)
			{
				offsetX = val - nowGridData.posX;
				nowLine = nowGridData.index;
			}
			maxSize = s._contentGrp.baseWidth;
			posX = -offsetX;
			while(posX < maxSize)
			{
				iLine = nowLine+i;
				w = s.getGridData(iLine).width;
				posX += w;
				++i;
			}
			nowEndLine = nowLine + i - 1;
			oldBoxs = s._boxs;
			s._boxs = nowEndLine - nowLine + 1;
			if(nowLine > s._lastBox)
			{
				if(nowLine <= s._lastEndBox)
				{
					len = nowLine - s._lastBox;
					for(i=0;i<len;++i)
					{
						grid = s._grids[0];
						s._grids.splice(oldBoxs, 0,grid);
						s._grids.shift();
					}
				}
			}
			else if(nowLine < s._lastBox)
			{
				var tempInd:number;
				if(nowEndLine >= s._lastBox)
				{
					len = s._lastBox - nowLine;
					isShift = true;
					n = 0;
					nLen = s._lastEndBox - nowEndLine;
					if(nLen < 0)
					{
						m = s._lastEndBox - nowLine + 1;
						mLen =  m-nLen;
						nLen = len;
					}
					else
					{
						mLen = 0;
						nLen = len  - nLen;
					}
					for(i=0;i<len;++i)
					{
						tempInd = nowEndLine - s._lastBox + 1 + i;
						if(tempInd < s._grids.length)
						{
							grid = s._grids[tempInd];
							s._grids.splice(tempInd,1);
							s._grids.unshift(grid);
						}
						else
							s._grids.unshift(s.getGrid());
					}
				}
			}
			
			if(oldBoxs > s._boxs)
			{
				for(i=s._boxs;i < oldBoxs; ++i)
				{
					grid = s._grids[i];
					s._contentGrp.removeElement(grid);
				}
			}
			else
			{
				if(isShift)
				{
					for(i = n;i < nLen; ++i)
					{
						grid = s._grids[i];
						s._contentGrp.addElement(grid);
					}
					for(i = m;i < mLen; ++i)
					{
						if(i < s._grids.length)
						{
							grid = s._grids[i];
							s._contentGrp.addElement(grid);
						}
						else
							s._grids.push(s._contentGrp.addElement(s.getGrid()));
					}
				}
				else
				{
					for(i=oldBoxs;i < s._boxs; ++i)
					{
						if(i < s._grids.length)
						{
							grid = s._grids[i];
							s._contentGrp.addElement(grid);							
						}
						else
							s._grids.push(s._contentGrp.addElement(s.getGrid()));
					}
				}
			}
			
			posX = 0;
			for(i=0;i<s._boxs;++i)
			{
				grid = s._grids[i];
				iLine = nowLine + i;
				grid.itemIndex = iLine;
				nowGridData = s._gridDataVec[iLine];
				if(s._lastBox > iLine || s._lastEndBox < iLine || update)
				{
					s.setGrid(grid,s._dataProvider[iLine]);
				}				
				grid.width = nowGridData.width==nowGridData.width?nowGridData.width:s._boxW;
				grid.height = nowGridData.height==nowGridData.height?nowGridData.height:s._boxH;
				if(updatePos)
				{
					grid.x = posX - offsetX;
					posX += grid.width;
					s._contentGrp.checkOutSize(grid,false);
				}				
			}
			s._lastBox = nowLine;
			s._lastEndBox = nowEndLine;
		}
		protected getNowGridData(val:number):GridData
		{
			let s = this;
			var st:number = (val / s._maxGridWidth >> 0);
			var i:number,len:number;
			var gridData:GridData,oldGridData:GridData;
			len = s._gridDataVec.length;
			for(i=0;i<len;++i)
			{
				gridData = s._gridDataVec[i];
				if(gridData.posX > val)
				{
					if(oldGridData)
						return oldGridData;
					return gridData;
				}
				oldGridData= gridData;
			}
			return gridData;
		}
		
		/**设置某行高度
		 * @param ind 行索引 val 高度值
		 * */
		public setGridWidth(ind:number,val:number):void
		{
			let s = this;
			if(ind >= s._gridDataVec.length)
			{
				throw(new Error("索引"+ind+"超出数组长度"))				
			}
			var gridData:GridData = s._gridDataVec[ind];
			if(gridData.width == val)
				return;
			if(s._maxGridWidth < val)
				s._maxGridWidth = val;
			gridData.width = val;
			// gridData.isSet = true;
			s.invalidGridData();
		}
		/**@inheritDoc*/
		public updateGridData():void
		{
			let s = this;
			var i:number,len:number;
			var gridData:GridData,oldGridData:GridData;
			len = s._gridDataVec.length;
			for(i=0;i<len;++i)
			{
				gridData = s._gridDataVec[i];
				if(oldGridData)
					gridData.posX = oldGridData.posX + oldGridData.width;
				oldGridData = gridData;
			}
			s.invalidGrids();
		}
		public set width(val:number)
		{
			let s = this;
			if(s._innerWidth == val)
				return;
			s._contentGrp.width = s._innerWidth = s.hScroller.width = s.group_width = val;
			s.invalidGrids();
		}		
		public get width():number
		{
			let s = this;			
			return egret.superGetter(GYDataListH, this, "width");
		}
		public set height(val:number)
		{
			let s = this;
			if(s._innerHeight == val)
				return;
			s._innerHeight = s._contentGrp.height = val;
			s.group_height= s._innerHeight + (s.hScroller.parent?s.hScroller.height:0);
		}
		public get height():number
		{
			let s = this;
			return egret.superGetter(GYDataListH, this, "height");
		}
		
		public static default_boxH:number = 40;
		public static default_boxW:number = 40;
	}
}