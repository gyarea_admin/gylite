declare module GYLite {
    class TextAreaSkin extends GYSkin implements IGYSkin {
        protected _background: egret.Texture;
        protected _backScaleShape: GYScaleSprite;
        protected _rect: Scale9GridRect;
        constructor(back?: egret.Texture, rect?: Scale9GridRect);
        release(): void;
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        set width(value: number);
        set height(value: number);
        get width(): number;
        get height(): number;
        set background(val: egret.Texture);
        set scale9GridRect(val: Scale9GridRect);
        clone(): IGYSkin;
    }
}
