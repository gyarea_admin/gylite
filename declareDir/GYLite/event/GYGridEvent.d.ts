declare module GYLite {
    class GYGridEvent extends GYEvent {
        static ROWGRID_DATACHANGE: string;
        static COLGRID_DATACHANGE: string;
        static COLGRID_NUMCHANGE: string;
        static ROWGRID_NUMCHANGE: string;
        constructor(type: string, bubbles?: boolean, cancelable?: boolean);
    }
}
