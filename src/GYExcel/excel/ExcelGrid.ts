module GYLite
{
																		
									
	export class ExcelGrid extends ItemRender implements IReceiver,IItemRender
	{
		protected _text:GYText;
		// protected _img:GYDrawImage;
		public excel:GYExcel;
		public selFrame:ExcelFrame;
	
		public constructor()
		{
			super();
			var s = this;
			s.initComponent();
		}
		protected initComponent():void
		{var s = this;
			s.width = 100;
			s.height = 30;
			s._text = new GYText(true);
			s._text.width = s.width;
			s._text.align = "center";
			s._text.touchEnabled = false;
			s._text.color = 0;
			// s._text.selectable = false;
			s._text.verticalCenter = 5;
			// s._text.useInvalidText = false;
			// s._text.useInvalidDisplay = false;			
			// s._text.embedFonts = false;			
			s._text.validFormat();		
			s.addElement(s._text);
			// s._img = new GYDrawImage;
			// s._img.touchEnabled = s._img.touchChildren = false;
			// s._img.verticalCenter = 5;
			// s.addElement(s._img);			
			s.addEventListener(GYViewEvent.VIEWCHANGE, s.widthAndHeightChange,s);	
			s.addEventListener(GYLite.GYTouchEvent.DOUBLE_CLICK,s.clkGrid, s);
			
			s.addEventListener(MouseEvent.ROLL_OVER, s.rollOver,s);
			s.touchEnabled = true;
			
		}
		
		protected drawBg(c:number=0xcccccc):void
		{var s = this;
			s.graphics.clear();
			s.graphics.beginFill(0xffffff,1);
			s.graphics.drawRect(0,0,s.width,s.height);
			s.graphics.beginFill(c,1);
			s.graphics.drawRect(1,1,s.width-2,1);
			s.graphics.drawRect(1,2,1,s.height - 2);
			s.graphics.drawRect(s.width-2,2,1,s.height - 2);
			s.graphics.drawRect(1,s.height-2,s.width-2,1);
			
			s.graphics.endFill();
		}
		public set selected(b:boolean)
		{var s = this;
			egret.superSetter(ExcelGrid, s, "selected", b);
			// super.selected = b;
			s.drawBg(b?0x00ff00:0xcccccc);
		}
		public get selected():boolean
		{var s = this;
			return egret.superGetter(ExcelGrid, s, "selected");
		}
		protected widthAndHeightChange(e:GYViewEvent):void
		{var s = this;
			s._text.width = s.width;
			// s._img.draw(s._text);			
			s.drawBg(s._selected?0x00ff00:0xcccccc);
		}
		/**设置格子信息 */
		public setData(obj:any):void
		{var s = this;
			s._data = obj as ExcelData;
			if(!obj || obj.value == null)
				s._text.text ="";				
			else
				s._text.text =String(obj.value);
				
			// s._img.draw(s._text);
			s.width = 100;
			s.height = 30;
		}
		protected clkGrid(e:egret.TouchEvent):void
		{var s = this;
			GYShowInput.show(s._text, this).addEventListener(egret.FocusEvent.FOCUS_OUT, s.inputDeactive,s);
			// s._img.visible = false;
			s._text.visible = false;
		}
		protected rollOver(e:MouseEvent):void
		{var s = this;
			if(GYSprite.isStageDown(s.selFrame.operHand))
				s.excel.selCalEndGrid(this);
		}
		protected inputDeactive(e:egret.Event):void
		{var s = this;
			// s._img.draw(s._text);
			// s._img.visible = true;
			s._text.visible = true;
			if(s._text.text === "0" || Number(s._text.text) == 0)
				s._data.value = s._text.text;
			else
				s._data.value = Number(s._text.text);
		}
		public receiveData(val:IDragger):void
		{var s = this;
			var frame:ExcelSelFrame = <any>val;
			if(frame == null)
				return;
			s.excel.moveData(frame.owner.nowStartRow, frame.owner.nowEndRow, frame.owner.nowStartCol, frame.owner.nowEndCol, s._data.row, s._data.col);
		}
	}
}