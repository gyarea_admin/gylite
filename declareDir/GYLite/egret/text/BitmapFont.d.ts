declare namespace egret {
    /**
     * Bitmap font, texture set of a font. It is generally used as the value of the BitmapText.font attribute.
     * @see http://bbs.egret-labs.org/thread-918-1-1.html TextureMerger
     * @see http://bbs.egret-labs.org/forum.php?mod=viewthread&tid=251 Text(Containing the specific usage of the bitmap font )
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/text/BitmapFont.ts
     * @language en_US
     */
    /**
     * 位图字体,是一个字体的纹理集，通常作为BitmapText.font属性的值。
     * @see http://bbs.egret-labs.org/thread-918-1-1.html TextureMerger
     * @see http://bbs.egret-labs.org/forum.php?mod=viewthread&tid=251 文本(含位图字体具体用法)
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/text/BitmapFont.ts
     * @language zh_CN
     */
    class BitmapFont extends SpriteSheet {
        /**
         * Create an egret.BitmapFont object
         * @param texture {egret.Texture} Texture set that use TextureMerger create
         * @param config {any} Configure data that use TextureMerger create
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 创建一个 egret.BitmapFont 对象
         * @param texture {egret.Texture} 使用TextureMerger生成的纹理集
         * @param config {any} 使用TextureMerger生成的配置数据
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        constructor(texture: Texture, config: any);
        /**
         * @private
         */
        private charList;
        /**
         * Obtain corresponding texture through the name attribute
         * @param name {string} name Attribute
         * @returns {egret.Texture}
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 通过 name 属性获取对应纹理
         * @param name {string} name属性
         * @returns {egret.Texture}
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        getTexture(name: string): Texture;
        /**
         * @private
         */
        getConfig(name: string, key: string): number;
        /**
         * @private
         */
        private firstCharHeight;
        /**
         * @private
         *
         * @returns
         */
        _getFirstCharHeight(): number;
        /**
         * @private
         *
         * @param fntText
         * @returns
         */
        private parseConfig;
        /**
         * @private
         *
         * @param configText
         * @param key
         * @returns
         */
        private getConfigByKey;
    }
}
