/**
@author 迷途小羔羊
2022.09.27
*/
declare module GYLite {
    class AtlasRender implements IResource {
        static defaultGap: number;
        static defaultAtlasName: string;
        static _instance: AtlasRender;
        static getInstance(): AtlasRender;
        /**获取batch纹理，如果存在，如果不存在则尝试进行合批创建*/
        static getBatchTexture(tex: egret.Texture, display: IBatch, gap?: number): egret.Texture;
        $disposed: boolean;
        protected _rectAtlasMgr: AtlasManager;
        protected _atlasDict: any;
        protected _webglRenderContext: egret.sys.RenderContext;
        /**
         * @param webglRenderContext 绘图上下文
         * @param maxSize 加入图集的最大尺寸限制
         * @param gap 小图填充之间的间隙
        */
        constructor(webglRenderContext: egret.sys.RenderContext, maxSize: number, gap: number);
        get atlasNum(): number;
        getRectAtlasMgr(): AtlasManager;
        /**移除合批到图集的小图
         * @param batchInfo 合批记录
        */
        removeBatch(batchInfo: BatchInfo): void;
        /**源小图合批到图集，会对tex进行合批操作，由此tex会产生batchInfo属性，代表已经合批
         * @param tex 源小图纹理
         * @param display 申请合批的显示对象(需实现IBatch接口)
         * @param releaseFree 是否自动释放多余空间以便找到空间合批 默认true
         * @param gap 小图间的空隙 默认1
         * @return 成功合批返回合批信息 BatchInfo，否则返回null
        */
        addBatch(tex: egret.Texture, display: IBatch, releaseFree?: boolean, gap?: number): BatchInfo;
        /**创建一张图集
         * @param width 宽度
         * @param height 高度
         * @param atlasName 图集名称
         * @param createSpriteSheet 图集配置，默认true，则创建空图集，当读取外部图集则认为是非空，可以设置为false，再调用AtlasRender.inputSpriteSheet方法导入配置
         * @param source 图集数据源
        */
        createAtlas(width: number, height: number, atlasName: string, source?: HTMLCanvasElement | HTMLImageElement | HTMLVideoElement, createSpriteSheet?: boolean): {
            atlas: Atlas;
            atlasRect: AtlasRect;
            error: SysError;
        };
        /**移除图集纹理
         * @param atlasName 图集名称
        */
        removeAtlas(atlasName: string): void;
        /**导入图集小图配置
         * @param spriteSheet 图集配置
         * @param atlasId 图集id
        */
        inputSpriteSheet(spriteSheet: ResObject, atlasId: number): void;
        dispose(): void;
        get disposed(): boolean;
        /**获取最近产生的一张名为atlasName的图集*/
        getLastAtlas(atlasName: string): Atlas;
    }
    class Point {
        x: number;
        y: number;
        constructor(x: number, y: number);
    }
    class Line {
        static minusX(srcL: Line, minusL: Line, result?: Line[]): Line[];
        a: Point;
        b: Point;
        private _borderW;
        private _borderH;
        constructor(p1: Point, p2: Point);
        setBorder(w: number, h: number): void;
        get right_b_x(): number;
        get right_b_y(): number;
        get right_a_x(): number;
        get right_a_y(): number;
        get bottom_a_x(): number;
        get bottom_a_y(): number;
        get bottom_b_x(): number;
        get bottom_b_y(): number;
        get lengthX(): number;
        get height(): number;
        convertToAtlasRect(parent: AtlasRect): AtlasRect;
    }
}
