declare namespace egret.sys {
    /**
     * @private
     * 填充路径
     */
    class FillPath extends Path2D {
        constructor();
        /**
         * 填充颜色
         */
        fillColor: number;
        /**
         * 填充透明度
         */
        fillAlpha: number;
    }
}
