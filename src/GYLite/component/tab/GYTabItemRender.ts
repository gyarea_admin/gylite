module GYLite
{
									
	export class GYTabItemRender extends GYButton implements IItemRender
	{
		protected _owner:IList;
		protected _data:TabData;
		protected _itemIndex:number;
		protected _col:number;
		protected _row:number;
		public constructor(skin:any = null)
		{
			super(skin);
			var s = this;
			s._owner=null;
			s._data=null;
			s._itemIndex=NaN;
			s._col=NaN;
			s._row=NaN;
		}
		public set selected(val:boolean)
		{
			let s = this;
			egret.superSetter(GYTabItemRender, s, "selected", val);			
			if(s._data != null)
				s._data.selected = (s.owner.selectedData == s._data);
		}
		public setData(obj:any)
		{
			let s = this;
			s._data = obj as TabData;
			if(s._data == null)
			{
				s.visible = false;
				return;
			}
			s.visible = true;
			s._data.selected = (s.owner.selectedData == s._data);
			s.label = s._data.label;
		}
		
		public getData():any
		{
			let s = this;
			return s._data;
		}
		
		public set owner(val:IList)
		{
			let s = this;
			s._owner = val;
			s.width = (s._owner as GYTab).boxW;
			s.height = (s._owner as GYTab).boxH;
		}
		
		public get owner():IList
		{
			let s = this;
			return s._owner;
		}
		
		public get itemIndex():number
		{
			let s = this;
			return s._itemIndex;
		}
		
		public set itemIndex(value:number)
		{
			let s = this;
			s._itemIndex = value;
		}
		public get col():number
		{
			let s = this;
			return s._col;
		}
		public set col(value:number)
		{
			let s = this;
			s._col = value;
		}
		public get row():number
		{
			let s = this;
			return s._row;
		}
		public set row(value:number)
		{
			let s = this;
			s._row = value;
		}
		protected getThemeSkin():IGYSkin
		{
			let s = this;
			return GYSprite.skinTheme.GetTabButtonSkin();
		}
	}
}