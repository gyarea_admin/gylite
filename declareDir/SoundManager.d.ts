declare class SoundManager {
    private static _instance;
    soundDict: any;
    static get instance(): SoundManager;
    private _loaddingDict;
    private _curBGM;
    private _paused;
    constructor();
    getDefaultBgm(): string;
    playBGM(url: string): void;
    closeBGM(): void;
    /**播放声音
     * @param url 声音路径
     * @param startTime 起始时间
     * @param loops 循环次数 0为永久循环
     * @param compF 播放完成的回调
     * @param thisObject 回调指向
     * @param type 声音类型 参考SoundData 常量
     * @param isHttp 是否网络声音
    */
    play(url: string, startTime?: number, loops?: number, compF?: Function, thisObject?: any, type?: number): void;
    private loadSoundComp;
    private bytesLoaded;
    /**播放字节流声音
     * @param bytes 字节数组ArrayBuffer
     * @param startTime 开始时间
     * @param loops 循环次数 0 永久循环
     * @param compF 结束回调函数
     * @param thisObject this指向
     * @param type 声音类型
     * @param url 声音地址
    */
    playBytes(bytes: ArrayBuffer, startTime?: number, loops?: number, compF?: Function, thisObject?: any, type?: number, url?: string): void;
    private bytesSoundLoaded;
    playBySoundData(soundObj: SoundData, startTime?: number, loops?: number, compF?: Function, thisObject?: any): void;
    /**停止声音，清除字典缓存
     * @param url 声音的url，为null则是停止所有
     * @param soundType 声音类型
     * @param disposeSrc 销毁声音源数据
    */
    stop(url?: string, soundType?: number, disposeSrc?: boolean): void;
    /**暂停所有声音*/
    pause(): void;
    /**恢复所有声音*/
    resume(): void;
    dispose(url?: string, disposeSrc?: boolean): void;
}
declare class SoundData implements GYLite.IPoolObject {
    sound: any;
    compFunc: any;
    thisObject: any;
    private _channel;
    private _tempChannels;
    timeId: number;
    soundType: number;
    position: number;
    loops: number;
    url: string;
    paused: boolean;
    static COMMON_SOUND: number;
    static BACKGROUND_SOUND: number;
    soundComplete(e?: egret.Event): void;
    get channel(): egret.SoundChannel;
    set channel(val: egret.SoundChannel);
    clear(): void;
    inPool: boolean;
    outPoolInit(): void;
    /**销毁声音，会把声音源数据销毁*/
    dispose(): void;
}
