declare module GYLite {
    /**IReceiver是一个接收接口，实现此接口，可接收来自IDragger的数据* */
    interface IReceiver extends IGYDisplay {
        receiveData(val: IDragger): void;
    }
}
