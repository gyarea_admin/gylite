declare namespace egret.sys {
    /**
     * @private
     * 渐变填充路径
     */
    class GradientFillPath extends Path2D {
        constructor();
        gradientType: string;
        colors: number[];
        alphas: number[];
        ratios: number[];
        matrix: Matrix;
    }
}
