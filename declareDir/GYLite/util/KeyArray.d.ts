declare module GYLite {
    interface IKeyObject {
        value: any;
        index: number;
    }
    /**key数组，是一种无序数组，需要指定一个唯一key并且相同key的对象加入到数组会覆盖原来的key的value，此数组无序，无法保证数组内的元素顺序，可通过指定的唯一key访问数组元素，也可以通过下标访问*/
    export class KeyArray {
        list: any[];
        dict: {
            [key: string]: IKeyObject;
        };
        private _key;
        /**数组元素指定的唯一key*/
        constructor(key: string);
        push(...items: any[]): number;
        /**注意,由于此对象是无序列表，所以第三个参数items只会在末尾添加元素，而并不从删除的位置插入*/
        splice(start: number, deleteCount: number, ...items: any[]): any[];
        sort(func: (a: any, b: any) => number): void;
        pop(): any;
        shift(): any;
        /**移除指定对象*/
        removeObject(obj: any): void;
        /**移除指定索引上的对象
         * (注意，每次移除对象，会获取列表最后一个对象，填充到移除的索引位置上，以便保证其他对象索引不变，但是最后一个对象的会被改变，所以请勿循环移除索引对象，因为此列表本身是无序的)
         *
        */
        remove(index: number): any;
        unshift(...items: any[]): number;
        /**根据key交换数组内两个对象的位置**/
        swapByKey(keyA: string | number, keyB: string | number): void;
        /**交换数组内两个对象的位置**/
        swap(objA: any, objB: any): void;
        /**是否存在指定key的对象*/
        hasKey(key: string | number): boolean;
        /**设置指定key上的对象*/
        setByKey(key: string | number, value: any): void;
        /**根据key删除对象*/
        deleteByKey(key: string): void;
        /**获取指定key上的对象*/
        getByKey(key: string | number): any;
        /**设置指定索引上的对象*/
        setByIndex(index: number, value: any): void;
        set length(val: number);
        get length(): number;
    }
    export {};
}
