module GYLite {
	export class LoadUI extends GYSprite{		
		private gyProgressBar:GYProgressBar;
		public constructor() {
			super();
			var s = this;
			s.gyProgressBar = new GYProgressBar();
			s.gyProgressBar.mode = ScaleMode.SCALE;			
			s.gyProgressBar.max = 100;
			s.gyProgressBar.value = 0;
			s.gyProgressBar.barX = 0;
			s.gyProgressBar.barY = 0;
			s.gyProgressBar.barMax = 283;
			s.gyProgressBar.width = 283;
			s.gyProgressBar.height = 25;
			s.gyProgressBar.moveBack = true;
			s.gyProgressBar.label = "0%";			
			s.gyProgressBar.labelDisplay.width = s.gyProgressBar.width;
			s.gyProgressBar.labelDisplay.color = ColorConst.fontColor;
			s.gyProgressBar.labelDisplay.y = s.gyProgressBar.height - s.gyProgressBar.labelDisplay.textHeight >> 1;
			s.gyProgressBar.labelDisplay.align = "center";
			// s.gyProgressBar.addEventListener(GYEvent.VALUE_COMMIT, s.barChangeFunc,s);			
			s.addElement(s.gyProgressBar);			
		}
		public setProg(val:number, max:number,lab:string = "努力加载中... "):void
		{
			var s = this;
			s.gyProgressBar.value = val;
			s.gyProgressBar.max = max;
			s.gyProgressBar.label = lab +  (s.gyProgressBar.value / s.gyProgressBar.max * 100 >> 0) + "%";
		}
		// private barChangeFunc(e:GYEvent):void
		// {var s = this;
		// 	s.gyProgressBar.label = (s.gyProgressBar.value / s.gyProgressBar.max * 100 >> 0) + "%";
		// }
		public show(pr:any)
		{var s = this;
			if(CommonUtil.GYIs(pr, egret.Stage))
			{
				pr.addChild(s);
				s.gyProgressBar.x = pr.stageWidth - s.gyProgressBar.width >> 1;
				s.gyProgressBar.y = pr.stageHeight - s.gyProgressBar.height >> 1;
			}				
			else
			{
				pr.addElement(s);
				s.gyProgressBar.x = pr.width - s.gyProgressBar.width >> 1;
				s.gyProgressBar.y = pr.height - s.gyProgressBar.height >> 1;
			}
				
		}
		public hide()
		{var s = this;
			if(s.parent)
			{
				if(CommonUtil.GYIs(s.parent, egret.Stage))
					s.parent.removeChild(s);
				else
					(<any>s.parent).removeElement(s);
			}			
		}
		private static _instance:LoadUI;
		public static getInstance():LoadUI
		{var s = this;
			s._instance = s._instance || new LoadUI;
			return s._instance;
		}
	}
}