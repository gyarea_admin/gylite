declare module GYLite {
    class MenuSkin extends GYSkin implements IMenuSkin {
        protected _background: egret.Texture;
        protected _backScaleShape: GYScaleSprite;
        protected _rect: Scale9GridRect;
        protected _list: GYLite.GYListV;
        constructor(back?: egret.Texture | GYScaleSprite, rect?: Scale9GridRect, list?: GYLite.GYListV);
        release(): void;
        set hostComponent(val: GYSprite);
        get hostComponent(): GYSprite;
        set background(val: egret.Texture);
        get background(): egret.Texture;
        set backgroundImage(val: GYScaleSprite);
        get backgroundImage(): GYScaleSprite;
        set scale9GridRect(val: Scale9GridRect);
        get scale9GridRect(): Scale9GridRect;
        get list(): GYLite.GYListV;
        set list(val: GYLite.GYListV);
        copy(skin: IGYSkin): void;
        clone(): IGYSkin;
    }
}
