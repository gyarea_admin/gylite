declare module GYLite {
    type Route = {
        /**起点索引*/ stInd: number;
        /**终点索引*/ endInd: number;
        /**路线*/ pts: number[];
        /**起点到路线的距离*/ stMinDis: number;
        /**终点到路线的距离*/ endMinDis: number;
    };
    class PositionUtil {
        static _pt: egret.Point;
        constructor();
        /**范围限制
         * @param value 检测值
         * @param min 最小 默认NaN 不检测
         * @param max 最大 默认NaN 不检测
         * @param return 超过范围返回范围极值，否则返回当前值
        */
        static rangeRestrct(value: number, min?: number, max?: number): number;
        /**求过点cx,cy的直线在直线l(ax,ay,bx,by)上的垂点
         * @return 垂点坐标
         * */
        static verticalPoint(ax: number, ay: number, bx: number, by: number, cx: number, cy: number): egret.Point;
        /**判断3个点是顺时针，逆时针，还是在一条直线上面
         * @return 小于0 逆时针 大于0 顺时针 等于0 直线 (注意2d坐标系是y轴翻转的，所以图形的顶点顺序应该跟数学计算相反)
         * */
        static clockwise(x1: number, y1: number, x2: number, y2: number, x3: number, y3: number): number;
        /**判断路径是顺时针，逆时针，还是在一条直线上面
         * @return 小于0 逆时针 大于0 顺时针 等于0 直线 (注意2d坐标系是y轴翻转的，所以图形的顶点顺序应该跟数学计算相反，此处表达的结果为数学坐标系上表现的顺逆时针)
         * */
        static clockwisePath(pathArr: Array<any>): number;
        /**计算路径转过的角度
         * @return 路径转过的角度
         * */
        static anglePath(pathArr: Array<any>): number;
        /**判断直线AB是否与直线CD相交*/
        static lineIntersectSide(ax: number, ay: number, bx: number, by: number, cx: number, cy: number, dx: number, dy: number): Boolean;
        /**检测图形是否相交
         * @param shapeA [x1,y1,x2,y2……]
         * @param shapeB [x1,y1,x2,y2……]
        */
        static isShapeInsert(shapeA: number[], shapeB: number[]): boolean;
        /**检测点是否在图形内*/
        static isPointInShape(tX: number, tY: number, arr: Array<number>): boolean;
        /**判断两条线段是否相交*/
        static intersect(ax: number, ay: number, bx: number, by: number, cx: number, cy: number, dx: number, dy: number): Boolean;
        /**转换坐标
         * @param toX 坐标X
         * @param toY 坐标Y
         * @param fromTarget 当前坐标的坐标系
         * @param toTarget 目标坐标的坐标系，为null则只转到全局坐标
         * */
        static convertPos(toX: number, toY: number, fromTarget: egret.Sprite, toTarget?: egret.Sprite): egret.Point;
        /**计算两点连线的线角度（弧度，正负pi）
         * @param aX 终点x
         * @param aY 终点y
         * @param aX 起点x
         * @param aY 起点y
         * */
        static calculateAngle(aX: number, aY: number, bX: number, bY: number): number;
        /**计算两点间xy轴距离和(曼哈顿距离)*/
        static calculateDistance3(aX: number, aY: number, bX: number, bY: number): number;
        /**计算两点间的距离的平方*/
        static calculateDistance2(aX: number, aY: number, bX: number, bY: number): number;
        /**计算两点间的距离*/
        static calculateDistance(aX: number, aY: number, bX: number, bY: number): number;
        /**数组转成数值数组*/
        static vectorToNumberArray(vec: Array<any>): Array<any>;
        /**数组转成整型数组*/
        static vectorToIntArray(vec: Array<any>): Array<any>;
        /**数组转成向下取整的整型数组*/
        static vectorToCeilArray(vec: Array<any>): Array<any>;
        /**生成椭圆形描点
         * @param rX X半径
         * @param rY Y半径
         * @param x 原点坐标X，默认0
         * @param y 原点坐标X，默认0
         * @param precision 精度（百分比）默认0.3，如果大于1，则认为是指定描点数量
        */
        static calRoundPoint(rX: number, rY: number, x?: number, y?: number, precision?: number): number[];
        /**生成矩形描点
         * @param rX 矩形宽度半径
         * @param rX 矩形高度半径
         * @param x 原点x，默认0
         * @param y 原点y，默认0
        */
        static calRectPoint(rX: number, rY: number, x?: number, y?: number): number[];
        /**绕点旋转
         * @param ox 绕x
         * @param oy 绕y
         * @param agl 角度
         * demo 你可以尝试以下代码，对图像进行绕点旋转动画
            let img:Laya.Image = new Laya.Image;
            let offsetX:number,offsetY:number;
            offsetX = offsetY = 0;
            img.x = 500;
            img.y = 500;
            img.skin="图片路径";
            Laya.stage.addChild(img);
            CommonUtil.addStageLoop(function(t:number):void{
                let pt:Laya.Point = PositionUtil.rotationXY(26,26,img.rotation*MathConst.ROTATION_ANGLE);
                img.x -= offsetX;
                img.y -= offsetY;
                offsetX = -pt.x;
                offsetY = -pt.y;
                img.x += offsetX;
                img.y += offsetY;
                img.rotation += 1;
            },self);
        */
        static rotationXY(ox: number, oy: number, agl: number): egret.Point;
        /**获取线段穿透的格子列表,返回数组[格子坐标y1,格子坐标x1,格子坐标y2,格子坐标x2……]，注意此处返回的是先y再x
         * test 342,702,602,402 - 垂直角度测试
         * test 342,702,342,402 - 垂直测试
         * test 342,702,602,602 - 水平角度测试
         * test 342,702,602,702 - 水平测试
         * @param ax 起点坐标x
         * @param ay 起点坐标y
         * @param bx 终点坐标x
         * @param by 终点坐标y
         * @param areaSize 格子大小 默认32
         * @param padding 扩展像素 默认0
        */
        static getAToBPosArray(ax: number, ay: number, bx: number, by: number, areaSize?: number, padding?: number): number[];
        /**根据图形顶点区域获取网格定点数组
         * @param arr 图形顶点数组[x0,y0,x1,y1……]
         * @param paddingX 间隔X
         * @param paddingY 间隔Y
         * @param staggerd 是否交错布局，默认false 二维格子布局，交错则每行间隔半个，每列错位半格
         *
        */
        static getGridPosByShape(arr: number[], paddingX?: number, paddingY?: number, staggerd?: boolean): number[];
        /**根据起点到终点，在指定的路线pts中，找到一段最短的途径路线
         * @param stX 起点x
         * @param stY 起点y
         * @param endX 终点x
         * @param endY 终点y
         * @param pts 路线
         * **/
        static getBestRoute(stX: number, stY: number, endX: number, endY: number, pts: number[]): Route;
        /**从起点索引stInd到终点索引截取一段数组的元素，stInd大于endInd则会自动反向顺序截取到新数组中
         * @param arr 数组
         * @param stInd 起始索引
         * @param endInd 结束索引
         * @param step 数组多少个元素作为一组，例如坐标[x1,y1,x2,y2……]则step为2，默认1
        */
        sliceArray(arr: any[], stInd: number, endInd: number, step?: number): any[];
    }
}
