declare var DEBUG: boolean;
declare var __reflect: any;
declare var __extends: any;
declare var global: Window & typeof globalThis;
declare var __global: {
    egret_stages: any[];
    FPS: any;
    ExternalInterface: any;
};
declare let __define: (o: any, p: any, g: any, s: any) => void;
declare namespace egret {
    type Nullable<T> = T | null;
    /**
     * The HashObject class is the base class for all objects in the Egret framework.The HashObject
     * class includes a hashCode property, which is a unique identification number of the instance.
     * @version Egret 2.4
     * @platform Web,Native
     * @language en_US
     */
    /**
     * Egret顶级对象。框架内所有对象的基类，为对象实例提供唯一的hashCode值。
     * @version Egret 2.4
     * @platform Web,Native
     * @language zh_CN
     */
    interface IHashObject {
        /**
         * a unique identification number assigned to this instance.
         * @version Egret 2.4
         * @platform Web,Native
         * @readOnly
         * @language en_US
         */
        /**
         * 返回此对象唯一的哈希值,用于唯一确定一个对象。hashCode为大于等于1的整数。
         * @version Egret 2.4
         * @platform Web,Native
         * @readOnly
         * @language zh_CN
         */
        hashCode: number;
    }
    /**
     * @private
     * 哈希计数
     */
    let $hashCount: number;
    /**
     * The HashObject class is the base class for all objects in the Egret framework.The HashObject
     * class includes a hashCode property, which is a unique identification number of the instance.
     * @version Egret 2.4
     * @platform Web,Native
     * @language en_US
     */
    /**
     * Egret顶级对象。框架内所有对象的基类，为对象实例提供唯一的hashCode值。
     * @version Egret 2.4
     * @platform Web,Native
     * @language zh_CN
     */
    class HashObject implements IHashObject {
        /**
         * Initializes a HashObject
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 创建一个 HashObject 对象
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        constructor();
        /**
         * @private
         */
        $hashCode: number;
        /**
         * a unique identification number assigned to this instance.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 返回此对象唯一的哈希值,用于唯一确定一个对象。hashCode为大于等于1的整数。
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        get hashCode(): number;
    }
}
declare namespace egret.wxgame {
    let version: string;
    /**
     * 运行环境是否为子域
     */
    let isSubContext: boolean;
    /**
     * 解决提交纹理异常临时方案
     */
    let preUploadTexture: boolean;
    /**微信小游戏共享画布*/
    let sharedCanvas: any;
    /**微信小游戏主域画布*/
    let canvas: any;
    /**微信小游戏sdk*/
    let wx: any;
    /**是否debug环境*/
    let debug: boolean;
}
