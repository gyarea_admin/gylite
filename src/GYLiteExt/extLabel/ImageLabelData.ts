module GYLite{
	export class ImageLabelData
	{
		public disposed:boolean;
		private _inPool:boolean;
		public bitmapData:egret.Texture;
		public width:number;
		public height:number;
		public offsetX:number;
		public offsetY:number;		
		public constructor()
		{
			let s = this;
			s.bitmapData = null;
			s.width = 0;
			s.height = 0;
			s.offsetX = 0;
			s.offsetY = 0;
			s._inPool = true;
		}
		public setTo(b:egret.Texture,w:number,h:number,ox:number,oy:number):void
		{let s = this;
			s.bitmapData = b;
			s.width = w;
			s.height = h;
			s.offsetX = ox;
			s.offsetY = oy;
		}
		
		public clear():void
		{
			if(this._inPool)return;
			PoolUtil.toPool(this, ImageLabelData);
		}
		public get inPool():boolean
		{
			return this._inPool;
		}
		
		public set inPool(val:boolean)
		{let s = this;
			s._inPool = val;
		}
		
		public outPoolInit():void
		{let s = this;
			s.bitmapData = null;
			s.width = 0;
			s.height = 0;
		}
		public dispose():void
		{
			
		}
	}
}	