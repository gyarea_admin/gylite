module GYLite
{
								
										
	/**富文本的文本单元 @see GYRichText*/
	export class RichLabel extends GYText implements IPoolObject
	{
		private _linkFun:Function;
		private _linkThisObject:any;
		/**@param textFormat 文本格式
		 * @param indent 首行缩进		 
		 * */
		public constructor()
		{
			super();
		}
		
		public clear():void
		{
			let s = this;
			if(s._inPool)return;
			PoolUtil.toPool(this, RichLabel);			
		}		
		public set height(val:number)
		{
			let s = this;			
			egret.superSetter(RichLabel, s, "height", val);
		}
		public get height():number
		{
			let s = this;			
			return s.textHeight;
		}
		public get linkFun():Function
		{
			let s = this;
			return s._linkFun;
		}
		
		public setLinkFun(value:Function, thisObject:any)
		{
			let s = this;
			s._linkFun = value;
			s._linkThisObject = thisObject;
		}

		public cutLastLine():string
		{
			let s = this;
			var lines:any = s.$getLinesArr();
			var obj:any = lines[lines.length-1];
			var eStr:string = s.getElementString(obj.elements)
			s.htmlText = s.htmlText.replace(s._lastLineText, "");
			//elements sytle width text
			return eStr;
		}
		public getElementString(e:any):string
		{
			let s = this;
			var i:number,len:number;
			var str:string="";			
			len = e.length;
			s._lastLineHtml= "";
			for(i=0;i<len;++i)
			{
				var obj:any = e[i];				
				str += obj.text;
				if(obj.style.tag!=null)
					s._lastLineHtml = s._lastLineHtml + obj.style.tag + obj.text + obj.style.tagEnd;
				else
					s._lastLineHtml = s._lastLineHtml + obj.text;				
			}
			s._lastLineText = str;
			return s._lastLineHtml;
		}
	
		public get inPool():boolean
		{
			let s = this;
			return s._inPool;
		}
		public set inPool(val:boolean)
		{
			let s = this;
			s._inPool = val;
		}
		public outPoolInit():void
		{
			let s = this;
			// s.indent = 0;
			// s.selectable=false;
			s.$setMultiline(true);
			s.$setWordWrap(true);
			s.$setX(0);
			s.$setY(0);
			
			// s.touchEnabled = true;
		}
		private _inPool:boolean;
		private _lastLineHtml:string;
		private _lastLineText:string;
	}
}