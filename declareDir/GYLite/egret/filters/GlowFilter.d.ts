declare namespace egret {
    /**
     * @class egret.GlowFilter
     * @classdesc
     * 使用 GlowFilter 类可以对显示对象应用发光效果。在投影滤镜的 distance 和 angle 属性设置为 0 时，发光滤镜与投影滤镜极为相似。
     * @extends egret.Filter
     * @version Egret 3.1.4
     * @platform Web,Native
     */
    class GlowFilter extends Filter {
        /**
         * @private
         */
        $red: number;
        /**
         * @private
         */
        $green: number;
        /**
         * @private
         */
        $blue: number;
        /**
         * Initializes a new GlowFilter instance.
         * @method egret.GlowFilter#constructor
         * @param color {number} The color of the glow. Valid values are in the hexadecimal format 0xRRGGBB. The default value is 0xFF0000.
         * @param alpha {number} The alpha transparency value for the color. Valid values are 0 to 1. For example, .25 sets a transparency value of 25%. The default value is 1.
         * @param blurX {number} The amount of horizontal blur. Valid values are 0 to 255 (floating point).
         * @param blurY {number} The amount of vertical blur. Valid values are 0 to 255 (floating point).
         * @param strength {number} The strength of the imprint or spread. The higher the value, the more color is imprinted and the stronger the contrast between the glow and the background. Valid values are 0 to 255.
         * @param quality {number} The number of times to apply the filter.
         * @param inner {boolean} Specifies whether the glow is an inner glow. The value true indicates an inner glow. The default is false, an outer glow (a glow around the outer edges of the object).
         * @param knockout {number} Specifies whether the object has a knockout effect. A value of true makes the object's fill transparent and reveals the background color of the document. The default value is false (no knockout effect).
         * @version Egret 3.1.4
         * @platform Web
         * @language en_US
         */
        /**
         * 初始化 GlowFilter 对象
         * @method egret.GlowFilter#constructor
         * @param color {number} 光晕颜色，采用十六进制格式 0xRRGGBB。默认值为 0xFF0000。
         * @param alpha {number} 颜色的 Alpha 透明度值。有效值为 0 到 1。例如，0.25 设置透明度值为 25%。
         * @param blurX {number} 水平模糊量。有效值为 0 到 255（浮点）。
         * @param blurY {number} 垂直模糊量。有效值为 0 到 255（浮点）。
         * @param strength {number} 印记或跨页的强度。该值越高，压印的颜色越深，而且发光与背景之间的对比度也越强。有效值为 0 到 255。
         * @param quality {number} 应用滤镜的次数。暂未实现。
         * @param inner {boolean} 指定发光是否为内侧发光。值 true 指定发光是内侧发光。值 false 指定发光是外侧发光（对象外缘周围的发光）。
         * @param knockout {number} 指定对象是否具有挖空效果。值为 true 将使对象的填充变为透明，并显示文档的背景颜色。
         * @version Egret 3.1.4
         * @platform Web
         * @language zh_CN
         */
        constructor(color?: number, alpha?: number, blurX?: number, blurY?: number, strength?: number, quality?: number, inner?: boolean, knockout?: boolean);
        /**
         * @private
         */
        $color: number;
        /**
         * The color of the glow.
         * @version Egret 3.1.4
         * @platform Web
         * @language en_US
         */
        /**
         * 光晕颜色。
         * @version Egret 3.1.4
         * @platform Web
         * @language zh_CN
         */
        get color(): number;
        set color(value: number);
        /**
         * @private
         */
        $alpha: number;
        /**
         * The alpha transparency value for the color.
         * @version Egret 3.1.4
         * @platform Web
         * @language en_US
         */
        /**
         * 颜色的 Alpha 透明度值。
         * @version Egret 3.1.4
         * @platform Web
         * @language zh_CN
         */
        get alpha(): number;
        set alpha(value: number);
        /**
         * @private
         */
        $blurX: number;
        /**
         * The amount of horizontal blur.
         * @version Egret 3.1.4
         * @platform Web
         * @language en_US
         */
        /**
         * 水平模糊量。
         * @version Egret 3.1.4
         * @platform Web
         * @language zh_CN
         */
        get blurX(): number;
        set blurX(value: number);
        /**
         * @private
         */
        $blurY: number;
        /**
         * The amount of vertical blur.
         * @version Egret 3.1.4
         * @platform Web
         * @language en_US
         */
        /**
         * 垂直模糊量。
         * @version Egret 3.1.4
         * @platform Web
         * @language zh_CN
         */
        get blurY(): number;
        set blurY(value: number);
        /**
         * @private
         */
        $strength: number;
        /**
         * The strength of the imprint or spread.
         * @version Egret 3.1.4
         * @platform Web
         * @language en_US
         */
        /**
         * 印记或跨页的强度。
         * @version Egret 3.1.4
         * @platform Web
         * @language zh_CN
         */
        get strength(): number;
        set strength(value: number);
        /**
         * @private
         */
        $quality: number;
        /**
         * The number of times to apply the filter.
         * @version Egret 3.1.4
         * @platform Web
         * @language en_US
         */
        /**
         * 应用滤镜的次数。
         * @version Egret 3.1.4
         * @platform Web
         * @language zh_CN
         */
        get quality(): number;
        set quality(value: number);
        /**
         * @private
         */
        $inner: boolean;
        /**
         * Specifies whether the glow is an inner glow.
         * @version Egret 3.1.4
         * @platform Web
         * @language en_US
         */
        /**
         * 指定发光是否为内侧发光。
         * @version Egret 3.1.4
         * @platform Web
         * @language zh_CN
         */
        get inner(): boolean;
        set inner(value: boolean);
        /**
         * @private
         */
        $knockout: boolean;
        /**
         * Specifies whether the object has a knockout effect.
         * @version Egret 3.1.4
         * @platform Web
         * @language en_US
         */
        /**
         * 指定对象是否具有挖空效果。
         * @version Egret 3.1.4
         * @platform Web
         * @language zh_CN
         */
        get knockout(): boolean;
        set knockout(value: boolean);
        /**
         * @private
         */
        $toJson(): string;
        protected updatePadding(): void;
    }
}
