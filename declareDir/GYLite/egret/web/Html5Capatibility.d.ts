declare namespace egret.web {
    /**
     * @private
     */
    class AudioType {
        /**
         * @private
         */
        static WEB_AUDIO: number;
        /**
         * @private
         */
        static HTML5_AUDIO: number;
    }
    /**
     * html5兼容性配置
     * @private
     */
    class Html5Capatibility extends HashObject {
        static _canUseBlob: boolean;
        static _audioType: number;
        /**
         * @private
         */
        static _AudioClass: any;
        /**
         * @private
         */
        constructor();
        /**
         * @private
         */
        private static ua;
        /**
         * @private
         *
         */
        static $init(): void;
        private static setAudioType;
        /**
         * @private
         * 获取ios版本
         * @returns {string}
         */
        private static getIOSVersion;
    }
    /**
     * @private
     */
    function getPrefixStyleName(name: string, element?: any): string;
    /**
     * @private
     */
    function getPrefix(name: string, element: any): string;
}
