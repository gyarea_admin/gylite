declare module GYLite {
    /**富文本的文本单元 @see GYRichText*/
    class RichLabel extends GYText implements IPoolObject {
        private _linkFun;
        private _linkThisObject;
        /**@param textFormat 文本格式
         * @param indent 首行缩进
         * */
        constructor();
        clear(): void;
        set height(val: number);
        get height(): number;
        get linkFun(): Function;
        setLinkFun(value: Function, thisObject: any): void;
        cutLastLine(): string;
        getElementString(e: any): string;
        get inPool(): boolean;
        set inPool(val: boolean);
        outPoolInit(): void;
        private _inPool;
        private _lastLineHtml;
        private _lastLineText;
    }
}
