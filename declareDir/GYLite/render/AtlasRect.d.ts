declare module GYLite {
    class AtlasRect extends egret.Rectangle implements GYLite.IPoolObject {
        /**创建虚拟图集区域
         * @param x 区域左上角x
         * @param y 区域左上角y
         * @param w 区域宽度(包括间隙gap)
         * @param h 区域高度(包括间隙gap)
         * @param parent 父级区域
         * @return AtlasRect区域
        */
        static createAtlasRect(x: number, y: number, w: number, h: number, parent: AtlasRect): AtlasRect;
        static maxSize: number;
        static minSize: number;
        /**(公共变量)图集空白区域队列*/ emptyRects: Array<AtlasRect>;
        /**(公共变量)图集纹理区域队列*/ textureRects: Array<AtlasRect>;
        /**(公共变量)图集名称*/ atlasName: string;
        /**(公共变量)id*/ atlasId: number;
        /**小图之间的间隙*/ gap: number;
        /**是否图集Rect*/ isMain: boolean;
        $childVec: Array<AtlasRect>;
        $parent: AtlasRect;
        /**小图纹理名称*/ name: string;
        /**纹理宽度，空白区域此属性为NaN*/
        textureWidth: number;
        /**纹理高度，空白区域此属性为NaN*/
        textureHeight: number;
        /**纹理区域的宽度、高度*/
        atlasTexSize: number[];
        /**纹理（指定的此区域的纹理）*/
        texture: egret.Texture;
        /**去除透明像素的X偏移*/ offX: number;
        /**去除透明像素的Y偏移*/ offY: number;
        /**包括透明像素的原始宽度*/ sourceWidth: number;
        /**包括透明像素的原始高度*/ sourceHeight: number;
        /**图集管理器，只有主图集有管理器的引用*/ private _mgr;
        /**创建虚拟图集区域
         * @param x 区域左上角x
         * @param y 区域左上角y
         * @param w 区域宽度
         * @param h 区域高度
         * @param mgr 图集管理器，只有主图集有管理器的引用
        */
        constructor(x?: number, y?: number, w?: number, h?: number, mgr?: AtlasManager, isMain?: boolean);
        /**虚拟图集管理器*/
        get mgr(): AtlasManager;
        /**图集资源唯一键 图集名称+id(id其实也是唯一的)*/
        get resKey(): string;
        /**添加空白区域*/
        addEmptyRect(atlasRect: AtlasRect): void;
        /**添加纹理区域*/
        addTextureRect(atlasRect: AtlasRect): void;
        /**拷贝引用父级图集的内容(因此内容共享于最外层主图集)*/
        copyAtlasProp(rect: AtlasRect): void;
        findTextureRect(texName: string): AtlasRect;
        /**裁切出指定宽高的空白区域
         * @param w 宽度（不包括间隙）
         * @param h 高度（不包括间隙）
         * @param type 裁剪策略 排列方式0 偏向正方形 1 优先横向 2 优先纵向
         * @param gap 间隙
         * @return 空间不足返回null，裁切成功返回AtlasRect（其中宽高是包括间隙的）
        */
        cutRect(w: number, h: number, type?: number, gap?: number): AtlasRect;
        private getClipRect;
        /**请求回到空白队列*/
        backToEmpty(): AtlasRect;
        /**图集尺寸最小化*/
        atlasMinResize(): void;
        /**是否已经填充图集*/
        isTexture(): boolean;
        /**是否已经被裁切使用*/
        isUse(): Boolean;
        /**输出图集配置*/
        outputJson(): any;
        /**输入图集配置*/
        inputJson(obj: any): void;
        createJsonSheet(): void;
        /**输出图集数据文件
         * @param show 是否显示到body上
         * @param callBack 完成回调 (result:IOutputAtlasResult[]):void=>{}
         * @param thisObj this指向
        */
        static outputAtlas(atlasRects: AtlasRect[], show: boolean, callBack: Function, thisObj: any, result?: IOutputAtlasResult[]): void;
        inPool: boolean;
        disposed: boolean;
        protected beforeToPool(): void;
        clear(): void;
        outPoolInit(): void;
        dispose(): void;
    }
    interface IOutputAtlasResult {
        json: any;
        blob: Blob;
    }
}
