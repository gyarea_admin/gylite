declare module GYLite {
    /**富文本的图片单元 @see GYRichText*/
    class RichSeqImage extends GYSeqImage implements IPoolObject {
        owner: GYRichText;
        constructor();
        getInit(): void;
        $onAddToStage(stage: egret.Stage, nestLevel: number): void;
        $onRemoveFromStage(): void;
        protected clipScroll(e?: GYViewEvent): void;
        get inPool(): boolean;
        set inPool(val: boolean);
        outPoolInit(): void;
        private _inPool;
        /**清理回收*/
        clear(): void;
        static imageList: Array<any>;
        /**获取图片实例
         * @param pr 父级对象
         * @param posX s.x坐标
         * @param posY s.y坐标
         * @param param 参数*/
        static getInstance(pr: GYRichText, posX: number, posY: number, param: string): RichSeqImage;
        /**获取图片文本
         * @param w-宽度 h-高度 Id-图片id
         * */
        static getImageRich(Id?: number, offsetX?: number, offsetY?: number, w?: number, h?: number): string;
    }
}
