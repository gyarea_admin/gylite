declare module GYLite {
    class TimeManager {
        private static timer;
        static dic: Array<any>;
        static timeoutDic: Dictionary;
        static timeIntervalDic: Dictionary;
        static init(): void;
        /**注册秒计监听
         * @param func 回调函数func(t:number);t为程序启动距离现在的毫秒数
         * @param thisObject
         * @return id 监听函数的唯一id
         * */
        static registered(func: Function, thisObject: any): number;
        private static getId;
        private static timeEvent;
        /**移除秒计监听
         * @param id 唯一标识id
         * @param func 监听函数
         * */
        static unRegistered(id: number, func: Function, thisObject: any): void;
        /**功能如同setTimeout，结束内部会自动调clearTimeout清理，重复调会自动停止之前未执行的timeout(请注意匿名函数不算重复的)*/
        static timeOut(func: Function, thisObject: any, delay: number, ...rest: any[]): number;
        /**移除timeout
         * @param id 唯一标识id
         * @param func 监听函数
         * */
        static unTimeOut(id: number, func: Function, thisObject: any): void;
        /**功能如同setInterval，重复调会自动停止之前未执行的timeInterval(请注意匿名函数不算重复的)*/
        static timeInterval(func: Function, thisObject: any, delay: number, ...rest: any[]): number;
        /**移除TimeManager.timeInterval监听
         * @param id 唯一标识id
         * @param func 监听函数
         * */
        static unTimeInterval(id: number, func: Function, thisObject: any): void;
    }
}
