module GYLite {
	/**单次添加小图到虚拟图集的基本信息*/
	export class AtlasInfo {
		/**关联的图集名称*/public atlasName:string;
		/**图集id*/public atlasId:number;
		/**图集宽度*/public width:number;
		/**图集高度*/public height:number;
		/**小图信息*/public rectInfo:RectInfo;
		public constructor(atlasName:string,atlasId:number,width:number,height:number,rectInfo:any=null) {
			let s= this;
			s.atlasId = atlasId;
			s.atlasName = atlasName;
			s.width = width;
			s.height = height;
			s.rectInfo = rectInfo;
		}
		public get resKey():string
		{
			return this.atlasName + "_" + this.atlasId;
		}
	}
	/**小图在虚拟图集的基本信息*/
	export class RectInfo {
		/**小图间隙*/public gap:number;
		/**在图集中的定位x（不包括间隙，包括透明像素）*/public innerX:number;
		/**在图集中的定位y（不包括间隙，包括透明像素）*/public innerY:number;
		/**图测量宽度（不包括间隙，不包括透明像素）*/public innerWidth:number;
		/**图测量高度（不包括间隙，不包括透明像素）*/public innerHeight:number;
		/**在图集中的定位x坐标(边框包括间隙)*/public x:number;
		/**在图集中的定位y坐标(边框包括间隙)*/public y:number;
		/**外框宽度(包括间隙，包括透明像素)*/public width:number;
		/**外框高度(包括间隙，包括透明像素)*/public height:number;
		/**小图名称*/public texName:string;
		/**合批绘制配置，默认null*/public drawParam:BatchDrawParam;
		/**透明像素偏移x*/public offX:number;
		/**透明像素偏移y*/public offY:number;
		/**图片宽度(不包括间隙，包括透明像素)*/public sourceWidth:number;
		/**图片高度(不包括间隙，包括透明像素)*/public sourceHeight:number;
		public constructor(texName:string,x:number,y:number,width:number,height:number,gap:number=1,offX:number=0,offY:number=0,sourceW:number=NaN,sourceH:number=NaN) {
			let s= this;
			s.offX = offX;
			s.offY = offY;
			s.gap = gap;
			s.x = x;
			s.y = y;
			s.width = sourceW == sourceW?sourceW:width;
			s.height = sourceH == sourceH?sourceH:height;
			s.innerX = x + gap + offX;
			s.innerY = y + gap + offY;
			s.sourceWidth = s.innerWidth = width - gap*2;
			s.sourceHeight = s.innerHeight = height - gap*2;
			s.texName = texName;
		}
	}
}