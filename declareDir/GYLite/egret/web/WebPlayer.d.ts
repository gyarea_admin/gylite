declare namespace egret.web {
    /**
     * @private
     */
    class WebPlayer extends egret.HashObject implements egret.sys.Screen {
        constructor(container: any, options: runEgretOptions);
        private init;
        private initOrientation;
        /**
         * 读取初始化参数
         */
        private readOption;
        /**
         * @private
         * 添加canvas到container。
         */
        private attachCanvas;
        playerOption: PlayerOption;
        /**
         * @private
         * 画布实例
         */
        private canvas;
        /**
         * @private
         * 播放器容器实例
         */
        private container;
        /**
         * @private
         * 舞台引用
         */
        stage: Stage;
        private webTouchHandler;
        private player;
        private webInput;
        private updateAfterTyping;
        /**
         * @private
         * 更新播放器视口尺寸
         */
        updateScreenSize(): void;
        setContentSize(width: number, height: number): void;
        /**
         * @private
         * 更新触摸数量
         */
        updateMaxTouches(): void;
    }
}
