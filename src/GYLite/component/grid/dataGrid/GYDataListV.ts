/**
 @author 迷途小羔羊
 2015.11.20
 */
module GYLite
{
								
	export class GYDataListV extends GYListV
	{
		protected _gridDataVec:GridData[];
		protected _maxGridHeight:number;
		protected _contentGrp:GYGroup;
		protected _lastEndBox:number=-1;
		protected _gridControler:boolean;
		
		/**垂直List组件 格子高度自定义(格子高度固定建议使用GYListV) 项数根据高度自适应 
		 * @param boxCount 初始化项数据 0初始化 非0 则需要外部提供s.gridDataVec的行数据
		 * @param getGridFunc():IItemRender 返回自定义格子对象的方法 
		 * @param setGridFunc(IItemRender, any) 设置格子数据的方法*/
		public constructor(boxCount:number, getGridFunc:Function=null, getThisObject:any=null, setGridFunc:Function=null, setThisObject:any=null)
		{
			super(0, getGridFunc, getThisObject, setGridFunc, setThisObject);
			var s = this;
			s._gridControler = (boxCount == 0);
			if(s._gridControler)
			{				
				s._gridDataVec = new Array<GridData>();
			}
		}
		protected initComponent():void
		{
			let s = this;
			super.initComponent();
			s._maxGridHeight=0;		
			s._lastEndBox=-1;			
			s._contentGrp = new GYGroup;
			s._contentGrp.canDrag = false;
			s._contentGrp.clipAndEnableScrolling = true;
			s._contentGrp.outSideOptimize = false;
			s.addElement(s._contentGrp);
			if(s._boxH == 0)s._boxH = GYDataListV.default_boxH;
			if(s._boxW == 0)s._boxW = GYDataListV.default_boxW;
		}
		/**是否格子数据的控制器（当GYDataList的gridDataVec引用外部数据源，则当前列表不是gridDataVec的控制器，则其不能修改此数据，否则会造成无法与外部数据同步）*/
		public get gridControler():boolean
		{
			return this._gridControler;
		}
		public set gridDataVec(val:GridData[])
		{
			let s = this;
			s._gridDataVec = val;
		}
		/**每行的格子信息*/
		public get gridDataVec():GridData[]
		{
			let s = this;
			return s._gridDataVec;
		}
		public getGridData(val:number):GridData
		{
			let s = this;
			if(val < s._gridDataVec.length)
				return s._gridDataVec[val];
			var i:number,len:number;
			var gridData:GridData,oldGridData:GridData;
			len = val + 1;
			if(s._gridDataVec.length > 0)
				oldGridData = s._gridDataVec[s._gridDataVec.length - 1];
			for(i=s._gridDataVec.length;i<len;++i)
			{
				gridData = s._gridDataVec[i] = new GridData;
				gridData.index = i;
				gridData.height = s._boxH;
				gridData.width = s._boxW;
				if(s._boxH > s._maxGridHeight)
					s._maxGridHeight= s._boxH;
				if(oldGridData)
				{
					gridData.posY = oldGridData.posY + oldGridData.height;
				}
				oldGridData = gridData;
			}
			return gridData;
		}
		/**@inheritDoc*/
		public scrollToIndex(val:number)
		{
			let s = this;
			var gridData:GridData;
			gridData = s._gridDataVec[val];
			s.vScroller.position = gridData.posY;
		}
		/**@inheritDoc*/
		public scrollToEndIndex(val:number):void
		{
			let s = this;
			var gridData:GridData;
			gridData = s._gridDataVec[val];
			s.vScroller.position = gridData.posY - s._innerHeight + gridData.height;
		}
		/**@inheritDoc*/
		public updateGrid():void
		{
			let s = this;
			var gridData:GridData;
			if(s._dataProvider && s._dataProvider.length > 0)
			{
				gridData = s.getGridData(s._dataProvider.length - 1);
				s._max=gridData.posY + gridData.height;
			}
			else
				s._max = s._contentGrp.baseHeight;
			s.vScroller.maximum = s._max-s._contentGrp.baseHeight;
			s.vScroller.barPercent = s._contentGrp.baseHeight/s._max;
			s.scrollerPolicy = s._scrollerPolicy;
			if(s.vScroller.position > s.vScroller.maximum)
				s.vScroller.position = s.vScroller.maximum;
			
			s.vScrollChange(null, true);
		}
		/**@inheritDoc*/
		public get dataProvider():Array<any>
		{
			let s = this;
			return s._dataProvider;
		}
		public set dataProvider(val:Array<any>)
		{
			let s = this;
			if(!s._keepSelected)
			{
				s._selectedData = null;
				s._selectIndex = -1;
				if(s._canSelectNum > 1)
					s._selectList.length = 0;
			}
			s._dataProvider = (val?val:[]);
			var endGridData:GridData;
			s.clearGridData();
			if(s._dataProvider && s._dataProvider.length > 0)
			{
				endGridData = s.getGridData(s._dataProvider.length - 1);
				s._max=endGridData.posY + endGridData.height;
			}
			else
				s._max = s._contentGrp.baseHeight;
			s.vScroller.maximum = s._max - s._contentGrp.baseHeight;
			s.vScroller.barPercent = s._contentGrp.baseHeight/s._max;
			s.scrollerPolicy = s._scrollerPolicy;
			if(s.vScroller.position > s.vScroller.maximum)
				s.vScroller.position = s.vScroller.maximum;
			
			s.vScrollChange(null, true);
		}
		private clearGridData()
		{
			let s = this;
			if(s._gridControler)
				s._gridDataVec.length = 0;
		}
		protected vScrollChange(e:GYScrollerEvent=null, update:boolean=false, updatePos:boolean = true):void
		{
			let s = this;
			if(s._dataProvider==null)
				return;
			var val:number;
			var h:number=0,offsetY:number=0,i:number=0,len:number=0,len2:number=0;
			var nowLine:number=0,nowEndLine:number=0,iLine:number=0,posY:number=0,maxSize:number=0,oldBoxs:number=0;
			var grid:IItemRender;
			var nowGridData:GridData;
			var isShift:boolean;
			var n:number=0,nLen:number=0,m:number=0,mLen:number=0;
			val = s.vScroller.position;
				
			nowGridData = s.getNowGridData(val);
			if(nowGridData)
			{
				offsetY = val - nowGridData.posY;
				nowLine = nowGridData.index;
			}
			maxSize = s._contentGrp.baseHeight;
			posY = -offsetY;
			while(posY < maxSize)
			{
				iLine = nowLine+i;
				h = s.getGridData(iLine).height;
				posY += h;
				++i;
			}
			nowEndLine = nowLine + i - 1;
			oldBoxs = s._boxs;
			s._boxs = nowEndLine - nowLine + 1;
			if(nowLine > s._lastBox)
			{
				if(nowLine <= s._lastEndBox)
				{
					len = nowLine - s._lastBox;
					for(i=0;i<len;++i)
					{
						grid = s._grids[0];
						s._grids.splice(oldBoxs, 0,grid);
						s._grids.shift();
					}
				}
			}
			else if(nowLine < s._lastBox)
			{
				var tempInd:number;
				if(nowEndLine >= s._lastBox)
				{
					len = s._lastBox - nowLine;
					if(len > 0)
					{
						isShift = true;
						n = 0;
						nLen = s._lastEndBox - nowEndLine;
						if(nLen < 0)
						{
							m = s._lastEndBox - nowLine + 1;
							mLen =  m-nLen;
							nLen = len;
						}
						else
						{
							mLen = 0;
							nLen = len  - nLen;
						}
						for(i=0;i<len;++i)
						{
							tempInd = nowEndLine - s._lastBox + 1 + i;
							if(tempInd < s._grids.length)
							{
								grid = s._grids[tempInd];
								s._grids.splice(tempInd,1);
								s._grids.unshift(grid);							
							}
							else
								s._grids.unshift(s.getGrid());
						}
					}
				}
			}

			if(oldBoxs > s._boxs)
			{
				for(i=s._boxs;i < oldBoxs; ++i)
				{
					grid = s._grids[i];
					s._contentGrp.removeElement(grid);
				}
			}
			else
			{
				if(isShift)
				{
					for(i = n;i < nLen; ++i)
					{
						grid = s._grids[i];
						s._contentGrp.addElement(grid);
					}
					for(i = m;i < mLen; ++i)
					{
						if(i < s._grids.length)
						{
							grid = s._grids[i];
							s._contentGrp.addElement(grid);
						}
						else
							s._grids.push(s._contentGrp.addElement(s.getGrid()));
					}
				}
				else
				{
					for(i=oldBoxs;i < s._boxs; ++i)
					{
						if(i < s._grids.length)
						{
							grid = s._grids[i];
							s._contentGrp.addElement(grid);			
						}
						else
							s._grids.push(s._contentGrp.addElement(s.getGrid()));
					}
				}
			}

			posY = 0;
			for(i=0;i<s._boxs;++i)
			{
				grid = s._grids[i];
				iLine = nowLine + i;
				grid.itemIndex = iLine;
				nowGridData = s._gridDataVec[iLine];
				if(s._lastBox > iLine || s._lastEndBox < iLine || update)
				{
					s.setGrid(grid,s._dataProvider[iLine]);
				}					
				grid.height = nowGridData.height==nowGridData.height?nowGridData.height:s._boxH;				
				grid.width = nowGridData.width==nowGridData.width?nowGridData.width:s._boxW;
				if(updatePos)
				{
					grid.y = posY - offsetY;
					posY += grid.height;
					s._contentGrp.checkOutSize(grid,false);
				}				
			}
			s._lastBox = nowLine;
			s._lastEndBox = nowEndLine;
		}
		protected getNowGridData(val:number):GridData
		{
			let s = this;
			var st:number = (val / s._maxGridHeight >> 0);
			var i:number,len:number;
			var gridData:GridData,oldGridData:GridData;
			len = s._gridDataVec.length;
			for(i=0;i<len;++i)
			{
				gridData = s._gridDataVec[i];
				if(gridData.posY > val)
				{
					if(oldGridData)
						return oldGridData;
					return gridData;
				}
				oldGridData= gridData;
			}
			return gridData;
		}
		
		/**设置某行高度
		 * @param ind 行索引 val 高度值
		 * */
		public setGridHeight(ind:number,val:number):void
		{
			let s = this;
			if(ind >= s._gridDataVec.length)
			{
				throw(new Error("索引"+ind+"超出数组长度"))				
			}
			var gridData:GridData = s._gridDataVec[ind]; 
			if(gridData.height == val)
				return;
			if(s._maxGridHeight < val)
				s._maxGridHeight = val;
			gridData.height = val;
			// gridData.isSet = true;
			s.invalidGridData();
		}
		/**@inheritDoc*/
		public updateGridData():void
		{
			let s = this;
			var i:number,len:number;
			var gridData:GridData,oldGridData:GridData;
			len = s._gridDataVec.length;
			for(i=0;i<len;++i)
			{
				gridData = s._gridDataVec[i];
				if(oldGridData)
					gridData.posY = oldGridData.posY + oldGridData.height;
				oldGridData = gridData;
			}
			s.invalidGrids();
		}
		public set width(val:number)
		{
			let s = this;
			if(s._innerWidth == val)
				return;
			s._innerWidth = s._contentGrp.width = val;
			s.group_width = s._innerWidth + (s.vScroller.parent?s.vScroller.width:0);
		}	
		public get width():number
		{
			let s = this;			
			return egret.superGetter(GYDataListV, this, "width");
		}	
		public set height(val:number)
		{
			let s = this;
			if(s._innerHeight == val)
				return;
			s.vScroller.height = s.group_height = s._innerHeight = s._contentGrp.height = val;
			s.invalidGrids();
		}
		public get height():number
		{
			let s = this;
			return egret.superGetter(GYDataListV, this, "height");
		}
		
		public static default_boxH:number = 40;
		public static default_boxW:number = 40;
	}
}