/**
 @author 迷途小羔羊
 2015.3.22
 */
module GYLite
{
					
	export class GYLinkButton extends GYButton
	{
		public constructor(skin:any=null)
		{
			super(skin);
		}
		protected getThemeSkin():IGYSkin
		{			
			return GYSprite.skinTheme.GetLinkButtonSkin();
		}
	}
}