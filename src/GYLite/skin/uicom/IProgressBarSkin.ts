module GYLite
{
				
	export interface IProgressBarSkin extends IGYSkin
	{
		width:number;
		height:number;
		barWidth:number;
		barHeight:number;
		backgroundWidth:number;
		backgroundHeight:number;
		barX:number;
		barY:number;
		clipX:number;
		clipY:number;
		mode:number;
		rotation:number;		
		// readonly textSkin:GYText;		
		// readonly backgroundSkin:GYScaleSprite;
		// readonly barSkin:GYScaleSprite;
		textSkin:GYText;		
		backgroundSkin:GYScaleSprite;
		barSkin:GYScaleSprite;
//		s.copy(val:IProgressBarSkin):void;
//		s.clone():any;
	}
}