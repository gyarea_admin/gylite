declare namespace egret.sys {
    /**
     * @private
     * 位图渲染节点
     */
    class NormalBitmapNode extends RenderNode {
        constructor();
        /**
         * 要绘制的位图
         */
        image: BitmapData;
        /**
         * 控制在缩放时是否对位图进行平滑处理。
         */
        smoothing: boolean;
        /**
         * 图片宽度。WebGL渲染使用
         */
        imageWidth: number;
        /**
         * 图片高度。WebGL渲染使用
         */
        imageHeight: number;
        /**
         * 翻转
         */
        rotated: boolean;
        sourceX: number;
        sourceY: number;
        sourceW: number;
        sourceH: number;
        drawX: number;
        drawY: number;
        drawW: number;
        drawH: number;
        /**
         * 绘制一次位图
         */
        drawImage(sourceX: number, sourceY: number, sourceW: number, sourceH: number, drawX: number, drawY: number, drawW: number, drawH: number): void;
        /**
         * 在显示对象的$updateRenderNode()方法被调用前，自动清空自身的drawData数据。
         */
        cleanBeforeRender(): void;
    }
}
