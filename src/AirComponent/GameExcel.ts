module GYLite
{
				
	export class GameExcel extends GYExcel
	{
		public constructor(cfg:any=null)
		{
			super(cfg);
		}
		// protected getExcelDrag():GYSprite
		// {var s = this;
		// 	return (GYSprite.skinTheme as SanGuoTheme).getDraggerBtn();
		// } 
		protected createExcelGrid():IItemRender
		{var s = this;
			var grid:GameExcelGrid;
			grid=new GameExcelGrid;
			grid.excel = s;
			grid.selFrame = s._selFrame;
			return grid;
		}
		protected createColGrid():IItemRender
		{var s = this;
			var grid:GameExcelColGrid;
			grid = new GameExcelColGrid;
			grid.excel = s;
			return grid;
		}
		protected createRowGrid():IItemRender
		{var s = this;
			var grid:GameExcelRowGrid;
			grid = new GameExcelRowGrid;
			grid.excel = s;
			return grid;
		}
	}
}