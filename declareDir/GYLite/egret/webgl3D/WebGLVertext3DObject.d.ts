declare module GYLite {
    class WebGLVertext3DObject {
        vertexIndex: number;
        indexIndex: number;
        tpVertexIndex: number;
        tpIndexIndex: number;
        vertSize: number;
        vertByteSize: number;
        maxQuadsCount: number;
        maxVertexCount: number;
        maxIndicesCount: number;
        protected _vertices: ArrayBuffer;
        protected _verticesFloat32View: Float32Array;
        protected _verticesUint32View: Uint32Array;
        indices: Uint16Array;
        protected _tpVertices: ArrayBuffer;
        protected _tpVerticesFloat32View: Float32Array;
        protected _tpVerticesUint32View: Uint32Array;
        tpIndices: Uint16Array;
        vertextIndex: number;
        indicesIndex: number;
        vertices: ArrayBuffer;
        tpVertices: ArrayBuffer;
        constructor();
        reachMaxSize(vertexCount?: number, indexCount?: number): boolean;
        getVertices(): ArrayBuffer;
        getIndices(): ArrayBuffer;
        cacheArrays(buffer: any, sourceX: any, sourceY: any, sourceWidth: any, sourceHeight: any, destX: any, destY: any, destWidth: any, destHeight: any, textureSourceWidth: any, textureSourceHeight: any, meshUVs: any, meshVertices: any, meshIndices: any, rotated: any, color: any): void;
    }
}
