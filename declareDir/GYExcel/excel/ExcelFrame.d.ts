declare module GYLite {
    class ExcelFrame extends GYSprite {
        private _operHand;
        startCol: number;
        startRow: number;
        endCol: number;
        endRow: number;
        calEndCol: number;
        calEndRow: number;
        calStartCol: number;
        calStartRow: number;
        owner: GYDataGrid;
        excel: GYExcel;
        private _nowCalStartCol;
        private _nowCalEndCol;
        private _nowStartCol;
        private _nowEndCol;
        private _nowCalStartRow;
        private _nowCalEndRow;
        private _nowStartRow;
        private _nowEndRow;
        private _updateFrame;
        private _selFrame;
        private _calFrame;
        private _drager;
        private _calDisX;
        private _calDisY;
        private _calDir;
        private _selStartX;
        private _selStartY;
        private _selWidth;
        private _selHeight;
        constructor(e: GYExcel);
        private init;
        private drawCalFrame;
        private drawBg;
        private calDrag;
        private calDragEnd;
        updateView(): void;
        /**选框重新布局*/
        invalidFrame(): void;
        private updateFrameView;
        /**选框手柄*/
        get operHand(): GYSprite;
        /**选框起始行*/
        get nowStartRow(): number;
        /**选框起始列*/
        get nowStartCol(): number;
        /**选框结束行*/
        get nowEndRow(): number;
        /**选框结束列*/
        get nowEndCol(): number;
        get drager(): DraggerHandle;
    }
}
