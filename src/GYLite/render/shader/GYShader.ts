module GYLite{
    export class GYShader{
        private static _d2_vert_src:string;
        private static _d2_frag_src:string;
        private static _d2_primitive_frag:string;
        public static getD2VertSrc():string
        {
            if(GYShader._d2_vert_src == null)
            {
                GYShader._d2_vert_src = `
                    attribute vec2 aVertexPosition;
                    attribute vec2 aTextureCoord;
                    attribute vec4 aColor;
                    uniform vec2 projectionVector;
                    // uniform vec2 offsetVector;
                    varying vec2 vTextureCoord;
                    varying vec4 vColor;
                    const vec2 center = vec2(-1.0, 1.0);
                    void main(void) {   
                        gl_Position = vec4( (aVertexPosition / projectionVector) + center , 0.0, 1.0);
                        vTextureCoord = aTextureCoord;
                        vColor = aColor;
                    }
                `;
            }
            return GYShader._d2_vert_src;
        }
        public static getD2FragSrc():string
        {
            if(GYShader._d2_frag_src == null)
            {
                GYShader._d2_frag_src = `
                    precision lowp float;
                    varying vec2 vTextureCoord;
                    varying vec4 vColor;
                    uniform sampler2D uSampler;                    
                    void main(void) {
                        vec4 v4c = texture2D(uSampler, vTextureCoord);                        
                        gl_FragColor = v4c * vColor;
                    }
                `;
            }
            return GYShader._d2_frag_src;
        }
        public static getD2_primitive_frag():string
        {
            if(GYShader._d2_primitive_frag== null)
            {
                GYShader._d2_primitive_frag = `precision lowp float;
                varying vec2 vTextureCoord;
                varying vec4 vColor;
                void main(void) {    
                    gl_FragColor = vColor;
                }`;
            }
            return GYShader._d2_primitive_frag;            
        }
    }
}