module GYLite
{
	/**布局策略类*/
	export class LayoutMode
	{
		/**绝对布局*/public static ABSOLUTE:number=0;
		/**相对布局*/public static RELATIVE:number=3;
		/**相对水平布局*/public static RELATIVE_HORIZON:number=1;
		/**相对垂直布局*/public static RELATIVE_VERTICAL:number=2;
		
		public $horizonalCenter:number;
		public $verticalCenter:number;
		public $right:number;
		public $bottom:number;
		public $left:number;
		public $top:number;
		/**宽度百分比[0,1]*/public percentWidth:number;
		/**高度百分比[0,1]*/public percentHeight:number;
		//private layoutMode:number;
		public constructor()
		{
			var s = this;
			s.$horizonalCenter=NaN;
			s.$verticalCenter=NaN;
			s.$right=NaN;
			s.$bottom=NaN;
			s.$left=NaN;
			s.$top=NaN;
			s.percentWidth=NaN;
			s.percentHeight=NaN;
		}
		public set horizonalCenter(val:number)
		{
			var s = this;
			if(val == val)
				s.$right=NaN;
			s.$horizonalCenter=val;
		}
		public get horizonalCenter():number
		{
			return this.$horizonalCenter
		}
		public set verticalCenter(val:number)
		{
			var s = this;
			if(val == val)
				s.$bottom=NaN;
			s.$verticalCenter=val;
		}
		public get verticalCenter():number
		{
			return this.$verticalCenter;
		}
		public set right(val:number)
		{
			var s = this;
			if(val == val)
				s.$horizonalCenter=NaN;
			s.$right=val;
		}
		/**相对左边*/
		public get left():number
		{
			return this.$left;
		}
		
		public set left(val:number)
		{
			var s = this;
			if(val == val)
				s.$horizonalCenter=NaN;
			s.$left = val;
		}
		/**相对上边*/
		public get top():number
		{
			return this.$top;
		}
		
		public set top(val:number)
		{
			var s = this;
			if(val == val)
				s.$verticalCenter=NaN;
			s.$top = val;
		}
		/**相对右边*/
		public get right():number
		{
			return this.$right;
		}
		public set bottom(val:number)
		{
			var s = this;
			if(val == val)
				this.$verticalCenter=NaN;
			this.$bottom=val;
		}
		/**相对底边*/
		public get bottom():number
		{
			return this.$bottom;
		}		
		public isPercentSize():boolean
		{
			var s = this;
			if(s.percentWidth != s.percentWidth && s.percentHeight != s.percentHeight)
				return false;
			else
				return true;
		}
		/**布局模式*/
		public get layoutMode():number
		{
			var s = this;
			let val:number;			
			val = LayoutMode.ABSOLUTE;
			if(s.$horizonalCenter == s.$horizonalCenter || s.$right == s.$right || s.$left == s.$left)
				val = LayoutMode.RELATIVE_HORIZON;
			if(s.$verticalCenter == s.$verticalCenter || s.$bottom == s.$bottom || s.$top == s.$top)
				val += LayoutMode.RELATIVE_VERTICAL;
			return val;
		}
		/**克隆布局策略*/
		public clone():LayoutMode
		{
			var s = this;
			var layout:LayoutMode = new LayoutMode;
			layout.bottom = s.$bottom;
			layout.horizonalCenter = s.$horizonalCenter;
			layout.percentHeight = s.percentHeight;
			layout.percentWidth = s.percentWidth;
			layout.right = s.$right;
			layout.verticalCenter = s.$verticalCenter;
			return layout;
		}


	}
}