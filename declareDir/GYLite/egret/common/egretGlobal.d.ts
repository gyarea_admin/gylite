declare module egret {
    var bufferScale: number;
    function getString(code: any): any;
    function $error(code: any, ...rest: any[]): void;
    function $warn(code: any, ...rest: any[]): void;
    function $markReadOnly(instance: any, property: any, isProperty: any): void;
    function $markCannotUse(instance: any, property: any, defaultValue: any): void;
}
