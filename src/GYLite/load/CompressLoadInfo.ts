module GYLite {
	export class CompressLoadInfo {
		private _progFunc:Function;
		private _callBackFunc:Function;
		private _resCount:number;
		private _resMax:number;
		public curLoadInfo:LoadInfo;
		public thisObject:any;
		public param:any;
		public msg:string;
		public errorCode:number;
		public startTime:number;
		public progressInfo:any[];
		public loader:GYLoader;		
		public constructor() {
			let s = this;
			s.errorCode = s._resCount = s._resMax = 0;
			s.startTime = Date.now();
			s.progressInfo = [];
			s.loader = new GYLoader(5,5);			
		}
		/**加载url的媒体资源，图片、mp3
		 * @param url url地址 
		 * @param type 类型 参考GYLoader常量
		*/
		public loadPath(url:string,type:number):void
		{
			let s= this;
			s.loader.loadPath(url,s.bytesCallBack,s,type);
		}
		/**加载url的数据资源
		 * @param url ur地址 
		 * @param type 类型 参考GYLoader常量
		*/
		public loadData(url:string,type:number):void
		{
			let s= this;
			s.loader.loadData(url,s.bytesCallBack,s,null,type,"GET");
		}
		/**读取bytes资源 png jpg bmp
		 * @param bytes uint8Array或者ByteArray的buffer属性
		 * @param callBackFunc 回调函数，返回参数为LoadInfo,返回后，loadInfo会自动被清理
		 * @param thisObject callBackFunc的this指向
		 * @param type 加载资源类型，参考GYLoader常量
		 * @param mimeType blob资源 如"image/png" 图片文件 "audio/basic" 声音文件
		 * @param param 附加参数
		 * @param retainBytes 是否保留加载的文件字节数据
		 * */
		public loadBytes(bytes:ArrayBuffer, type:number = 1, mimeType:string = "image/png",param:any = null,path:string=null,retainBytes:boolean=false):void
		{
			let s =this;			
			s.loader.loadBytes(bytes,s.bytesCallBack,s,type,mimeType,param,retainBytes).path = path;			
		}
		public setResCount(cur, max): void {
			let s = this;
			s._resCount = cur;
			s._resMax = max;
		}
		public set callBackFunc(val: Function) {
			let s = this;
			s._callBackFunc = val;
		}
		/**加载完成回调函数*/
		public get callBackFunc(): Function {
			let s = this;
			return s._callBackFunc;
		}
		public set progFunc(val: Function) {
			let s = this;
			s._progFunc = val;
		}
		/**加载进度回调函数*/
		public get progFunc(): Function {
			let s = this;
			return s._progFunc;
		}
		public callBack(l: LoadInfo = null,key:string=null,startTime:number=NaN) {
			let s = this;
			--s._resCount;			
			s.curLoadInfo = l;
			let obj:any;
			obj = {key:(key?key:(l?l.path:"null")),startTime:Date.now()};
			obj.time = l?(Date.now() - l.startTime):(startTime==startTime?(obj.startTime - startTime):(obj.startTime - s.startTime));
			if(l&&l.param)
			{
				obj.time += l.param.delayCount?l.param.delayCount:0;
				obj.compress = l.param.compress;				
			}			
			else
			{
				obj.compress = "none";
			}						
			s.progressInfo.push(obj);
			if (s._progFunc != null)
				s._progFunc.call(s.thisObject, s);
			if (s._resCount == 0) {				
				if (s._callBackFunc != null)
					s._callBackFunc.call(s.thisObject, s);
			}			
		}
		private bytesCallBack(l: LoadInfo = null,key:string=null,startTime:number=NaN):void
		{
			let s = this;
			s.callBack(l,key,startTime);			
		}

		/**资源总数*/
		public get resMax(): number {
			let s = this;
			return s._resMax;
		}

		/**未解压的资源数*/
		public get resCount(): number {
			let s = this;
			return s._resCount;
		}
	}
}