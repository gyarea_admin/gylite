module GYLite
{
							
			
	export class GameItem extends ItemRender
	{
		private static bgBitData:egret.Texture;
		private static frameBitData:egret.Texture;
		private static gridBitData:egret.Texture;
		public lab:GYText;
		public img:GYImage;
		public frame:GYImage;
		public gridImg:GYImage;
		// public maskShape:GYLite.GYSprite;
		public constructor()
		{
			super();			
			var s = this;
			s.touchEnabled = true;
			if(GameItem.bgBitData == null)
			{
				GameItem.bgBitData = GYLoader.getRes("11","Img/ui.png").refRes(s.owner);
				GameItem.frameBitData = GYLoader.getRes("12","Img/ui.png").refRes(s.owner);
				GameItem.gridBitData = GYLoader.getRes("13","Img/ui.png").refRes(s.owner);
			}
			s.width = 160;
			s.height = 60;
			s.lab = new GYText(true);
			s.lab.x = 65;
			s.lab.y = 30;
			s.lab.width = s.width - s.lab.x;
			s.lab.touchEnabled = false;
			// s.lab.selectable = false;
			s.lab.verticalCenter = 0;
			// s.lab.glow = true;			
			s.img = new GYImage;
			s.img.source = GameItem.bgBitData;
			s.addElement(s.img);
			s.frame = new GYImage;
			s.frame.x = s.frame.y = -2;
			s.frame.touchEnabled = false;
			s.gridImg = new GYImage;
			s.gridImg.x = 9;
			s.gridImg.y = 9;
			s.addElement(s.gridImg);
			s.addElement(s.lab);
			s.addElement(s.frame);
			// s.maskShape = new GYLite.GYSprite;
			// let g:egret.Graphics;
			// g = s.maskShape.graphics;
			// g.beginFill(0);
			// g.drawRoundRect(0,0,s.width,s.height,40,40);
			// g.endFill();
			// s.addElement(s.maskShape);
			// s.mask = s.maskShape;
		}
		public setData(obj:any):void
		{var s = this;
			s._data = obj;
			if(s._data==null)
			{
				s.visible = false;
				return;
			}
			s.visible = true;
			s.lab.htmlText = "<font color='#ff00ff'>" + s._data.Name + "</font>\n<font color='#ADFF2F'>售价：</font><font color='#f9f61b'>" + (s.itemIndex + 1) *  10 + "元宝</font>";
			s.gridImg.source = GameItem.gridBitData;
		}
		public set selected(b:boolean)
		{var s = this;
			egret.superSetter(GameItem, s, "selected", b);
			// super.selected = b;
			s.frame.source = (b?GameItem.frameBitData:null);
		}
		public get selected():boolean
		{var s = this;
			return egret.superGetter(GameItem, s, "selected");
		}
	}
}