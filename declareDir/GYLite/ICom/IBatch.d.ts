declare module GYLite {
    /**IBatch是一个可以支持纹理合批的显示对象接口
     * */
    interface IBatch {
        /**合批图集名称*/
        getBatchAtlasName(): string;
        setBatchAtlasName(val: string): void;
        $hashCode: number;
        /**显示对象唯一标识*/
        get hashCode(): number;
        /**合批绘制方式*/
        getBatchDrawParam(): BatchDrawParam;
        setBatchDrawParam(val: BatchDrawParam): void;
        /**当前是否合批状态*/
        isBatch(): boolean;
        /**是否开启合批*/
        enableBatch(val: boolean): void;
        /**清理合批引用*/
        clearBatch(): void;
    }
}
