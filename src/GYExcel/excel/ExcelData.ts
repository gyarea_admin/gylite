module GYLite
{
	export class ExcelData
	{
		public static TYPE_NONE:number = 0;
		public static TYPE_NUMBER:number = 1;
		public static TYPE_STRING:number = 2;
		public static TYPE_CURRENCY:number = 3;
		public static TYPE_DATE:number = 4;
		protected static date:Date = new Date;
		protected _dataType:number=0;
		protected formatFunc:Function=ExcelData.FormatValueNone;
		private _value:any;
		public col:number=0;
		public row:number=0;
		public index:number=0;
//		public currencySign:string;
		/**格式化为数字*/
		public static FormatValueByNumber(val:any):any
		{
			return Number(val);
		}
		/**格式化为字符串*/
		public static FormatValueByString(val:any):any
		{
			return String(val);
		}
		/**格式化为货币*/
		public static FormatValueByCURRENCY(val:any):any
		{
			return  "￥" + String(val);
		}
		/**格式化为日期*/
		public static FormatValueByDATE(val:any):any
		{
			ExcelData.date.setTime(Number(val));
			return  ExcelData.date.getFullYear() + "-" + (ExcelData.date.getMonth() + 1) + "-" + ExcelData.date.getDate();
		}
		/**无格式*/
		public static FormatValueNone(val:any):any
		{
			return val;
		}		
		public set value(val:any)
		{var s = this;
			s._value = s.formatFunc(val);
		}
		/**excel格子的数据*/
		public get value():any	
		{var s = this;
			return s._value;
		}
		public set dataType(val:number)
		{var s = this;
			s._dataType = val;
			if(s._dataType == ExcelData.TYPE_NUMBER)
				s.formatFunc = ExcelData.FormatValueByNumber;
			else if(s._dataType == ExcelData.TYPE_STRING)
				s.formatFunc = ExcelData.FormatValueByString;
			else if(s._dataType == ExcelData.TYPE_CURRENCY)
				s.formatFunc = ExcelData.FormatValueByCURRENCY;
			else if(s._dataType == ExcelData.TYPE_DATE)
				s.formatFunc = ExcelData.FormatValueByDATE;
			else
				s.formatFunc = ExcelData.FormatValueNone;
		}
		/**格子的格式,参照ExcelData常量*/
		public get dataType():number
		{var s = this;
			return s._dataType;
		}
	}
}