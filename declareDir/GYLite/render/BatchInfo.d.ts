declare module GYLite {
    /**小图的动态合批信息**/
    class BatchInfo extends egret.HashObject implements IResource, IPoolObject {
        $disposed: boolean;
        /**引用合批的显示对象列表*/
        protected _displayMap: any;
        /**合批图集信息*/
        atlasInfo: AtlasInfo;
        /**合批图集*/
        atlas: Atlas;
        /**合批后产生的资源对象*/
        batchRes: ResObject;
        /**关联合批的源纹理对象*/ $sourceTexture: egret.Texture;
        /**数据源*/ $source: HTMLImageElement | HTMLCanvasElement | GYTextElement;
        /**是否已经进行texSubImage上传gpu*/ isTexSubImage: boolean;
        /**合批后图集中的小图纹理的资源对象*/ $batchTexture: egret.Texture;
        protected _mgr: BatchManager;
        /**显示对象对batchInfo的引用数*/ $referenceCount: number;
        constructor(atlasInfo: AtlasInfo, atlas: Atlas, batchRes: ResObject, srcTex: egret.Texture, mgr?: BatchManager);
        get disposed(): boolean;
        /**测量绘制gpu纹理*/
        measureAndDraw(): void;
        /**添加引用的显示对象
        * @param display 显示对象
        */
        addReference(display: IBatch): void;
        /**移除引用的显示对象
         * @param display 显示对象
         * @return 返回是否无显示对象引用此batch
        */
        removeReference(display: IBatch): void;
        /**合批销毁*/
        dispose(): void;
        protected beforeToPool(): void;
        clear(): void;
        inPool: boolean;
        outPoolInit(): void;
    }
}
