declare module GYLite {
    /**单次添加小图到虚拟图集的基本信息*/
    class AtlasInfo {
        /**关联的图集名称*/ atlasName: string;
        /**图集id*/ atlasId: number;
        /**图集宽度*/ width: number;
        /**图集高度*/ height: number;
        /**小图信息*/ rectInfo: RectInfo;
        constructor(atlasName: string, atlasId: number, width: number, height: number, rectInfo?: any);
        get resKey(): string;
    }
    /**小图在虚拟图集的基本信息*/
    class RectInfo {
        /**小图间隙*/ gap: number;
        /**在图集中的定位x（不包括间隙，包括透明像素）*/ innerX: number;
        /**在图集中的定位y（不包括间隙，包括透明像素）*/ innerY: number;
        /**图测量宽度（不包括间隙，不包括透明像素）*/ innerWidth: number;
        /**图测量高度（不包括间隙，不包括透明像素）*/ innerHeight: number;
        /**在图集中的定位x坐标(边框包括间隙)*/ x: number;
        /**在图集中的定位y坐标(边框包括间隙)*/ y: number;
        /**外框宽度(包括间隙，包括透明像素)*/ width: number;
        /**外框高度(包括间隙，包括透明像素)*/ height: number;
        /**小图名称*/ texName: string;
        /**合批绘制配置，默认null*/ drawParam: BatchDrawParam;
        /**透明像素偏移x*/ offX: number;
        /**透明像素偏移y*/ offY: number;
        /**图片宽度(不包括间隙，包括透明像素)*/ sourceWidth: number;
        /**图片高度(不包括间隙，包括透明像素)*/ sourceHeight: number;
        constructor(texName: string, x: number, y: number, width: number, height: number, gap?: number, offX?: number, offY?: number, sourceW?: number, sourceH?: number);
    }
}
